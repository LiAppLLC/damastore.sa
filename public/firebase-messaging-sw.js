

importScripts('https://www.gstatic.com/firebasejs/7.23.0/firebase-app.js');
importScripts('https://www.gstatic.com/firebasejs/7.23.0/firebase-messaging.js');


firebase.initializeApp({
    apiKey: "AIzaSyCiUPH6xsFh8XzPHcxQTnyinXTpJQA_rQs",
    authDomain: "damastore-20a76.firebaseapp.com",
    projectId: "damastore-20a76",
    storageBucket: "damastore-20a76.appspot.com",
    messagingSenderId: "614626349242",
    appId: "1:614626349242:web:30baab3b46cfc4e2f1b283",
    measurementId: "G-PR8PXQ1928"
    });

/*
Retrieve an instance of Firebase Messaging so that it can handle background messages.
*/
const messaging = firebase.messaging();
messaging.setBackgroundMessageHandler(function(payload) {
    console.log(
        "[firebase-messaging-sw.js] Received background message ",
        payload,
    );
    /* Customize notification here */
    const notificationTitle = "Background Message Title";
    const notificationOptions = {
        body: "Background Message body.",
        icon: "/itwonders-web-logo.png",
    };

    return self.registration.showNotification(
        notificationTitle,
        notificationOptions,
    );
});
