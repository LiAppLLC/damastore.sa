<?php

use App\Http\Controllers\c\CproductsController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

//Route::middleware('auth:api')->get('/user', function (Request $request) {
//    return $request->user();
//});

/////////////////////////////////////////// ////
///Auth & User
Route::post('register', 'AuthController@register')->name('register');
Route::post('new_register', 'AuthController@register')->name('new_register');
Route::post('login', 'AuthController@login')->name('login');
Route::post('logout', 'AuthController@logout')->name('logout');
Route::post('refresh', 'AuthController@refresh')->name('refresh');
Route::post('me', 'AuthController@me')->name('me');
Route::post('IsEmailExists', 'AuthController@IsEmailExists')->name('IsEmailExists');
Route::post('checkToken', 'AuthController@CheckToken')->name('checkToken');
Route::post('sendVerificationCodeRegistering', 'AuthController@SendVerfication')->name('sendVerificationCodeRegistering');
Route::post('verifyaccount', 'AuthController@confrimEmail')->name('verifyaccount');
Route::post('sendVerificationCodeResetingPasssword', 'AuthController@SendResetpassword')->name('sendVerificationCodeResetingPasssword');
Route::post('changePassword', 'AuthController@changePassword')->name('api_changePassword');
Route::post('verifyRestingPassword', 'AuthController@resetpassword')->name('verifyRestingPassword');
Route::post('editUser', 'UserController@editUser')->name('editUser');
Route::post('getUserProfile', 'UserController@getUser')->name('getUserProfile');
Route::post('getAllUsersFilter/{pageIndex}/{pageSize}', 'UserController@getAll')->name('getAllUsersFilter');
Route::post('signInWithGoogle', 'View\emailController@signInWithGoogle')->name('signInWithGoogle');
Route::post('signInWithFacebook', 'View\emailController@signInWithFacebook')->name('signInWithFacebook');

Route::post('user_delete', 'UserController@user_delete')->name('user_delete');

// new
Route::post('loginByFaceBook', 'AuthController@loginByFaceBook')->name('loginByFaceBook');
///////////////////////////////////////////////
///File
Route::resource('file', 'FileController');
Route::post('upload', 'FileController@upload');
Route::post('getFilesByFilter/{index}/{size}', 'FileController@getByFilter');
///////////////////////////////////////////////
//////////////////////////////////////////
///Category
Route::get('category/{pageIndex}/{pageSize}', 'CategoryController@index');
Route::resource('category', 'CategoryController');
Route::post('deleteCategory/{id}', 'CategoryController@destroy');
Route::get('categoryChildren/{pageIndex}/{pageSize}', 'CategoryController@categoryChildren');
Route::post('getCategoriesByFilter', 'CategoryController@categoriesByFilter');
Route::post('editCategory/{id}', 'CategoryController@update');
Route::post('categoryChangeOrder', 'CategoryController@changeOrder');
Route::post('getSubCategoryFilter', 'CategoryController@getSubCategoryFilter');
////////////////////////////////////////////
///Product
Route::resource('product', 'ProductController')->except([
    'store', 'update'
]);
Route::post('getAllProducts/{index}/{size}', 'ProductController@getAllProducts');
Route::post('editProduct/{id}', 'ProductController@update');
Route::post('editProductProperties/{product_id}', 'ProductController@editProductProperties');
Route::post('getbyfilter/{index}/{size}', 'ProductController@getByFilter');
Route::post('getAllProductReviews/{index}/{size}', 'ProductController@getAllReviews');
Route::post('product/getbycategory/{index}/{size}/{id}', 'ProductController@getByCategory');
Route::post('product/getbycompany/{id}', 'ProductController@getByCompany');
Route::post('product/addtocompany/{id}', 'ProductController@addToCompany');
Route::post('product/like', 'ProductController@like');
Route::post('product/dislike', 'ProductController@dislike');
Route::post('product/softdelete/{id}', 'ProductController@softDelete');
////////////////////////////////////////////
///Home
Route::get('home/{index}/{size}', 'HomeController@getHomePage');
////////////////////////////////////////////
///Role
Route::post('addRole', 'RoleController@add');
Route::get('getRoleById/{id}', 'RoleController@getById');
Route::post('getAllRoles/{index}/{size}', 'RoleController@getAll');
Route::post('editRole/{id}', 'RoleController@Edit');
Route::post('deleteRole/{id}', 'RoleController@delete');
////////////////////////////////////////////
///Permission
Route::post('addPermission', 'PermissionController@add');
Route::get('getPermissionById/{id}', 'PermissionController@getById');
Route::post('getAllPermissions/{index}/{size}', 'PermissionController@getAll');
Route::post('editPermission/{id}', 'PermissionController@Edit');
Route::post('deletePermission/{id}', 'PermissionController@delete');
////////////////////////////////////////////
///User Roles
Route::post('addMultiUserRoles', 'UserRoleController@addMulti');
Route::post('getRolesByUser/{index}/{page}', 'UserRoleController@getByUser');
Route::post('deleteMultiUserRoles', 'UserRoleController@deleteMulti');
////////////////////////////////////////////
///Role Permissions
Route::post('addMultiRolePermission', 'RolePermissionController@addMulti');
Route::post('getPermissionsByRole/{index}/{size}', 'RolePermissionController@getByRole');
Route::post('deleteMultiRolePermission', 'RolePermissionController@deleteMulti');
////////////////////////////////////////////
///Order
Route::post('addOrder', 'OrderController@add');
Route::get('getOrderById/{id}', 'OrderController@getById');
Route::post('getAllOrders/{index}/{size}', 'OrderController@getAll');
Route::post('changeOrderStatus', 'OrderController@changeOrderStatus');
Route::post('refund', 'OrderController@refund')->name('refund');
Route::post('startOrder', 'paypal\paypalController@startOrder')->name('startOrder');
Route::post('failedOrder', 'paypal\paypalController@failedOrder')->name('failedOrder');
////////////////////////////////////////////

Route::post('HeaderAdd', 'View\configController@HeaderAdd')->name('HeaderAdd');
Route::post('getAllHeader', 'View\configController@getAllHeader')->name('getAllHeader');



////////////////////////////////////////////
/// product in admin panel
Route::post('category', 'ProductController@category')->name('category');
Route::post('Ccategory', 'ProductController@Ccategory')->name('Ccategory');
Route::post('Type', 'ProductController@Type')->name('Type');
Route::post('ProductAdd', 'ProductController@ProductAdd')->name('ProductAdd');
Route::post('ProductEdit', 'ProductController@ProductEdit')->name('ProductEdit');
Route::post('ProductDelete', 'ProductController@ProductDelete')->name('ProductDelete');
Route::post('TypeEdit', 'ProductController@TypeEdit')->name('TypeEdit');
Route::post('TypeAdd', 'ProductController@TypeAdd')->name('TypeAdd');
Route::post('TypeDelete', 'ProductController@TypeDelete')->name('TypeDelete');
Route::post('CTypeEdit', 'ProductController@CTypeEdit')->name('CTypeEdit');
Route::post('CTypeAdd', 'ProductController@CTypeAdd')->name('CTypeAdd');
Route::post('CTypeDelete', 'ProductController@CTypeDelete')->name('CTypeDelete');
Route::post('CType', 'ProductController@CType')->name('CType');
Route::post('ProductList/{pageIndex}/{pageSize}', 'ProductController@ProductList')->name('ProductList');
Route::post('HomePageProductList', 'ProductController@HomePageProductList')->name('HomePageProductList');
Route::post('TypesList/{pageIndex}/{pageSize}', 'ProductController@TypesList')->name('TypesList');
Route::post('CTypesList/{pageIndex}/{pageSize}', 'ProductController@CTypesList')->name('CTypesList');
Route::post('ProductCartList', 'ProductController@ProductCartList')->name('ProductCartList');
Route::post('related_product', 'ProductController@related_product')->name('related_product');
Route::post('get_products_by_id', 'ProductController@get_products_by_id')->name('get_products_by_id');


// apis for mobile devices

Route::post('categories', 'ApiController@categories')->name('categories_api');
Route::post('typesWithCate', 'ApiController@types')->name('types_api');
Route::post('getProduct', 'ApiController@get_product')->name('get_product_api');
Route::post('getListProducts/{pageIndex}/{pageSize}', 'ApiController@getListProducts')->name('getListProducts_api');
Route::post('searchByText/{pageIndex}/{pageSize}', 'ApiController@searchByText')->name('searchByText');

//paypal
Route::post('OrderCancel', 'paypal\paypalController@OrderCancel')->name('OrderCancel');

//end apis


/// attributes controller

Route::post('AttributeGet', 'AttributesController@AttributeGet')->name('AttributeGet');
Route::post('AttributeAdd', 'AttributesController@AttributeAdd')->name('AttributeAdd');
Route::post('AttributeEdit', 'AttributesController@AttributeEdit')->name('AttributeEdit');
Route::post('AttributeDelete', 'AttributesController@AttributeDelete')->name('AttributeDelete');
Route::post('AttributeDeleteForProduct', 'AttributesController@AttributeDelete_for_product')->name('AttributeDeleteForProduct');
Route::post('AttributeAddForProduct', 'AttributesController@AttributeAdd_for_product')->name('AttributeAddForProduct');

////////////////////////////////////////////



/////////////////////////////////////////////
Route::post('/getPrice', 'paypal\paypalController@getPrice')->name('getPrice');
Route::post('/submitPay', 'paypal\paypalController@submitPay')->name('submitPay');
Route::post('/submitedPaing', 'paypal\paypalController@submitedPaing')->name('submitedPaing');
Route::post('/getinit', 'paypal\paypalController@getinit')->name('getinit');
Route::post('/submitedSale', 'paypal\paypalController@submitedSale')->name('submitedSale');
Route::post('/TestAuth', 'paypal\paypalController@TestAuth')->name('TestAuth');

Route::post('/GetNameOfImage', 'ProductController@GetNameOfImage')->name('GetNameOfImage');


Route::post('/AddCoupon', 'CouponController@store')->name('AddCoupon');
Route::put('/UpdateCoupon/{id}', 'CouponController@update')->name('UpdateCoupon');
Route::delete('/DeleteCoupon/{id}', 'CouponController@destroy')->name('DeleteCoupon');
Route::get('/GetCoupon', 'CouponController@index')->name('GetCoupon');

Route::post('/MyPendingOrderList', 'ProductController@MyPendingOrderList')->name('MyPendingOrderList');
Route::post('/MyHistoryOrderList', 'ProductController@MyHistoryOrderList')->name('MyHistoryOrderList');
Route::post('/GetOrderDetails', 'ProductController@GetOrderDetails')->name('GetOrderDetails');
Route::post('/MyAllOrderList/{index}/{size}', 'ProductController@MyAllOrderList')->name('MyAllOrderList');
Route::post('/ChangeOrderStatus', 'ProductController@ChangeOrderStatus')->name('ChangeOrderStatus');


/////////////////////////////////////////////
//user coupon
Route::post('/UserAddCoupon', 'UserCouponController@store')->name('UserAddCoupon');
Route::put('/UserUpdateCoupon/{id}', 'UserCouponController@update')->name('UserUpdateCoupon');
Route::delete('/UserDeleteCoupon/{id}', 'UserCouponController@destroy')->name('UserDeleteCoupon');
Route::get('/UserGetCoupon', 'UserCouponController@index')->name('UserGetCoupon');


Route::post('/save-token', [App\Http\Controllers\HomeController::class, 'saveToken'])->name('api-save-token');



Route::resource('CustomProduct', 'c\CproductsController');
