<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

//Route::get('{locale}/welcome', function ($locale) {
//    if (! in_array($locale, ['en', 'es', 'fr'])) {
//        abort(400);
//    }
//
//    App::setLocale($locale);
//
//    //
//});
//
//
//Route::get('/', function () {
//    return view('welcome');
//});

//Route::group(['middleware' => ['web']], function () {


// email
Route::get('/email1',       'View\emailController@email1')->name('email1');
Route::get('/email2',       'View\emailController@email2')->name('email2');
Route::get('/email3',       'View\emailController@email3')->name('email3');
Route::get('/sendEmail',    'View\emailController@sendEmail')->name('sendEmail');
Route::get('/OrderEmail',    'View\emailController@OrderEmail')->name('OrderEmail');


Route::middleware(['web', 'Localization'])->group(function () {

    // sign in
    Route::get('/CustomerLoginRegister', 'View\mainController@CustomerLoginRegister')->name('CustomerLoginRegister');
    //Route::post('/signInWithGoogle', 'View\emailController@signInWithGoogle')->name('signInWithGoogle');
    Route::get('/', 'View\mainController@welcome')->name('');
    Route::get('/products/{id}', 'ProductController@index_products')->name('index_products');

    // Route::get('/contact', function () {
    //     return view('Welcome.views.shop.contacts');
    // });
    Route::view('/contact', 'Welcome.views.shop.contacts');


    // Route::get('/about', function () {
    //     return view('Welcome.views.shop.about');
    // });
    Route::view('/about', 'Welcome.views.shop.about');


    Route::get('/Cproduct/{id}', 'CproductController@index')->name('Cproduct');
    Route::get('/CproductDetails/{id}', 'CproductController@CproductDetails');

    // Route::get('/Deletion', function () {
    //     return view('Welcome.views.shop.deletetion');
    // });

    Route::view('/Deletion', 'Welcome.views.shop.deletetion');

    // Route::get('/terms', function () {
    //     return view('Welcome.views.shop.terms');
    // });

    Route::view('/terms', 'Welcome.views.shop.terms');

    // Route::get('/return', function () {
    //     return view('Welcome.views.shop.return');
    // });

    Route::view('/return', 'Welcome.views.shop.return');
    // products
    Route::get('/shop',                 'View\mainController@shop')->name('shop');
    Route::get('/productDetails/{id}',  'View\mainController@productDetails')->name('productDetails');
    Route::post('/store',               'View\mainController@store')->name('store');

    Route::post('/changeLang', 'View\mainController@changeLang')->name('changeLang');

    Route::get('/PrivacyPolicy',        'View\mainController@PrivacyPolicy')->name('PrivacyPolicy');
    Route::get('/TermsConditions',      'View\mainController@TermsConditions')->name('TermsConditions');



    // cart
    Route::get('/cart', 'View\mainController@cart')->name('cart');
});

Route::middleware(['web', 'WebAuth', 'Localization'])->group(function () {








    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    // template
    Route::get('/corporateDemo',        'View\mainController@corporateDemo')->name('corporateDemo');
    Route::get('/agencyDemo',           'View\mainController@agencyDemo')->name('agencyDemo');
    Route::get('/shopProductList',      'View\mainController@shopProductList')->name('shopProductList');
    Route::get('/shopHome1',            'View\mainController@shopHome1')->name('shopHome1');
    Route::get('/shopHome2',            'View\mainController@shopHome2')->name('shopHome2');
    Route::get('/shopHome3',            'View\mainController@shopHome3')->name('shopHome3');
    Route::get('/shopHome4',            'View\mainController@shopHome4')->name('shopHome4');
    Route::get('/shopHome5',            'View\mainController@shopHome5')->name('shopHome5');
    Route::get('/shopHome6',            'View\mainController@shopHome6')->name('shopHome6');
    Route::get('/shopHome7',            'View\mainController@shopHome7')->name('shopHome7');
    Route::get('/shopHome8',            'View\mainController@shopHome8')->name('shopHome8');
    Route::get('/shopProductDetails',   'View\mainController@shopProductDetails')->name('shopProductDetails');
    Route::get('/AboutAs',              'View\mainController@AboutAs')->name('AboutAs');
    Route::get('/ContactAs',            'View\mainController@ContactAs')->name('ContactAs');
    Route::get('/GalleryPage',          'View\mainController@GalleryPage')->name('GalleryPage');
    Route::get('/Portofolio',           'View\mainController@Portofolio')->name('Portofolio');
    Route::get('/BlogList',             'View\mainController@BlogList')->name('BlogList');
    Route::get('/ProductComparison',    'View\mainController@ProductComparison')->name('ProductComparison');
    Route::get('/CheckoutComplete',     'View\mainController@CheckoutComplete')->name('CheckoutComplete');
    Route::get('/ShopComponents1',      'View\mainController@ShopComponents1')->name('ShopComponents1');
    Route::get('/ShopComponents7',      'View\mainController@ShopComponents7')->name('ShopComponents7');
    Route::get('/WishList',             'View\mainController@WishList')->name('WishList');

    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////



    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    // user interface



    // my profile
    Route::get('/CustomerDashboard',    'View\mainController@CustomerDashboard')->name('CustomerDashboard');
    Route::get('/editprofile',          'View\mainController@editprofile')->name('editprofile');
    Route::get('/changePassword',       'View\mainController@changePassword')->name('changePassword');
    Route::get('/deletion',       'View\mainController@deletion')->name('deletion');





    // order
    Route::get('/order',                        'View\mainController@order')->name('order');
    Route::get('/orderHistory',                 'View\mainController@orderHistory')->name('orderHistory');
    Route::get('/orderDetails/{orderDetails}',  'View\mainController@orderDetails')->name('orderDetails');
    Route::get('/Checkout',                     'View\mainController@Checkout')->name('Checkout');
    Route::get('/OrderHistory',                 'View\mainController@OrderHistory')->name('OrderHistory');
    Route::get('/PendingOrder',                 'View\mainController@PendingOrder')->name('PendingOrder');

    Route::get('/wallet',                        'View\mainController@wallet')->name('wallet');



    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////



    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    // admin panel

    // admin
    Route::get('/admin', 'View\adminController@admin')->name('admin');

    // users
    Route::get('/admin/users', 'View\adminController@users')->name('admin_users');

    // types
    Route::get('/admin/types/list', 'View\adminController@types_list')->name('admin_types_list');

    Route::get('/admin/Ctypes/list', 'View\adminController@c_types_list')->name('c_admin_types_list');

    Route::get('/admin/coupon/list', 'View\adminController@coupon')->name('admin_coupon_list');
    Route::get('/admin/notification/list', 'View\adminController@notification')->name('notification');


    // Attributes
    Route::get('/admin/attributes/list', 'View\adminController@attributes_list')->name('admin_attributes_list');

    // Role Management
    Route::get('/admin/permissions/list',           'View\adminController@permissions_list')->name('admin_permissions_list');
    Route::get('/admin/user_roles_list/{id}',       'View\adminController@user_roles_list')->name('admin_user_roles_list');
    Route::get('/admin/roles/list',                 'View\adminController@roles_list')->name('admin_roles_list');
    Route::get('/admin/role_permissions_list/{id}', 'View\adminController@role_permissions_list')->name('admin_role_permissions_list');

    // orders
    Route::get('/admin/orders/list',        'View\adminController@orders_list')->name('admin_orders_list');
    Route::get('/admin/order/{orderId}',    'View\adminController@orderDetails')->name('admin_orderDetails');

    // products
    Route::get('/admin/products/add',       'View\adminController@products_add')->name('admin_products_add');
    Route::get('/admin/products/edit/{id}', 'View\adminController@products_edit')->name('admin_products_edit');
    Route::get('/admin/products/list',      'View\adminController@products_list')->name('admin_products_list');
    Route::get('/admin/products/accounting',      'View\adminController@accounting')->name('admin_products_accounting');
    // products_c
    Route::get('/admin/Cproducts/add',       'View\adminController@Cproducts_add')->name('c_admin_products_add');
    Route::get('/admin/Cproducts/edit/{id}', 'View\adminController@Cproducts_edit')->name('c_admin_products_edit');
    Route::get('/admin/Cproducts/list',      'View\adminController@Cproducts_list')->name('c_admin_products_list');

    // config
    Route::get('/admin/config',             'View\adminController@configList')->name('admin_configList');
    Route::get('/admin/config/header/add',  'View\adminController@config_header_add')->name('config_header_add');
    Route::get('/admin/config/header/list', 'View\adminController@config_header_list')->name('config_header_list');

    // Demo
    Route::get('/adminDemo', 'View\adminController@adminDemo')->name('adminDemo');


    Route::get('/admin/categories/list', 'View\adminController@catecories_list')->name('admin_catecories_list');
    Route::get('/admin/categories/edit/{id}', 'View\adminController@categories_edit')->name('admin_categories_edit');
    Route::post('/admin/categories/edit/{id}', 'View\adminController@categories_update')->name('admin_categories_update');
    Route::get('/admin/categories/add', 'View\adminController@categories_add')->name('admin_categories_add');
    Route::post('/admin/categories/add', 'View\adminController@categories_add_post')->name('admin_categories_add_post');

    //Ccategories
    Route::get('/admin/Ccategories/list', 'View\CcategoreiesController@catecories_list')->name('c_admin_catecories_list');
    Route::get('/admin/Ccategories/edit/{id}', 'View\CcategoreiesController@categories_edit')->name('c_admin_categories_edit');
    Route::post('/admin/Ccategories/edit/{id}', 'View\CcategoreiesController@categories_update')->name('c_admin_categories_update');
    Route::get('/admin/Ccategories/add', 'View\CcategoreiesController@categories_add')->name('c_admin_categories_add');
    Route::post('/admin/Ccategories/add', 'View\CcategoreiesController@categories_add_post')->name('c_admin_categories_add_post');


    //upload Images For Products
    Route::post('ajaxImageUpload', 'View\adminController@ajaxImageUploadPost')->name('ajaxImageUpload');
    Route::post('ajaxImageGet', 'View\adminController@ajaxImageGet')->name('ajaxImageGet');
    Route::post('ajaxImageDelete', 'View\adminController@ajaxImageDelete')->name('ajaxImageDelete');
});





// Route::get('/test-subhe', function () {
//     return view('subhe');
// });


Route::post('/save-token', [App\Http\Controllers\HomeController::class, 'saveToken'])->name('save-token');
Route::post('/changeprices', [App\Http\Controllers\HomeController::class, 'changeprices'])->name('changeprices');
Route::post('/send-notification', [App\Http\Controllers\HomeController::class, 'sendNotification'])->name('send.notification');

//csv
Route::get('/CsvGetter', [App\Http\Controllers\csvcontroller::class, 'exportCsv'])->name('exportCsv');
// Route::get('/CsvGettertest', [App\Http\Controllers\csvcontroller::class, 'exportCsvtest'])->name('exportCsvtest');


//currency
Route::view('changeCurrency/{currency}', function (\Illuminate\Http\Request $request, $currency) {
    if ($currency == 'auto') {
        $currency_json = (unserialize(file_get_contents('http://www.geoplugin.net/php.gp?ip=' . $request->ip())));
        if ($currency_json['geoplugin_currencyCode']) {
            $currencyCode = $currency_json['geoplugin_currencyCode'];
            session()->put('currency', $currencyCode);
        }
        return back();
    } else {
        session()->put('currency', $currency);

        return back();
    }
})->name('change_currency');
