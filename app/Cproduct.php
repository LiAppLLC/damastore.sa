<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Cproduct extends Model
{
    protected $guarded = [];
    protected $fillable = [
        'name',
        'description',
        'ar_name',
        'ar_description',
        'price',
        'is_price',
        'short_description',
        'ar_short_description',
        'supplier',
        'supplier',
        'product_code',
        'shipping_costs',
        'category_id',
        'type_id',
        'days',
        'image',
        'available',
    ];

    public function category()
    {
        return $this->belongsTo(Ccategory::class);
    }
    public function type()
    {
        return $this->belongsTo(Ctype::class);
    }
    public function images()
    {
        return $this->hasMany(CimagesGallery::class,'product_id');
    }
}
