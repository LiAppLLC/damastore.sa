<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class OrderStates extends Model
{
    protected $guarded = [];
    protected $table="orderstates";
}
