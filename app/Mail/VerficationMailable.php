<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Support\Facades\Mail;
use App\Http\config;

class VerficationMailable extends Mailable
{
    use Queueable, SerializesModels;
    public $name;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($name)
    {
        $this->name = $name;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->view('email.verficationMail');
    }

    public static function sendEmail($email, $fullName, $verificationcode){

        $subject = config::$email_title;


        $data = array(
            'name' => $fullName ,
            'verificationCode' => $verificationcode
        );

        Mail::send('email.name2', $data, function($message) use ($email, $subject) {
            $message->to($email)->subject($subject);
        });

    }

    public static function sendOrderDetailsByEmail($email, $fullName, $Data){

        $subject = 'Your Order Details';


        $data = array(
            'name' => $fullName ,
            'data' => $Data
        );
        Mail::send('email.name3', $data, function($message) use ($email, $subject) {
            $message->from('info@damastore.sa', 'DAMASTORE');
            $message->to($email);
            $message->subject($subject);
        });
        $email2="hmed2sbhe@gmail.com";
        Mail::send('email.name3', $data, function($message) use ( $email2, $subject) {
            $message->from('info@damastore.sa', 'DAMASTORE');
            $message->to($email2);
            $message->subject($subject);
        });

        // $email3="A.hassoun@li-app.com";
        // Mail::send('email.name3', $data, function($message) use ( $email3, $subject) {
        //     $message->from('info@damastore.sa', 'DAMASTORE');
        //     $message->to($email3);
        //     $message->subject($subject);
        // });
        $email4="Ammarov010@gmail.com";
        Mail::send('email.name3', $data, function($message) use ( $email4, $subject) {
            $message->from('info@damastore.sa', 'DAMASTORE');
            $message->to($email4);
            $message->subject($subject);
        });

    }
}
