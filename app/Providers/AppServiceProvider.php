<?php

namespace App\Providers;

use App\Category;
use App\Product;
use App\Type;
use App\Http\Resources\CategoryAr;
use App\Http\Resources\CategoryEn;
use App\Http\Resources\TypeAr;
use App\Http\Resources\TypeEn;
use Illuminate\Support\Facades\View;
use Illuminate\Support\ServiceProvider;
use Illuminate\Support\Facades\Schema;
use Illuminate\Support\Facades\Session;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        if (!session('lang')) {

            Session::put('lang', 'en');
        };
        Schema::defaultStringLength(191);
        $lastproduct = Product::offset(0)->limit(2)->orderBy('created_at', 'DESC')->get();
        // if(session('lang')=='ar'){
        //     $CategoryForSearch=CategoryAr::collection(Category::all());
        //     $TypeForSearch=TypeAr::collection(Type::all());
        // }
        // if(session('lang')=='en'){
        //     $CategoryForSearch=CategoryEn::collection(Category::all());
        //     $TypeForSearch=TypeEn::collection(Type::all());
        // }
        View::composer('*', function ($view) use ($lastproduct) {
            $view->with(
                ['lastproduct' => $lastproduct ]
            );
        });
    }
}
