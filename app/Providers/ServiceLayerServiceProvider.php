<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use App\Http\Services\CompanyService;
use App\Http\Services\FileService;
use App\Http\Services\UserService;
use App\Http\Services\PaymentService;
use App\Http\Services\CurrencyService;
use App\Http\Services\CategoryService;
use App\Http\Services\RejectedReasonService;

class ServiceLayerServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind(AuthService::class, function () {
        return new AuthService();
    });
        $this->app->bind(FileService::class, function () {
            return new FileService();
    });

    $this->app->bind(UserService::class, function () {
        return new UserService();
});

$this->app->bind(CompanyService::class, function () {
    return new CompanyService();
});

$this->app->bind(CategoryService::class, function () {
    return new CategoryService();
});

$this->app->bind(RejectedReasonService::class, function () {
    return new RejectedReasonService();
});




    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }
}
