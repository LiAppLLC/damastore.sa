<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Ctype extends Model
{
    public function product()
    {
        return $this->hasMany(Cproduct::class);
    }
}
