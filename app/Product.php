<?php

namespace App;

use App\traits\ChangingPrice;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\Http\Controllers\ExchangeRate;
use App\File as GetFile;

class Product extends Model
{
    use ChangingPrice;
    //    use SoftDeletes;
    //    protected $dates = ['deleted_at'];
    protected $guarded = [];
    protected $fillable = ['name', 'description', 'price', 'display_on_home', 'display_order', 'published', 'image_id'];

    public function Image()
    {
        return $this->belongsTo('App\File', 'image_id');
    }
    public function getImageUrl()
    {
        return File::where('target_id', $this->id)->where('target_name', 'PRODUCTS')->first()->path;
    }

    public function Tags()
    {
        return $this->belongsToMany('App\Tag');
    }

    public function categories()
    {
        return $this->belongsToMany('App\Category');
    }

    public function Company()
    {
        return $this->belongsTo('App\Company');
    }
    public function category()
    {
        return $this->belongsTo('App\Category', 'category_id');
    }
    public function type()
    {
        return $this->belongsTo('App\Type', 'type_id');
    }
    public function promotion()
    {
        return $this->belongsTo('App\Promotion', 'promotion_id');
    }


    public function getCategorieById($langs)
    {
        $id = (int)$this->category_id;
        if ($langs == 'en') {
            return \App\Http\Resources\CategoryEn::collection(Category::all()->where('id', $id));
        }
        if ($langs == 'ar') {
            return \App\Http\Resources\CategoryAr::collection(Category::all()->where('id', $id));
        }
    }

    public function getTypeById($langs)
    {
        $id = (int)$this->type_id;
        if ($langs == 'en') {
            return \App\Http\Resources\TypeEn::collection(Type::all()->where('id', $id));
        }
        if ($langs == 'ar') {
            return \App\Http\Resources\TypeAr::collection(Type::all()->where('id', $id));
        }
    }

    public function name_of_attribute($langs)
    {
        if ($langs == 'en') {
            $id = (int)$this->attribute_id;
            return attributes::where('id', $id)->first()->name;
        }
        if ($langs == 'ar') {
            $id = (int)$this->attribute_id;
            return attributes::where('id', $id)->first()->ar_name;
        }
    }
    public function getAttributsById($langs)
    {
        $id = (int)$this->id;
        if ($langs == 'en') {
            $data = \App\Http\Resources\AttributeValuesEn::collection(attribute_values::all()->where('product_id', $id));
            foreach ($data as $item) {
                $item->name = $item->name_of_attribute($langs);
            }
            return $data;
        }
        if ($langs == 'ar') {
            $data = \App\Http\Resources\AttributeValuesAR::collection(attribute_values::all()->where('product_id', $id));
            foreach ($data as $item) {
                $item->name = $item->name_of_attribute($langs);
            }
            return $data;
        }
    }
    public function getMainImagebyId()
    {
        return GetFile::where('target_id', $this->id)->where('target_name', '=', 'PRODUCTS')->get();
    }
    public function getImagesbyId()
    {
        return GetFile::where('target_id', $this->id)->where('target_name', '=', 'PRODUCTS_IMAGES')->get();
    }
}
