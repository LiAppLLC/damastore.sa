<?php


namespace App\traits;

use App\Http\Controllers\ExchangeRate;

trait ChangingPrice
{
    public function changePrice($products, $ip)
    {
        if (!session('currency') || session('currency') == 'auto') {
            $currency_json = (unserialize(file_get_contents('http://www.geoplugin.net/php.gp?ip=' . $ip)));
            if ($currency_json['geoplugin_currencyCode']) {
                // $currencyCode = $currency_json['geoplugin_currencyCode'];
                $currencyCode = "SAR";
                session()->put('currency', $currencyCode);
                $converse_rate = ExchangeRate::getConverseRate($currencyCode);
                foreach ($products as $item) {
                    $item->price = ceil($item->price * $converse_rate) . " " . $currencyCode;
                    $item->price_sale = ceil($item->price_sale * $converse_rate) . " " . $currencyCode;
                    $item->currency = $currencyCode;
                }
            } else {
                $converse_rate = ExchangeRate::getConverseRate('SAR');
                session()->put('currency', 'SAR');
                foreach ($products as $item) {
                    $item->price = ceil($item->price * $converse_rate) . " " . session('currency');
                    $item->price_sale = ceil($item->price_sale * $converse_rate) . " " . session('currency');
                    $item->currency = session('currency');
                }
            }
        } else {
            if (session('currency') == 'SAR') {
                $converse_rate = 1;
            } else {
                $converse_rate = ExchangeRate::getConverseRate(session('currency'));
            }
            foreach ($products as $item) {
                $item->price = ceil($item->price * $converse_rate) . " " . session('currency');
                $item->price_sale = ceil($item->price_sale * $converse_rate) . " " . session('currency');
                $item->currency = session('currency');
            }
        }
    }

    public function changePriceCart($products, $ip)
    {
        if (!session('currency') || session('currency') == 'auto') {
            $currency_json = (unserialize(file_get_contents('http://www.geoplugin.net/php.gp?ip=' . $ip)));
            if ($currency_json['geoplugin_currencyCode']) {
                // $currencyCode = $currency_json['geoplugin_currencyCode'];
                $currencyCode = "SAR";
                session()->put('currency', $currencyCode);
                $converse_rate = ExchangeRate::getConverseRate($currencyCode);
                foreach ($products as $item) {
                    $item->price = ceil($item->price * $converse_rate);
                    $item->price_sale = ceil($item->price_sale * $converse_rate);
                    $item->currency = $currencyCode;
                }
            } else {
                $converse_rate = ExchangeRate::getConverseRate('SAR');
                session()->put('currency', 'SAR');
                foreach ($products as $item) {
                    $item->price = ceil($item->price * $converse_rate);
                    $item->price_sale = ceil($item->price_sale * $converse_rate);
                    $item->currency = session('currency');
                }
            }
        } else {
            if (session('currency') == 'SAR') {
                $converse_rate = 1;
            } else {
                $converse_rate = ExchangeRate::getConverseRate(session('currency'));
            }
            foreach ($products as $item) {
                $item->price = ceil($item->price * $converse_rate);
                $item->price_sale = ceil($item->price_sale * $converse_rate);
                $item->currency = session('currency');
            }
        }
    }

    public function changePriceSingle($products, $ip)
    {
        if (!session('currency') || session('currency') == 'auto') {
            $currency_json = (unserialize(file_get_contents('http://www.geoplugin.net/php.gp?ip=' . $ip)));
            if ($currency_json['geoplugin_currencyCode']) {
                // $currencyCode = $currency_json['geoplugin_currencyCode'];
                $currencyCode = "SAR";
                session()->put('currency', $currencyCode);
                $converse_rate = ExchangeRate::getConverseRate($currencyCode);
                if ($products) {
                    $products->price = ceil($products->price * $converse_rate);
                    $products->price_sale = ceil($products->price_sale * $converse_rate);
                    $products->currency = $currencyCode;
                }
            } else {
                $converse_rate = ExchangeRate::getConverseRate('SAR');
                session()->put('currency', 'SAR');
                if ($products) {
                    $products->price = ceil($products->price * $converse_rate);
                    $products->price_sale = ceil($products->price_sale * $converse_rate);
                    $products->currency = session('currency');
                }
            }
        } else {
            if (session('currency') == 'SAR') {
                $converse_rate = 1;
            } else {
                $converse_rate = ExchangeRate::getConverseRate(session('currency'));
            }
            if ($products) {
                $products->price = ceil($products->price * $converse_rate);
                $products->price_sale = ceil($products->price_sale * $converse_rate);
                $products->currency = session('currency');
            }
        }
    }


    //best practis
    public function getCurrencyForProductApi($HeaderCurrency, $Session, $Ip, $product)
    {
        if ($HeaderCurrency) {
            if ($HeaderCurrency == 'SAR') {
                $converse_rate = 1;
            } else {
                $converse_rate = ExchangeRate::getConverseRate($HeaderCurrency);
            }
            if ($product) {
                foreach ($product as $item) {
                    $item->price = ceil($item->price * $converse_rate);
                    $item->price_sale = ceil($item->price_sale * $converse_rate);
                    $item->currency = $HeaderCurrency;
                }
            }
        } elseif (!$HeaderCurrency) {
            $currency_json = (unserialize(file_get_contents('http://www.geoplugin.net/php.gp?ip=' . $Ip)));
            if ($currency_json['geoplugin_currencyCode']) {
                // $currencyCode = $currency_json['geoplugin_currencyCode'];
                $currencyCode = "SAR";
                $converse_rate = ExchangeRate::getConverseRate($currencyCode);
                foreach ($product as $item) {
                    $item->price = ceil($item->price * $converse_rate);
                    $item->price_sale = ceil($item->price_sale * $converse_rate);
                    $item->currency = $currencyCode;
                }
            } else {
                $currencyCode = 'SAR';
                $converse_rate = 1;
                foreach ($product as $item) {
                    $item->price = ceil($item->price * $converse_rate);
                    $item->price_sale = ceil($item->price_sale * $converse_rate);
                    $item->currency = $currencyCode;
                }
            }
        } elseif ($Session && !$HeaderCurrency) {
        } elseif (!$Session && !$HeaderCurrency) {
        }
    }
}
