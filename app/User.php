<?php

namespace App;
use DB;
use Illuminate\Http\Request;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Tymon\JWTAuth\Contracts\JWTSubject;
use Illuminate\Support\Facades\Hash;
use Carbon\Carbon;
use App\UsersRole;
use App\Role;


class User extends Authenticatable implements JWTSubject
{
    use Notifiable;



    public static $tableName = 'users';
    public static $tb_id = 'id';
    public static $tb_email = 'email';
    public static $tb_username = 'username';
    public static $tb_firstName = 'firstName';
    public static $tb_lastName = 'lastName';

    public static $tb_bornDate = 'bornDate';
    public static $tb_phone = 'phone';
    public static $tb_emailVerifiedAt = 'emailVerifiedAt';
    public static $tb_password = 'password';
    public static $tb_reputation = 'reputation';
    public static $tb_verify = 'verify';
    public static $tb_blocked = 'blocked';
    public static $tb_lastActivity = 'lastActivity';
    public static $tb_lastLoggedIn = 'lastLoggedIn';
    public static $tb_token = 'token';

    public static $tb_google = 'google';
    public static $tb_facebook = 'facebook';

    public static $tb_created_at = 'created_at';
    public static $tb_updated_at = 'updated_at';





    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'id','username', 'email','firstName','lastName', 'bornDate','phone','emailVerifiedAt','image','password','verify','blocked','lastActivity'
        ,'lastLoggedIn'
        ,'device_token'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    // Rest omitted for brevity

    /**
     * Get the identifier that will be stored in the subject claim of the JWT.
     *
     * @return mixed
     */
    public function getJWTIdentifier()
    {
        return $this->getKey();
    }

    /**
     * Return a key value array, containing any custom claims to be added to the JWT.
     *
     * @return array
     */
    public function getJWTCustomClaims()
    {
        return [];
    }


    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];




    public function Files()
    {
        return $this->hasMany('App\File');
    }

    public function Companies()
    {
        return $this->hasMany('App\Company');
    }
    public function Properties()
    {
        return $this->hasMany('App\Property');
    }
    public function Tags()
    {
        return $this->hasMany('App\Tag');
    }
    public function Roles()
    {
        return $this->hasMany('App\Role');
    }


    public static function RegOrLogin( $request)
    {


        $user = DB::table(self::$tableName)->where(self::$tb_email, '=', $request->email)->first();

        if(!$user){
            // new user // register
            DB::table(self::$tableName)
                ->insert([
                    self::$tableName.'.'.self::$tb_email=>$request->email,
                    self::$tableName.'.'.self::$tb_firstName=>$request->Fname,
                    self::$tableName.'.'.self::$tb_lastName=>$request->Lname,
                    self::$tableName.'.'.self::$tb_username=>$request->name,
                    self::$tableName.'.'.self::$tb_verify =>1,
                    self::$tableName.'.'.self::$tb_google =>1,
                    self::$tableName.'.'.self::$tb_password =>Hash::make('728080330284-30rvpujtk8qfef37pggpcd1iv3il4k34.apps.googleusercontent.com'),
                    self::$tableName.'.'.self::$tb_created_at =>Carbon::now(),
                    self::$tableName.'.'.self::$tb_updated_at =>Carbon::now(),
                ]);
            $user = DB::table(self::$tableName)->where(self::$tb_email, '=', $request->email)->first();

            $userRole = new UsersRole();
            $userRole->user_id = $user->id;
            $userRole->role_id = Role::where('name','Registered')->first()->id;
            $userRole->save();

            return $request->email;
        }else{
            // user exist
            return $user->email;
        }


    }

}
