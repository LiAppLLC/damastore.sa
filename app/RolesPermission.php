<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class RolesPermission extends Model
{
    protected $guarded = [];

    public function Roles()
    {
        return $this->hasMany('App\Role');
    }
    public function Permissions()
    {
        return $this->hasMany('App\Permission');
    }
}
