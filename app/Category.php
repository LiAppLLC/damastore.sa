<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Category extends Model
{
    protected $guarded = [];



    public function Image()
    {
        return $this->belongsTo('App\File', 'image_id');
    }
  public function Products()
  {
      return $this->belongsToMany('App\Product');
  }
    public function Companies()
    {
        return $this->belongsToMany('App\Company');
    }
    public function getalltypes($langs){
        $id = (int)$this->id;
        if($langs=='en'){
            return \App\Http\Resources\TypeEn::collection(Type::all()->where('category', $id));
         }
         if($langs=='ar'){
            return \App\Http\Resources\TypeAr::collection(Type::all()->where('category', $id));
         }
    }


}
