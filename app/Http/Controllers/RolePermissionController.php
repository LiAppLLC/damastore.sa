<?php

namespace App\Http\Controllers;

use App\Http\Services\RolePermissionService;
use App\RolesPermission;
use App\Permission;
use App\Http\Utility;
use Illuminate\Http\Request;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\Validator;

class RolePermissionController extends Controller
{

    protected $rolepermissionservice;
    public function __construct(RolePermissionService $rolePermissionService)
    {
        $this->rolepermissionservice = $rolePermissionService;
        $this->middleware('ApiAuth:api');
    }

    public function addMulti(Request $request)
    {

        $temp = explode(',', $request->rolePermissions);
        $arr = [];
        foreach ($temp as $item){
            array_push($arr, ['permission_id' => (int)$item]);
        }
        $request->rolePermissions = $arr;



        $data = $request->rolePermissions;
        $role_id = $request->role_id;

        $rolePermission=$this->rolepermissionservice->addMulti($role_id,$data);

        return Utility::ToApi("Role permission added successfully",true,  $rolePermission,"Ok",201);

    }

    public function getByRole(Request $request,$pageindex,$pageSize)
    {
        $validator = Validator::make($request->all(), [
            'role_id' =>['required']
        ]);

        if ($validator->fails()) {

            return   Utility::ToApi("missing fields",false, ["validator"=>$validator->messages()->first()],"BadRequest",400);

        }

        $data=$request->only('role_id');
        list($rolePermission,$total)=$this->rolepermissionservice->getByRole($data['role_id'],$pageindex,$pageSize);
        if($rolePermission!=null)
        return Utility::ToPageApi("Role permission return successfully",true,$rolePermission,$total,"Ok",200);
    }

    public function deleteMulti(Request $request)
    {
        $temp = explode(',', $request->rolePermissions);
        $arr = [];
        foreach ($temp as $item){
            array_push($arr, ['permission_id' => (int)$item]);
        }
        $request->rolePermissions = $arr;

        $data = $request->rolePermissions;
        $role_id = $request->role_id;

       $rolePermission= $this->rolepermissionservice->deleteMulti($role_id,$data);
       return Utility::ToApi("Role permission deleted successfully",true,  $rolePermission,"Ok",200);
    }

}
