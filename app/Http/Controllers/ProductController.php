<?php

namespace App\Http\Controllers;

use App\Category;
use App\File as GetFile;
use App\Order;
use App\user_coupon;
use App\Payment;
use App\ordered_product;
use App\traits\ChangingPrice;
use App\Type;
use App\Company;
use App\Exceptions\CustomException;
use App\Http\Services\ProductService;
use App\Http\Services\ForbiddenService;
use App\Http\Services\SearchHistoryService;
use App\Product;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;
use App\Http\Utility;
use Illuminate\Support\Arr;
use App\Http\Services\FileService;
use App\File;
use JWTAuth;
use App\OrderStates;
use Session;
use App\Http\Services\AuthService;
use App\attributes;
use App\attribute_values;
use App\Ccategory;
use App\Cproduct;
use App\Ctype as AppCtype;
use Illuminate\Support\Facades\File as FilesSubhe;
use Illuminate\Support\Facades\Storage;
use App\Http\Controllers\Controller;
use App\Providers\RouteServiceProvider;
use Illuminate\Foundation\Auth\ConfirmsPasswords;
use Illuminate\Support\Facades\Redirect;
use DB;
use Carbon\Carbon;
use Illuminate\Support\Facades\App as FacadesApp;
use Illuminate\Support\Facades\Lang;
use Illuminate\Support\Facades\App;
use App\Http\Resources\AttributesAr;
use App\Http\Resources\AttributesEn;
use App\Http\Resources\ProductAr;
use App\Http\Resources\ProductEn;
use App\Http\Resources\AttributeValuesAR;
use App\Http\Resources\AttributeValuesEn;
use Symfony\Polyfill\Ctype\Ctype;

class ProductController extends Controller
{

    protected $productservice;
    protected $forbiddenservice;
    protected $fileService;

    public function __construct(
        ProductService $productService,
        ForbiddenService $forbiddenService,
        FileService $fileService
    ) {
        $this->productservice = $productService;
        $this->forbiddenservice = $forbiddenService;
        $this->fileService = $fileService;
        $this->middleware('ApiAuth:api', ['except' => ['index', 'index_products', 'show', 'getByFilter', 'getByCompany', 'getByCategory', 'HomePageProductList', 'ProductList', 'TypesList', 'category', 'Type', 'get_products_by_id', 'related_product', 'ProductCartList', 'laravel_session']]);
    }

    use ChangingPrice;

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

        $p = Product::all();
        $product = $this->productservice->getPropertiesTags($p);
        return Utility::ToApi("Product returned successfully", true, $p, "Ok", 200);
    }

    public function getAllProducts($index, $size)
    {

        list($product, $total) = $this->productservice->getAll((int)$index, (int)$size);
        $product = $this->productservice->getPropertiesTags($product);

        return Utility::ToPageApi("Product returned successfully", true, $product, $total, "Ok", 200);
    }

    public function addToCompany(Request $request, $company)
    {
        $user = Auth()->user();
        $company1 = Company::find($company);
        if ($company1 == null) {
            throw new CustomException("company not found");
        }
        if ($user->id != $company1->user_id)
            throw new CustomException("company not allowed");
        if ($company1->companyStatus_id != 2)
            throw new CustomException("company not approved");

        $validator = Validator::make($request->all(), [
            'name' => ['required', 'string'],
            'description' => ['required', 'string'],
            'price' => ['required', 'numeric'],
            'published' => ['boolean'],

        ]);

        if ($validator->fails()) {

            return Utility::ToApi("missing fields", false, ["validator" => $validator->messages()->first()], "BadRequest", 400);
        }

        $data = $request->only('name', 'description', 'image_id', 'display_on_home', 'display_order', 'price', 'published', 'tag_id', 'property_id', 'category_id');

        $this->forbiddenservice->check($data['name'], "add product name", $user->id, $company1->id);
        $this->forbiddenservice->check($data['description'], "add product description", $user->id, $company1->id);

        if (!Arr::exists($data, 'description'))
            $data['description'] = null;
        if (!Arr::exists($data, 'image_id'))
            $data['image_id'] = null;
        if (!Arr::exists($data, 'display_on_home'))
            $data['display_on_home'] = false;
        if (!Arr::exists($data, 'published'))
            $data['published'] = true;
        if (!Arr::exists($data, 'display_order'))
            $data['display_order'] = -1;
        if (!Arr::exists($data, 'tag_id'))
            $data['tag_id'] = null;
        if (!Arr::exists($data, 'property_id'))
            $data['property_id'] = null;
        // dd($data);
        $product = $this->productservice->addToCompany($data, $company);
        return Utility::ToApi("Product added successfully", true, $product, "Ok", 201);
    }


    public function getByCompany($company)
    {

        $product = $this->productservice->getByCompany($company);
        return Utility::ToApi("Products for this company returned successfully", true, $product, "Ok", 200);
    }

    public function getByCategory($pageindex, $pageSize, $category)
    {

        list($product, $total) = $this->productservice->getByCategory($pageindex, $pageSize, $category);
        return Utility::ToPageApi("Products for this category returned successfully", true, $product, $total, "OK", 200);
    }

    public function getByFilter(Request $request, $index, $size)
    {
        list($product, $total) = $this->productservice->getByFilter($request, (int)$index, (int)$size);
        $product = $this->productservice->getPropertiesTags($product);

        $searchHistoryService = new SearchHistoryService();
        $searchHistoryService->add($request->only('name', 'category_id', 'tags'));

        if ($product != null)
            return Utility::ToPageApi("Products filtered successfully", true, $product, $total, "Ok", 200);
        else
            return Utility::ToApi("There's no products with this filter", true, null, "Ok", 200);
    }

    public function getAllReviews(Request $request, $pageIndex, $pageSize)
    {
        list($productreview, $total) = $this->productservice->getAllReviews($request, $pageIndex, $pageSize);
        return Utility::ToPageApi("Products filtered successfully", true, $productreview, $total, "Ok", 200);
    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {

        $product = $this->productservice->get($id);
        $this->productservice->view($id);
        $product = $this->productservice->getPropertiesTags([$product]);
        $product = $product[0];
        return Utility::ToApi("Product return successfully", true, $product, "OK", 200);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $p = Product::find($id);
        $user = Auth()->user();
        $company = new Company();
        $company = $company->where('user_id', $user->id);
        $company = $company->where('id', $p->company_id)->get();

        if ($company == null)
            return Utility::ToApi("You dont have access to this product", false, "BadRequest", "NO ACCESS", 401);


        $data = $request->only('name', 'description', 'image_id', 'display_on_home', 'display_order', 'published', 'price');

        if (Arr::exists($data, 'name'))
            $this->forbiddenservice->check($data['name'], "edit product name", $user->id, $company->first()->id);
        if (Arr::exists($data, 'description'))
            $this->forbiddenservice->check($data['description'], "edit product description", $user->id, $company->first()->id);

        $product = $this->productservice->update($id, $data);


        return Utility::ToApi("Product updated successfully", true, $product, "OK", 200);
    }

    public function editProductProperties(Request $request, $product_id)
    {

        $p = Product::find($product_id);
        $user = Auth()->user();
        $company = new Company();
        $company = $company->where([
            ['user_id', '=', $user->id],
            ['id', '=', $p->company_id]
        ])->first();

        if ($company == null)
            return Utility::ToApi("You dont have access to this company", false, "BadRequest", "NO ACCESS", 401);

        $data = $request->Tags;
        foreach ($data as $key => $item) {
            $this->forbiddenservice->check($data[$key]['tag_name'], "add tag name", $user->id, $company->id);
            $this->forbiddenservice->check($data[$key]['tag_description'], "add tag description", $user->id, $company->id);
        }

        $this->productservice->updateProductProperties($product_id, $request);

        $product = $this->productservice->get($product_id);
        $product = $this->productservice->getPropertiesTags([$product]);

        return Utility::ToApi("Product return successfully", true, $product, "OK", 200);
    }

    public function like(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'product_id' => ['required', 'string']
        ]);

        if ($validator->fails()) {
            return Utility::ToApi("missing fields", false, ["validator" => $validator->messages()->first()], "BadRequest", 400);
        }

        $data = $request->only('product_id');
        $productreview = $this->productservice->like($data['product_id']);

        return Utility::ToApi("user liked product", true, $productreview, "OK", 200);
    }

    public function dislike(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'product_id' => ['required', 'string']
        ]);

        if ($validator->fails()) {
            return Utility::ToApi("missing fields", false, ["validator" => $validator->messages()->first()], "BadRequest", 400);
        }

        $data = $request->only('product_id');
        $productreview = $this->productservice->dislike($data['product_id']);

        return Utility::ToApi("user disliked product", true, $productreview, "OK", 200);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $p = Product::find($id);
        $user = Auth()->user();
        if ($p->company_id != $user->company_id)
            return Utility::ToApi("You dont have access to this compnay", false, "BadRequest", "NO ACCESS", 401);
        $product = $this->productservice->delete($id);
        return Utility::ToApi("Product has been deleted", true, $product, "OK", 200);
    }

    public function softDelete($id)
    {
        $product = Product::find($id);
        if ($product == null)
            return Utility::ToApi("no product found", true, "BadRequest", "EMPTY", 200);

        $user = Auth()->user();
        $company = new Company();
        $company = $company->where('user_id', $user->id);
        $company = $company->where('id', $product->company_id)->get();

        if ($company == null)
            return Utility::ToApi("You dont have access to this compnay", false, "BadRequest", "NO ACCESS", 401);

        $product = $this->productservice->softdelete($id);
        return Utility::ToApi("Product has been deleted softly", true, $product, "OK", 200);
    }


    public function ProductAdd(Request $request)
    {
        // foreach (json_decode($response) as $area)
        // {
        //  print_r($area); // this is your area from json response
        // }

        $validator = Validator::make($request->all(), [
            'name' => 'required',
            'supplier' => 'required',
            'ar_name' => 'required',
            'description' => 'required',
            'ar_description' => 'required',
            'short_description' => 'required',
            'ar_short_description' => 'required',
            'price' => 'required',
            'price_sale' => 'required',
            'product_code' => 'required|unique:products,product_code',
            'shipping_costs' => 'required',
            'category' => 'required',
            'type' => 'required',
            'quantity' => 'required',
            'product_photo' => 'required',
        ]);

        if ($validator->fails()) {
            return response()->json(array('Validation Error.', $validator->errors()));
        }

        $product = new Product();

        $product->name = $request->name;
        $product->supplier = $request->supplier;
        $product->ar_name = $request->ar_name;
        $product->description = $request->description;
        $product->ar_description = $request->ar_description;
        $product->short_description = $request->short_description;
        $product->ar_short_description = $request->ar_short_description;
        $product->product_code = $request->product_code;
        $product->unit = $request->price;
        $product->shipping_costs = $request->shipping_costs;
        $product->quantity = $request->quantity;
        $product->price = $request->price * 2;
        $product->price_sale = $request->price_sale * 2;
        $product->user_id = $request->user_id;
        $product->type_id = $request->type;
        $product->category_id = $request->category;

        $product->save();
        $product_id = $product->id;
        $product_photo = $request->file('product_photo');

        $product->product_photo = $this->fileService->addImage($product_photo, $product['id'], 'PRODUCTS');
        $datas = json_decode($request->data);

        foreach ($datas as $data2) {
            $Attribute = new attribute_values();
            $Attribute->attribute_id = $data2->id;
            $Attribute->product_id = $product_id;
            $Attribute->value = $data2->value;
            $Attribute->ar_value = $data2->ar_value;
            $Attribute->image = 'null';
            $Attribute->attribute_value_key = $data2->id . ':' . $data2->value . ':' . $data2->ar_value;
            $Attribute->save();
        }

        //        dd($request->all(), $header);
        return Utility::ToApi("Product has been added successfuly", true, $product, "OK", 200);
    }


    public function TypeAdd(Request $request)
    {
        //        dd($request->all());

        $type = new Type();

        $type->name = $request->name;
        $type->category = $request->category;
        $type->ar_name = $request->ar_name;
        $type->description = $request->description;
        $type->ar_description = $request->ar_description;
        $type->created_at = Carbon::now();
        $type->updated_at = Carbon::now();

        $type->save();

        return Utility::ToApi("Product has been added successfuly", true, $type, "OK", 200);
    }
    public function CTypeAdd(Request $request)
    {
        //        dd($request->all());

        $type = new AppCtype();

        $type->name = $request->name;
        $type->category = $request->category;
        $type->ar_name = $request->ar_name;
        $type->description = $request->description;
        $type->ar_description = $request->ar_description;
        $type->created_at = Carbon::now();
        $type->updated_at = Carbon::now();
        $type->save();

        return Utility::ToApi("Ctype has been added successfuly", true, $type, "OK", 200);
    }

    public function ProductEdit(Request $request)
    {

        $validator = Validator::make($request->all(), [
            'name' => 'required',
            'supplier' => 'required',
            'ar_name' => 'required',
            'description' => 'required',
            'ar_description' => 'required',
            'short_description' => 'required',
            'ar_short_description' => 'required',
            'price' => 'required',
            'salesPrice' => 'required',
            'product_code' => 'required',
            'shipping_costs' => 'required',
            'category' => 'required',
            'type' => 'required',
            'quantity' => 'required',

        ]);

        if ($validator->fails()) {
            return response()->json(array('Validation Error.', $validator->errors()));
        }

        $product = Product::find($request->prod_id);

        if ($product) {
            $product->name = $request->name;
            $product->ar_name = $request->ar_name;
            $product->description = $request->description;
            $product->ar_description = $request->ar_description;
            $product->short_description = $request->short_description;
            $product->ar_short_description = $request->ar_short_description;
            $product->product_code = $request->product_code;
            $product->price = $request->price;
            $product->price_sale = $request->salesPrice;
            $product->quantity = $request->quantity;
            $product->shipping_costs = $request->shipping_costs;
            //            $product->unit ="null";
            $product->supplier = $request->supplier;
            $product->category_id = $request->category;
            $product->type_id = $request->type;
            $product->updated_at = Carbon::now();

            $product_photo = $request->file('product_photo');
            if ($product_photo) {
                $this->fileService->addImage($product_photo, $product['id'], 'PRODUCTS');
            }

            $product->save();
        }

        return Utility::ToApi("Product has been added successfuly", true, $product, "OK", 200);
    }

    public function TypeEdit(Request $request)
    {

        $type = Type::find($request->type_id);

        if ($type) {
            $type->name = $request->name;
            $type->ar_name = $request->ar_name;
            $type->description = $request->description;
            $type->ar_description = $request->ar_description;
            $type->updated_at = Carbon::now();

            $type->save();
        }

        return Utility::ToApi("Product has been added successfuly", true, $type, "OK", 200);
    }
    public function CTypeEdit(Request $request)
    {

        $type = AppCtype::find($request->type_id);

        if ($type) {
            $type->name = $request->name;
            $type->ar_name = $request->ar_name;
            $type->description = $request->description;
            $type->ar_description = $request->ar_description;
            $type->updated_at = Carbon::now();

            $type->save();
        }

        return Utility::ToApi("Ctype has been added successfuly", true, $type, "OK", 200);
    }

    public function ProductList(Request $request, $pageIndex, $pageSize)
    {


        //        "category" => "1"
        //      "type" => "1"
        //      "min-num" => null
        //      "max-num" => null
        //      "user_id" => "1"

        //$products = Product::skip($pageIndex * $pageSize)->take($pageSize)->orderBy('created_at')->get();
        if (empty($request->sale)) {
            $request->sale = true;
        }
        $query = DB::table('products');

        if ($request->category) {
            $query->where('category_id', '=', $request->category);
        }
        if ($request->type) {
            $query->where('type_id', '=', $request->type);
        }
        if ($request->min_num) {
            $query->where('price', '>', $request->min_num);
        }
        if ($request->max_num) {
            $query->where('price', '<', $request->max_num);
        }

        if ($request->sale == "true") {
            $query->where('price_sale', '>', 0);
        } else {
            $query->where('price_sale', '=', 0);
        }

        $products = $query->skip($pageIndex * $pageSize)->take($pageSize)->orderBy('created_at')->get();
        $count = $query->count();

        $minimumVal = 999999999;
        $maximumVal = 0;
        $this->changePrice($products, $request->ip());
        foreach ($products as $index => $item) {

            $type = Type::where('id', '=', $item->type_id)->first();
            if ($type) {
                $products[$index]->type = $type->name;
            }

            $category = Category::where('id', '=', $item->category_id)->first();
            if ($category) {
                $products[$index]->category = $category->name;
            }

            $images = File::where('target_name', '=', 'PRODUCTS')->where('target_id', '=', $item->id)->get();

            if (sizeof($images) > 0) {
                $products[$index]->image = $images[0];
            }

            if ($item->price > $maximumVal) {
                $maximumVal = $item->price;
            }

            if ($item->price < $minimumVal) {
                $minimumVal = $item->price;
            }
        }
        $all_products = Product::all()->count();
        $data = [
            'all_products' => $all_products,
            'data' => $products,
            'pageIndex' => $pageIndex,
            'totalCount' => $count,
            'maximumVal' => $maximumVal,
            'minimumVal' => $minimumVal
        ];

        return Utility::ToApi("Product has been listed successfuly", true, $data, "OK", 200);
    }

    public function HomePageProductList(Request $request)
    {

        //        "category" => "1"
        //      "type" => "1"
        //      "min-num" => null
        //      "max-num" => null
        //      "user_id" => "1"
        // =============================================================================================================
        // =============================================================================================================
        // most recent products

        $query = DB::table('products');

        if ($request->category) {
            $query->where('category_id', '=', $request->category);
        }
        if ($request->type) {
            $query->where('type_id', '=', $request->type);
        }
        if ($request->min_num) {
            $query->where('price', '>', $request->min_num);
        }
        if ($request->max_num) {
            $query->where('price', '<', $request->max_num);
        }

        if ($request->sale == "true") {
            $query->where('price_sale', '>', 0);
        } else {
            $query->where('price_sale', '=', 0);
        }

        $products = $query->skip(0)->take(8)->orderBy('created_at')->get();
        $count = $query->count();

        $minimumVal = 999999999;
        $maximumVal = 0;
        foreach ($products as $index => $item) {
            $type = Type::where('id', '=', $item->type_id)->first();
            if ($type) {
                if (session('lang') == 'ar') {
                    $products[$index]->type = $type->ar_name;
                } else {
                    $products[$index]->type = $type->name;
                }
            }

            $category = Category::where('id', '=', $item->category_id)->first();
            if ($category) {
                if (session('lang') == 'ar') {
                    $products[$index]->category = $category->ar_name;
                } else {
                    $products[$index]->category = $category->name;
                }
            }

            $images = File::where('target_name', '=', 'PRODUCTS')->where('target_id', '=', $item->id)->get();

            if (sizeof($images) > 0) {
                $products[$index]->image = $images[0];
            }

            if ($item->price > $maximumVal) {
                $maximumVal = $item->price;
            }

            if ($item->price < $minimumVal) {
                $minimumVal = $item->price;
            }
        }
        $mostrecentprod = [
            'data' => $products,
            'totalCount' => $count,
            'maximumVal' => $maximumVal,
            'minimumVal' => $minimumVal
        ];
        // =============================================================================================================
        // =============================================================================================================


        // =============================================================================================================
        // =============================================================================================================
        // latest offer

        $query = DB::table('products');

        $query->where('price_sale', '>', 0);

        $product = $query->orderBy('created_at')->first();
        $this->changePriceSingle($product, $request->ip());
        $type = Type::where('id', '=', $product->type_id)->first();
        if ($type) {
            if (session('lang') == 'ar') {
                $product->type = $type->ar_name;
            } else {
                $product->type = $type->name;
            }
        }

        $category = Category::where('id', '=', $product->category_id)->first();
        if ($category) {
            if (session('lang') == 'ar') {
                $products[$index]->category = $category->ar_name;
            } else {
                $products[$index]->category = $category->name;
            }
        }

        $images = File::where('target_name', '=', 'PRODUCTS')->where('target_id', '=', $product->id)->get();

        if (sizeof($images) > 0) {
            $product->image = $images[0];
        }


        $latestoffer = [
            'data' => $product
        ];

        // =============================================================================================================
        // =============================================================================================================


        // =============================================================================================================
        // =============================================================================================================
        // product list based on category

        $categories = Category::get();
        $allProductList = [];

        foreach ($categories as $index => $item) {

            $query = DB::table('products');

            $query->where('category_id', '=', $item->id);

            $products = $query->skip(0)->take(8)->orderBy('created_at')->get();

            foreach ($products as $prodindex => $proditem) {

                $type = Type::where('id', '=', $proditem->type_id)->first();
                if ($type) {
                    if (session('lang') == 'ar') {
                        $products[$prodindex]->type = $type->ar_name;
                    } else {
                        $products[$prodindex]->type = $type->name;
                    }
                }

                if (session('lang') == 'ar') {
                    $products[$prodindex]->category = $item->ar_name;
                } else {
                    $products[$prodindex]->category = $item->name;
                }
                $products[$prodindex]->image = GetFile::where('target_id', $products[$prodindex]->id)->where('target_name', '=', 'PRODUCTS')->first()->path;
            }

            $categories[$index]->products = $products;

            //            foreach( $products as $product){
            //                $this->changePrice($product, $request->ip());
            //            }

        }

        $allProductList = [
            'data' => $categories,
        ];

        // =============================================================================================================
        // =============================================================================================================


        $data = [
            'mostrecentprod' => $mostrecentprod,
            'latestoffer' => $latestoffer,
            'allProductList' => $allProductList,
        ];

        return Utility::ToApi("Product has been listed successfuly", true, $data, "OK", 200);
    }

    public function TypesList(Request $request, $pageIndex, $pageSize)
    {

        $Types = Type::skip($pageIndex * $pageSize)->take($pageSize)->orderBy('created_at')->get();

        $count = Type::count();

        foreach ($Types as $index => $item) {

            $Types[$index]->prod_count = Product::where('type_id', '=', $item->id)->count();
        }

        $data = [
            'data' => $Types,
            'totalCount' => $count
        ];

        return Utility::ToApi("Product has been listed successfuly", true, $data, "OK", 200);
    }
    public function CTypesList(Request $request, $pageIndex, $pageSize)
    {

        $Types = AppCtype::skip($pageIndex * $pageSize)->take($pageSize)->orderBy('created_at')->get();

        $count = AppCtype::count();

        // foreach ($Types as $index => $item) {

        //     $Types[$index]->prod_count = Cproduct::where('type_id', '=', $item->id)->count();
        // }

        $data = [
            'data' => $Types,
            'totalCount' => $count
        ];

        return Utility::ToApi("CTypes has been listed successfuly", true, $data, "OK", 200);
    }

    public function ProductCartList(Request $request)
    {
        $arr = explode(",", $request->arr);
        $query = DB::table('products');
        $query->whereIn('id', $arr);
        $productss = $query->orderBy('created_at')->get();
        $count = $query->count();
        $minimumVal = 999999999;
        $maximumVal = 0;
        if (Auth::user()) {
            $coupon = user_coupon::where('user_id', Auth::user()->id)->where('status', 'use')->first();
        } else {
            $coupon = null;
        }
        $this->changePriceCart($productss, $request->ip());
        foreach ($productss as $index => $item) {
            $type = Type::where('id', '=', $item->type_id)->first();
            if ($type) {
                if (session('lang') == 'ar') {
                    $productss[$index]->type = $type->ar_name;
                } else {
                    $productss[$index]->type = $type->name;
                }
            }
            // $productss[$index]->attributes = $productss->getAttributsById(session('lang'));
            $category = Category::where('id', '=', $item->category_id)->first();
            if ($category) {
                if (session('lang') == 'ar') {
                    $productss[$index]->category = $category->ar_name;
                } else {
                    $productss[$index]->category = $category->name;
                }
            }

            $images = File::where('target_name', '=', 'PRODUCTS')->where('target_id', '=', $item->id)->get();

            if (sizeof($images) > 0) {
                $productss[$index]->image = $images[0];
            }


            if ($item->price > $maximumVal) {
                $maximumVal = $item->price;
            }

            if ($item->price < $minimumVal) {
                $minimumVal = $item->price;
            }
        }

        $data = [
            'data' => $productss,
            'totalCount' => $count,
            'maximumVal' => $maximumVal,
            'minimumVal' => $minimumVal,
            'coupon' => $coupon,
        ];

        return Utility::ToApi("Product has been listed successfuly", true, $data, "OK", 200);
    }

    public function ProductDelete(Request $request)
    {


        File::where('target_name', '=', 'PRODUCTS')->where('target_id', '=', $request->product_id)->delete();
        Product::where('id', '=', $request->product_id)->delete();

        return Utility::ToApi("Product has been deleted successfuly", true, $request->product_id, "OK", 200);
    }

    public function TypeDelete(Request $request)
    {

        Type::where('id', '=', $request->type_id)->delete();

        return Utility::ToApi("Product has been deleted successfuly", true, $request->type_id, "OK", 200);
    }
    public function CTypeDelete(Request $request)
    {

        AppCtype::where('id', '=', $request->type_id)->delete();

        return Utility::ToApi("Ctype has been deleted successfuly", true, $request->type_id, "OK", 200);
    }


    public function category(Request $request)
    {
        if ($request->header('language') == 'ar') {
            $category = \App\Http\Resources\CategoryAr::collection(Category::get());
            return Utility::ToApi("All categories has been listed successfuly", true, $category, "OK", 200);
        } elseif ($request->header('language') == 'en') {
            $category = \App\Http\Resources\CategoryEn::collection(Category::get());
            return Utility::ToApi("All categories has been listed successfuly", true, $category, "OK", 200);
        } elseif (session('lang') == 'ar') {
            $category = \App\Http\Resources\CategoryAr::collection(Category::get());
            return Utility::ToApi("All categories has been listed successfuly", true, $category, "OK", 200);
        } elseif (session('lang') == 'en') {
            $category = \App\Http\Resources\CategoryEn::collection(Category::get());
            return Utility::ToApi("All categories has been listed successfuly", true, $category, "OK", 200);
        } elseif (!session('lang')) {
            $category = \App\Http\Resources\CategoryEn::collection(Category::get());
            return Utility::ToApi("All categories has been listed successfuly", true, $category, "OK", 200);
        }
    }
    public function Ccategory(Request $request)
    {
        if ($request->header('language') == 'ar') {
            $category = \App\Http\Resources\CcategoryAr::collection(Ccategory::get());
            return Utility::ToApi("All categories has been listed successfuly", true, $category, "OK", 200);
        } elseif ($request->header('language') == 'en') {
            $category = \App\Http\Resources\CcategoryEn::collection(Ccategory::get());
            return Utility::ToApi("All categories has been listed successfuly", true, $category, "OK", 200);
        } elseif (session('lang') == 'ar') {
            $category = \App\Http\Resources\CcategoryAr::collection(Ccategory::get());
            return Utility::ToApi("All categories has been listed successfuly", true, $category, "OK", 200);
        } elseif (session('lang') == 'en') {
            $category = \App\Http\Resources\CcategoryEn::collection(Ccategory::get());
            return Utility::ToApi("All categories has been listed successfuly", true, $category, "OK", 200);
        } elseif (!session('lang')) {
            $category = \App\Http\Resources\CcategoryEn::collection(Ccategory::get());
            return Utility::ToApi("All categories has been listed successfuly", true, $category, "OK", 200);
        }
    }


    public function Type(Request $request)
    {

        //for APIs start
        if ($request->header('language') == "ar") {
            $type = \App\Http\Resources\TypeAr::collection(Type::get());
            return Utility::ToApi("All Types has been listed successfuly", true, $type, "OK", 200);
        } elseif ($request->header('language') == "en") {
            $type = \App\Http\Resources\TypeEn::collection(Type::get());
            return Utility::ToApi("All Types has been listed successfuly", true, $type, "OK", 200);
        }
        //for APIs end

        //for dash it is bad practice but ):

        elseif ($request->category_id && session('lang') == 'en') {
            $type = \App\Http\Resources\TypeEn::collection(Type::all()->where('category', $request->category_id));
            return Utility::ToApi("Types has been listed successfuly", true, $type, "OK", 200);
        } elseif ($request->category_id && session('lang') == 'ar') {
            $type = \App\Http\Resources\TypeAr::collection(Type::all()->where('category', $request->category_id));
            return Utility::ToApi("Types has been listed successfuly", true, $type, "OK", 200);
        }
        //for dash end

        //for web statr
        elseif (session('lang') == 'ar') {
            $type = \App\Http\Resources\TypeAr::collection(Type::get());
            return Utility::ToApi("All Types has been listed successfuly", true, $type, "OK", 200);
        } elseif (session('lang') == 'en') {
            $type = \App\Http\Resources\TypeEn::collection(Type::get());
            return Utility::ToApi("All Types has been listed successfuly", true, $type, "OK", 200);
        } elseif (session('lang') == " ") {
            $type = \App\Http\Resources\TypeEn::collection(Type::get());
            return Utility::ToApi("All Types has been listed successfulysss", true, $type, "OK", 200);
        }
        //for web end
        // }else{
        //     $type = App\Http\Resources\TypeEn::collection(Type::get());
        //     return   Utility::ToApi("All Types has been listed successfuly",true, $type,"OK",200);
        // }
    }

    public function CType(Request $request)
    {

        //for APIs start
        if ($request->header('language') == "ar") {
            $type = \App\Http\Resources\CtypeAr::collection(AppCtype::get());
            return Utility::ToApi("All Types has been listed successfuly", true, $type, "OK", 200);
        } elseif ($request->header('language') == "en") {
            $type = \App\Http\Resources\CtypeEn::collection(AppCtype::get());
            return Utility::ToApi("All Types has been listed successfuly", true, $type, "OK", 200);
        }
        //for APIs end

        //for dash it is bad practice but ):

        elseif ($request->category_id && session('lang') == 'en') {
            $type = \App\Http\Resources\CtypeEn::collection(AppCtype::all()->where('category', $request->category_id));
            return Utility::ToApi("Types has been listed successfuly", true, $type, "OK", 200);
        } elseif ($request->category_id && session('lang') == 'ar') {
            $type = \App\Http\Resources\CtypeAr::collection(AppCtype::all()->where('category', $request->category_id));
            return Utility::ToApi("Types has been listed successfuly", true, $type, "OK", 200);
        }
        //for dash end

        //for web statr
        elseif (session('lang') == 'ar') {
            $type = \App\Http\Resources\CtypeAr::collection(AppCtype::get());
            return Utility::ToApi("All Types has been listed successfuly", true, $type, "OK", 200);
        } elseif (session('lang') == 'en') {
            $type = \App\Http\Resources\CtypeEn::collection(AppCtype::get());
            return Utility::ToApi("All Types has been listed successfuly", true, $type, "OK", 200);
        } elseif (session('lang') == " ") {
            $type = \App\Http\Resources\CtypeEn::collection(AppCtype::get());
            return Utility::ToApi("All Types has been listed successfulysss", true, $type, "OK", 200);
        }
        //for web end
        // }else{
        //     $type = App\Http\Resources\TypeEn::collection(Type::get());
        //     return   Utility::ToApi("All Types has been listed successfuly",true, $type,"OK",200);
        // }
    }

    public function get_products_by_id(Request $request)
    {
        // Lang::getLocale();
        $product = Product::where('id', '=', $request->id)->first();
        $this->changePriceSingle($product, $request->ip());
        $type = Type::where('id', '=', $product->type_id)->first();
        if ($type) {
            $product->type = $type->name;
        }

        $category = Category::where('id', '=', $product->category_id)->first();
        if ($category) {
            $product->category = $category->name;
        }

        $images = File::where('target_name', '=', 'PRODUCTS')->where('target_id', '=', $product->id)->get();

        if (sizeof($images) > 0) {
            $product->image = $images[0];
        }

        $Attributes = attribute_values::all()->where('product_id', $product->id);

        foreach ($Attributes as $attribute) {
            $attribute->name = $attribute->name_of_attribute('en');
            $attribute->ar_name = $attribute->name_of_attribute('ar');
        }
        $product->attributes = $Attributes;
        if (session('lang') == 'ar') {
        };
        // // 'attributes' => $Attributes
        return Utility::ToApi("Product has been accessed successfuly", true, $product, "OK", 200);
    }


    public function related_product(Request $request)
    {

        $product = Product::where('id', '=', $request->id)->first();

        $query = DB::table('products');
        $query->where('category_id', '=', $product->category_id);
        $query->where('type_id', '=', $product->type_id);
        $query->where('id', '!=', $product->id);

        $products = $query->skip(0)->take(4)->orderBy('created_at')->get();

        $count = $query->count();

        $minimumVal = 999999999;
        $maximumVal = 0;
        if ($request->header('language') == "ar") {
            $this->getCurrencyForProductApi($request->header('currency'), '', $request->ip(), $products);
        } else {
            $this->changePriceCart($products, $request->ip());
        }

        foreach ($products as $index => $item) {

            $type = Type::where('id', '=', $item->type_id)->first();
            if (session('lang') == 'ar' || $request->header('language') == 'ar') {
                $products[$index]->type = $type->name;
            }

            $category = Category::where('id', '=', $item->category_id)->first();
            if (session('lang') == 'ar' || $request->header('language') == 'ar') {
                $products[$index]->category = $category->name;
            }

            $images = File::where('target_name', '=', 'PRODUCTS')->where('target_id', '=', $item->id)->get();

            if (sizeof($images) > 0) {
                $products[$index]->image = $images[0];
            }

            if (session('lang') == 'ar' || $request->header('language') == 'ar') {

                $products[$index]->name = $products[$index]->ar_name;
                $products[$index]->description = $products[$index]->ar_description;
                $products[$index]->short_description = $products[$index]->ar_short_description;
            };

            if ($item->price > $maximumVal) {
                $maximumVal = $item->price;
            }

            if ($item->price < $minimumVal) {
                $minimumVal = $item->price;
            }
        }

        $data = [
            'data' => $products,
            'totalCount' => $count,
            'maximumVal' => $maximumVal,
            'minimumVal' => $minimumVal,
        ];

        return Utility::ToApi("Product has been listed successfuly", true, $data, "OK", 200);
    }

    public function MyAllOrderList(Request $request, $pageIndex, $pageSize)
    {

        $token = $request->user;

        JWTAuth::setToken($token);
        $user = JWTAuth::toUser();
        if ($user) {
            $orders = Order::skip($pageIndex * $pageSize)->take($pageSize)->orderBy('created_at', 'DESC')->get();
            foreach ($orders as $index => $item) {
                $user = User::where('id', '=', $item->user_id)->first();
                $orders[$index]->username = $user->username;

                $OrdersStatus = OrderStates::where('id', '=', $item->orderStatus_id)->first();
                $orders[$index]->orderStatus_name = $OrdersStatus->name;
            }
            return Utility::ToApi("Product has been listed successfuly", true, $orders, "OK", 200);
        }
        return -1;
    }

    public function MyPendingOrderList(Request $request)
    {

        $token = $request->user;

        JWTAuth::setToken($token);
        $user = JWTAuth::toUser();
        if ($user) {
            $orders = Order::where('user_id', '=', $user->id)->where('orderStatus_id', '!=', 5)->get();
            foreach ($orders as $index => $item) {
                $OrdersStatus = OrderStates::where('id', '=', $item->orderStatus_id)->first();
                $orders[$index]->orderStatus_name = $OrdersStatus->name;
            }
            return Utility::ToApi("Product has been listed successfuly", true, $orders, "OK", 200);
        }
        return -1;
    }

    public function MyHistoryOrderList(Request $request)
    {

        $token = $request->user;

        JWTAuth::setToken($token);
        $user = JWTAuth::toUser();

        if ($user) {
            $orders = Order::where('user_id', '=', $user->id)->where('orderStatus_id', '=', 5)->get();
            foreach ($orders as $index => $item) {
                $OrdersStatus = OrderStates::where('id', '=', $item->orderStatus_id)->first();
                $orders[$index]->orderStatus_name = $OrdersStatus->name;
            }
            return Utility::ToApi("Product has been listed successfuly", true, $orders, "OK", 200);
        }
        return -1;
    }

    public function ChangeOrderStatus(Request $request)
    {

        $token = $request->user;

        JWTAuth::setToken($token);
        $user = JWTAuth::toUser();
        if ($user) {
            $Order = Order::where('id', '=', $request->id)->update(['orderStatus_id' => $request->orderStatus_id]);
            return Utility::ToApi("Order Status has been Changed successfuly", true, $Order, "OK", 200);
        }
        return -1;
    }


    public function GetOrderDetails(Request $request)
    {

        $token = $request->user;

        JWTAuth::setToken($token);
        $user = JWTAuth::toUser();
        if ($user) {
            $orders = Order::where('id', '=', $request->id)->first();
            if ($orders) {
                if ($user->id == $orders->user_id) {
                    $OrdersStatus = OrderStates::where('id', '=', $orders->orderStatus_id)->first();
                    $orders->orderStatus_name = $OrdersStatus->name;

                    $payment = Payment::where('orderID', '=', $orders->orderID)->first();
                    $ordered_product = ordered_product::where('order_id', '=', $orders->id)->get();
                    foreach ($ordered_product as $index => $item) {
                        $product = Product::where('id', '=', $item->product_id)->first();
                        $ordered_product[$index]->product_name = $product->name;
                        $ordered_product[$index]->price = $product->price;
                        $ordered_product[$index]->supplier = $product->supplier;
                        $ordered_product[$index]->unit = $product->unit;
                    }

                    $OrderStates = OrderStates::get();
                    foreach ($OrderStates as $index => $item) {
                        if ($item->id == $orders->orderStatus_id) {
                            $OrderStates[$index]->active = 1;
                        } else {
                            $OrderStates[$index]->active = 0;
                        }
                    }
                    $Data = [
                        'ordered_product' => $ordered_product,
                        'orders' => $orders,
                        'payment' => $payment,
                        'OrderStates' => $OrderStates
                    ];

                    return Utility::ToApi("Product has been listed successfuly", true, $Data, "OK", 200);
                    // } elseif ($user->name == 'admin') {
                } else {
                    $OrdersStatus = OrderStates::where('id', '=', $orders->orderStatus_id)->first();
                    $orders->orderStatus_name = $OrdersStatus->name;

                    $payment = Payment::where('orderID', '=', $orders->orderID)->first();
                    $ordered_product = ordered_product::where('order_id', '=', $orders->id)->get();
                    foreach ($ordered_product as $index => $item) {
                        $product = Product::where('id', '=', $item->product_id)->first();
                        $ordered_product[$index]->product_name = $product->name;
                        $ordered_product[$index]->price = $product->price;
                        $ordered_product[$index]->supplier = $product->supplier;
                        $ordered_product[$index]->unit = $product->unit;
                    }

                    $OrderStates = OrderStates::get();
                    foreach ($OrderStates as $index => $item) {
                        if ($item->id == $orders->orderStatus_id) {
                            $OrderStates[$index]->active = 1;
                        } else {
                            $OrderStates[$index]->active = 0;
                        }
                    }
                    $Data = [
                        'ordered_product' => $ordered_product,
                        'orders' => $orders,
                        'payment' => $payment,
                        'OrderStates' => $OrderStates
                    ];

                    return Utility::ToApi("Product has been listed successfuly", true, $Data, "OK", 200);
                }
            }
        }
        return Utility::ToApi("something went wrong", true, null, "OK", 201);
    }

    public function GetNameOfImage(Request $request)
    {
        $validation = Product::where('product_code', $request->name)->first();
        if (!empty($validation)) {
            return Utility::ToApi("This image for Another Product (for {{$validation->name}}) please Change it to continue", false, null, "OK", 201);
        } else {
            return Utility::ToApi("Ok you can continue", true, null, "OK", 201);
        }
    }
    public function index_products(Request $request, $id)
    {
        if (!session('lang')) {

            Session::put('lang', 'en');
        };
        $data = Category::get();
        $products = Product::where('category_id', $id)->paginate('9');
        foreach ($products as $item) {
            $item->image = $item->getImageUrl();
        }
        return view('Welcome.views.shop.products', compact('data', 'products'));
    }
}
