<?php

namespace App\Http\Controllers;

use App\Http\Services\RoleService;
use App\Role;
use App\Http\Utility;
use Illuminate\Http\Request;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\Validator;

class RoleController extends Controller
{

    protected $roleservice;
    public function __construct(RoleService $roleService)
    {
        $this->roleservice = $roleService;
        $this->middleware('ApiAuth:api');
    }

    public function add(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'name'=>'required','string'
        ]);
        if ($validator->fails())
            return   Utility::ToApi("missing fields",false, ["validator"=>$validator->messages()->first()],"BadRequest",400);

        $data=$request->only('name');

        $role=$this->roleservice->add($data);

        return Utility::ToApi("Role added successfully",true,  $role,"Ok",201);

    }

    public function getById($id)
    {
        $role=$this->roleservice->get($id);
        if($role!=null)
        return Utility::ToApi("Role return successfully",true,  $role,"Ok",200);
    }

     public function getAll(Request $request,$pageIndex,$pageSize)
    {
        list($role,$total)=$this->roleservice->getAll($request,$pageIndex,$pageSize);
        return Utility::ToPageApi("Roles returned successfully",true, $role,$total,"Ok",200);
    }

    public function Edit(Request $request,$id)
    {
        $data=$request->only('name');

        $role=$this->roleservice->Edit($id,$data);

        return Utility::ToApi("Role Edited successfully",true,  $role,"Ok",201);

    }

    public function delete ($id)
    {
       $role= $this->roleservice->delete($id);
       return Utility::ToApi("Role deleted successfully",true,  $role,"Ok",200);
    }

}
