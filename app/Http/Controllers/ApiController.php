<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Utility;
use App\Category;
use App\Type;
use App\Product;
use App\Http\Resources\ProductAr,
    App\Http\Resources\ProductEn,
    App\Http\Resources\ImagesProduct,
    App\Http\Resources\MainImageProduct;
use Illuminate\Support\Facades\Session;
use App\traits\ChangingPrice;
use Illuminate\Support\Facades\App;

class ApiController extends Controller
{

    use ChangingPrice;

    public function categories(Request $request)
    {
        //moblie
        if ($request->header('language') == 'ar') {
            $category = \App\Http\Resources\CategoryAr::collection(Category::get());
            foreach ($category as $item) {

                $item->types = $item->getalltypes('ar');
            }
            return Utility::ToApi("All categories has been listed successfuly", true, $category, "OK", 200);
        } elseif ($request->header('language') == 'en') {
            $category = \App\Http\Resources\CategoryEn::collection(Category::get());
            foreach ($category as $item) {

                $item->types = $item->getalltypes('en');
            }
            return Utility::ToApi("All categories has been listed successfuly", true, $category, "OK", 200);
        } //web
        elseif (session('lang') == 'ar') {
            $category = \App\Http\Resources\CategoryAr::collection(Category::get());
            return Utility::ToApi("All categories has been listed successfuly", true, $category, "OK", 200);
        } elseif (session('lang') == 'en') {
            $category = \App\Http\Resources\CategoryEn::collection(Category::get());
            return Utility::ToApi("All categories has been listed successfuly", true, $category, "OK", 200);
        }
        //    elseif(!session('lang')){
        //        $category = \App\Http\Resources\CategoryEn::collection(Category::get());
        //        return Utility::ToApi("All categories has been listed successfuly", true, $category, "OK", 200);
        //    }

    }


    //Types
    public function types(Request $request)
    {
        if ($request->category_id && $request->header('language') == 'en') {
            $type = \App\Http\Resources\TypeEn::collection(Type::all()->where('category', $request->category_id));
            return Utility::ToApi("Types has been listed successfuly", true, $type, "OK", 200);
        } elseif ($request->category_id && $request->header('language') == 'ar') {
            $type = \App\Http\Resources\TypeAr::collection(Type::all()->where('category', $request->category_id));
            return Utility::ToApi("Types has been listed successfuly", true, $type, "OK", 200);
        } // for web
        elseif ($request->category_id && session('lang') == 'en') {
            $type = \App\Http\Resources\TypeEn::collection(Type::all()->where('category', $request->category_id));
            return Utility::ToApi("Types has been listed successfuly", true, $type, "OK", 200);
        } elseif ($request->category_id && session('lang') == 'ar') {
            $type = \App\Http\Resources\TypeAr::collection(Type::all()->where('category', $request->category_id));
            return Utility::ToApi("Types has been listed successfuly", true, $type, "OK", 200);
        }
    }

    //product by id
    public function get_product(Request $request)
    {
        $ProductId = $request->id;
        $HeaderLang = $request->header('language');
        $HeaderCurrency = $request->header('currency');
        $Session = session('lang');
        $Ip = $request->ip();

        //mobile section
        if ($HeaderLang && $ProductId) {
            $product = Product::where('id', $ProductId)->get();
            //get category and type and attributes
            foreach ($product as $item) {
                $item->category = $item->getCategorieById($HeaderLang);
                $item->type = $item->getTypeById($HeaderLang);
                $item->attributes = $item->getAttributsById($HeaderLang);
                $item->main_image = MainImageProduct::collection($item->getMainImagebyId());
                $item->images = ImagesProduct::collection($item->getImagesbyId());
            }
            //get currency
            $this->getCurrencyForProductApi($HeaderCurrency, $Session, $Ip, $product);
            //for finish get values and filter
            if ($HeaderLang == 'ar') {
                $data = ProductAr::collection($product);
            }
            if ($HeaderLang == 'en') {
                $data = ProductEn::collection($product);
            }

            return Utility::ToApi("return product has been successfuly", true, $data, "OK", 200);
        }
        //web section
        if ($Session && $ProductId) {

            return "web hi please";
        }
    }

    public function getListProducts(Request $request, $pageIndex, $pageSize)
    {
        // $ProductId = "delete";
        $HeaderLang = $request->header('language');
        $HeaderCurrency = $request->header('currency');
        $Session = session('lang');
        $Ip = $request->ip();

        //mobile section
        if ($HeaderLang) {
            $product = Product::all()->skip($pageIndex * $pageSize)->take($pageSize)->sortByDesc('created_at');
            //get category and type and attributes
            foreach ($product as $item) {
                $item->category = $item->getCategorieById($HeaderLang);
                $item->type = $item->getTypeById($HeaderLang);
                $item->attributes = $item->getAttributsById($HeaderLang);
                $item->main_image = MainImageProduct::collection($item->getMainImagebyId());
                $item->images = ImagesProduct::collection($item->getImagesbyId());
            }
            //get currency
            $this->getCurrencyForProductApi($HeaderCurrency, $Session, $Ip, $product);
            //for finish get values and filter
            if ($HeaderLang == 'ar') {
                $data = ProductAr::collection($product);
            }
            if ($HeaderLang == 'en') {
                $data = ProductEn::collection($product);
            }

            return Utility::ToApi("return product has been successfuly", true, $data, "OK", 200);
        }
        //web section
        if ($Session) {

            return "web hi please";
        }
    }

    //api for search
    public function searchByText(Request $request, $pageIndex, $pageSize)
    {

        $HeaderLang = $request->header('language');
        $HeaderCurrency = $request->header('currency');
        $Session = session('lang');
        $Ip = $request->ip();
        $category = $request->category;
        $type = $request->type;
        $Text = $request->text;
        $from = intval($request->from);
        $to = intval($request->to);
        if ($from == '' && $to == '') {
            $from = 1;
            $to = 1000000000;
        }
        if ($HeaderLang) {
            if ($HeaderLang == 'ar') {
                $name = 'ar_name';
                $description = 'ar_description';
            } elseif ($HeaderLang == 'en') {
                $name = 'name';
                $description = 'description';
            }
            //search method
            // if cate and type unset
            if ($category == '' && $type == '' ) {
                $product = Product::query()
                    ->where($name, 'like', '%' . $Text . '%')
                    ->whereBetween('price', [$from, $to])
                    ->orWhere($description, 'like', '%' . $Text . '%')
                    ->whereBetween('price', [$from, $to])
                    ->skip($pageIndex * $pageSize)
                    ->take($pageSize)
                    ->orderBy('created_at')
                    ->get();
            }
            // if cate and type set
            if ($category && $type) {
                $product = Product::query()
                    ->where($name, 'like', '%' . $Text . '%')
                    ->where('category_id', '=', $category)
                    ->where('type_id', '=', $type)
                    ->whereBetween('price', [$from, $to])
                    ->orWhere($description, 'like', '%' . $Text . '%')
                    ->where('category_id', '=', $category)
                    ->where('type_id', '=', $type)
                    ->whereBetween('price', [$from, $to])
                    ->skip($pageIndex * $pageSize)
                    ->take($pageSize)
                    ->orderBy('created_at')
                    ->get();
            }
            // if cate unset and type set
            if ($category == '' && $type) {
                $product = Product::query()
                    ->where($name, 'like', '%' . $Text . '%')
                    ->where('type_id', '=', $type)
                    ->whereBetween('price', [$from, $to])
                    ->orWhere($description, 'like', '%' . $Text . '%')
                    ->where('type_id', '=', $type)
                    ->whereBetween('price', [$from, $to])
                    ->skip($pageIndex * $pageSize)
                    ->take($pageSize)
                    ->orderBy('created_at')
                    ->get();
            }
            // if cate set and type unset
            if ($category && $type == '') {
                $product = Product::query()
                    ->where($name, 'like', '%' . $Text . '%')
                    ->where('category_id', '=', $category)
                    ->whereBetween('price', [$from, $to])
                    ->orWhere($description, 'like', '%' . $Text . '%')
                    ->where('category_id', '=', $category)
                    ->whereBetween('price', [$from, $to])
                    ->skip($pageIndex * $pageSize)
                    ->take($pageSize)
                    ->orderBy('created_at')
                    ->get();
            }
            if ($category == '' && $type && $Text == '') {
                $product = Product::query()
                    ->where('type_id', '=', $type)
                    ->whereBetween('price', [$from, $to])
                    ->skip($pageIndex * $pageSize)
                    ->take($pageSize)
                    ->orderBy('created_at')
                    ->get();
            }
            if ($category && $type && $Text == '') {
                $product = Product::query()
                    ->where('category_id', '=', $category)
                    ->where('type_id', '=', $type)
                    ->whereBetween('price', [$from, $to])
                    ->skip($pageIndex * $pageSize)
                    ->take($pageSize)
                    ->orderBy('created_at')
                    ->get();
            }
            // if cate set and type unset
            if ($category && $type == '' && $Text == '') {
                $product = Product::query()
                    ->where('category_id', '=', $category)
                    ->whereBetween('price', [$from, $to])
                    ->skip($pageIndex * $pageSize)
                    ->take($pageSize)
                    ->orderBy('created_at')
                    ->get();
            }

            //get category and type and attributes
            foreach ($product as $item) {
                $item->category = $item->getCategorieById($HeaderLang);
                $item->type = $item->getTypeById($HeaderLang);
                $item->attributes = $item->getAttributsById($HeaderLang);
                $item->main_image = MainImageProduct::collection($item->getMainImagebyId());
                $item->images = ImagesProduct::collection($item->getImagesbyId());
            }
            //get currency
            $this->getCurrencyForProductApi($HeaderCurrency, $Session, $Ip, $product);
            //for finish get values and filter
            if ($HeaderLang == 'ar') {
                $data = ProductAr::collection($product);
            }
            if ($HeaderLang == 'en') {
                $data = ProductEn::collection($product);
            }

            if (count($data) <= 0) {
                return Utility::ToApi("No results found", true, "No results found", "OK", 200);
            }
            $datas = [
                'data' => $data,
                'price range' => $from . ': to :' . $to
            ];
            return Utility::ToApi("return product has been successfuly", true, $datas, "OK", 200);
        }
    }
}
