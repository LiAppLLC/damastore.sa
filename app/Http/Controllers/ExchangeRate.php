<?php

namespace App\Http\Controllers;

use Illuminate\Routing\Controller;

class ExchangeRate extends Controller
{
    public static function getConverseRate($name)
    {
        if ($name) {
            if ($obj = self::makeApiRequest()) {
                if (isset($obj['rates'])) {
                    return $obj['rates'][$name];
                } else {
                    return 1;
                }
            } else {
                return 1;
            }
        } else {
            return 1;
        }
    }

    public static function makeApiRequest()
    {
        try {
            $currency_json = file_get_contents("http://data.fixer.io/api/latest?access_key=e01d8e91fcc030ac5464efbee4b147d5");
            return json_decode($currency_json, true);
        } catch (\Exception $e) {
            return 0;
        }
    }


    public static function getAllRates()
    {
        $obj = self::makeApiRequest();
        $results = [];
        foreach ($obj['conversion_rates'] as $key => $value) {
            $results[$key] = $value;
        }
        return $results;
    }
}
