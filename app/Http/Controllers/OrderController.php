<?php

namespace App\Http\Controllers;

use App\Http\Services\OrderService;
use App\Order;
use App\refund;
use App\Payment;
use App\Http\Utility;
use Illuminate\Http\Request;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\Validator;
use JWTAuth;
use App\Http\Services\UserRoleService;

class OrderController extends Controller
{

    protected $orderservice;
    public function __construct(OrderService $orderService)
    {
        $this->orderservice = $orderService;
        $this->middleware('ApiAuth:api');
    }

    public function add(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'shipping'=>'required',
            'products'=>'required',
        ]);
        if ($validator->fails())
            return   Utility::ToApi("missing fields",false, ["validator"=>$validator->messages()->first()],"BadRequest",400);

        $order=$this->orderservice->add($request);

        return Utility::ToApi("Order added successfully",true,  $order,"Ok",201);

    }

    public function getById($id)
    {
        $order=$this->orderservice->get($id);
        if($order!=null)
        return Utility::ToApi("Order return successfully",true,  $order,"Ok",200);
    }

     public function getAll(Request $request,$pageIndex,$pageSize)
    {
        list($order,$total)=$this->orderservice->getAll($request,$pageIndex,$pageSize);
        return Utility::ToPageApi("Orders returned successfully",true, $order,$total,"Ok",200);
    }

    public function changeOrderStatus(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'status'=>'required',
            'order_id'=>'required',
            'company_id'=>'required'
        ]);
        if ($validator->fails())
            return   Utility::ToApi("missing fields",false, ["validator"=>$validator->messages()->first()],"BadRequest",400);

        $data=$request->only('status','order_id','company_id');

        $order=$this->orderservice->changeOrderStatus($data);

        return Utility::ToApi("Order status changed successfully",true,  $order,"Ok",201);

    }

    public function refund(Request $request)
    {
        //dd($request->all());

        $order_id = $request->order_id;
        $token = $request->user;

        JWTAuth::setToken($token);
        $user = JWTAuth::toUser();

        $userroleService = new UserRoleService();
        $user->roles = $userroleService->getByUser($user->id,0,99999);

        foreach ($user->roles[0] as $item){
            if($item->name == 'superUser'){
                $payment = Payment::where('id', $order_id)->first();
                // dd($payment);
//                [
//                    "captures_id" => "4YB14578K4864401F"
//                    "reference_id" => "PUHF"
//                    "orderID" => "3MB36518UT697722S"
//                    "payerID" => "28G9X5L2TEQ9G"
//                    "create_time" => "2020-12-25T16:27:12Z"
//                    "country_code" => "CA"
//                    "email_address" => "abd.r.horani@gmail.com"
//                    "UserFullName" => "Abd Alhorani"
//                    "payer_id" => "28G9X5L2TEQ9G"
//                    "amountValue" => "230.00"
//                    "amountCurrency_code" => "USD"
//                    "payeeEmail_address" => "sb-gfpwq3644088@business.example.com"
//                    "payeeMerchant_id" => "REQKZDXV58DCU"
//                    "status" => "COMPLETED"
//                    "created_at" => "2020-12-25 16:27:32"
//                    "updated_at" => "2020-12-25 16:27:32"
//                  ]
                //dd($payment);
                //dd($payment->captures_id);

//                $headers = ['Content-Type: application/json'];
$ch = curl_init();
$clientId='AdQtSsBcE3eYuhJWSdys_9vR1yc6LIKbJylLTAAsCUUlzx15DvTHXJQuRpchNHGjhhs42Iwuxc7_Cc5Y';
$secret='EPrgqWNKU_I8xSWeERXZvEiHjjAlxKtD_4zm0240uZyFtwrDqKugcACol5-Jtqah_JSxP3Zz-KBx9Ur0';
// $clientId = "myId";
// $secret = "mySecret";

curl_setopt($ch, CURLOPT_URL, "https://api.sandbox.paypal.com/v1/oauth2/token");
curl_setopt($ch, CURLOPT_HEADER, false);
curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
curl_setopt($ch, CURLOPT_POST, true);
curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
curl_setopt($ch, CURLOPT_USERPWD, $clientId.":".$secret);
curl_setopt($ch, CURLOPT_POSTFIELDS, "grant_type=client_credentials");

$result = curl_exec($ch);

if(empty($result))die("Error: No response.");
else
{
    $json = json_decode($result);
    // print_r($json->access_token);
}
curl_close ($ch);

                // $headers = ['Content-Type: application/json', 'Authorization: Bearer '.$json->access_token];
//                $headers = ['Content-Type: application/json', 'PayPal-Request-Id: 123e4567-e89b-12d3-a456-426655440020'];
                $request_body = [];//dd("https://api.sandbox.paypal.com/v2/payments/captures/".$payment->captures_id."/refund");

                $curl = curl_init("https://api-m.sandbox.paypal.com/v2/payments/captures/".$payment->captures_id."/refund");

                // curl_setopt ($curl, CURLOPT_HTTPHEADER, $headers);
                curl_setopt($curl, CURLOPT_HTTPHEADER, array(
                    'Authorization: Bearer ' . $json->access_token,
                    'Accept: application/json',
                    'Content-Type: application/json'
                ));
                curl_setopt ($curl, CURLOPT_RETURNTRANSFER, true);
                curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
                curl_setopt ($curl, CURLOPT_POST,true);
                // curl_setopt ($curl, CURLOPT_POSTFIELDS, json_encode($request_body));
                //curl_setopt ($curl, CURLOPT_USERPWD, 'ASQj_Zu8HgOGthmrFVoHPGXv4-plJH82vXq4WbhESR4tVGhn-nK3rqctVVeiqbgPiYayLLx6zJtWnazt:EDZm8-4g8sBWv8L_jngfLl0i3qt5AlyJ8RfVtgpy0y3arSbyVZds2aZARNVNhrqh-NHlVqDqvpmWVQB0');

                $response = curl_exec ($curl);

       $json_arr = json_decode($response);

    //  dd($json_arr,$json_arr->id);
                 $refund = new refund();
                 $refund->tr_id=$json_arr->id;
                 $refund->status=$json_arr->status;
                 $refund->total=$payment->amountValue;
                 $refund->payment_id=(int)$payment->id;
                 $user_id=Order::where('id',$payment->reference_id)->first()->user_id;

                 $refund->user_id= (int)$user_id;
                 $refund->save();

                 $update_status_order= Order::find($payment->reference_id);
                 $update_status_order->orderStatus_id=10;
                 $update_status_order->save();

                 $payment_update=Payment::find($payment->id);
                 $payment_update->status="Refunded";
                 $payment_update->save();

                 curl_close ($curl);
                return Utility::ToApi("order hus been refunded successfuly",true, $refund,"Ok",200);
            }
        }


        return Utility::ToApi("order cant refund",true,  null,"Ok",401);

    }

    public function delete ($id)
    {
       //$order= $this->orderservice->delete($id);
       return Utility::ToApi("Order deleted successfully",true,  $order,"Ok",200);
    }

}
