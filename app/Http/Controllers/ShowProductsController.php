<?php

namespace App\Http\Controllers;
use App;
use App\traits\ChangingPrice;
use Session;
use App\Category;
use Illuminate\Http\Request;
use App\Http\Resources\CategoryAr;
use App\Http\Resources\CategoryEn;
use App\Product;
use Illuminate\Http\Resources\Json\ResourceCollection;

class ShowProductsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    // public function index(Request $request,$id)
    // {
    //     if(!session('lang')){

    //         Session::put('lang', 'en');
    //     };
    //         $data=Category::get();
    //         $products=Product::where('category_id',$id)->paginate('9');
    //         foreach ($products as $item) {
    //             $item->image=$item->getImageUrl();
    //         }
    //     return view('Welcome.views.shop.products',compact('data','products'));
    // }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
