<?php

namespace App\Http\Controllers\View;

use App;
use Session;

use App\Mail\VerficationMailable;
use App\VerficationCode;

use App\Http\Controllers\Controller;
use App\Providers\RouteServiceProvider;
use Illuminate\Foundation\Auth\ConfirmsPasswords;
use Illuminate\Http\Request;
use App\Http\Services\AuthService;
use App\Http\Utility;

use App\User;
use Carbon\Carbon;

class emailController extends Controller
{

    protected $authService;
    public function __construct(AuthService $authService)
    {
        $this->authService = $authService;
    }

    public function email1()
    {

        $name = 'Abode';
        $verificationCode = '123456';

        return view('email/name', compact('name', 'verificationCode'));
    }
    public function email2()
    {

        $name = 'Abode';
        $verificationCode = '123456';

        return view('email/name2', compact('name', 'verificationCode'));
    }
    public function email3()
    {

        $name = 'Abode';
        $data = [];
        $data['products'] = [];
        array_push($data['products'], [1, 'name1', 2, 10]);
        array_push($data['products'], [1, 'name2', 3, 30]);
        array_push($data['products'], [1, 'name3', 4, 70]);
        $data['tax'] = '10';
        $data['total'] = '110';

        return view('email/name3', compact('name', 'data'));
    }

    public function sendEmail()
    {

        VerficationMailable::sendEmail('abd.r.horani@gmail.com', 'Abdelrahman Alhorani', '123456');
        dd('done');
    }

    public function OrderEmail()
    {

        VerficationMailable::sendOrderDetailsByEmail('abd.r.horani@gmail.com', 'Abdelrahman Alhorani', '123456');
        dd('Order Email done');
    }

    public  function signInWithGoogle(Request $request)
    {
        $email =  User::RegOrLogin($request);
        $user = $this->authService->EmailLogin($email, '728080330284-30rvpujtk8qfef37pggpcd1iv3il4k34.apps.googleusercontent.com');
        if ($user) {
            if ($user[0] == 1) {

                return Utility::ToApi("Login successful", true, $user[1], "ok", 201);
            }
            if ($user[0] == -1) {
                return Utility::ToApi("Blocked User", true, $user[1], "ok", 402);
            }
            if ($user[0] == -2) {
                return Utility::ToApi("Your account is not verified yet, check your Email", true, $user[1], "ok", 403);
            }
            //return Utility::ToApi("Login successful", true, $user[1], "ok", 201);
        }
        return   Utility::ToApi("Unauthorized", false, ['error' => 'Unauthorized'], "Unauthorized", 401);
    }

    public  function signInWithFacebook(Request $request)
    {


        $email =  User::RegOrLogin($request);
        $user = $this->authService->EmailLogin($email, '468866187481149');
        if ($user) {
            if ($user[0] == 1) {

                return Utility::ToApi("Login successful", true, $user[1], "ok", 201);
            }
            if ($user[0] == -1) {
                return Utility::ToApi("Blocked User", true, $user[1], "ok", 402);
            }
            if ($user[0] == -2) {
                return Utility::ToApi("Your account is not verified yet, check your Email", true, $user[1], "ok", 403);
            }
            //return Utility::ToApi("Login successful", true, $user[1], "ok", 201);
        }
        return   Utility::ToApi("Unauthorized", false, ['error' => 'Unauthorized'], "Unauthorized", 401);
    }
}
