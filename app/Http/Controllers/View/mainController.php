<?php

namespace App\Http\Controllers\View;
use App;
use App\traits\ChangingPrice;
use Session;
use App\User;
use Illuminate\Support\Facades\Auth;
use JWTAuth;
use App\Order;

use App\Http\Controllers\Controller;
use App\Providers\RouteServiceProvider;
use Illuminate\Foundation\Auth\ConfirmsPasswords;
use Illuminate\Http\Request;


class mainController extends Controller
{
    use ChangingPrice;
    public function changeLang(Request $request){
        Session::put('lang', $request->lang);
    }


    public function corporateDemo(){return view('demos/corporate_1/index');}
    public function agencyDemo(){return view(' demos/agency_1/index');}
    public function shopHome1(){return view('demos/default/shop-home-1');}
    public function shopHome2(){return view('demos/default/shop-home-2');}
    public function shopHome3(){return view('demos/default/shop-home-3');}
    public function shopHome4(){return view('demos/default/shop-home-4');}
    public function shopHome5(){return view('demos/default/shop-home-5');}
    public function shopHome6(){return view('demos/default/shop-home-6');}
    public function shopHome7(){return view('demos/default/shop-home-7');}
    public function shopHome8(){return view('demos/default/shop-home-8');}
    public function shopProductList(){return view('demos/default/shop-product-list');}
    public function shopProductDetails(){return view('demos/default/shop-product-details');}
    public function ProductComparison(){return view('demos/default/shop-product-comparison');}
    public function CheckoutComplete(){return view('demos/default/shop-checkout-complete');}
    public function ShopComponents1(){return view('demos/default/component-shop-1');}
    public function ShopComponents7(){return view('demos/default/component-shop-7');}
    public function WishList(){return view('demos/default/shop-product-wishlist');}
    public function AboutAs(){return view('demos/default/page-about-1');}
    public function ContactAs(){return view('demos/default/page-contact-1');}
    public function GalleryPage(){return view('demos/default/page-fullwidth-gallery');}
    public function Portofolio(){return view('demos/default/page-masonry-portfolio');}
    public function BlogList(){return view('demos/default/page-blog-list');}
    public function welcome(Request $request){
        //// subhe start code for home page
             $products = App\Product::paginate(8);
             $products_last = App\Product::skip(0)->take(1)->orderby('created_at','DESC')->get();
             $category = App\Category::all();
                $this->changePrice($products, $request->ip());
                $this->changePrice($products_last, $request->ip());

                //for set lang default
                if(!session('lang')){

                    Session::put('lang', 'en');
                };
        //// subhe end code for home page
        return view('Welcome.welcome' ,compact('products','category','products_last'));
    }

    

    public function CustomerLoginRegister(){
        return view('Welcome/views/auth/shop-customer-account');
    }
    public function CustomerDashboard(){
        return view('Welcome/views/myprofile/shop-customer-dashboard');
    }
    public function deletion(){
         $user_id = Session::get('userObject')->id;
         $user=User::find($user_id);
         if($user){
            return view('Welcome/views/myprofile/shop-customer-deletion',compact('user'));
         }else{
             return redirect('/');
         }
        
    }
    public function editprofile(){
        return view('Welcome/views/myprofile/shop-customer-profile');
    }
    public function changePassword(){
        return view('Welcome/views/myprofile/shop-customer-changePassword');
    }
    public function Checkout(){
        return view('demos/default/shop-checkout');
    }
    public function orderHistory(){
        return view('Welcome/views/myprofile/shop-order-history-2');
    }
    public function PendingOrder(){
        return view('Welcome/views/myprofile/pendingOrder');
    }
    public function shop(){
        return view('Welcome/views/shop/shop-product-grid');
    }
    public function productDetails(Request $request){
        $product= App\Product::find($request->id);
        $product_img=App\File::where('target_id',$request->id)->where('target_name', '=', 'PRODUCTS')->get();
        $product_gallery=App\File::where('target_id',$request->id)->where('target_name', '=', 'PRODUCTS_IMAGES')->get();
        if(!session('lang')){

            Session::put('lang', 'en');
        };
        $products= App\Product::where('id',$request->id)->first();
        return view('Welcome/views/productDetails/productDetails',compact('product_img','product_gallery','product'))->with(['products'=>$products]);
    }
    public function cart(){
        return view('Welcome/views/cart/shop-cart');
    }

    public function PrivacyPolicy(){
        return view('terms/PrivacyPolicy');
    }

    public function TermsConditions(){
        return view('terms/TermsConditions');
    }

    public function order(){

        return view('Welcome/views/checkout/shop-checkout');
    }
    public function orderDetails($orderID){

        $user = Session::get('userObject');
        $order = Order::where('id', '=', $orderID)->first();
        if($order->user_id == $user->id) {
            return view('Welcome/views/myprofile/orderDetails');
        } else {
            return redirect('/CustomerLoginRegister');
        }
    }



    public function store(Request $request){
        $_SESSION['LAST_ACTIVITY'] = time();
        $token = $request->user;

        JWTAuth::setToken($token);
        $user = JWTAuth::toUser();

        Session::put('userObject', $user);
    }
    public function wallet(){
     return view('Welcome.views.myprofile.wallet');
    }

}
