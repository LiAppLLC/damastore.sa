<?php

namespace App\Http\Controllers\View;

use App\Http\Services\AuthService;
use App;
use Illuminate\Support\Facades\Validator;
use Session;
use App\traits\ChangingPrice;
use App\Product;
use App\Type;
use App\Category;
use App\File;
use Illuminate\Support\Facades\File as FilesSubhe;
use Illuminate\Support\Facades\Storage;
use App\Http\Controllers\Controller;
use App\Providers\RouteServiceProvider;
use Illuminate\Foundation\Auth\ConfirmsPasswords;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;
use App\attributes;
use App\Cproduct;

class adminController extends Controller
{
    use ChangingPrice;

    protected $authService;

    public function __construct(AuthService $authService)
    {
        $this->authService = $authService;
        if (!session('lang')) {

            Session::put('lang', 'en');
        };
    }

    public function admin()
    {
        $_SESSION['LAST_ACTIVITY'] = time();
        $user = Session::get('userObject');

        $function = 'Dashbored';
        if ($this->authService->canDo($function, $user->id)) {
            $permissions = [
                'Dashbored' => $this->authService->canDo('Dashbored', $user->id),
                'users_list' => $this->authService->canDo('users_list', $user->id),
                'orders_list' => $this->authService->canDo('orders_list', $user->id),
                'orders_add' => $this->authService->canDo('orders_add', $user->id),
                'orders_edit' => $this->authService->canDo('orders_edit', $user->id),
                'orders_delete' => $this->authService->canDo('orders_delete', $user->id),
                'assignRoleToUser' => $this->authService->canDo('assignRoleToUser', $user->id),
                'permissions_list' => $this->authService->canDo('permissions_list', $user->id),
                'permissions_add' => $this->authService->canDo('permissions_add', $user->id),
                'permissions_edit' => $this->authService->canDo('permissions_edit', $user->id),
                'permissions_delete' => $this->authService->canDo('permissions_delete', $user->id),
                'assignPermissionsToRole' => $this->authService->canDo('assignPermissionsToRole', $user->id),
                'roles_list' => $this->authService->canDo('roles_list', $user->id),
                'roles_add' => $this->authService->canDo('roles_add', $user->id),
                'roles_edit' => $this->authService->canDo('roles_edit', $user->id),
                'roles_delete' => $this->authService->canDo('roles_delete', $user->id),
                'types_list' => $this->authService->canDo('types_list', $user->id),
                'types_add' => $this->authService->canDo('types_add', $user->id),
                'types_edit' => $this->authService->canDo('types_edit', $user->id),
                'types_delete' => $this->authService->canDo('types_delete', $user->id),
                'products_list' => $this->authService->canDo('products_list', $user->id),
                'products_edit' => $this->authService->canDo('products_edit', $user->id),
                'products_delete' => $this->authService->canDo('products_delete', $user->id)
            ];
            return view('admin/views/Dashboard', compact('permissions'));
        } else {
            return Redirect::back()->withErrors(['msg', 'The Message']);
        }
    }

    public function users()
    {
        $_SESSION['LAST_ACTIVITY'] = time();
        $user = Session::get('userObject');

        $function = 'users_list';
        if ($this->authService->canDo($function, $user->id)) {
            $permissions = [
                'Dashbored' => $this->authService->canDo('Dashbored', $user->id),
                'users_list' => $this->authService->canDo('users_list', $user->id),
                'orders_list' => $this->authService->canDo('orders_list', $user->id),
                'orders_add' => $this->authService->canDo('orders_add', $user->id),
                'orders_edit' => $this->authService->canDo('orders_edit', $user->id),
                'orders_delete' => $this->authService->canDo('orders_delete', $user->id),
                'assignRoleToUser' => $this->authService->canDo('assignRoleToUser', $user->id),
                'permissions_list' => $this->authService->canDo('permissions_list', $user->id),
                'permissions_add' => $this->authService->canDo('permissions_add', $user->id),
                'permissions_edit' => $this->authService->canDo('permissions_edit', $user->id),
                'permissions_delete' => $this->authService->canDo('permissions_delete', $user->id),
                'assignPermissionsToRole' => $this->authService->canDo('assignPermissionsToRole', $user->id),
                'roles_list' => $this->authService->canDo('roles_list', $user->id),
                'roles_add' => $this->authService->canDo('roles_add', $user->id),
                'roles_edit' => $this->authService->canDo('roles_edit', $user->id),
                'roles_delete' => $this->authService->canDo('roles_delete', $user->id),
                'types_list' => $this->authService->canDo('types_list', $user->id),
                'types_add' => $this->authService->canDo('types_add', $user->id),
                'types_edit' => $this->authService->canDo('types_edit', $user->id),
                'types_delete' => $this->authService->canDo('types_delete', $user->id),
                'products_list' => $this->authService->canDo('products_list', $user->id),
                'products_edit' => $this->authService->canDo('products_edit', $user->id),
                'products_delete' => $this->authService->canDo('products_delete', $user->id)
            ];
            return view('admin/views/users/users', compact('permissions'));
        } else {
            return Redirect::back()->withErrors(['msg', 'The Message']);
        }
    }

    public function products_list(Request $request)
    {
        $_SESSION['LAST_ACTIVITY'] = time();
        $user = Session::get('userObject');
        $roles = $this->authService->getUserRoles($user->id);

        $function = 'products_list';
        if ($this->authService->canDo($function, $user->id)) {
            $permissions = [
                'Dashbored' => $this->authService->canDo('Dashbored', $user->id),
                'users_list' => $this->authService->canDo('users_list', $user->id),
                'orders_list' => $this->authService->canDo('orders_list', $user->id),
                'orders_add' => $this->authService->canDo('orders_add', $user->id),
                'orders_edit' => $this->authService->canDo('orders_edit', $user->id),
                'orders_delete' => $this->authService->canDo('orders_delete', $user->id),
                'assignRoleToUser' => $this->authService->canDo('assignRoleToUser', $user->id),
                'permissions_list' => $this->authService->canDo('permissions_list', $user->id),
                'permissions_add' => $this->authService->canDo('permissions_add', $user->id),
                'permissions_edit' => $this->authService->canDo('permissions_edit', $user->id),
                'permissions_delete' => $this->authService->canDo('permissions_delete', $user->id),
                'assignPermissionsToRole' => $this->authService->canDo('assignPermissionsToRole', $user->id),
                'roles_list' => $this->authService->canDo('roles_list', $user->id),
                'roles_add' => $this->authService->canDo('roles_add', $user->id),
                'roles_edit' => $this->authService->canDo('roles_edit', $user->id),
                'roles_delete' => $this->authService->canDo('roles_delete', $user->id),
                'types_list' => $this->authService->canDo('types_list', $user->id),
                'types_add' => $this->authService->canDo('types_add', $user->id),
                'types_edit' => $this->authService->canDo('types_edit', $user->id),
                'types_delete' => $this->authService->canDo('types_delete', $user->id),
                'products_list' => $this->authService->canDo('products_list', $user->id),
                'products_edit' => $this->authService->canDo('products_edit', $user->id),
                'products_delete' => $this->authService->canDo('products_delete', $user->id)
            ];

            $products = Product::all();
            $this->changePrice($products, $request->ip());
            return view('admin/views/products/list', compact('permissions'), compact('products'));
        } else {
            return Redirect::back()->withErrors(['msg', 'The Message']);
            //            @if($errors->any())
            //                <h4>{{$errors->first()}}</h4>
            //            @endif
        }
    }

    public function types_list()
    {
        $_SESSION['LAST_ACTIVITY'] = time();
        $user = Session::get('userObject');
        if ($user) {
            $function = 'types_list';
            if ($this->authService->canDo($function, $user->id)) {
                $permissions = [
                    'Dashbored' => $this->authService->canDo('Dashbored', $user->id),
                    'users_list' => $this->authService->canDo('users_list', $user->id),
                    'orders_list' => $this->authService->canDo('orders_list', $user->id),
                    'orders_add' => $this->authService->canDo('orders_add', $user->id),
                    'orders_edit' => $this->authService->canDo('orders_edit', $user->id),
                    'orders_delete' => $this->authService->canDo('orders_delete', $user->id),
                    'assignRoleToUser' => $this->authService->canDo('assignRoleToUser', $user->id),
                    'permissions_list' => $this->authService->canDo('permissions_list', $user->id),
                    'permissions_add' => $this->authService->canDo('permissions_add', $user->id),
                    'permissions_edit' => $this->authService->canDo('permissions_edit', $user->id),
                    'permissions_delete' => $this->authService->canDo('permissions_delete', $user->id),
                    'assignPermissionsToRole' => $this->authService->canDo('assignPermissionsToRole', $user->id),
                    'roles_list' => $this->authService->canDo('roles_list', $user->id),
                    'roles_add' => $this->authService->canDo('roles_add', $user->id),
                    'roles_edit' => $this->authService->canDo('roles_edit', $user->id),
                    'roles_delete' => $this->authService->canDo('roles_delete', $user->id),
                    'types_list' => $this->authService->canDo('types_list', $user->id),
                    'types_add' => $this->authService->canDo('types_add', $user->id),
                    'types_edit' => $this->authService->canDo('types_edit', $user->id),
                    'types_delete' => $this->authService->canDo('types_delete', $user->id),
                    'products_list' => $this->authService->canDo('products_list', $user->id),
                    'products_edit' => $this->authService->canDo('products_edit', $user->id),
                    'products_delete' => $this->authService->canDo('products_delete', $user->id)
                ];
                return view('admin/views/types/list', compact('permissions'));
            } else {
                return Redirect::back()->withErrors(['msg', 'The Message']);
            }
        } else {
        }
    }
    public function c_types_list()
    {
        $_SESSION['LAST_ACTIVITY'] = time();
        $user = Session::get('userObject');
        if ($user) {
            $function = 'types_list';
            if ($this->authService->canDo($function, $user->id)) {
                $permissions = [
                    'Dashbored' => $this->authService->canDo('Dashbored', $user->id),
                    'users_list' => $this->authService->canDo('users_list', $user->id),
                    'orders_list' => $this->authService->canDo('orders_list', $user->id),
                    'orders_add' => $this->authService->canDo('orders_add', $user->id),
                    'orders_edit' => $this->authService->canDo('orders_edit', $user->id),
                    'orders_delete' => $this->authService->canDo('orders_delete', $user->id),
                    'assignRoleToUser' => $this->authService->canDo('assignRoleToUser', $user->id),
                    'permissions_list' => $this->authService->canDo('permissions_list', $user->id),
                    'permissions_add' => $this->authService->canDo('permissions_add', $user->id),
                    'permissions_edit' => $this->authService->canDo('permissions_edit', $user->id),
                    'permissions_delete' => $this->authService->canDo('permissions_delete', $user->id),
                    'assignPermissionsToRole' => $this->authService->canDo('assignPermissionsToRole', $user->id),
                    'roles_list' => $this->authService->canDo('roles_list', $user->id),
                    'roles_add' => $this->authService->canDo('roles_add', $user->id),
                    'roles_edit' => $this->authService->canDo('roles_edit', $user->id),
                    'roles_delete' => $this->authService->canDo('roles_delete', $user->id),
                    'types_list' => $this->authService->canDo('types_list', $user->id),
                    'types_add' => $this->authService->canDo('types_add', $user->id),
                    'types_edit' => $this->authService->canDo('types_edit', $user->id),
                    'types_delete' => $this->authService->canDo('types_delete', $user->id),
                    'products_list' => $this->authService->canDo('products_list', $user->id),
                    'products_edit' => $this->authService->canDo('products_edit', $user->id),
                    'products_delete' => $this->authService->canDo('products_delete', $user->id)
                ];
                return view('admin/views/Cproducts/type', compact('permissions'));
            } else {
                return Redirect::back()->withErrors(['msg', 'The Message']);
            }
        } else {
        }
    }

    public function roles_list()
    {
        $_SESSION['LAST_ACTIVITY'] = time();
        $user = Session::get('userObject');

        $function = 'roles_list';
        if ($this->authService->canDo($function, $user->id)) {
            $permissions = [
                'Dashbored' => $this->authService->canDo('Dashbored', $user->id),
                'users_list' => $this->authService->canDo('users_list', $user->id),
                'orders_list' => $this->authService->canDo('orders_list', $user->id),
                'orders_add' => $this->authService->canDo('orders_add', $user->id),
                'orders_edit' => $this->authService->canDo('orders_edit', $user->id),
                'orders_delete' => $this->authService->canDo('orders_delete', $user->id),
                'assignRoleToUser' => $this->authService->canDo('assignRoleToUser', $user->id),
                'permissions_list' => $this->authService->canDo('permissions_list', $user->id),
                'permissions_add' => $this->authService->canDo('permissions_add', $user->id),
                'permissions_edit' => $this->authService->canDo('permissions_edit', $user->id),
                'permissions_delete' => $this->authService->canDo('permissions_delete', $user->id),
                'assignPermissionsToRole' => $this->authService->canDo('assignPermissionsToRole', $user->id),
                'roles_list' => $this->authService->canDo('roles_list', $user->id),
                'roles_add' => $this->authService->canDo('roles_add', $user->id),
                'roles_edit' => $this->authService->canDo('roles_edit', $user->id),
                'roles_delete' => $this->authService->canDo('roles_delete', $user->id),
                'types_list' => $this->authService->canDo('types_list', $user->id),
                'types_add' => $this->authService->canDo('types_add', $user->id),
                'types_edit' => $this->authService->canDo('types_edit', $user->id),
                'types_delete' => $this->authService->canDo('types_delete', $user->id),
                'products_list' => $this->authService->canDo('products_list', $user->id),
                'products_edit' => $this->authService->canDo('products_edit', $user->id),
                'products_delete' => $this->authService->canDo('products_delete', $user->id)
            ];
            return view('admin/views/roles/list', compact('permissions'));
        } else {
            return Redirect::back()->withErrors(['msg', 'The Message']);
        }
    }

    public function role_permissions_list()
    {
        $_SESSION['LAST_ACTIVITY'] = time();
        $user = Session::get('userObject');

        $function = 'assignPermissionsToRole';
        if ($this->authService->canDo($function, $user->id)) {
            $permissions = [
                'Dashbored' => $this->authService->canDo('Dashbored', $user->id),
                'users_list' => $this->authService->canDo('users_list', $user->id),
                'orders_list' => $this->authService->canDo('orders_list', $user->id),
                'orders_add' => $this->authService->canDo('orders_add', $user->id),
                'orders_edit' => $this->authService->canDo('orders_edit', $user->id),
                'orders_delete' => $this->authService->canDo('orders_delete', $user->id),
                'assignRoleToUser' => $this->authService->canDo('assignRoleToUser', $user->id),
                'permissions_list' => $this->authService->canDo('permissions_list', $user->id),
                'permissions_add' => $this->authService->canDo('permissions_add', $user->id),
                'permissions_edit' => $this->authService->canDo('permissions_edit', $user->id),
                'permissions_delete' => $this->authService->canDo('permissions_delete', $user->id),
                'assignPermissionsToRole' => $this->authService->canDo('assignPermissionsToRole', $user->id),
                'roles_list' => $this->authService->canDo('roles_list', $user->id),
                'roles_add' => $this->authService->canDo('roles_add', $user->id),
                'roles_edit' => $this->authService->canDo('roles_edit', $user->id),
                'roles_delete' => $this->authService->canDo('roles_delete', $user->id),
                'types_list' => $this->authService->canDo('types_list', $user->id),
                'types_add' => $this->authService->canDo('types_add', $user->id),
                'types_edit' => $this->authService->canDo('types_edit', $user->id),
                'types_delete' => $this->authService->canDo('types_delete', $user->id),
                'products_list' => $this->authService->canDo('products_list', $user->id),
                'products_edit' => $this->authService->canDo('products_edit', $user->id),
                'products_delete' => $this->authService->canDo('products_delete', $user->id)
            ];
            return view('admin/views/roles/permission_list', compact('permissions'));
        } else {
            return Redirect::back()->withErrors(['msg', 'The Message']);
        }
    }

    public function permissions_list()
    {
        $_SESSION['LAST_ACTIVITY'] = time();
        $user = Session::get('userObject');

        $function = 'permissions_list';
        if ($this->authService->canDo($function, $user->id)) {
            $permissions = [
                'Dashbored' => $this->authService->canDo('Dashbored', $user->id),
                'users_list' => $this->authService->canDo('users_list', $user->id),
                'orders_list' => $this->authService->canDo('orders_list', $user->id),
                'orders_add' => $this->authService->canDo('orders_add', $user->id),
                'orders_edit' => $this->authService->canDo('orders_edit', $user->id),
                'orders_delete' => $this->authService->canDo('orders_delete', $user->id),
                'assignRoleToUser' => $this->authService->canDo('assignRoleToUser', $user->id),
                'permissions_list' => $this->authService->canDo('permissions_list', $user->id),
                'permissions_add' => $this->authService->canDo('permissions_add', $user->id),
                'permissions_edit' => $this->authService->canDo('permissions_edit', $user->id),
                'permissions_delete' => $this->authService->canDo('permissions_delete', $user->id),
                'assignPermissionsToRole' => $this->authService->canDo('assignPermissionsToRole', $user->id),
                'roles_list' => $this->authService->canDo('roles_list', $user->id),
                'roles_add' => $this->authService->canDo('roles_add', $user->id),
                'roles_edit' => $this->authService->canDo('roles_edit', $user->id),
                'roles_delete' => $this->authService->canDo('roles_delete', $user->id),
                'types_list' => $this->authService->canDo('types_list', $user->id),
                'types_add' => $this->authService->canDo('types_add', $user->id),
                'types_edit' => $this->authService->canDo('types_edit', $user->id),
                'types_delete' => $this->authService->canDo('types_delete', $user->id),
                'products_list' => $this->authService->canDo('products_list', $user->id),
                'products_edit' => $this->authService->canDo('products_edit', $user->id),
                'products_delete' => $this->authService->canDo('products_delete', $user->id)
            ];
            return view('admin/views/permissions/list', compact('permissions'));
        } else {
            return Redirect::back()->withErrors(['msg', 'The Message']);
        }
    }

    public function user_roles_list()
    {
        $_SESSION['LAST_ACTIVITY'] = time();
        $user = Session::get('userObject');

        $function = 'assignRoleToUser';
        if ($this->authService->canDo($function, $user->id)) {
            $permissions = [
                'Dashbored' => $this->authService->canDo('Dashbored', $user->id),
                'users_list' => $this->authService->canDo('users_list', $user->id),
                'orders_list' => $this->authService->canDo('orders_list', $user->id),
                'orders_add' => $this->authService->canDo('orders_add', $user->id),
                'orders_edit' => $this->authService->canDo('orders_edit', $user->id),
                'orders_delete' => $this->authService->canDo('orders_delete', $user->id),
                'assignRoleToUser' => $this->authService->canDo('assignRoleToUser', $user->id),
                'permissions_list' => $this->authService->canDo('permissions_list', $user->id),
                'permissions_add' => $this->authService->canDo('permissions_add', $user->id),
                'permissions_edit' => $this->authService->canDo('permissions_edit', $user->id),
                'permissions_delete' => $this->authService->canDo('permissions_delete', $user->id),
                'assignPermissionsToRole' => $this->authService->canDo('assignPermissionsToRole', $user->id),
                'roles_list' => $this->authService->canDo('roles_list', $user->id),
                'roles_add' => $this->authService->canDo('roles_add', $user->id),
                'roles_edit' => $this->authService->canDo('roles_edit', $user->id),
                'roles_delete' => $this->authService->canDo('roles_delete', $user->id),
                'types_list' => $this->authService->canDo('types_list', $user->id),
                'types_add' => $this->authService->canDo('types_add', $user->id),
                'types_edit' => $this->authService->canDo('types_edit', $user->id),
                'types_delete' => $this->authService->canDo('types_delete', $user->id),
                'products_list' => $this->authService->canDo('products_list', $user->id),
                'products_edit' => $this->authService->canDo('products_edit', $user->id),
                'products_delete' => $this->authService->canDo('products_delete', $user->id)
            ];
            return view('admin/views/users/role_list', compact('permissions'));
        } else {
            return Redirect::back()->withErrors(['msg', 'The Message']);
        }
    }

    public function orders_list()
    {
        $_SESSION['LAST_ACTIVITY'] = time();
        $user = Session::get('userObject');

        $function = 'orders_list';
        if ($this->authService->canDo($function, $user->id)) {
            $permissions = [
                'Dashbored' => $this->authService->canDo('Dashbored', $user->id),
                'users_list' => $this->authService->canDo('users_list', $user->id),
                'orders_list' => $this->authService->canDo('orders_list', $user->id),
                'orders_add' => $this->authService->canDo('orders_add', $user->id),
                'orders_edit' => $this->authService->canDo('orders_edit', $user->id),
                'orders_delete' => $this->authService->canDo('orders_delete', $user->id),
                'assignRoleToUser' => $this->authService->canDo('assignRoleToUser', $user->id),
                'permissions_list' => $this->authService->canDo('permissions_list', $user->id),
                'permissions_add' => $this->authService->canDo('permissions_add', $user->id),
                'permissions_edit' => $this->authService->canDo('permissions_edit', $user->id),
                'permissions_delete' => $this->authService->canDo('permissions_delete', $user->id),
                'assignPermissionsToRole' => $this->authService->canDo('assignPermissionsToRole', $user->id),
                'roles_list' => $this->authService->canDo('roles_list', $user->id),
                'roles_add' => $this->authService->canDo('roles_add', $user->id),
                'roles_edit' => $this->authService->canDo('roles_edit', $user->id),
                'roles_delete' => $this->authService->canDo('roles_delete', $user->id),
                'types_list' => $this->authService->canDo('types_list', $user->id),
                'types_add' => $this->authService->canDo('types_add', $user->id),
                'types_edit' => $this->authService->canDo('types_edit', $user->id),
                'types_delete' => $this->authService->canDo('types_delete', $user->id),
                'products_list' => $this->authService->canDo('products_list', $user->id),
                'products_edit' => $this->authService->canDo('products_edit', $user->id),
                'products_delete' => $this->authService->canDo('products_delete', $user->id)
            ];
            return view('admin/views/orders/list', compact('permissions'));
        } else {
            return Redirect::back()->withErrors(['msg', 'The Message']);
        }
    }

    public function orderDetails()
    {
        $_SESSION['LAST_ACTIVITY'] = time();
        $user = Session::get('userObject');

        $function = 'orders_edit';
        if ($this->authService->canDo($function, $user->id)) {
            $permissions = [
                'Dashbored' => $this->authService->canDo('Dashbored', $user->id),
                'users_list' => $this->authService->canDo('users_list', $user->id),
                'orders_list' => $this->authService->canDo('orders_list', $user->id),
                'orders_add' => $this->authService->canDo('orders_add', $user->id),
                'orders_edit' => $this->authService->canDo('orders_edit', $user->id),
                'orders_delete' => $this->authService->canDo('orders_delete', $user->id),
                'assignRoleToUser' => $this->authService->canDo('assignRoleToUser', $user->id),
                'permissions_list' => $this->authService->canDo('permissions_list', $user->id),
                'permissions_add' => $this->authService->canDo('permissions_add', $user->id),
                'permissions_edit' => $this->authService->canDo('permissions_edit', $user->id),
                'permissions_delete' => $this->authService->canDo('permissions_delete', $user->id),
                'assignPermissionsToRole' => $this->authService->canDo('assignPermissionsToRole', $user->id),
                'roles_list' => $this->authService->canDo('roles_list', $user->id),
                'roles_add' => $this->authService->canDo('roles_add', $user->id),
                'roles_edit' => $this->authService->canDo('roles_edit', $user->id),
                'roles_delete' => $this->authService->canDo('roles_delete', $user->id),
                'types_list' => $this->authService->canDo('types_list', $user->id),
                'types_add' => $this->authService->canDo('types_add', $user->id),
                'types_edit' => $this->authService->canDo('types_edit', $user->id),
                'types_delete' => $this->authService->canDo('types_delete', $user->id),
                'products_list' => $this->authService->canDo('products_list', $user->id),
                'products_edit' => $this->authService->canDo('products_edit', $user->id),
                'products_delete' => $this->authService->canDo('products_delete', $user->id)
            ];
            return view('admin/views/orders/edit', compact('permissions'));
        } else {
            return Redirect::back()->withErrors(['msg', 'The Message']);
        }
    }

    public function products_add()
    {
        $_SESSION['LAST_ACTIVITY'] = time();
        $user = Session::get('userObject');

        $function = 'products_add';
        if ($this->authService->canDo($function, $user->id)) {
            $permissions = [
                'Dashbored' => $this->authService->canDo('Dashbored', $user->id),
                'users_list' => $this->authService->canDo('users_list', $user->id),
                'orders_list' => $this->authService->canDo('orders_list', $user->id),
                'orders_add' => $this->authService->canDo('orders_add', $user->id),
                'orders_edit' => $this->authService->canDo('orders_edit', $user->id),
                'orders_delete' => $this->authService->canDo('orders_delete', $user->id),
                'assignRoleToUser' => $this->authService->canDo('assignRoleToUser', $user->id),
                'permissions_list' => $this->authService->canDo('permissions_list', $user->id),
                'permissions_add' => $this->authService->canDo('permissions_add', $user->id),
                'permissions_edit' => $this->authService->canDo('permissions_edit', $user->id),
                'permissions_delete' => $this->authService->canDo('permissions_delete', $user->id),
                'assignPermissionsToRole' => $this->authService->canDo('assignPermissionsToRole', $user->id),
                'roles_list' => $this->authService->canDo('roles_list', $user->id),
                'roles_add' => $this->authService->canDo('roles_add', $user->id),
                'roles_edit' => $this->authService->canDo('roles_edit', $user->id),
                'roles_delete' => $this->authService->canDo('roles_delete', $user->id),
                'types_list' => $this->authService->canDo('types_list', $user->id),
                'types_add' => $this->authService->canDo('types_add', $user->id),
                'types_edit' => $this->authService->canDo('types_edit', $user->id),
                'types_delete' => $this->authService->canDo('types_delete', $user->id),
                'products_list' => $this->authService->canDo('products_list', $user->id),
                'products_edit' => $this->authService->canDo('products_edit', $user->id),
                'products_delete' => $this->authService->canDo('products_delete', $user->id)
            ];
            $Attributes = attributes::all();
            return view('admin/views/products/add', compact('permissions', 'Attributes'));
        } else {
            return Redirect::back()->withErrors(['msg', 'The Message']);
        }
    }
    public function products_edit($id)
    {
        //        $user = Session::get('userObject');
        //
        //        $product = Product::where('id', '=', $id)->first();
        //
        //        $type = Type::where('id', '=', $product->id)->first();
        //        if($type) {
        //            $product->type = $type->name;
        //        }
        //
        //        $category = Category::where('id', '=', $product->category_id)->first();
        //        if($category) {
        //            $product->category = $category->name;
        //        }
        //
        //        $images = File::where('target_name', '=', 'PRODUCTS')->where('target_id', '=', $product->id)->get();
        //
        //        if(sizeof($images) > 0) {
        //            $product->image = $images[0];
        //        }
        //
        //        return view('admin/views/products/edit', compact('product'));

        $_SESSION['LAST_ACTIVITY'] = time();
        $user = Session::get('userObject');

        $function = 'products_edit';
        if ($this->authService->canDo($function, $user->id)) {
            $permissions = [
                'Dashbored' => $this->authService->canDo('Dashbored', $user->id),
                'users_list' => $this->authService->canDo('users_list', $user->id),
                'orders_list' => $this->authService->canDo('orders_list', $user->id),
                'orders_add' => $this->authService->canDo('orders_add', $user->id),
                'orders_edit' => $this->authService->canDo('orders_edit', $user->id),
                'orders_delete' => $this->authService->canDo('orders_delete', $user->id),
                'assignRoleToUser' => $this->authService->canDo('assignRoleToUser', $user->id),
                'permissions_list' => $this->authService->canDo('permissions_list', $user->id),
                'permissions_add' => $this->authService->canDo('permissions_add', $user->id),
                'permissions_edit' => $this->authService->canDo('permissions_edit', $user->id),
                'permissions_delete' => $this->authService->canDo('permissions_delete', $user->id),
                'assignPermissionsToRole' => $this->authService->canDo('assignPermissionsToRole', $user->id),
                'roles_list' => $this->authService->canDo('roles_list', $user->id),
                'roles_add' => $this->authService->canDo('roles_add', $user->id),
                'roles_edit' => $this->authService->canDo('roles_edit', $user->id),
                'roles_delete' => $this->authService->canDo('roles_delete', $user->id),
                'types_list' => $this->authService->canDo('types_list', $user->id),
                'types_add' => $this->authService->canDo('types_add', $user->id),
                'types_edit' => $this->authService->canDo('types_edit', $user->id),
                'types_delete' => $this->authService->canDo('types_delete', $user->id),
                'products_list' => $this->authService->canDo('products_list', $user->id),
                'products_edit' => $this->authService->canDo('products_edit', $user->id),
                'products_delete' => $this->authService->canDo('products_delete', $user->id)
            ];
            $Attributes = attributes::all();
            return view('admin/views/products/edit', compact('permissions', 'Attributes'));
        } else {
            return Redirect::back()->withErrors(['msg', 'The Message']);
        }
    }

    public function catecories_list()
    {
        $_SESSION['LAST_ACTIVITY'] = time();
        $user = Session::get('userObject');

        $permissions = [
            'Dashbored' => $this->authService->canDo('Dashbored', $user->id),
            'users_list' => $this->authService->canDo('users_list', $user->id),
            'orders_list' => $this->authService->canDo('orders_list', $user->id),
            'orders_add' => $this->authService->canDo('orders_add', $user->id),
            'orders_edit' => $this->authService->canDo('orders_edit', $user->id),
            'orders_delete' => $this->authService->canDo('orders_delete', $user->id),
            'assignRoleToUser' => $this->authService->canDo('assignRoleToUser', $user->id),
            'permissions_list' => $this->authService->canDo('permissions_list', $user->id),
            'permissions_add' => $this->authService->canDo('permissions_add', $user->id),
            'permissions_edit' => $this->authService->canDo('permissions_edit', $user->id),
            'permissions_delete' => $this->authService->canDo('permissions_delete', $user->id),
            'assignPermissionsToRole' => $this->authService->canDo('assignPermissionsToRole', $user->id),
            'roles_list' => $this->authService->canDo('roles_list', $user->id),
            'roles_add' => $this->authService->canDo('roles_add', $user->id),
            'roles_edit' => $this->authService->canDo('roles_edit', $user->id),
            'roles_delete' => $this->authService->canDo('roles_delete', $user->id),
            'types_list' => $this->authService->canDo('types_list', $user->id),
            'types_add' => $this->authService->canDo('types_add', $user->id),
            'types_edit' => $this->authService->canDo('types_edit', $user->id),
            'types_delete' => $this->authService->canDo('types_delete', $user->id),
            'products_list' => $this->authService->canDo('products_list', $user->id),
            'products_edit' => $this->authService->canDo('products_edit', $user->id),
            'products_delete' => $this->authService->canDo('products_delete', $user->id)
        ];
        $category = Category::all();
        return view('admin/views/categories/list', compact('permissions'), compact('category'));
    }

    public function categories_edit(Request $request, $id)
    {
        $_SESSION['LAST_ACTIVITY'] = time();
        $user = Session::get('userObject');

        $permissions = [
            'Dashbored' => $this->authService->canDo('Dashbored', $user->id),
            'users_list' => $this->authService->canDo('users_list', $user->id),
            'orders_list' => $this->authService->canDo('orders_list', $user->id),
            'orders_add' => $this->authService->canDo('orders_add', $user->id),
            'orders_edit' => $this->authService->canDo('orders_edit', $user->id),
            'orders_delete' => $this->authService->canDo('orders_delete', $user->id),
            'assignRoleToUser' => $this->authService->canDo('assignRoleToUser', $user->id),
            'permissions_list' => $this->authService->canDo('permissions_list', $user->id),
            'permissions_add' => $this->authService->canDo('permissions_add', $user->id),
            'permissions_edit' => $this->authService->canDo('permissions_edit', $user->id),
            'permissions_delete' => $this->authService->canDo('permissions_delete', $user->id),
            'assignPermissionsToRole' => $this->authService->canDo('assignPermissionsToRole', $user->id),
            'roles_list' => $this->authService->canDo('roles_list', $user->id),
            'roles_add' => $this->authService->canDo('roles_add', $user->id),
            'roles_edit' => $this->authService->canDo('roles_edit', $user->id),
            'roles_delete' => $this->authService->canDo('roles_delete', $user->id),
            'types_list' => $this->authService->canDo('types_list', $user->id),
            'types_add' => $this->authService->canDo('types_add', $user->id),
            'types_edit' => $this->authService->canDo('types_edit', $user->id),
            'types_delete' => $this->authService->canDo('types_delete', $user->id),
            'products_list' => $this->authService->canDo('products_list', $user->id),
            'products_edit' => $this->authService->canDo('products_edit', $user->id),
            'products_delete' => $this->authService->canDo('products_delete', $user->id)
        ];
        $category = Category::find($id);
        return view('admin/views/categories/edit', compact('permissions', 'category'));
    }
    public function categories_update(Request $request, $id)
    {
        $category = Category::find($id);
        $category->name = $request->name;
        $category->ar_name = $request->ar_name;
        $category->description = $request->description;
        $category->ar_description = $request->ar_description;
        //        dd($request->all());
        if ($request->file('cate_photo')) {
            $files = $request->file('cate_photo');
            $destinationPath = '/images_cate/'; // upload path
            $profileImage = time() . "." . $files->clientExtension();
            $files->move(public_path($destinationPath), $profileImage);
            $category->image = "images_cate/" . $profileImage;
        }
        if ($request->file('cate_svg')) {
            $files = $request->file('cate_svg');
            $destinationPath = '/svg_cate/'; // upload path
            $profileImage = time() . "." . $files->clientExtension();
            $files->move(public_path($destinationPath), $profileImage);
            $category->svg = "svg_cate/" . $profileImage;
        }
        $category->save();
        $categorys = "Category updated successfully";
        return redirect('/admin/categories/list')->with($categorys);
    }
    public function categories_add(Request $request)
    {
        $_SESSION['LAST_ACTIVITY'] = time();
        $user = Session::get('userObject');

        $permissions = [
            'Dashbored' => $this->authService->canDo('Dashbored', $user->id),
            'users_list' => $this->authService->canDo('users_list', $user->id),
            'orders_list' => $this->authService->canDo('orders_list', $user->id),
            'orders_add' => $this->authService->canDo('orders_add', $user->id),
            'orders_edit' => $this->authService->canDo('orders_edit', $user->id),
            'orders_delete' => $this->authService->canDo('orders_delete', $user->id),
            'assignRoleToUser' => $this->authService->canDo('assignRoleToUser', $user->id),
            'permissions_list' => $this->authService->canDo('permissions_list', $user->id),
            'permissions_add' => $this->authService->canDo('permissions_add', $user->id),
            'permissions_edit' => $this->authService->canDo('permissions_edit', $user->id),
            'permissions_delete' => $this->authService->canDo('permissions_delete', $user->id),
            'assignPermissionsToRole' => $this->authService->canDo('assignPermissionsToRole', $user->id),
            'roles_list' => $this->authService->canDo('roles_list', $user->id),
            'roles_add' => $this->authService->canDo('roles_add', $user->id),
            'roles_edit' => $this->authService->canDo('roles_edit', $user->id),
            'roles_delete' => $this->authService->canDo('roles_delete', $user->id),
            'types_list' => $this->authService->canDo('types_list', $user->id),
            'types_add' => $this->authService->canDo('types_add', $user->id),
            'types_edit' => $this->authService->canDo('types_edit', $user->id),
            'types_delete' => $this->authService->canDo('types_delete', $user->id),
            'products_list' => $this->authService->canDo('products_list', $user->id),
            'products_edit' => $this->authService->canDo('products_edit', $user->id),
            'products_delete' => $this->authService->canDo('products_delete', $user->id)
        ];
        return view('admin/views/categories/add', compact('permissions'));
    }
    public function categories_add_post(Request $request)
    {
        $_SESSION['LAST_ACTIVITY'] = time();
        $user = Session::get('userObject');

        $permissions = [
            'Dashbored' => $this->authService->canDo('Dashbored', $user->id),
            'users_list' => $this->authService->canDo('users_list', $user->id),
            'orders_list' => $this->authService->canDo('orders_list', $user->id),
            'orders_add' => $this->authService->canDo('orders_add', $user->id),
            'orders_edit' => $this->authService->canDo('orders_edit', $user->id),
            'orders_delete' => $this->authService->canDo('orders_delete', $user->id),
            'assignRoleToUser' => $this->authService->canDo('assignRoleToUser', $user->id),
            'permissions_list' => $this->authService->canDo('permissions_list', $user->id),
            'permissions_add' => $this->authService->canDo('permissions_add', $user->id),
            'permissions_edit' => $this->authService->canDo('permissions_edit', $user->id),
            'permissions_delete' => $this->authService->canDo('permissions_delete', $user->id),
            'assignPermissionsToRole' => $this->authService->canDo('assignPermissionsToRole', $user->id),
            'roles_list' => $this->authService->canDo('roles_list', $user->id),
            'roles_add' => $this->authService->canDo('roles_add', $user->id),
            'roles_edit' => $this->authService->canDo('roles_edit', $user->id),
            'roles_delete' => $this->authService->canDo('roles_delete', $user->id),
            'types_list' => $this->authService->canDo('types_list', $user->id),
            'types_add' => $this->authService->canDo('types_add', $user->id),
            'types_edit' => $this->authService->canDo('types_edit', $user->id),
            'types_delete' => $this->authService->canDo('types_delete', $user->id),
            'products_list' => $this->authService->canDo('products_list', $user->id),
            'products_edit' => $this->authService->canDo('products_edit', $user->id),
            'products_delete' => $this->authService->canDo('products_delete', $user->id)
        ];
        $category = new Category;
        $category->name = $request->name;
        $category->ar_name = $request->ar_name;
        $category->description = $request->description;
        $category->ar_description = $request->ar_description;
        if ($request->file('cate_photo')) {
            $files = $request->file('cate_photo');
            $destinationPath = '/images_cate/'; // upload path
            $profileImage = time() . "." . $files->clientExtension();
            $files->move(public_path($destinationPath), $profileImage);
            $category->image = "images_cate/" . $profileImage;
        }
        if ($request->file('cate_svg')) {
            $files = $request->file('cate_svg');
            $destinationPath = '/svg_cate/'; // upload path
            $profileImage = time() . "." . $files->clientExtension();
            $files->move(public_path($destinationPath), $profileImage);
            $category->svg = "svg_cate/" . $profileImage;
        }
        $category->save();
        return redirect('/admin/categories/list');
    }

    public function adminDemo()
    {
        $user = Session::get('userObject');
        return view('admin/adminLte');
    }

    public function configList()
    {
        $user = Session::get('userObject');
        return view('admin/views/config');
    }


    public function config_header_add()
    {
        $user = Session::get('userObject');
        return view('admin/views/config/header/add');
    }
    public function config_header_list()
    {
        $user = Session::get('userObject');
        return view('admin/views/config/header/list');
    }

    //upload Images For Products
    public function ajaxImageUploadPost(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'product_id' => 'required',
            'title' => 'required',
            'image' => 'required|image|mimes:jpeg,png,jpg,gif,svg|max:2048',
        ]);
        if ($validator->passes()) {
            $images_products = new File();
            if ($request->file('image')) {
                $files = $request->file('image');
                $destinationPath = '/images_products/'; // upload path
                $profileImage = time() . "." . $files->clientExtension();
                $files->move(public_path($destinationPath), $profileImage);
                $images_products->path = "images_products/" . $profileImage;
            }
            $images_products->target_id = $request->product_id;
            $images_products->target_name = 'PRODUCTS_IMAGES';
            $images_products->save();
            return response()->json(['success' => 'done']);
        }
        return response()->json(['error' => $validator->errors()->all()]);
    }

    public function ajaxImageGet(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'product_id' => 'required'
        ]);
        if ($validator->passes()) {
            $images_products = File::all()->where('target_id', $request->product_id)->where('target_name', '=', 'PRODUCTS_IMAGES');
            return response()->json(['data' => $images_products]);
        }
        return response()->json(['error' => $validator->errors()->all()]);
    }

    public function ajaxImageDelete(Request $request)
    {
        $images_products = File::find($request->image_id);
        if (FilesSubhe::exists("./" . $images_products->path)) {
            FilesSubhe::delete("./" . $images_products->path);
            File::destroy($request->image_id);
        }
        return response()->json(['success' => 'done']);
    }
    public function attributes_list()
    {
        $_SESSION['LAST_ACTIVITY'] = time();
        $user = Session::get('userObject');
        $roles = $this->authService->getUserRoles($user->id);

        $function = 'types_list';
        if ($this->authService->canDo($function, $user->id)) {
            $permissions = [
                'Dashbored' => $this->authService->canDo('Dashbored', $user->id),
                'attributes_list' => $this->authService->canDo('attributes_list', $user->id),
                'users_list' => $this->authService->canDo('users_list', $user->id),
                'orders_list' => $this->authService->canDo('orders_list', $user->id),
                'orders_add' => $this->authService->canDo('orders_add', $user->id),
                'orders_edit' => $this->authService->canDo('orders_edit', $user->id),
                'orders_delete' => $this->authService->canDo('orders_delete', $user->id),
                'assignRoleToUser' => $this->authService->canDo('assignRoleToUser', $user->id),
                'permissions_list' => $this->authService->canDo('permissions_list', $user->id),
                'permissions_add' => $this->authService->canDo('permissions_add', $user->id),
                'permissions_edit' => $this->authService->canDo('permissions_edit', $user->id),
                'permissions_delete' => $this->authService->canDo('permissions_delete', $user->id),
                'assignPermissionsToRole' => $this->authService->canDo('assignPermissionsToRole', $user->id),
                'roles_list' => $this->authService->canDo('roles_list', $user->id),
                'roles_add' => $this->authService->canDo('roles_add', $user->id),
                'roles_edit' => $this->authService->canDo('roles_edit', $user->id),
                'roles_delete' => $this->authService->canDo('roles_delete', $user->id),
                'types_list' => $this->authService->canDo('types_list', $user->id),
                'types_add' => $this->authService->canDo('types_add', $user->id),
                'types_edit' => $this->authService->canDo('types_edit', $user->id),
                'types_delete' => $this->authService->canDo('types_delete', $user->id),
                'products_list' => $this->authService->canDo('products_list', $user->id),
                'products_edit' => $this->authService->canDo('products_edit', $user->id),
                'products_delete' => $this->authService->canDo('products_delete', $user->id)
            ];

            return view('admin/views/attributes/list', compact('permissions'));
        }
    }
    public function notification()
    {
        $_SESSION['LAST_ACTIVITY'] = time();
        $user = Session::get('userObject');
        $roles = $this->authService->getUserRoles($user->id);
        $permissions = [
            'Dashbored' => $this->authService->canDo('Dashbored', $user->id),
            'users_list' => $this->authService->canDo('users_list', $user->id),
            'orders_list' => $this->authService->canDo('orders_list', $user->id),
            'orders_add' => $this->authService->canDo('orders_add', $user->id),
            'orders_edit' => $this->authService->canDo('orders_edit', $user->id),
            'orders_delete' => $this->authService->canDo('orders_delete', $user->id),
            'assignRoleToUser' => $this->authService->canDo('assignRoleToUser', $user->id),
            'permissions_list' => $this->authService->canDo('permissions_list', $user->id),
            'permissions_add' => $this->authService->canDo('permissions_add', $user->id),
            'permissions_edit' => $this->authService->canDo('permissions_edit', $user->id),
            'permissions_delete' => $this->authService->canDo('permissions_delete', $user->id),
            'assignPermissionsToRole' => $this->authService->canDo('assignPermissionsToRole', $user->id),
            'roles_list' => $this->authService->canDo('roles_list', $user->id),
            'roles_add' => $this->authService->canDo('roles_add', $user->id),
            'roles_edit' => $this->authService->canDo('roles_edit', $user->id),
            'roles_delete' => $this->authService->canDo('roles_delete', $user->id),
            'types_list' => $this->authService->canDo('types_list', $user->id),
            'types_add' => $this->authService->canDo('types_add', $user->id),
            'types_edit' => $this->authService->canDo('types_edit', $user->id),
            'types_delete' => $this->authService->canDo('types_delete', $user->id),
            'products_list' => $this->authService->canDo('products_list', $user->id),
            'products_edit' => $this->authService->canDo('products_edit', $user->id),
            'products_delete' => $this->authService->canDo('products_delete', $user->id)
        ];
        return view('admin.views.notification.list', compact('permissions'));
    }

    public function coupon()
    {
        $_SESSION['LAST_ACTIVITY'] = time();
        $user = Session::get('userObject');
        $roles = $this->authService->getUserRoles($user->id);
        $permissions = [
            'Dashbored' => $this->authService->canDo('Dashbored', $user->id),
            'users_list' => $this->authService->canDo('users_list', $user->id),
            'orders_list' => $this->authService->canDo('orders_list', $user->id),
            'orders_add' => $this->authService->canDo('orders_add', $user->id),
            'orders_edit' => $this->authService->canDo('orders_edit', $user->id),
            'orders_delete' => $this->authService->canDo('orders_delete', $user->id),
            'assignRoleToUser' => $this->authService->canDo('assignRoleToUser', $user->id),
            'permissions_list' => $this->authService->canDo('permissions_list', $user->id),
            'permissions_add' => $this->authService->canDo('permissions_add', $user->id),
            'permissions_edit' => $this->authService->canDo('permissions_edit', $user->id),
            'permissions_delete' => $this->authService->canDo('permissions_delete', $user->id),
            'assignPermissionsToRole' => $this->authService->canDo('assignPermissionsToRole', $user->id),
            'roles_list' => $this->authService->canDo('roles_list', $user->id),
            'roles_add' => $this->authService->canDo('roles_add', $user->id),
            'roles_edit' => $this->authService->canDo('roles_edit', $user->id),
            'roles_delete' => $this->authService->canDo('roles_delete', $user->id),
            'types_list' => $this->authService->canDo('types_list', $user->id),
            'types_add' => $this->authService->canDo('types_add', $user->id),
            'types_edit' => $this->authService->canDo('types_edit', $user->id),
            'types_delete' => $this->authService->canDo('types_delete', $user->id),
            'products_list' => $this->authService->canDo('products_list', $user->id),
            'products_edit' => $this->authService->canDo('products_edit', $user->id),
            'products_delete' => $this->authService->canDo('products_delete', $user->id)
        ];
        return view('admin.views.coupon.list', compact('permissions'));
    }
    public function accounting()
    {
        $_SESSION['LAST_ACTIVITY'] = time();
        $user = Session::get('userObject');
        $roles = $this->authService->getUserRoles($user->id);
        $permissions = [
            'Dashbored' => $this->authService->canDo('Dashbored', $user->id),
            'users_list' => $this->authService->canDo('users_list', $user->id),
            'orders_list' => $this->authService->canDo('orders_list', $user->id),
            'orders_add' => $this->authService->canDo('orders_add', $user->id),
            'orders_edit' => $this->authService->canDo('orders_edit', $user->id),
            'orders_delete' => $this->authService->canDo('orders_delete', $user->id),
            'assignRoleToUser' => $this->authService->canDo('assignRoleToUser', $user->id),
            'permissions_list' => $this->authService->canDo('permissions_list', $user->id),
            'permissions_add' => $this->authService->canDo('permissions_add', $user->id),
            'permissions_edit' => $this->authService->canDo('permissions_edit', $user->id),
            'permissions_delete' => $this->authService->canDo('permissions_delete', $user->id),
            'assignPermissionsToRole' => $this->authService->canDo('assignPermissionsToRole', $user->id),
            'roles_list' => $this->authService->canDo('roles_list', $user->id),
            'roles_add' => $this->authService->canDo('roles_add', $user->id),
            'roles_edit' => $this->authService->canDo('roles_edit', $user->id),
            'roles_delete' => $this->authService->canDo('roles_delete', $user->id),
            'types_list' => $this->authService->canDo('types_list', $user->id),
            'types_add' => $this->authService->canDo('types_add', $user->id),
            'types_edit' => $this->authService->canDo('types_edit', $user->id),
            'types_delete' => $this->authService->canDo('types_delete', $user->id),
            'products_list' => $this->authService->canDo('products_list', $user->id),
            'products_edit' => $this->authService->canDo('products_edit', $user->id),
            'products_delete' => $this->authService->canDo('products_delete', $user->id)
        ];
        $products = Product::all();
        return view('admin/views/products/accounting', compact('permissions', 'products'));
    }







    public function Cproducts_list(Request $request)
    {
        $_SESSION['LAST_ACTIVITY'] = time();
        $user = Session::get('userObject');
        $roles = $this->authService->getUserRoles($user->id);

        $function = 'products_list';
        if ($this->authService->canDo($function, $user->id)) {
            $permissions = [
                'Dashbored' => $this->authService->canDo('Dashbored', $user->id),
                'users_list' => $this->authService->canDo('users_list', $user->id),
                'orders_list' => $this->authService->canDo('orders_list', $user->id),
                'orders_add' => $this->authService->canDo('orders_add', $user->id),
                'orders_edit' => $this->authService->canDo('orders_edit', $user->id),
                'orders_delete' => $this->authService->canDo('orders_delete', $user->id),
                'assignRoleToUser' => $this->authService->canDo('assignRoleToUser', $user->id),
                'permissions_list' => $this->authService->canDo('permissions_list', $user->id),
                'permissions_add' => $this->authService->canDo('permissions_add', $user->id),
                'permissions_edit' => $this->authService->canDo('permissions_edit', $user->id),
                'permissions_delete' => $this->authService->canDo('permissions_delete', $user->id),
                'assignPermissionsToRole' => $this->authService->canDo('assignPermissionsToRole', $user->id),
                'roles_list' => $this->authService->canDo('roles_list', $user->id),
                'roles_add' => $this->authService->canDo('roles_add', $user->id),
                'roles_edit' => $this->authService->canDo('roles_edit', $user->id),
                'roles_delete' => $this->authService->canDo('roles_delete', $user->id),
                'types_list' => $this->authService->canDo('types_list', $user->id),
                'types_add' => $this->authService->canDo('types_add', $user->id),
                'types_edit' => $this->authService->canDo('types_edit', $user->id),
                'types_delete' => $this->authService->canDo('types_delete', $user->id),
                'products_list' => $this->authService->canDo('products_list', $user->id),
                'products_edit' => $this->authService->canDo('products_edit', $user->id),
                'products_delete' => $this->authService->canDo('products_delete', $user->id)
            ];

            $products = Cproduct::all();
            // $this->changePrice($products, $request->ip());
            return view('admin/views/Cproducts/list', compact('permissions'), compact('products'));
        } else {
            return Redirect::back()->withErrors(['msg', 'The Message']);
        }
    }
    public function Cproducts_add()
    {
        $_SESSION['LAST_ACTIVITY'] = time();
        $user = Session::get('userObject');

        $function = 'products_add';
        if ($this->authService->canDo($function, $user->id)) {
            $permissions = [
                'Dashbored' => $this->authService->canDo('Dashbored', $user->id),
                'users_list' => $this->authService->canDo('users_list', $user->id),
                'orders_list' => $this->authService->canDo('orders_list', $user->id),
                'orders_add' => $this->authService->canDo('orders_add', $user->id),
                'orders_edit' => $this->authService->canDo('orders_edit', $user->id),
                'orders_delete' => $this->authService->canDo('orders_delete', $user->id),
                'assignRoleToUser' => $this->authService->canDo('assignRoleToUser', $user->id),
                'permissions_list' => $this->authService->canDo('permissions_list', $user->id),
                'permissions_add' => $this->authService->canDo('permissions_add', $user->id),
                'permissions_edit' => $this->authService->canDo('permissions_edit', $user->id),
                'permissions_delete' => $this->authService->canDo('permissions_delete', $user->id),
                'assignPermissionsToRole' => $this->authService->canDo('assignPermissionsToRole', $user->id),
                'roles_list' => $this->authService->canDo('roles_list', $user->id),
                'roles_add' => $this->authService->canDo('roles_add', $user->id),
                'roles_edit' => $this->authService->canDo('roles_edit', $user->id),
                'roles_delete' => $this->authService->canDo('roles_delete', $user->id),
                'types_list' => $this->authService->canDo('types_list', $user->id),
                'types_add' => $this->authService->canDo('types_add', $user->id),
                'types_edit' => $this->authService->canDo('types_edit', $user->id),
                'types_delete' => $this->authService->canDo('types_delete', $user->id),
                'products_list' => $this->authService->canDo('products_list', $user->id),
                'products_edit' => $this->authService->canDo('products_edit', $user->id),
                'products_delete' => $this->authService->canDo('products_delete', $user->id)
            ];
            // $Attributes = attributes::all();
            return view('admin/views/Cproducts/add', compact('permissions'));
        } else {
            return Redirect::back()->withErrors(['msg', 'The Message']);
        }
    }
    public function Cproducts_edit($id)
    {
        $_SESSION['LAST_ACTIVITY'] = time();
        $user = Session::get('userObject');

        $function = 'products_edit';
        if ($this->authService->canDo($function, $user->id)) {
            $permissions = [
                'Dashbored' => $this->authService->canDo('Dashbored', $user->id),
                'users_list' => $this->authService->canDo('users_list', $user->id),
                'orders_list' => $this->authService->canDo('orders_list', $user->id),
                'orders_add' => $this->authService->canDo('orders_add', $user->id),
                'orders_edit' => $this->authService->canDo('orders_edit', $user->id),
                'orders_delete' => $this->authService->canDo('orders_delete', $user->id),
                'assignRoleToUser' => $this->authService->canDo('assignRoleToUser', $user->id),
                'permissions_list' => $this->authService->canDo('permissions_list', $user->id),
                'permissions_add' => $this->authService->canDo('permissions_add', $user->id),
                'permissions_edit' => $this->authService->canDo('permissions_edit', $user->id),
                'permissions_delete' => $this->authService->canDo('permissions_delete', $user->id),
                'assignPermissionsToRole' => $this->authService->canDo('assignPermissionsToRole', $user->id),
                'roles_list' => $this->authService->canDo('roles_list', $user->id),
                'roles_add' => $this->authService->canDo('roles_add', $user->id),
                'roles_edit' => $this->authService->canDo('roles_edit', $user->id),
                'roles_delete' => $this->authService->canDo('roles_delete', $user->id),
                'types_list' => $this->authService->canDo('types_list', $user->id),
                'types_add' => $this->authService->canDo('types_add', $user->id),
                'types_edit' => $this->authService->canDo('types_edit', $user->id),
                'types_delete' => $this->authService->canDo('types_delete', $user->id),
                'products_list' => $this->authService->canDo('products_list', $user->id),
                'products_edit' => $this->authService->canDo('products_edit', $user->id),
                'products_delete' => $this->authService->canDo('products_delete', $user->id)
            ];
            // $Attributes = attributes::all();
            return view('admin/views/Cproducts/edit', compact('permissions'));
        } else {
            return Redirect::back()->withErrors(['msg', 'The Message']);
        }
    }
}
