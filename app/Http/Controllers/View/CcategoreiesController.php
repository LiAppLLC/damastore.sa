<?php

namespace App\Http\Controllers\View;

use App\Ccategory;
use App\Http\Services\AuthService;
use App;
use Illuminate\Support\Facades\Validator;
use Session;
use App\traits\ChangingPrice;
use App\Product;
use App\Type;
use App\Category;
use App\File;
use Illuminate\Support\Facades\File as FilesSubhe;
use Illuminate\Support\Facades\Storage;
use App\Http\Controllers\Controller;
use App\Providers\RouteServiceProvider;
use Illuminate\Foundation\Auth\ConfirmsPasswords;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;
use App\attributes;


class CcategoreiesController extends Controller
{
    use ChangingPrice;

    protected $authService;

    public function __construct(AuthService $authService)
    {
        $this->authService = $authService;
        if (!session('lang')) {

            Session::put('lang', 'en');
        };
    }
    public function catecories_list()
    {
        $_SESSION['LAST_ACTIVITY'] = time();
        $user = Session::get('userObject');

        $permissions = [
            'Dashbored' => $this->authService->canDo('Dashbored', $user->id),
            'users_list' => $this->authService->canDo('users_list', $user->id),
            'orders_list' => $this->authService->canDo('orders_list', $user->id),
            'orders_add' => $this->authService->canDo('orders_add', $user->id),
            'orders_edit' => $this->authService->canDo('orders_edit', $user->id),
            'orders_delete' => $this->authService->canDo('orders_delete', $user->id),
            'assignRoleToUser' => $this->authService->canDo('assignRoleToUser', $user->id),
            'permissions_list' => $this->authService->canDo('permissions_list', $user->id),
            'permissions_add' => $this->authService->canDo('permissions_add', $user->id),
            'permissions_edit' => $this->authService->canDo('permissions_edit', $user->id),
            'permissions_delete' => $this->authService->canDo('permissions_delete', $user->id),
            'assignPermissionsToRole' => $this->authService->canDo('assignPermissionsToRole', $user->id),
            'roles_list' => $this->authService->canDo('roles_list', $user->id),
            'roles_add' => $this->authService->canDo('roles_add', $user->id),
            'roles_edit' => $this->authService->canDo('roles_edit', $user->id),
            'roles_delete' => $this->authService->canDo('roles_delete', $user->id),
            'types_list' => $this->authService->canDo('types_list', $user->id),
            'types_add' => $this->authService->canDo('types_add', $user->id),
            'types_edit' => $this->authService->canDo('types_edit', $user->id),
            'types_delete' => $this->authService->canDo('types_delete', $user->id),
            'products_list' => $this->authService->canDo('products_list', $user->id),
            'products_edit' => $this->authService->canDo('products_edit', $user->id),
            'products_delete' => $this->authService->canDo('products_delete', $user->id)
        ];
        $category = Ccategory::all();
        return view('admin/views/Cproducts/category/list', compact('permissions'), compact('category'));
    }
    public function categories_edit(Request $request, $id)
    {
        $_SESSION['LAST_ACTIVITY'] = time();
        $user = Session::get('userObject');

        $permissions = [
            'Dashbored' => $this->authService->canDo('Dashbored', $user->id),
            'users_list' => $this->authService->canDo('users_list', $user->id),
            'orders_list' => $this->authService->canDo('orders_list', $user->id),
            'orders_add' => $this->authService->canDo('orders_add', $user->id),
            'orders_edit' => $this->authService->canDo('orders_edit', $user->id),
            'orders_delete' => $this->authService->canDo('orders_delete', $user->id),
            'assignRoleToUser' => $this->authService->canDo('assignRoleToUser', $user->id),
            'permissions_list' => $this->authService->canDo('permissions_list', $user->id),
            'permissions_add' => $this->authService->canDo('permissions_add', $user->id),
            'permissions_edit' => $this->authService->canDo('permissions_edit', $user->id),
            'permissions_delete' => $this->authService->canDo('permissions_delete', $user->id),
            'assignPermissionsToRole' => $this->authService->canDo('assignPermissionsToRole', $user->id),
            'roles_list' => $this->authService->canDo('roles_list', $user->id),
            'roles_add' => $this->authService->canDo('roles_add', $user->id),
            'roles_edit' => $this->authService->canDo('roles_edit', $user->id),
            'roles_delete' => $this->authService->canDo('roles_delete', $user->id),
            'types_list' => $this->authService->canDo('types_list', $user->id),
            'types_add' => $this->authService->canDo('types_add', $user->id),
            'types_edit' => $this->authService->canDo('types_edit', $user->id),
            'types_delete' => $this->authService->canDo('types_delete', $user->id),
            'products_list' => $this->authService->canDo('products_list', $user->id),
            'products_edit' => $this->authService->canDo('products_edit', $user->id),
            'products_delete' => $this->authService->canDo('products_delete', $user->id)
        ];
        $category = Ccategory::find($id);
        return view('admin/views/Cproducts/category/edit', compact('permissions', 'category'));
    }
    public function categories_update(Request $request, $id)
    {
        $category = Ccategory::find($id);
        $category->name = $request->name;
        $category->ar_name = $request->ar_name;
        $category->description = $request->description;
        $category->ar_description = $request->ar_description;
        //        dd($request->all());
        if ($request->file('cate_photo')) {
            $files = $request->file('cate_photo');
            $destinationPath = '/c_images_cate/'; // upload path
            $profileImage = time() . "." . $files->clientExtension();
            $files->move(public_path($destinationPath), $profileImage);
            $category->image = "c_images_cate/" . $profileImage;
        }
        if ($request->file('cate_svg')) {
            $files = $request->file('cate_svg');
            $destinationPath = '/c_svg_cate/'; // upload path
            $profileImage = time() . "." . $files->clientExtension();
            $files->move(public_path($destinationPath), $profileImage);
            $category->svg = "c_svg_cate/" . $profileImage;
        }
        $category->save();
        $categorys = "Category updated successfully";
        return redirect('/admin/Ccategories/list')->with($categorys);
    }
    public function categories_add(Request $request)
    {
        $_SESSION['LAST_ACTIVITY'] = time();
        $user = Session::get('userObject');

        $permissions = [
            'Dashbored' => $this->authService->canDo('Dashbored', $user->id),
            'users_list' => $this->authService->canDo('users_list', $user->id),
            'orders_list' => $this->authService->canDo('orders_list', $user->id),
            'orders_add' => $this->authService->canDo('orders_add', $user->id),
            'orders_edit' => $this->authService->canDo('orders_edit', $user->id),
            'orders_delete' => $this->authService->canDo('orders_delete', $user->id),
            'assignRoleToUser' => $this->authService->canDo('assignRoleToUser', $user->id),
            'permissions_list' => $this->authService->canDo('permissions_list', $user->id),
            'permissions_add' => $this->authService->canDo('permissions_add', $user->id),
            'permissions_edit' => $this->authService->canDo('permissions_edit', $user->id),
            'permissions_delete' => $this->authService->canDo('permissions_delete', $user->id),
            'assignPermissionsToRole' => $this->authService->canDo('assignPermissionsToRole', $user->id),
            'roles_list' => $this->authService->canDo('roles_list', $user->id),
            'roles_add' => $this->authService->canDo('roles_add', $user->id),
            'roles_edit' => $this->authService->canDo('roles_edit', $user->id),
            'roles_delete' => $this->authService->canDo('roles_delete', $user->id),
            'types_list' => $this->authService->canDo('types_list', $user->id),
            'types_add' => $this->authService->canDo('types_add', $user->id),
            'types_edit' => $this->authService->canDo('types_edit', $user->id),
            'types_delete' => $this->authService->canDo('types_delete', $user->id),
            'products_list' => $this->authService->canDo('products_list', $user->id),
            'products_edit' => $this->authService->canDo('products_edit', $user->id),
            'products_delete' => $this->authService->canDo('products_delete', $user->id)
        ];
        return view('admin/views/Cproducts/category/add', compact('permissions'));
    }
    public function categories_add_post(Request $request)
    {
        $_SESSION['LAST_ACTIVITY'] = time();
        $user = Session::get('userObject');

        $permissions = [
            'Dashbored' => $this->authService->canDo('Dashbored', $user->id),
            'users_list' => $this->authService->canDo('users_list', $user->id),
            'orders_list' => $this->authService->canDo('orders_list', $user->id),
            'orders_add' => $this->authService->canDo('orders_add', $user->id),
            'orders_edit' => $this->authService->canDo('orders_edit', $user->id),
            'orders_delete' => $this->authService->canDo('orders_delete', $user->id),
            'assignRoleToUser' => $this->authService->canDo('assignRoleToUser', $user->id),
            'permissions_list' => $this->authService->canDo('permissions_list', $user->id),
            'permissions_add' => $this->authService->canDo('permissions_add', $user->id),
            'permissions_edit' => $this->authService->canDo('permissions_edit', $user->id),
            'permissions_delete' => $this->authService->canDo('permissions_delete', $user->id),
            'assignPermissionsToRole' => $this->authService->canDo('assignPermissionsToRole', $user->id),
            'roles_list' => $this->authService->canDo('roles_list', $user->id),
            'roles_add' => $this->authService->canDo('roles_add', $user->id),
            'roles_edit' => $this->authService->canDo('roles_edit', $user->id),
            'roles_delete' => $this->authService->canDo('roles_delete', $user->id),
            'types_list' => $this->authService->canDo('types_list', $user->id),
            'types_add' => $this->authService->canDo('types_add', $user->id),
            'types_edit' => $this->authService->canDo('types_edit', $user->id),
            'types_delete' => $this->authService->canDo('types_delete', $user->id),
            'products_list' => $this->authService->canDo('products_list', $user->id),
            'products_edit' => $this->authService->canDo('products_edit', $user->id),
            'products_delete' => $this->authService->canDo('products_delete', $user->id)
        ];
        $category = new Ccategory;
        $category->name = $request->name;
        $category->ar_name = $request->ar_name;
        $category->description = $request->description;
        $category->ar_description = $request->ar_description;
        if ($request->file('cate_photo')) {
            $files = $request->file('cate_photo');
            $destinationPath = '/c_images_cate/'; // upload path
            $profileImage = time() . "." . $files->clientExtension();
            $files->move(public_path($destinationPath), $profileImage);
            $category->image = "c_images_cate/" . $profileImage;
        }
        if ($request->file('cate_svg')) {
            $files = $request->file('cate_svg');
            $destinationPath = '/c_svg_cate/'; // upload path
            $profileImage = time() . "." . $files->clientExtension();
            $files->move(public_path($destinationPath), $profileImage);
            $category->svg = "c_svg_cate/" . $profileImage;
        }
        $category->save();
        return redirect('/admin/Ccategories/list');
    }
}
