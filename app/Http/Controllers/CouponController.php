<?php

namespace App\Http\Controllers;

use App\coupon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class CouponController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $coupon = coupon::all();
        return response()->json(array('coupon', $coupon));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'coupon' => 'required',
            'coupon_value' => 'required',
            'coupon_start' => 'required',
            'coupon_end' => 'required',
        ]);

        if ($validator->fails()) {
            return response()->json(array('Validation Error.', $validator->errors()));
        }
        $coupon = new coupon();
        $coupon->coupon = $request->coupon;
        $coupon->value = $request->coupon_value;
        $coupon->start = $request->coupon_start;
        $coupon->end = $request->coupon_end;
        $coupon->save();
        return response()->json(['successes' => 'done']);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        coupon::destroy($id);
    }
}
