<?php

namespace App\Http\Controllers;

use App\Http\Services\UserRoleService;
use App\Role;
use App\UsersRole;
use App\Http\Utility;
use Illuminate\Http\Request;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\Validator;

class UserRoleController extends Controller
{

    protected $userroleservice;
    public function __construct(UserRoleService $userRoleService)
    {
        $this->userroleservice = $userRoleService;
        $this->middleware('ApiAuth:api');
    }

    public function addMulti(Request $request)
    {
        $temp = explode(',', $request->userRoles);
        $arr = [];
        foreach ($temp as $item){
            array_push($arr, ['role_id' => (int)$item]);
        }
        $request->userRoles = $arr;

        $data = $request->userRoles;
        $user_id = $request->user_id;

        $userRole=$this->userroleservice->addMulti($user_id,$data);

        return Utility::ToApi("User role added successfully",true,  $userRole,"Ok",201);

    }

    public function getByUser(Request $request,$pageIndex,$pageSize)
    {
        $validator = Validator::make($request->all(), [
            'user_id' =>['required']
        ]);

        if ($validator->fails()) {

            return   Utility::ToApi("missing fields",false, ["validator"=>$validator->messages()->first()],"BadRequest",400);

        }

        $data=$request->only('user_id');
        list($userRole,$total)=$this->userroleservice->getByUser($data['user_id'],$pageIndex,$pageSize);
        if($userRole!=null)
        return Utility::ToPageApi("User role return successfully",true,$userRole,$total,"Ok",200);
    }

    public function deleteMulti(Request $request)
    {
        $temp = explode(',', $request->userRoles);
        $arr = [];
        foreach ($temp as $item){
            array_push($arr, ['role_id' => (int)$item]);
        }
        $request->userRoles = $arr;

        $data = $request->userRoles;
        $user_id = $request->user_id;

       $userRole= $this->userroleservice->deleteMulti($user_id,$data);
       return Utility::ToApi("User role deleted successfully",true,  $userRole,"Ok",200);
    }

}
