<?php

namespace App\Http\Controllers\paypal;

use App;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Http;
use Session;
use App\File;
use App\Order;
use App\Category;
use App\Payment;
use Carbon\Carbon;
use App\Product;
use App\ordered_product;
use JWTAuth;
use App\Http\Controllers\Controller;
use App\Providers\RouteServiceProvider;
use App\User;
use Illuminate\Foundation\Auth\ConfirmsPasswords;
use Illuminate\Http\Request;
use App\Mail\mailsender;
use App\Http\Services\FileService;
use App\Http\Utility;

use App\configModel\Header;
use App\Http\Services\AuthService;
use App\Mail\VerficationMailable;
use App\user_coupon;
use Illuminate\Support\Facades\Mail as FacadesMail;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Auth;


class paypalController extends Controller
{

    public function getPrice(Request $request)
    {


        $items_arr = [];

        $total = 0;
        $shipping = 0;
        $arr = explode(",", $request->products);
        for ($i = 0; $i < sizeof($arr); $i = $i + 2) {
            $product = Product::find((int)$arr[$i]);

            $prodObject = new class
            {
            };
            $prodObject->name = $product->name;
            $prodObject->description = $product->short_description;
            $prodObject->unit_amount = new class
            {
            };
            $prodObject->unit_amount->currency_code = "USD";
            // if($product->price_sale > 0) {
            //     $prodObject->unit_amount->value = $product->price_sale;
            // }else{
            $prodObject->unit_amount->value = $product->price;
            //            $prodObject->unit_amount->shipping = (int)$product->shipping_costs;
            // }
            $prodObject->quantity = $arr[$i + 1];

            array_push($items_arr, $prodObject);

            if (!$product) {
                return   Utility::ToApi("product not exist", true, null, "OK", 601);
            } else {
                // if($product->price_sale > 0){
                //     $total += $product->price_sale * (int)$arr[$i + 1];
                // }else{
                $total += ($product->price * (int)$arr[$i + 1]);
                $shipping += ($product->shipping_costs * (int)$arr[$i + 1]);
                // }
            }
        }

        $tax = ceil(($total * 5) / 100);
        $subtotal = ceil($total);
        $shipping_ceil = ceil($shipping);


        $amount_arr = [];
        $amountObject = new class
        {
        };


        $amountObject->breakdown = new class
        {
        };
        $amountObject->breakdown->item_total = new class
        {
        };
        $amountObject->breakdown->item_total->currency_code = "USD";
        $amountObject->breakdown->item_total->value = $subtotal;
        $amountObject->breakdown->tax_total = new class
        {
        };
        $amountObject->breakdown->tax_total->currency_code = "USD";
        $amountObject->breakdown->tax_total->value = $tax;
        $amountObject->breakdown->shipping = new class
        {
        };
        $amountObject->breakdown->shipping->currency_code = "USD";
        $amountObject->breakdown->shipping->value = $shipping_ceil;


        $coupon = user_coupon::where('user_id', Auth::user()->id)->where('status','use')->first();
        if (!empty($coupon)) {
            $coupon_value=ceil(($subtotal * $coupon->value) / 100);
            $amountObject->breakdown->discount = new class
            {
            };
            $amountObject->breakdown->discount->currency_code = "USD";
            $amountObject->breakdown->discount->value = $coupon_value;
        }else{
            $coupon_value =0;
        }

        $total = ($subtotal + $tax + $shipping_ceil) - $coupon_value;
        $amountObject->currency_code = "USD";
        $amountObject->value = $total;
        $data = [];
        array_push($data, $items_arr);
        array_push($data, $amountObject);

        return   Utility::ToApi("price Object", true, $data, "OK", 200);
    }

    public function submitPay(Request $request)
    {
        Session::put('paid', 1);

        return 1;
    }

    public function submitedPaing(Request $request)
    {

        $token = $request->user;

        JWTAuth::setToken($token);
        $user = JWTAuth::toUser();

        if ($user) {
            $productsEmail = [];
            $productsEmail['products'] = [];
            $arr = explode(",", $request->products);
            for ($i = 0; $i < sizeof($arr); $i = $i + 2) {
                $product = Product::find((int)$arr[$i]);
                // if($product->price_sale > 0){
                //     array_push($productsEmail['products'], [$arr[$i], $product->name, $arr[$i+1], $product->price_sale]);
                // }else{
                array_push($productsEmail['products'], [$arr[$i], $product->name, $arr[$i + 1], $product->price]);
                // }

                if (!$product) {
                    return   Utility::ToApi("product not exist", true, null, "OK", 601);
                }
            }

            $getAddressUser = User::where('id', JWTAuth::user()->id)->first();
            $payment = new Payment();
            $payment->orderID = $request->orderID;
            $payment->payerID = $request->payerID;
            $payment->create_time = $request->create_time;
            $payment->country_code = $request->country_code;
            $payment->email_address = $request->email_address;
            $payment->UserFullName = $request->UserFullName;
            $payment->payer_id = $request->payer_id;
            $payment->amountValue = $request->amountValue;
            $payment->amountCurrency_code = $request->amountCurrency_code;
            $payment->payeeEmail_address = $request->payeeEmail_address;
            $payment->payeeMerchant_id = $request->payeeMerchant_id;
            $payment->status = $request->status;
            $payment->captures_id = $request->captures_id;
            $payment->reference_id = $request->reference_id;
            $payment->created_at = Carbon::now();
            $payment->updated_at = Carbon::now();
            $payment->address = $getAddressUser->address;
            $payment->zipcode = $getAddressUser->zipcode;
            $payment->city = $getAddressUser->city;
            $payment->country = $getAddressUser->country;
            $payment->save();

            if($request->coupon_id>0){
                $coupon = user_coupon::find($request->coupon_id);
                $coupon->status="used";
                $coupon->save();
            }
            $order = Order::get()->where('id', '=', $request->reference_id)->first();
            $productsEmail['tax'] = $order->tax;
            $productsEmail['shipping'] = $order->shipping_costs;
            $productsEmail['total'] = (int)$order->total;
            $order->orderID = $request->orderID;
            $order->orderStatus_id = 8; // paid
            $order->save();

            //            $details = [
            //                'title' => 'Mail from Damastore',
            //                'body' => 'This is for testing email using smtp'
            //            ];
            //
            //               Mail::to('hmed2sbhe@gmail.com')->send(new mailsender($details));

            VerficationMailable::sendOrderDetailsByEmail($user->email, "!", $productsEmail);

            return   Utility::ToApi("payment has been submitted successfuly", true, null, "OK", 200);
        }

        return   Utility::ToApi("user not exist", true, null, "OK", 600);
    }
    public function getinit(Request $request){
        $response = Http::withHeaders([
            'Authorization' => 'Key_Test ZGFtYXN0b3JlLmRhbWFzdG9yZUFwcDoyY2EwODZjMzRlOTY0NjZmOGQ2YjUwODFjMGQxZDg1OQ==',
            'Content-Type'=> "application/json"
        ])->acceptJson()->post('https://api-test.noonpayments.com/payment/v1/order', $request->all());
        return $response;
    }
    public function submitedSale(Request $request){
        $response = Http::withHeaders([
            'Authorization' => 'Key_Test ZGFtYXN0b3JlLmRhbWFzdG9yZUFwcDoyY2EwODZjMzRlOTY0NjZmOGQ2YjUwODFjMGQxZDg1OQ==',
            'Content-Type'=> "application/json"
        ])->acceptJson()->post('https://api-test.noonpayments.com/payment/v1/order', $request->all());
        return $response;
    }

    public function startOrder(Request $request)
    {

        $token = $request->user;

        JWTAuth::setToken($token);
        $user = JWTAuth::toUser();

        $arr = explode(",", $request->products);
        for ($i = 0; $i < sizeof($arr); $i = $i + 2) {
            $product = Product::find((int)$arr[$i]);
            if (!$product) {
                return   Utility::ToApi("product not exist", true, null, "OK", 601);
            }
        }

        $total_price = 0;
        $total_qty = 0;
        $shipping_costs = 0;
        $arr = explode(",", $request->products);
        for ($i = 0; $i < sizeof($arr); $i = $i + 2) {
            $product = Product::find((int)$arr[$i]);
            $total_price += $product->price * $arr[$i + 1];
            $shipping_costs += (int)$product->shipping_costs * $arr[$i + 1];
            $total_qty += $arr[$i + 1];
        }
        $getCurrentOrder = Order::where('user_id', JWTAuth::user()->id)->where('orderStatus_id', 6)->first();
        if (empty($getCurrentOrder)) {
            $validation = 10;
        } else {
            $validation = $getCurrentOrder->orderStatus_id;
        }
        if ($validation === 6) {
            DB::table('ordered_products')->where('order_id', $getCurrentOrder->id)->delete();
            $order = Order::find($getCurrentOrder->id);
            $order->orderID = $request->orderID;
            $order->tax = ceil(($total_price * 5) / 100);
            $order->total = ($total_price + $order->tax) + (int)$shipping_costs;
            $order->quantity = $total_qty;
            $order->user_id = $user->id;
            $order->orderStatus_id = 6; // created
            $order->shipping = 1;
            $order->created_at = Carbon::now();
            $order->updated_at = Carbon::now();
            $order->shipping_costs = (int)$shipping_costs;
            $order->save();

            for ($i = 0; $i < sizeof($arr); $i = $i + 2) {
                $product = Product::find((int)$arr[$i]);
                $ordered_product = new ordered_product();
                $ordered_product->order_id = $order->id;
                $ordered_product->product_id = $product->id;
                $ordered_product->qty = $arr[$i + 1];
                $ordered_product->price = $product->price;
                $ordered_product->created_at = Carbon::now();
                $ordered_product->updated_at = Carbon::now();
                $ordered_product->save();
            }
            return $order->id;
        } else {
            $order = new Order();
            // $now = Carbon::now();

            // $allorders = Order::get();
            // $max = 0;
            // foreach ($allorders as $item) {
            //     if ((int)substr($item->id, -5) > $max && substr($now->year, -2) == (int)substr($item->id, -9, -7) && $now->month == (int)substr($item->id, -7, -5)) {
            //         $max = (int)substr($item->id, -5);
            //     }
            // }
            // $max += 1;
            // $generatedOrder = substr($now->year, -2) . $now->month . str_pad($max, 5, '0', STR_PAD_LEFT);
            // $order->id = $generatedOrder;
            $order->orderID = $request->orderID;
            $order->tax = ceil(($total_price * 5) / 100);
            $order->total = ($total_price + $order->tax) + (int)$shipping_costs;
            $order->quantity = $total_qty;
            $order->user_id = $user->id;
            $order->orderStatus_id = 6; // created
            $order->shipping = 1;
            $order->created_at = Carbon::now();
            $order->updated_at = Carbon::now();
            $order->shipping_costs = (int)$shipping_costs;
            $order->save();

            for ($i = 0; $i < sizeof($arr); $i = $i + 2) {
                $product = Product::find((int)$arr[$i]);
                $ordered_product = new ordered_product();
                $ordered_product->order_id = $order->id;
                $ordered_product->product_id = $product->id;
                $ordered_product->qty = $arr[$i + 1];
                $ordered_product->price = $product->price;
                $ordered_product->created_at = Carbon::now();
                $ordered_product->updated_at = Carbon::now();
                $ordered_product->save();
            }
            return $order->id;
        }
    }



    public function failedOrder(Request $request)
    {


        $order = Order::get()->where('id', '=', $request->orderID)->first();
        $order->orderStatus_id = 9; // failed
        $order->save();

        return   Utility::ToApi("order is failed", true, $order, "OK", 200);
    }
    public function TestAuth(Request $request)
    {
        if (auth()->user()) {
            $getAddressUser = User::where('id', JWTAuth::user()->id)->first()->address;
            if ($getAddressUser) {
                return response()->json(array('status' => 'ok'));
            } else {
                return response()->json(array('status' => 'needAddress'));
            }
        } else {
            return response()->json(array('status' => 'no'));
        }
    }
    public function OrderCancel(Request $request)
    {
        $orderId = $request->id;
        if ($orderId) {
            $order = Order::find($orderId);
            $order->orderStatus_id = 7;
            $order->save();
            return   Utility::ToApi("Order is Canceled", true, "canceled", "OK", 200);
        }
        return   Utility::ToApi("Order is not found", true, "0", "OK", 200);
    }
}
