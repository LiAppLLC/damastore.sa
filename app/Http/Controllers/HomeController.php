<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Services\HomeService;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;
use App\Http\Utility;
use App\Product;
use Illuminate\Support\Arr;
use App\User;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    protected $homeservice;
    public function __construct(HomeService $homeService)
    {
        // $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function getHomePage($pageIndex, $pageSize)
    {
        list($home1, $total) = $this->homeservice->getHomePage($pageIndex, $pageSize);
        $home2 = $this->homeservice->getTopsHomePage();
        $homePage = new Arr();
        $homePage->OldHomePage = $home1;
        $homePage->NewHomePage = $home2;

        return Utility::ToPageApi("home page returned successfully", true, $homePage, $total, "Ok", 200);
    }


    public function changeprices(Request $request)
    {
        $s = explode(',', $request->arr_products);
        $number_for = $request->number_for;
        foreach ($s as $item) {

            $product = Product::find($item);
            $native_price = $product->unit;
            $product->price = ($native_price * $number_for);
            $product->save();
        }
        return response()->json(array('status' => 'all done'));
    }


    public function saveToken(Request $request)
    {
        // auth()->user()->update(['device_token'=>$request->token]);
        $user = User::find(Auth::user()->id);
        $user->device_token = $request->token;
        $user->save();
        return response()->json(['token saved successfully.']);
    }

    /**
     * Write code on Method
     *
     * @return response()
     */
    public function sendNotification(Request $request)
    {
        $firebaseToken = User::whereNotNull('device_token')->pluck('device_token')->all();

        $SERVER_API_KEY = 'AAAAjxqV0Lo:APA91bGlPVO4Ndpo1kuy3XpUUkXre89qHUBR2bw7ooqslCVrd6BszCBizkM7tq6rwUcFe9W-Q_5BFrSLaJ_TVeEnLir_L2esdHn0F3d9FHcpg40hgSYdlHkJAbPOA1kIp9HbWkaLrBZS';

        $data = [
            "registration_ids" => $firebaseToken,
            "notification" => [
                "title" => $request->title,
                "body" => $request->body,
                "sound" => "default"
            ],
            "android" => [
                "notification" => [
                    'default_sound' => true,
                    "default_vibrate_timings" => true,
                    "default_light_settings" => true,
                    "notification_priority" => "PRIORITY_HIGH",
                    "visibility" => "PUBLIC",
                    "direct_boot_ok" => true,
                ]
            ]
        ];
        $dataString = json_encode($data);

        $headers = [
            'Authorization: key=' . $SERVER_API_KEY,
            'Content-Type: application/json',
        ];

        $ch = curl_init();

        curl_setopt($ch, CURLOPT_URL, 'https://fcm.googleapis.com/fcm/send');
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $dataString);

        $response = curl_exec($ch);
        return  redirect()->back()->with($response);
    }
}
