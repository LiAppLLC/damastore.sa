<?php

namespace App\Http\Controllers\api;
use App;
use Session;
use App\File;

use App\Http\Controllers\Controller;
use App\Providers\RouteServiceProvider;
use Illuminate\Foundation\Auth\ConfirmsPasswords;
use Illuminate\Http\Request;

use App\Http\Services\FileService;
use App\Http\Utility;

use App\configModel\Header;

class configController extends Controller
{

    protected $fileService;
    public function __construct(FileService $fileService)
    {
//        $this->middleware('ApiAuth:api');
        $this->fileService = $fileService;
    }

    public function admin(){
        return view('admin/views/Dashboard');
    }

    public function HeaderAdd(Request $request){

        $header = new Header();
        $header->name = $request->name;
        $header->description = $request->description;
        $header->save();

        $image1 = $request->file('image1');
        $image2 = $request->file('image2');

        $header->image1 = $this->fileService->addImage($image1, $header['id'], 'HEADERS');
        $header->image2 = $this->fileService->addImage($image2, $header['id'], 'HEADERS');

//        dd($request->all(), $header);
        return   Utility::ToApi("Header has been added successfuly",true, $header,"OK",200);
    }

    public function getAllHeader(Request $request){

        $headers = Header::all();

        foreach ($headers as $index => $item){
            $images = File::where('target_name', '=', 'HEADERS')->where('target_id', '=', $item->id)->get();
            if(sizeof($images) > 0) {
                $headers[$index]->image1 = $images[0];
                if (sizeof($images) > 0) {
                    $headers[$index]->image2 = $images[1];
                }
            }
        }

//        dd($request->all(), $header);
        return   Utility::ToApi("Header List",true, $headers,"OK",200);
    }


}
