<?php

namespace App\Http\Controllers;

use App\Product;
use Illuminate\Http\Request;

class csvcontroller extends Controller
{
    public function exportCsv(Request $request)
    {
        $fileName = 'products.csv';
        $products = Product::all();

        $headers = array(
            "Content-type"        => "text/csv",
            "Content-Disposition" => "attachment; filename=$fileName",
            "Pragma"              => "no-cache",
            "Cache-Control"       => "must-revalidate, post-check=0, pre-check=0",
            "Expires"             => "0"
        );

        $columns = array(
            'id', 'title', 'description',
            'availability', 'condition', 'price', 'link',
            'image_link', 'brand', 'item_group_id', 'fb_product_category', 'google_product_category'
        );
        $file = fopen('products.csv', 'w');
        fputcsv($file, $columns);
        foreach ($products as $product) {
            $row['id']  = $product->id;
            $row['title']    = $product->name;
            $row['description']    = str_replace("&nbsp;", ' ', strip_tags($product->description));
            $row['availability']    = "in stock";
            $row['condition']    = "new";
            $row['price']    = $product->price . " USD";
            $row['link']    = "https://damastore.net/productDetails/" . $product->id;
            $row['image_link']    = "https://damastore.net/images/" . $product->getImageUrl();
            $row['brand']    = "DamaStore";
            $row['item_group_id']    = $product->category_id;
            $row['fb_product_category']    = $product->category->name . ' > ' . $product->type->name;
            $row['google_product_category']    = $product->category->name . ' > ' . $product->type->name;
            fputcsv($file, array(
                $row['id'], $row['title'], $row['description'],
                $row['availability'], $row['condition'], $row['price'],
                $row['link'], $row['image_link'], $row['brand'],
                $row['item_group_id'],
                $row['fb_product_category'],
                $row['google_product_category'],
            ));
        }
        fclose($file);
        return redirect('/products.csv');
    }
}
