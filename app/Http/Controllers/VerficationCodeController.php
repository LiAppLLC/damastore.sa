<?php

namespace App\Http\Controllers;

use App\verficationCode;
use Illuminate\Http\Request;

class VerficationCodeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\verficationCode  $verficationCode
     * @return \Illuminate\Http\Response
     */
    public function show(verficationCode $verficationCode)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\verficationCode  $verficationCode
     * @return \Illuminate\Http\Response
     */
    public function edit(verficationCode $verficationCode)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\verficationCode  $verficationCode
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, verficationCode $verficationCode)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\verficationCode  $verficationCode
     * @return \Illuminate\Http\Response
     */
    public function destroy(verficationCode $verficationCode)
    {
        //
    }
}
