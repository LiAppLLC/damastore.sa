<?php

namespace App\Http\Controllers;

use App\Category;
use App\Http\Services\CategoryService;
use App\Http\Utility;
use Categories;
use Illuminate\Http\Request;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Validator;

class CategoryController extends Controller
{
    protected $CategoryService;
    public function __construct(CategoryService $CategoryService)
    {
        $this->CategoryService = $CategoryService;
        $this->middleware('ApiAuth:api', ['except' => ['index','categoriesByFilter','categoryChildren','getSubCategoryFilter']]);
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index($pageindex,$pageSize)
    {
        list($category,$total)=$this->CategoryService->getAll($pageindex,$pageSize);

        return Utility::ToPageApi("All Categories",true,$category,$total,"OK",200);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'name' =>['required', 'string']
        ]);

        if ($validator->fails()) {

            return   Utility::ToApi("missing fields",false, ["validator"=>$validator->messages()->first()],"BadRequest",400);

        }
        $data=$request->only('name','description','image_id','display_on_home','display_order','parent_id');
        if( !Arr::exists($data, 'description'))
        $data['description'] = null;
        if( !Arr::exists($data, 'image_id'))
        $data['image_id'] = null;
        if( !Arr::exists($data, 'display_on_home'))
        $data['display_on_home'] = false;
        if( !Arr::exists($data, 'display_order'))
        $data['display_order'] = null;
        if( !Arr::exists($data, 'parent_id'))
            $data['parent_id'] = -1;
        if($data['parent_id']!=-1)
        {
            $C=Category::find($data['parent_id']);
            if($C==null)
                return Utility::ToApi("Category not found",true,  null,"Ok",200);

        }

        $category=$this->CategoryService->add($data);
        return Utility::ToApi("Category added successfully",true,  $category,"Ok",201);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Category  $category
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $category=$this->CategoryService->get($id);

        return   Utility::ToApi("Category return successfully",true,$category,"OK",200);
    }

    public function categoriesByFilter(Request $request)
    {
        $category=$this->CategoryService->getByFilter($request->filter);

        return   Utility::ToApi("Category return successfully",true,$category,"OK",200);
    }

    public function getSubCategoryFilter(Request $request)
    {
        $subcategory=$this->CategoryService->getSubCategoryFilter($request);
        return Utility::ToApi("SubCategories return successfully",true,  $subcategory,"Ok",200);
    }

    public function categoryChildren($pageIndex,$pageSize)
    {
        list($category,$total)=$this->CategoryService->categoryChildren($pageIndex,$pageSize);
        return Utility::ToPageApi("categories return successfully",true,$category,$total,"Ok",200);
    }


    public function changeOrder(Request $request)
    {
        $categories=$this->CategoryService->changeOrder($request->orders);

        return   Utility::ToApi("Category order changed successfully",true,$categories,"OK",200);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Category  $category
     * @return \Illuminate\Http\Response
     */
    public function edit(Category $category)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Category  $category
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request,$id)
    {
        $validator = Validator::make($request->all(), [
            'name' =>['required'],
            'description' => ['required'],
            'image_id' =>['required'],
            'display_order' =>['required']
        ]);

        if ($validator->fails()) {

            return   Utility::ToApi("missing fields",false, ["validator"=>$validator->messages()->first()],"BadRequest",400);

        }

        $data=$request->only('name','description','image_id', 'display_on_home','display_order');

        $category=$this->CategoryService->Edit($id,$data);

        Utility::ToApi("Category order changed successfully",true,$category,"OK",200);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Category  $category
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $images_cate = Category::find($id);
        if (File::exists("./" . $images_cate->image)) {
            File::delete("./" . $images_cate->image);
            Category::destroy($id);
        }
        return response()->json(['success' => 'done']);
    }
}
