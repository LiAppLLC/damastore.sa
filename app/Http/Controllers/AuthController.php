<?php

namespace App\Http\Controllers;
use App\Http\Services\AuthService;
use App\Http\Utility;
use Illuminate\Http\Request;
use App\Providers\RouteServiceProvider;
use Illuminate\Support\Facades\Validator;
use App\Exceptions\CustomException;
use Illuminate\Support\Arr;
use Session;
class AuthController extends Controller
{



     /**
     * Create a new AuthController instance.
     *
     * @return void
     */
    protected $authService;
    public function __construct(AuthService $authService)
    { $this->authService = $authService;
        $this->middleware('ApiAuth:api', ['except' => ['login','register','confrimEmail','changepassword','SendVerfication','SendResetpassword','SendVerfication','resetpassword','refresh']]);
    }

    /**
     * Get a JWT token via given credentials.
     *
     * @param  \Illuminate\Http\Request  $request
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function login(Request $request)
    {

        $validator = Validator::make($request->all(), [
            'password' =>['required'],
        ]);
        if($validator->fails())
        throw new CustomException($validator->messages()->first());
        $credentials = $request->only('email', 'password','username');
        if( !Arr::exists($credentials, 'username')&& !Arr::exists($credentials, 'email'))
        return   Utility::ToApi("username or email not exisits",false, ['error' => 'Unauthorized'],"BadRequest",400);

        if( Arr::exists($credentials, 'username')) {
            $user = $this->authService->UsernameLogin($credentials["username"], $credentials["password"]);
        }else if( Arr::exists($credentials, 'email')) {
            $user = $this->authService->EmailLogin($credentials["email"], $credentials["password"]);
        }if($user[0] == 1) {

            return Utility::ToApi("Login successful", true, $user[1], "ok", 201);
        }
        if($user[0] == -1) {
            return Utility::ToApi("Blocked User", true, $user[1], "ok", 402);
        }
        if($user[0] == -2) {
            return Utility::ToApi("Your account is not verified yet, check your Email", true, $user[1], "ok", 403);
        }

        return   Utility::ToApi("Unauthorized",false, ['error' => 'Unauthorized'],"Unauthorized",401);

    }

    public function register(Request $request)
    {

        try {


            $validator = Validator::make($request->all(), [
                'username' =>['required', 'string','unique:users'],
                'email' => ['required', 'string', 'email', 'max:255', 'unique:users'],
                'firstName' => ['required', 'string'],
                'lastName' => ['required', 'string'],
                'bornDate' => ['required', ],
                'phone' => ['required', 'string', 'unique:users'],
                'password' => ['required', 'string', 'min:8' ],
            ]);

            if ($validator->fails()) {

                return   Utility::ToApi("missing fields",false, ["validator"=>$validator->messages()->first()],"BadRequest",400);

            }

            $data=$request->only('username','email', 'password','firstName','lastName','bornDate','phone');
            $user =$this->authService->register($data);
            $this->authService->SendVerfication(["username"=>$user->username]);

            return   Utility::ToApi("Registration Sucessful",true,$user,"OK",201);

        } catch (Throwable $th) {
            return   Utility::ToApi("Wrong Email or Password",false, ['error' => $th],"Unauthorized",401);
        }




    }




    public function SendVerfication(Request $request)
    {


            $validator = Validator::make($request->all(), [

                'username' => ['required', 'string'],

            ]);

            if ($validator->fails()) {

                return  Utility::ToApi("validatotion",false,["validator"=>$validator->messages()->first()],"BadRequest",400);

            }

            $data=$request->only('username')['username'];
            $result=$this->authService->SendVerfication($data);
            if($result){
                return  Utility::ToApi("verfication sent  successfuly",true,"code sent","ok",200);
            }




        }


    public function SendResetpassword(Request $request)
    {


            $validator = Validator::make($request->all(), [

                'username' => ['required', 'string'],

            ]);

            if ($validator->fails()) {

                return  Utility::ToApi("validatotion",false,["validator"=>$validator->messages()->first()],"BadRequest",400);

            }

            $data=$request->only('username')['username'];
            $result=$this->authService->SendResetPassword($data);
            if($result){
                return  Utility::ToApi("verfication sent  successfuly",true,"code sent","ok",200);
            }




        }

        public function confrimEmail(Request $request)
        {


                $validator = Validator::make($request->all(), [

                    'username' => ['required', 'string'],
                    'verificationcode' => ['required', 'string'],

                ]);

                if ($validator->fails()) {

                    return  Utility::ToApi("validatotion",false,["validator"=>$validator->messages()->first()],"BadRequest",400);

                }

                $username=$request->only('username')['username'];
                $code=$request->only('verificationcode')['verificationcode'];
                $result=$this->authService->VerfiyCode($username,$code,false);
                if($result){
                    return  Utility::ToApi("confirm complate  successfuly",true,"account verfied","ok",200);
                }




            }



            public function resetpassword(Request $request)
        {


                $validator = Validator::make($request->all(), [

                    'username' => ['required', 'string'],
                    'verificationcode' => ['required', 'string'],
                    'newpassword' =>['required','string']
                ]);

                if ($validator->fails()) {

                    return  Utility::ToApi("validatotion",false,["validator"=>$validator->messages()->first()],"BadRequest",400);

                }

                $username=$request->only('username')['username'];
                $code=$request->only('verificationcode')['verificationcode'];
                $newPassword=$request->only('newpassword')['newpassword'];
                $result=$this->authService->VerfiyCode($username,$code,true);
                if($result){
                $result=$this->authService->resetPassword($username,$newPassword);
                if($result){
                    return  Utility::ToApi("confirm complate  successfuly",true,"code sent","ok",200);
                }
            }
        }




            public function changePassword(Request $request)
            {


                    $validator = Validator::make($request->all(), [
                        'newpassword' =>['required','string','min:8'],
                        'password'=>['required','string']
                    ]);

                    if ($validator->fails()) {

                        return  Utility::ToApi($validator->messages()->first(),false,null,"BadRequest",400);

                    }

                    $newPassword=$request->only('newpassword')['newpassword'];
                    $oldPassword=$request->only('password')['password'];
                    $result=$this->authService->changePassword($newPassword,$oldPassword);
                    if($result){
                        return  Utility::ToApi("Password changed successfully",true,"Password changed successfully","ok",200);
                    }
            }



    /**
     * Get the authenticated User
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function me()
    {
        return response()->json($this->guard()->user());
    }

    /**
     * Log the user out (Invalidate the token)
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function logout()
    {
        $this->guard()->logout();

        return response()->json(['message' => 'Successfully logged out']);
    }

    /**
     * Refresh a token.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function refresh(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'token' =>['required', 'string']
        ]);

        if ($validator->fails()) {
            return   Utility::ToApi("missing fields",false, ["validator"=>$validator->messages()->first()],"BadRequest",400);
        }

        $data=$request->only('token');

        $user= $this->authService->refreshToken($data);
        if($user==null){
            return   Utility::ToApi("Unauthorized",false, ['error' => 'Unauthorized'],"Unauthorized",401);
        }

        return  Utility::ToApi("Token refreshed successfully",true,$user,"OK",201);
    }

    /**
     * Get the token array structure.
     *
     * @param  string $token
     *
     * @return \Illuminate\Http\JsonResponse
     */
    protected function respondWithToken($token)
    {
        return [
            'access_token' => $token,
            'token_type' => 'bearer',
            'expires_in' => $this->guard()->factory()->getTTL()
        ];
    }

    /**
     * Get the guard to be used during authentication.
     *
     * @return \Illuminate\Contracts\Auth\Guard
     */
    public function guard()
    {
        return Auth::guard();
    }

    public function CheckToken()
    {

        $user= Auth()->user();
        if($user==null){
         $token =  $this->guard()->refresh();
         $user->role_id=(int)$user->role_id;
         $user->token=$token;
          return   Utility::ToApi("Sucessful login",true,$user,"OK",201);
        }
        else
        {
            return   Utility::ToApi("Unauthorized",false, ['error' => 'Unauthorized'],"Unauthorized",401);
        }

    }




    public function loginByFaceBook(Request $request){

        try {

            $user =$this->authService->loginByFaceBook($request);

            return   Utility::ToApi("Registration Sucessful",true,$user,"OK",201);

        } catch (Throwable $th) {
            return   Utility::ToApi("Wrong Email or Password",false, ['error' => $th],"Unauthorized",401);
        }
    }

}
