<?php

namespace App\Http\Controllers\c;

use App\CimagesGallery;
use App\Cproduct;
use App\Http\Controllers\Controller;
use App\Http\Utility;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class CproductsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // return $request->all();
        $validator = Validator::make($request->all(), [
            'name' => 'required',
            'supplier' => 'required',
            'ar_name' => 'required',
            'description' => 'required',
            'ar_description' => 'required',
            'short_description' => 'required',
            'ar_short_description' => 'required',
            'price' => 'required',
            'is_price' => 'required',
            'product_code' => 'required|unique:products,product_code',
            'shipping_costs' => 'required',
            'category_id' => 'required',
            'type_id' => 'required',
            'image_gallery' => 'required',
            'days' => 'required',
            'image' => 'required',
            'available' => 'required',
        ]);

        if ($validator->fails()) {
            return response()->json(array('Validation Error.', $validator->errors()));
        }
        if ($request->file('image')) {
            $files = $request->file('image');
            $destinationPath = '/c_images_product/'; // upload path
            $profileImage = time() . "." . $files->clientExtension();
            $files->move(public_path($destinationPath), $profileImage);
            $image = "c_images_product/" . $profileImage;
        }
        $product = Cproduct::create($request->all());
        $product->image = $image;
        $product->save();
        if ($request->file('image_gallery')) {
            $index=0;
            foreach ($request->file('image_gallery') as $gall) {
                $files = $gall;
                $destinationPath = '/c_images_gallery_product/'; // upload path
                $profileImage = time() . "." . $files->clientExtension();
                $files->move(public_path($destinationPath),$index.$profileImage);
                $imageg = "c_images_gallery_product/" . $index.$profileImage;
                $CimagesGallery = new CimagesGallery;
                $CimagesGallery->product_id = $product->id;
                $CimagesGallery->url = $imageg;
                $CimagesGallery->save();
                $index++;
            }
        }
        return Utility::ToApi("Product has been added successfuly", true, $product, "OK", 200);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $delete = Cproduct::destroy($id);
        if ($delete) {
            return response()->json(array('success' => 'true'));
        } else {
            return response()->json(array('success' => 'false'));
        }
    }
}
