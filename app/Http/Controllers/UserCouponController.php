<?php

namespace App\Http\Controllers;

use App\user_coupon;
use App\coupon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;

class UserCouponController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if (Auth::user()) {
            $coupon = user_coupon::all();
            return response()->json(array('coupon' => $coupon));
        } else {
            return response()->json((array('error' => 'unauthorize')));
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        if (Auth::user()) {
            $ifcouponuse = user_coupon::where('user_id', Auth::user()->id)->where('status', 'use')->first();
            if ($ifcouponuse) {
                $data = [
                    'status' => 'used',
                    'msg' => 'The Coupon "' . $ifcouponuse->coupon_code . '" Already in Use !',
                    'data' => $ifcouponuse
                ];
                return response()->json($data);
            }
            $validator = Validator::make($request->all(), [
                'coupon' => 'required',
            ]);

            if ($validator->fails()) {
                $data = [
                    'status' => 'error',
                    'validator' => $validator->errors()
                ];
                return response()->json($data);
            }
            $date = date('Y-m-d');
            $coupon = coupon::where('coupon', $request->coupon)->first();
            // dd($coupon);
            if (!empty($coupon)) {
                $user_coupon = user_coupon::where('coupon_id', $coupon->id)->first();
                if (!empty($user_coupon)) {
                    $data = [
                        'status' => 'used',
                        'msg' => 'The Coupon Already Used !',
                        'data' => 'null'
                    ];
                    return response()->json($data);
                } else {
                    //date after validate
                    if ($coupon->end >= $date && $coupon->start <= $date) {
                        $user_coupon_new = new user_coupon;
                        $user_coupon_new->user_id = Auth::user()->id;
                        $user_coupon_new->coupon_id = $coupon->id;
                        $user_coupon_new->coupon_code = $coupon->coupon;
                        $user_coupon_new->value = $coupon->value;
                        $user_coupon_new->start = $coupon->start;
                        $user_coupon_new->end = $coupon->end;
                        $user_coupon_new->status = "use";
                        $user_coupon_new->save();

                        $data = [
                            'status' => 'submitted',
                            'msg' => 'The Coupon apply already !',
                            'data' =>  $user_coupon_new
                        ];
                        return response()->json($data);
                    } else {
                        $data = [
                            'status' => 'expired',
                            'msg' => 'The Coupon is Expired',
                            'data' => 'null'
                        ];
                        return response()->json($data);
                    }
                }
            } else {
                $data = [
                    'status' => 'notExist',
                    'msg' => 'The Coupon Not Exist',
                    'data' => 'null'
                ];
                return response()->json($data);
            }
        } else {
            return response()->json((array('error' => 'unauthorize')));
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        user_coupon::destroy($id);
    }
}
