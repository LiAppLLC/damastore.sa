<?php

namespace App\Http\Controllers;

use App\Http\Services\PermissionService;
use App\Permission;
use App\Http\Utility;
use Illuminate\Http\Request;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\Validator;

class PermissionController extends Controller
{

    protected $permissionservice;
    public function __construct(PermissionService $permissionService)
    {
        $this->permissionservice = $permissionService;
        $this->middleware('ApiAuth:api');
    }

    public function add(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'name'=>'required','string'
        ]);
        if ($validator->fails())
            return   Utility::ToApi("missing fields",false, ["validator"=>$validator->messages()->first()],"BadRequest",400);

        $data=$request->only('name');

        $permission=$this->permissionservice->add($data);

        return Utility::ToApi("Permission added successfully",true,  $permission,"Ok",201);

    }

    public function getById($id)
    {
        $permission=$this->permissionservice->get($id);
        if($permission!=null)
        return Utility::ToApi("Permission return successfully",true,  $permission,"Ok",200);
    }

     public function getAll(Request $request,$pageIndex,$pageSize)
    {
        list($permission,$total)=$this->permissionservice->getAll($request,$pageIndex,$pageSize);
        return Utility::ToPageApi("Permissions returned successfully",true, $permission,$total,"Ok",200);
    }

    public function Edit(Request $request,$id)
    {
        $data=$request->only('name');

        $permission=$this->permissionservice->Edit($id,$data);

        return Utility::ToApi("Permission Edited successfully",true,  $permission,"Ok",201);

    }

    public function delete ($id)
    {
       $permission= $this->permissionservice->delete($id);
       return Utility::ToApi("Permission deleted successfully",true,  $permission,"Ok",200);
    }

}
