<?php

namespace App\Http\Controllers;
use App\Http\Services\UserService;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Validator;
use App\Http\Utility;
use App\Order;
use App\User;
use Illuminate\Support\Facades\Auth;

class UserController extends Controller
{



    protected $userService;
    public function __construct(UserService $userService)
    { $this->userService = $userService;
        $this->middleware('ApiAuth:api');
    }



    public function editUser(Request $request)
    {
            $id = Auth::user()->id;
            if($id) {
                $validator = Validator::make($request->all(), [

                    'username' => ['required', 'string'],
                    'firstName' => ['required', 'string'],
                    'lastName' => ['required', 'string'],
                    'bornDate' => ['required'],
                    'phone' => ['required', 'string', 'unique:users,phone,' . $id],
                    'city' => ['required'],
                    'country' => ['required'],
                    'zipcode' => ['required'],
                    'address' => ['required'],
                ]);

                if ($validator->fails()) {

                    return  Utility::ToApi('Missing Data',true,$validator->errors(),"OK",200);
                }

                $data = $request->only( 'username','firstName', 'lastName', 'bornDate', 'phone',
                    'city','country','zipcode','address');
                $user = $this->userService->EditUser($data);
                return  Utility::ToApi('Edit user info successfully',true,$user,"OK",201);

            }

        return  Utility::ToApi('user not login',true,'unAuth',"OK",200);


    }


    public function getUser()
    {

            $user =$this->userService->getUser();

            return   Utility::ToApi("get user",true,$user,"OK",200);

    }

    public function getAll(Request $request,$pageIndex,$pageSize)
    {

            list($user,$total) =$this->userService->getAllFilter( $request,$pageIndex,$pageSize);

            return   Utility::ToPageApi("All users returned successfully",true,$user,$total,"OK",200);

    }
    public function user_delete(Request $request){

        User::destroy($request->id);
        Order::where('user_id', 1)->delete();
        \App\UsersRole::where('user_id', 1)->delete();

        return   Utility::ToPageApi("Delete successfully",true,'','',"OK",200);
    }   


}
