<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Utility;
use App\attributes;
use App\attribute_values;
use App\Resources\AttributesAr;
use App\Resources\AttributesEn;

class AttributesController extends Controller
{

    public function AttributeGet(Request $request){
        $Attribute=attributes::all();
        return  Utility::ToApi("Attribute has been Edit successfuly",true, $Attribute,"OK",200);
    }

    public function AttributeAdd(Request $request){

        $Attribute = new attributes();
        $Attribute->name = $request->name;
        $Attribute->ar_name = $request->ar_name;
        $Attribute->save();
        return  Utility::ToApi("Attribute has been Add successfuly",true, $Attribute,"OK",200);

    }

    public function AttributeEdit(Request $request){

        $Attribute = attributes::find($request->attr_id);
        $Attribute->name = $request->name;
        $Attribute->ar_name= $request->ar_name;
        $Attribute->save();
        return  Utility::ToApi("Attribute has been Edit successfuly",true, $Attribute,"OK",200);
    }

    public function AttributeDelete(Request $request){
        attributes::destroy($request->att_id);
        return  Utility::ToApi("Attribute has been Delete successfuly",true, 'done',"OK",200);

    }
    public function AttributeDelete_for_product(Request $request){
        attribute_values::destroy($request->att_id);
        return  Utility::ToApi("Attribute has been Delete successfuly",true, 'done',"OK",200);

    }

    public function AttributeAdd_for_product(Request $request){

        $Attribute= new attribute_values();
        $Attribute->attribute_id=$request->attribute_id;
        $Attribute->product_id= $request->product_id;
        $Attribute->value=$request->value;
        $Attribute->ar_value=$request->ar_value;
        $Attribute->image='null';
        $Attribute->attribute_value_key=$request->attribute_id.':'.$request->value.':'.$request->ar_value;
        $Attribute->save();
        $attribute_values=attribute_values::find($Attribute->id);
        // attribute_values::destroy($request->att_id);
        return  Utility::ToApi("Attribute has been Delete successfuly",true, $attribute_values,"OK",200);

    }
}
