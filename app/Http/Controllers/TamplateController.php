<?php

namespace App\Http\Controllers;

use App\Tamplate;
use Illuminate\Http\Request;

class TamplateController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Tamplate  $tamplate
     * @return \Illuminate\Http\Response
     */
    public function show(Tamplate $tamplate)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Tamplate  $tamplate
     * @return \Illuminate\Http\Response
     */
    public function edit(Tamplate $tamplate)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Tamplate  $tamplate
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Tamplate $tamplate)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Tamplate  $tamplate
     * @return \Illuminate\Http\Response
     */
    public function destroy(Tamplate $tamplate)
    {
        //
    }
}
