<?php

namespace App\Http\Services;
use App\Forbidden;
use App\ForbiddenDetail;
use http\Env\Request;
use Illuminate\Support\Facades\Validator;
use App\Exceptions\CustomException;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Arr;

class ForbiddenDetailService
{
    public function add($data){

        $forbiddenDetail = new ForbiddenDetail($data);

        $forbiddenDetail->save();

        return $forbiddenDetail;
    }

    public function getByFilter(\Illuminate\Http\Request $request,$pageIndex,$pageSize){
        $forbiddenDetail=new ForbiddenDetail();

        if ($request->input('opertaion'))
        {
            $forbiddenDetail= $forbiddenDetail->where('opertaion', 'like', '%'.$request->input('opertaion').'%');
        }

        if ($request->input('word'))
        {
            $forbiddenDetail= $forbiddenDetail->where('word','like', '%'.$request->input('word').'%');
        }

        if ($request->input('company_id'))
        {
            $forbiddenDetail= $forbiddenDetail->where('company_id',$request->input('company_id'));
        }

        if ($request->input('user_id'))
        {
            $forbiddenDetail= $forbiddenDetail->where('user_id',$request->input('user_id'));
        }

        $AllDetail = clone $forbiddenDetail;
        $forbiddenDetail= $forbiddenDetail->get();
        $total = count($forbiddenDetail);

        if($total==0)
        throw new CustomException("no forbidden word found");

        $AllDetail = $AllDetail->skip($pageIndex * $pageSize)->take($pageSize)->get();

        foreach($AllDetail as $key => $item)
        {
            $AllDetail[$key]->load('Company');
            $AllDetail[$key]->load('User');
        }

        return [$AllDetail,$total];
        }

}

?>
