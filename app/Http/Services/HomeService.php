<?php

namespace App\Http\Services;
use App\Http\Services\ProductService;
use App\Http\Services\CategoryService;
use App\Category;
use App\Product;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;
use App\Exceptions\CustomException;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Arr;

class HomeService {

    protected $productservice;
    protected $categoryservice;
    public function __construct(ProductService $productService, CategoryService $categoryService)
    {
        $this->productservice = $productService;
        $this->categoryservice = $categoryService;
    }

    public function getHomePage($pageIndex,$pageSize){

        $category = Category::where('display_on_home',1)->where('parent_id',-1);

        $total = count($category->get());
        $category = $category->skip($pageIndex*$pageSize)->take($pageSize)->get();
        $category = $category->load('image');

        $productNum = 10;
        foreach($category as $key => $item)
        {
            $ids = Category::where('parent_id',$item->id);
            $ids = $ids->select('id');
            $ids = $ids->get();

            $products = new Product();
            $products = $products->where('display_on_home',1);
            $products = $products->wherein('category_id',$ids)->get();

            $selectedProducts=[];

            if(count($products)<=$productNum)
            {
                $products->Load('Image');
                 $selectedProducts = $products;
            }

            else
            {
                $i=0;
                while(count($selectedProducts)<$productNum)
                {
                    foreach($ids as $id)
                    {
                        $product = $products->where('category_id',$id->id);
                        $product = $product->skip($i)->first();

                        if($product==null) continue;
                        $product->Load('Image');

                        array_push($selectedProducts, $product);
                    }
                    $i++;
                }
        }

            //if(count($selectedProducts)!=0)
            //$this->productservice->getFullDate($selectedProducts);
            $category[$key]->products = $selectedProducts;
        }

        return [$category,$total];
}
        public function getTopsHomePage()
            {
                $homePage = [];

                $searchHistoryService = new SearchHistoryService();
                $topSearchedCategories = new Arr();
                $topSearchedCategories->category = 1;
                $topSearchedCategories->product = 0;
                $topSearchedCategories->title = 'Top Searched Categories';
                $topSearchedCategories->elements = $searchHistoryService->TopSearchedCategories(6);
                array_push($homePage, $topSearchedCategories);

                $productService = new ProductService();
                $topPromotedProducts = new Arr();
                $topPromotedProducts->category = 0;
                $topPromotedProducts->product = 1;
                $topPromotedProducts->title = 'Top Promoted Products';
                $topPromotedProducts->elements = $productService->TopPromoted(6);
                array_push($homePage, $topPromotedProducts);

                $productService = new ProductService();
                $mostViewedProducts = new Arr();
                $mostViewedProducts->category = 0;
                $mostViewedProducts->product = 1;
                $mostViewedProducts->title = 'Most Viewed Products';
                $mostViewedProducts->elements = $productService->TopViews(6);
                array_push($homePage, $mostViewedProducts);

                $orderService = new OrderService();
                $topMostSellers = new Arr();
                $topMostSellers->category = 0;
                $topMostSellers->product = 1;
                $topMostSellers->title = 'Top Most Sellers';
                $topMostSellers->elements = $orderService->TopSeller(6);
                array_push($homePage, $topMostSellers);

                return $homePage;
            }

}
