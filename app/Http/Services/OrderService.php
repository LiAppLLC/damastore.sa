<?php

namespace App\Http\Services;
use App\Order;
use App\ProductsOrder;
use App\OrdersStatus;
use App\Product;
use App\Company;
use Illuminate\Support\Facades\Validator;
use Illuminate\Http\Request;
use App\Exceptions\CustomException;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Arr;

class OrderService {

public function add($request){
    try {
        $order = Order::create([
            'user_id' => Auth()->user()->id,
            'shipping' => $request->input('shipping')
        ]);

        foreach($request->products as $product)
        {
            $currproduct = Product::find($product['product_id']);
            if($currproduct == null)
                throw new CustomException("no such product '".$currproduct->name."' found");

            $order->quantity += $product['quantity'];
            $order->total += $currproduct->price * $product['quantity'];

            if($order->company_id == null)
                $order->company_id = $currproduct->company_id;
            else if ($order->company_id !=$currproduct->company_id)
                throw new CustomException("products must be from same company");

            ProductsOrder::create([
                'product_id' => $product['product_id'],
                'order_id' => $order->id,
                'quantity' => $product['quantity']
            ]);
        }

        $order->update();
        $order = self::get($order->id);

        return $order;

    } catch (Throwable $th) {
        throw new CustomException($th);
    }
}

public function get($id){
    $order=Order::find($id);
    if($order==null)
        throw new CustomException("no order found");

    $productOrder = ProductsOrder::where('order_id',$order->id);
    $productOrderIds = $productOrder->select('product_id')->get();
    $product = Product::whereIn('id',$productOrderIds)->get();
    foreach($product as $key => $p)
    {
        $product[$key]->quantity = ProductsOrder::where('order_id',$order->id)->
        where('product_id',$p->id)->first()['quantity'];
        $product[$key]->load('Image');
    }

    $order->products = $product;
    $order->status = OrdersStatus::find($order->orderStatus_id)['name'];
    $order->Load('Company.Image','Company.cover');
    $order->load('User');

    return $order;
}

    public function TopSeller($count)
    {
        $products_ids = ProductsOrder::select('product_id')->groupBy('product_id')
                                        ->orderByRaw('COUNT(*) DESC')->limit((int)$count)->get();
        $products = Product::whereIn('id',$products_ids)->get();
        $products->load("Image");

        return $products;
    }

    public function getAll($request,$pageIndex,$pageSize){
        $orders = new Order();
        if ($request->input('user_id'))
        {
            $orders= $orders->where('user_id', $request->input('user_id'));
        }
        if ($request->input('company_id'))
        {
            $orders= $orders->where('company_id', $request->input('company_id'));
        }
        if ($request->input('quantity'))
        {
            $orders= $orders->where('quantity', $request->input('quantity'));
        }
        if (!is_null($request->input('shipping')))
        {
            $orders= $orders->where('shipping', $request->input('shipping'));
        }
        if ($request->input('status'))
        {
            $status_id = OrdersStatus::where('name',$request->input('status'))->first()['id'];
            $orders= $orders->where('orderStatus_id', $status_id);
        }

        if ($request->input('minTotal') && !empty($request->input('minTotal')) && $request->input('maxTotal') == null)
        {

            $orders= $orders->where('total','>=',$request->input('minTotal'));
        }

        if ($request->input('minTotal') == null && $request->input('maxTotal') && !empty($request->input('maxTotal')))
        {

            $orders= $orders->where('total','<=',$request->input('maxTotal'));
        }

        if ($request->input('minTotal') && !empty($request->input('minTotal')) && $request->input('maxTotal') && !empty($request->input('maxTotal')) )
        {

            $orders= $orders->where('total','>=',$request->input('minTotal'))
            ->where('total','<=',$request->input('maxTotal'));
        }

        $total = $orders->count();
        if($total==0)
        throw new CustomException("no order found");

        $orders= $orders->skip($pageIndex*$pageSize)->take($pageSize)->get();

        foreach($orders as $key => $order)
            $orders[$key] = self::get($order->id);

        return [$orders,$total];
    }

    public function changeOrderStatus($data){

        $order= Order::find($data['order_id']);
        if($order ==null)
        {
            throw new CustomException("order not found");
        }

        $user = Auth()->user();
        if($data['status'] == 'canceled' && $user->id == $order->user_id)
        {
            $status_id = OrdersStatus::where('name',$data['status'])->first()['id'];
            if($status_id == null)
                throw new CustomException("status not found");
            $order->orderStatus_id = $status_id;
            $order->save();
            $order = self::get($order->id);

            return $order;
        }

        $company = Company::find($data['company_id']);
        if($company->user_id != $user->id)
            throw new CustomException("user has no access on this company");
        if($company->id != $order->company_id)
            throw new CustomException("company has no access on this order");
        if($data['status'] == 'canceled' || $data['status'] == 'expired')
            throw new CustomException("company has no access on this Status");

        $status_id = OrdersStatus::where('name',$data['status'])->first()['id'];
        if($status_id == null)
            throw new CustomException("status not found");

        $order->orderStatus_id = $status_id;

        $order->save();
        $order = self::get($order->id);

        return $order;
    }

    public function delete($id){

        $order= Order::find($id);
        if($order ==null)
        {
            throw new CustomException("order not found");
        }

        $order->delete();

        return $order;
    }

}
