<?php

namespace App\Http\Services;
use App\ProductsTag;
use App\Property;
use App\Product;
use http\Env\Request;
use Illuminate\Support\Facades\Validator;
use App\Exceptions\CustomException;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Arr;
class ProductsTagService
{
    public function add($data){

        $productsTag = new ProductsTag($data);

        $productsTag->save();

        return $productsTag;
    }
}

?>