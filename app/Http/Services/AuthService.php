<?php

namespace App\Http\Services;
use App\Mail\VerficationMailable;
use App\User;
use App\Role;
use App\VerficationCode;
use Illuminate\Support\Facades\Validator;
use App\Exceptions\CustomException;
use Illuminate\Support\Facades\Auth;
use Illuminate\Foundation\Auth\RegistersUsers;
use Illuminate\Support\Facades\Hash;
use Tymon\JWTAuth\Facades\JWTAuth;
use Tymon\JWTAuth\Exceptions\JWTException;
use Carbon\Carbon;
use App\Http\config;
class AuthService {

    use RegistersUsers;
    public function EmailLogin($email,$password){
        $credentials =["email"=>$email,"password"=>$password];

        $token = $this->guard()->attempt($credentials);
        if ($token) {
            $user= Auth()->user();
            if($user->blocked)
//                throw new CustomException("Blocked User");
                return [-1, $user];
            if(!$user->verify)
//                throw new CustomException("Your account is not verified yet, check your Email");
                return [-2, $user];
            $user->lastLoggedIn=Carbon::now();
            $user->token=$token;
            $user->update();
            //$user->load("Files");
            //$user->load("Roles");

            $userRoleService = new UserRoleService();
            $rolePermissionService = new RolePermissionService();
            $user->roles = $userRoleService->getByUserOnly($user->id);
            foreach($user->roles as $key => $item)
                $user->roles[$key]->permissions = $rolePermissionService->getByRoleOnly($item->id);

            return   [1, $user];
        }

        return null;
    }

    public function getUserRoles($id){
        $userRoleService = new UserRoleService();
        $rolePermissionService = new RolePermissionService();
        $roles = $userRoleService->getByUserOnly($id);
        $arr_id = [];
        $arr_name = [];
        foreach($roles as $key => $item) {
            $roles[$key]->permissions = $rolePermissionService->getByRoleOnly($item->id);
            foreach ($roles[$key]->permissions as $elem){
                array_push($arr_id, $elem->id);
                array_push($arr_name, $elem->name);
            }
            $roles[$key]->permissions_id = $arr_id;
            $roles[$key]->permissions_name = $arr_name;
        }
        return $roles;
    }

    public function canDo($function, $user_id){
        $roles = $this->getUserRoles($user_id);
        foreach ($roles as $item){
            foreach ($item->permissions_name as $elem){
                if($elem == $function){
                    return true;
                }
            }
        }
        return false;
    }

    public function UsernameLogin($username,$password){
        $credentials =["username"=>$username,"password"=>$password];
        $fieldType = filter_var($credentials["username"], FILTER_VALIDATE_EMAIL) ? 'email' : 'username';
        $token = Auth()->attempt(array($fieldType => $credentials['username'], 'password' => $credentials['password']));
        if ($token) {
            $user= Auth()->user();
            if($user->blocked)
                return [-1, $user];
//                throw new CustomException("Blocked User");
            if(!$user->verify)
                return [-2, $user];
//                throw new CustomException("Your account is not verified yet, check your Email");
            $user->lastLoggedIn=Carbon::now();
            $user->token=$token;
            $user->update();
            $user->load("Files");

            $userRoleService = new UserRoleService();
            $rolePermissionService = new RolePermissionService();
            $user->roles = $userRoleService->getByUserOnly($user->id);
            foreach($user->roles as $key => $item)
                $user->roles[$key]->permissions = $rolePermissionService->getByRoleOnly($item->id);

            return   [1, $user];
        }

        return null;
    }

    public function PhoneLogin($phone,$password){
        $credentials =["phone"=>$phone,"password"=>$password];

        $token = $this->guard()->attempt($credentials);
        if ($token) {
            $user= Auth()->user();
            if($user->blocked)
                throw new CustomException("Blocked User");
            if(!$user->verify)
                throw new CustomException("Your account is not verified yet, check your Email");
            $user->lastLoggedIn=Carbon::now();
            $user->token=$token;
            $user->update();

            $userRoleService = new UserRoleService();
            $rolePermissionService = new RolePermissionService();
            $user->roles = $userRoleService->getByUserOnly($user->id);
            foreach($user->roles as $key => $item)
                $user->roles[$key]->permissions = $rolePermissionService->getByRoleOnly($item->id);

            return   $user;
        }

        return null;
    }

    public function register($data)
    {

        try {

            $user = User::create([
                'username' => $data['username'],
                'email' => $data['email'],
                'bornDate' => $data['bornDate'],
                'firstName' => $data['firstName'],
                'lastName' => $data['lastName'],
                'email' => $data['email'],
                'password' => Hash::make($data['password']),
                'phone'=>$data['phone'],
                'verify'=>false
            ]);
            $credentials =["username"=>$data['username'],"password"=>$data['password']];
            $token = $this->guard()->attempt($credentials);
            $user= Auth()->user();
            $user->lastLoggedIn=Carbon::now();
            $user->token=$token;
//            $user->verified=false;
            $user->update();

            $userRole = new UserRoleService();
            $userRole->add([
                'user_id' => $user->id,
                'role_id' => Role::where('name','Registered')->first()->id
            ]);

            return  $user;

        } catch (Throwable $th) {
            throw new CustomException($th);
        }
    }

    public function loginByFaceBook($data){
        try {

            $newdata = [];
            $newdata['username'] = $data->name;
            $newdata['email'] = $data->email;
            $newdata['bornDate'] = '2020-10-09';
            $newdata['firstName'] = $data->name;
            $newdata['lastName'] = $data->last_name;
            $newdata['phone'] = '0912345678';
            $newdata['password'] = '0000';

            $this->register($newdata);

        } catch (Throwable $th) {
            throw new CustomException($th);
        }
    }


    public function SendVerfication($username)
    {

        $user = User::where('username',$username)->first();
        if($user==null)
            throw new CustomException("username does not exist");
        if($user->blocked)
            throw new CustomException("user is blocked");
        if($user->verify)
            throw new CustomException("User is already verified");
        $verficationCode  =VerficationCode::find($user->id);
        $data['user_id'] = $user->id;
        $data['code'] = mt_rand(100000, 999999);
        $data['type'] = config::$verification_type_of_verifingAccount;
        if($verficationCode ){
            $verficationCode->code=$data['code'];
            $verficationCode->used=false;
            $verficationCode->type=$data['type'];
            $verficationCode->save();
        }else{
            //insert
            $verificationcode = new VerficationCode($data);
            $verificationcode->save();
        }

        VerficationMailable::sendEmail($user->email, $user->firstName . ' ' . $user->lastName, $data['code']);


        return   true;


    }

    public function SendResetPassword($username)
    {


        $user = User::where('username',$username)->first();
        if($user==null)
            throw new CustomException("username does not exist");
        if($user->blocked)
            throw new CustomException("user is blocked");
        if(!$user->verify)
            throw new CustomException("User is not verified");
        $verficationCode  =VerficationCode::find($user->id);
        $data['user_id'] = $user->id;
        $data['code'] = mt_rand(100000, 999999);
        $data['type'] = config::$verification_type_of_resetingPassword;
        if($verficationCode ){
            $verficationCode->code=$data['code'];
            $verficationCode->used=false;
            $verficationCode->type=$data['type'];
            $verficationCode->save();
        }else{
            //insert
            $verificationcode = new VerficationCode($data);
            $verificationcode->save();
        }

        VerficationMailable::sendEmail($user->email, $user->firstName . ' ' . $user->lastName, $data['code']);


        return   true;


    }
    public function VerfiyCode($username,$code,$restpassword)
    {


        $user = User::where('username',$username)->first();
        if($user==null)
            throw new CustomException("username does not exist");
        if($user->blocked)
            throw new CustomException("user is blocked");
        if($user->verify)
            if(!$restpassword){
                throw new CustomException("user is verified");
            }
        $verficationCode  =VerficationCode::find($user->id);

        if($verficationCode ){
            if($verficationCode->code ==$code){
                if($verficationCode->used)
                    throw new CustomException("Expired code");
                $verficationCode->used=true;
                $verficationCode->save();
                $user->verify=true;
                $user->save();

                return   true;
            }else{
                throw new CustomException("Wrong code");
            }
        }else{
            throw new CustomException("No verfication code");
        }



        return false;


    }



    public function resetPassword($username,$newpassword){

        $user = User::where('username',$username)->first();
        if($user==null)
            throw new CustomException("username does not exist");
        if($user->blocked)
            throw new CustomException("user is blocked");
        $user->password =Hash::make($newpassword);
        $user->save();

        return true;
    }
    public function changePassword($newpassword,$oldpassword){

        $user= Auth()->user();
        if($user==null)
            throw new CustomException("User not found");
        if($user->blocked)
            throw new CustomException("User is blocked");
        $credentials =["email"=>$user->email,"password"=>$oldpassword];

        $token = $this->guard()->attempt($credentials);
        if ($token) {
            $user->password =Hash::make($newpassword);
            $user->save();
            return true;
        }else
            throw new CustomException("Old password is wrong");

    }
    public function refreshToken($data)
    {
        $user = Auth()->user();
        if($user)
            return $user;

        $user = User::where('token',$data['token'])->first();
        if($user!=null)
        {
            $token =  $this->guard()->refresh();
            $user->token=$token;
            $user->update();
            $user->load("Files");

            $userRoleService = new UserRoleService();
            $rolePermissionService = new RolePermissionService();
            $user->roles = $userRoleService->getByUserOnly($user->id);
            foreach($user->roles as $key => $item)
                $user->roles[$key]->permissions = $rolePermissionService->getByRoleOnly($item->id);
        }

        return  $user;
    }











}
