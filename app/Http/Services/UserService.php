<?php

namespace App\Http\Services;
use App\Mail\VerficationMailable;
use App\User;
use App\Http\Services\UserRoleService;
use App\VerficationCode;
use Illuminate\Support\Facades\Validator;
use App\Exceptions\CustomException;
use Illuminate\Support\Facades\Auth;
use Illuminate\Foundation\Auth\RegistersUsers;
use Illuminate\Support\Facades\Hash;
use Tymon\JWTAuth\Facades\JWTAuth;
use Tymon\JWTAuth\Exceptions\JWTException;
use Carbon\Carbon;
use App\Http\config;

class UserService {

    /*protected $userroleService;
    public function __construct(UserRoleService $userRoleService)
    {
        $this->userroleService = $userRoleService;
    }*/

    public function getUser(){

        $user= Auth()->user();
        if($user){
            if(!$user->verify)
            throw new CustomException("User is not verified");
            if($user->blocked)
            throw new CustomException("User is blocked");
            $user->load("Files");
            $userroleService = new UserRoleService();
            $user->roles = $userroleService->getByUser($user->id,0,99999);

        return $user;
        }

        throw new CustomException("No user found");
    }

    public function getAllFilter($request,$pageIndex,$pageSize){

        $users = new User();

        if ($request->input('username'))
        {
            $users= $users->where('username', 'LIKE', '%' . $request->input('username') . '%');
        }
        if ($request->input('email'))
        {
            $users= $users->where('email', 'LIKE', '%' . $request->input('email') . '%');
        }
        if ($request->input('firstName'))
        {
            $users= $users->where('firstName', 'LIKE', '%' . $request->input('firstName') . '%');
        }
        if ($request->input('lastName'))
        {
            $users= $users->where('lastName', 'LIKE', '%' . $request->input('lastName') . '%');
        }
        if ($request->input('bornDate'))
        {
            $users= $users->where('bornDate', $request->input('bornDate'));
        }
        if ($request->input('phone'))
        {
            $users= $users->where('phone', $request->input('phone'));
        }
        if ($request->input('verify'))
        {
            $users= $users->where('verify', $request->input('verify'));
        }
        if ($request->input('blocked'))
        {
            $users= $users->where('blocked', $request->input('blocked'));
        }
        if ($request->input('lastActivity'))
        {
            $users= $users->where('lastActivity', $request->input('lastActivity'));
        }
        if ($request->input('lastLoggedIn'))
        {
            $users= $users->where('lastLoggedIn', $request->input('lastLoggedIn'));
        }


        $total = $users->count();
        if($total==0)
        throw new CustomException("no user found");

        $users = $users->skip($pageIndex * $pageSize)->take($pageSize)->orderBy('username')->get();

        $users->load("Files");

        return [$users,$total];
    }

    public function EditUser($data){

        $user= Auth()->user();

           if($user==null)
           throw new CustomException("No user found");
           if(!$user->verify)
               return [-2, null];
            $repeatedUser = User::where('id', '!=', $user->id)->where('username', '=', $data['username'])->first();

            if(!$repeatedUser) {
                $user->username = $data['username'];
                $user->bornDate = $data['bornDate'];
                $user->firstName = $data['firstName'];
                $user->lastName = $data['lastName'];
                $user->phone = $data['phone'];
                $user->city = $data['city'];
                $user->address = $data['address'];
                $user->country = $data['country'];
                $user->zipcode = $data['zipcode'];
                $user->save();
                $user->load("Files");

                return $user;
            }else{
                return [-3, null];
            }

    }

}
