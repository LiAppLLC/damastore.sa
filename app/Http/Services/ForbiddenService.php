<?php

namespace App\Http\Services;
use App\Forbidden;
use Illuminate\Support\Facades\Validator;
use App\Http\Services\ForbiddenDetailService;
use Illuminate\Http\Request;
use App\Exceptions\CustomException;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Arr;
class ForbiddenService {

    protected $forbiddenDetailService;
    public function __construct(ForbiddenDetailService $ForbiddenDetailService)
    {
        $this->forbiddenDetailService = $ForbiddenDetailService;
    }

public function add($data){
    try {

        $forbidden = Forbidden::create([
            'word' => $data['word'],
        ]);

        return $forbidden;

    } catch (Throwable $th) {
        throw new CustomException($th);
    }
}

    public function check($word,$operation,$user_id,$company_id){
        $forbidden= new Forbidden();
        $forbidden= $forbidden->select('word')->get()->toArray();

        $arr = [];
        foreach ($forbidden as $key => $item)
            array_push($arr, $item['word']);

        $matches = array();
        $matchFound = preg_match_all(
                "/\b(" . implode($arr,"|") . ")\b/i",
                $word,
                $matches
              );

        // if it find matches bad words
        if ($matchFound) {
            $word = array_unique($matches[0]);

            //record to table
             $data['operation'] = $operation;
             $data['word'] = $word[0];
             $data['user_id'] = $user_id;
             $data['company_id'] = $company_id;
             $this->forbiddenDetailService->add($data);

            throw new CustomException("word '".$word[0]."' is not allowed");
        }
        }

    public function getAll($pageIndex=0,$pageSize=30){
        $forbiddens=Forbidden::skip($pageIndex * $pageSize)->take($pageSize)->get();
        if(count($forbiddens)==0)
        throw new CustomException("no forbidden word found");
        $total = count(Forbidden::all());

        return [$forbiddens,$total];
    }

    public function Edit($id,$data){

        $forbidden=Forbidden::find($id);
        if($forbidden ==null)
        {
            throw new CustomException("forbidden word not found");
        }

        if( Arr::exists($data, 'word'))
        $forbidden->word=$data['word'];

        $forbidden->save();
        return $forbidden;
    }

    public function delete($id){

        $forbidden=Forbidden::find($id);
        if($forbidden ==null)
        {
            throw new CustomException("forbidden word not found");
        }

        $forbidden->delete();
        return $forbidden;
    }

}
