<?php

namespace App\Http\Services;
use App\Role;
use Illuminate\Support\Facades\Validator;
use Illuminate\Http\Request;
use App\Exceptions\CustomException;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Arr;
use App\Http\Services\RolePermissionService;

class RoleService {

public function add($data){
    try {

        $role = Role::create($data);

        return $role;

    } catch (Throwable $th) {
        throw new CustomException($th);
    }
}

public function get($id){
    $role=Role::find($id);
    if($role==null)
    throw new CustomException("no role found");

    return $role;
}

    public function getAll($request,$pageIndex,$pageSize){
        $roles = new Role();
        if ($request->input('name'))
        {
            $roles= $roles->where('name', $request->input('name'));
        }

        $total = $roles->count();
        if($total==0)
        throw new CustomException("no role found");

        $roles= $roles->skip($pageIndex*$pageSize)->take($pageSize)->orderBy('name')->get();

        $RolePermission = new RolePermissionService();
        if($roles){
            foreach ($roles as $index => $item){
                $roles[$index]->permission_count = count($RolePermission->getByRoleOnly($item->id));
            }
        }

        return [$roles,$total];
    }

    public function Edit($id,$data){

        $role= Role::find($id);
        if($role ==null)
        {
            throw new CustomException("role not found");
        }

        if( Arr::exists($data, 'name'))
        $role->name=$data['name'];

        $role->save();

        return $role;
    }

    public function delete($id){

        $role= Role::find($id);
        if($role ==null)
        {
            throw new CustomException("role not found");
        }

        $role->delete();

        return $role;
    }

}
