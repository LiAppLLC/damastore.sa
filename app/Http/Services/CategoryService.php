<?php

namespace App\Http\Services;
use App\Category;
use Illuminate\Support\Facades\Validator;
use Illuminate\Http\Request;
use App\Exceptions\CustomException;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Arr;
class CategoryService {


public function add($data){
    try {

        $category = Category::create([
            'name' => $data['name'],
            'description' => $data['description'],
            'image_id' => $data['image_id'],
            'display_on_home' => $data['display_on_home'],
            'display_order' => $data['display_order'],
            'parent_id' => $data['parent_id'],
        ]);

        return $category;

    } catch (Throwable $th) {
        throw new CustomException($th);
    }
}

public function get($id){
    $category=Category::find($id);
    if($category ==null)
    throw new CustomException("category not found");
    $category->Load('Image');
    $category->img=$category->Image['path'];

    if($category->parent_id != -1)
    $category->parent = Category::where('id',$category->parent_id)->first();

    return $category;
    }

    public function getSubCategoryFilter(Request $request){
        $subcategory= new Category();

        if ($request->input('name') != null)
        {
            $subcategory= $subcategory->where('name','like', '%'. $request->input('name') .'%');
        }

        if ($request->input('parent_category_id') != null)
        {
            $subcategory= $subcategory->where('parent_id',$request->input('parent_category_id'));
        }

        if ($request->input('display_on_home') != null)
        {
            $subcategory= $subcategory->where('display_on_home',$request->input('display_on_home'));
        }
        $subcategory = $subcategory->get();

        if(count($subcategory)==0)
        throw new CustomException("no sub category found");

        $subs=clone  $subcategory;
        $subs->Load('Image');
        $i=0;
        foreach ($subs as $item ) {
        if($item->Image) {
            $subcategory[$i]->img = $item->Image['path'];
        }
        $i++;
            }

        return $subcategory;

        }

    public function getByFilter($filter){
        $category=Category::where('name',$filter)->orWhere('display_on_home',$filter)
        ->orWhere('display_order',$filter)->get();
        if($category ==null)
        throw new CustomException("category not found");
        $categories1=clone  $category;
        $categories1->Load('Image');
        $i=0;
        foreach ($categories1 as $item ) {
            $category[$i]->img=$item->Image['path'];
            $i++;
        }

        return $category;
        }

    public function getAll($pageIndex=0,$pageSize=30){
        $categories=Category::skip($pageIndex * $pageSize)->take($pageSize)->get();
        if(count($categories)==0)
        throw new CustomException("no categories found");
        $total = count(Category::all());
        $categories1=clone  $categories;
        $categories1->Load('Image');
        $i=0;
        foreach ($categories1 as $item ) {
            $categories[$i]->img=$item->Image['path'];
            $i++;
        }

        return [$categories,$total];
    }

    public function categoryChildren($pageIndex,$pageSize)
    {
        $categories=Category::where('parent_id',-1)->orderBy('display_order');

        if(count($categories->get())==0)
        throw new CustomException("no categories found");

        $total = count($categories->get());
        $categories = $categories->skip($pageIndex*$pageSize)->take($pageSize)->get();

        $categories->Load('Image');

        foreach($categories as $key => $item)
        {
            $children=Category::where('parent_id',$categories[$key]->id)->orderBy('display_order')->get();
            $children->Load('Image');
            $categories[$key]->children = $children;
        }

        return [$categories,$total];
    }

    public function changeOrder($data) {
        if($data==null || count($data)==0)
        throw new CustomException("no order found");
        foreach ($data as $item) {
            $category=Category::find($item['id']);
            if($category==null)
            throw new CustomException("category id ".$item['id']." not found");
            $category->display_order = $item['order'];
            $category->save();
        }
        return $this->getAll();
    }

    public function Edit($id,$data){

        $category=Category::find($id);
        if($category ==null)
        {
            throw new CustomException("category not found");
        }

        if( Arr::exists($data, 'name'))
        $category->name=$data['name'];
        if( Arr::exists($data, 'description'))
        $category->description=$data['description'];
        if( Arr::exists($data, 'image_id'))
        $category->image_id=$data['image_id'];
        if( Arr::exists($data, 'display_on_home'))
        $category->display_on_home=$data['display_on_home'];
        if( Arr::exists($data, 'display_order'))
        $category->display_order=$data['display_order'];

        $category->save();
        return $category;
    }

    public function delete($id){

        $category=Category::find($id);
        if($category ==null)
        {
            throw new CustomException("category not found");
        }

        $subcategory = Category::where('parent_id',$category->id)->first();
        if($subcategory)
            throw new CustomException("can't delete category has children");

        $category->delete();
        return $category;
    }

}
