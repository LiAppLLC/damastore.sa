<?php

namespace App\Http\Services;
use App\File;
use Illuminate\Support\Facades\Validator;
use App\Exceptions\CustomException;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\Http;
use Illuminate\Http\Client\Response;
use Illuminate\Support\Str;
//use Intervention\Image\ImageManagerStatic as Image;
use Carbon\Carbon;
class FileService {

    public function addImage($file, $target_id, $target_name, $onePic = 1){
        try {

            $image = $file;

            if (!empty($image) && $image->isValid()) {
                if ($image->isValid()) {
                    /*            $imageName = md5(uniqid(rand(), true)) . '.jpg';*/
                    $filename= time() . '.' . $image->getClientOriginalExtension();
                    $filename_tosave = $filename;

                    $path = public_path('images');
                    $image->move($path, $filename);


                    $dbfile= new File;

                    $dbfile->Path = $filename_tosave;
                    $dbfile->target_id = $target_id;
                    $dbfile->target_name = $target_name;

                    $filename=url($path.$filename);

                    $dbfile->extension = pathinfo($path.$filename, PATHINFO_EXTENSION);
                    $dbfile->accepted = 1;

                    if($onePic == 1){
                        File::where('target_name', '=', 'PRODUCTS')->where('target_id', '=', $target_id)->delete();
                    }

                    $dbfile->save();

                }

            }

        } catch (Exception $e) {
            throw new CustomException($e);
        }

        return $dbfile;
    }


public function add($file){
    try {

        $image = $file;
//        $image = $request->file('imageBlog');

// $path = str_replace('app\Services', '\public\uploads', dirname(FILE));
        /*    $path = str_replace('app/Http/Controllers/AdminController', '/public/images', dirname(_FILE_));*/
        if (!empty($image) && $image->isValid()) {
            if ($image->isValid()) {
                /*            $imageName = md5(uniqid(rand(), true)) . '.jpg';*/
                $filename= time() . '.' . $image->getClientOriginalExtension();
                $filename_tosave = $filename;

                $path = public_path('images');
                $image->move($path, $filename);


                $dbfile= new File;
                $dbfile->user_id=Auth()->user()->id;

                $dbfile->Path = $filename_tosave;

                $filename=url($path.$filename);

                $dbfile->extension = pathinfo($path.$filename, PATHINFO_EXTENSION);
                $dbfile->accepted = 1;

                $dbfile->save();

            }

        }


//        $path = 'upload/files/store/';
//        $accepted = 1;
//
//        $filename=  Carbon::now()->isoFormat('MM_DD_YYYY_HH_mm_SS').$file->getClientOriginalName();
//
//        $file->move($path, $filename);
//
//        //check for forbidden images
//        $url_image=Str::replaceArray(' ',['%20'],url($path.$filename));
//        $response = Http::asForm()->post('http://www.picpurify.com/analyse/1.1', [
//            'API_KEY' => 'zrkY2e9uAlovffZrf3zNZiwuYlH7h4HY',
//            'task' => 'porn_moderation,suggestive_nudity_moderation,drug_moderation,gore_moderation',
//            'url_image' => $url_image
//        ]);
//
//        if(!$response->ok() || $response['status'] == 'failure')
//            throw new CustomException("picpurify returns error retry request ,or check if key expired");
//        else if($response['final_decision'] != 'OK')
//            {
//                $accepted = 0;
//
//            }
//
//        Image::configure(array('driver' => 'imagick'));
//
//        if($accepted==1)
//        {
//            $image_medium = Image::make($path.$filename)->resize(600, 600);
//            $image_medium->save($path.'med'.$filename);
//
//            $image_small = Image::make($path.$filename)->resize(300, 300);
//            $image_small->save($path.'small'.$filename);
//        }
//
//        $dbfile= new File;
//        $dbfile->user_id=Auth()->user()->id;
//
//        $dbfile->Path = url($path.$filename);
//
//        if($accepted==1)
//        {
//            $dbfile->medium_path = url($path.'med'.$filename);
//            $dbfile->small_path = url($path.'small'.$filename);
//        }
//
//        $filename=url($path.$filename);
//
//        $dbfile->extension = pathinfo($path.$filename, PATHINFO_EXTENSION);
//        $dbfile->accepted = $accepted;
//        $dbfile->save();

    } catch (Exception $e) {
        throw new CustomException($e);
    }

//    return null;
    return $dbfile;
}

public function get($id){
    $file=File::find($id);
    if($file == null)
    throw new CustomException("File not found");

    if($file->accepted == 0)
    throw new CustomException("Forbidden Image");

    return $file;
    }

    public function getUserFiles(){
        $user =Auth()->user()->Files;
        if($user ==null)
        throw new CustomException("File not found");
        return $user;
    }

    public function getByFilter(\Illuminate\Http\Request $request,$pageIndex,$pageSize){
        $file=new File();

        if ($request->input('user_id'))
        {
            $file= $file->where('user_id',$request->input('user_id'));
        }

        if (!is_null($request->input('accepted')))
        {
            $file= $file->where('accepted',$request->input('accepted'));
        }

        $total = $file->count();

        if($total==0)
        throw new CustomException("no image found");

        $file = $file->skip($pageIndex * $pageSize)->take($pageSize)->get();

        return [$file,$total];
        }

   /* public function processImage($image)
    {
            $image = $request->file('image');
            if($image->isValid()){
                $fileName=time().'-'.str_slug($request->p_name,"-").'.'.$image->getClientOriginalExtension();

                $large_image_path = config::$imagePathLarge . $fileName;
                $medium_image_path = config::$imagePathMedium . $fileName;
                $small_image_path = config::$imagePathSmall . $fileName;

                //Resize Image
                File::make($image)->save($large_image_path);
                File::make($image)->resize(600,600)->save($medium_image_path);
                File::make($image)->resize(300,300)->save($small_image_path);
                $request->image = $fileName;
                $this->image = $request->image;
            }
    }*/
}
