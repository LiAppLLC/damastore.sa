<?php

namespace App\Http\Services;
use App\Category;
use App\User;
use App\Company;
use App\Product;
use App\Property;
use App\ProductsTag;
use App\Tag;
use App\productReview;
use App\Action;
use App\Reputation;
use App\Promotion;
use App\CompanyFollower;
use App\Http\Services\TagService;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;
use App\Exceptions\CustomException;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Arr;
class ProductService {
    public function add($data){
        $validator =Validator::make($data,[
            'name'=>'required','string',
            'price'=>'required', 'numeric',
            'description'=>'required', 'string'
        ]);
        if($validator->fails())
            throw new CustomException($validator->messages()->first());

        $product= new Product($data);
        $product->save();
        return $product;
    }

    public function addToCompany($data,$company){

        $company1=Company::find($company);
        if($company1 ==null)
        {
            throw new CustomException("company not found");
        }
        else {

          $prod=new Product($data);
          $prod->company_id= $company;
          if($data['category_id']!=null)
          $prod->category_id= $data['category_id'];

//          if($data['tag_id']!=null)
//              $prod->tag_id= $data->tag_id;

          if($data['property_id']!=null)
              $prod->property_id= $data['property_id'];

          $prod->save();
        }
         return $prod;
    }

public function update($id,$data)
{
    $user= Auth()->user();
    $product=Product::find($id);
    if ($product==null)
        throw new CustomException("Product is not found");

        if( Arr::exists($data, 'name'))
        $product->name=$data['name'];
        if( Arr::exists($data, 'price'))
        $product->price=$data['price'];
        if( Arr::exists($data, 'description'))
        $product->description=$data['description'];

    if( Arr::exists($data, 'image_id'))
        $product->image_id=$data['image_id'];
    if( Arr::exists($data, 'display_on_home'))
        $product->display_on_home=$data['display_on_home'];
    if( Arr::exists($data, 'display_order'))
        $product->display_order=$data['display_order'];
    if( Arr::exists($data, 'published'))
        $product->published=$data['published'];
      $product->save();
return $product;
}

public function updateProductProperties($product_id, $request){
    $product=Product::where('id', '=', $product_id)->first();

    if ($product==null)
        throw new CustomException("Product is not found");


    $productService = new ProductService();
    $res = $productService->getPropertiesTags([$product]);

    if($product->category_id == $request->category_id){


        $differenceRes = $productService->difference($productService->getTagsOfNew($request->Tags, $request), $productService->getTagsOfOld($res[0]->properties));
        $differenceRes['tagsToAdd'] = $productService->getTagsForms($request, $differenceRes['tagsToAdd'], $product_id);
        //$differenceRes['tagsToAdd']
        //$differenceRes['tagsToRemove']

        $tagService = new TagService();
        foreach ($differenceRes['tagsToAdd'] as $item) {
            $tagService->addToProduct($item);
        }
        foreach ($differenceRes['tagsToRemove'] as $item) {
            $tagService->deleteByName($item, $product_id);
        }
    }else{

        $tagsToRemove = $productService->getTagsOfOld($res->properties);
        $tagsToAdd = $productService->getTagsOfNew($request->Tags, $request);

        $tagsToAdd = $productService->getTagsForms($request, $tagsToAdd, $product_id);

        $tagService = new TagService();
        foreach ($tagsToAdd as $item) {
            $tagService->addToProduct($item);
        }
        foreach ($tagsToRemove as $item) {
            $tagService->deleteByName($item, $product_id);
        }

        Product::where('id', '=', $product_id)->update(['category_id' => $request->category_id]);
        $ProductRes = Product::where('id', '=', $product_id)->first();
    }

    return $product_id;
}

public function getTagsForms($req, $arr, $product_id){

    $newArr = [];
    foreach ($req->Tags as $key => $item){
        if(in_array($item['tag_name'], $arr)){
            array_push($newArr, $req->Tags[$key]);
        }
    }
    $arr = [];
    foreach ($req->Tags as $key => $item){
        $item['product_id'] = $product_id;
        array_push($arr, $item);
    }
    return $arr;
}

public function getTagsOfNew($tags){
    $tagsArr = [];
    foreach ($tags as $item){
        if(!in_array($item['tag_name'], $tagsArr)){
            array_push($tagsArr, $item['tag_name']);
        }
    }
    return $tagsArr;
}

public function getTagsOfOld($tags){
    $tagsArr = [];
    foreach ($tags as $item){
        foreach ($item['tags'] as $elem){
            if(!in_array($elem, $tagsArr)){
                array_push($tagsArr, $elem);
            }
        }
    }
    return $tagsArr;
}

public function difference($new_tags, $old_tag){

    $tagsToAdd = [];
    foreach ($new_tags as $item){
        if(!in_array($item, $old_tag)){
            array_push($tagsToAdd, $item);
        }
    }

    $tagsToRemove = [];
    foreach ($old_tag as $item){
        if(!in_array($item, $new_tags)){
            array_push($tagsToRemove, $item);
        }
    }

    return [
        'tagsToAdd' => $tagsToAdd,
        'tagsToRemove' => $tagsToRemove
    ];

}

public function get($id)
{
    $user= Auth()->user();
    $product=Product::find($id);
    if ($product==null)
        throw new CustomException("Product is not found");

    $product->likes = productReview::where('product_id',$product->id)->where('action_id',1)->count();
    $product->dislikes = productReview::where('product_id',$product->id)->where('action_id',2)->count();
    $product->views = productReview::where('product_id',$product->id)->where('action_id',3)->count();
    if($user)
    {
    $product->isLike = productReview::where('product_id',$product->id)->where('user_id',$user->id)->where('action_id',1)->count();
    $product->isDislike = productReview::where('product_id',$product->id)->where('user_id',$user->id)->where('action_id',2)->count();
    }

    $product->Load('Image');
    if($product->Image) {
        $product->img = $product->Image['path'];
    }

    $companyFollower = CompanyFollower::where('company_id',$product->company_id)->count();

    $product->Load('category');

    $product->Load('company.Image','company.cover');
    $product->company->followers = $companyFollower;
    if($product->company->user_id==$user->id)
        $product->company->owner = 1;
    else
        $product->company->owner = 0;

    $product->Load('promotion');
    $product->suggested = Product::where('category_id',$product->category_id)
    ->where('id','!=',$product->id)->take(3)->get();

    return $product;

}
public function getPropertiesTags($Products){
    foreach($Products as $Product)
    {
    if($Product->category_id) {
        $category = Category::where('id', '=', (int)$Product->category_id)->first();
        $property = Property::where('category_id', $category->id)->get();
    }

    $Product->properties = [];
    $element = [];
    foreach ($property as $item){

        $productsTag = ProductsTag::where('product_id', '=', $Product->id)->get();
        $tags = [];
        foreach ($productsTag as $productsTagitem){
            $tag = Tag::where('id', $productsTagitem->tag_id)
            ->where('property_id',$item->id)->first();
            if($tag==null) continue;
            array_push($tags, $tag->name);
        }
        array_push($element, ['propertyName' => $item->name, 'propertyId' => $item->id, 'tags' => $tags]);//dd($temp);
    }
    $Product->properties = $element;
}

    return $Products;
}

public function delete($id)
{
    $product=Product::find($id);
    if ($product==null)
        throw new CustomException("Product is not found");

    $product->forceDelete();
    return $product;
}

public function softdelete($id)
    {
        $product=Product::find($id);
        if ($product==null)
            throw new CustomException("Product is not found");

        $product->delete();
        return $product;
    }

    public function getByCompany($company)
    {
        $c=Company::find($company);
        if($c ==null)
        {
            throw new CustomException("company not found");
        }
        $company1=Product::where('company_id',$company)->get();
        $company1->Load('Image');
        $company1->Load('category');
        $company1->Load('company');
        return $company1;
    }

    public function getByFilter(\Illuminate\Http\Request $request, $pageIndex, $pageSize){

        $user= Auth()->user();

        $product= new Product();

        $promotionService = new PromotionService();
        $promotionService->checkAllValid();
        $promotionProduct = new Product();

        $promotionProduct = $promotionProduct->where('promotion_id','!=','null');

        if ($request->input('name') && !empty($request->input('name')))
        {
            $product= $product->where('name', 'LIKE', '%' . $request->input('name') . '%');
        }

        if ($request->input('minPrice') && !empty($request->input('minPrice')) && $request->input('maxPrice') == null)
        {

            $product= $product->where('price','>=',$request->input('minPrice'));
        }

        if ($request->input('minPrice') == null && $request->input('maxPrice') && !empty($request->input('maxPrice')))
        {

            $product= $product->where('price','<=',$request->input('maxPrice'));
        }

        if ($request->input('minPrice') && !empty($request->input('minPrice')) && $request->input('maxPrice') && !empty($request->input('maxPrice')) )
        {

            $product= $product->where('price','>=',$request->input('minPrice'))
            ->where('price','<=',$request->input('maxPrice'));
        }

        if ($request->input('description') && !empty($request->input('description')))
        {
            $product= $product->where('description',$request->input('description'));
        }

        if (!is_null($request->input('published')) && !empty($request->input('published')))
        {
            $product= $product->where('published',$request->input('published'));
        }

        if (!is_null($request->input('display_on_home')) && !empty($request->input('display_on_home')))
        {
            $product= $product->where('display_on_home',$request->input('display_on_home'));
        }

        if ($request->input('display_order') && !empty($request->input('display_order')))
        {
            $product= $product->where('display_order',$request->input('display_order'));
        }

        if ($request->input('image_id') && !empty($request->input('image_id')))
        {
            $product= $product->where('image_id',$request->input('image_id'));
        }

        if ($request->input('category_id') && !empty($request->input('category_id')))
        {
            $category = Category::where('parent_id',$request->input('category_id'));
            $category_ids = $category->select('id')->get();

            $product= $product->where('category_id',$request->input('category_id'))
            ->orwhereIn('category_id',$category_ids);

            $promotionProduct = $promotionProduct->where('category_id',$request->input('category_id'))
            ->orwhereIn('category_id',$category_ids);
        }

        if ($request->input('company_id') && !empty($request->input('company_id')))
        {
            $product= $product->where('company_id',$request->input('company_id'));
        }

        if ($request->input('tags') && !empty($request->input('tags')))
        {

        foreach($request->tags as $tag)
        {
            $tagIds =[];
            foreach($tag['tag_name'] as $frgmntTag)
            {
                $searchtag = Tag::where('name','like','%' . $frgmntTag . '%')
                ->where('property_id',$tag['property_id'])->get();
                if(count($searchtag)!=0)
                foreach($searchtag as $t)
                    array_push($tagIds,$t->id);
            }
            $producttag = ProductsTag::whereIn('tag_id',$tagIds);
            if($producttag->count()!=0)
            {
            $productIds = $producttag->select('product_id')->get();
            $product = $product->whereIn('id',$productIds);
            }
        }

        }

        $productCount = $product->count();
        $product = $product->skip($pageIndex * $pageSize)->take($pageSize)->orderBy('display_order')->get();
        if(count($product)==0)
            throw new CustomException("product not found");

        $product->Load('Image');
        $product->Load('promotion');
        $i=0;
        foreach ($product as $item ) {
            if($item->Image) {
                $product[$i]->img = $item->Image['path'];

                $product[$i]->like = productReview::where('product_id',$item->id)->where('action_id',1)->count();
                $product[$i]->dislike = productReview::where('product_id',$item->id)->where('action_id',2)->count();
                $product[$i]->view = productReview::where('product_id',$item->id)->where('action_id',3)->count();
                if($user)
                    {
                        $product[$i]->isLike = productReview::where('product_id',$item->id)->where('user_id',$user->id)->where('action_id',1)->count();
                        $product[$i]->isDislike = productReview::where('product_id',$item->id)->where('user_id',$user->id)->where('action_id',2)->count();
                    }
                $product[$i]->isPromoted = 0;
            }
            $i++;
        }

        if($promotionProduct->count()>0)
        {
        $promotions = Promotion::whereIn('id',$promotionProduct->select('promotion_id'));
        $promotions = $promotions->orderBy('value', 'desc')->limit(4);
        $promotionProduct = $promotionProduct->whereIn('promotion_id',$promotions->select('id')->get())->get();

        $promotionProduct->Load('Image');
        $product->Load('promotion');
        $i=0;
        foreach ($promotionProduct as $item ) {
            if($item->Image) {
                $promotionProduct[$i]->img = $item->Image['path'];

                $promotionProduct[$i]->like = productReview::where('product_id',$item->id)->where('action_id',1)->count();
                $promotionProduct[$i]->dislike = productReview::where('product_id',$item->id)->where('action_id',2)->count();
                $promotionProduct[$i]->view = productReview::where('product_id',$item->id)->where('action_id',3)->count();
                if($user)
                    {
                        $promotionProduct[$i]->isLike = productReview::where('product_id',$item->id)->where('user_id',$user->id)->where('action_id',1)->count();
                        $promotionProduct[$i]->isDislike = productReview::where('product_id',$item->id)->where('user_id',$user->id)->where('action_id',2)->count();
                    }
                $promotionProduct[$i]->isPromoted = 1;
            }
            $i++;
        }
        $product = $product->merge($promotionProduct);
    }

        return [$product, $productCount];

    }

public function getByCategory($pageIndex,$pageSize,$category)
{
    $categoryProduct=Product::where('category_id',$category)->skip($pageIndex * $pageSize)->take($pageSize)->get();
    if($categoryProduct ==null)
    {
        throw new CustomException("category not found");
    }
    foreach($categoryProduct as $key => $catProduct)
    {
        $categoryProduct[$key]->load('Image');
        $categoryProduct[$key]->Load('company');
    }
     $total = count(Product::where('category_id',$category)->get());
    return [$categoryProduct,$total];
}


    public function getAll($pageIndex=0,$pageSize=30){
        $products=Product::skip($pageIndex * $pageSize)->take($pageSize)->orderBy('display_order')->get();
        if(count($products)==0)
            throw new CustomException("no Products found");
        $total = count(Product::all());
        $products1=clone  $products;
        $products->Load('Image');
        foreach ($products1 as $key => $item ) {
            if($item->Image){
                $products[$key]->img=$item->Image['path'];
            }else{
                $products[$key]->img='';
            }
        }
        return [$products,$total];
    }


    public function getFullDate($products){
        foreach ($products as $key => $item){

            if($item->company_id) {
                $company = Company::find($item->company_id)->first();
                $products[$key]->company_name = $company->name;
            }

            if($item->category_id) {
                $category = Category::find($item->category_id)->first();
                $products[$key]->category_name = $category->name;
                $property = Property::where('category_id', $category->id)->get();
            }

            $productsTag = ProductsTag::where('product_id', '=', $item->id)->get();
            $arr = [];
            foreach ($productsTag as $productsTagitem){
                $tag = Tag::find($productsTagitem->tag_id)->first();
                array_push($arr, $tag->name);
            }
            $products[$key]->tags = $arr;
        }
        return $products;
    }

    public function like($product_id)
    {
        $user = Auth()->user();
        if($user==null)
            throw new CustomException("No user found");
        $product=Product::find($product_id);
        if ($product==null)
            throw new CustomException("Product is not found");

        $like = Action::where('name','like')->first();
        $dislike = Action::where('name','dislike')->first();
        $none = Action::where('name','none')->first();

        $productreview = productReview::where('user_id',$user->id)->where('product_id',$product_id)->
        where('action_id',$like->id)->first();
        if($productreview)
        {
            $productreview->delete();

            $productreview->load('user');
            $productreview->load('product');
            $productreview->action = $none;
            return $productreview;
        }

        //add repuatation
        $reputationHistoryService = new ReputationHistoryService();
        $reputationHistoryService->addUsersReputation('send like', 'get like', $product_id);

        $productreview = productReview::where('user_id',$user->id)->where('product_id',$product_id)->
        where('action_id',$dislike->id)->first();
        if($productreview)
        {
            $productreview->action_id = $like->id;
            $productreview->update();

            $productreview->load('user');
            $productreview->load('product');
            $productreview->load('action');
            return $productreview;
        }

        $productreview = productReview::create([
            'user_id' => $user->id,
            'product_id' => $product_id,
            'action_id' => $like->id
        ]);

        $productreview->load('user');
        $productreview->load('product');
        $productreview->load('action');

        return $productreview;
    }

    public function dislike($product_id)
    {
        $user = Auth()->user();
        if($user==null)
            throw new CustomException("No user found");
        $product=Product::find($product_id);
        if ($product==null)
            throw new CustomException("Product is not found");

        $like = Action::where('name','like')->first();
        $dislike = Action::where('name','dislike')->first();
        $none = Action::where('name','none')->first();

        $productreview = productReview::where('user_id',$user->id)->where('product_id',$product_id)->
        where('action_id',$dislike->id)->first();
        if($productreview)
        {
            $productreview->delete();

            $productreview->load('user');
            $productreview->load('product');
            $productreview->action = $none;
            return $productreview;
        }

        //add repuatation
        $reputationHistoryService = new ReputationHistoryService();
        $reputationHistoryService->addUsersReputation('send dislike', 'get dislike', $product_id);

        $productreview = productReview::where('user_id',$user->id)->where('product_id',$product_id)->
        where('action_id',$like->id)->first();
        if($productreview)
        {
            $productreview->action_id = $dislike->id;
            $productreview->update();

            $productreview->load('user');
            $productreview->load('product');
            $productreview->load('action');
            return $productreview;
        }

        $productreview = productReview::create([
            'user_id' => $user->id,
            'product_id' => $product_id,
            'action_id' => $dislike->id
        ]);

        $productreview->load('user');
        $productreview->load('product');
        $productreview->load('action');

        return $productreview;
    }

    public function view($product_id)
    {
        $user = Auth()->user();
        if($user==null)
            return;
        $product=Product::find($product_id);
        if ($product==null)
            return;

        $view = Action::where('name','view')->first();

        $productreview = productReview::where('user_id',$user->id)->where('product_id',$product_id)->
        where('action_id',$view->id)->first();
        if($productreview) return;

         //add repuatation
         $reputationHistoryService = new ReputationHistoryService();
         $reputationHistoryService->addUsersReputation('send view', 'get view', $product_id);

        $productreview = productReview::create([
            'user_id' => $user->id,
            'product_id' => $product_id,
            'action_id' => $view->id
        ]);

    }

    public function TopViews($count)
    {
        $products_ids = productReview::select('product_id')->groupBy('product_id')
                                        ->orderByRaw('COUNT(*) DESC')->limit((int)$count)->get();
        $products = Product::whereIn('id',$products_ids)->get();
        $products->Load('Image');

        return $products;
    }

    public function TopPromoted($count)
    {
        $promotionService = new PromotionService();
        $promotionService->checkAllValid();

        $promotions = Promotion::orderBy('value', 'desc')->limit(4);
        $products = Product::whereIn('promotion_id',$promotions->select('id')->get())->get();
        $products->Load('Image');

        return $products;
    }

    public function getAllReviews(\Illuminate\Http\Request $request, $pageIndex, $pageSize)
    {
        $productreview = new productReview();

        if ($request->input('user_id') && !empty($request->input('user_id')))
        {
            $productreview= $productreview->where('user_id',$request->input('user_id'));
        }

        if ($request->input('product_id') && !empty($request->input('product_id')))
        {
            $productreview= $productreview->where('product_id',$request->input('product_id'));
        }

        if ($request->input('action') && !empty($request->input('action')))
        {
            $action = Action::where('name',$request->input('action'))->first();
            if($action==null)
                throw new CustomException("invalid action name");

            $productreview= $productreview->where('action_id',$action->id);
        }

        $total = $productreview->count();
        if($total==0)
        throw new CustomException("no product review found");

        $productreview = $productreview->skip($pageIndex * $pageSize)->take($pageSize)->get();

        $productreview->load('user');
        $productreview->load('product');
        $productreview->load('action');

        return [$productreview, $total];
    }

}
