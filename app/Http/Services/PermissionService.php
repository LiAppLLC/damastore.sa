<?php

namespace App\Http\Services;
use App\Permission;
use App\RolesPermission;
use Illuminate\Support\Facades\Validator;
use Illuminate\Http\Request;
use App\Exceptions\CustomException;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Arr;
class PermissionService {

public function add($data){
    try {

        $permission = Permission::create($data);

        return $permission;

    } catch (Throwable $th) {
        throw new CustomException($th);
    }
}

public function get($id){
    $permission= Permission::find($id);
    if($permission==null)
    throw new CustomException("no permission found");

    return $permission;
}

    public function getAll($request,$pageIndex,$pageSize){
        $permissions = new Permission();
        if ($request->input('name'))
        {
            $permissions= $permissions->where('name', $request->input('name'));
        }

        $total = $permissions->count();
        if($total==0)
        throw new CustomException("no permission found");

        $permissions= $permissions->skip($pageIndex*$pageSize)->take($pageSize)->orderBy('name')->get();

        foreach($permissions as $key => $item)
        {
            $rolesPermission = RolesPermission::where('permission_id',$item->id)->first();
            if($rolesPermission)
                $permissions[$key]->used = 1;
            else
                $permissions[$key]->used = 0;
        }

        return [$permissions,$total];
    }

    public function Edit($id,$data){

        $permission = Permission::find($id);
        if($permission ==null)
        {
            throw new CustomException("permission not found");
        }

        if( Arr::exists($data, 'name'))
        $permission->name=$data['name'];

        $permission->save();

        return $permission;
    }

    public function delete($id){

        $permission= Permission::find($id);
        if($permission ==null)
        {
            throw new CustomException("permission not found");
        }

        $permission->delete();

        return $permission;
    }

}
