<?php

namespace App\Http\Services;
use App\UsersRole;
use App\Role;
use Illuminate\Support\Facades\Validator;
use Illuminate\Http\Request;
use App\Exceptions\CustomException;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Arr;
class UserRoleService {

    public function add($data){
        try {
            $role = Role::where('id',$data['role_id']);
            if($role->count()==0)
                throw new CustomException("role not found");

            $userRole = UsersRole::where('user_id',$data['user_id'])
            ->where('role_id',$data['role_id']);
            if($userRole->count()!=0)
                return;//throw new CustomException("role already exists for this user");
            $userRole = UsersRole::create($data);

            return $userRole;

        } catch (Throwable $th) {
            throw new CustomException($th);
        }
    }

public function addMulti($user_id,$data){

    foreach ($data as $key => $item){
        $data[$key]['user_id'] = $user_id;
    }

    $userRoles = [];
    foreach ($data as $item) {
        $added = self::add($item);
        if($added)
        array_push($userRoles, $added);
    }

    return $userRoles;
}

public function getByUser($user_id,$pageIndex = 0,$pageSize = 1000){
    $userRole = UsersRole::where('user_id',$user_id)->get();

    $role = Role::all();
    foreach($role as $key => $item)
    {
        if($userRole->where('role_id',$item->id)->first())
            $role[$key]->added = 1;
        else
            $role[$key]->added = 0;
    }

    $total = count($role);
    if($total==0)
        throw new CustomException("no role found");

    $role = $role->skip($pageIndex * $pageSize)->take($pageSize);

    return [$role,$total];
}

public function getByUserOnly($user_id){
    $userRole = UsersRole::where('user_id',$user_id);
    $userRole = $userRole->select('role_id');

    $role = Role::whereIn('id',$userRole)->get();

    return $role;
}

    public function delete($data){

        $userRole= UsersRole::where('user_id',$data['user_id'])
        ->where('role_id',$data['role_id'])->first();
        if($userRole ==null)
        {
            return;
            //throw new CustomException("user role not found");
        }

        $userRole->delete();

        return $userRole;
    }

    public function deleteMulti($user_id,$data){

        $deletedUserRoles = [];
        self::getByUser($user_id);
        foreach ($data as $item) {
            $item['user_id'] = $user_id;
            $deleted = self::delete($item);
            if($deleted)
            array_push($deletedUserRoles, $deleted);
        }

        return $deletedUserRoles;
    }

}
