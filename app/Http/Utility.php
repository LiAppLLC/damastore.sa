<?php

namespace App\Http;
use App\Http\Resources\ApiResponse;
use App\Http\Resources\ApiPageResponse;

class Utility{

    public static function ToApi($message,$success,$data,$codeMessage,$status){

        $apiResponse = collect();
        $apiResponse->message =$message;
        $apiResponse->success=$success;
        $apiResponse->data=$data;
        $apiResponse->codeMessage=$codeMessage;
        $apiResponse->status=$status;

        return new ApiResponse($apiResponse) ;

    }

    public static function ToPageApi($message,$success,$data,$total,$codeMessage,$status){

        $apiResponse = collect();
        $apiResponse->message =$message;
        $apiResponse->success=$success;
        $apiResponse->data=$data;
        $apiResponse->total=$total;
        $apiResponse->codeMessage=$codeMessage;
        $apiResponse->status=$status;

        return new ApiPageResponse($apiResponse) ;

    }
}
