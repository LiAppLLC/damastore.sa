<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class TypeAr extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            "id"=>$this->id,
            "category_id"=>$this->category,
            "name"=>$this->ar_name,
            "description"=>$this->ar_description,
            "created_at"=>$this->created_at,
            "updated_at"=>$this->updated_at
        ];
    }
}
