<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class AttributeValuesEn extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        //return parent::toArray($request);
        return[
            'id'=>$this->id,
            'attribute_id'=>$this->attribute_id,
            'product_id' =>$this->product_id,
            'value' =>$this->value,
            'name' =>$this->name
            ];
    }
}
