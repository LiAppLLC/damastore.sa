<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class CategoryAr extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {

        return [
            "id"=>$this->id,
            "name"=>$this->ar_name,
            "types"=>$this->types,
            "description"=>$this->ar_description,
            "image"=>$this->image,
            "svg"=>$this->svg,
            "created_at"=>$this->created_at,
            "updated_at"=>$this->updated_at
            ];
    }
}
