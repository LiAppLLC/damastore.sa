<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class ProductAr extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        // return parent::toArray($request);

        return[
        'id'=>$this->id,
        'product_code'=>$this->product_code,
        'price'=>$this->price,
        'price_sale'=>$this->price_sale,
        'currency'=>$this->currency,
        'quantity'=>$this->quantity,
        'type_id'=>$this->type_id,
        'type'=>$this->type,
        'category_id'=>$this->category_id,
        'category'=>$this->category,
        'attributes'=>$this->attributes,
        'user_id'=>$this->user_id,
        'shipping_costs'=>$this->shipping_costs,
        'unit'=>$this->unit,
        'description'=>$this->ar_description,
        'name'=>$this->ar_name,
        'main_image'=>$this->main_image,
        'images'=>$this->images,
        'short_description'=>$this->ar_short_description,
        'created_at'=>$this->created_at,
        'updated_at'=>$this->updated_at,
        ];
    }
}
