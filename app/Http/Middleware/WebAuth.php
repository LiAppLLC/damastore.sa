<?php

namespace App\Http\Middleware;

use Closure;
use App\Http\Utility;
use JWTAuth;
use Session;

class WebAuth
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $_SESSION['LAST_ACTIVITY'] = time();
        $user = Session::get('userObject');

        if($user) {
            return $next($request);
        } else {
            return redirect('/CustomerLoginRegister');
        }

    }
}
