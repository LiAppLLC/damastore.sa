<?php

namespace App\Http\Middleware;
use Illuminate\Http\Request;
use closure;
use Session;
use App;

class Localization
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request,closure $next)
    {

        $lang = '';
        $ip = $request->ip();
        $apiKey = '0d9ae0e75446b5dc1933f40139a4ff6c';

        $lang = Session::get('lang');//dd($lang);

        if ($lang) {
            App::setLocale($lang);
            return $next($request);
        }
        if($ip != '127.0.0.1'){
            $url = 'http://api.ipstack.com/' . $ip . '?access_key=' . $apiKey;
            $location = json_decode(file_get_contents($url), true);
            if($location) {
                if (sizeof($location['location']['languages']) > 0) {
                    $lang = $location['location']['languages'][0]['code'];
                } else {
                    //$request->ip()
                    $lang = Session::get('lang');

                    if (!$lang) {
                        $lang = App::getLocale();
                    }
                }
            } else {
                //$request->ip()
                $lang = Session::get('lang');

                if (!$lang) {
                    $lang = App::getLocale();
                }
            }
        }
        else {
            //$request->ip()
            $lang = Session::get('lang');//dd($lang);
            if (!$lang) {
                $lang = App::getLocale();
            }
        }

        App::setLocale($lang);

        return $next($request);

    }


}

