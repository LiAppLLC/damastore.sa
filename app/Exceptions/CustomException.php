<?php

namespace App\Exceptions;

use Exception;
use App\Http\Utility;
class CustomException  extends Exception{

    protected $ex;
    public function __construct($message)
    {

        
        $this->ex= $message;
    }

    public function render($request)
    {
     return response( Utility::ToApi($this->ex,false,null,"Exception",500));
    }
    
}