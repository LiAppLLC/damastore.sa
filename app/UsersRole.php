<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UsersRole extends Model
{
    protected $guarded = [];

    public static $tableName = 'users_roles';

    public function Users()
    {
        return $this->hasMany('App\User');
    }
    public function Roles()
    {
        return $this->hasMany('App\Role');
    }
}
