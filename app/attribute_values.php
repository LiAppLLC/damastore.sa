<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class attribute_values extends Model
{
     public function name_of_attribute($langs){

     if($langs=='en'){
        $id = (int)$this->attribute_id;
        return attributes::where('id', $id)->first()->name;
     }
     if($langs=='ar'){
        $id = (int)$this->attribute_id;
        return attributes::where('id', $id)->first()->ar_name;
     }
    }
}
