<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class productReview extends Model
{
    protected $guarded = [];
    protected $table="product_review";

    public function user()
    {
        return $this->belongsTo('App\User', 'user_id', 'id');
    }

    public function product()
    {
        return $this->belongsTo('App\Product', 'product_id', 'id');
    }

    public function action()
    {
        return $this->belongsTo('App\Action', 'action_id', 'id');
    }
}
