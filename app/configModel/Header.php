<?php

namespace App\configModel;

use Illuminate\Database\Eloquent\Model;

class Header extends Model
{
    protected $guarded = [];
    protected $table="headers";
}
