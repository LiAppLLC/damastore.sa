<?php
return[

    'Product Grid'=>'Cuadrícula de producto',
    'Category'=>'Historial de pedidos',
    'Type'=>'Mis direcciones',
    'products'=>'productos',
    'Sale'=>'Reducción',
    'Price Range'=>'Rango de precios',
    'Filter'=>'Filtrar',
    'to'=>'A',
    'from'=>'desde',
    'shop'=>'Su pedido',
  'Add Cart'=>'Agregar carrito',
    'Sale'=>'Reducción',
    'Remove From Cart'=>'Quitar del carrito'
];
