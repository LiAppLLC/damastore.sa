<?php
return[

    'Product Grid'=>'Grille de produits',
    'Category'=>'Catégorie',
    'Type'=>'Type',
    'products'=>'Des produits',
    'Sale'=>'Vente',
    'Price Range'=>'Échelle des prix',
    'Filter'=>'Filtre',
    'to'=>'À',
    'from'=>'De',
    'shop'=>'Boutique',
  'Add Cart'=>'Ajouter un panier',
    'Sale'=>'Vente',
    'Remove From Cart'=>'Retirer du panier'
];
