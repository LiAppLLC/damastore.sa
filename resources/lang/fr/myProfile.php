<?php
return[
    'Email'=>'Email',
    'My Wishlist'=>'Ma liste d\'envies',
    'My Addresses'=>'Mes adresses',
    'Order History'=>'Historique des commandes',
    'Change Password'=>'Changer le mot de passe',
    'Edit Profile'=>'Editer le profil',
    'My Dashbord'=>'Mon Dashbord',
    'My Profile'=>'Mon profil',

    'Edit Profile'=>'Editer le profil',
    'Submit'=>'Soumettre',
    'Cancel'=>'Annuler',
    'Email Address'=>'Adresse électronique',
    'Phone'=>'Téléphone',
    'Born Date'=>'Date de naissance',
    'yyyy-mm-dd'=>'aaaa-mm-jj',
    'Last Name'=>'Nom de famille',
    'First Name'=>'Prénom',
    'User Name'=>'Nom d\'utilisateur',

    'Password'=>'Mot de passe',
    'Repeat Password'=>'Répéter le mot de passe',
    'New Password'=>'nouveau mot de passe',
    'Old Password'=>'ancien mot de passe',
    'Change Password'=>'Changer le mot de passe',
    'Date'=>'Date',
    'Payment Method'=>'Mode de paiement',
    'Price'=>'Prix',
    'Description'=>'La description',
    'Order'=>'Ordre',
    'OrderStatus'=>'Statut de la commande',
    'Image'=>'Image',
    'Order History'=>'Historique des commandes',
    'pendingOrder'=>'commande en cours',
    'Quantity' => 'Quantité',
    'Details' => 'Détails'


];
