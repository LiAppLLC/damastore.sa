<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Authentication Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */

    'test' => 'تيست',
    'dir'=>'rtl',
    'FIND_US'=>'ابحث عنا',
    'LATEST_POSTS'=>'آخر المشاركات',
    'DAMA_STORE'=>'داما ستور',
    'hesitate'=>'لا تترددوا في الاتصال بنا.',
    'Category'=>'الصنف',
    'Type'=>'النوع',
    'From'=>'من السعر',
    'To'=>'إلى السعر',
    'Search'=>'بحث',
    'productDetails'=>'مواصفات المنتج',
    'zoomEnable'=>'انقر لتشغيل عدسة الزوم',
    'Size'=>'الحجم',
    'Quantity'=>'العدد',
    'Unit'=>'الوحدة',
    'ProductCode'=>'رمز المنتج',
    'MostPopular'=>'الأكثر شهرة',
    'attributes'=>'الخصائص',
    'read more'=>'المزيد'

];
