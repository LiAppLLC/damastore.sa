<?php
return[
'Product Grid'=>'شبكة المنتج',
    'Category'=>'الفئة',
    'Type'=>'النوع',
    'products'=>'المنجات',
    'Sale'=>'تخفيض السعر',
    'Price Range'=>'نطاق السعر',
    'Filter'=>'فلتر',
    'to'=>'إلى',
    'from'=>'من',
    'shop'=>'تسوق',
    'Add Cart'=>'اضافة إلى السلة',
    'Sale'=>'تخفيض',
    'Remove From Cart'=>'حذف من السلة'
];
