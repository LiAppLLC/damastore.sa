<?php
return[
    'Email'=>'O email',
    'My Wishlist'=>'Minha Lista de Desejos',
    'My Addresses'=>'Meus endereços',
    'Order History'=>'Histórico de pedidos',
    'Change Password'=>'Mudar senha',
    'Edit Profile'=>'Editar Perfil',
    'My Dashbord'=>'My Dashbord',
    'My Profile'=>'Meu perfil',

    'Edit Profile'=>'Editar Perfil',
    'Submit'=>'Enviar',
    'Cancel'=>'Cancelar',
    'Email Address'=>'Endereço de e-mail',
    'Phone'=>'telefone',
    'Born Date'=>'Data de nascimento',
    'yyyy-mm-dd'=>'aaaa-mm-dd',
    'Last Name'=>'Último nome',
    'First Name'=>'Primeiro nome',
    'User Name'=>'Nome do usuário',

    'Password'=>'Senha',
    'Repeat Password'=>'Repita a senha',
    'New Password'=>'Nova senha',
    'Old Password'=>'Senha Antiga',
    'Change Password'=>'Mudar senha',
    'Date'=>'Encontro',
    'Payment Method'=>'Forma de pagamento',
    'Price'=>'Preço',
    'Description'=>'Descrição',
    'Order'=>'Ordem',
    'OrderStatus'=>'Status do pedido',
    'Image'=>'Imagem',
    'Order History'=>'Histórico de pedidos',
    'pendingOrder'=>'pedido pendente',
    'Quantity' => 'Quantidade',
    'Details' => 'Detalhes'


];
