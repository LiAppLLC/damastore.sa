<?php
return[
    'Email'=>'Email',
    'My Wishlist'=>'Meine Wunschliste',
    'My Addresses'=>'Meine Adressen',
    'Order History'=>'Bestellverlauf',
    'Change Password'=>'Passwort ändern',
    'Edit Profile'=>'Profil bearbeiten',
    'My Dashbord'=>'Mein Dashbord',
    'My Profile'=>'Mein Profil',

    'Edit Profile'=>'Profil bearbeiten',
    'Submit'=>'einreichen',
    'Cancel'=>'Stornieren',
    'Email Address'=>'E-Mail-Addresse',
    'Phone'=>'Telefon',
    'Born Date'=>'Geburtsdatum',
    'yyyy-mm-dd'=>'JJJJ-MM-TT',
    'Last Name'=>'Nachname',
    'First Name'=>'Vorname',
    'User Name'=>'Nutzername',

    'Password'=>'Passwort',
    'Repeat Password'=>'Wiederhole das Passwort',
    'New Password'=>'Neues Kennwort',
    'Old Password'=>'Altes Passwort',
    'Change Password'=>'Passwort ändern',
    'Date'=>'Datum',
    'Payment Method'=>'Zahlungsmethode',
    'Price'=>'Preis',
    'Description'=>'Description',
    'Order'=>'Auftrag',
    'OrderStatus'=>'Bestellstatus',
    'Image'=>'Image',
    'Order History'=>'Bestellverlauf',
    'pendingOrder'=>'Ausstehende Bestellung',
    'Quantity' => 'Menge',
    'Details' => 'Einzelheiten'


];
