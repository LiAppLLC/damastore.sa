<?php
return[

    'Product Grid'=>'Produktraster',
    'Category'=>'Kategorie',
    'Type'=>'Art',
    'products'=>'Produkte',
    'Sale'=>'Verkauf',
    'Price Range'=>'Preisklasse',
    'Filter'=>'Filter',
    'to'=>'Zu',
    'from'=>'Von',
    'shop'=>'Geschäft',
  'Add Cart'=>'Zum Einkaufswagen hinzufügen',
    'Sale'=>'Verkauf',
    'Remove From Cart'=>'Aus dem Warenkorb nehmen'
];
