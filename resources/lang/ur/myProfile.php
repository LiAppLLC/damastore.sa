<?php
return[
    'Email'=>'ای میل',
    'My Wishlist'=>'میری خواہشات کی فہرست',
    'My Addresses'=>'میرے پتے',
    'Order History'=>'آرڈر کی تاریخ',
    'Change Password'=>'پاس ورڈ تبدیل کریں',
    'Edit Profile'=>'پروفائل میں ترمیم کریں',
    'My Dashbord'=>'میرا ڈیش بورڈ',
    'My Profile'=>'میری پروفائل',

    'Edit Profile'=>'پروفائل میں ترمیم کریں',
    'Submit'=>'جمع کرائیں',
    'Cancel'=>'منسوخ کریں',
    'Email Address'=>'آرڈر کی حیثیت',
    'Phone'=>'زیر التواء آرڈر',
    'Born Date'=>'مقدار',
    'yyyy-mm-dd'=>'تفصیل',
    'Last Name'=>'پروڈکٹ گرڈ',
    'First Name'=>'پروڈکٹ',
    'User Name'=>'قیمت کی حد',

    'Password'=>'فلٹر',
    'Repeat Password'=>'پاس ورڈ دوبارہ لکھیے',
    'New Password'=>'نیا پاس ورڈ',
    'Old Password'=>'پرانا پاسورڈ',
    'Change Password'=>'پاس ورڈ تبدیل کریں',
    'Date'=>'تاریخ',
    'Payment Method'=>'ادائیگی کا طریقہ',
    'Price'=>'قیمت',
    'Description'=>'میری پروفائل',
    'Order'=>'ترتیب',
    'OrderStatus'=>'آرڈر اسٹیٹس',
    'Image'=>'پیش کیا',
    'Order History'=>'آرڈر کی تاریخ',
    'pendingOrder'=>'زیر التواء آرڈر',
    'Quantity' => 'مقدار',
    'Details' => 'تفصیلات'


];
