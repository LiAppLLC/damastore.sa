<?php
return[
    'Email'=>'אימייל',
    'My Wishlist'=>'רשימת המשאלות שלי',
    'My Addresses'=>'הכתובות שלי',
    'Order History'=>'היסטוריית הזמנות',
    'Change Password'=>'שנה סיסמא',
    'Edit Profile'=>'ערוך פרופיל',
    'My Dashbord'=>'הדשבורד שלי',
    'My Profile'=>'הפרופיל שלי',

    'Edit Profile'=>'ערוך פרופיל',
    'Submit'=>'שלח',
    'Cancel'=>'לְבַטֵל',
    'Email Address'=>'כתובת דוא"ל',
    'Phone'=>'מכשיר טלפון',
    'Born Date'=>'תאריך לידה',
    'yyyy-mm-dd'=>'yyyy-mm-dd',
    'Last Name'=>'שם משפחה',
    'First Name'=>'שם פרטי',
    'User Name'=>'שם משתמש',

    'Password'=>'סיסמה',
    'Repeat Password'=>'חזור על הסיסמה',
    'Old Password'=>'סיסמה ישנה',
    'New Password'=>'סיסמה חדשה',
    'Change Password'=>'שנה סיסמא',
    'Date'=>'תַאֲרִיך',
    'Payment Method'=>'אמצעי תשלום',
    'Price'=>'מחיר',
    'Description'=>'Description',
    'Order'=>'להזמין',
    'OrderStatus'=>'מצב הזמנה',
    'Image'=>'Image',
    'Order History'=>'היסטוריית הזמנות',
    'pendingOrder'=>'הזמנה בהמתנה',
    'Quantity' => 'כַּמוּת',
    'Details' => 'פרטים'


];
