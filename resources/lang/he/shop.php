<?php
return[

    'Product Grid'=>'רשת מוצרים',
    'Category'=>'קטגוריה',
    'Type'=>'סוּג',
    'products'=>'מוצרים',
    'Sale'=>'מְכִירָה',
    'Price Range'=>'טווח מחירים',
    'Filter'=>'לְסַנֵן',
    'to'=>'ל',
    'from'=>'מ',
    'shop'=>'לִקְנוֹת',
  'Add Cart'=>'הוסף עגלה',
    'Sale'=>'מְכִירָה',
    'Remove From Cart'=>'הסר לעגלה'
];
