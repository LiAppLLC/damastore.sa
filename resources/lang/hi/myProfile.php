<?php
return[
    'Email'=>'ईमेल',
    'My Wishlist'=>'मेरी इच्छा सूची',
    'My Addresses'=>'मेरे पते',
    'Order History'=>'आदेश इतिहास',
    'Change Password'=>'पासवर्ड बदलें',
    'Edit Profile'=>'प्रोफ़ाइल संपादित करें',
    'My Dashbord'=>'माई डैशबॉर्ड',
    'My Profile'=>'मेरी प्रोफाइल',

    'Edit Profile'=>'प्रोफ़ाइल संपादित करें',
    'Submit'=>'प्रस्तुत',
    'Cancel'=>'रद्द करना',
    'Email Address'=>'ईमेल पता',
    'Phone'=>'फ़ोन',
    'Born Date'=>'जन्म तिथि',
    'yyyy-mm-dd'=>'yyyy-mm-dd',
    'Last Name'=>'उपनाम',
    'First Name'=>'पहला नाम',
    'User Name'=>'उपयोगकर्ता नाम',

    'Password'=>'कुंजिका',
    'Repeat Password'=>'पासवर्ड दोहराएं',
    'New Password'=>'नया पासवर्ड',
    'Old Password'=>'पुराना पासवर्ड',
    'Change Password'=>'पासवर्ड बदलें',
    'Date'=>'दिनांक',
    'Payment Method'=>'भुगतान का तरीका',
    'Price'=>'कीमत',
    'Description'=>'विवरण',
    'Order'=>'गण',
    'OrderStatus'=>'आदेश की स्थिति',
    'Image'=>'छवि',
    'Order History'=>'आदेश इतिहास',
    'pendingOrder'=>'लंबित आदेश',
    'Quantity' => 'मात्रा',
    'Details' => 'विवरण'


];
