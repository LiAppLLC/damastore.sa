<?php
return[
    'Email'=>'电子邮件',
    'My Wishlist'=>'我的收藏',
    'My Addresses'=>'我的地址',
    'Order History'=>'订单历史',
    'Change Password'=>'更改密码',
    'Edit Profile'=>'编辑个人资料',
    'My Dashbord'=>'我的达世博',
    'My Profile'=>'我的简历',

    'Edit Profile'=>'编辑个人资料',
    'Submit'=>'提交',
    'Cancel'=>'取消',
    'Email Address'=>'电子邮件地址',
    'Phone'=>'电话',
    'Born Date'=>'出生日期',
    'yyyy-mm-dd'=>'yyyy-mm-dd',
    'Last Name'=>'姓',
    'First Name'=>'姓',
    'User Name'=>'用户名',

    'Password'=>'密码',
    'Repeat Password'=>'重设密码',
    'New Password'=>'新密码',
    'Old Password'=>'旧密码',
    'Change Password'=>'更改密码',
    'Date'=>'日期',
    'Payment Method'=>'付款方法',
    'Price'=>'价钱',
    'Description'=>'描述',
    'Order'=>'订购',
    'OrderStatus'=>'Order Status',
    'Image'=>'图片',
    'Order History'=>'订单状态',
    'pendingOrder'=>'挂单',
    'Quantity' => '数量',
    'Details' => '细节'


];
