<?php
return[
    'Email'=>'Электронное письмо',
    'My Wishlist'=>'Список моих желаний',
    'My Addresses'=>'Мои Адреса',
    'Order History'=>'История заказов',
    'Change Password'=>'Сменить пароль',
    'Edit Profile'=>'Редактировать профиль',
    'My Dashbord'=>'Мой Dashboard',
    'My Profile'=>'Мой профайл',

    'Edit Profile'=>'Edit Profile',
    'Submit'=>'представить',
    'Cancel'=>'Аннулирование',
    'Email Address'=>'Адрес электронной почты',
    'Phone'=>'Телефон',
    'Born Date'=>'Дата рождения',
    'yyyy-mm-dd'=>'гггг-мм-дд',
    'Last Name'=>'Фамилия',
    'First Name'=>'Имя',
    'User Name'=>'Имя пользователя',

    'Password'=>'пароль',
    'Repeat Password'=>'Повторите пароль',
    'New Password'=>'новый пароль',
    'Old Password'=>'Прежний пароль',
    'Change Password'=>'Сменить пароль',
    'Date'=>'Дата',
    'Payment Method'=>'Способ оплаты',
    'Price'=>'Цена',
    'Description'=>'Описание',
    'Order'=>'заказ',
    'OrderStatus'=>'Статус заказа',
    'Image'=>'Образ',
    'Order History'=>'История заказов',
//    num123
    'pendingOrder'=>'Pending Order',
    'Quantity' => 'количество',
    'Details' => 'Детали'


];
