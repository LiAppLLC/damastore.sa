<?php

return[

    'MyProfile'=>'私のプロフィール',
    'Language'=>'言語',
    'Home'=>'ホーム',
//   num123
    'Pages'=>'Pages',
    'Shop'=>'ショップ',
    'SignOut'=>'サインアウト',
    'SignIn'=>'サインイン',
    'Total Cost'=>'総費用',
    'Total'=>'合計',
    'Unit Price'=>'単価',
    'Qty'=>'数量',
    'Type'=>'タイプ',
    'Category'=>'カテゴリー',
    'Description'=>'説明',
    'Image'=>'画像',
    'Continue Shopping'=>'ショッピングを続ける',
    'Checkout'=>'チェックアウト',
    'Product Code'=>'製品コード',
    'Unit'=>'単位',
    'Size'=>'サイズ',
    'Dont have an account yet?'=>'まだアカウントをお持ちではありませんか？',
    'Register Now!'=>'今すぐ登録！',
    'Join us and enjoy shopping online today.'=>'私たちに参加して、今日オンラインショッピングを楽しんでください。',
    'Email Address'=>'電子メールアドレス',
    'Phone'=>'電話',
    'Born Date'=>'生年月日',
    'yyyy-mm-dd'=>'yyyy-mm-dd',
    'Last Name'=>'苗字',
    'First Name'=>'ファーストネーム',
    'User Name'=>'ユーザー名',
    'Password'=>'パスワード',
    'Account Password'=>'アカウントパスワード',
    'Verify'=>'確認',
    'Verification Code'=>'検証コード',
    'Username'=>'ユーザー名',
    'Your username'=>'ユーザー名',
    'Verify your accountyou!'=>'アカウントを確認してください！',
    'or you have the verification code!'=>'またはあなたは確認コードを持っています！',
    'Register'=>'登録',
    'Login'=>'ログインする',
    'Forget My Password!'=>'パスワードを忘れて！',
    'Reset Password'=>'パスワードを再設定する',
    'Sign out'=>'サインアウト',
    'Sign in with Twitter'=>'Twitterでサインイン',
    'Sign in with Facebook'=>'Facebookでサインイン',
    'Your Order'=>'ご注文',
    'Product'=>'製品',
    'alert'=>'注意',
    'hint'=>'ヒント',
    'ContinueShopping'=>'ショッピングを続ける',
    'Product Grid'=>'製品',
    'products'=>'製品',
    'Sale'=>'Sale	セール',
    'Price Range'=>'価格帯',
    'Filter'=>'フィルタ',
    'to'=>'に',
    'from'=>'から',
    'Add Cart'=>'カートを追加',
    'Remove From Cart'=>'カートから削除',
    'Email'=>'Eメール',
    'My Wishlist'=>'私のウィッシュリスト',
    'My Addresses'=>'私の住所',
    'Order History'=>'注文履歴',
    'Change Password'=>'パスワードを変更する',
    'Edit Profile'=>'プロファイル編集',
    'My Dashbord'=>'私のダッシュボード',
    'My Profile'=>'私のプロフィール',
    'Submit'=>'参加する',
    'Cancel'=>'キャンセル',
    'Repeat Password'=>'パスワードを再度入力してください',
    'New Password'=>'新しいパスワード',
    'Old Password'=>'以前のパスワード',
    'Date'=>'日付',
    'Payment Method'=>'支払方法',
    'Price'=>'価格',
    'Order'=>'注文',
    'OrderStatus'=>'注文の状況',
    'pendingOrder'=>'保留中の注文',
    'Quantity' => '量',
    'Details' => '詳細',
//    num123
    'Registeration'=>'Registeration',
    'LOADMORE'=>'LOAD MORE',
    'LOADING'=>'LOADING',
    'NOMOREWORKS'=>'NO MORE WORKS'







//    'MyProfile'=>'',
//    'Language'=>'',
//    'Home'=>'',
//    'Pages'=>'',
//    'Shop'=>'',
//    'SignOut'=>'',
//    'SignIn'=>'',
//    'Total Cost'=>'',
//    'Total'=>'',
//    'Unit Price'=>'',
//    'Qty'=>'',
//    'Type'=>'',
//    'Category'=>'',
//    'Description'=>'',
//    'Image'=>'',
//    'Continue Shopping'=>'',
//    'Checkout'=>'',
//    'Product Code'=>'',
//    'Unit'=>'',
//    'Size'=>'',
//    'Dont have an account yet?'=>'',
//    'Register Now!'=>'',
//    'Join us and enjoy shopping online today.'=>'',
//    'Email Address'=>'',
//    'Phone'=>'',
//    'Born Date'=>'',
//    'yyyy-mm-dd'=>'',
//    'Last Name'=>'',
//    'First Name'=>'',
//    'User Name'=>'',
//    'Password'=>'',
//    'Account Password'=>'',
//    'Verify'=>'',
//    'Verification Code'=>'',
//    'Username'=>'',
//    'Your username'=>'',
//    'Verify your accountyou!'=>'',
//    'or you have the verification code!'=>'',
//    'Register'=>'',
//    'Login'=>'',
//    'Forget My Password!'=>'',
//    'Reset Password'=>'',
//    'Sign out'=>'',
//    'Sign in with Twitter'=>'',
//    'Sign in with Facebook'=>'',
//    'Your Order'=>'',
//    'Product'=>'',
//    'alert'=>'',
//    'hint'=>'',
//    'ContinueShopping'=>'',
//    'Product Grid'=>'',
//    'products'=>'',
//    'Sale'=>'',
//    'Price Range'=>'',
//    'Filter'=>'',
//    'to'=>'',
//    'from'=>'',
//    'Add Cart'=>'',
//    'Remove From Cart'=>'',
//    'Email'=>'',
//    'My Wishlist'=>'',
//    'My Addresses'=>'',
//    'Order History'=>'',
//    'Change Password'=>'',
//    'Edit Profile'=>'',
//    'My Dashbord'=>'',
//    'My Profile'=>'',
//    'Submit'=>'',
//    'Cancel'=>'',
//    'Repeat Password'=>'',
//    'New Password'=>'',
//    'Old Password'=>'',
//    'Date'=>'',
//    'Payment Method'=>'',
//    'Price'=>'',
//    'Order'=>'',
//    'OrderStatus'=>'',
//    'pendingOrder'=>'',
//    'Quantity' => '',
//    'Details' => ''

];
