<?php
return[
    'Email'=>'Eメール',
    'My Wishlist'=>'私のウィッシュリスト',
    'My Addresses'=>'私の住所',
    'Order History'=>'注文履歴',
    'Change Password'=>'パスワードを変更する',
    'Edit Profile'=>'プロファイル編集',
    'My Dashbord'=>'私のダッシュボード',
    'My Profile'=>'プロファイル編集',

    'Edit Profile'=>'プロファイル編集',
    'Submit'=>'参加する',
    'Cancel'=>'キャンセル',
    'Email Address'=>'電子メールアドレス',
    'Phone'=>'電話',
    'Born Date'=>'生年月日',
    'yyyy-mm-dd'=>'yyyy-mm-dd',
    'Last Name'=>'苗字',
    'First Name'=>'名字',
    'User Name'=>'用户名',

    'Password'=>'パスワード',
    'Repeat Password'=>'パスワードを再度入力してください',
    'New Password'=>'新しいパスワード',
    'Old Password'=>'以前のパスワード',
    'Change Password'=>'パスワードを変更する',
    'Date'=>'日付',
    'Payment Method'=>'支払方法',
    'Price'=>'価格',
    'Description'=>'説明',
    'Order'=>'注文',
    'OrderStatus'=>'注文の状況',
    'Image'=>'画像',
    'Order History'=>'注文履歴',
    'pendingOrder'=>'保留中の注文',
    'Quantity' => '量',
    'Details' => '詳細'


];
