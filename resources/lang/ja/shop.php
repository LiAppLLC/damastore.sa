<?php
return[

    'Product Grid'=>'製品グリッド',
    'Category'=>'カテゴリー',
    'Type'=>'タイプ',
    'products'=>'製品',
    'Sale'=>'セール',
    'Price Range'=>'価格帯',
    'Filter'=>'フィルタ',
    'to'=>'に',
    'from'=>'から',
    'shop'=>'ショップ',
  'Add Cart'=>'カートを追加',
    'Sale'=>'セール',
    'Remove From Cart'=>'カートから削除'
];
