<?php
return[
    'Email'=>'이메일',
    'My Wishlist'=>'내 위시리스트',
    'My Addresses'=>'내 주소',
    'Order History'=>'주문 내역',
    'Change Password'=>'비밀번호 변경',
    'Edit Profile'=>'프로필 편집',
    'My Dashbord'=>'내 Dashbord',
    'My Profile'=>'내 프로필',

    'Edit Profile'=>'프로필 편집',
    'Submit'=>'제출',
    'Cancel'=>'취소',
    'Email Address'=>'이메일 주소',
    'Phone'=>'전화',
    'Born Date'=>'태어난 날짜',
    'yyyy-mm-dd'=>'yyyy-mm-dd',
    'Last Name'=>'성',
    'First Name'=>'이름',
    'User Name'=>'사용자 이름',

    'Password'=>'암호',
    'Repeat Password'=>'비밀번호 반복',
    'New Password'=>'새 비밀번호',
    'Old Password'=>'기존 비밀번호',
    'Change Password'=>'비밀번호 변경',
    'Date'=>'데이트',
    'Payment Method'=>'결제 방법',
    'Price'=>'가격',
    'Description'=>'기술',
    'Order'=>'주문',
    'OrderStatus'=>'주문 상태',
    'Image'=>'영상',
    'Order History'=>'주문 내역',
    'pendingOrder'=>'Pending Order',
    'Quantity' => '수량',
    'Details' => '세부'


];
