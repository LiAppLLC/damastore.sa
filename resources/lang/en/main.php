<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Authentication Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */

    'test' => 'test',
    'dir'=>'',
    'FIND_US'=>'Find Us',
    'LATEST_POSTS'=>'Latest Update',
    'DAMA_STORE'=>'DamaStore',
    'hesitate'=>'Do not hesitate to contact us.',
    'Category'=>'Category',
    'Type'=>'Type',
    'From'=>'From price',
    'To'=>'To price',
    'Search'=>'Search',
    'productDetails'=>'Product Details',
    'zoomEnable'=>'Click to operate the zoom lens',
    'Size'=>'Size',
    'Quantity'=>'Quantity',
    'Unit'=>'Unit',
    'ProductCode'=>'Product Code',
    'MostPopular'=>'Most Popular',
    'attributes'=>'Attributes',
    'read more'=>'Read More'

];
