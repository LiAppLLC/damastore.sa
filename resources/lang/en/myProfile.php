<?php
return[
    'Email'=>'Email',
    'My Wishlist'=>'My Wishlist',
    'My Addresses'=>'My Addresses',
    'Order History'=>'Order History',
    'Change Password'=>'Change Password',
    'Edit Profile'=>'Edit Profile',
    'My Dashbord'=>'My Dashbord',
    'My Profile'=>'My Profile',

    'Edit Profile'=>'Edit Profile',
    'Submit'=>'Submit',
    'Cancel'=>'Cancel',
    'Email Address'=>'Email Address',
    'Phone'=>'Phone',
    'Born Date'=>'Born Date',
    'yyyy-mm-dd'=>'yyyy-mm-dd',
    'Last Name'=>'Last Name',
    'First Name'=>'First Name',
    'User Name'=>'User Name',

    'Password'=>'Password',
    'Repeat Password'=>'Repeat Password',
    'New Password'=>'New Password',
    'Old Password'=>'Old Password',
    'Change Password'=>'Change Password',
    'Date'=>'Date',
    'Payment Method'=>'Payment Method',
    'Price'=>'Price',
    'Description'=>'Description',
    'Order'=>'Order',
    'OrderStatus'=>'Order Status',
    'Image'=>'Image',
    'Order History'=>'Order History',
    'pendingOrder'=>'Pending Order',
    'Quantity' => 'Quantity',
    'Details' => 'Details'


];
