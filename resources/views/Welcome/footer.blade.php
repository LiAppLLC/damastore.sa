<style>
    .c-theme-font, .c-theme-color {
        color: gray !important;
    }
    .c-layout-footer.c-layout-footer-3 .c-prefooter .c-container .c-blog > .c-post > .c-post-content > .c-post-title a:hover {
        color: #f3f3f3 !important;
    }
    .c-layout-footer.c-layout-footer-3 .c-prefooter .c-container .c-links > li > a:hover {
        color: #f3f3f3 !important;
    }
    .c-layout-footer.c-layout-footer-3 .c-prefooter .c-container .c-socials > li > a > i {
        position: relative;
        text-align: center;
        display: inline-block;
        padding: 11px;
        width: 38px;
        background: white;
         color: #f99d1c !important;
        font-size: 18px;
        border-radius: 50%;
        font-weight: bold;
        box-shadow: 0 0 9px #00000038;
    }
    .c-layout-go2top {
        bottom: 69px !important;
        right: 29px !important;
    }
    .whatsapp-button{
        position: fixed;
        bottom: 15px;
        left: 15px;
        z-index: 99;
        background-color: #25d366;
        border-radius: 50px;
        color: #ffffff;
        text-decoration: none;
        width: 60px;
        height: 60px;
        font-size: 30px;
        display: flex;
        flex-direction: column;
        justify-content: center;
        align-items: center;
        -webkit-box-shadow: 0px 0px 25px -6px rgba(0,0,0,1);
        -moz-box-shadow: 0px 0px 25px -6px rgba(0,0,0,1);
        box-shadow: 0px 0px 25px -6px rgba(0,0,0,1);
        animation: effect 5s infinite ease-in;
    }

    @keyframes effect {
        20%, 100% {
            width: 60px;
            height: 60px;
            font-size: 40px;
        }
        0%, 10%{
            width: 65px;
            height: 65px;
            font-size: 45px;
        }
        5%{
            width: 60px;
            height: 60px;
            font-size: 40px;
        }
    }
    @keyframes effect2 {
        20%, 100% {
            width: 60px;
            height: 60px;

        }
        0%, 10%{
            width: 65px;
            height: 65px;

        }
        5%{
            width: 60px;
            height: 60px;

        }
    }
    .fb_dialog_content iframe{
        bottom: 82px !important;
    left: 2px !important;
        animation: effect2 5s infinite ease-in;
    }
</style>
<!-- START Bootstrap-Cookie-Alert -->
<div class="alert text-center cookiealert" role="alert">
    <b>
        @lang('home.cookies1')
    </b>
        @lang('home.cookies2')
     <a href="https://cookiesandyou.com/" target="_blank">
        @lang('home.cookies3')
    </a>

    <button type="button" class="btn btn-primary btn-sm acceptcookies">
        @lang('home.cookies4')
    </button>
</div>
<!-- END Bootstrap-Cookie-Alert -->
<!-- GetButton.io widget -->
<a target="_blank" href="https://api.whatsapp.com/send?phone=+12017333228&text=" class="whatsapp-button"><i class="fab fa-whatsapp"></i></a>
<!-- /GetButton.io widget -->
<div class="divsearch">
    <h2 style="width: 100%;border-bottom: 1px solid gray">@lang('main.Search')</h2>
    <button type="button" class="close_search" onclick="close_function()">
        <span>&times;</span>
    </button>
    <div class="row" style="margin-bottom:5px">
        <div class="col-md-12">
            <input id="text_search" class="form-control" placeholder="@lang('main.Search') ..."/>
        </div>
    </div>
    <div class="row">
        <div class="col-md-3" style="margin-bottom:5px">
            <select onchange="get_type(this.value)" id="category_search" class="form-control">
                <option value="">@lang('main.Category')</option>
                {{-- @foreach($CategoryForSearch as $category)
                <option value="{{$category->id}}">{{$category->name}}</option>
                @endforeach --}}
            </select>
        </div>
        <div class="col-md-3" style="margin-bottom:5px">
            <select  id="type_search" class="form-control">
                <option value="">@lang('main.Type')</option>
                {{-- @foreach($TypeForSearch as $Type)
                <option value="{{$Type->id}}">{{$Type->name}}</option>
                @endforeach --}}
            </select>
        </div>
        <div class="col-md-3" style="margin-bottom:5px">
            <input id="from_search" class="form-control" type="number" placeholder="@lang('main.From')">
        </div>
        <div class="col-md-3" style="margin-bottom:5px">
            <input id="to_search" class="form-control" type="number" placeholder="@lang('main.To')">
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            <button class="price_class" onclick="getsearch()" style="font-size:14px">
                @lang('main.Search')
            </button>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            <div id="result"></div>
        </div>
    </div>
</div>
<!-- BEGIN: LAYOUT/FOOTERS/FOOTER-5 -->
<a class="navigation"  name="footer"></a>
<footer class="c-layout-footer c-layout-footer-3 c-bg-dark">
    <div class="c-prefooter">
        <div class="container">
            <div class="row">
                <div class="col-md-3">
                    <div class="c-container c-first">
                        <div class="c-content-title-1">
                            <h3 class="c-font-bold c-font-white">
                                <img src="{{asset('assets_new/img/logo_2.png')}}" width="40px" />
                                @lang('main.DAMA_STORE')<span class="c-theme-font"></span></h3>
                            <div class="c-line-left hide"></div>

                        </div>
                        <ul class="c-links">
                            <li><a class="navigation"  href="/">@lang('home.11')</a></li>
                            <li><a class="navigation"  href="/about">@lang('home.12')</a></li>
                            <li><a class="navigation"  href="/terms">@lang('home.13')</a></li>
                            <li><a class="navigation"  href="/return">@lang('home.14')</a></li>
                            <li><a class="navigation"  href="/contact">@lang('home.15')</a></li>
                            <li><a class="navigation"  href="/Deletion">@lang('home.16')</a></li>
                        </ul>
                    </div>
                </div>
                <div class="col-md-5">
                    <div class="c-container">
                        <div class="c-content-title-1">
                            <h3 class="c-font-bold c-font-white">
                                <i class="icon-clock c-theme-font" style="color: white !important;"></i>
                                @lang('main.LATEST_POSTS')
                            </h3>
                            <div class="c-line-left hide"></div>
                        </div>
                        <div class="c-blog">
                            @foreach ($lastproduct as $product)
                                <div class="c-post">
                                    <div class="c-post-img"><img src="/images/{{$product->getImageUrl()}}" alt="" style="width:35px;height:35px;object-fit: cover"/></div>
                                    <div class="c-post-content">
                                        <h4 class="c-post-title"><a class="navigation"  href="/productDetails/{{$product->id}}">
                                                @if(app()->getLocale()=='ar')
                                                    {{$product->ar_name}}
                                                @else
                                                    {{$product->name}}
                                                @endif
                                            </a></h4>
                                        <p class="c-text">
                                            @if(app()->getLocale()=='ar')
                                                {{$product->ar_short_description}}
                                            @else
                                                {{$product->short_description}}
                                            @endif
                                        </p>
                                    </div>
                                </div>
                            @endforeach
                            <a href="/shop" class="navigation btn btn-md c-btn-border-1x c-theme-btn c-btn-uppercase c-btn-square c-btn-bold c-read-more hide">Read More</a>
                        </div>
                    </div>
                </div>

                <div class="col-md-4">
                    <div class="c-container c-last">
                        <div class="c-content-title-1">
                            <h3 class=" c-font-bold c-font-white">
                                <i class="icon-pointer c-theme-font" style="color: white !important;"></i>
                                @lang('main.FIND_US')
                            </h3>
                            <div class="c-line-left hide"></div>
                            <p>
                                3 Germay Dr,Unit 4 #1541
                                Wilmington DE 19804
                                United States Of America</p>
                        </div>
                        <p class="c-text">
                            @lang('main.hesitate')</p>
                        <ul class="c-socials">
                            <li><a class="navigation"  href="https://facebook.com/Damastore2020"><i class="fa fa-facebook" aria-hidden="true"></i></a></li>
                            <li><a class="navigation"  href="https://www.instagram.com/Damastore50"><i class="fa fa-instagram" aria-hidden="true"></i></a></li>
                            <li><a class="navigation"  href="https://twitter.com/Damastore1"><i class="fa fa-twitter" aria-hidden="true"></i></a></li>
                            <li><a class="navigation"  href="https://www.linkedin.com/company/liappllc"><i class="fa fa-linkedin" aria-hidden="true"></i></a></li>

                        </ul>
                        <ul class="c-address">
                            <li>
                                <i class="icon-envelope c-theme-font"></i>
                                Customer-service@li-app.com</li>
                            <li><i class="icon-call-end c-theme-font"></i> +1 (201) 7332228</li>
                            {{--                            <li><i class="icon-envelope c-theme-font"></i></li>--}}
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="c-postfooter">
        <div class="container">
            <div class="row">
                <div class="col-md-6 col-sm-12 c-col">
                    <p class="c-copyright c-font-grey">2021 Powerd by &copy; Li App LLC
                        <span class="c-font-grey-3">All Rights Reserved.</span>
                    </p>
                </div>
            </div>
        </div>
    </div>
</footer><!-- END: LAYOUT/FOOTERS/FOOTER-5 -->

<!-- BEGIN: LAYOUT/FOOTERS/GO2TOP -->
<div class="c-layout-go2top">
    <i class="icon-arrow-up"></i>
</div>

<!-- Load Facebook SDK for JavaScript -->
<div id="fb-root"></div>
<script>
  window.fbAsyncInit = function() {
    FB.init({
      xfbml            : true,
      version          : 'v10.0'
    });
  };

  (function(d, s, id) {
  var js, fjs = d.getElementsByTagName(s)[0];
  if (d.getElementById(id)) return;
  js = d.createElement(s); js.id = id;
  js.src = 'https://connect.facebook.net/en_US/sdk/xfbml.customerchat.js';
  fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));
// FB.CustomerChat.show(shouldShowDialog: true);
</script>

<!-- Your Chat Plugin code -->
<div class="fb-customerchat"
  attribution="setup_tool"
  page_id="110462564198889"
  greeting_dialog_display="show"  theme_color="#ff7e29"
  >
</div>
<!-- The core Firebase JS SDK is always required and must be listed first -->
<script src="https://www.gstatic.com/firebasejs/8.2.9/firebase-app.js"></script>

<!-- TODO: Add SDKs for Firebase products that you want to use
     https://firebase.google.com/docs/web/setup#available-libraries -->
<script src="https://www.gstatic.com/firebasejs/8.2.9/firebase-analytics.js"></script>
<script src="https://www.gstatic.com/firebasejs/8.2.9/firebase-messaging.js"></script>

<script src="https://cdn.jsdelivr.net/gh/Wruczek/Bootstrap-Cookie-Alert@gh-pages/cookiealert.js"></script>
<script src="{{asset('https://cdnjs.cloudflare.com/ajax/libs/jquery/3.5.1/jquery.min.js')}}" integrity="sha512-bLT0Qm9VnAYZDflyKcBaQ2gg0hSYNQrJ8RilYldYQ1FxQYoCLtUjuuRuZo+fjqhx/qtq/1itJ0C2ejDxltZVFg==" crossorigin="anonymous"></script>
{{--<script src="{{asset('assets/global/plugins/excanvas.min.js')}}"></script>--}}
{{-- <script
  src="https://code.jquery.com/jquery-3.6.0.js"
  integrity="sha256-H+K7U5CnXl1h5ywQfKtSj8PCmoN9aaq30gDh27Xc0jk="
  crossorigin="anonymous"></script> --}}
<script src="{{asset('assets/plugins/jquery.min.js')}}" type="text/javascript"></script>
{{-- <script src="{{asset('firebase-messaging-sw.js')}}" type="text/javascript"></script> --}}
<script src="https://unpkg.com/swiper@6.4.10/swiper-bundle.min.js"></script>
{{-- <script
  src="https://code.jquery.com/jquery-migrate-3.4.0.js"
  integrity="sha256-0Nkb10Hnhm4EJZ0QDpvInc3bRp77wQIbIQmWYH3Y7Vw="
  crossorigin="anonymous"></script> --}}
<script src="{{asset('assets/plugins/jquery-migrate.min.js')}}" type="text/javascript"></script>
<script src="{{asset('assets/plugins/bootstrap/js/bootstrap.min.js')}}" type="text/javascript"></script>
<script src="{{asset('assets/plugins/jquery.easing.min.js')}}" type="text/javascript"></script>
<script src="{{asset('assets/plugins/reveal-animate/wow.js')}}" type="text/javascript"></script>
<script src="{{asset('assets/demos/index/js/scripts/reveal-animate/reveal-animate.js')}}" type="text/javascript"></script>
<script src="{{asset('assets/plugins/revo-slider/js/jquery.themepunch.tools.min.js')}}" type="text/javascript"></script>
<script src="{{asset('assets/plugins/revo-slider/js/jquery.themepunch.revolution.min.js')}}" type="text/javascript"></script>
<script src="{{asset('assets/plugins/revo-slider/js/extensions/revolution.extension.slideanims.min.js')}}" type="text/javascript"></script>
<script src="{{asset('assets/plugins/revo-slider/js/extensions/revolution.extension.layeranimation.min.js')}}" type="text/javascript"></script>
<script src="{{asset('assets/plugins/revo-slider/js/extensions/revolution.extension.navigation.min.js')}}" type="text/javascript"></script>
<script src="{{asset('assets/plugins/revo-slider/js/extensions/revolution.extension.video.min.js')}}" type="text/javascript"></script>
<script src="{{asset('assets/plugins/revo-slider/js/extensions/revolution.extension.parallax.min.js')}}" type="text/javascript"></script>
<script src="{{asset('assets/plugins/cubeportfolio/js/jquery.cubeportfolio.min.js')}}" type="text/javascript"></script>
<script src="{{asset('assets/plugins/owl-carousel/owl.carousel.min.js')}}" type="text/javascript"></script>
<script src="{{asset('assets/plugins/counterup/jquery.waypoints.min.js')}}" type="text/javascript"></script>
<script src="{{asset('assets/plugins/counterup/jquery.counterup.min.js')}}" type="text/javascript"></script>
<script src="{{asset('assets/plugins/fancybox/jquery.fancybox.pack.js')}}" type="text/javascript"></script>
<script src="{{asset('assets/plugins/smooth-scroll/jquery.smooth-scroll.js')}}" type="text/javascript"></script>
<script src="{{asset('assets/plugins/typed/typed.min.js')}}" type="text/javascript"></script>
<script src="{{asset('assets/plugins/slider-for-bootstrap/js/bootstrap-slider.js')}}" type="text/javascript"></script>
<script src="{{asset('assets/plugins/js-cookie/js.cookie.js')}}" type="text/javascript"></script>
<script src="{{asset('assets/base/js/components.js')}}" type="text/javascript"></script>
<script src="{{asset('assets/base/js/components-shop.js')}}" type="text/javascript"></script>
<script src="{{asset('assets/base/js/app.js')}}" type="text/javascript"></script>
<script src="{{asset('assets/demos/default/js/scripts/revo-slider/slider-4.js')}}" type="text/javascript"></script>
<script src="{{asset('assets/plugins/isotope/isotope.pkgd.min.js')}}" type="text/javascript"></script>
<script src="{{asset('assets/plugins/isotope/imagesloaded.pkgd.min.js')}}" type="text/javascript"></script>
<script src="{{asset('assets/plugins/isotope/packery-mode.pkgd.min.js')}}" type="text/javascript"></script>
<script src="{{asset('assets/plugins/ilightbox/js/jquery.requestAnimationFrame.js')}}" type="text/javascript"></script>
<script src="{{asset('assets/plugins/ilightbox/js/jquery.mousewheel.js')}}" type="text/javascript"></script>
<script src="{{asset('assets/plugins/ilightbox/js/ilightbox.packed.js')}}"  type="text/javascript"></script>
<script src="{{asset('assets/demos/default/js/scripts/pages/isotope-gallery.js')}}" type="text/javascript"></script>
<script src="{{asset('assets/plugins/revo-slider/js/extensions/revolution.extension.parallax.min.js')}}" type="text/javascript"></script>
<script src="{{asset('assets/plugins/revo-slider/js/extensions/revolution.extension.kenburn.min.js')}}" type="text/javascript"></script>
<script src="https://cdn.jsdelivr.net/npm/sweetalert2@10"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/owl-carousel/1.3.3/owl.carousel.min.js"></script>
<script src="https://i-like-robots.github.io/EasyZoom/dist/easyzoom.js"></script>
<script src="/assets_new/slick/slick.js" type="text/javascript" charset="utf-8"></script>
<script type="text/javascript" src="//connect.facebook.net/en_US/sdk.js"></script>
@if(Route::currentRouteName()=='cart' || Route::currentRouteName()=='CustomerLoginRegister' || Route::currentRouteName()=='order')
    <script src="https://www.paypalobjects.com/api/checkout.js" data-version-4></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.19.2/jquery.validate.min.js" data-version-4></script>
@endif
<script>
    function opensearch(){
        $('.divsearch').show('slow');
        $.ajax({

            type: "POST",
            url: '{{route('categories_api')}}',
            data: null,
// mimeType: 'application/json',
// headers: {"Authorization": 'Bearer ' + userObject.token},
            processData: false,
            contentType: false,
            success: function (res) {
                if(res.status == 200) {
                    //console.log('category', res);
                    res.data.forEach(function(item, index, arr) {
                        if(index == 0){
                            $('#category_search').append('<option value="' + item.id + '">' + item.name + '</option>');
                        }else {
                            $('#category_search').append('<option value="' + item.id + '">' + item.name + '</option>');
                        }
                    });
                    // get_type('1');
                }
            },
            error: function (res, response) {
                $( ".loading" ).remove();
            },
        });
    }
    function getsearch(){
        let text = $('#text_search').val();
        let category = $('#category_search').val();
        let type = $('#type_search').val();
        let from = $('#from_search').val();
        let to = $('#to_search').val();
        let Resulte=$('#result');
        Resulte.html('<i class="fa fa-spinner fa-spin"></i>');
        let formData_getProds = new FormData();
        formData_getProds.append('text', text);
        formData_getProds.append('category', category);
        formData_getProds.append('type', type);
        formData_getProds.append('from', from);
        formData_getProds.append('to', to);
        // console.log(formData_getProds);
        $.ajax({
            type: "POST",
            url: '/api/searchByText/0/9999999',
            data: formData_getProds,
            //mimeType: 'application/json',
            headers: {"currency":"{{session('currency')}}",
                "language":"{{session('lang')}}"},
            processData: false,
            contentType: false,
            success: function (res) {
                if(res.status == 200) {
                    Resulte.text('');
                    Resulte.html('');
                    for(var i in res.data.data){
                        Resulte.append('<a href="/productDetails/' + res.data.data[i].id+'"><div style="font-size:12px" class="row se_main align-items-center"><div class="col-md-4 se1"><img src="/'+res.data.data[i].main_image[0].path+'"/></div><div class="col-md-4 se2"><p>'+res.data.data[i].name+'</p></div><div class="col-md-4 se3"><p>'+res.data.data[i].short_description+'<br>'+res.data.data[i].price+' '+res.data.data[i].currency+'</p></div></div>')
                    }
                }
            },
            error: function(res){
                console.log(res);
            }
        });
    }
    function close_function() {
        $('.divsearch').hide('slow');
    }

    function get_type(id){
        var userObject = JSON.parse(localStorage.getItem("DamaStore_userObject"));
        var formData = new FormData();
        formData.append('category_id',id)
        $('#type').html('');
        $.ajax({
            type: "POST",
            url: '{{route('types_api')}}',
            data:formData,
            // mimeType: 'application/json',
            // headers: {"Authorization": 'Bearer ' + userObject.token},
            processData: false,
            contentType: false,
            success: function (res) {
                if(res.status == 200) {
                    console.log('Type', res);
                    res.data.forEach(function(item, index, arr) {
                        if(index == 0){
                            $('#type_search').append('<option selected="selected" value="' + item.id + '">' + item.name + '</option>');
                        }else {
                            $('#type_search').append('<option value="' + item.id + '">' + item.name + '</option>');
                        }
                    });
                }
            },
            error: function (res, response) {
                $( ".loading" ).remove();
            },
        });

    }
    window.addEventListener("cookieAlertAccept", function() {
    // alert("cookies accepted")
})
</script>


<script>

    var userObject = JSON.parse(localStorage.getItem("DamaStore_userObject"));
      var firebaseConfig = {
      apiKey: "AIzaSyCiUPH6xsFh8XzPHcxQTnyinXTpJQA_rQs",
      authDomain: "damastore-20a76.firebaseapp.com",
      databaseURL: "https://damastore-20a76.firebaseio.com",
      projectId: "damastore-20a76",
      storageBucket: "damastore-20a76.appspot.com",
      messagingSenderId: "614626349242",
      appId: "1:614626349242:web:30baab3b46cfc4e2f1b283",
      measurementId: "G-PR8PXQ1928"
      };

      firebase.initializeApp(firebaseConfig);
      const messaging = firebase.messaging();
      // firebase.analytics();

      <?php
$cookie_name = "Token";
if(!isset($_COOKIE[$cookie_name])) {

?>
      function initFirebaseMessagingRegistration() {
              messaging
              .requestPermission()
              .then(function () {
                  return messaging.getToken()
              })
              .then(function(token) {
                //   console.log(token);


                  $.ajaxSetup({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    }
                });
                  $.ajax({
                      headers: {
                      "Authorization": 'Bearer ' + userObject.token
                  },
                      url: '{{ route("save-token") }}',
                      type: 'POST',
                      data: {
                          "_token": "{{ csrf_token() }}",
                          token: token
                      },
                      dataType: 'JSON',
                      success: function (response) {
                        console.log('Token saved successfully.');
                        function setCookie(cname, cvalue, exdays) {
                        var d = new Date();
                        d.setTime(d.getTime() + (exdays * 24 * 60 * 60 * 1000));
                        var expires = "expires="+d.toUTCString();
                        document.cookie = cname + "=" + cvalue + ";" + expires + ";path=/";
                         }
                        setCookie("Token", token, 30);
                      },
                      error: function (err) {
                          console.log('User Chat Token Error'+ err);
                      },
                  });

              }).catch(function (err) {
                  console.log('User Chat Token Error'+ err);
              });
       }

       initFirebaseMessagingRegistration();
       <?php
}
?>

      messaging.onMessage(function(payload) {
          const noteTitle = payload.notification.title;
          const noteOptions = {
              body: payload.notification.body,
              icon: payload.notification.icon,
          };
          new Notification(noteTitle, noteOptions);
      });

    if (Notification.permission === "default") {
    // If it's okay let's create a notification
    Notification.requestPermission().then(function (permission) {
      // If the user accepts, let's create a notification
      if (permission === "granted") {
        var notification = new Notification("Notifications have been successfully activated Welcome to our store!");

      }
    });
  }

  </script>
<!-- Facebook Pixel Code -->
<script>
!function(f,b,e,v,n,t,s)
{if(f.fbq)return;n=f.fbq=function(){n.callMethod?
n.callMethod.apply(n,arguments):n.queue.push(arguments)};
if(!f._fbq)f._fbq=n;n.push=n;n.loaded=!0;n.version='2.0';
n.queue=[];t=b.createElement(e);t.async=!0;
t.src=v;s=b.getElementsByTagName(e)[0];
s.parentNode.insertBefore(t,s)}(window,document,'script',
'https://connect.facebook.net/en_US/fbevents.js');
fbq('init', '746623102719986');
fbq('track', 'PageView');
</script>
<noscript>
<img height="1" width="1"
src="https://www.facebook.com/tr?id=746623102719986&ev=PageView
&noscript=1"/>
</noscript>
<!-- End Facebook Pixel Code -->
