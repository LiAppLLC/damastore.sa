<!-- BEGIN: LAYOUT/HEADERS/HEADER-1 -->
<!-- BEGIN: HEADER -->
<style>
    .logo_mobiles {
        display: none;
    }

    @media only screen and (max-width:991px) {
        .logo_mobiles {
            position: absolute;
            top: 30px;
            left: 50%;
            transform: translate(-50%, -50%);
            display: block;
        }
    }

    @media (min-width:991px; ) {
        .c_menu {
            display: block !important;

        }

        .c-mega-menu {
            display: block !important;
        }
    }

    @media (min-width: 992px) {
        .c-page-on-scroll.c-layout-header-fixed .c-layout-header .c-mega-menu .nav.navbar-nav>li>.c-link {
            transition: all 0.2s;
            padding: 22px 5px 22px 7px;
            min-height: 65px;
        }
    }

    @media (min-width: 992px) {
        .c-layout-header .c-navbar .c-mega-menu>.nav.navbar-nav>li>.c-link {
            letter-spacing: 1px;
            font-style: normal;
            transition: all 0.2s;
            padding: 41px 12px 39px 12px;
            min-height: 100px;
            font-size: 17px;
        }
    }
    @media (min-width: 992px) and (max-width:1233px) {
        .c-layout-header .c-navbar .c-mega-menu>.nav.navbar-nav>li>.c-link {
            padding: 40px 4px 39px 4px ;
            font-size: 12px !important;
        }
    }
    /* .nav>li>a{
        border: 4px solid transparent;
    } */
    .nav>li>a:hover {
    text-decoration: none;
    color: orange;
    border-radius: 50%;
    border-bottom:5px solid orange !important;
    box-shadow: 0px 15px 10px -15px #111;
}

</style>
<header class="c-layout-header c-layout-header-4 c-layout-header-default-mobile">
    <div class="c-navbar">
        <div class="container" style="width: 98%">
            <!-- BEGIN: BRAND -->
            <div class="c-navbar-wrapper clearfix">
                <img src="/assets/base/img/layout/logos/logo-5.png" alt="DamaStore2"
                    class="c-desktop-logo-inverse dlogo logo_mobiles" style="width: 75px;">
                <div class="c-brand c-pull-left">
                    <a href="/" class="navigation c-logo">
                        <img src="/assets/base/img/layout/logos/logo-5.png" alt="DamaStore1"
                            class="c-desktop-logo dlogo" style="width: 100px;">
                        <img src="/assets/base/img/layout/logos/logo-5.png" alt="DamaStore2"
                            class="c-desktop-logo-inverse dlogo" style="width: 100px;">
                        {{-- <span class="c-mobile-logo dlogo" style="top: 0px;">DamaStore</span> --}}
                    </a>
                    <button class="c-hor-nav-toggler" type="button" data-target=".c-mega-menu">
                        <span class="c-line"></span>
                        <span class="c-line"></span>
                        <span class="c-line"></span>
                    </button>
                    <button class="c-topbar-toggler" type="button">
                        <i class="fa fa-ellipsis-v"></i>
                    </button>
                    <button class="search_mobile" onclick="opensearch()" type="button">
                        <i class="fa fa-search"></i>
                        {{-- @lang('main.Search') --}}
                    </button>
                    <button onclick="gotocart()" class="navigation c-btn-icon c-cart-toggler">
                        <i class="icon-handbag c-cart-icon"></i>
                        <span class="c-cart-number c-theme-bg" id="cart_num_mobile"></span>
                    </button>
                    <script>
                        function gotocart() {
                            window.location = "/cart"
                        }

                    </script>

                </div>

                <!-- END: BRAND -->
                <!-- BEGIN: QUICK SEARCH -->
                <form class="c-quick-search" action="#">
                    <input type="text" name="query" placeholder="Type to search..." value="" class="form-control"
                        autocomplete="off">
                    <span class="c-theme-link">&times;</span>
                </form>
                <!-- END: QUICK SEARCH -->
                <!-- BEGIN: HOR NAV -->
                <!-- BEGIN: LAYOUT/HEADERS/MEGA-MENU -->
                <!-- BEGIN: MEGA MENU -->
                <!-- Dropdown menu toggle on mobile: c-toggler class can be applied to the link arrow or link itself depending on toggle mode -->
                <nav id="c_menu"
                    class="c-mega-menu c-pull-right c-mega-menu-dark c-mega-menu-dark-mobile c-fonts-uppercase c-fonts-bold menu-div">
                    <ul class="nav navbar-nav c-theme-nav">
                        <li>
                            <a href="/" class="navigation c-link dropdown-toggle hover_subhe">
                                <i class="fas fa-home"></i>
                                @lang('home.Home')
                                {{-- <span class="c-arrow c-toggler"></span> --}}
                            </a>
                        </li>
                        <li>
                            <a href="{{ route('shop') }}" class="navigation c-link dropdown-toggle hover_subhe">
                                <i class="fas fa-shopping-cart"></i>
                                @lang('home.Shop')
                                {{-- <span class="c-arrow c-toggler"></span> --}}
                            </a>
                        </li>
                        <li>
                            <a href="{{ route('Cproduct','all') }}" class="navigation c-link dropdown-toggle hover_subhe">
                                <i class="fas fa-shopping-cart"></i>
                                @lang('home.Custom Product')
                                {{-- <span class="c-arrow c-toggler"></span> --}}
                            </a>
                        </li>
   <li id="myprofile-btn">
                            <a href="{{ route('wallet') }}" class="navigation c-link dropdown-toggle hover_subhe">
                                <i class="fas fa-wallet"></i>

                                @lang('home.wallet')
                            </a>

                        </li>
                        <li id="myprofile-btn">
                            <a href="{{ asset('CustomerDashboard') }}"
                                class="navigation c-link dropdown-toggle hover_subhe">
                                <i class="fas fa-user-circle"></i>
                                @lang('home.MyProfile')
                                {{-- <span class="c-arrow c-toggler"></span> --}}
                            </a>

                        </li>

                        <li id="cart-btn" class="c-cart-toggler-wrapper">
                            <a href="{{ route('cart') }}" class="navigation c-btn-icon c-cart-toggler">
                                <i class="icon-handbag c-cart-icon"></i>
                                <span class="c-cart-number c-theme-bg " id="cart_num"></span>
                            </a>
                        </li>
                        <li>
                            <a style="cursor: pointer" class=" navigation c-link dropdown-toggle hover_subhe " onclick="opensearch();"
                                type="button">
                                <i class="fa fa-search"></i>
                                @lang('main.Search')
                        </a>
                        </li>
                        <li class="c-menu-type-classic">
                            <a href="javascript:;" class="c-link dropdown-toggle ">
                                <i class="fas fa-language"></i>
                                @lang('home.Language')
                                <span class="c-arrow c-toggler"></span>
                            </a>
                            <ul class="dropdown-menu c-menu-type-classic c-pull-left">

                                <li @if ($locale == 'ar') class="dropdown-submenu c-active"
@else
                                    class="dropdown-submenu" @endif>
                                    <a class="navigation" id="Arabiclang">العربية
                                        {{-- <span class="c-arrow c-toggler"></span> --}}
                                    </a>
                                </li>

                                <li @if ($locale == 'en') class="dropdown-submenu c-active"
@else
                                    class="dropdown-submenu" @endif>
                                    <a class="navigation" id="Englishlang">English
                                        {{-- <span class="c-arrow c-toggler"></span> --}}
                                    </a>

                                </li>

                                <li @if ($locale == 'de') class="dropdown-submenu c-active"
@else
                                    class="dropdown-submenu" @endif>
                                    <a class="navigation" id="Deutsche">Deutsche
                                        {{-- <span class="c-arrow c-toggler"></span> --}}
                                    </a>

                                </li>

                                <li @if ($locale == 'es') class="dropdown-submenu c-active"
@else
                                    class="dropdown-submenu" @endif>
                                    <a class="navigation" id="española">española
                                        {{-- <span class="c-arrow c-toggler"></span> --}}
                                    </a>

                                </li>

                                <li @if ($locale == 'fr') class="dropdown-submenu c-active"
@else
                                    class="dropdown-submenu" @endif>
                                    <a class="navigation" id="français">français
                                        {{-- <span class="c-arrow c-toggler"></span> --}}
                                    </a>

                                </li>

                                <li @if ($locale == 'he') class="dropdown-submenu c-active"
@else
                                    class="dropdown-submenu" @endif>
                                    <a class="navigation" id="עִברִית">עִברִית
                                        {{-- <span class="c-arrow c-toggler"></span> --}}
                                    </a>

                                </li>

                                <li @if ($locale == 'hi') class="dropdown-submenu c-active"
@else
                                    class="dropdown-submenu" @endif>
                                    <a class="navigation" id="भारतीय">भारतीय
                                        {{-- <span class="c-arrow c-toggler"></span> --}}
                                    </a>

                                </li>

                                <li @if ($locale == 'ja') class="dropdown-submenu c-active"
@else
                                    class="dropdown-submenu" @endif>
                                    <a class="navigation" id="日本人">日本人
                                        {{-- <span class="c-arrow c-toggler"></span> --}}
                                    </a>

                                </li>

                                <li @if ($locale == 'ko') class="dropdown-submenu c-active"
@else
                                    class="dropdown-submenu" @endif>
                                    <a class="navigation" id="한국어">한국어
                                        {{-- <span class="c-arrow c-toggler"></span> --}}
                                    </a>

                                </li>

                                <li @if ($locale == 'pt') class="dropdown-submenu c-active"
@else
                                    class="dropdown-submenu" @endif>
                                    <a class="navigation" id="Português">Português
                                        {{-- <span class="c-arrow c-toggler"></span> --}}
                                    </a>

                                </li>

                                <li @if ($locale == 'rus') class="dropdown-submenu c-active"
@else
                                    class="dropdown-submenu" @endif>
                                    <a class="navigation" id="русский">русский
                                        {{-- <span class="c-arrow c-toggler"></span> --}}
                                    </a>

                                </li>

                                <li @if ($locale == 'ur') class="dropdown-submenu c-active"
@else
                                    class="dropdown-submenu" @endif>
                                    <a class="navigation" id="اردو">اردو
                                        {{-- <span class="c-arrow c-toggler"></span> --}}
                                    </a>

                                </li>

                                <li @if ($locale == 'zh') class="dropdown-submenu c-active"
@else
                                    class="dropdown-submenu" @endif>
                                    <a class="navigation" id="中文">中文
                                        {{-- <span class="c-arrow c-toggler"></span> --}}
                                    </a>

                                </li>

                            </ul>
                        </li>
                        <li>
                            {{-- <a href="#" data-toggle="modal" data-target="#login-form" class="c-btn-border-opacity-04 c-btn btn-no-focus c-btn-header btn btn-sm c-btn-border-1x c-btn-dark c-btn-circle c-btn-uppercase c-btn-sbold"><i class="icon-user"></i> Sign In</a> --}}
                            <a id="signin-btn" href="/CustomerLoginRegister"
                                class="navigation c-btn-border-opacity-04 c-btn btn-no-focus c-btn-header btn btn-sm c-btn-border-1x c-btn-dark c-btn-circle c-btn-uppercase c-btn-sbold"><i
                                    class="icon-user"></i>@lang('home.SignIn')</a>
                            <button id="signout-btn" type="button"
                                class="c-btn-border-opacity-04 c-btn btn-no-focus c-btn-header btn btn-sm c-btn-border-1x c-btn-dark c-btn-circle c-btn-uppercase c-btn-sbold">
                                <i class="icon-user"></i>@lang('home.SignOut')</button>


                        </li>


                        {{-- <li class="c-quick-sidebar-toggler-wrapper"> --}}
                        {{-- <a href="#" class="c-quick-sidebar-toggler"> --}}
                        {{-- <span class="c-line"></span> --}}
                        {{-- <span class="c-line"></span> --}}
                        {{-- <span class="c-line"></span> --}}
                        {{-- </a> --}}
                        {{-- </li> --}}
                        <style>
                            @media (min-width: 992px) {
                                .subhe_rate_select {
                                    width: 80px;
                                    border: 0;
                                    cursor: pointer;
                                    background-color: transparent;
                                    border-radius: 8px;
                                    /*margin: 34px 15px 33px 15px;*/
                                    /*line-height: 0px;*/
                                }
                            }

                            .subhe_rate_select:hover {
                                background-color: orange;
                            }

                            subhe_hover:hover.subhe_rate_select {
                                background-color: orange;
                            }

                        </style>
                        <li>
                            <a style="display: none" href="javascript:;"
                                class="subhe_hover navigation c-btn-border-opacity-04 c-btn btn-no-focus c-btn-header btn btn-sm c-btn-border-1x c-btn-dark c-btn-circle c-btn-uppercase c-btn-sbold">
                                <select id="currencyList" style="display: none" class="subhe_rate_select " onchange="change_currency();">
                                    <option disabled selected="selected">
                                        @if (session('currency'))
                                        {{ session('currency') }} @else
                                            Currency @endif
                                    </option>
                                    {{-- <option value="auto" label="Based on IP automatic">Based on IP automatic</option> --}}
                                    <option value="USD" selected label="US dollar">USD</option>
                                    {{-- <option value="EUR" label="Euro">EUR</option>
                                    <option value="JPY" label="Japanese yen">JPY</option>
                                    <option value="GBP" label="Pound sterling">GBP</option> --}}
                                    {{-- <option disabled>──────────</option> --}}
                                    {{-- <option value="AED" label="United Arab Emirates dirham">AED</option>
                                    <option value="AFN" label="Afghan afghani">AFN</option>
                                    <option value="ALL" label="Albanian lek">ALL</option>
                                    <option value="AMD" label="Armenian dram">AMD</option>
                                    <option value="ANG" label="Netherlands Antillean guilder">ANG</option>
                                    <option value="AOA" label="Angolan kwanza">AOA</option>
                                    <option value="ARS" label="Argentine peso">ARS</option>
                                    <option value="AUD" label="Australian dollar">AUD</option>
                                    <option value="AWG" label="Aruban florin">AWG</option>
                                    <option value="AZN" label="Azerbaijani manat">AZN</option>
                                    <option value="BAM" label="Bosnia and Herzegovina convertible mark">BAM</option>
                                    <option value="BBD" label="Barbadian dollar">BBD</option>
                                    <option value="BDT" label="Bangladeshi taka">BDT</option>
                                    <option value="BGN" label="Bulgarian lev">BGN</option>
                                    <option value="BHD" label="Bahraini dinar">BHD</option>
                                    <option value="BIF" label="Burundian franc">BIF</option>
                                    <option value="BMD" label="Bermudian dollar">BMD</option>
                                    <option value="BND" label="Brunei dollar">BND</option>
                                    <option value="BOB" label="Bolivian boliviano">BOB</option>
                                    <option value="BRL" label="Brazilian real">BRL</option>
                                    <option value="BSD" label="Bahamian dollar">BSD</option>
                                    <option value="BTN" label="Bhutanese ngultrum">BTN</option>
                                    <option value="BWP" label="Botswana pula">BWP</option>
                                    <option value="BYN" label="Belarusian ruble">BYN</option>
                                    <option value="BZD" label="Belize dollar">BZD</option>
                                    <option value="CAD" label="Canadian dollar">CAD</option>
                                    <option value="CDF" label="Congolese franc">CDF</option>
                                    <option value="CHF" label="Swiss franc">CHF</option>
                                    <option value="CLP" label="Chilean peso">CLP</option>
                                    <option value="CNY" label="Chinese yuan">CNY</option>
                                    <option value="COP" label="Colombian peso">COP</option>
                                    <option value="CRC" label="Costa Rican colón">CRC</option>
                                    <option value="CUC" label="Cuban convertible peso">CUC</option>
                                    <option value="CUP" label="Cuban peso">CUP</option>
                                    <option value="CVE" label="Cape Verdean escudo">CVE</option>
                                    <option value="CZK" label="Czech koruna">CZK</option>
                                    <option value="DJF" label="Djiboutian franc">DJF</option>
                                    <option value="DKK" label="Danish krone">DKK</option>
                                    <option value="DOP" label="Dominican peso">DOP</option>
                                    <option value="DZD" label="Algerian dinar">DZD</option>
                                    <option value="EGP" label="Egyptian pound">EGP</option>
                                    <option value="ERN" label="Eritrean nakfa">ERN</option>
                                    <option value="ETB" label="Ethiopian birr">ETB</option>
                                    <option value="EUR" label="EURO">EUR</option>
                                    <option value="FJD" label="Fijian dollar">FJD</option>
                                    <option value="FKP" label="Falkland Islands pound">FKP</option>
                                    <option value="GBP" label="British pound">GBP</option>
                                    <option value="GEL" label="Georgian lari">GEL</option>
                                    <option value="GGP" label="Guernsey pound">GGP</option>
                                    <option value="GHS" label="Ghanaian cedi">GHS</option>
                                    <option value="GIP" label="Gibraltar pound">GIP</option>
                                    <option value="GMD" label="Gambian dalasi">GMD</option>
                                    <option value="GNF" label="Guinean franc">GNF</option>
                                    <option value="GTQ" label="Guatemalan quetzal">GTQ</option>
                                    <option value="GYD" label="Guyanese dollar">GYD</option>
                                    <option value="HKD" label="Hong Kong dollar">HKD</option>
                                    <option value="HNL" label="Honduran lempira">HNL</option>
                                    <option value="HKD" label="Hong Kong dollar">HKD</option>
                                    <option value="HRK" label="Croatian kuna">HRK</option>
                                    <option value="HTG" label="Haitian gourde">HTG</option>
                                    <option value="HUF" label="Hungarian forint">HUF</option>
                                    <option value="IDR" label="Indonesian rupiah">IDR</option>
                                    <option value="ILS" label="Israeli new shekel">ILS</option>
                                    <option value="IMP" label="Manx pound">IMP</option>
                                    <option value="INR" label="Indian rupee">INR</option>
                                    <option value="IQD" label="Iraqi dinar">IQD</option>
                                    <option value="IRR" label="Iranian rial">IRR</option>
                                    <option value="ISK" label="Icelandic króna">ISK</option>
                                    <option value="JEP" label="Jersey pound">JEP</option>
                                    <option value="JMD" label="Jamaican dollar">JMD</option>
                                    <option value="JOD" label="Jordanian dinar">JOD</option>
                                    <option value="JPY" label="Japanese yen">JPY</option>
                                    <option value="KES" label="Kenyan shilling">KES</option>
                                    <option value="KGS" label="Kyrgyzstani som">KGS</option>
                                    <option value="KHR" label="Cambodian riel">KHR</option>
                                    <option value="KID" label="Kiribati dollar">KID</option>
                                    <option value="KMF" label="Comorian franc">KMF</option>
                                    <option value="KPW" label="North Korean won">KPW</option>
                                    <option value="KRW" label="South Korean won">KRW</option>
                                    <option value="KWD" label="Kuwaiti dinar">KWD</option>
                                    <option value="KYD" label="Cayman Islands dollar">KYD</option>
                                    <option value="KZT" label="Kazakhstani tenge">KZT</option>
                                    <option value="LAK" label="Lao kip">LAK</option>
                                    <option value="LBP" label="Lebanese pound">LBP</option>
                                    <option value="LKR" label="Sri Lankan rupee">LKR</option>
                                    <option value="LRD" label="Liberian dollar">LRD</option>
                                    <option value="LSL" label="Lesotho loti">LSL</option>
                                    <option value="LYD" label="Libyan dinar">LYD</option>
                                    <option value="MAD" label="Moroccan dirham">MAD</option>
                                    <option value="MDL" label="Moldovan leu">MDL</option>
                                    <option value="MGA" label="Malagasy ariary">MGA</option>
                                    <option value="MKD" label="Macedonian denar">MKD</option>
                                    <option value="MMK" label="Burmese kyat">MMK</option>
                                    <option value="MNT" label="Mongolian tögrög">MNT</option>
                                    <option value="MOP" label="Macanese pataca">MOP</option>
                                    <option value="MRU" label="Mauritanian ouguiya">MRU</option>
                                    <option value="MUR" label="Mauritian rupee">MUR</option>
                                    <option value="MVR" label="Maldivian rufiyaa">MVR</option>
                                    <option value="MWK" label="Malawian kwacha">MWK</option>
                                    <option value="MXN" label="Mexican peso">MXN</option>
                                    <option value="MYR" label="Malaysian ringgit">MYR</option>
                                    <option value="MZN" label="Mozambican metical">MZN</option>
                                    <option value="NAD" label="Namibian dollar">NAD</option>
                                    <option value="NGN" label="Nigerian naira">NGN</option>
                                    <option value="NIO" label="Nicaraguan córdoba">NIO</option>
                                    <option value="NOK" label="Norwegian krone">NOK</option>
                                    <option value="NPR" label="Nepalese rupee">NPR</option>
                                    <option value="NZD" label="New Zealand dollar">NZD</option>
                                    <option value="OMR" label="Omani rial">OMR</option>
                                    <option value="PAB" label="Panamanian balboa">PAB</option>
                                    <option value="PEN" label="Peruvian sol">PEN</option>
                                    <option value="PGK" label="Papua New Guinean kina">PGK</option>
                                    <option value="PHP" label="Philippine peso">PHP</option>
                                    <option value="PKR" label="Pakistani rupee">PKR</option>
                                    <option value="PLN" label="Polish złoty">PLN</option>
                                    <option value="PRB" label="Transnistrian ruble">PRB</option>
                                    <option value="PYG" label="Paraguayan guaraní">PYG</option>
                                    <option value="QAR" label="Qatari riyal">QAR</option>
                                    <option value="RON" label="Romanian leu">RON</option>
                                    <option value="RON" label="Romanian leu">RON</option>
                                    <option value="RSD" label="Serbian dinar">RSD</option>
                                    <option value="RUB" label="Russian ruble">RUB</option>
                                    <option value="RWF" label="Rwandan franc">RWF</option>
                                    <option value="SAR" label="Saudi riyal">SAR</option>
                                    <option value="SEK" label="Swedish krona">SEK</option>
                                    <option value="SGD" label="Singapore dollar">SGD</option>
                                    <option value="SHP" label="Saint Helena pound">SHP</option>
                                    <option value="SLL" label="Sierra Leonean leone">SLL</option>
                                    <option value="SLS" label="Somaliland shilling">SLS</option>
                                    <option value="SOS" label="Somali shilling">SOS</option>
                                    <option value="SRD" label="Surinamese dollar">SRD</option>
                                    <option value="SSP" label="South Sudanese pound">SSP</option>
                                    <option value="STN" label="São Tomé and Príncipe dobra">STN</option>
                                    <option value="SYP" label="Syrian pound">SYP</option>
                                    <option value="SZL" label="Swazi lilangeni">SZL</option>
                                    <option value="THB" label="Thai baht">THB</option>
                                    <option value="TJS" label="Tajikistani somoni">TJS</option>
                                    <option value="TMT" label="Turkmenistan manat">TMT</option>
                                    <option value="TND" label="Tunisian dinar">TND</option>
                                    <option value="TOP" label="Tongan paʻanga">TOP</option>
                                    <option value="TRY" label="Turkish lira">TRY</option>
                                    <option value="TTD" label="Trinidad and Tobago dollar">TTD</option>
                                    <option value="TVD" label="Tuvaluan dollar">TVD</option>
                                    <option value="TWD" label="New Taiwan dollar">TWD</option>
                                    <option value="TZS" label="Tanzanian shilling">TZS</option>
                                    <option value="UAH" label="Ukrainian hryvnia">UAH</option>
                                    <option value="UGX" label="Ugandan shilling">UGX</option>
                                    <option value="USD" label="United States dollar">USD</option>
                                    <option value="UYU" label="Uruguayan peso">UYU</option>
                                    <option value="UZS" label="Uzbekistani soʻm">UZS</option>
                                    <option value="VES" label="Venezuelan bolívar soberano">VES</option>
                                    <option value="VND" label="Vietnamese đồng">VND</option>
                                    <option value="VUV" label="Vanuatu vatu">VUV</option>
                                    <option value="WST" label="Samoan tālā">WST</option>
                                    <option value="XAF" label="Central African CFA franc">XAF</option>
                                    <option value="XCD" label="Eastern Caribbean dollar">XCD</option>
                                    <option value="XOF" label="West African CFA franc">XOF</option>
                                    <option value="XPF" label="CFP franc">XPF</option>
                                    <option value="ZAR" label="South African rand">ZAR</option>
                                    <option value="ZMW" label="Zambian kwacha">ZMW</option>
                                    <option value="ZWB" label="Zimbabwean bonds">ZWB</option> --}}
                                </select>
                            </a>
                            <script>
                                function change_currency() {
                                    let currency = document.getElementById('currencyList').value;
                                    window.location = "/changeCurrency/" + currency;
                                }

                            </script>
                        </li>

                    </ul>
                </nav>
                <!-- END: MEGA MENU -->
                <!-- END: LAYOUT/HEADERS/MEGA-MENU -->
                <!-- END: HOR NAV -->
            </div>
        </div>
        <!-- BEGIN: LAYOUT/HEADERS/QUICK-CART -->
        <!-- BEGIN: CART MENU -->

        <!-- END: CART MENU -->
        <!-- END: LAYOUT/HEADERS/QUICK-CART -->
    </div>

</header>
<!-- END: HEADER -->
<!-- END: LAYOUT/HEADERS/HEADER-1 -->
<script>
    $(document).on('click', function(e) {

        if ($(e.target).closest(".c-hor-nav-toggler").length === 0 && $(e.target).closest(".navbar-nav")
            .length === 0) {
            $("#c_menu").removeClass("c-shown");
        }
    });

</script>
