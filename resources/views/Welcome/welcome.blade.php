@extends('Welcome.home')
  <!-- Link Swiper's CSS -->

@section('content')
 <!-- Link Swiper's CSS -->
<link rel="stylesheet" href="https://unpkg.com/swiper@6.4.10/swiper-bundle.min.css" />
    <?php
    $locale = App::getLocale();
    ?>
    <!-- BEGIN: PAGE CONTAINER -->
    <div class="c-layout-page">
        <!-- BEGIN: PAGE CONTENT -->
        <!-- BEGIN: LAYOUT/SLIDERS/REVO-SLIDER-4 -->
        <style>
        .swiper-container {
            width: 100%;
            height: auto;
            margin-left: auto;
            margin-right: auto;
          }
          .swiper-wrapper{
              height: auto;
          }
          .swiper-slide {
            text-align: center;
            font-size: 18px;
            background: #fff;

            /* Center slide text vertically */
            display: -webkit-box;
            display: -ms-flexbox;
            display: -webkit-flex;
            display: flex;
            -webkit-box-pack: center;
            -ms-flex-pack: center;
            -webkit-justify-content: center;
            justify-content: center;
            -webkit-box-align: center;
            -ms-flex-align: center;
            -webkit-align-items: center;
            align-items: center;
          }
          .swiper-pagination-bullet-active{
              background: #ffbc40;
          }
        </style>
        <div class="swiper-container">
            <div class="swiper-wrapper">

              <div class="swiper-slide">
                   <a href="https://www.youtube.com/channel/UC5b5Dj1Nl0nP4YigH5yLh6A">
                  <img width="100%" src="{{asset('assets/base/img/content/backgrounds/1.jpg')}}">
                </a>
              </div>

              <div class="swiper-slide">
                <a href="https://www.youtube.com/channel/UC5b5Dj1Nl0nP4YigH5yLh6A">
                <img width="100%" src="{{asset('assets/base/img/content/backgrounds/2.jpg')}}">
            </a>
            </div>
            <div class="swiper-slide">
                <a href="https://www.youtube.com/channel/UC5b5Dj1Nl0nP4YigH5yLh6A">
                <img width="100%" src="{{asset('assets/base/img/content/backgrounds/3.jpg')}}">
            </a>
            </div>
            <div class="swiper-slide">
                <a href="https://www.youtube.com/channel/UC5b5Dj1Nl0nP4YigH5yLh6A">
                <img width="100%" src="{{asset('assets/base/img/content/backgrounds/4.jpg')}}">
            </a>
            </div>
            <div class="swiper-slide">
                <a href="https://www.youtube.com/channel/UC5b5Dj1Nl0nP4YigH5yLh6A">
                <img width="100%" src="{{asset('assets/base/img/content/backgrounds/5.jpg')}}">
            </a>
            </div>
            <div class="swiper-slide">
                <a href="https://www.youtube.com/channel/UC5b5Dj1Nl0nP4YigH5yLh6A">
                <img width="100%" src="{{asset('assets/base/img/content/backgrounds/6.jpg')}}">
            </a>
            </div>
            </div>
            <!-- Add Pagination -->
            <div class="swiper-pagination" style="color: #ffbc40"></div>
            <!-- Add Arrows -->
            <div class="swiper-button-next" style="color: #ffbc40"></div>
            <div class="swiper-button-prev" style="color: #ffbc40"></div>
          </div>
        <!-- END: LAYOUT/SLIDERS/REVO-SLIDER-4 -->
<link rel="stylesheet" type="text/css" href="/assets_new/slick/slick.css">
<link rel="stylesheet" type="text/css" href="/assets_new/slick/slick-theme.css">
        <section class="center slider">
        @foreach($category as $categorys)
        <a href="/products/{{$categorys->id}}" style="width: auto !important">
            <div class="displayWidth" id="s_{{$categorys->id}}">
              <img src="/{{$categorys->image}}">
              <br>
              <div class="slick-text">
              @if(app()->getLocale()=='ar')
              {{$categorys->ar_name}}
              @else
              {{$categorys->name}}
              @endif
              </div>
            </div>
        </a>
        @endforeach
          </section>
        <!-- BEGIN: CONTENT/SHOPS/SHOP-3-1 -->
        {{-- <div class="c-content-box c-overflow-hide c-bg-grey">
            <div class="c-content-product-3 c-bs-grid-reset-space">
                <div class="row" id="latestOffer">
                </div>
            </div>
        </div><!-- END: CONTENT/SHOPS/SHOP-3-1 --> --}}

        <div class="container">
            <div class="c-content-title-4">
                <h3 class="c-font-uppercase c-center c-font-bold c-line-strike"><span class="c-bg-grey-1">@lang('home.lastoffer')</span></h3>
            </div>
            @foreach($products_last as $item)
          <div class="row last_prod_div">
            <div  class="col-sm-8 last_image" >
            <img src="/images/{{$item->getImageUrl()}}" >
             </div>
             <div class="col-sm-4 last_text">
                   <h1>
                    @if(app()->getLocale()=='ar')
                    {{$item->ar_name}}
                    @else
                    {{$item->name}}
                    @endif
                   </h1>
                   <p>
                   <div class="contener">
                   @if(app()->getLocale()=='ar')
                   {!! $item->ar_description !!}
                   @else
                   {!! $item->description !!}
                   @endif
                   </div>
                   <div>
                          <span class="c-price c-font-14 price_last"> {{$item->price}} </span>
                           <span class="c-price c-font-14  c-font-slim c-font-line-through price_last"> {{$item->price_sale}} </span>
                           {{-- {{$item->currency}}  --}}
                   <br>
                   <button onclick="location.href='/productDetails/{{$item->id}}'">@lang('main.read more')</button>
                   </div>
                   </p>
              </div>
            </div>
            @endforeach
        </div>
        <br><br>
        <!-- BEGIN: CONTENT/SHOPS/SHOP-2-1 -->
        <div class="c-content-box c-size-md c-overflow-hide c-bs-grid-small-space c-bg-grey-1">
            <div class="container">
                <div class="c-content-title-4">
                    <h3 class="c-font-uppercase c-center c-font-bold c-line-strike"><span class="c-bg-grey-1">@lang('home.mostrecent')</span></h3>
                </div>
                <style>
                    .c-bg-img-center {
                        /* background-repeat: no-repeat; */
                        /*background-position: center;*/
                        /*background-size: auto !important;*/
                    }
                </style>
                <div class="row" id="">
                    @foreach($products as $product)

                    <div class="col-md-3 col-sm-6 c-margin-b-20">
                        <div class="c-content-product-2 c-bg-white c-border">
                            <div class="c-content-overlay">
                                <div class="c-label c-bg-red c-font-uppercase c-font-white c-font-13 c-font-bold">@lang('home.Sale')</div>
                                <div class="c-overlay-wrapper">
                                    <div class="c-overlay-content">
                                        <a href="/productDetails/{{$product->id}}" class="navigation btn btn-md c-btn-grey-1 c-btn-uppercase c-btn-bold c-btn-border-1x c-btn-square">@lang('home.exp')</a>
                                    </div></div>
                                <div class="c-bg-img-center c-overlay-object" data-height="height" style="height: 300px; background-image: url(/images/{{$product->getImageUrl()}});"></div></div>
                            <div class="c-info" style="height: 100px">
                                <p class="c-title c-font-16 c-font-slim">
                                @if(app()->getLocale()=='ar')
                                {{$product->ar_name}}
                                @else
                                {{$product->name}}
                                @endif

                                </p>
                                <p class="c-price c-font-14 c-font-slim">{{$product->price}}</p>
                                <span class="c-font-14 c-font-line-through c-font-red">{{$product->price_sale}}</span>
                                </p>
                            </div>
                            <div class="btn-group btn-group-justified" role="group">
                                <div class="btn-group c-border-top" role="group" id="quantity_input2_div_{{$product->id}}">
                                    <input class="form-control" type="number" value="1" id="quantity_input2_{{$product->id}}" name="quantity_input2">
                                </div>
                                <div class="btn-group c-border-left c-border-top" role="group">
                                    <button class="btn btn-sm c-btn-white c-btn-uppercase c-btn-square c-font-grey-3 c-font-white-hover c-bg-red-2-hover c-btn-product qty_btn2" data-id="{{$product->id}}" id="qty_btn2_{{$product->id}}">@lang('home.Add Cart')</button>
                                    <button class="btn btn-sm c-btn-white c-btn-uppercase c-btn-square c-font-grey-3 c-font-white-hover c-bg-red-2-hover c-btn-product qty_btn_rem2 qty_btn_rem_none" data-id="{{$product->id}}" id="qty_btn_rem2_{{$product->id}}" style="margin: 0px !important; display: none;">@lang('home.Remove From Cart')</button>
                                </div>
                            </div>
                        </div>
                    </div>
                @endforeach

                </div>
                {{$products->links()}}
            </div>
        </div><!-- END: CONTENT/SHOPS/SHOP-2-1 -->
<h1>

</h1>

        <!-- BEGIN: PAGE CONTENT -->
        <div class="c-content-box c-size-md">
            <div class="container">
                <div id="filters-container" class="cbp-l-filters-text">

                </div>

                <div id="grid-container" class="cbp-l-grid-agency row">
                </div>

                <div id="loadMore-container" class="cbp-l-loadMore-button c-margin-t-60">
                    <button href="#" onclick="goToCate()" class="btn cbp-l-loadMore-link btn c-btn-square c-btn-border-2x c-btn-dark c-btn-bold c-btn-uppercase">
                        @lang('home.LOADMORE')
                    </button>
                    <input type="hidden" id="id_cate">
                </div>
            </div>
        </div>
        <!-- END: PAGE CONTENT -->


    </div>
    <!-- END: PAGE CONTAINER -->


@endsection





@section('script')


    {{--<script>--}}

    //forcefullwidth_wrapper_tp_banner
    //$(".forcefullwidth_wrapper_tp_banner").height('150%');
    $(".forcefullwidth_wrapper_tp_banner").css('overflow-y', 'hidden');
    $(".forcefullwidth_wrapper_tp_banner").css('overflow-x', 'hidden');
    {{-- $(".tp-revslider-mainul").height('150%'); --}}

        var userObject = JSON.parse(localStorage.getItem("DamaStore_userObject"));

        // blocked
        // bornDate
        // created_at
        // email
        // emailVerifiedAt
        // files
        // firstName
        // id
        // lastActivity
        // lastLoggedIn
        // lastName
        // phone
        // reputation
        // roles
        // token
        // updated_at
        // username
        // verify


        var lenght = 1;
        var pageSize = 8;
        var selectedPage = 1;
        var localstorageVar = 'Product_page';
        var categories_id = [];

        function getData(filter) {

            $.ajax({
                type: "POST",
                url: '{{route('HomePageProductList')}}',
                data: filter,
                // mimeType: 'application/json',
                processData: false,
                contentType: false,
                //headers: {"Authorization": 'Bearer ' + userObject.token},
                success: function (res) {

                    // most recent
                    //console.log('latestoffer', res.data.latestoffer);
                    // category: "cat4"
                    // category_id: 4
                    // created_at: "2020-10-20 21:49:27"
                    // description: "prod desc"
                    // id: 5
                    // image: Object { id: 921, path: "1603486411.PNG", target_id: 5, … }
                    // name: "prod name"
                    // price: 1232
                    // price_sale: 1050
                    // product_code: "prod code"
                    // quantity: 23
                    // size: "213"
                    // type: "type3"
                    // type_id: 3
                    // unit: "34"
                    // updated_at: "2020-10-28 10:44:01"
                    // user_id: 1



                    $('#filters-container').empty();
                    var first = true;
                    res.data.allProductList.data.forEach(function (item, index, arr) {
                        var active = 'subhe_cate_fe';
                        if(first){
                            active = 'subhe_cate_fe_active';
                            first = false;
                        }

                        categories_id.push(item.id);
                        $('#filters-container').append(
                        '<div data-filter=".identity" id="cat_id-' + item.id + '" data-id="' + item.id + '" class="' + active + ' cat_class subhe_cate_fe cbp-filter-item"> ' +
                        @if(app()->getLocale()=='ar')
                        '    ' + item.ar_name + '<!--<div class="cbp-filter-counter">' + item.name + '</div>--> ' +
                        @else
                        '    ' + item.name + '<!--<div class="cbp-filter-counter">' + item.name + '</div>--> ' +
                        @endif
                        '</div> '
                        );
                    });

                    $('#grid-container').empty();
                    first = true;
                    res.data.allProductList.data.forEach(function (item, index, arr) {
                        var display = '';
                        if(first){
                            $('#id_cate').val(item.id);
                            first = false;
                        }else{
                            display = ' style="display: none"';
                        }

                        $('#grid-container').append(
                        '<div id="div_cat_id_' + item.id + '"'+ display + '></div>'
                        );

                    });

                    res.data.allProductList.data.forEach(function (item, index, arr) {
                        if(item.products.length > 0){
                            item.products.forEach(function (proditem, prodindex, prodarr) {
                            console.log('proditem',  proditem);
                                $('#div_cat_id_' + item.id).append(
                                '<div class="cbp-item graphic prod-cat"> ' +
                                '    <a href="/productDetails/' + proditem.id + '" target="_blank" > ' +
                                '        <div class=""> ' +
                                '            <div class="cbp-caption-defaultWrap" style="text-align: center"> ' +
                                '                <img src="/images/' + proditem.image+ '" alt="" style="width: 232px;"> ' +
                                '            </div> ' +
                                '        </div> ' +
                                @if(app()->getLocale()=='ar')
                                '        <div class="cbp-l-grid-agency-title">' + proditem.ar_name + '</div> ' +
                                @else
                                '        <div class="cbp-l-grid-agency-title">' + proditem.name + '</div> ' +
                                @endif
                                '        <div class="cbp-l-grid-agency-desc">' + proditem.category + ' / ' + proditem.type + '</div> ' +
                                '    </a> ' +
                                '</div>'
                                );
                            });
                        }else{
                            $('#div_cat_id_' + item.id).append(
                            '<div class="c-content-box c-size-lg"> ' +
                            '    <div class="container"> ' +
                            '        <div class="c-shop-cart-page-1 c-center"> ' +
                            '            <i class="fa fa-frown-o c-font-dark c-font-50 c-font-thin "></i> ' +
                            '            <h2 class="c-font-thin c-center">@lang('home.noDataFound')!</h2> ' +
                            //'            <a href="#" class="btn c-btn btn-lg c-btn-dark c-btn-square c-font-white c-font-bold c-font-uppercase">Continue Shopping</a> ' +
                            '        </div> ' +
                            '    </div> ' +
                            '</div>'
                            );
                        }
                    });

                },
                error: function (res, response) {
                },
            });
        }



        var formDataInit = new FormData();
        formDataInit.append('sale', true);
        $('#checkbox-sidebar-3-1').click();
        getData(formDataInit);

        $(document).on("click", ".clickable-btn", function(e) {
            selectedPage = changePage($(this).data('id'), selectedPage, localstorageVar);
        });



        $(document).on("click", ".cat_class", function(e) {

            var id = $(this).data('id');
            $('#id_cate').val(id);
            //console.log('categories_id', categories_id);
            //console.log('id', id);
            categories_id.forEach(function (item, index, arr) {
                $('#cat_id-' + item).removeClass('subhe_cate_fe_active cbp-filter-item-active');
                $('#div_cat_id_' + item).hide('slow');
            });
            $('#cat_id-' + id).addClass('subhe_cate_fe_active cbp-filter-item-active');
            $('#div_cat_id_' + id).show('slow');
            $('#id_cate').val(id);
        });

        $(document).on("click", ".qty_btn", function(e) {
            //if(userObject){
                var id = $(this).data('id');
                var qty = $('#quantity_input_' + id).val();
                AddToCart(id, qty, 'damastore_cart', 'quantity_input_', 'qty_btn_', 'qty_btn_rem_', 'quantity_input2_div_', 'qty_btn2_', 'qty_btn_rem2_');
            //}else{
            //    Swal.fire({
            //        icon: 'error',
            //        title: 'Oops...',
            //        text: 'you should be logged in',
            //    });
            //}
        });

        $(document).on("click", ".qty_btn_rem", function(e) {
            //if(userObject){
                var id = $(this).data('id');
                removeFromCart(id, 'damastore_cart', 'quantity_input_', 'qty_btn_', 'qty_btn_rem_', 'quantity_input2_div_', 'qty_btn2_', 'qty_btn_rem2_');
            //}else{
            //    Swal.fire({
            //        icon: 'error',
            //        title: 'Oops...',
            //        text: 'you should be logged in',
            //    });
            //}
        });

        $(document).on("click", ".qty_btn2", function(e) {
            //if(userObject){
                var id = $(this).data('id');
                var qty = $('#quantity_input2_' + id).val();
                AddToCart(id, qty, 'damastore_cart', 'quantity_input2_div_', 'qty_btn2_', 'qty_btn_rem2_', 'quantity_input_', 'qty_btn_', 'qty_btn_rem_');
            //}else{
            //    Swal.fire({
            //        icon: 'error',
            //        title: 'Oops...',
            //        text: 'you should be logged in',
            //    });
            //}
        });

        $(document).on("click", ".qty_btn_rem2", function(e) {
            //if(userObject){
                var id = $(this).data('id');
                removeFromCart(id, 'damastore_cart', 'quantity_input2_div_', 'qty_btn2_', 'qty_btn_rem2_', 'quantity_input_', 'qty_btn_', 'qty_btn_rem_');
            //}else{
            //    Swal.fire({
            //        icon: 'error',
            //        title: 'Oops...',
            //        text: 'you should be logged in',
            //    });
            //}
        });




          $(".center").slick({
            dots: true,
            infinite: true,
            centerMode: false,
            slidesToShow: 4,
            slidesToScroll: 4,
            autoplay: true,
            @if(app()->getLocale()=='ar')
            rtl: true,
            @else
            rtl: false,
            @endif
            responsive: [
                {
                    breakpoint: 600,
                    settings: {
                        slidesToShow: 3,
                        slidesToScroll: 3,

                    }
                },
                {
                    breakpoint: 480,
                    settings: {
                        slidesToShow: 2,
                        slidesToScroll: 2,
                    }
                }

          ]

          });
           $('.center').on('init afterChange', function(event, slick){
            $('.center').eq(0).next().addClass('slick-center');
           });
           $('#s1').on("click", function(e){
           alert("hi");

          });

        // ===============================================================
        var swiper = new Swiper('.swiper-container', {
            slidesPerView: 1,
            spaceBetween: 30,
            loop: true,
            pagination: {
              el: '.swiper-pagination',
              clickable: true,
            },
            navigation: {
              nextEl: '.swiper-button-next',
              prevEl: '.swiper-button-prev',
            },
          });
    {{--</script>--}}

@endsection
<script>
   function goToCate() {
       let id = $('#id_cate').val();
       if(id){
        window.location="/products/"+id;
       }
   }
</script>



