{{--<!DOCTYPE html>--}}

{{--<html lang="en" class="ie9">--}}
{{--<!--[if !IE]><!-->--}}
{{--<html lang="en" dir="rtl">--}}
{{--<!--<![endif]-->--}}
{{--<!-- BEGIN HEAD -->--}}

@extends('main')
@section('main_content')
@yield('meta')
@include('Welcome.header')


<body class="c-layout-header-fixed c-layout-header-mobile-fixed" id="How-It-Works">
<?php
$locale = App::getLocale();
?>
<style>
    .error{
        color:red;
    }
</style>
@include('Welcome.navbar')
<link href="/assets_new/css/main.css" rel="stylesheet" type="text/css"/>
<script src="/assets_new/js/main.js"></script>
{{--@include('Welcome.views.quick-sidebar.quick-sidebar')--}}
@yield('content')
@include('Welcome.footer')

<script>
    // $(window).on('load', function() {
        $(".svg_cont").fadeOut(2000);
        $('#logo_svg').css("display", "block");
    // });
</script>
@include('outOfDocumentReadyscript')

<script>
    $(document).ready(function() {
        @include('globalscript')
        @yield('script')
        // $(document).on('click', '.navigation', function() {
        //     $('#How-It-Works').append('<div class="loading">Loading&#8230;</div>');
        // });
    });
</script>

@yield('js_sc')
@endsection

{{--</body>--}}

{{--</html>--}}
