@extends('Welcome.views.home1')
@section('style')
    <style>
        .card {
            height: inherit;
            overflow: hidden;
            border-radius: 8px;
            box-shadow: 0 0 5px rgba(128, 128, 128, 0.144);
            border: 1px solid rgba(0, 0, 0, 0.062);
        }

    </style>
@endsection

@section('home_breadcrumb_title')
    {{-- @lang('home.Shop') --}}
@endsection

@section('home_content')

    <?php $locale = App::getLocale(); ?>


    @if ($locale == 'ar')
        <style>
            .c-layout-sidebar-menu {
                direction: rtl;
            }

        </style>
    @endif
    @if ($locale == 'ar')

    @endif
    @if ($locale == 'en')

    @endif
    <br>
    <div class="container">
        <div class="row row-offcanvas row-offcanvas-right">

            <div class="col-xs-12 col-sm-9">
                <p class="pull-right visible-xs">
                    {{-- <button type="button" class="btn btn-primary btn-xs" data-toggle="offcanvas">Toggle nav</button> --}}
                </p>
                <div class="jumbotron">
                    @if ($locale == 'ar')
                    <h1>اهلا بك في متجر داما ستور</h1>
                    <p>This is an example to show the potential of an offcanvas layout pattern . Try some responsive-range
                        viewport sizes to see it in action.</p>
                    @endif
                    @if ($locale == 'en')
                    <h1>Welcome to DamaStore</h1>
                    <p>This is an example to show the potential of an offcanvas layout pattern . Try some responsive-range
                        viewport sizes to see it in action.</p>
                    @endif

                </div>
                <div class="row">
                    @foreach ($product as $item)
                        @if ($locale == 'ar')
                            <div class="col-xs-12 col-lg-4 " style="height: 400px;overflow:hidden;position: relative;margin-bottom:10px;">
                                <div class="card">
                                    <img src="/{{ $item->image }}" style="width: 100%;height: 250px;object-fit:cover;">
                                    <div style="padding:3px;">
                                        <h2>{{ $item->ar_name }}</h2>
                                        <p>
                                            {{ $item->ar_short_description }}
                                        </p>
                                        <p><a class="btn btn-default" href="/CproductDetails/{{$item->id}}" role="button"
                                                style="position: absolute;bottom:5px;width:87.2%;">View details &raquo;</a>
                                        </p>
                                    </div>
                                </div>
                            </div>
                            <!--/.col-xs-6.col-lg-4-->
                        @endif
                        @if ($locale == 'en')
                            <div class="col-xs-12 col-lg-4" style="height: 400px;overflow:hidden;position: relative;margin-bottom:10px;">
                                <div class="card">
                                    <img src="/{{ $item->image }}" style="width: 100%;height: 250px;object-fit:cover;">
                                    <div style="padding:3px;">
                                        <h2>{{ $item->name }}</h2>
                                        <p>
                                            {{ $item->short_description }}
                                        </p>
                                        <p><a class="btn btn-default" href="/CproductDetails/{{$item->id}}" role="button"
                                                style="position: absolute;bottom:5px;width:87.2%;">View details &raquo;</a>
                                        </p>
                                    </div>
                                </div>
                            </div>
                            <!--/.col-xs-6.col-lg-4-->
                        @endif
                    @endforeach

                </div>
                <!--/row-->
                {{ $product->links() }}
            </div>
            <!--/.col-xs-12.col-sm-9-->
            <div class="col-xs-6 col-sm-3 sidebar-offcanvas" id="sidebar">
                <div class="list-group">
                    @foreach (\App\Ccategory::all() as $item)
                        @if ($locale == 'ar')
                            <a href="{{ route('Cproduct', $item->id) }}"
                                class="list-group-item">{{ $item->ar_name }}</a>
                        @else
                            <a href="{{ route('Cproduct', $item->id) }}" class="list-group-item">{{ $item->name }}</a>
                        @endif
                    @endforeach
                </div>
            </div>
            <!--/.sidebar-offcanvas-->
        </div>
        <!--/row-->

        <hr>

    </div>
    <!--/.container-->
@endsection



@section('script')

@endsection
