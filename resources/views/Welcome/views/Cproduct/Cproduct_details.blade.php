@extends('Welcome.views.home1')
@section('style')
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css"
        integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/slick-carousel/1.8.1/slick.css"
        integrity="sha512-wR4oNhLBHf7smjy0K4oqzdWumd+r5/+6QO/vDda76MW5iug4PT7v86FoEkySIJft3XA0Ae6axhIvHrqwm793Nw=="
        crossorigin="anonymous" referrerpolicy="no-referrer" />
    <link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Open+Sans:400,700" rel="stylesheet">


    <style type="text/css">
        /*****************globals*************/
        body {
            /* font-family: 'open sans'; */
            overflow-x: hidden;
        }

        img {
            max-width: 100%;
        }

        .preview {
            display: -webkit-box;
            display: -webkit-flex;
            display: -ms-flexbox;
            display: flex;
            -webkit-box-orient: vertical;
            -webkit-box-direction: normal;
            -webkit-flex-direction: column;
            -ms-flex-direction: column;
            flex-direction: column;
        }

        @media screen and (max-width: 996px) {
            .preview {
                margin-bottom: 20px;
            }
        }


        .card {
            margin-top: 50px;
            background: #eee;
            padding: 3em;
            line-height: 1.5em;
        }

        @media screen and (min-width: 997px) {
            .wrapper {
                display: -webkit-box;
                display: -webkit-flex;
                display: -ms-flexbox;
                display: flex;
            }
        }

        .details {
            display: -webkit-box;
            display: -webkit-flex;
            display: -ms-flexbox;
            display: flex;
            -webkit-box-orient: vertical;
            -webkit-box-direction: normal;
            -webkit-flex-direction: column;
            -ms-flex-direction: column;
            flex-direction: column;
        }

        .colors {
            -webkit-box-flex: 1;
            -webkit-flex-grow: 1;
            -ms-flex-positive: 1;
            flex-grow: 1;
        }

        .product-title,
        .price,
        .sizes,
        .colors {
            text-transform: UPPERCASE;
            font-weight: bold;
        }

        .checked,
        .price span {
            color: #ff9f1a;
        }

        .product-title,
        .rating,
        .product-description,
        .price,
        .vote,
        .sizes {
            margin-bottom: 15px;
        }

        .product-title {
            margin-top: 0;
        }

        .size {
            margin-right: 10px;
        }

        .size:first-of-type {
            margin-left: 40px;
        }

        .color {
            display: inline-block;
            vertical-align: middle;
            margin-right: 10px;
            height: 2em;
            width: 2em;
            border-radius: 2px;
        }

        .color:first-of-type {
            margin-left: 20px;
        }

        .add-to-cart,
        .like {
            background: #ff9f1a;
            padding: 1.2em 1.5em;
            border: none;
            text-transform: UPPERCASE;
            font-weight: bold;
            color: #fff;
            -webkit-transition: background .3s ease;
            transition: background .3s ease;
        }

        .add-to-cart:hover,
        .like:hover {
            background: #b36800;
            color: #fff;
        }

        .not-available {
            text-align: center;
            line-height: 2em;
        }

        .not-available:before {
            font-family: fontawesome;
            content: "\f00d";
            color: #fff;
        }

        .orange {
            background: #ff9f1a;
        }

        .green {
            background: #85ad00;
        }

        .blue {
            background: #0076ad;
        }

        .tooltip-inner {
            padding: 1.3em;
        }

        @-webkit-keyframes opacity {
            0% {
                opacity: 0;
                -webkit-transform: scale(3);
                transform: scale(3);
            }

            100% {
                opacity: 1;
                -webkit-transform: scale(1);
                transform: scale(1);
            }
        }

        @keyframes opacity {
            0% {
                opacity: 0;
                -webkit-transform: scale(3);
                transform: scale(3);
            }

            100% {
                opacity: 1;
                -webkit-transform: scale(1);
                transform: scale(1);
            }
        }

        /*# sourceMappingURL=style.css.map */

        .sizeimg {
            width: 100px !important;
            height: 100px !important;
            margin: 2px !important;
            border-radius: 2px !important;
        }

        .slider-nav-img {
            margin-top: 10px;
        }

        .w-100 {
            width: 100%;
        }

        @media only screen and (max-width: 1024px) {
            .slick-slide img {
                width: unset !important;
                height: unset !important;
            }
        }

        .slick-slide img {
            width: unset !important;
            height: unset !important;
            object-fit: unset !important;
            overflow: unset !important;
            border-radius: unset !important;
            border: unset !important;
            box-shadow: unset !important;
            transition: unset !important;
        }

    </style>

    <script src="https://code.jquery.com/jquery-1.12.4.min.js"
        integrity="sha256-ZosEbRLbNQzLpnKIkEdrPv7lOy9C27hHQ+Xp8a4MxAQ=" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/slick-carousel/1.8.1/slick.min.js"
        integrity="sha512-XtmMtDEcNz2j7ekrtHvOVR4iwwaD6o/FUJe6+Zq+HgcCsk3kj4uSQQR8weQ2QVj1o0Pk6PwYLohm206ZzNfubg=="
        crossorigin="anonymous" referrerpolicy="no-referrer"></script>
@endsection

@section('home_breadcrumb_title')
    {{-- @lang('home.Shop') --}}
@endsection

@section('home_content')

    <?php $locale = App::getLocale(); ?>


    @if ($locale == 'ar')
        <style>
            .c-layout-sidebar-menu {
                direction: rtl;
            }

        </style>
    @endif



    <br>

    <div class="container">
        <div class="card shadow">
            <div class="container-fluid">
                <div class="wrapper row">
                    <div class="preview col-md-6">
                        <div class="slider-for">
                            {{-- @dd($product[0]->images) --}}
                            @foreach ($product[0]->images as $gallery)
                                <div><img class="w-100" src="/{{ $gallery->url }}" /></div>
                            @endforeach
                        </div>
                        <div class="slider-nav slider-nav-img">
                            @foreach ($product[0]->images as $gallery)
                            <div><img class="sizeimg" src="/{{ $gallery->url }}" /></div>
                        @endforeach
                        </div>
                    </div>
                    @if ($locale == 'ar')
                    <div class="details col-md-6">
                        <h2 class="product-title">
                            <span style="color: orange" class="glyphicon glyphicon-bookmark"></span>
                            {{$product[0]->ar_name}}</h2>
                        <div class="rating">
                            <span class="review-no">{{$product[0]->ar_short_description}}</span>
                        </div>
                        <p class="product-description">
                            {{$product[0]->ar_description}}
                        </p>
                        <h4 class="price" data-toggle="tooltip" data-placement="top" title="قم بإدخال طلبك وارساله لنا لنعيد إليك السعر" >
                            السعر الحالي: <span>{{$product[0]->price == 0 ? 'اطلب السعر' : "$" .$product[0]->price}}</span></h4>
                        <p class="vote">
                            <span style="color: orange" class="glyphicon glyphicon-star"></span>
                            <strong>91%</strong> استمتع من المشترين بهذا المنتج! <strong>(87 صوت)</strong></p>
                        <h3 class="">الصنف:
                            {{$product[0]->category->ar_name}}
                        </h3>
                        <h3 class="">النوع:
                            {{$product[0]->type->ar_name}}
                        </h3>
                        </h3>
                        <h5 3class="">رمز المنتج :
                            {{$product[0]->product_code}}
                        </h5>
                        <div class="action">
                            {{-- <button class="add-to-cart btn btn-default" type="button">Order Now</button> --}}
                            <button type="button" class=" btn btn-info btn-lg" data-toggle="modal" data-target="#myModal">اطلب الآن</button>

                            {{-- <button class="like btn btn-default btn-lg" type="button"><span class="fa fa-heart"></span></button> --}}
                        </div>
                    </div>
                    @endif
                    @if ($locale == 'en')
                    <div class="details col-md-6">
                        <h2 class="product-title">
                            <span style="color: orange" class="glyphicon glyphicon-bookmark"></span>
                            {{$product[0]->name}}</h2>
                        <div class="rating">
                            <span class="review-no">{{$product[0]->short_description}}</span>
                        </div>
                        <p class="product-description">
                            {{$product[0]->description}}
                        </p>
                        <h4 class="price" data-toggle="tooltip" data-placement="top" title="Order First to show price">current price: <span>{{$product[0]->price == 0 ? 'Order Price' : "$" .$product[0]->price}}</span></h4>
                        <p class="vote">
                            <span style="color: orange" class="glyphicon glyphicon-star"></span>
                            <strong>91%</strong> of buyers enjoyed this product! <strong>(87 votes)</strong></p>
                        <h3 class="">Category:
                            {{$product[0]->category->name}}
                        </h3>
                        <h3 class="sizes">Type:
                            {{$product[0]->type->name}}
                        </h3>
                        </h3>
                        <h5 class="sizes">Product Code:
                            {{$product[0]->product_code}}
                        </h5>
                        <div class="action">
                            {{-- <button class="add-to-cart btn btn-default" type="button">Order Now</button> --}}
                            <button type="button" class=" btn btn-info btn-lg" data-toggle="modal" data-target="#myModal">Order Now</button>

                            {{-- <button class="like btn btn-default btn-lg" type="button"><span class="fa fa-heart"></span></button> --}}
                        </div>
                    </div>
                    @endif

                </div>
            </div>
        </div>
        <div class="modal fade center" style="top: 32%;" id="myModal" role="dialog">
            <div class="modal-dialog">

              <!-- Modal content-->
              <div class="modal-content">
                <div class="modal-header">
                  <button type="button" class="close" data-dismiss="modal">&times;</button>
                  @if ($locale == 'ar')
                  <h4 class="modal-title">ادخل المواصفات المطلوبة</h4>
                  @endif
                  @if ($locale == 'en')
                  <h4 class="modal-title">Enter the required specifications</h4>
                  @endif
                </div>
                <div class="modal-body">
                  <textarea class="form-control"></textarea>
                  <br>
                  @if ($locale == 'ar')
                  <button type="button" class="btn btn-info">اطلب</button>
                  @endif
                  @if ($locale == 'en')
                  <button type="button" class="btn btn-info">Order</button>
                  @endif

                </div>
                <div class="modal-footer">
                    @if ($locale == 'ar')
                    <button type="button" class="btn btn-default" data-dismiss="modal">اغلاق</button>
                    @endif
                    @if ($locale == 'en')
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    @endif
                </div>
              </div>

            </div>
          </div>
    </div>
    <br>

@endsection

@section('script')
    $('.slider-for').slick({
    slidesToShow: 1,
    slidesToScroll: 1,
    arrows: false,
    @if(app()->getLocale()=='ar')
    rtl: true,
    @else
    rtl: false,
    @endif
    asNavFor: '.slider-nav'
    });
    $('.slider-nav').slick({
    slidesToShow: 3,
    slidesToScroll: 1,
    asNavFor: '.slider-for',
    centerMode: true,
    focusOnSelect: true,
    arrows: false,
    @if(app()->getLocale()=='ar')
    rtl: true,
    @else
    rtl: false,
    @endif
    responsive: [
    {
    breakpoint: 600,
    settings: {
    slidesToShow: 2,
    slidesToScroll: 2,

    }
    },
    {
    breakpoint: 480,
    settings: {
    slidesToShow: 1,
    slidesToScroll: 2,
    }
    }
    ]
    });

    $('[data-toggle="tooltip"]').tooltip();
@endsection
