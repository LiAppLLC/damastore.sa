<div role="form" id="ProductFilterForm">
    <ul class="c-shop-filter-search-1 list-unstyled">
        <li>
            <label class="control-label c-font-uppercase c-font-bold"> @lang('home.Category')</label>
            <select id="category-list" onchange="get_type_1(this.value)" name="category" class="form-control c-square c-theme">
                <option value="-1">All</option>
            </select>
        </li>
        <li>
            <label class="control-label c-font-uppercase c-font-bold"> @lang('home.Type')</label>
            <select id="type-list" name="type" class="form-control c-square c-theme">
                <option value="-1">All</option>
            </select>
        </li>

        <li style="visibility: hidden">
            <label class="control-label c-font-uppercase c-font-bold"> @lang('home.products')</label>
            <div class="c-checkbox">
                <input type="checkbox" name="sale" id="checkbox-sidebar-3-1" class="c-check"> <label for="checkbox-sidebar-3-1">
                    <span class="inc"></span> <span class="check"></span> <span class="box"></span>
                    <p> @lang('home.Sale')</p>
                </label>
            </div>
        </li>

        <li>
            <label class="control-label c-font-uppercase c-font-bold"> @lang('home.Price Range')</label>
            <p id="priceRange"></p>
            <div class="c-price-range-box input-group">
                <div class="c-price input-group col-md-6 pull-left">
                    <span class="input-group-addon c-square c-theme">$</span>
                    <input type="number" name="min_num" id="from-input" class="form-control c-square c-theme" placeholder="@lang('home.from')">
                </div>
                <div class="c-price input-group col-md-6 pull-left">
                    <span class="input-group-addon c-square c-theme">$</span>
                    <input type="number" name="max_num" id="to-input" class="form-control c-square c-theme" placeholder="@lang('home.to')">
                </div>
            </div>
        </li>
        <button id="ProductFilterBtn" class="btn c-theme-btn c-btn-square c-btn-uppercase c-font-bold" style="margin-top: 20px;" type="submit"><i class="fa fa-filter" aria-hidden="true"></i> @lang('home.Filter')</button>

</ul><!-- END: CONTENT/SHOPS/SHOP-FILTER-SEARCH-1 -->
</div>

