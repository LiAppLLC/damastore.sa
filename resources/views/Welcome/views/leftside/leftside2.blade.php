<div class="c-layout-sidebar-menu c-theme ">
    <!-- BEGIN: LAYOUT/SIDEBARS/SHOP-SIDEBAR-DASHBOARD -->
    <div class="c-sidebar-menu-toggler">
        <h3 class="c-title c-font-uppercase c-font-bold">@lang('home.My Profile')</h3>
    </div>

    <ul class="c-sidebar-menu collapse " style="display: block !important;">
        <li class="c-dropdown c-open">
            <ul class="c-dropdown-menu">
                <li class="c-active">
                    <a class="navigation" href="{{route('CustomerDashboard')}}"> @lang('home.My Dashbord')</a>
                </li>
                <li class="">
                    <a class="navigation" href="{{route('editprofile')}}"> @lang('home.Edit Profile')</a>
                </li>
                <li class="">
                    <a class="navigation" href="{{route('changePassword')}}"> @lang('home.Change Password')</a>
                </li>
                <li class="">
                    <a class="navigation" href="{{route('PendingOrder')}}"> @lang('home.pendingOrder')</a>
                </li>
                <li class="">
                    <a class="navigation" href="{{route('wallet')}}"> @lang('home.wallet')</a>
                </li>
                   <li class="">
                    <a class="navigation" href="{{route('deletion')}}"> @lang('home.deletion')</a>
                </li>
            </ul>
        </li>
    </ul><!-- END: LAYOUT/SIDEBARS/SHOP-SIDEBAR-DASHBOARD -->
</div>
