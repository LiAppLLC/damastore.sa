


@extends('Welcome.views.home2')


@section('home_content')

    <?php
    $locale = App::getLocale();
    ?>
    @if( $locale=="ar")
        <style>
            .form-group {
                direction: rtl;
            }
            .c-font-uppercase {
                float: right;
            }

        </style>
    @endif

    <h3 class="c-font-uppercase c-font-bold"> @lang('home.Edit Profile')</h3>
    <div class="c-line-left">
        <div class="col-md-8">
            <form class="c-shop-form-1" id="editUserForm">
            {{ csrf_field() }}
            <!-- BEGIN: ADDRESS FORM -->
                <div class="">
                    <div class="form-group">
                        <div class="row">
                            <div class="col-md-12">
                                <label class="control-label">@lang('home.User Name')</label>
                                <input type="text" id="username"  name="username" class="form-control c-square c-theme" placeholder="@lang('home.User Name')" required>
                            </div>
                            <div class="col-md-6">
                                <label class="control-label"> @lang('home.First Name')</label>
                                <input type="text" id="firstName" name="firstName" class="form-control c-square c-theme" placeholder="@lang('home.First Name')" required>
                            </div>
                            <div class="col-md-6">
                                <label class="control-label"> @lang('home.Last Name')</label>
                                <input type="text" id="lastName" name="lastName" class="form-control c-square c-theme" placeholder="@lang('home.Last Name')" required>
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label"> @lang('home.Born Date')</label>
                        <input type="date" id="bornDate" name="bornDate" class="form-control c-square c-theme" placeholder="@lang('home.yyyy-mm-dd')" required>
                    </div>
                    <div class="row">
                        <div class="form-group col-md-6">
                            <label class="control-label"> @lang('home.Email Address')</label>
                            <input type="email" id="email" disabled name="email" class="form-control c-square c-theme" placeholder="@lang('home.Email Address')" required>
                        </div>
                        <div class="form-group col-md-6">
                            <label class="control-label"> @lang('home.Phone')</label>
                            <input type="tel" id="phone" name="phone" class="form-control c-square c-theme" placeholder="@lang('home.Phone')" required>
                        </div>
                    </div>
                    <hr>
                    <h3 style="text-align: center ;width: 100%">@lang('home.Shipping Information')</h3>
                    <div class="row">
                        <div class="form-group col-md-6">
                            <label class="control-label"> @lang('home.city')</label>
                            <input type="text" id="city" name="city" class="form-control c-square c-theme" placeholder="@lang('home.city')" required>
                        </div>
                        <div class="form-group col-md-6">
                            <label class="control-label"> @lang('home.country')</label>
                            <input type="text" id="country" name="country" class="form-control c-square c-theme" placeholder="@lang('home.country')" required>
                        </div>
                    </div>
                    <div class="row">
                        <div class="form-group col-md-6">
                            <label class="control-label"> @lang('home.zipcode')</label>
                            <input type="text" id="zipcode" name="zipcode" class="form-control c-square c-theme" placeholder="@lang('home.zipcode')" required>
                        </div>
                        <div class="form-group col-md-6">
                            <label class="control-label" for="address"> @lang('home.address')</label>
                            <textarea id="address" name="address" class="form-control c-square c-theme" required></textarea>
                        </div>
                    </div>

                    <div class="row c-margin-t-30">
                        <div class="form-group col-md-12" role="group">
                            <button type="submit" class="btn btn-lg c-theme-btn c-btn-square c-btn-uppercase c-btn-bold"> @lang('home.Submit')</button>
                            <button type="submit" class="btn btn-lg btn-default c-btn-square c-btn-uppercase c-btn-bold"> @lang('home.Cancel')</button>
                            <a href="/cart" id="gotocart" style="display: none;background-color: #00c054;color:whitesmoke;" class="btn btn-lg btn-default c-btn-square c-btn-uppercase c-btn-bold"> @lang('home.gotocart')</a>
                        </div>
                    </div>
                </div>
                <!-- END: ADDRESS FORM -->
            </form>
        </div>

    </div>



@endsection




@section('script')



{{--    $('#editUserForm').validate({--}}

{{--    rules: {--}}


{{--    username: {--}}
{{--    required: true--}}
{{--    },--}}
{{--    password: {--}}
{{--    required: true--}}
{{--    },--}}


{{--    firstName: {--}}
{{--    required: true--}}
{{--    },--}}
{{--    lastName: {--}}
{{--    required: true--}}
{{--    },--}}
{{--    bornDate: {--}}
{{--    required: true--}}
{{--    },--}}
{{--    email: {--}}
{{--    required: true--}}
{{--    },--}}
{{--    phone: {--}}
{{--    required: true--}}
{{--    }--}}

{{--    },--}}
{{--    messages: {--}}
{{--    username: {--}}
{{--    required:  "*اختر اسم المستخدم"--}}
{{--    },--}}
{{--    password: {--}}
{{--    required:  "*  ادخل كلمة المرور"--}}
{{--    },--}}


{{--    firstName: {--}}
{{--    required: "*ادخل الاسم الأول"--}}
{{--    },--}}
{{--    lastName: {--}}
{{--    required:  "*ادخل الاسم الأخير"--}}
{{--    },--}}
{{--    bornDate: {--}}
{{--    required:  "*ادخل  تاريخ الولادة"--}}
{{--    },--}}
{{--    email: {--}}
{{--    required:  "*ادخل  الإيميل"--}}
{{--    },--}}
{{--    phone: {--}}
{{--    required: "*ادخل  رقم الهاتف"--}}
{{--    }--}}

{{--    }--}}

{{--    });--}}


	{{--<script>--}}
        var userObject = JSON.parse(localStorage.getItem("DamaStore_userObject"));
{{--		$('#username').val(userObject.username);--}}
{{--		$('#firstName').val(userObject.firstName);--}}
{{--		$('#lastName').val(userObject.lastName);--}}
{{--		$('#bornDate').val(userObject.bornDate);--}}
{{--		$('#email').val(userObject.email);--}}
{{--		$('#phone').val(userObject.phone);--}}
        // blocked
        // bornDate
        // created_at
        // email
        // emailVerifiedAt
        // files
        // firstName
        // id
        // lastActivity
        // lastLoggedIn
        // lastName
        // phone
        // reputation
        // roles
        // token
        // updated_at
        // username
        // verify

{{--        console.log('userObject', userObject);--}}

                $.ajax({
                type: "POST",
                url: '{{route('getUserProfile')}}',
                data:'',
                // mimeType: 'application/json',
                processData: false,
                contentType: false,
                headers: {"Authorization": 'Bearer ' + userObject.token},
                success: function (res) {
                if(res.status == 200) {
                        console.log(res.data);
                        $('#username').val(res.data.username);
                        $('#firstName').val(res.data.firstName);
                        $('#lastName').val(res.data.lastName);
                        $('#bornDate').val(res.data.bornDate);
                        $('#email').val(res.data.email);
                        $('#phone').val(res.data.phone);
                        $('#city').val(res.data.city);
                        $('#country').val(res.data.country);
                        $('#zipcode').val(res.data.zipcode);
                        $('#address').val(res.data.address);
                }
                if(res.status == 400) {
                // console.log(res.data.validator);
                Swal.fire({
                icon: 'error',
                title: 'Oops...',
                text: 'reload page please and check you internet.',
                });
                }
                },
                error: function (res, response) {
Swal.fire({
icon: 'error',
title: 'Oops...',
text: 'reload page please and check you internet.',
});
                },
});





        $('#editUserForm').submit(function (e) {

            var formData = new FormData($(this)[0]);
            e.preventDefault();
            e.stopPropagation();

            $.ajax({

                type: "POST",
                url: '{{route('editUser')}}',
                data: formData,
                // mimeType: 'application/json',
                processData: false,
                contentType: false,
                headers: {"Authorization": 'Bearer ' + userObject.token},
                success: function (res) {
                    if(res.status == 201) {
                        Swal.fire({
                            icon: 'success',
                            title: 'success',
                            text: 'your profile has been edited successfuly, thanks for using our services',
                        }).then(function() {

                            localStorage.setItem("DamaStore_userObject", JSON.stringify(res.data));
                            $('#gotocart').show('slow');
                        });

                    }
                    else{
                        var arr;
                        for(var i in res.data){
                            if(res.data[i][0]){
                                arr+=res.data[i][0]
                            }

                        }
                        Swal.fire({
                            icon: 'error',
                            title: 'Oops...',
                            text: arr,
                        });
                    }

                },
                error: function (res, response) {
                },
            });


        });


@endsection



