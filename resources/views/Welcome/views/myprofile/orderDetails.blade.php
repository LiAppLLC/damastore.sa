@extends('Welcome.views.home1')
@section('style')

	<style>
        .value-div{
            color: #808891;
        }

        /*Form Wizard*/
        .bs-wizard > .bs-wizard-step {padding: 0; position: relative;}
        .bs-wizard > .bs-wizard-step + .bs-wizard-step {}
        .bs-wizard > .bs-wizard-step .bs-wizard-stepnum {color: #595959;  margin-bottom: 5px;}
        .bs-wizard > .bs-wizard-step .bs-wizard-info {color: #999; font-size: 14px;}
        .bs-wizard > .bs-wizard-step > .bs-wizard-dot {position: absolute; width: 30px; height: 30px; display: block; background: #fbe8aa; top: 45px; left: 50%; margin-top: -15px; margin-left: -15px; border-radius: 50%;}
        .bs-wizard > .bs-wizard-step > .bs-wizard-dot:after {content: ' '; width: 14px; height: 14px; background: #fbbd19; border-radius: 50px; position: absolute; top: 8px; left: 8px; }
        .bs-wizard > .bs-wizard-step > .progress {position: relative; border-radius: 0px; height: 8px; box-shadow: none; margin: 20px 0;}
        .bs-wizard > .bs-wizard-step > .progress > .progress-bar {width:0px; box-shadow: none; background: #fbe8aa;}
        .bs-wizard > .bs-wizard-step.complete > .progress > .progress-bar {width:100%;}
        .bs-wizard > .bs-wizard-step.active > .progress > .progress-bar {width:50%;}
        .bs-wizard > .bs-wizard-step:first-child.active > .progress > .progress-bar {width:0%;}
        .bs-wizard > .bs-wizard-step:last-child.active > .progress > .progress-bar {width: 100%;}
        .bs-wizard > .bs-wizard-step.disabled > .bs-wizard-dot {background-color: #f5f5f5;}
        .bs-wizard > .bs-wizard-step.disabled > .bs-wizard-dot:after {opacity: 0;}
        .bs-wizard > .bs-wizard-step:first-child  > .progress {left: 50%; width: 50%;}
        .bs-wizard > .bs-wizard-step:last-child  > .progress {width: 50%; }
        .bs-wizard > .bs-wizard-step.disabled a.bs-wizard-dot{ pointer-events: none; }
        [dir='rtl'] .bs-wizard > .bs-wizard-step:first-child > .progress {
    right: 50% !important;
    width: 50%;
}
	</style>

@endsection

@section('home_content')

    <?php
    $locale = App::getLocale();
    ?>



		<!-- BEGIN: PAGE CONTENT -->
		<div class="c-content-box c-size-lg">
	<div class="container">
		<form class="c-shop-form-1">
			<div class="row">

				<!-- BEGIN: ORDER FORM -->
				<div class="col-md-12">
                    <div class="c-content-bar-1 c-align-left c-bordered c-theme-border c-shadow">
                        <h1 class="c-font-bold c-font-uppercase c-font-24">Your Order</h1>
                        <div class="row bs-wizard col-12" id="progressBar">

                            <div id="Created" class="col-md-2 col-xs-2 bs-wizard-step complete steps_order">
                                <div class="text-center bs-wizard-stepnum" id="Created">Created</div>
                                <div class="progress"><div class="progress-bar"></div></div>
                                <span class="bs-wizard-dot"></span>
                            </div>
                            <div id="Ordered" class="col-md-2 col-xs-2 bs-wizard-step complete steps_order">
                                    <div class="text-center bs-wizard-stepnum"  >Ordered</div>
                                     <div class="progress"><div class="progress-bar"></div></div>
                                <span class="bs-wizard-dot"></span>
                            </div>
                            <div id="Paid" class="col-md-2 col-xs-2 bs-wizard-step disabled steps_order">
                                <div class="text-center bs-wizard-stepnum"  >Paid</div>
                                <div class="progress"><div class="progress-bar"></div></div>
                                <span class="bs-wizard-dot"></span>
                            </div>
                            <div id="Confirm" class="col-md-2 col-xs-2 bs-wizard-step disabled steps_order">
                                <div class="text-center bs-wizard-stepnum" >Confirm</div>
                                <div class="progress"><div class="progress-bar"></div></div>
                                <span class="bs-wizard-dot"></span>
                            </div>
                            <div id="Packed" class="col-md-2 col-xs-2 bs-wizard-step disabled steps_order">
                                <div class="text-center bs-wizard-stepnum" >Packed</div>
                                <div class="progress"><div class="progress-bar"></div></div>
                                <span class="bs-wizard-dot"></span>
                            </div>
                            <div id="Shipped" class="col-md-2 col-xs-2 bs-wizard-step disabled disabled steps_order">
                                <div class="text-center bs-wizard-stepnum" >Shipped</div>
                                <div class="progress"><div class="progress-bar"></div></div>
                                <span class="bs-wizard-dot"></span>
                            </div>
                            <div id="Deliver" class="col-md-2 col-xs-2 bs-wizard-step disabled steps_order">
                                <div class="text-center bs-wizard-stepnum" >Deliver</div>
                                <div class="progress"><div class="progress-bar"></div></div>
                                <span class="bs-wizard-dot"></span>
                            </div>
                            <div id="Failed" class="col-md-2 col-xs-2 bs-wizard-step disabled  steps_order">
                                <div class="text-center bs-wizard-stepnum" >Failed</div>
                                <div class="progress"><div class="progress-bar"></div></div>
                                <span class="bs-wizard-dot"></span>
                            </div>

                        </div>
                        <ul class="c-order list-unstyled">
                            <li class="row c-margin-b-15">
                                <div class="col-md-6 c-font-20"><h2>Order ID </h2></div>
                                <div class="col-md-6 c-font-20"><h2 class="value-div" id="id_number" ></h2></div>
                                <div class="col-md-6 c-font-20"><h2>Order ID From Noon</h2></div>
                                <div class="col-md-6 c-font-20"><h2 class="value-div" id="orderID"></h2></div>

                                <div class="col-md-6 c-font-20"><h2>Order Status</h2></div>
                                <div class="col-md-6 c-font-20"><h2 class="value-div" id="orderStatus_id"></h2></div>

                                <div class="col-md-6 c-font-20"><h2>Quantity</h2></div>
                                <div class="col-md-6 c-font-20"><h2 class="value-div" id="quantity"></h2></div>

                                <div class="col-md-6 c-font-20"><h2>Total Price</h2></div>
                                <div class="col-md-6 c-font-20"><h2 class="value-div" id="total"></h2></div>

                                <div class="col-md-6 c-font-20"><h2>Create Time</h2></div>
                                <div class="col-md-6 c-font-20"><h2 class="value-div" id="created_at"></h2></div>
                                <div class="col-md-12 c-font-20"><h4>Shipping information</h4></div>

                                <div class="col-md-6 c-font-20"><h2>Address</h2></div>
                                <div class="col-md-6 c-font-20"><h2 class="value-div" id="address"></h2></div>
                                <div class="col-md-6 c-font-20"><h2>City</h2></div>
                                <div class="col-md-6 c-font-20"><h2 class="value-div" id="city"></h2></div>
                                <div class="col-md-6 c-font-20"><h2>Country</h2></div>
                                <div class="col-md-6 c-font-20"><h2 class="value-div" id="country"></h2></div>
                                <div class="col-md-6 c-font-20"><h2>Zip Code</h2></div>
                                <div class="col-md-6 c-font-20"><h2 class="value-div" id="zipcode"></h2></div>

                            </li>
                            <li class="row c-border-top c-margin-b-15"></li>
                            <li class="row">
                            </li>
                        </ul>
                    </div>
					<div class="c-content-bar-1 c-align-left c-bordered c-theme-border c-shadow">
                        <h1 class="c-font-bold c-font-uppercase c-font-24">Your Order</h1>
                        <ul class="c-order list-unstyled">
                            <li class="row c-margin-b-15">
                                <div class="col-md-6 col-sm-6 col-xs-6 c-font-20"><h2>Product</h2></div>
                                <div class="col-md-6 col-sm-6 col-xs-6 c-font-20"><h2>Total</h2></div>
                            </li>
                            <li class="row c-border-bottom">
                                <div id="products_order">
                                </div>
                            </li>

                            <li class="row c-margin-b-15 c-margin-t-15">
                                <div class="col-md-6 col-sm-6 col-xs-6 c-font-20">
                                    <p class="c-font-30">Tax</p>
                                </div>
                                <div class="col-md-6 col-sm-6 col-xs-6 c-font-20">
                                    <p class="c-font-bold c-font-30"><span class="c-shipping-total" id="Tax"></span><i style="font-size: 15px">SAR</i></p>
                                </div>
                            </li>
                            <li class="row c-margin-b-15 c-margin-t-15">
                                <div class="col-md-6 col-sm-6 col-xs-6 c-font-20">
                                    <p class="c-font-30">Shipping costs</p>
                                </div>
                                <div class="col-md-6 col-sm-6 col-xs-6 c-font-20">
                                    <p class="c-font-bold c-font-30"><span class="c-shipping-total" id="shipping_costs"></span><i style="font-size: 15px">SAR</i></p>
                                </div>
                            </li>
                            <li class="row c-margin-b-15 c-margin-t-15">
                                <div class="col-md-6 col-sm-6 col-xs-6 c-font-20">
                                    <p class="c-font-30">Total</p>
                                </div>
                                <div class="col-md-6 col-sm-6 col-xs-6 c-font-20">
                                    <p class="c-font-bold c-font-30"><span class="c-shipping-total" id="totalCost"></span><i style="font-size: 15px">SAR</i></p>
                                </div>
                            </li>
                            <li class="row c-border-top c-margin-b-15"></li>
                            <li class="row">
                            </li>
                        </ul>
                    </div>
                    <div class="c-content-bar-1 c-align-left c-bordered c-theme-border c-shadow row">
                        <h1 class="c-font-bold c-font-uppercase c-font-24">Your Payment Details</h1>
                        <ul class="c-order list-unstyled">
                            <li class="row c-margin-b-15">
                                <div class="col-md-6 c-font-20"><h2>Payer Id</h2></div>
                                <div class="col-md-6 c-font-20"><h2 class="value-div" id="payer_id"></h2></div>

                                <div class="col-md-6 c-font-20"><h2>status</h2></div>
                                <div class="col-md-6 c-font-20"><h2 class="value-div" id="status"></h2></div>

                                <div class="col-md-6 c-font-20"><h2>Payee Merchant Id</h2></div>
                                <div class="col-md-6 c-font-20"><h2 class="value-div" id="payeeMerchant_id"></h2></div>

                                <div class="col-md-6 c-font-20"><h2>Payee Email Address</h2></div>
                                <div class="col-md-6 c-font-20"><h2 class="value-div" id="payeeEmail_address"></h2></div>

                                <div class="col-md-6 c-font-20"><h2>Email Address</h2></div>
                                <div class="col-md-6 c-font-20"><h2 class="value-div" id="email_address"></h2></div>

                                <div class="col-md-6 c-font-20"><h2>Amount Value</h2></div>
                                <div class="col-md-6 c-font-20"><h2 class="value-div" ><span id="amountValue"></span> <span id="amountCurrency_code"></span></h2></div>

                                <div class="col-md-6 c-font-20"><h2>Country Code</h2></div>
                                <div class="col-md-6 c-font-20"><h2 class="value-div" id="country_code"></h2></div>

                                <div class="col-md-6 c-font-20"><h2>Create Time</h2></div>
                                <div class="col-md-6 c-font-20"><h2 class="value-div" id="created_at_payment"></h2></div>
                            </li>
                            <li class="row c-border-top c-margin-b-15"></li>
                            <li class="row">
                            </li>
                        </ul>
                    </div>
				</div>
				<!-- END: ORDER FORM -->
			</div>
		</form>
	</div>
</div>
		<!-- END: PAGE CONTENT -->


@endsection



@section('script')

	{{--<script>--}}

        var userObject = JSON.parse(localStorage.getItem("DamaStore_userObject"));

        // blocked
        // bornDate
        // created_at
        // email
        // emailVerifiedAt
        // files
        // firstName
        // id
        // lastActivity
        // lastLoggedIn
        // lastName
        // phone
        // reputation
        // roles
        // token
        // updated_at
        // username
        // verify


    var url = window.location.pathname;
    var id = url.substring(url.lastIndexOf('/') + 1);
    var formData_getProd = new FormData();
    formData_getProd.append('id', id);
    formData_getProd.append("user" , userObject.token);

    //created_at: "2020-11-14T03:10:56.000000Z"​​​
    //orderID: "93G55839H1451001H"
    //orderStatus_id: 1
    //quantity: 9​​​
    //shipping: 0
    //total: 8670
    $('#How-It-Works').append('<div class="loading">Loading&#8230;</div>');
    $.ajax({

        type: "POST",
        url: '/api/GetOrderDetails',
        data: formData_getProd,
        // mimeType: 'application/json',
        processData: false,
        contentType: false,
        headers: {"Authorization": 'Bearer ' + userObject.token},
        success: function (res) {
 console.log(res);
            $('#products_order').empty();


            // ==============================================================================
            // order
{{--            var date = new Date(res.data.orders.created_at),--}}
{{--            yr = date.getFullYear(),--}}
{{--            month = date.getMonth() < 10 ? '0' + date.getMonth() : date.getMonth(),--}}
{{--            day = date.getDate() < 10 ? '0' + date.getDate() : date.getDate(),--}}
{{--            newDate = yr + '-' + month + '-' + day;--}}
            $('#created_at').text(res.data.orders.created_at);
            $('#id_number').text(res.data.orders.id);
            $('#orderID').text(res.data.orders.orderID);
            $('#quantity').text(res.data.orders.quantity);
            $('#total').text((res.data.orders.total)+' SAR');
            $('#totalCost').text((res.data.orders.total));
            $('#shipping_costs').text(parseInt(res.data.orders.shipping_costs));
            $('#Tax').text(res.data.orders.tax);
            $('#orderStatus_id').text(res.data.orders.orderStatus_name);
            // ==============================================================================

            // ==============================================================================
            // OrderStates
             if(res.data.orders.orderStatus_name=="Paid"){
    $('#'+res.data.orders.orderStatus_name).addClass('complete ');
    $('#'+res.data.orders.orderStatus_name).removeClass('disabled');
              }
    if(res.data.orders.orderStatus_name=="Confirm"){
    $('#'+res.data.orders.orderStatus_name).addClass('complete ');
    $('#'+res.data.orders.orderStatus_name).removeClass('disabled');
    $('#Paid').addClass('complete');
    $('#Paid').removeClass('disabled');
    }
    if(res.data.orders.orderStatus_name=="Packed"){
    $('#'+res.data.orders.orderStatus_name).addClass('complete ');
    $('#'+res.data.orders.orderStatus_name).removeClass('disabled');
    $('#Paid').addClass('complete');
    $('#Paid').removeClass('disabled');
    $('#Confirm').addClass('complete');
    $('#Confirm').removeClass('disabled');
    }
    if(res.data.orders.orderStatus_name=="Shipped"){
    $('#'+res.data.orders.orderStatus_name).addClass('complete ');
    $('#'+res.data.orders.orderStatus_name).removeClass('disabled');
    $('#Paid').addClass('complete');
    $('#Paid').removeClass('disabled');
    $('#Confirm').addClass('complete');
    $('#Confirm').removeClass('disabled');
    $('#Packed').addClass('complete');
    $('#Packed').removeClass('disabled');
    }
    if(res.data.orders.orderStatus_name=="Deliver"){
    $('#'+res.data.orders.orderStatus_name).addClass('complete ');
    $('#'+res.data.orders.orderStatus_name).removeClass('disabled');
    $('#Paid').addClass('complete');
    $('#Paid').removeClass('disabled');
    $('#Confirm').addClass('complete');
    $('#Confirm').removeClass('disabled');
    $('#Packed').addClass('complete');
    $('#Packed').removeClass('disabled');
    $('#Shipped').addClass('complete');
    $('#Shipped').removeClass('disabled');
    }
    if(res.data.orders.orderStatus_name=="Failed"){
    $('#Failed').addClass('complete active');
    $('#Failed').removeClass('disabled');
    }

            var found = false;
            var active = '';
            var len = res.data.OrderStates.length;
            var col = 0;
            if(len > 6){
            col = 1;
            }
            if(len == 5 || len == 6){
            col = 2;
            }
            if(len == 4){
            col = 3;
            }
            if(len == 3){
            col = 4;
            }
            if(len == 2){
            col = 6;
            }
            if(len == 8){
            col = 12;
            }
            res.data.OrderStates.forEach(function (item, index, arr) {

            if(!found){
            if(item.active == 1){
            active = 'active';
            found = true;
            }else{
            active = 'complete';
            }
            }else{
            active = 'disabled';
            }

{{--            $('#progressBar').append('' +--}}
{{--            ' <div class="col-md-' + col + ' col-xs-' + col + ' bs-wizard-step ' + active + '"> ' +--}}
{{--                '     <div class="text-center bs-wizard-stepnum">' + item.name + '</div> ' +--}}
{{--                '     <div class="progress"><div class="progress-bar"></div></div> ' +--}}
{{--                '     <span class="bs-wizard-dot"></span> ' +--}}
{{--                ' </div> ' +--}}
{{--            '');--}}

            });

            // ==============================================================================

            // ==============================================================================
            // payment
    if(res.data.payment){
    var date = new Date(res.data.payment.created_at),
    yr = date.getFullYear(),
    month = date.getMonth()+1,
    day = date.getDate(),
    newDate = yr + '-' + month + '-' + day;
    $('#payer_id').text(res.data.payment.payer_id);
    $('#status').text(res.data.payment.status);
    $('#payeeMerchant_id').text(res.data.payment.payeeMerchant_id);
    $('#payeeEmail_address').text(res.data.payment.payeeEmail_address);
    $('#email_address').text(res.data.payment.email_address);
    $('#amountCurrency_code').text(res.data.payment.amountCurrency_code);
    $('#amountValue').text(res.data.payment.amountValue);
    $('#country_code').text(res.data.payment.country_code);
    $('#address').text(res.data.payment.address);
    $('#zipcode').text(res.data.payment.zipcode);
    $('#city').text(res.data.payment.city);
    $('#country').text(res.data.payment.country);
    $('#created_at_payment').text(newDate);
    }else{
    newDate=" ";
    $('#payer_id').text('null');
    $('#status').text('null');
    $('#payeeMerchant_id').text('null');
    $('#payeeEmail_address').text('null');
    $('#email_address').text('null');
    $('#amountCurrency_code').text('null');
    $('#amountValue').text('null');
    $('#country_code').text('null');
    $('#created_at_payment').text(newDate);
    }



            // ==============================================================================

            // ==============================================================================
            // products

            // ==============================================================================

            res.data.ordered_product.forEach(function (item, index, arr) {


                var date = new Date(item.created_at),
                    yr = date.getFullYear(),
                    month = date.getMonth()+1,
                    day = date.getDate(),
                    newDate = yr + '-' + month + '-' + day;

                $('#products_order').append('<li class="row c-margin-b-15 c-margin-t-15"> ' +
				'		<div class="col-md-6 col-sm-6 col-xs-6 c-font-20"> ' +
				'			<a href="/productDetails/' + item.product_id + '" class="navigation c-theme-link">' + item.product_name + ' x ' + item.qty + '</a></div> ' +
				'		<div class="col-md-6 col-sm-6 col-xs-6 c-font-20"> ' +
				'			<p class="">' + item.price + ' SAR</p> ' +
				'		</div> ' +
				'	</li>');


            });
            $( ".loading" ).remove();
        },
        error: function (res, response) {

            $( ".loading" ).remove();
        },
    });












@endsection
