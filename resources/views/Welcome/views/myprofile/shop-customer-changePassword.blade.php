


@extends('Welcome.views.home2')


@section('home_content')

    <?php
    $locale = App::getLocale();
    ?>

    @if( $locale=="ar")
        <style>
            .form-group {
                direction: rtl;
            }
            .c-font-uppercase {
                float: right;
            }

        </style>
    @endif

    <div class="c-layout-sidebar-content ">
        <!-- BEGIN: PAGE CONTENT -->
            <div class="c-line-left">
                <div class="row">
                    <div class="col-md-6">
                        <h3 class="c-font-uppercase c-font-bold"> @lang('home.Change Password')</h3>
                        <br>
                        <br>
                        <form class="c-shop-form-1" id="changePasswordForm">
                        {{ csrf_field() }}
                        <!-- BEGIN: ADDRESS FORM -->
                            <div class="">



                                <!-- BEGIN: PASSWORD -->
                                <div class="row">
                                    <div class="form-group col-md-12">
                                        <label class="control-label"> @lang('home.Old Password')</label>
                                        <input type="password" id="password" name="password" class="form-control c-square c-theme" placeholder="@lang('home.Password')">
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="form-group col-md-12">
                                        <label class="control-label"> @lang('home.New Password')</label>
                                        <input type="password" id="newpassword" name="newpassword" class="form-control c-square c-theme" placeholder="@lang('home.Password')">
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="form-group col-md-12">
                                        <label class="control-label"> @lang('home.Repeat Password')</label>
                                        <input type="password" id="repeated_newpassword"  name="repeated_newpassword" class="form-control c-square c-theme" placeholder="@lang('home.Password')">
                                    </div>
                                </div>
                                <!-- END: PASSWORD -->




                                <div class="row c-margin-t-30">
                                    <div class="form-group col-md-12" role="group">
                                        <button type="submit" class="btn btn-lg c-theme-btn c-btn-square c-btn-uppercase c-btn-bold"> @lang('home.Submit')</button>
                                        <button type="submit" class="btn btn-lg btn-default c-btn-square c-btn-uppercase c-btn-bold"> @lang('home.Cancel')</button>
                                    </div>
                                </div>
                            </div>
                            <!-- END: ADDRESS FORM -->
                        </form>
                    </div>
                </div>

            </div>
        </div><!-- END: PAGE CONTENT -->
    </div>



@endsection






@section('script')



    $('#changePasswordForm').validate({

    rules: {



    password: {
    required: true
    },
    repeated_newpassword: {
    required: true
    },
    newpassword: {
    required: true
    }






    },
    messages: {

    password: {
    required:  "*  ادخل كلمة المرور"
    },
    repeated_newpassword: {
    required: "*  ادخل كلمة المرور ثانية"
    },
    newpassword: {
    required: "*  ادخل كلمة المرور الجديدة"
    }


    }

    });





	{{--<script>--}}
        var userObject = JSON.parse(localStorage.getItem("DamaStore_userObject"));
        // blocked
        // bornDate
        // created_at
        // email
        // emailVerifiedAt
        // files
        // firstName
        // id
        // lastActivity
        // lastLoggedIn
        // lastName
        // phone
        // reputation
        // roles
        // token
        // updated_at
        // username
        // verify
        console.log('userObject', userObject);


        $('#changePasswordForm').submit(function (e) {

            var formData = new FormData($(this)[0]);
            e.preventDefault();
            e.stopPropagation();

            var repeated_newpassword = $('#repeated_newpassword').val();
            var newpassword = $('#newpassword').val();

            if(newpassword == repeated_newpassword) {
                $.ajax({

                    type: "POST",
                    url: '{{route('api_changePassword')}}',
                    data: formData,
                    // mimeType: 'application/json',
                    processData: false,
                    contentType: false,
                    headers: {"Authorization": 'Bearer ' + userObject.token},
                    success: function (res) {
                        if (res.status == 200) {
                            Swal.fire({
                                icon: 'success',
                                title: 'success',
                                text: 'your Password has been Changed successfuly, thanks for using our services',
                            }).then(function () {
                                window.location = "/CustomerDashboard";
                            });

                        }
                        if (res.status == 500) {
                            // console.log(res.data.validator);
                            Swal.fire({
                                icon: 'error',
                                title: 'Oops...',
                                text: res.msg,
                            });
                        }

                    },
                    error: function (res, response) {
                    },
                });
            }else{
                Swal.fire({
                    icon: 'error',
                    title: 'Oops...',
                    text: 'New password not matched',
                });
			}


        });


@endsection



