@extends('Welcome.views.home2')


@section('home_content')

    <?php
    $locale = App::getLocale();
    ?>



	<div class="c-layout-sidebar-content ">
		<!-- BEGIN: PAGE CONTENT -->
		<!-- BEGIN: CONTENT/SHOPS/SHOP-CUSTOMER-DASHBOARD-1 -->
		<div class="c-content-title-1">
			<h3 class="c-font-uppercase c-font-bold">@lang('home.deletion') - {{$user->username}}</h3>
		</div>
		<div class="row">
			<div class="col-md-8">
		<div class="row">
            <div class="col-md-12">
                <i class="fa fa-info-circle" style="color:gray;"></i>
                @lang('home.deletionuser1')
            </div>
            <hr>
            <div class="col-md-12">
                <i class="fa fa-info-circle" style="color:gray;"></i>
                @lang('home.deletionuser2')
            </div>
            <hr>
            <style>
                .button {
                  padding: 15px 25px;
                  font-size: 24px;
                  text-align: center;
                  cursor: pointer;
                  outline: none;
                  color: #fff;
                  background-color: #ff0000e3;
                  border: none;
                  border-radius: 15px;
                  box-shadow: 0 9px #999;
                  transition: all 0.1s;
                }
                
                .button:hover {background-color: #d64343;
                color:black;
                }
                
                .button:active {
                  background-color: #ff0000;
                  box-shadow: 0 5px #666;
                  transform: translateY(4px);

                }
                </style>
            <div class="col-md-12 text-center m-5" >
                <div class="row m-5 p-5" style="padding: 50px;border:2px dashed red;border-radius: 15px;margin:10px;">
                    <div class="col-md-12 m-5 p-5">
                        <a href="" class="button m-5 " data-id="">@lang('home.delete')</a>
                    </div>
                </div>
            </div>
        </div>
			</div>
		</div><!-- END: CONTENT/SHOPS/SHOP-CUSTOMER-DASHBOARD-1 -->
		<!-- END: PAGE CONTENT -->
	</div>



@endsection

@section('script')
var userObject = JSON.parse(localStorage.getItem("DamaStore_userObject"));
if(userObject){
    consloe.log('ok');
  }else{
      window.location.href="/";
  }
$(document).on('click', '.button', function (e) {
    e.preventDefault();
    var id = $(this).data('id');
    Swal.fire({
            title: "@lang('home.delete1')",
            type: "error",
            confirmButtonClass: "btn-danger",
            confirmButtonText: "@lang('home.delete2')",
            cancelButtonText: '@lang('home.delete3')',
            showCancelButton: true,
        }).then((result) => {
            if (result.isConfirmed) {
            $.ajax({
                type: "POST",
                headers: {"Authorization": 'Bearer ' + userObject.token},
                url: "{{route('user_delete')}}",
                data: {id:'{{$user->id}}'},
                success: function (data) {
                    localStorage.removeItem("DamaStore_userObject");
                    Swal.fire({
                        title: '<i class="fas fa-check"></i>',
                        text: '',
                        imageUrl: '/assets/base/img/layout/logos/logo-5.png',
                        imageWidth: 400,
                        imageHeight: 200,
                        imageAlt: 'Custom image',
                      }).then((result) => {
                        {{-- localStorage.removeItem("DamaStore_userObject"); --}}
                        window.location.href="/";
                      })
                    }         
            });
        }else if (result.isDenied) {
            {{-- Swal.fire('Changes are not saved', '', 'info') --}}
          }
    });
});

@endsection
