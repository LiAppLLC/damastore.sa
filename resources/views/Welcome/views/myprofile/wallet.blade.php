


@extends('Welcome.views.home2')

@section('style')

	<style>
		@media (max-width: 990px) {
			.row-div {
				margin-right: 0;
				margin-left: 0;
			}
			.col-hide{
				display: none;
			}
		}


	</style>

@endsection

@section('home_content')

    <?php
    $locale = App::getLocale();
    ?>			<div class="c-layout-sidebar-content ">
		<!-- BEGIN: PAGE CONTENT -->
		<!-- BEGIN: CONTENT/SHOPS/SHOP-ORDER-HISTORY-2 -->
		<div class="c-content-title-1">
			<h3 class="c-font-uppercase c-font-bold"> @lang('home.wallet')</h3>
		</div>
		<div class="row c-margin-b-40 c-order-history-2 row-div">
			<div class="row c-cart-table-title">
				<div class="col-md-12 c-cart-ref">
					<h3 class="c-font-uppercase c-font-bold c-font-16 c-font-grey-2">
                         @lang('home.nomoney')
                        </h3>
				</div>
			</div>
			<!-- BEGIN: ORDER HISTORY ITEM ROW -->
			<div id="orders">

			</div>
			<!-- END: ORDER HISTORY ITEM ROW -->


		</div>

		<!-- END: CONTENT/SHOPS/SHOP-ORDER-HISTORY-2 -->
		<!-- END: PAGE CONTENT -->
	</div>


@endsection





@section('script')
@endsection
