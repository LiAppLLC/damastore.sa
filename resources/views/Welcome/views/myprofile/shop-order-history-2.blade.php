


@extends('Welcome.views.home2')


@section('style')

	<style>
		@media (max-width: 990px) {
			.row-div {
				margin-right: 0;
				margin-left: 0;
			}
			.col-hide{
				display: none;
			}
		}


	</style>

@endsection

@section('home_content')

    <?php
    $locale = App::getLocale();
    ?>
    @if( $locale=="ar")
        <style>
            .form-group {
                direction: rtl;
            }
            .c-font-uppercase {
                float: right;
            }

        </style>
    @endif

    <div class="c-layout-sidebar-content ">
		<!-- BEGIN: PAGE CONTENT -->
		<!-- BEGIN: CONTENT/SHOPS/SHOP-ORDER-HISTORY-2 -->
		<div class="c-content-title-1">
			<h3 class="c-font-uppercase c-font-bold"> @lang('home.Order History')</h3>
		</div>
		<div class="row c-margin-b-40 c-order-history-2 row-div">
			<div class="row c-cart-table-title">
				<div class="col-md-3 c-cart-ref">
					<h3 class="c-font-uppercase c-font-bold c-font-16 c-font-grey-2"> @lang('home.Order')</h3>
				</div>
				<div class="col-md-1 c-cart-desc">
					<h3 class="c-font-uppercase c-font-bold c-font-16 c-font-grey-2 col-hide"> @lang('home.Price')</h3>
				</div>
				<div class="col-md-1 c-cart-price">
					<h3 class="c-font-uppercase c-font-bold c-font-16 c-font-grey-2 col-hide"> @lang('home.Quantity')</h3>
				</div>
				<div class="col-md-2 c-cart-qty">
					<h3 class="c-font-uppercase c-font-bold c-font-16 c-font-grey-2 col-hide"> @lang('home.Date')</h3>
				</div>
				<div class="col-md-2 c-cart-price">
					<h3 class="c-font-uppercase c-font-bold c-font-16 c-font-grey-2 col-hide"> @lang('home.OrderStatus')</h3>
				</div>
				<div class="col-md-3 c-cart-price">
					<h3 class="c-font-uppercase c-font-bold c-font-16 c-font-grey-2"> @lang('home.Details')</h3>
				</div>
			</div>
			<!-- BEGIN: ORDER HISTORY ITEM ROW -->
			<div id="orders">

			</div>
			<!-- END: ORDER HISTORY ITEM ROW -->


		</div>

		<!-- END: CONTENT/SHOPS/SHOP-ORDER-HISTORY-2 -->
		<!-- END: PAGE CONTENT -->
	</div>


@endsection





@section('script')


	{{--<script>--}}
	var userObject = JSON.parse(localStorage.getItem("DamaStore_userObject"));
	$('#username').val(userObject.username);
	$('#firstName').val(userObject.firstName);
	$('#lastName').val(userObject.lastName);
	$('#bornDate').val(userObject.bornDate);
	$('#email').val(userObject.email);
	$('#phone').val(userObject.phone);
	// blocked
	// bornDate
	// created_at
	// email
	// emailVerifiedAt
	// files
	// firstName
	// id
	// lastActivity
	// lastLoggedIn
	// lastName
	// phone
	// reputation
	// roles
	// token
	// updated_at
	// username
	// verify
	console.log('userObject', userObject);

	function getData() {

	var formData = new FormData();
	formData.append("user" , userObject.token);

	$.ajax({

	type: "POST",
	url: '/api/MyHistoryOrderList',
	data: formData,
	// mimeType: 'application/json',
	processData: false,
	contentType: false,
	headers: {"Authorization": 'Bearer ' + userObject.token},
	success: function (res) {
	console.log('ggg', res);
	$('#orders').empty();

	//created_at: "2020-11-14T02:44:57.000000Z"
	//id: 3
	//orderStatus_id: 1
	//quantity: 8
	//shipping: 0
	//total: 6229
	//updated_at: "2020-11-14T02:44:57.000000Z"
	//user_id: 2

	res.data.forEach(function (item, index, arr) {

	//var image;
	//if (item.image) {
	//    image = get_hostname($(location).attr("href")) + '/images/' + item.image.path;
	//} else {
	//    image = 'https://static.thenounproject.com/png/2999524-200.png';
	//}

	var date = new Date(item.created_at),
	yr = date.getFullYear(),
	month = date.getMonth() < 10 ? '0' + date.getMonth() : date.getMonth(),
	day = date.getDate() < 10 ? '0' + date.getDate() : date.getDate(),
	newDate = yr + '-' + month + '-' + day;

	$('#orders').append('<div class="row c-cart-table-row"> ' +
		' <h2 class="c-font-uppercase c-font-bold c-theme-bg c-font-white c-cart-item-title c-cart-item-first">Item 1 </h2> ' +
		' <div class="col-md-3 col-sm-3 col-xs-6 c-cart-ref"> ' +
			' 	<p class="c-cart-sub-title c-theme-font c-font-uppercase c-font-bold">Order</p> ' +
			' 	<p>#' + item.orderID + '</p> ' +
			' </div> ' +
		' <div class="clearfix col-md-1 col-sm-3 col-xs-6 c-cart-price col-hide"> ' +
			' 	<p class="c-cart-sub-title c-theme-font c-font-uppercase c-font-bold">Price</p> ' +
			' 	<p class="c-cart-price c-font-bold">$' + item.total + '</p> ' +
			' </div> ' +
		' <div class="col-md-1 col-sm-3 col-xs-6 c-cart-total col-hide"> ' +
			' 	<p class="c-cart-sub-title c-theme-font c-font-uppercase c-font-bold">Quantity</p> ' +
			' 	<p class="c-cart-price c-font-bold">' + item.quantity + '</p> ' +
			' </div> ' +
		' <div class="col-md-2 col-sm-3 col-xs-6 c-cart-qty col-hide"> ' +
			' 	<p class="c-cart-sub-title c-theme-font c-font-uppercase c-font-bold">Date</p> ' +
			' 	<p>' + newDate + '</p> ' +
			' </div> ' +
		' <div class="col-md-2 col-sm-3 col-xs-6 c-cart-total col-hide"> ' +
			' 	<p class="c-cart-sub-title c-theme-font c-font-uppercase c-font-bold">orderStatus_name</p> ' +
			' 	<p class="c-cart-price c-font-bold">' + item.orderStatus_name + '</p> ' +
			' </div> ' +
		' <div class="col-md-3 col-sm-3 col-xs-6 c-cart-qty"> ' +
			' 	<p class="c-cart-sub-title c-theme-font c-font-uppercase c-font-bold">Details</p> ' +
			' 	<a class="navigation"  href="orderDetails/' + item.orderID + '"><button type="button" class="btn btn-block btn-success" data-orderID="' + item.orderID + '" >Details</button></a> ' +
			' </div> ' +
		' </div>');

	});

	},
	error: function (res, response) {
	},
	});

	}

	getData();


@endsection
