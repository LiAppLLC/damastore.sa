@extends('Welcome.views.home2')


@section('home_content')

    <?php
    $locale = App::getLocale();
    ?>



	<div class="c-layout-sidebar-content ">
		<!-- BEGIN: PAGE CONTENT -->
		<!-- BEGIN: CONTENT/SHOPS/SHOP-CUSTOMER-DASHBOARD-1 -->
		<div class="c-content-title-1">
			<h3 class="c-font-uppercase c-font-bold">@lang('home.My Dashbord')</h3>
			{{--<div class="c-line-left"></div>--}}
			{{--<p class="">--}}
			{{--Hello <a href="#" class="c-theme-link">Drake Hiro</a> (not <a href="#" class="c-theme-link">Drake Hiro</a>? <a href="#" class="c-theme-link">Sign out</a>). <br />--}}
			{{--</p>--}}
		</div>
		<div class="row">
			<div class="col-md-8">
				<h3 class="c-font-uppercase c-font-bold" id="user_username"></h3>
				<ul class="list-unstyled">
					<li id="user_data"></li>
					<li id="user_phone"></li>
					{{--<li>Fax: 800 123 3456</li>--}}
					<li> @lang('home.Email'): <a href="" class="navigation c-theme-link" id="user_email"></a></li>
				</ul>
			</div>
		</div><!-- END: CONTENT/SHOPS/SHOP-CUSTOMER-DASHBOARD-1 -->
		<!-- END: PAGE CONTENT -->
	</div>



@endsection

@section('script')


	{{--<script>--}}
	var userObject = JSON.parse(localStorage.getItem("DamaStore_userObject"));

	$('#user_email').text(userObject.email);
	$('#user_email').attr('href', 'mailto:' + userObject.email);
	if(userObject.phone){
		$('#user_phone').text(userObject.phone);
	}
	$('#user_username').text(userObject.username);
	if(userObject.firstName && userObject.lastName && userObject.bornDate){
		$('#user_data').text(userObject.firstName + ' ' + userObject.lastName + ', ' + userObject.bornDate);
	}
	// blocked
	// bornDate
	// created_at
	// email
	// emailVerifiedAt
	// files
	// firstName
	// id
	// lastActivity
	// lastLoggedIn
	// lastName
	// phone
	// reputation
	// roles
	// token
	// updated_at
	// username
	// verify
	console.log('userObject', userObject);



@endsection
