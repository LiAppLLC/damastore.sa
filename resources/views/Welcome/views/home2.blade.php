@extends('Welcome.home')

@section('content')

	<!-- BEGIN: PAGE CONTAINER -->
	<div class="c-layout-page">
		@include('Welcome.views.breadcrumb.breadcrumb')
		<div class="container">
			<div class="c-layout-sidebar-menu c-theme ">
				@include('Welcome.views.leftside.leftside2')
			</div>
			<div class=" ">
				@yield('home_content')
			</div>
		</div>
	</div>

@endsection

{{-- c-layout-sidebar-content --}}
