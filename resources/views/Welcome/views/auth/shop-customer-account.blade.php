@extends('Welcome.views.home1')
@section('style')

    <meta name="google-signin-client_id" content="728080330284-30rvpujtk8qfef37pggpcd1iv3il4k34.apps.googleusercontent.com">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <link rel="stylesheet" href="https://jqueryvalidation.org/files/demo/site-demos.css">


@endsection

@section('home_breadcrumb_title')
@lang('home.Registeration')
@endsection



@section('home_content')

    <?php
    $locale = App::getLocale();
    ?>

    @if( $locale=="ar")
        <style>
            .form-group {
                direction: rtl;
            }


        </style>
    @endif

    <body class="c-layout-header-fixed c-layout-header-mobile-fixed c-layout-header-topbar c-layout-header-topbar-collapse">



    <!-- BEGIN: PAGE CONTENT -->
    <!-- BEGIN: CONTENT/SHOPS/SHOP-LOGIN-REGISTER-1 -->
    <div class="c-content-box c-size-md c-bg-white">
        <div class="container">
            <div class="c-shop-login-register-1">
                <div class="row">
                    <div class="col-md-6">
                        <div class="panel panel-default c-panel">
                            <div class="panel-body c-panel-body">
                                <form class="c-form-login" id="signInForm">
                                    {{ csrf_field() }}
                                    <div class="form-group has-feedback">
                                        <input type="text" name="username" id="username" class="form-control c-square c-theme input-lg" placeholder="@lang('home.Username')">
                                        <span class="glyphicon glyphicon-user form-control-feedback c-font-grey"></span>
                                    </div>
                                    <div class="form-group has-feedback">
                                        <input type="password" name="password" id="password" class="form-control c-square c-theme input-lg" placeholder="@lang('home.Password')">
                                        <span class="glyphicon glyphicon-lock form-control-feedback c-font-grey"></span>
                                    </div>
                                    <div class="row c-margin-t-40">
                                        <div class="col-xs-8">
                                        </div>
                                        <div class="col-xs-4">
                                            <button type="submit" class="pull-right btn btn-lg c-theme-btn c-btn-square c-btn-uppercase c-btn-bold">@lang('home.Login') </button>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <style>
.open-ids {
  width: 236px;
  margin: 0 auto;
}

.auth-provider {
   font-family: system-ui,roboto,sans-serif;
  font-weight: 500;
   font-size: 16px;
  line-height: 40px;
  padding: 0 21px;
  border-radius: 20px;
  cursor: pointer;
  margin-bottom: 16px;
  box-sizing: border-box;
  border: 1px solid #d6d9dc;
  text-align: center;
  background: #FFF;
  color: #535a60
}
.abcRioButton {
    width: 100% !important;
    box-shadow: none;
   font-family: system-ui,roboto,sans-serif;
  font-weight: 500;
   font-size: 16px;
  line-height: 40px;
  padding: 0 21px;
  border-radius: 20px;
  cursor: pointer;
  margin-bottom: 16px;
  box-sizing: border-box;
  border: 1px solid #d6d9dc;
  text-align: center;
  background: #FFF;
  color: #535a60
}
.abcRioButtonContents {
    font-family: system-ui,roboto,sans-serif !important;
  font-weight: 500 !important;
   font-size: 16px !important;
}

.customBtn {
  color: #535a60 !important;
  border-color: #d6d9dc !important;
}

.facebook-login {
  color: #FFF;
  background-color: #395697;
  border-color: transparent
}

.kyber-login {
  color: #FFF;
  background-color: #54ae85;
  border-color: transparent
}

.telegram-login {
  margin-bottom: 12px;
}

.svg-icon {
  vertical-align: middle;
  padding-bottom: 4px;
}
                                        </style>
                                        <br>
                                        <div class="col-md-12">
                                            <div class="open-ids">

                                                 <div class=" google-login" data-longtitle="true" id="customBtn">
                                                    <svg aria-hidden="true" class="svg-icon" width="18" height="18" viewBox="0 0 18 18">
                                                       <g>
                                                          <path d="M16.51 8H8.98v3h4.3c-.18 1-.74 1.48-1.6 2.04v2.01h2.6a7.8 7.8 0 0 0 2.38-5.88c0-.57-.05-.66-.15-1.18z" fill="#4285F4"></path>
                                                          <path d="M8.98 17c2.16 0 3.97-.72 5.3-1.94l-2.6-2a4.8 4.8 0 0 1-7.18-2.54H1.83v2.07A8 8 0 0 0 8.98 17z" fill="#34A853"></path>
                                                          <path d="M4.5 10.52a4.8 4.8 0 0 1 0-3.04V5.41H1.83a8 8 0 0 0 0 7.18l2.67-2.07z" fill="#FBBC05"></path>
                                                          <path d="M8.98 4.18c1.17 0 2.23.4 3.06 1.2l2.3-2.3A8 8 0 0 0 1.83 5.4L4.5 7.49a4.77 4.77 0 0 1 4.48-3.3z" fill="#EA4335"></path>
                                                       </g>
                                                    </svg>
                                                    <span class="buttonText">Log in with Google</span>

                                                 </div>
                                                 <div class="auth-provider facebook-login" id="face">
                                                    <svg aria-hidden="true" class="svg-icon" width="18" height="18" viewBox="0 0 18 18">
                                                       <path d="M1.88 1C1.4 1 1 1.4 1 1.88v14.24c0 .48.4.88.88.88h7.67v-6.2H7.46V8.4h2.09V6.61c0-2.07 1.26-3.2 3.1-3.2.88 0 1.64.07 1.87.1v2.16h-1.29c-1 0-1.19.48-1.19 1.18V8.4h2.39l-.31 2.42h-2.08V17h4.08c.48 0 .88-.4.88-.88V1.88c0-.48-.4-.88-.88-.88H1.88z" fill="#fff"></path>
                                                    </svg>
                                                    Log in with Facebook
                                                 </div>

                                              </div>
                                            {{-- <div id='gbtn' class="g-signin2" data-onsuccess="onSignIn" ></div> --}}
                                        </div>

                                        <form  id="form_test">
                                            {{csrf_field()}}
                                            <input id="em" type="text" name="email" value="" hidden>
                                            <input id="name" type="text" name="name" value="" hidden>
                                            <input id="Fname" type="text" name="Fname" value="" hidden>
                                            <input id="Lname" type="text" name="Lname" value="" hidden>
                                        </form>


                                        {{--<a class="navigation" href="#" onclick="signOut();">@lang('home.Sign out')</a>--}}


                                    </div>
                                </form>
                                <div class="c-margin-fix" style="margin-top: 20px;">
                                    <div class="c-checkbox c-toggle-hide" data-object-selector="c-form-forgetMyPassword" data-animation-speed="600">
                                        <input type="checkbox" id="checkbox6-445" class="c-check">
                                        <label for="checkbox6-445"> <span class="inc"></span> <span class="check"></span>
                                            <span class="box"></span>  @lang('home.Forget My Password!') </label>
                                    </div>
                                </div>
                                <form class="c-form-forgetMyPassword c-margin-t-20" id="forgetPasswordForm">
                                    {{ csrf_field() }}
                                    <div class="form-group has-feedback">
                                        <input type="text" name="username" class="form-control c-square c-theme input-lg" placeholder="@lang('home.Username') ">
                                        <span class="glyphicon glyphicon-user form-control-feedback c-font-grey"></span>
                                    </div>
                                    <div class="form-group c-margin-t-40">
                                        <button type="submit" class="btn btn-lg c-theme-btn c-btn-square c-btn-uppercase c-btn-bold">@lang('home.Reset Password') </button>
                                    </div>
                                    <div class="c-margin-fix" style="margin-top: 20px;">
                                        <div class="c-checkbox c-toggle-hide" data-object-selector="c-form-resetPassword" data-animation-speed="600">
                                            <input type="checkbox" id="checkbox6-449" class="c-check" hidden>
                                            <label for="checkbox6-449"> <span class="inc"></span> <span class="check"></span>
                                                <span class="box"></span>  @lang('home.or you have the verification code!') </label>
                                        </div>
                                    </div>
                                </form>
                                <form class="c-form-resetPassword c-margin-t-20" id="ResetPasswordForm">
                                    {{ csrf_field() }}
                                    <label> @lang('home.Reset Password')</label>
                                    <div class="form-group">
                                        <label class="control-label"> @lang('home.Your username')</label>
                                        <input type="text" id="resetPass_username" name="username" class="form-control c-square c-theme" placeholder="@lang('home.Username')">
                                        <span class="glyphicon glyphicon-user form-control-feedback c-font-grey"></span>
                                    </div>
                                    <div class="form-group">
                                        <label class="control-label"> @lang('home.Verification Code')</label>
                                        <input type="tel" name="verificationcode" class="form-control c-square c-theme" placeholder="@lang('home.Verification Code')">
                                    </div>
                                    <div class="form-group">
                                        <label class="control-label"> @lang('home.New Password')</label>
                                        <input type="password" name="newpassword" class="form-control c-square c-theme" placeholder="@lang('home.Password')">
                                    </div>
                                    <div class="form-group c-margin-t-40">
                                        <button type="submit" class="btn btn-lg c-theme-btn c-btn-square c-btn-uppercase c-btn-bold"> @lang('home.Reset Password')</button>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="panel panel-default c-panel">
                            <div class="panel-body c-panel-body">
                                <div class="c-content-title-1">
                                    <h3 class="c-left"><i class="icon-user"></i> @lang('home.Dont have an account yet?')</h3>
                                    <div class="c-line-left c-theme-bg"></div>
                                    <p> @lang('home.Join us and enjoy shopping online today.')</p>
                                </div>
                                <div class="c-margin-fix">
                                    <div class="c-checkbox c-toggle-hide" data-object-selector="c-form-register" data-animation-speed="600">
                                        <input type="checkbox" id="checkbox6-444" class="c-check">
                                        <label for="checkbox6-444"> <span class="inc"></span> <span class="check"></span>
                                            <span class="box"></span>  @lang('home.Register Now!') </label>
                                    </div>
                                </div>
                                <form class="c-form-register c-margin-t-20" id="registerForm">
                                    {{ csrf_field() }}
                                    <div class="form-group">
                                        <div class="row">
                                            <div class="col-md-12">
                                                <label class="control-label"> @lang('home.User Name')</label>
                                                <input type="text" name="username" id="username" class="form-control c-square c-theme" placeholder="@lang('home.User Name')">
                                            </div>
                                            <div class="col-md-6">
                                                <label class="control-label"> @lang('home.First Name')</label>
                                                <input type="text" name="firstName" class="form-control c-square c-theme" placeholder="@lang('home.First Name')">
                                            </div>
                                            <div class="col-md-6">
                                                <label class="control-label"> @lang('home.Last Name')</label>
                                                <input type="text" name="lastName" class="form-control c-square c-theme" placeholder="@lang('home.Last Name')">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="control-label"> @lang('home.Born Date')</label>
                                        <input type="date" name="bornDate" class="form-control c-square c-theme" placeholder="@lang('home.yyyy-mm-dd')">
                                    </div>
                                    <div class="row">
                                        <div class="form-group col-md-6">
                                            <label class="control-label"> @lang('home.Email Address')</label>
                                            <input type="email" name="email" class="form-control c-square c-theme" placeholder="@lang('home.Email Address')">
                                        </div>
                                        <div class="form-group col-md-6">
                                            <label class="control-label"> @lang('home.Phone')</label>
                                            <input type="tel" name="phone" class="form-control c-square c-theme" placeholder="@lang('home.Phone')">
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="control-label"> @lang('home.Account Password')</label>
                                        <input type="password" name="password" class="form-control c-square c-theme" placeholder="@lang('home.Password')">
                                    </div>
                                    <div class="form-group c-margin-t-40">
                                        <button type="submit" class="btn btn-lg c-theme-btn c-btn-square c-btn-uppercase c-btn-bold">@lang('home.Register')</button>
                                    </div>
                                    <div class="c-margin-fix" style="margin-top: 20px;">
                                        <div class="c-checkbox c-toggle-hide" data-object-selector="c-form-verifyAccount1" data-animation-speed="600">
                                            <input type="checkbox" id="checkbox6-446" class="c-check" hidden>
                                            <label for="checkbox6-446"> <span class="inc"></span> <span class="check"></span>
                                                <span class="box"></span>  @lang('home.or you have the verification code!') </label>
                                        </div>
                                    </div>
                                </form>
                                <div class="c-margin-fix" style="margin-top: 20px;">
                                    <div class="c-checkbox c-toggle-hide" data-object-selector="c-form-verifyAccount" data-animation-speed="600">
                                        <input type="checkbox" id="checkbox6-446" class="c-check" hidden>
                                    </div>
                                </div>

                                <form class="c-form-verifyAccount1 c-margin-t-20" id="verifyAccountForm">
                                    {{ csrf_field() }}
                                    <label> @lang('home.Verify your accountyou!') </label>
                                    <div class="form-group">
                                        <label class="control-label"> @lang('home.Your username')</label>
                                        <input type="text" id="verificationCode_username" name="username" class="form-control c-square c-theme" placeholder="@lang('home.Username')">
                                        <span class="glyphicon glyphicon-user form-control-feedback c-font-grey"></span>
                                    </div>
                                    <div class="form-group">
                                        <label class="control-label"> @lang('home.Verification Code')</label>
                                        <input type="tel" name="verificationcode" class="form-control c-square c-theme" placeholder="@lang('home.Verification Code')">
                                    </div>
                                    <div class="form-group c-margin-t-40">
                                        <button type="submit" class="btn btn-lg c-theme-btn c-btn-square c-btn-uppercase c-btn-bold">@lang('home.Verify')</button>
                                    </div>
                                </form>

                                <form class="c-form-verifyAccount c-margin-t-20" id="verifyAccountForm">
                                    {{ csrf_field() }}
                                    <label> @lang('home.Verify your accountyou!')</label>
                                    <div class="form-group">
                                        <label class="control-label"> @lang('home.Your username')</label>
                                        <input type="text" name="username" class="form-control c-square c-theme" placeholder="@lang('home.Username')">
                                        <span class="glyphicon glyphicon-user form-control-feedback c-font-grey"></span>
                                    </div>
                                    <div class="form-group">
                                        <label class="control-label"> @lang('home.Verification Code')</label>
                                        <input type="tel" name="verificationcode" class="form-control c-square c-theme" placeholder="@lang('home.Verification Code')">
                                    </div>
                                    <div class="form-group c-margin-t-40">
                                        <button type="submit" class="btn btn-lg c-theme-btn c-btn-square c-btn-uppercase c-btn-bold"> @lang('home.Verify')</button>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12">

                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- END: CONTENT/SHOPS/SHOP-LOGIN-REGISTER-1 -->

    <!-- END: PAGE CONTENT -->
    </div>
    <!-- END: PAGE CONTAINER -->

    <!-- BEGIN: LAYOUT/FOOTERS/FOOTER-6 -->

    <!-- BEGIN: LAYOUT/BASE/BOTTOM -->
    <!-- BEGIN: CORE PLUGINS -->
    <script>
        window.fbAsyncInit = function() {

            FB.init({
                appId: '468866187481149',
                cookie: true, // Enable cookies to allow the server to access the session.
                xfbml: true, // Parse social plugins on this webpage.
                version: 'v10.0' // Use this Graph API version for this call.
            });
            FB.AppEvents.logPageView();
        };
// Load the SDK Asynchronously
(function(d){
     var js, id = 'facebook-jssdk', ref = d.getElementsByTagName('script')[0];
     if (d.getElementById(id)) {return;}
     js = d.createElement('script'); js.id = id; js.async = true;
     js.src = "//connect.facebook.net/en_US/all.js";
     ref.parentNode.insertBefore(js, ref);
   }(document));
    </script>
    <script async defer crossorigin="anonymous" src="https://connect.facebook.net/en_US/sdk.js"></script>

    </body>

@endsection



@section('script')


    {{--<script>--}}



        $(document).ready(function (){
            $('#gbtn').click(function(){
                gbtn = true;
            });
               document.getElementById('face').addEventListener('click', function() {
        FB.login(function(response) {
            if (response.authResponse) {
                console.log('Welcome!  Fetching your information.... ');
                FB.api('/me?fields=id,email,first_name,last_name,name', function(response) {
                    console.log('Good to see you, ' + response.name + '.');
                    var formData = new FormData();
                    formData.append('email', response.email);
                    {{-- formData.append('ImageUrl', response.picture.data.url); --}}
                    formData.append('name', response.name);
                    formData.append('Fname', response.first_name);
                    formData.append('Lname', response.last_name);
                    $.ajax({
                        type: "POST",
                        url: '{{ route('signInWithFacebook') }}',
                        data: formData,

                        // mimeType: 'application/json',
                        processData: false,
                        contentType: false,
                        success: function(res) {

                            if (res.status == 201) {

                                localStorage.setItem("DamaStore_userObject",
                                    JSON.stringify(res.data));
                                localStorage.setItem("damastore_cart", "");

                                var formData = new FormData();
                                formData.append('user', res.data.token);

                                $.ajax({
                                    type: "POST",
                                    url: '{{ route('store') }}',
                                    data: formData,
                                    // mimeType: 'application/json',
                                    processData: false,
                                    contentType: false,
                                    success: function(res) {
                                        Swal.fire({
                                            icon: 'success',
                                            title: 'success',
                                            text: 'sign in successfuly, thanks for using our services',
                                        }).then(function() {
                                            // save user object to localstorage
                                            window
                                                .location =
                                                "/";
                                        });

                                    },
                                    error: function(res, response) {},
                                });


                            }
                            if (res.status == 401) {
                                // console.log(res.data.validator);
                                Swal.fire({
                                    icon: 'error',
                                    title: 'Email or password is wrong',
                                    text: res.data.validator,
                                });
                            }

                        },
                        error: function(res, response) {
                            $(".loading").remove();
                        },

                    });

                });

            } else {
                console.log('User cancelled login or did not fully authorize.');
            }
        });
    }, false);
            $('#verifyAccountForm').validate({
                rules: {
                    Username: {
                        required: true
                    },
                    verificationcode: {
                        required: true
                    },
                },
                messages: {
                    Username: {
                        required:  "* username is required"
                    },
                    verificationcode: {
                        required:  "* verification code is required"
                    },
                }
            });
            $.validator.addMethod('mypassword', function(value, element) {
                return this.optional(element) || (value.match(/(?=.*[A-Z])/) && value.match(/[0-9]/));
            },
            'The password must contain at least one uppercase letter and at least one number, and it must be at least 8 characters long');
            $('#registerForm').validate({
                rules: {
                    username: {
                        required: true
                    },
                    password: {
                        required: true,
                        minlength: 8,
                        mypassword: true
                    },
                    firstName: {
                        required: true
                    },
                    lastName: {
                        required: true
                    },
                    bornDate: {
                        required: true
                    },
                    email: {
                        required: true
                    },
                    phone: {
                        required: true
                    }
                },
                messages: {
                    username: {
                        required:  "* username is required"
                    },
                    password: {
                        required:  "*  password is required",
                        minlength:  "* Minimum of 8 characters ",
                        mypassword:  "*The password must contain at least one uppercase letter and at least one number, and it must be at least 8 characters long",

                    },
                    firstName: {
                        required: "* First name is required"
                    },
                    lastName: {
                        required:  "* Last name is required"
                    },
                    bornDate: {
                        required:  "* BornDate is required"
                    },
                    email: {
                        required:  "* Email is required"
                    },
                    phone: {
                        required: "* Phone number is required"
                    }
                }
            });



            $('#forgetPasswordForm').validate({
                rules: {
                    username: {
                        required: true
                    },
                },
                messages: {
                    username: {
                        required:  "* username is required"
                    },
                }
            });
            $('#signInForm').validate({
                rules: {
                    username: {
                        required: true
                    },
                    password: {
                        required: true
                    },
                },
                messages: {
                    username: {
                        required:  "* Username is required"
                    },
                    password: {
                        required:  "* Password is required"
                    },

                }

            });







            //function onSignIn(googleUser) {
            //    console.log('hey');
            //    var profile = googleUser.getBasicProfile();
            //    console.log(profile);
            //    console.log('ID: ' + profile.getId()); // Do not send to your backend! Use an ID token instead.
            //    console.log('Name: ' + profile.getName());
            //    console.log('Image URL: ' + profile.getImageUrl());
            //    console.log('Email: ' + profile.getEmail()); // This is null if the 'email' scope is not present.

            //    let name = profile.getName();
            //    let email = profile.getEmail();

            //    $('#em').val(email);

            //    $('#form_test').submit();
            //}

            function signOut() {
                var auth2 = gapi.auth2.getAuthInstance();
                 auth2.signOut().then(function () {
                       console.log('User signed out.');
                });
            }


            $('#form_test').submit(function (e) {
                console.log('submiiiit');
                var formData = new FormData($(this)[0]);
                e.preventDefault();
                e.stopPropagation();
                /*
                            let _token = $('meta[name="csrf-token"]').attr('content');

                            console.log('csrf-token : ' + _token);

                 */
                $('#How-It-Works').append('<div class="loading">Loading&#8230;</div>');
                $.ajax({
                    type: "POST",
                    url: '{{route('signInWithGoogle')}}',
                    data: formData,

                    // mimeType: 'application/json',
                    processData: false,
                    contentType: false,
                    success: function ( res ) {
                        console.log('ajax done');
                        window.location.href = '/';

                        /*
                        res = JSON.parse(res);
                        */

                        /*    if(res.code==200){
                                Swal.fire({
                                    icon: 'success',
                                    title: 'تم تسجيل الدخول بنجاح',
                                    text: 'مرحبا',
                                    confirmButtonText: "نعم",
                                }).then(function() {
                                    window.location.href = '/admin';
                                });
                            }
                            else if(res.code==100){
                                Swal.fire({
                                    icon: 'success',
                                    title: 'تم تسجيل الدخول بنجاح',
                                    text: 'مرحبا',
                                    confirmButtonText: "نعم",
                                }).then(function() {
                                    window.location.href = '/user';
                                });
                            }
                            else if(res.code==-1){
                                Swal.fire({
                                    icon: 'error',
                                    title: 'عذرا',
                                    text: 'إنتظر تأكيد المدير لحسابك',
                                    confirmButtonText: "نعم",
                                }).then(function() {
                                    window.location.href = '/';
                                });
                            }
                            else {
                                Swal.fire({
                                    icon: 'error',
                                    title: 'عذرا',
                                    text: 'إسم مستخدم او كلمة سر خاطئة',
                                    confirmButtonText: "نعم",
                                }).then(function() {
                                    window.location.href = '/signin';
                                });
                            }*/


                    },
                    error: function ( res , response ) {
                        $( ".loading" ).remove();
                    },

                });



            });

            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
        });


        $('#signInForm').submit(function (e) {

            var formData = new FormData($(this)[0]);
            e.preventDefault();
            e.stopPropagation();
            if($('#signInForm').valid()){
                $('#How-It-Works').append('<div class="loading">Loading&#8230;</div>');
                $.ajax({


                    type: "POST",
                    url: '{{route('login')}}',
                    data: formData,
                    // mimeType: 'application/json',
                    processData: false,
                    contentType: false,
                    success: function (res) {

                        if(res.status == 201) {

                            localStorage.setItem("DamaStore_userObject", JSON.stringify(res.data));
                            //localStorage.setItem("damastore_cart", "");

                            var formData = new FormData();
                            formData.append('user', res.data.token);

                            $.ajax({
                                type: "POST",
                                url: '{{route('store')}}',
                                data: formData,
                                // mimeType: 'application/json',
                                processData: false,
                                contentType: false,
                                success: function (res) {
                                    Swal.fire({
                                        icon: 'success',
                                        title: 'success',
                                        text: 'sign in successfuly, thanks for using our services',
                                    }).then(function() {
                                        // save user object to localstorage
                                        window.location = "/";
                                    });

                                },
                                error: function (res, response) {
                                },
                            });

                            // Swal.fire({
                            //     icon: 'success',
                            //     title: 'success',
                            //     text: 'sign in successfuly, thanks for using our services',
                            // }).then(function() {
                            //     window.location = "/";
                            //     // save user object to localstorage
                            //     localStorage.setItem("DamaStore_userObject", JSON.stringify(res.data));
                            //     localStorage.setItem("damastore_cart", "");
                            // });

                        }
                        if(res.status == 500) {
                            // console.log(res.data.validator);
                            Swal.fire({
                                icon: 'error',
                                title: 'someting went wrong',
                                text: res.msg,
                            });
                            $( ".loading" ).remove();
                        }
                        if(res.status == 401) {
                            // console.log(res.data.validator);
                            Swal.fire({
                                icon: 'error',
                                title: 'Email or password is wrong',
                                text: res.msg,
                            });
                            $( ".loading" ).remove();
                        }
                        if(res.status == 402) {
                            // console.log(res.data.validator);
                            Swal.fire({
                                icon: 'error',
                                title: 'sorry',
                                text: res.msg,
                            });
                            $( ".loading" ).remove();
                        }
                        if(res.status == 403) {
                            // console.log(res.data.validator);
                            Swal.fire({
                                icon: 'error',
                                title: 'sorry',
                                text: res.msg,
                            });
                            $( ".loading" ).remove();
                        }

                    },
                    error: function (res, response) {
                        $( ".loading" ).remove();
                    },
                });
            }
        });

        $('#verifyAccountForm').submit(function (e) {

            var formData = new FormData($(this)[0]);
            e.preventDefault();
            e.stopPropagation();

            if($('#verifyAccountForm').valid()) {
                $('#How-It-Works').append('<div class="loading">Loading&#8230;</div>');
                $.ajax({


                    type: "POST",
                    url: '{{route('verifyaccount')}}',
                    data: formData,
                    // mimeType: 'application/json',
                    processData: false,
                    contentType: false,
                    success: function (res) {

                        if (res.status == 200) {
                            Swal.fire({
                                icon: 'success',
                                title: 'success',
                                text: 'you can sign in now',
                            }).then(function () {
                                $('#checkbox6-446').click();
                                $(".loading").remove();

                            });
                        }
                        if (res.status == 500) {
                            Swal.fire({
                                icon: 'error',
                                title: 'Oops...',
                                text: res.msg,
                            });
                        }
                        $(".loading").remove();
                    },
                    error: function (res, response) {
                        $(".loading").remove();
                    },
                });
            }
        });

        $('#forgetPasswordForm').submit(function (e) {

            var formData = new FormData($(this)[0]);
            e.preventDefault();
            e.stopPropagation();

            if($('#forgetPasswordForm').valid()) {
                $('#How-It-Works').append('<div class="loading">Loading&#8230;</div>');
                $.ajax({


                    type: "POST",
                    url: '{{route('sendVerificationCodeResetingPasssword')}}',
                    data: formData,
                    // mimeType: 'application/json',
                    processData: false,
                    contentType: false,
                    success: function (res) {

                        if (res.status == 200) {
                            $('#resetPass_username').val(formData.get('username'));
                            $('#checkbox6-445').click();
                            $('#checkbox6-449').click();
                            $(".loading").remove();
                        }
                        if (res.status == 500) {
                            Swal.fire({
                                icon: 'error',
                                title: 'Oops, something went wrong...',
                                text: res.msg,
                            });
                        }
                        $(".loading").remove();
                    },
                    error: function (res, response) {
                        $(".loading").remove();
                    },
                });
            }
        });

        $('#ResetPasswordForm').submit(function (e) {

            var formData = new FormData($(this)[0]);
            e.preventDefault();
            e.stopPropagation();

            if($('#ResetPasswordForm').valid()) {
                $('#How-It-Works').append('<div class="loading">Loading&#8230;</div>');
                $.ajax({


                    type: "POST",
                    url: '{{route('verifyRestingPassword')}}',
                    data: formData,
                    // mimeType: 'application/json',
                    processData: false,
                    contentType: false,
                    success: function (res) {

                        if (res.status == 200) {
                            Swal.fire({
                                icon: 'success',
                                title: 'success',
                                text: 'you can sign in now',
                            }).then(function () {
                                $('#checkbox6-449').click();
                                $(".loading").remove();
                            });
                            $(".loading").remove();
                        }
                        if (res.status == 500) {
                            Swal.fire({
                                icon: 'error',
                                title: 'Oops, something went wrong...',
                                text: res.msg,
                            });
                        }
                        $(".loading").remove();
                    },
                    error: function (res, response) {
                        $(".loading").remove();
                    },
                });
            }
        });



        $('#registerForm').submit(function (e) {

            var formData = new FormData($(this)[0]);
            e.preventDefault();
            e.stopPropagation();

            if($('#registerForm').valid()) {
                $('#How-It-Works').append('<div class="loading">Loading&#8230;</div>');
                $.ajax({

                    type: "POST",
                    url: '{{route('register')}}',
                    data: formData,
                    // mimeType: 'application/json',
                    processData: false,
                    contentType: false,
                    success: function (res) {
                        if (res.status == 201) {
                            Swal.fire({
                                icon: 'success',
                                title: 'success',
                                text: 'check your mail to verify your account',
                            }).then(function () {
                                // window.location = "/";
                                $('#verificationCode_username').val(formData.get('username'));
                                $('#checkbox6-444').click();
                                $('#checkbox6-446').click();
                            });

                        }
                        if (res.status == 400) {
                            // console.log(res.data.validator);
                            Swal.fire({
                                icon: 'error',
                                title: 'Oops...',
                                text: res.data.validator,
                            });
                        }
                        $(".loading").remove();
                    },
                    error: function (res, response) {
                        $(".loading").remove();
                    },
                });
            }

        });


@endsection



<script>
var getUrlParameter = function getUrlParameter(sParam) {
    var sPageURL = window.location.search.substring(1),
        sURLVariables = sPageURL.split('&'),
        sParameterName,
        i;

    for (i = 0; i < sURLVariables.length; i++) {
        sParameterName = sURLVariables[i].split('=');

        if (sParameterName[0] === sParam) {
            return typeof sParameterName[1] === undefined ? true : decodeURIComponent(sParameterName[1]);
        }
    }
    return false;
};


 (function() {
    var po = document.createElement('script');
    po.type = 'text/javascript'; po.async = true;
    po.src = 'https://apis.google.com/js/client:plusone.js?onload=render';
    var s = document.getElementsByTagName('script')[0];
    s.parentNode.insertBefore(po, s);
  })();

//   if(getUrlParameter('google')==="false"){
//     window.location = "/";
//   }

  function onSuccess(googleUser) {
            // Respond to signin, see https://developers.google.com/+/web/signin/
            var profile = googleUser.getBasicProfile();
            $('#em').val(profile.getEmail());
            $('#form_test #name').val(profile.getName());
            // $('#form_test').submit();
            var formData = new FormData();
            formData.append('name',profile.getName());
            formData.append('Fname',profile.getName());
            formData.append('Lname',profile.getName());
            formData.append('username',profile.getName());
            formData.append('email',profile.getEmail());
            $.ajax({
                    type: "POST",
                    url: '{{route('signInWithGoogle')}}',
                    data: formData,
                    // mimeType: 'application/json',
                    processData: false,
                    contentType: false,
                    success: function ( res ) {
                        if(getUrlParameter('google')==="false"){
                        signOut();
                        }else{
                        // console.log(res);
                        localStorage.setItem("DamaStore_userObject", JSON.stringify(res.data));
                        var formData = new FormData();
                            formData.append('user', res.data.token);

                            $.ajax({
                                type: "POST",
                                url: '{{route('store')}}',
                                data: formData,
                                // mimeType: 'application/json',
                                processData: false,
                                contentType: false,
                                success: function (res) {
                                    Swal.fire({
                                        icon: 'success',
                                        title: 'success',
                                        text: 'sign in successfuly, thanks for using our services',
                                    }).then(function() {
                                        // save user object to localstorage
                                        window.location = "/";
                                    });

                                },
                                error: function (res, response) {
                                },
                            });
                        }
                    //    window.location.href = '/';
                    },
                    error: function ( res , response ) {
                        $( ".loading" ).remove();
                    },

                });
  console.log('ID: ' + profile.getId()); // Do not send to your backend! Use an ID token instead.
  console.log('Name: ' + profile.getName());
  console.log('Image URL: ' + profile.getImageUrl());
  console.log('Email: ' + profile.getEmail()); // This is null if the 'email' scope is not present.
          }
          function onFailure(error) {
      console.log(error);
    }
    function render() {
            gapi.signin2.render('customBtn', {
              'callback': 'signinCallback',
              'clientid': '728080330284-30rvpujtk8qfef37pggpcd1iv3il4k34.apps.googleusercontent.com',
              'cookiepolicy': 'single_host_origin',
              'scope': 'profile email',
              'onsuccess': onSuccess,
              'onfailure': onFailure,
              'longtitle': true,
            });
          }
          function signOut() {
    var auth2 = gapi.auth2.getAuthInstance();
    auth2.signOut().then(function () {
      console.log('User signed out.');
      window.location = "/";
    });
  }

</script>

