@extends('Welcome.views.home1')
@section('style')

	<style>

	</style>

@endsection

@section('home_content')

    <?php
    $locale = App::getLocale();
    ?>

<script>
    // @if(session("currency")!=="USD")
    // function change_currency() {
    //                                 let currency = document.getElementById('currencyList').value;
    //                                 window.location = "/changeCurrency/USD";
    //                             }
    //                             change_currency();
    //                             @endif

</script>

		<!-- BEGIN: PAGE CONTENT -->
		<div class="c-content-box c-size-lg">
	<div class="container">
		<form class="c-shop-form-1">
			<div class="row">

				<!-- BEGIN: ORDER FORM -->
				<div class="col-md-11">
					<div class="c-content-bar-1 c-align-left c-bordered c-theme-border c-shadow">
                        <div style="width: 100%;text-align: start">
                            <a href="/cart" class="btn btn-primary">@lang('home.edit')</a>
                            <a href="#" id="orderCancel" class="btn btn-primary">@lang('home.cancel')</a>
                        </div>
					<h1 class="c-font-bold c-font-uppercase c-font-24"> @lang('home.Your Order')</h1>
                        <ul class="c-order list-unstyled">
                            <li class="row c-margin-b-15">
							<div class="col-md-6 c-font-20"><h2> @lang('home.Product')</h2></div>
							<div class="col-md-6 c-font-20"><h2> @lang('home.Total')</h2></div>
                            </li>
                            <li class="row c-border-bottom"></li>
                            <div id="products_order">
                            </div>
                            <hr>
                            <li id="div_coupon" class="row c-margin-b-15 c-margin-t-15">
                                <div class="col-md-6 c-font-20">
								<p class="c-font-30"> @lang('home.coupon')</p>
                                </div>
                                <div class="col-md-6 c-font-20">
                                    <p class="c-font-bold c-font-30"><span class="c-shipping-total" id="coupon"></span>
                                        <span style="font-size:14px;">{{session('currency')}}</span>
                                    </p>

                                </div>
                            </li>
                            <li class="row c-margin-b-15 c-margin-t-15">
                                <div class="col-md-6 c-font-20">
								<p class="c-font-30"> @lang('home.Tax')</p>
                                </div>
                                <div class="col-md-6 c-font-20">
                                    <p class="c-font-bold c-font-30"><span class="c-shipping-total" id="Tax"></span>
                                        <span style="font-size:14px;">{{session('currency')}}</span>
                                    </p>
                                </div>
                            </li>
                            <li class="row c-margin-b-15 c-margin-t-15">
                                <div class="col-md-6 c-font-20">
								<p class="c-font-30"> @lang('home.sum_shipping')</p>
                                </div>
                                <div class="col-md-6 c-font-20">
                                    <p class="c-font-bold c-font-30"><span class="c-shipping-total" id="sum_shipping"></span>
                                        <span style="font-size:14px;">{{session('currency')}}</span>
                                    </p>
                                </div>
                            </li>
                            <li class="row c-margin-b-15 c-margin-t-15">
                                <div class="col-md-6 c-font-20">
								<p class="c-font-30"> @lang('home.Total')</p>
                                </div>
                                <div class="col-md-6 c-font-20">
                                    <p class="c-font-bold c-font-30"><span class="c-shipping-total" id="totalCost"></span>
                                        <span style="font-size:14px;">{{session('currency')}}</span>
                                    </p>
                                </div>
                            </li>
                            <li class="row c-border-top c-margin-b-15"></li>
                            <li class="row">
                            </li>
                        </ul>
                    </div>
                    <div  class="c-content-bar-1 c-align-left c-bordered c-theme-border c-shadow row" style="text-align: center">
                        <a class="btn" style="background-color: yellow;color:rgb(112, 112, 112);padding:10px 50px;border-color:rgb(112, 112, 112)"
                        id="goToNoon"
                        >Pay</a>
                        <div style="width: 100%;font-size: 25px;text-align: center;padding: 15px;color:red;" id="pleasestayinpage"></div>
                        <script src="https://cdn.checkout.com/sandbox/js/checkout.js"></script>
                         <style>
                             .c-layout-header{
                                 z-index: 999 !important;
                             }
                         </style>
                    </div>
                    <div style="display: none;" class="c-content-bar-1 c-align-left c-bordered c-theme-border c-shadow row">

                        <script
                                src="https://www.paypal.com/sdk/js?client-id=AXilx4abHZbu9dcHCzGIM6k4xdRBDJVJw7e5CpdLb1VwoF60XJyqjZeCRT9lsAiuY5D1E0po5PBIq46h"> // Required. Replace SB_CLIENT_ID with your sandbox client ID.
                        </script>
                        <div class="col-md-3"></div>
                        <div class="col-md-6">
                            <div id="paypal-button-container"></div>
                        </div>
                    </div>
				</div>
				<!-- END: ORDER FORM -->
			</div>
		</form>
	</div>
</div>
<input type="hidden" id="coupon_id">
		<!-- END: PAGE CONTENT -->


@endsection



@section('script')

	{{--<script>--}}
        var order_id = localStorage.getItem('damastore_Order');

    // if you want to cancel the payment order
    $('#orderCancel').click(function(){
    var formData = new FormData();
    formData.append('id', order_id);
    $.ajax({
    type: "POST",
    url: '{{route('OrderCancel')}}',
    data: formData,
    // mimeType: 'application/json',
    processData: false,
    contentType: false,
    headers: {"Authorization": 'Bearer ' + userObject.token},
    success: function (res) {
       if(res.data=="canceled"){
    // save user object to localstorage
        emptyCart();
        window.location = "/cart";
         }
    },
    error: function (res){

    }
      });
    });
        var userObject = JSON.parse(localStorage.getItem("DamaStore_userObject"));

        // blocked
        // bornDate
        // created_at
        // email
        // emailVerifiedAt
        // files
        // firstName
        // id
        // lastActivity
        // lastLoggedIn
        // lastName
        // phone
        // reputation
        // roles
        // token
        // updated_at
        // username
        // verify


        function getData(arr) {

            {{-- console.log(arr); --}}

            id_arr = [];
            qty_arr = [];
            qty_price_arr = [];
            var totalPrice = 0;
            let sum_shipping = 0;
            let shipping_costs= 0;
            arr.forEach(function (item, index, arr) {
                id_arr.push(item[0]);
                qty_arr.push(item[1]);
            });

            var formData = new FormData();
            formData.append('arr', id_arr);


            $.ajax({

                type: "POST",
                url: '/api/ProductCartList',
                data: formData,
                // mimeType: 'application/json',
                processData: false,
                contentType: false,
                headers: {"Authorization": 'Bearer ' + userObject.token},
                success: function (res) {

                    $('#products_order').empty();


                    res.data.data.forEach(function (item, index, arr) {

                        qty_price_arr.push([qty_arr[index], item.price]);

                        var image;
                        if (item.image) {
                            image ='/images/' + item.image.path;
                        } else {
                            image = 'https://static.thenounproject.com/png/2999524-200.png';
                        }

                        var date = new Date(item.created_at),
                            yr = date.getFullYear(),
                            month = date.getMonth() < 10 ? '0' + date.getMonth() : date.getMonth(),
                            day = date.getDate() < 10 ? '0' + date.getDate() : date.getDate(),
                            newDate = yr + '-' + month + '-' + day;

                        var price = '';
                        var sale_grid = '';

                            price = '<p class="c-price c-font-14 c-font-slim">' + item.price + ' <span style="font-size:14px;"> {{session("currency")}} </span></p>';


                        var gprice = 0;

                            gprice = item.price;
                            shipping_costs = parseInt(item.shipping_costs);
                            {{-- console.log(gprice , shipping_costs,qty_arr[index]); --}}



                        $('#products_order').append('<li class="row c-margin-b-15 c-margin-t-15"> ' +
						'		<div class="col-md-6 c-font-20"> ' +
                            @if(session('lang') == 'ar')
						'			<a href="/productDetails/' + item.id + '" class="navigation c-theme-link">' + item.ar_name + ' x ' + qty_arr[index] + '</a></div> ' +
				        	@else
                        '			<a href="/productDetails/' + item.id + '" class="navigation c-theme-link">' + item.name + ' x ' + qty_arr[index] + '</a></div> ' +
                            @endif

                        '		<div class="col-md-6 c-font-20"> ' +
						'			<p >' + (gprice * qty_arr[index]) + ' <span style="font-size:14px;"> {{session("currency")}} </span> </p> ' +
						'		</div> ' +
						'	</li>');
                        sum_shipping += (shipping_costs * qty_arr[index]);
                        totalPrice += (gprice * qty_arr[index]);

                    });
                    {{-- console.log(sum_shipping); --}}
                    let couponValue=0;
                    if(res.data.coupon){
                       couponValue=Math.ceil((totalPrice * res.data.coupon.value ) / 100);
                        $('#coupon_id').val(res.data.coupon.id);
                        $('#coupon').text(couponValue);
                    }else{
                        $('#coupon').hide();
                        $('#div_coupon').hide();
                        couponValue=0;
                    }
                    var tax = (totalPrice * 5 ) / 100;
                    totalPrice = (totalPrice + tax + sum_shipping)-couponValue;
                    $('#totalCost').text(Math.ceil(totalPrice));
                    $('#sum_shipping').text(Math.ceil(sum_shipping));
                    $('#Tax').text(Math.ceil(tax));

                },
                error: function (res, response) {
                },
            });

        }

	var cart_arr = localStorage.getItem('damastore_cart');
	var arr = JSON.parse(cart_arr);
	getData(arr);










        var Gdata;
        var Gdetails;

        function purchaseform() {

            $.ajax({
                type: "POST",
                url: '{{route('submitPay')}}',
                data: null,
                processData: false,
                contentType: false,
                success: function ( res ) {

                    // if you want to save the payment order
                    FinishSubmit();
                },
                error: function ( res , response ) {
                    $( ".loading" ).remove();
                },
            });
        };

        // if you want to save the payment order
        function FinishSubmit() {
    $('#pleasestayinpage').text('Please Stay In this Page To complete Your Order...');
            var formData = new FormData();
            formData.append("orderID" , Gdata.orderID);
            formData.append("payerID" , Gdata.payerID);
            formData.append("paymentID" , Gdata.paymentID);
            formData.append("create_time" , Gdetails.create_time);
            formData.append("country_code" , Gdetails.payer.address.country_code);
            formData.append("email_address" , Gdetails.payer.email_address);
            formData.append("UserFullName" , Gdetails.payer.name.given_name + ' ' + Gdetails.payer.name.surname);
            formData.append("payer_id" , Gdetails.payer.payer_id);
            formData.append("amountValue" , Gdetails.purchase_units[0].amount.value);
            formData.append("amountCurrency_code" , Gdetails.purchase_units[0].amount.currency_code);
            formData.append("payeeEmail_address" , Gdetails.purchase_units[0].payee.email_address);
            formData.append("payeeMerchant_id" , Gdetails.purchase_units[0].payee.merchant_id);
            formData.append("status" , Gdetails.status);
            formData.append("captures_id" , Gdetails.purchase_units[0].payments.captures[0].id);
            formData.append("reference_id" , Gdetails.purchase_units[0].reference_id);
            formData.append("products" , arr);
            formData.append("coupon_id" , $('#coupon_id').val());
            formData.append("user" , userObject.token);

            $.ajax({
                type: "POST",
                url: '{{route('submitedPaing')}}',
                data: formData,
                processData: false,
                contentType: false,
                success: function ( res ) {
                    Swal.fire({
                        icon: 'success',
                        title: 'success',
                        text: 'your payment has been submitted successfuly',
                    }).then(function() {
                        // save user object to localstorage
                        emptyCart();
                        window.location = "/";
                    });
                },
                error: function ( res , response ) {
                    $( ".loading" ).remove();
                },
            });
        };


        function FailedOrder() {
            var formData = new FormData();
            formData.append("orderID" , order_id);

            $.ajax({
                type: "POST",
                url: '{{route('failedOrder')}}',
                data: formData,
                processData: false,
                contentType: false,
                success: function ( res ) {
                    Swal.fire({
                        icon: 'danger',
                        title: 'success',
                        text: 'your order is failed',
                    }).then(function() {
                        // save user object to localstorage
                        //emptyCart();
                        window.location = "/cart";
                    });
                },
                error: function ( res , response ) {
                    $( ".loading" ).remove();
                },
            });
        };
        var noon_amount;
        var noon_items;
        // to get the price from api and initialize the paypal button
        function getPriceform() {
            var formData = new FormData();
            formData.append("products" , arr);

            $.ajax({
                type: "POST",
                url: '{{route('getPrice')}}',
                headers: {"Authorization": 'Bearer ' + userObject.token},
                data: formData,
                processData: false,
                contentType: false,
                success: function ( res ) {
                    // console.log(res.data[1]);
                    // console.log(res.data[0]);
                     noon_amount = res.data[1];
                     noon_items = res.data[0];
                    //var price = res;
                    paypal.Buttons({
                        createOrder: function(data, actions) {
                            return actions.order.create({
                                purchase_units: [{
                                    reference_id: order_id,
                                    amount: res.data[1],
                                    items: res.data[0],
                                    application_context: {
                                        shipping_preference: 'NO_SHIPPING'
                                    },
                                }]
                            })
                        },
                        onApprove: function(data, actions) {
                            return actions.order.capture().then(function(details) {
                                {{-- console.log('Gdata', data);
                                console.log('Gdetails', details); --}}
                                Gdata = data;
                                Gdetails = details;

                                if(details.purchase_units[0].amount.currency_code == details.purchase_units[0].payments.captures[0].amount.currency_code){
                                    if(details.purchase_units[0].amount.value == details.purchase_units[0].payments.captures[0].amount.value){

                                        // $('#How-It-Works').append('<div class="loading">Loading&#8230;</div>');

                                        // to save that you pay in session
                                        //purchaseform();
                                        FinishSubmit();
                                    }
                                }else{
                                    FailedOrder();
                                }
                            });
                        }
                    }).render('#paypal-button-container');
                },
                error: function ( res , response ) {
                },
            });
        };

        // get the price from api and initialize the paypal button
        getPriceform();
        $('#goToNoon').click(function(){
            $('#How-It-Works').append('<div class="loading">Loading&#8230;</div>');
            $.ajax({
                type: "POST",
                url: '{{route('getinit')}}',
                data: {
                    "apiOperation": "INITIATE",
                    "order": {
                        "name": "Damastore",
                        "channel": "web",
                        "reference": order_id,
                        "amount": noon_amount.value,
                        "currency": "SAR",
                        "category": "pay"
                    },
                    "configuration": {
                        "locale": "{{$locale}}",
                        "paymentAction": "Authorize",
                        "returnUrl": "https://damastore.sa/order"
                    }
                },
                success: function ( data ) {
                    var res =JSON.parse(data)
                    if(res.message==="Processed successfully"){
                        $( ".loading" ).remove();
                        $.ajax({
                            dataType:"jsonp",
                            jsonpCallback:'',
                            url: res.result.checkoutData.jsUrl,
                            complete: function(){
                                var settings =
                                {
                                  Checkout_Method: 1,
                                  SecureVerification_Method: 0,
                                  Call_Back: ResponseCallBack,
                                  Frame_Id: "The_Frame_Id"
                                };
                                ProcessCheckoutPayment(settings);
                                function ResponseCallBack (back){
                                    $('#pleasestayinpage').text('Please Stay In this Page To complete Your Order...');
                                    $.ajax({
                                        type: "POST",
                                        url: '{{route('submitedSale')}}',
                                        data: {
                                                "apiOperation": "SALE",
                                                "order": {
                                                    "id": back.orderId
                                                }
                                        },
                                        success: function ( data ) {
                                            var res =JSON.parse(data)
                                            if(res.message==="Processed successfully"){
                                                  {{-- console.log(res); --}}
                                                  var formData = new FormData();
                                                  formData.append("orderID" , res.result.order.id);
                                                  formData.append("payerID" , res.result.order.reference);
                                                  formData.append("paymentID" , res.result.transaction.id);
                                                  formData.append("create_time" , res.result.order.creationTime);
                                                  formData.append("country_code" , res.result.paymentDetails.cardCountry);
                                                  formData.append("email_address" , res.result.paymentDetails.brand);
                                                  formData.append("UserFullName" , res.result.paymentDetails.cardCountryName);
                                                  formData.append("payer_id" , res.result.transaction.stan);
                                                  formData.append("amountValue" , res.result.order.amount);
                                                  formData.append("amountCurrency_code" , res.result.order.currency);
                                                  formData.append("payeeEmail_address" , 'null');
                                                  formData.append("payeeMerchant_id" , 'null');
                                                  formData.append("status" , res.result.order.status);
                                                  formData.append("captures_id" ,"null");
                                                  formData.append("reference_id" , res.result.order.reference);
                                                  formData.append("products" , arr);
                                                  formData.append("coupon_id" , $('#coupon_id').val());
                                                  formData.append("user" , userObject.token);

                                                  $.ajax({
                                                      type: "POST",
                                                      url: '{{route('submitedPaing')}}',
                                                      data: formData,
                                                      processData: false,
                                                      contentType: false,
                                                      success: function ( res ) {
                                                          Swal.fire({
                                                              icon: 'success',
                                                              title: 'success',
                                                              text: 'your payment has been submitted successfuly',
                                                          }).then(function() {
                                                              // save user object to localstorage
                                                              emptyCart();
                                                              window.location = "/";
                                                          });
                                                      },
                                                      error: function ( res , response ) {
                                                          $( ".loading" ).remove();
                                                      },
                                                  });
                                            }else{
                                                Swal.fire({
                                                    icon: 'error',
                                                    title: 'Error',
                                                    text: 'Please check your card information and card balance : '+res.message,
                                                }).then(function() {

                                                });
                                            }
                                        },
                                        error: function ( res , response ) {
                                            $( ".loading" ).remove();
                                        },
                                    });
                                    {{-- console.log(back) --}}
                                }
                            },
                            success: function ( res ) {
                            },
                            error: function ( res , response ) {
                            },
                        });
                    }
                },
                error: function ( res , response ) {
                    $( ".loading" ).remove();
                },
            });
        });


@endsection

@section('js_sc')
<script>

</script>
@endsection
