<!-- BEGIN: LAYOUT/BREADCRUMBS/BREADCRUMBS-2 -->
<div class="c-layout-breadcrumbs-1 c-subtitle c-fonts-uppercase c-fonts-bold c-bordered c-bordered-both">
    <div class="container">
        <div class="c-page-title c-pull-left">
            {{--<h3 class="c-font-uppercase c-font-sbold">@lang('home.Product Grid')</h3>--}}
            <h3 class="c-font-uppercase c-font-sbold">@yield('home_breadcrumb_title')</h3>
            <h4 class=""></h4>
        </div>
        <ul class="c-page-breadcrumbs c-theme-nav c-pull-right c-fonts-regular">
            @yield('home_breadcrumb_path')
            {{--<li><a class="navigation" href="shop-product-grid.html">@lang('home.Product Grid')</a></li>--}}
            {{--<li>/</li>--}}
            {{--<li class="c-state_active"></li>--}}

        </ul>
    </div>
</div>
<!-- END: LAYOUT/BREADCRUMBS/BREADCRUMBS-2 -->