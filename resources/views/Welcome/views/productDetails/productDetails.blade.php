@extends('Welcome.views.home1')
@section('meta')
<meta property="og:ID" content="{{$products->id}}">
<meta property="og:title" content="{{$products->name}}">
<meta property="og:description" content="{{ htmlspecialchars ($products->description) }}">
<meta property="og:url" content="https://damastore.net/productDetails/{{$products->id}}">
@foreach($product_img as $product)
<meta property="og:image" content="https://damastore.net/images/{{$product->path}}">
@endforeach
<meta property="product:brand" content="DamaStore">
<meta property="product:availability" content="in stock">
<meta property="product:condition" content="new">
<meta property="product:price:amount" content="{{$products->price}}">
<meta property="product:price:currency" content="USD">
<meta property="product:retailer_item_id" content="{{$products->id}}">
<meta property="product:item_group_id" content="{{$products->category->id}}">
@endsection
@section('style')

	<style>

    }
		.c-shop-product-details-2 .c-product-gallery > .c-product-gallery-content {
			height: 472px  !important;
		}
        .magnify{
            border-radius: 50%;
            border: 2px solid rgb(255, 255, 255);
            position: absolute;
            z-index: 20;
            background-repeat: no-repeat;
            background-color: white;
            box-shadow: inset 0 0 20px rgba(0,0,0,.5);
            display: none;
            cursor: none;
        }
        .c-shop-product-details-2 .c-product-gallery > .c-product-gallery-content {
            height: 508px !important;
        }
        .c-shop-product-details-2 .c-product-gallery > .c-product-gallery-content img {
    /* width: auto !important; */
}
@media only screen and (max-width: 400px){
        .c-shop-product-details-2 .c-product-gallery > .c-product-gallery-content img {
    width: 100% !important;
    height: auto !important;
}
}
	</style>

@endsection

@section('home_breadcrumb_title')
    @lang('main.productDetails')
@endsection

@section('home_content')

    <?php
    $locale = App::getLocale();
    ?>

		<!-- BEGIN: PAGE CONTENT -->
		<!-- BEGIN: CONTENT/SHOPS/SHOP-PRODUCT-DETAILS-2 -->
<div class="c-content-box c-size-lg c-bg-white">
	<div class="container">
		<div class="c-shop-product-details-2">
			<div class="row">
				<div class="col-md-6">
					<div class="c-product-gallery">
						<div class="c-product-gallery-content">
							<div class="">
                                <button class="button_glass subhe_gal"  >@lang('main.zoomEnable')</button>
                                <div class="img-magnifier-container subhe_gallery"  >
                                    @foreach($product_img as $product)
                                    <img class="magnifiedImg" id="img_zoom"  style="height:400px;object-fit: cover" src="/images/{{$product->path}}" alt="" />
                                    @endforeach
                                </div>
                                <p></p>
                                @if($product_gallery)
                                   <div style="width:100%;overflow:auto">
                                       @foreach($product_img as $product)
                                           <div class="small_image" style="display:inline-block;width:70px;height:70px;object-fit: cover" >
                                               <img onclick="replaceImage_src('images/{{$product->path}}');" src="/images/{{$product->path}}" alt=" " style="width:70px;height:70px;object-fit: cover">
                                           </div>
                                       @endforeach
                                            @foreach($product_gallery as $imagesGallery)
                                                <div class="small_image" style="display:inline-block;width:70px;height:70px;object-fit: cover" href="/{{$imagesGallery->path}}" data-standard="/{{$imagesGallery->path}}">
                                                    <img onclick="replaceImage_src('{{$imagesGallery->path}}');" src="/{{$imagesGallery->path}}" alt=" " style="width:70px;height:70px;object-fit: cover">
                                                </div>
                                            @endforeach
                                   </div>
                                @endif
                                <script>
                                    function replaceImage_src(src){
                                        $('#img_zoom').fadeOut(
                                            'normal', function(){
                                                document.getElementById('img_zoom').src = "/"+src;
                                          $('#img_zoom').fadeIn();
                                         });
                                    }
                                </script>
                                </div>
							</div>
						</div>

					</div>
				<div class="col-md-6">
					<div class="c-product-meta">
					<table  style="width: 100%;">
					<tr>
					<td style="border-bottom:3px solid #e0dbd3;">
					<h1 id="name" style="font-size: calc(3vw + 2vmin); text-transform: uppercase;"></h1></td>
					<td style="text-align:center;font-size: calc(1vw + 1vmin);border-bottom:3px solid orange;">
					<div style="width: auto;" class="price_class">
					{{-- {{session('currency')}}  --}}
					<span id="price"></span>
                    </div>
					</td>
					<tr>
					<td>

					</td>
					<td style="text-align:center">
					{{-- <div class="offer_text" style="background-color:rgba(238, 23, 41, 0.562);">@lang('home.Sale')</div>
					<div class="offer_text" style="background-color:orange">@lang('home.New')</div> --}}
					</td>
					</tr>

					</table>
					<br>
                    <table class="table_cate_type">
                        <tr>
                            <td><span style="color:orange">@lang('main.Category'):</span></td>
                            <td><span id="category"></span></td>
                            <td><span style="color:orange">@lang('main.Type'):</span></td>
                            <td><span id="type"></span></td>
                        </tr>
                    </table>
                    <br>

						<div class="c-product-review">
                            <div id="attributes" class="attributes">
                                <div class="title_attr">@lang('main.attributes')</div>
                                </div>
						</div>
						<div style=" display: flex;
                        justify-content: center;
                        align-items: center;">
						<div id="addtocartqty" style="display: inline-block;margin:5px auto;">
                            <div class="c-input-group c-spinner">
                    <p class="c-product-meta-label c-product-margin-2 c-font-uppercase c-font-bold">@lang('main.Quantity'):</p>
                    <input type="text" class="form-control c-item-1" id="inputqty" value="1">
                                <div class="c-input-group-btn-vertical">
                    <button class="btn btn-default" type="button" data_input="c-item-1"><i class="fa fa-caret-up"></i></button>
                    <button class="btn btn-default" type="button" data_input="c-item-1"><i class="fa fa-caret-down"></i></button>
                               </div>
                          </div>
                    </div>
                                <div style="display: inline-block" >
                                    <button class="price_class c-font-uppercase" id="addtocart">@lang('shop.Add Cart') <i class="fa fa-shopping-cart"></i></button>
                                    <button class="price_class c-font-uppercase" id="removetocart" style="background-color: #e7505a !important; border-color: #e7505a !important;display: none">@lang('shop.Remove From Cart')</button>
                                </div>
                            <br>
                            </div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>

<!-- END: CONTENT/SHOPS/SHOP-PRODUCT-DETAILS-2 -->
<div class="c-product-short-desc" id="description">

</div>
	<table class="table_unit">
        {{-- <tr>
            <td><strong>@lang('main.Size') : </strong></td>
            <td><span id="size"></span></td>
        </tr>
        <tr>
            <td><strong>@lang('main.Quantity') : </strong></td>
            <td><span id="quantity"></span></td>
        </tr>
        <tr>
            <td><strong>@lang('main.Unit') : </strong></td>
            <td><span id="unit"></span></td>
        </tr> --}}
        <tr>
            <td><strong>@lang('main.ProductCode') : </strong></td>
            <td><span id="product_code"></span></td>
        </tr>
    </table>

<!-- BEGIN: CONTENT/SHOPS/SHOP-2-2 -->
<div class="c-content-box c-size-md  c-bs-grid-small-space">
	<div class="container">
		<div class="c-content-title-4">
			<h3 class="c-font-uppercase c-center c-font-bold c-line-strike"><span class="c-bg-white">@lang('main.MostPopular')</span></h3>
		</div>
		<div class="row">
			<div data-slider="owl">
				<div class="owl-carousel owl-theme c-theme owl-small-space c-owl-nav-center row" style="display: block;opacity:1 !important;"  id="products-grid">

				</div>
			</div>
		</div>
	</div>
</div><!-- END: CONTENT/SHOPS/SHOP-2-2 -->


@endsection



@section('script')

	{{--<script>--}}

    var gid;
    var gprice;
        // =============================================================================================================
        // get Data of product

        var url = window.location.pathname;
        var id = url.substring(url.lastIndexOf('/') + 1);
        var formData_getProd = new FormData();
        formData_getProd.append('id', id);

        $.ajax({

            type: "POST",
            url: '{{route('get_product_api')}}',
            data: formData_getProd,
            // mimeType: 'application/json',
            headers: {"currency":"{{session('currency')}}",
            "language":"{{session('lang')}}"},
            processData: false,
            contentType: false,
            success: function (res) {
                if(res.status == 200) {
                    console.log('get_product_api :', res.data);
                    {{-- $('#unit').text(res.data[0].unit); --}}
                    {{-- $('#size').text('10'); --}}
                    for(let i in res.data[0].attributes){
                       let name = res.data[0].attributes[i].name;
                       let value = res.data[0].attributes[i].value;
                        $('#attributes').append('<div class="name">'+name+' :</div><div class="value">'+value+'</div>')
                    }
                    $('#quantity').text(res.data[0].quantity);
                    $('#price').append(res.data[0].price +' '+res.data[0].currency+'<br><span class="c-font-slim c-font-line-through">'+res.data[0].price_sale +' '+res.data[0].currency+'</span>');
                    $('#product_code').text(res.data[0].product_code);
                    $('#description').html('<div class="title_dec">@lang("home.discription")</div><br>' + res.data[0].description);
                    $('#name').text(res.data[0].name);
                    var image = '';
{{--                    if(res.data.image) {--}}
{{--                        image = get_hostname($(location).attr("href")) + '/images/' + res.data.image.path;--}}
{{--                    }--}}
{{--                    // else{--}}
{{--                    //     image = 'https://static.thenounproject.com/png/2999524-200.png';--}}
{{--                    // }--}}
{{--                    $('#image').attr('src', image);--}}

                    var curr_category_id = res.data[0].category_id;
                    var curr_type_id = res.data[0].type_id;

					$('#category').text(res.data[0].category[0].name);
					$('#type').text(res.data[0].type[0].name);

                    gid = res.data[0].id;
                    if(res.data[0].price_sale != 0){
                        gprice = res.data[0].price_sale;
                    }else{
                        gprice = res.data[0].price;
                    }


                    var cart_arr = localStorage.getItem('damastore_cart');

                    if(cart_arr){
                        var arr = JSON.parse(cart_arr);
                        var exist = false;

                        arr.forEach(function (arritem, index, arr) {
                            if(arritem[0] == res.data[0].id){
                                exist = true;
                            }
                        });
                        if(exist){
                            // if added
                            //console.log('exist');
                            $('#addtocart').hide('fast');
                            $('#addtocartqty').hide('fast');
                            $('#removetocart').show('fast');
                        }else{
                            // if not added
                            //console.log('not exist');
                            $('#addtocart').show('fast');
                            $('#addtocartqty').show('fast');
                            $('#removetocart').hide('fast');
                        }
                    }
                }
            },
            error: function (res, response) {
            },
        });




        $.ajax({

            type: "POST",
            url: '/api/related_product',
            data: formData_getProd,
            // mimeType: 'application/json',
            processData: false,
            contentType: false,
            //headers: {"Authorization": 'Bearer ' + userObject.token},
            success: function (res) {
				console.log('d', res);

                //$('#priceRange').text('Price Range: $' + res.data.minimumVal + ' - $' + res.data.maximumVal);
                //$('#from-input').attr('min', res.data.minimumVal);
                //$('#from-input').attr('max', res.data.maximumVal);
                //$('#to-input').attr('min', res.data.minimumVal);
                //$('#to-input').attr('max', res.data.maximumVal);


                $('#products-grid').empty();

                res.data.data.forEach(function (item, index, arr) {

                        var image;
                        if (item.image) {
                            image = '/images/' + item.image.path;
                        } else {
                            image = 'https://static.thenounproject.com/png/2999524-200.png';
                        }

                        var date = new Date(item.created_at),
                            yr = date.getFullYear(),
                            month = date.getMonth() < 10 ? '0' + date.getMonth() : date.getMonth(),
                            day = date.getDate() < 10 ? '0' + date.getDate() : date.getDate(),
                            newDate = yr + '-' + month + '-' + day;

                        var price = '';
                        var sale_grid = '';
                        if(item.price_sale){
                            sale_grid = '<div class="c-label c-bg-red c-font-uppercase c-font-white c-font-13 c-font-bold">@lang('shop.Sale')</div>';
                            price = '<p class="c-price c-font-14 c-font-slim">{{session('currency')}} ' + item.price + ' <span class="c-font-14 c-font-line-through c-font-red">$' + item.price + '</span></p>';
                        }else{
                            price = '<p class="c-price c-font-14 c-font-slim">{{session('currency')}} ' + item.price_sale + '</p>';
                        }




	//console.log(item);

    $('#products-grid').append('<div class="col-md-3 col-sm-6 c-margin-b-20">' +
        '<div class="c-content-product-2 c-bg-white c-border">' +
            '<div class="c-content-overlay">' + sale_grid +
                '<div class="c-overlay-wrapper">' +
                    '<div class="c-overlay-content">' +
                        '<a href="/productDetails/' + item.id + '" class="navigation btn btn-md c-btn-grey-1 c-btn-uppercase c-btn-bold c-btn-border-1x c-btn-square">@lang('home.Details')</a>' +
                        '</div>' +
                    '</div>' +
                '<div class="c-bg-img-center c-overlay-object" data-height="height" style="height: 230px; background-image: url(/images/' + item.image.path + ');"></div>' +
                '</div>' +
            '<div class="c-info" style="height: 100px">\n' +
                @if(app()->getLocale()=='ar')
                '<p class="c-title c-font-16 c-font-slim">' + item.ar_name + '</p>' + price +
                @else
                '<p class="c-title c-font-16 c-font-slim">' + item.name + '</p>' + price +
                @endif
                '</div>' +
            '<div class="btn-group btn-group-justified" role="group">' +
                '<div class="btn-group c-border-top" role="group" id="quantity_input2_div_' + item.id + '">' +
                    '<input class="form-control" type="number" value="1" id="quantity_input2_' + item.id + '" name="quantity_input2">' +
                    '</div>' +
                '<div class="btn-group c-border-left c-border-top" role="group">' +
                    '<button class="btn btn-sm c-btn-white c-btn-uppercase c-btn-square c-font-grey-3 c-font-white-hover c-bg-red-2-hover c-btn-product qty_btn2" data-id="' + item.id + '" id="qty_btn2_' + item.id + '">@lang('home.Add Cart')</button>' +
                   {{-- '</div>' +
                   '<div class="btn-group c-border-left c-border-top" role="group">' + --}}
                    '<button class="btn btn-sm c-btn-white c-btn-uppercase c-btn-square c-font-grey-3 c-font-white-hover c-bg-red-2-hover c-btn-product qty_btn_rem2 qty_btn_rem_none" data-id="' + item.id + '" id="qty_btn_rem2_' + item.id + '" style="margin-left: 0 !important;margin-right: 0px;display:none;">@lang('home.Remove From Cart')</button>' +
                    '</div>' +
                '</div>' +
            '</div>' +
        '</div>');



    redOrBlueBtn(item.id, 'damastore_cart', 'quantity_input_', 'qty_btn_', 'qty_btn_rem_', 'quantity_input2_div_', 'qty_btn2_', 'qty_btn_rem2_');

	});

                    },
            error: function (res, response) {
            },
        });

        // =============================================================================================================

    $(document).on("click", "#addtocart", function(e) {
    //console.log(gid);
    //console.log(gprice);
    //console.log($('#inputqty').val());
    $('#addtocart').hide('fast');
    $('#addtocartqty').hide('fast');
    $('#removetocart').show('fast');

    var cart_arr = localStorage.getItem('damastore_cart');
    if(cart_arr){
        var arr = JSON.parse(cart_arr);
        var exist = false;
        arr.forEach(function (item, index, arr) {
            if(item[0] == gid){
                exist = true;
            }
        });
        if(!exist){
            arr.push([gid, parseInt($('#inputqty').val())]);
            localStorage.setItem('damastore_cart', JSON.stringify(arr));
        }
    }else{
        var arr = [];
        arr.push([gid, parseInt($('#inputqty').val())]);
        localStorage.setItem('damastore_cart', JSON.stringify(arr));
    }
    updateCartOrder('damastore_cart');

    //AddToCart(id, qty, 'damastore_cart', 'quantity_input_', 'qty_btn_', 'qty_btn_rem_', 'quantity_input2_div_', 'qty_btn2_', 'qty_btn_rem2_');
    });

    $(document).on("click", "#removetocart", function(e) {
        //console.log(gid);
        //console.log(gprice);
        $('#addtocart').show('fast');
        $('#addtocartqty').show('fast');
        $('#removetocart').hide('fast');

        var arr = JSON.parse(localStorage.getItem('damastore_cart'));
        if(arr){
            var new_arr = [];
            arr.forEach(function (item, index, arr) {
                if(item[0] != gid){
                    new_arr.push(item);
                }
            });
            localStorage.setItem('damastore_cart', JSON.stringify(new_arr));
        }

        updateCartOrder('damastore_cart');

        //removeFromCart(id, 'damastore_cart', 'quantity_input_', 'qty_btn_', 'qty_btn_rem_', 'quantity_input2_div_', 'qty_btn2_', 'qty_btn_rem2_');
    });


    $(document).on("click", ".qty_btn", function(e) {
    var id = $(this).data('id');
    var qty = $('#quantity_input_' + id).val();
    AddToCart(id, qty, 'damastore_cart', 'quantity_input_', 'qty_btn_', 'qty_btn_rem_', 'quantity_input2_div_', 'qty_btn2_', 'qty_btn_rem2_');
    });

    $(document).on("click", ".qty_btn_rem", function(e) {
    var id = $(this).data('id');
    removeFromCart(id, 'damastore_cart', 'quantity_input_', 'qty_btn_', 'qty_btn_rem_', 'quantity_input2_div_', 'qty_btn2_', 'qty_btn_rem2_');
    });

    $(document).on("click", ".qty_btn2", function(e) {
    var id = $(this).data('id');
    var qty = $('#quantity_input2_' + id).val();
    AddToCart(id, qty, 'damastore_cart', 'quantity_input2_div_', 'qty_btn2_', 'qty_btn_rem2_', 'quantity_input_', 'qty_btn_', 'qty_btn_rem_');
    });

    $(document).on("click", ".qty_btn_rem2", function(e) {
    var id = $(this).data('id');
    removeFromCart(id, 'damastore_cart', 'quantity_input2_div_', 'qty_btn2_', 'qty_btn_rem2_', 'quantity_input_', 'qty_btn_', 'qty_btn_rem_');
    });

    /*Size is  set in pixels... supports being written as: '250px' */
    var magnifierSize = 250;

    /*How many times magnification of image on page.*/
    var magnification = 4;

    function magnifier() {

    this.magnifyImg = function(ptr, magnification, magnifierSize) {
    var $pointer;
    if (typeof ptr == "string") {
    $pointer = $(ptr);
    } else if (typeof ptr == "object") {
    $pointer = ptr;
    }

    if(!($pointer.is('img'))){
    alert('Object must be image.');
    return false;
    }

    magnification = +(magnification);

    $pointer.hover(function() {
    $(this).css('cursor', 'none');
    $('.magnify').show();
    //Setting some variables for later use
    var width = $(this).width();
    var height = $(this).height();
    var src = $(this).attr('src');
    var imagePos = $(this).offset();
    var image = $(this);

    if (magnifierSize == undefined) {
    magnifierSize = '150px';
    }

    $('.magnify').css({
    'background-size': width * magnification + 'px ' + height * magnification + "px",
    'background-image': 'url("' + src + '")',
    'width': magnifierSize,
    'height': magnifierSize
    });

    //Setting a few more...
    var magnifyOffset = +($('.magnify').width() / 2);
    var rightSide = +(imagePos.left + $(this).width());
    var bottomSide = +(imagePos.top + $(this).height());

    $(document).mousemove(function(e) {
    if (e.pageX < +(imagePos.left - magnifyOffset / 6) || e.pageX > +(rightSide + magnifyOffset / 6) || e.pageY < +(imagePos.top - magnifyOffset / 6) || e.pageY > +(bottomSide + magnifyOffset / 6)) {
    $('.magnify').hide();
    $(document).unbind('mousemove');
    }
    var backgroundPos = "" - ((e.pageX - imagePos.left) * magnification - magnifyOffset) + "px " + -((e.pageY - imagePos.top) * magnification - magnifyOffset) + "px";
    $('.magnify').css({
    'left': e.pageX - magnifyOffset,
    'top': e.pageY - magnifyOffset,
    'background-position': backgroundPos
    });
    });
    }, function() {

    });
    };

    this.init = function() {
    $('body').prepend('<div class="magnify"></div>');
    }

    return this.init();
    }

    var magnify = new magnifier();
    magnify.magnifyImg('#img_zoom', magnification, magnifierSize);

    $('#products-grid').css('opacity','1')
@endsection
<script>


</script>
