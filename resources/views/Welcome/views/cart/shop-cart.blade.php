
@extends('Welcome.views.home1')
@section('style')

	<style>

	</style>

@endsection

@section('home_content')

    <?php
    $locale = App::getLocale();
    ?>


<!-- BEGIN: CONTENT/SHOPS/SHOP-CART-1 -->
<div class="c-content-box c-size-lg">
	<div class="container">
		<div class="c-shop-cart-page-1">
			<div class="row c-cart-table-title">
				<div class="col-md-2 c-cart-image">
					<h3 class="c-font-uppercase c-font-bold c-font-16 c-font-grey-2"> @lang('home.Image')</h3>
				</div>
				<div class="col-md-3 c-cart-desc">
					<h3 class="c-font-uppercase c-font-bold c-font-16 c-font-grey-2"> @lang('home.Description')</h3>
				</div>
				<div class="col-md-1 c-cart-ref">
					<h3 class="c-font-uppercase c-font-bold c-font-16 c-font-grey-2"> @lang('home.Category')</h3>
				</div>
				<div class="col-md-1 c-cart-ref">
					<h3 class="c-font-uppercase c-font-bold c-font-16 c-font-grey-2"> @lang('home.Type')</h3>
				</div>
				<div class="col-md-1 c-cart-qty">
					<h3 class="c-font-uppercase c-font-bold c-font-16 c-font-grey-2"> @lang('home.Qty')</h3>
				</div>
				<div class="col-md-1 c-cart-price">
					<h3 class="c-font-uppercase c-font-bold c-font-16 c-font-grey-2"> @lang('home.Unit Price')</h3>
				</div>
				<div class="col-md-1 c-cart-total">
					<h3 class="c-font-uppercase c-font-bold c-font-16 c-font-grey-2"> @lang('home.Total')</h3>
				</div>
				<div class="col-md-1 c-cart-remove"></div>
			</div>
		</div>
		<div class="c-shop-cart-page-1" id="products">
		</div>
		<div class="c-shop-cart-page-1">
			<!-- BEGIN: SUBTOTAL ITEM ROW -->
			<div class="row">
                <div class="col-md-6 m-2">
                    <br>
                    <div class="c-cart-buttons">
                        <input style="display: inline-block;width:auto;" type="text" name="coupon"  id="coupon" class="form-control c-square c-theme" placeholder="@lang('home.coupon_use')">
                        <span class="navigation btn c-btn c-theme-btn c-btn-square c-font-white c-font-bold c-font-uppercase"  id="coupon_submit">@lang('home.coupon_submit')</span>
                        <br>
                        <span id="note" style="color: goldenrod"></span>
                    </div>
                </div>
                	<div id="div_coupon" style="display: none" class="c-cart-subtotal-row c-margin-t-20">
					<div class="col-md-2 col-md-offset-8 col-sm-6 col-xs-6 c-cart-subtotal-border">
						<h3 class="c-font-uppercase c-font-bold c-right c-font-16 c-font-grey-2">
                            <a href="#" class="c-theme-link" style="" id="delete_coupon">×</a>
                            @lang('home.coupon')
                        </h3>
					</div>
					<div class="col-md-1 col-sm-6 col-xs-6 c-cart-subtotal-border">
						<h3 class="c-font-bold c-font-16"> {{session('currency')}} <span id="coupon_value"></span></h3>
                    </div>
				</div>
				<div class="c-cart-subtotal-row c-margin-t-20">
					<div class="col-md-2 col-md-offset-8 col-sm-6 col-xs-6 c-cart-subtotal-border">
						<h3 class="c-font-uppercase c-font-bold c-right c-font-16 c-font-grey-2"> @lang('home.Tax')</h3>
					</div>
					<div class="col-md-1 col-sm-6 col-xs-6 c-cart-subtotal-border">
						<h3 class="c-font-bold c-font-16"> {{session('currency')}} <span id="Tax"></span></h3>
					</div>
				</div>
				<div class="c-cart-subtotal-row c-margin-t-20">
					<div class="col-md-2 col-md-offset-8 col-sm-6 col-xs-6 c-cart-subtotal-border">
						<h3 class="c-font-uppercase c-font-bold c-right c-font-16 c-font-grey-2"> @lang('home.Total Cost')</h3>
					</div>
					<div class="col-md-1 col-sm-6 col-xs-6 c-cart-subtotal-border">
						<h3 class="c-font-bold c-font-16"> {{session('currency')}} <span id="totalCost"></span></h3>
					</div>
				</div>
			</div>

            <div class="row">
                <div class="col-md-4">
                    <div class="c-cart-buttons" style="">
                        <a href="{{route('shop')}}" style="width:100%;margin:2px;" class="navigation btn c-btn btn-lg c-btn-red c-btn-square c-font-white c-font-bold c-font-uppercase c-cart-float-l">@lang('home.ContinueShopping')</a>
                        <span class="navigation btn c-btn btn-lg c-theme-btn c-btn-square c-font-white c-font-bold c-font-uppercase c-cart-float-r" style="float:none;width:100%;margin:2px;" id="checkout-btn">@lang('home.Checkout')</span>
                    </div>
                </div>
            </div>
		</div>
	</div>
</div><!-- END: CONTENT/SHOPS/SHOP-CART-1 -->




		<!-- END: PAGE CONTENT -->
	</div>
	<!-- END: PAGE CONTAINER -->

<input type="hidden" id="coupon_value_up">
<input type="hidden" id="coupon_id">
@endsection



@section('script')

	{{--<script>--}}

	var userObject = JSON.parse(localStorage.getItem("DamaStore_userObject"));

	// blocked
	// bornDate
	// created_at
	// email
	// emailVerifiedAt
	// files
	// firstName
	// id
	// lastActivity
	// lastLoggedIn
	// lastName
	// phone
	// reputation
	// roles
	// token
	// updated_at
	// username
	// verify


    function getData(arr) {

console.log('hey');

	id_arr = [];
	qty_arr = [];
	qty_price_arr = [];
	var totalPrice = 0;
	arr.forEach(function (item, index, arr) {
		id_arr.push(item[0]);
		qty_arr.push(item[1]);
	});

	var formData = new FormData();
	formData.append('arr', id_arr);
    let x;
   if(userObject){
    x=userObject.token;
   } else{
       x='null';
   }
        $.ajax({

            type: "POST",
            url: '/api/ProductCartList',
            data: formData,
            // mimeType: 'application/json',
            processData: false,
            contentType: false,
            headers: {"Authorization": 'Bearer ' + x },
            success: function (res) {

                $('#products').empty();
                console.log(res);
               console.log(res.data.coupon);

                res.data.data.forEach(function (item, index, arr) {

					var qty_match;
					id_arr.forEach(function (arr_item, arr_index, arr_arr) {
						if(arr_item == item.id){
							qty_match = qty_arr[arr_index];
							console.log(arr_index, qty_match);
						}
					});

                    qty_price_arr.push([qty_match, item.price]);

                    var image;
                    if (item.image) {
                        image = '/images/' + item.image.path;
                    } else {
                        image = 'https://static.thenounproject.com/png/2999524-200.png';
                    }

                    var date = new Date(item.created_at),
                        yr = date.getFullYear(),
                        month = date.getMonth() < 10 ? '0' + date.getMonth() : date.getMonth(),
                        day = date.getDate() < 10 ? '0' + date.getDate() : date.getDate(),
                        newDate = yr + '-' + month + '-' + day;

                    var price = '';
                    var sale_grid = '';

                        price = '<p class="c-price c-font-14 c-font-slim">{{session('currency')}} ' + item.price + '</p>';


    var prodprice;
        prodprice = item.price;

                    $('#products').append('<div class="row c-cart-table-row" id="WholeDiv_' + item.id + '"> ' +
                        '	<h2 class="c-font-uppercase c-font-bold c-theme-bg c-font-white c-cart-item-title"></h2> ' +
                        '	<div class="col-md-2 col-sm-3 col-xs-5 c-cart-image"> ' +
                        '		<img src="' + image + '"/> ' +
                        '	</div> ' +
                        '	<div class="col-md-3 col-sm-9 col-xs-7 c-cart-desc"> ' +
            @if(session('lang') == 'ar')
                '		<h3><a href="/productDetails/' + item.id + '" class="navigation c-font-bold c-theme-link c-font-22 c-font-dark">' + item.ar_name + '</a></h3> ' +
                @else
                '		<h3><a href="/productDetails/' + item.id + '" class="navigation c-font-bold c-theme-link c-font-22 c-font-dark">' + item.name + '</a></h3> ' +
                @endif

                        '		<p> @lang('home.Product Code'): ' + item.product_code + '</p> ' +
                        {{-- '		<p> @lang('home.Unit'): ' + item.unit + '</p> ' +
                        '		<p> @lang('home.Size'): ' + item.size + '</p> ' + --}}
                        '	</div> ' +
                        '	<div class="col-md-1 col-sm-3 col-xs-6 c-cart-ref"> ' +
                        '		<p class="c-cart-sub-title c-theme-font c-font-uppercase c-font-bold">@lang('main.Category')</p> ' +
                        '		<p>' + item.category + '</p> ' +
                        '	</div> ' +
                        '	<div class="col-md-1 col-sm-3 col-xs-6 c-cart-ref"> ' +
                        '		<p class="c-cart-sub-title c-theme-font c-font-uppercase c-font-bold">@lang('main.Type')</p> ' +
                        '		<p>' + item.type + '</p> ' +
                        '	</div> ' +
                        '	<div class="col-md-1 col-sm-3 col-xs-6 c-cart-qty"> ' +
                        '		<p class="c-cart-sub-title c-theme-font c-font-uppercase c-font-bold">QTY</p> ' +
                        '		<div class="c-input-group c-spinner"> ' +
                        '			<input type="text" class="form-control c-item-2 qty_counter" data-id="' + item.id +'" data-price="' + prodprice + '" id="qty_counter_' + item.id + '" value="' + qty_match + '"> ' +
                        '			<div class="c-input-group-btn-vertical"> ' +
                        '				<button class="btn btn-default up_btn" data-price="' + prodprice + '" data-id="' + item.id + '" type="button" data_input="c-item-2" data-maximum="10"><i class="fa fa-caret-up"></i></button> ' +
                        '				<button class="btn btn-default down_btn" id="down_btn_' + item.id + '" data-price="' + prodprice + '" data-id="' + item.id + '" type="button" data_input="c-item-2"><i class="fa fa-caret-down"></i></button> ' +
                        '			</div> ' +
                        '		</div> ' +
                        '	</div> ' +
                        '	<div class="col-md-1 col-sm-3 col-xs-6 c-cart-price"> ' +
                        '		<p class="c-cart-sub-title c-theme-font c-font-uppercase c-font-bold">Unit Price</p> ' +
                        '		<p class="c-cart-price c-font-bold"> {{session('currency')}} ' + prodprice + '</p> ' +
                        '	</div> ' +
                        '	<div class="col-md-1 col-sm-3 col-xs-6 c-cart-total"> ' +
                        '		<p class="c-cart-sub-title c-theme-font c-font-uppercase c-font-bold">Total</p> ' +
                        '		<p class="c-cart-price c-font-bold"> {{session('currency')}} <span id="unit_total_qty_' + item.id + '">' + (prodprice * qty_match) + ' </span></p> ' +
                        '	</div> ' +
                        '	<div class="col-md-1 col-sm-12 c-cart-remove"> ' +
                        '		<a href="#" class="c-theme-link c-cart-remove-desktop removeProdBtn" data-id="' + item.id + '" data-price="' + prodprice + '">×</a> ' +
                        '		<a href="#" data-id="' + item.id + '" data-price="' + prodprice + '" class="removeProdBtn navigation c-cart-remove-mobile btn c-btn c-btn-md c-btn-square c-btn-red c-btn-border-1x c-font-uppercase">Remove item from Cart</a> ' +
                        '	</div> ' +
                        '</div>');

					if(qty_match == 1){
						$('#down_btn_' + item.id).attr('disabled', true);
					}

                    totalPrice += (prodprice * qty_match);


					$( ".loading" ).remove();
                });
                if(res.data.coupon){
                    $('#div_coupon').show('slow');
                    var couponValue = Math.ceil((totalPrice * res.data.coupon.value) / 100);
                    $('#coupon_value').html(couponValue);
                    $('#coupon_value_up').val(res.data.coupon.value);
                    $('#coupon_id').val(res.data.coupon.id);
                }else{
                    couponValue=0;
                }
                var Tax = (totalPrice * 5) / 100;
                $('#Tax').text(Math.ceil(Tax));
                totalPrice = (totalPrice + Tax )- couponValue;
                $('#totalCost').text(Math.ceil(totalPrice));
            },
            error: function (res, response) {
            },
        });

    }

	// class : qty_counter, data-id, data-price
	// id : qty_counter_

	// id : unit_total_qty_

	// id : totalCost

	// var :
	// totalPrice
	// qty_price_arr


	$(document).on("click", ".up_btn", function(e) {
		var id = $(this).data('id');
		var price = $(this).data('price');
		if(parseInt($('#qty_counter_' + id).val()) == 1){
			$('#down_btn_' + id).attr('disabled', false);
		}
		$('#qty_counter_' + id).val(parseInt($('#qty_counter_' + id).val()) + 1);
		$('#unit_total_qty_' + id).text(price * parseInt($('#qty_counter_' + id).val()));
        $('#coupon_value').text((Math.ceil($('#coupon_value_up').val() * Math.ceil(parseInt($('#totalCost').text())) ) / 100));
		$('#Tax').text(Math.ceil(parseInt($('#Tax').text()) + ((price * 5) / 100)));
        var cooooopon =Math.ceil(parseInt($('#coupon_value').text()));
		$('#totalCost').text(Math.ceil(parseInt($('#totalCost').text()) + price + ((price * 5) / 100) - cooooopon));
		increaseQty(id, 'damastore_cart');
        window.location="/cart";
	});

	$(document).on("click", ".down_btn", function(e) {
		var id = $(this).data('id');
		var price = $(this).data('price');
		if(parseInt($('#qty_counter_' + id).val()) == 2){
			$('#down_btn_' + id).attr('disabled', true);
		}
		$('#qty_counter_' + id).val(parseInt($('#qty_counter_' + id).val()) - 1);
		$('#unit_total_qty_' + id).text(price * parseInt($('#qty_counter_' + id).val()));
        $('#coupon_value').text((Math.ceil($('#coupon_value_up').val() * Math.ceil(parseInt($('#totalCost').text()))) /100));
        var cooooopon =Math.ceil(parseInt($('#coupon_value').text()));
		$('#Tax').text(Math.ceil(parseInt($('#Tax').text()) - ((price * 5) / 100)));
		$('#totalCost').text(Math.ceil(parseInt($('#totalCost').text()) - price - ((price * 5) / 100) - cooooopon));
		decreaseQty(id, 'damastore_cart');
        window.location="/cart";
	});

	$(document).on("click", ".removeProdBtn", function(e) {
		var id = $(this).data('id');
		var qty = $('#qty_counter_' + id).val();
		var price = $(this).data('price');

		removeFromCartRec(id, 'damastore_cart');
		var arr = JSON.parse(localStorage.getItem('damastore_cart'));
		if(arr.length == 0){
			$('#Tax').text(parseInt(0));
			$('#totalCost').text(parseInt(0));
		}else{
			$('#Tax').text(Math.ceil(parseInt($('#Tax').text()) - ((price * qty * 5) / 100)));
			$('#totalCost').text(Math.ceil(parseInt($('#totalCost').text()) - (price * qty) - ((price * qty * 5) / 100)));
		}
		$('#WholeDiv_' + id).remove();

	});


	$(document).on("click", "#checkout-btn", function(e) {
		if(userObject){
		if(userObject.token){
        $.ajax({
            type: "POST",
            url: '{{route('TestAuth')}}',
            headers: {"Authorization": 'Bearer ' + userObject.token},
            data:{user:userObject.token},
            processData: false,
            contentType: false,
            success: function ( res ) {
            console.log( res );
            if(res.status==="ok"){
            	StartOrder();
               }
            if(res.status==="needAddress"){
    Swal.fire({
    icon: 'note',
    title: 'note',
    text: 'your Shipping information not set !',
    }).then(function() {
    window.location.href = "/editprofile";
    });
            }
            },
            error: function ( res , response ) {
               alert('please sign in first .');
            },
        });
    }
		}else{
			window.location.href = "/CustomerLoginRegister";
		}
	});

    // if you want to save the payment order
    function StartOrder() {
		var cart_arr = localStorage.getItem('damastore_cart');
		var arr = JSON.parse(cart_arr);
        var formData = new FormData();
        formData.append("products" , arr);
        formData.append("user" , userObject.token);

        $.ajax({
            type: "POST",
            url: '{{route('startOrder')}}',
		    headers: {"Authorization": 'Bearer ' + userObject.token},
            data: formData,
            processData: false,
            contentType: false,
            success: function ( res ) {
				localStorage.setItem('damastore_Order', res);
				window.location.href = "/order";
            },
            error: function ( res , response ) {
                $( ".loading" ).remove();
            },
        });
    };



    $(document).on("click", "#coupon_submit", function(e) {
        var userObject = JSON.parse(localStorage.getItem("DamaStore_userObject"));
		var coupon = $('#coupon').val();
        var formData = new FormData();
        formData.append("coupon" , coupon);
        if(!userObject){
            $('#note').html('Please Login first ! <a href="/CustomerLoginRegister">Click To Login</a>')
          return false;
        }
        $.ajax({
            type: "POST",
            url: '{{route('UserAddCoupon')}}',
            headers: {"Authorization": 'Bearer ' + userObject.token},
            data: formData,
            processData: false,
            contentType: false,
            success: function ( res ) {
				if(res.status=='error')
                {
                    $('#note').html(res.validator.coupon)
                }
                if(res.status=='notExist')
                {
                    $('#note').html(res.msg);
                }
                if(res.status=='expired')
                {
                    $('#note').html(res.msg);
                }
                if(res.status=='submitted')
                {
                    $('#note').html(res.msg);
                    window.location.href = "/cart";
                }
                   if(res.status=='used')
                {
                    $('#note').html(res.msg);
                }
            },
            error: function ( res , response ) {
                console.log(res, response)
            },
        });
    });


    $(document).on("click", "#delete_coupon", function(e) {
        var userObject = JSON.parse(localStorage.getItem("DamaStore_userObject"));
		var coupon_id = $('#coupon_id').val();
        $.ajax({
            type: "delete",
            url: '/api/UserDeleteCoupon/'+ coupon_id,
            headers: {"Authorization": 'Bearer ' + userObject.token},
            data: null,
            processData: false,
            contentType: false,
            success: function ( res ) {
                    window.location.href = "/cart";
            },
            error: function ( res , response ) {
                console.log(res, response)
            },
        });
    });



    var cart_arr = localStorage.getItem('damastore_cart');
	var arr = JSON.parse(cart_arr);
    getData(arr);



@endsection
