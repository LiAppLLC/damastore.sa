@extends('Welcome.views.home1')
@section('style')
    <style>
        .about {
            cursor: pointer;
            background-color: #ffffff;
            color: #FFFFFF;
            padding-top: 20px;
            padding-bottom: 30px;
        }

        .about h1 {
            padding: 10px 0;
            margin-bottom: 20px;
        }

        .about h2 {
            opacity: .8;
        }

        .about span {
            display: block;
            width: 100px;
            height: 100px;
            line-height: 100px;
            margin: auto;
            border-radius: 50%;
            font-size: 40px;
            color: #FFFFFF;
            opacity: 0.7;
            background-color: #ffb23f;
            border: 2px solid #c2c2c2;

            webkit-transition: all .5s ease;
            moz-transition: all .5s ease;
            os-transition: all .5s ease;
            transition: all .5s ease;

        }

        .about-item:hover span {
            opacity: 1;
            border: 3px solid #CC0039;
            font-size: 42px;
            -webkit-transform: scale(1.1, 1.1) rotate(360deg);
            -moz-transform: scale(1.1, 1.1) rotate(360deg);
            -o-transform: scale(1.1, 1.1) rotate(360deg);
            transform: scale(1.1, 1.1) rotate(360deg);
        }

        .about-item:hover h2 {
            opacity: 1;
            -webkit-transform: scale(1.1, 1.1);
            -moz-transform: scale(1.1, 1.1);
            -o-transform: scale(1.1, 1.1);
            transform: scale(1.1, 1.1);
        }

        .about .lead {
            color: #b1b1b1;
            font-size: 14px;
            font-weight: bold;
        }

    </style>
@endsection

@section('home_breadcrumb_title')
    {{-- @lang('home.Shop') --}}
@endsection

@section('home_content')

    <?php $locale = App::getLocale(); ?>


    @if ($locale == 'ar')
        <style>
            .c-layout-sidebar-menu {
                direction: rtl;
            }

        </style>
    @endif
    @if ($locale == 'ar')
        <section class="text-center about">
            {{-- <h1>Deletion policy</h1> --}}
            <div class="container">
                <div class="row justify-content-center">

                    <div class="clearfix visible-md-block visible-sm-block"></div>
                    <div class="col-lg-12 col-sm-12 col-ex-12 about-item wow lightSpeedIn" data-wow-offset="200">
                        <span class="fa fa-remove"></span>
                        <h2>سياسة الحذف</h2>
                        <p class="lead " style="text-align: start">
                        <ul style="text-align: start">
                            <li><i class="fa fa-info-circle" style="color:gray;"></i><b
                                    style="background-color:transparent;color:#000000;font-family:Calibri,sans-serif;font-size:12pt;">
                                     حذف بيانات المستخدم من موقع داما ستور:</b></li>
                                     <hr>
                            <li><i class="fa fa-info-circle" style="color:gray;"></i><b
                                    style="background-color:transparent;color:#000000;font-family:Calibri,sans-serif;font-size:12pt;">
                                     يقوم داماستور بحذف جميع بيانات التصفح و البيانات الشخصية للمستخدم و أي بيانات محفوظة لدينا في المخدم اثناء عملية التسجيل، يمكن الوصول لحذف البيانات بالضغط على الرابط في حساب المستخدم .
                                </b></li>
                        </ul>
                        </p>
                    </div>

                </div>

            </div>
        </section>
    @else
        <section class="text-center about">
            {{-- <h1>Deletion policy</h1> --}}
            <div class="container">
                <div class="row justify-content-center">

                    <div class="clearfix visible-md-block visible-sm-block"></div>
                    <div class="col-lg-12 col-sm-12 col-ex-12 about-item wow lightSpeedIn" data-wow-offset="200">
                        <span class="fa fa-remove"></span>
                        <h2>Deletion polic</h2>
                        <p class="lead " style="text-align: start">
                        <ul style="text-align: start">
                            <li><i class="fa fa-info-circle" style="color:gray;"></i><b
                                    style="background-color:transparent;color:#000000;font-family:Calibri,sans-serif;font-size:12pt;">
                                     Deleting user’s  data from the DamaStore website:
                                </b></li>
                                <hr>
                            <li><i class="fa fa-info-circle" style="color:gray;"></i><b
                                    style="background-color:transparent;color:#000000;font-family:Calibri,sans-serif;font-size:12pt;">
                                     DamaStore deletes all browsing data, the personal data, and any data we have saved on the server during the registration process, you can delete the data by clicking on the link in the user's account.</b></li>
                        </ul>
                        </p>
                    </div>

                </div>

            </div>
        </section>
    @endif
@endsection



@section('script')

@endsection
