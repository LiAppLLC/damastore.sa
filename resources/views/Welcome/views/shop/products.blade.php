@extends('Welcome.views.home1')
@section('style')
<style>

/*panel*/
.panel {
    border: none;
    box-shadow: none;
}

.panel-heading {
    border-color:#eff2f7 ;
    font-size: 16px;
    font-weight: 300;
}

.panel-title {
    color: #2A3542;
    font-size: 14px;
    font-weight: 400;
    margin-bottom: 0;
    margin-top: 0;
}


/*product list*/

.prod-cat li a{
    border-bottom: 1px dashed white;
}

.prod-cat li a {
    color: #72757a;
    background-color: #fcd49b;
    transition: all 0.2s;
    border-radius: 50px;
}

.prod-cat li ul {
    margin-left: 0px;
}

.prod-cat li ul li a{
    border-bottom:none;
}
.prod-cat li ul li a:hover,.prod-cat li ul li a:focus, .prod-cat li ul li.active a , .prod-cat li a:hover,.prod-cat li a:focus, .prod-cat li a.active{
    background: none;
    color: #ffa734;
}

.pro-lab{
    margin-right: 20px;
    font-weight: normal;
}

.pro-sort {
    padding-right: 20px;
    float: left;
}

.pro-page-list {
    margin: 5px 0 0 0;
}

.product-list img{
    width: 100%;
    border-radius: 4px 4px 0 0;
    -webkit-border-radius: 4px 4px 0 0;
}

.product-list .pro-img-box {
    position: relative;
}
.adtocart {
    background: #ff9736;
    width: 50px;
    height: 50px;
    border-radius: 50%;
    -webkit-border-radius: 50%;
    color: #fff;
    display: inline-block;
    text-align: center;
    border: 3px solid #fff;
    left: 45%;
    bottom: -25px;
    position: absolute;
}

.adtocart i{
    color: #fff;
    font-size: 25px;
    line-height: 42px;
}

.pro-title {
    color: #5A5A5A;
    display: inline-block;
    margin-top: 20px;
    font-size: 16px;
}

.product-list .price {
    color:#fc5959 ;
    font-size: 15px;
}

.pro-img-details {
    margin-left: -15px;
}

.pro-img-details img {
    width: 100%;
}

.pro-d-title {
    font-size: 16px;
    margin-top: 0;
}

.product_meta {
    border-top: 1px solid #eee;
    border-bottom: 1px solid #eee;
    padding: 10px 0;
    margin: 15px 0;
}

.product_meta span {
    display: block;
    margin-bottom: 10px;
}
.product_meta a, .pro-price{
    color:#fc5959 ;
}

.pro-price, .amount-old {
    font-size: 18px;
    padding: 0 10px;
}

.amount-old {
    text-decoration: line-through;
}

.quantity {
    width: 120px;
}

.pro-img-list {
    margin: 10px 0 0 -15px;
    width: 100%;
    display: inline-block;
}

.pro-img-list a {
    float: left;
    margin-right: 10px;
    margin-bottom: 10px;
}

.pro-d-head {
    font-size: 18px;
    font-weight: 300;
}
.prod-cat {
            width: 100% !important;
        }
        @media screen and (max-width:991px)
        {
            .prod-cat {
                width: auto !important;
        }
        .prod-cat {
                margin-left: 0px !important;
            }

        }
</style>
@endsection

@section('home_breadcrumb_title')
    @lang('home.Shop')
@endsection

@section('home_content')

    <?php
    $locale = App::getLocale();
    ?>


    @if( $locale=="ar")
        <style>
            .c-layout-sidebar-menu {
                direction: rtl;
            }


        </style>
    @endif


    <link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css" rel="stylesheet" />
    <div class="container bootdey">
        <div class="col-md-3">
            <section class="panel">
                <div class="panel-body">
                    {{-- <input type="text" placeholder="Keyword Search" class="form-control" /> --}}
                </div>
            </section>
            <section class="panel">
                <header class="panel-heading">
                    @lang('main.Category')
                </header>
                <div class="panel-body">

                    <ul class="nav prod-cat">
                        {{-- <li>
                            <a href="#" class="active"><i class="fa fa-angle-right"></i> Dress</a>
                            <ul class="nav">
                                <li class="active"><a href="#">- Shirt</a></li>
                                <li><a href="#">- Pant</a></li>
                                <li><a href="#">- Shoes</a></li>
                            </ul>
                        </li> --}}

                        @foreach ($data as $item)
                        <li>
                            @if (session('lang') == 'ar')
                            <a href="/products/{{$item->id}}"><i class="fa fa-list" style="padding:5px"></i>{{$item->ar_name}}</a>
                            @else
                            <a href="/products/{{$item->id}}"><i class="fa fa-angle" style="padding:5px"></i>{{$item->name}}</a>
                            @endif
                        </li>
                        @endforeach
                    </ul>
                </div>
            </section>
            {{-- <section class="panel">
                <header class="panel-heading">
                    Price Range
                </header>
                <div class="panel-body sliders">
                    <div id="slider-range" class="slider"></div>
                    <div class="slider-info">
                        <span id="slider-range-amount"></span>
                    </div>
                </div>
            </section>
            <section class="panel">
                <header class="panel-heading">
                    Filter
                </header>
                <div class="panel-body">
                    <form role="form product-form">
                        <div class="form-group">
                            <label>Brand</label>
                            <select class="form-control hasCustomSelect" style="-webkit-appearance: menulist-button; width: 231px; position: absolute; opacity: 0; height: 34px; font-size: 12px;">
                                <option>Wallmart</option>
                                <option>Catseye</option>
                                <option>Moonsoon</option>
                                <option>Textmart</option>
                            </select>
                            <span class="customSelect form-control" style="display: inline-block;"><span class="customSelectInner" style="width: 209px; display: inline-block;">Wallmart</span></span>
                        </div>
                        <div class="form-group">
                            <label>Color</label>
                            <select class="form-control hasCustomSelect" style="-webkit-appearance: menulist-button; width: 231px; position: absolute; opacity: 0; height: 34px; font-size: 12px;">
                                <option>White</option>
                                <option>Black</option>
                                <option>Red</option>
                                <option>Green</option>
                            </select>
                            <span class="customSelect form-control" style="display: inline-block;"><span class="customSelectInner" style="width: 209px; display: inline-block;">White</span></span>
                        </div>
                        <div class="form-group">
                            <label>Type</label>
                            <select class="form-control hasCustomSelect" style="-webkit-appearance: menulist-button; width: 231px; position: absolute; opacity: 0; height: 34px; font-size: 12px;">
                                <option>Small</option>
                                <option>Medium</option>
                                <option>Large</option>
                                <option>Extra Large</option>
                            </select>
                            <span class="customSelect form-control" style="display: inline-block;"><span class="customSelectInner" style="width: 209px; display: inline-block;">Small</span></span>
                        </div>
                        <button class="btn btn-primary" type="submit">Filter</button>
                    </form>
                </div>
            </section>
            <section class="panel">
                <header class="panel-heading">
                    Best Seller
                </header>
                <div class="panel-body">
                    <div class="best-seller">
                        <article class="media">
                            <a class="pull-left thumb p-thumb">
                                <img src="https://via.placeholder.com/250x220/FFB6C1/000000" />
                            </a>
                            <div class="media-body">
                                <a href="#" class="p-head">Item One Tittle</a>
                                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p>
                            </div>
                        </article>
                        <article class="media">
                            <a class="pull-left thumb p-thumb">
                                <img src="https://via.placeholder.com/250x220/A2BE2/000000" />
                            </a>
                            <div class="media-body">
                                <a href="#" class="p-head">Item Two Tittle</a>
                                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p>
                            </div>
                        </article>
                        <article class="media">
                            <a class="pull-left thumb p-thumb">
                                <img src="https://via.placeholder.com/250x220/6495ED/000000" />
                            </a>
                            <div class="media-body">
                                <a href="#" class="p-head">Item Three Tittle</a>
                                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p>
                            </div>
                        </article>
                    </div>
                </div>
            </section> --}}
        </div>
        <div class="col-md-9">
            <section class="panel">
                <div class="panel-body">
                    {{-- <div class="pull-right">
                      {{$products->links()}}
                    </div> --}}
                </div>
            </section>

            <div class="row product-list">
                @foreach ($products as $item)

                <div class="col-md-4">
                    <section class="panel">
                        <div class="pro-img-box">
                            <img src="/images/{{$item->image}}" alt="" />
                            <a href="/productDetails/{{$item->id}}" class="adtocart">
                                <i class="fa fa-shopping-cart"></i>
                            </a>
                        </div>

                        <div class="panel-body text-center">
                            <h4>
                                <a href="#" class="pro-title">
                                    @if (session('lang') == 'ar')
                                {{$item->ar_name}}
                                    @else
                                   {{$item->name}}
                                    @endif
                                </a>
                            </h4>
                            <p class="price">{{$item->price}} {{session('currency')}}</p>
                        </div>
                    </section>
                  </div>
                @endforeach
                </div>
            </div>
            {{$products->links()}}
        </div>
    </div>




@endsection



@section('script')

@endsection
