@extends('Welcome.views.home1')
@section('style')
<style>
    .about{
	cursor: pointer;
	background-color: #ffffff;
	color: #FFFFFF;
	padding-top: 20px;
	padding-bottom: 30px;
}
.about h1 {
	padding: 10px 0;
	margin-bottom: 20px;
}
.about h2 {
	opacity: .8;
}
.about span {
	display: block;
	width: 100px;
	height: 100px;
	line-height: 100px;
	margin:auto;
	border-radius:50%;
	font-size: 40px;
	color: #FFFFFF;
	opacity: 0.7;
	background-color: #ffb23f;
	border: 2px solid #c2c2c2;

	webkit-transition:all .5s ease;
 	moz-transition:all .5s ease;
 	os-transition:all .5s ease;
 	transition:all .5s ease;

}
.about-item:hover span{
	opacity: 1;
	border: 3px solid #CC0039;
	font-size: 42px;
	-webkit-transform: scale(1.1,1.1) rotate(360deg) ;
	-moz-transform: scale(1.1,1.1) rotate(360deg) ;
	-o-transform: scale(1.1,1.1) rotate(360deg) ;
	transform: scale(1.1,1.1) rotate(360deg) ;
}
.about-item:hover h2{
	opacity: 1;
	-webkit-transform: scale(1.1,1.1)  ;
	-moz-transform: scale(1.1,1.1)  ;
	-o-transform: scale(1.1,1.1)  ;
	transform: scale(1.1,1.1) ;
}
.about .lead{
	color: #b1b1b1;
	font-size: 14px;
	font-weight: bold;
}

</style>
@endsection

@section('home_breadcrumb_title')
    {{-- @lang('home.Shop') --}}
@endsection

@section('home_content')

    <?php
    $locale = App::getLocale();
    ?>


    @if( $locale=="ar")
        <style>
            .c-layout-sidebar-menu {
                direction: rtl;
            }


        </style>
    @endif
	@if( $locale=="ar")
    <section class="text-center about">
        <h1>نبذة عنا</h1>
        <div class="container">
          <div class="row">
            <div class="col-lg-4 col-sm-6 col-ex-12 about-item wow lightSpeedIn" data-wow-offset="200" >
              <span class="fa fa-group"></span>
              <h2>داما ستور</h2>
              <p class="lead">
				، هو أول المشاريع التي قامت بانجازها شركة Li-app LLC التي تأسست في 1/6/2020و التي تقوم فكرتها على انشاء و برمجة تطبيقات الهواتف المحمولة و المواقع الالكترونية، و انترنت الأشياء و الذكاء الصنعي.				</p>
            </div>
            <div class="col-lg-4 col-sm-6 col-ex-12 about-item wow lightSpeedIn" data-wow-offset="200">
              <span class="fa fa-info"></span>
              <h2>الفكرة من انشاء داما ستور </h2>
              <p class="lead">
				كانت الفكرة من مشروع داما ستور هو انشاء متجر الكتروني يقوم بالتسويق للعراقة الدمشقية و الحرف اليدوية التي تصنع فيها و نقلها لجميع دول العالم و عكس الحضارة التاريخية فيها و التي تمتد للألاف السنين، 
				حيث تعتبر الصناعات اليدوية الدمشقية.. إبداع فني يعكس ذوق صاحبها ومهنيته 
			   اشتهرت دمشق بصناعاتها اليدوية التقليدية المختلفة المتميزة بالجودة والإتقان والجمال ومن أهم هذه الصناعات الفخارية والزجاجية والمعدنية والنسيجية والجلدية والسيوف الدمشقية ونسيج البروكار.
			   وتدل هذه المصنوعات اليدوية على ذوق صاحبها وصانعها فقد تميز الصانع الفني الدمشقي بالمهارة اليدوية والخبرة المهنية والذوق الفني والحس الجمالي ما جعل مصنوعاته متميزة بالدقة والابتكار والتجديد وحقق لها شهرة واسعة في كل العالم.
			   
				</p>
            </div>
            <div class="clearfix visible-md-block visible-sm-block"></div>
            <div class="col-lg-4 col-sm-6 col-ex-12 about-item wow lightSpeedIn" data-wow-offset="200">
              <span class="fa fa-file"></span>
              <h2>الهدف من انشاء متجر داما ستور</h2>
              <p class="lead">
				لأن سوق المهن اليدوية من أكثر الأمكنة التي يتردّد عليها الناس و السائحين في دمشق، فهم يحبّون هذه المنتوجات التي تخرج من أيدي أولئك الحرفيّين، الذين يصنعونها بحب, ينقلون من خلالها عراقة و تاريخ حضارات متتالية مرت على مدينة واحدة.
				كان هدفنا من هذا المتجر هو نقل ما يحبه السياح و يبحثون عنه و يستمتعون به عند القدوم إلى دمشق ليكون بمتناول يدهم في كل مكان بالعالم و كأنهم يقومون برحلة داخل سوق دمشقي و لكن بطراز عصري حديث .
				يسعدنا ان تحصلوا على ما تحبون و أن ينال اعجابكم كل ما هو متوفر بمتجرنا و يسرنا معرفة أرائكم حول ما نقدمه و حول اشياء تريدون ان تكون موجودة في المتجر .
				
			</p>
            </div>

          </div>

        </div>
      </section>
	  @else
	  <section class="text-center about">
        <h1>About US</h1>
        <div class="container">
          <div class="row">
            <div class="col-lg-4 col-sm-6 col-ex-12 about-item wow lightSpeedIn" data-wow-offset="200" >
              <span class="fa fa-group"></span>
              <h2>DamaStore
			</h2>
              <p class="lead">
				It is the first project carried out by Li-app LLC, which was founded on 6/1/2020, and whose idea is based on creating and programming mobile phone applications, websites, the Internet of things and artificial intelligence.
				</p>
            </div>
            <div class="col-lg-4 col-sm-6 col-ex-12 about-item wow lightSpeedIn" data-wow-offset="200">
              <span class="fa fa-info"></span>
              <h2>The idea of ​​creating Dama Store</h2>
              <p class="lead">
				The idea of ​​the Dama Store project was to set up an online store that would market the Damascene heritage and the handicrafts that are made in it and transfer them to all countries of the world and reflect the historical civilization in it that extends for thousands of years,
				Damascene handicrafts are considered  an artistic creation that reflects the taste and professionalism of its owner
				Damascus is famous for its various traditional handicrafts, distinguished by quality, perfection and beauty. The most important of these industries are pottery, glass, metal, textile, leather, Damascene swords and brocade weaving.
				These handicrafts are indicative of the taste of their owner and maker. The Damascene artistic maker was distinguished by the craftsmanship, professional experience, artistic taste and aesthetic sense, which made his crafts distinguished by accuracy, innovation and renewal, and achieved wide fame for them all over the world.
				
			</p>
            </div>
            <div class="clearfix visible-md-block visible-sm-block"></div>
            <div class="col-lg-4 col-sm-6 col-ex-12 about-item wow lightSpeedIn" data-wow-offset="200">
              <span class="fa fa-file"></span>
              <h2>The goal of creating a Dama Store</h2>
              <p class="lead">
				Because the handicraft market is one of the most frequent places for people and tourists in Damascus, they love these products that come out of the hands of those craftsmen, who create them with love, conveying through them the nobility and history of successive civilizations that passed over one city.
				Our goal of this store was to convey what tourists love and search for and enjoy when they come to Damascus to be within their reach everywhere in the world, as if they were making a trip inside a Damascus market, but in a modern, modern style.
				
				We are happy to get what you love and to impress you All that is available in our store, and we are pleased to know your opinions about what we offer and about things that you want to be in the store
				
				</p>
            </div>

          </div>

        </div>
      </section>
	  @endif
@endsection



@section('script')

@endsection
