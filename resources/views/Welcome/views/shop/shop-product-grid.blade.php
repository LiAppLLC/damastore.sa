@extends('Welcome.views.home')
@section('style')

<style>
	.disabled{
		width: 40px;
		height: 40px;
		text-align: center;
		display: inline-block;
		font-size: 17px;
		color: #5c6873;
		font-weight: 400;
		padding: 7px 4px 6px 4px;
		border: 1px solid #eeeeee;
		border-color: #e2e2e2;
		background: #e2e2e2;
		color: #fff;
	}
	.disabled:hover{
		border-color: #bebebe;
		background: #bebebe;
		color: #fff;
	}
    .c-prev{
        cursor: pointer;
    }
	.pagination-btn{
		width: 40px;
		height: 40px;
		text-align: center;
		display: inline-block;
		font-size: 17px;
		color: #5c6873;
		font-weight: 400;
		padding: 7px 4px 6px 4px;
		border: 1px solid #eeeeee;
	}

	.pagination-btn:hover{
		border-color: #f99d1c;
		background: #f99d1c;
		color: #fff;
	}

	.c-active{
		border-color: #f99d1c;
		background: #f99d1c;
		color: #fff;
	}

    .active-btn{
        color: #ffffff;
        background: #8998b5;
        border-color: #8998b5;
    }

    .right-card{
        margin-top: 10px;
        margin-bottom: 10px;
        border-right: 1px solid #f99d1c7d;
        width: 65%;
        border-bottom: 1px solid #f99d1c7d;
        border-top: 1px solid #f99d1c7d;
        padding: 10px;
    }

    .left-card{
        margin-top: 10px;
        margin-bottom: 10px;
        border-left: 1px solid #f99d1c7d;
        /*width: 65%;*/
        border-bottom: 1px solid #f99d1c7d;
        border-top: 1px solid #f99d1c7d;
        /*padding: 10px;*/
    }

    @media (max-width: 990px) {
        .right-card{
            margin-top: -11px;
            margin-bottom: 10px;
            border-right: 1px solid #f99d1c7d;
            width: 100%;
            border-bottom: 1px solid #f99d1c7d;
            border-left: 1px solid #f99d1c7d;
            padding: 10px;
        }

        .left-card{
            margin-top: 10px;
            margin-bottom: 10px;
            border-left: 1px solid #f99d1c7d;
            width: 100%;
            border-right: 1px solid #f99d1c7d;
            border-top: 1px solid #f99d1c7d;
            /*padding: 10px;*/
        }
    }

    .c-danger-btn{
        color: #ffffff;
        background: #d23232;
        border-color: #d23232;
        display: none;
    }

    .qty_btn_rem_none{
        display: none;
    }
    .c-content-product-2 .btn + .btn {
        margin-left: 0;
    }

    .qty_btn_rem2{
        color: #ffffff !important;
        background: #d23232;
        border-color: #d23232;
    }
</style>

@endsection

@section('home_breadcrumb_title')
<i class="fas fa-shopping-cart"></i> @lang('home.Shop')
@endsection

@section('home_content')

    <?php
    $locale = App::getLocale();
    ?>


    @if( $locale=="ar")
        <style>
            .c-layout-sidebar-menu {
                direction: rtl;
            }


        </style>
    @endif
<!-- BEGIN: PAGE CONTENT -->
<!-- BEGIN: CONTENT/SHOPS/SHOP-RESULT-FILTER-1 -->
<div class="row">
    <div class="col-md-6">
<div class="c-shop-result-filter-1 clearfix form-inline">
	<div class="c-filter">
		{{-- <div class="btn-group">
			<button type="button" id="list-btn" class="btn btn-default"><i class="fa fa-list" aria-hidden="true"></i></button>
			<button type="button" id="grid-btn" class="btn btn-default active-btn"><i class="fa fa-th"></i></button>
		</div> --}}
	</div>
</div>
</div>
<div class="col-md-9">
<!-- END: CONTENT/SHOPS/SHOP-RESULT-FILTER-1 -->

{{-- <div class="c-margin-t-20"></div> --}}
<!-- BEGIN: CONTENT/SHOPS/SHOP-2-7 -->
<div class="c-bs-grid-small-space">
	<div class="row" id="products-grid" ></div>
    <div class="row c-margin-b-40" style="display: none;" id="products-list"></div>
</div>
</div>
</div>
<!-- END: CONTENT/SHOPS/SHOP-2-7 -->

{{-- <div class="c-margin-t-20"></div> --}}

<ul class="c-content-pagination c-square c-theme pull-right" id="pagination-div"></ul>
    <!-- END: PAGE CONTENT -->
			{{--</div>--}}

	<!-- END: PAGE CONTAINER -->



@endsection



@section('script')


	{{--<script>--}}


            var userObject = JSON.parse(localStorage.getItem("DamaStore_userObject"));

            // blocked
            // bornDate
            // created_at
            // email
            // emailVerifiedAt
            // files
            // firstName
            // id
            // lastActivity
            // lastLoggedIn
            // lastName
            // phone
            // reputation
            // roles
            // token
            // updated_at
            // username
            // verify




	var lenght = 1;
	var pageSize = 8;
	var selectedPage = 1;
    var localstorageVar = 'Product_page';

	function getData(pageIndex, filter) {

                $.ajax({

                    type: "POST",
                    url: '/api/ProductList/' + pageIndex + '/' + pageSize,
                    data: filter,
                    // mimeType: 'application/json',
                    processData: false,
                    contentType: false,
                    headers: {"currency":"{{session('currency')}}",
                    "language":"{{session('lang')}}"},
                    success: function (res) {
                        $('#priceRange').text('Price Range: ' + res.data.minimumVal + ' - ' + res.data.maximumVal);
                        $('#from-input').attr('min', res.data.minimumVal);
                        $('#from-input').attr('max', res.data.maximumVal);
                        $('#to-input').attr('min', res.data.minimumVal);
                        $('#to-input').attr('max', res.data.maximumVal);
                        $('.input-group-addon').html('{{session('currency')}}');

						$('#products-grid').empty();
						$('#products-list').empty();
                        lenght = changeLength(res.data.totalCount);

                        res.data.data.forEach(function (item, index, arr) {
                            var image;
                            if (item.image) {
                                image ='/images/' + item.image.path;
                            } else {
                                image = 'https://static.thenounproject.com/png/2999524-200.png';
                            }

                            var date = new Date(item.created_at),
                                yr = date.getFullYear(),
                                month = date.getMonth() < 10 ? '0' + date.getMonth() : date.getMonth(),
                                day = date.getDate() < 10 ? '0' + date.getDate() : date.getDate(),
                                newDate = yr + '-' + month + '-' + day;

                            var price = '';
                            var sale_grid = '';
                            if(item.price_sale){
                                sale_grid = '<div class="c-label c-bg-red c-font-uppercase c-font-white c-font-13 c-font-bold">@lang('home.Sale')</div>';
                                price = '<p class="c-price c-font-14 c-font-slim">' + item.price + ' <span class="c-font-14 c-font-line-through c-font-red">' + item.price_sale + '</span></p>';
                            }else{
                                price = '<p class="c-price c-font-14 c-font-slim">' + item.price + '</p>';
                            }
                            $('#products-grid').append('<div class="col-md-3 col-sm-6 c-margin-b-20">' +
                                '<div class="c-content-product-2 c-bg-white c-border">' +
                                '<div class="c-content-overlay">' + sale_grid +
                                '<div class="c-overlay-wrapper">' +
                                '<div class="c-overlay-content">' +
                                '<a href="/productDetails/' + item.id + '" class="navigation btn btn-md c-btn-grey-1 c-btn-uppercase c-btn-bold c-btn-border-1x c-btn-square">@lang('home.Details')</a>' +
                                '</div>' +
                                '</div>' +
                                '<div class="c-bg-img-center c-overlay-object" data-height="height" style="height: 230px; background-image: url(' + image + ');"></div>' +
                                '</div>' +
                                '<div class="c-info" style="height: 100px">\n' +
                                    @if(app()->getLocale()=='ar')
                                '<p class="c-title c-font-16 c-font-slim">' + item.ar_name + '</p>' + price +
                                @else
                                '<p class="c-title c-font-16 c-font-slim">' + item.name + '</p>' + price +
                                @endif
                                '</div>' +
                                '<div class="btn-group btn-group-justified" role="group">' +
                                '<div class="btn-group c-border-top" role="group" id="quantity_input2_div_' + item.id + '">' +
                                '<input class="form-control" type="number" value="1" id="quantity_input2_' + item.id + '" name="quantity_input2">' +
                                '</div>' +
                                '<div class="btn-group c-border-left c-border-top" role="group">' +
                                '<button class="btn btn-sm c-btn-white c-btn-uppercase c-btn-square c-font-grey-3 c-font-white-hover c-bg-red-2-hover c-btn-product qty_btn2" data-id="' + item.id + '" id="qty_btn2_' + item.id + '">@lang('home.Add Cart')</button>' +
                                {{--'</div>' +--}}
                                {{--'<div class="btn-group c-border-left c-border-top" role="group">' +--}}
                                '<button class="btn btn-sm c-btn-white c-btn-uppercase c-btn-square c-font-grey-3 c-font-white-hover c-bg-red-2-hover c-btn-product qty_btn_rem2 qty_btn_rem_none" style="margin:0 !important;" data-id="' + item.id + '" id="qty_btn_rem2_' + item.id + '">@lang('home.Remove From Cart')</button>' +
                                '</div>' +
                                '</div>' +
                                '</div>' +
                                '</div>');


                                var price = '';
                                var sale_list = '';
                                if(item.price_sale){
                                    sale_list = '<div class="c-label c-bg-red c-font-uppercase c-font-white c-font-13 c-font-bold">@lang('home.Sale')</div>';
                                    price = '<p class="c-price c-font-26 c-font-thin">' + item.price_sale + ' <span class="c-font-26 c-font-line-through c-font-red">' + item.price + '</span></p>';
                                }else{
                                    price = '<p class="c-price c-font-26 c-font-thin">' + item.price + '</p>';
                                }
                            $('#products-list').append('<div class="c-content-product-2 c-bg-white" style="border: none;">\n' +
                                '    <div class="col-md-4 col-xs-12 col-sm-12 left-card">\n' +
                                '        <div class="c-content-overlay">\n' + sale_list +
                                '            <div class="c-overlay-wrapper">\n' +
                                '                <div class="c-overlay-content">\n' +
                                '                    <a href="productDetails/' + item.id + '" class="navigation btn btn-md c-btn-grey-1 c-btn-uppercase c-btn-bold c-btn-border-1x c-btn-square">@lang('home.Details')</a>\n' +
                                '                </div>\n' +
                                '            </div>\n' +
                                '            <div class="c-bg-img-center c-overlay-object" data-height="height" style="height: 176.5px; background-image: url(' + image + ');"></div>\n' +
                                '        </div>\n' +
                                '    </div>\n' +
                                '    <div class="col-md-8 col-xs-12 col-sm-12 right-card" style="height:176.5px">\n' +
                                '        <div class="c-info-list">\n' +
                                '            <h3 class="c-title c-font-bold c-font-22 c-font-dark">\n' +
                                '                <a class="c-theme-link" href="navigation shop-product-details-2.html">' + item.name + '</a>\n' +
                                '            </h3>\n' +
                                '            <div class="c-desc c-font-16 c-font-thin" style="height:30px;overflow:hidden;">'+ item.description +'</div>\n' + price +
                                '        </div>\n' +
                                '        <div class="row">\n' +
                                '           <div class="col-md-3"> ' +
                                '           <input class="form-control" type="number" value="1" id="quantity_input_' + item.id + '" name="quantity_input"> ' +
                                '            <button type="submit" class="btn btn-sm c-danger-btn c-btn-square c-btn-uppercase c-btn-bold qty_btn_rem" data-id="' + item.id + '" id="qty_btn_rem_' + item.id + '">\n' +
                                '                <i class="fa fa-shopping-cart"></i>@lang('home.Remove From Cart')\n' +
                                '            </button>\n' +
                                '           </div> ' +
                                '           <div class="col-md-3"> ' +
                                '            <button type="submit" class="btn btn-sm c-theme-btn c-btn-square c-btn-uppercase c-btn-bold qty_btn" data-id="' + item.id + '" id="qty_btn_' + item.id + '">\n' +
                                '                <i class="fa fa-shopping-cart"></i>@lang('home.Add Cart')\n' +
                                '            </button>\n' +

                                '           </div> ' +
                                {{--'            <button type="submit" class="btn btn-sm btn-default c-btn-square c-btn-uppercase c-btn-bold">\n' +--}}
                                {{--'                <i class="fa fa-heart-o"></i>Add Wishlist\n' +--}}
                                {{--'            </button>\n' +--}}
                                '        </div>\n' +
                                '    </div>\n' +
                                '    <div class="clearfix"></div>  ' +
                                '</div>');

                            redOrBlueBtn(item.id, 'damastore_cart', 'quantity_input_', 'qty_btn_', 'qty_btn_rem_', 'quantity_input2_div_', 'qty_btn2_', 'qty_btn_rem2_');


                        });

                        {{-- console.log(res.data.all_products); --}}
                        var all_products =res.data.all_products
                        function createB(all_products){
                            $('#pagination-div').empty();
                            var pages = all_products / 8;
                            var nump =parseInt(res.data.pageIndex);
                            var validition=parseInt(res.data.pageIndex);
                            console.log("pageIndex"+nump);
                            var start="";
                            if(nump==0){
                            start = '<li class="c-prev pagination-btn clickable-btn" data-id="1"><i class="fa fa-angle-left"></i></i></li>';
                            }else{
                            start = '<li class="c-prev pagination-btn clickable-btn" data-id="'+(nump)+'"><i class="fa fa-angle-left"></i></i></li>';
                            }
                            $('#pagination-div').append(start);
                            var p=0;
                            var p = nump +3;
                            for(var i=nump;p>i;i++){
                                if(p-2 >= pages){
                                    return false;
                                }
                                if(validition==i){
                                    before = '<li class="c-prev c-active pagination-btn clickable-btn" data-id="'+(i+1)+'">'+(i+1)+'</i></li>';
                                    $('#pagination-div').append(before);
                                }else{
                                    before = '<li class="c-prev pagination-btn clickable-btn" data-id="'+(i+1)+'">'+(i+1)+'</i></li>';
                                    $('#pagination-div').append(before);
                                }
                            }
                            var end = '<li class="c-prev pagination-btn clickable-btn" data-id="'+(p-1)+'"><i class="fa fa-angle-right"></i></i></li>';
                            $('#pagination-div').append(end);
                        }
                        createB(all_products);

                        //console.log('hey');

						{{-- createB1tnList(); --}}

                    },
                    error: function (res, response) {
                    },
                });
            }



    var formDataInit = new FormData();
    formDataInit.append('sale', true);
    if($('#category-list').val() != -1){
        formDataInit.append('category', $('#category-list').val());
    }
    if($('#type-list').val() != -1){
        formDataInit.append('type', $('#type-list').val());
    }
    $('#checkbox-sidebar-3-1').click();
    getData(0, formDataInit);

	$(document).on("click", ".clickable-btn", function(e) {
        selectedPage = changePage($(this).data('id'), selectedPage, localstorageVar);
	});



	$(document).on("click", ".qty_btn", function(e) {
        //if(userObject){
            var id = $(this).data('id');
            var qty = $('#quantity_input_' + id).val();
            AddToCart(id, qty, 'damastore_cart', 'quantity_input_', 'qty_btn_', 'qty_btn_rem_', 'quantity_input2_div_', 'qty_btn2_', 'qty_btn_rem2_');
        //}else{
        //    Swal.fire({
        //        icon: 'error',
        //        title: 'Oops...',
        //        text: 'you should be logged in',
        //    });
        //}
	});

    $(document).on("click", ".qty_btn_rem", function(e) {
        //if(userObject){
            var id = $(this).data('id');
            removeFromCart(id, 'damastore_cart', 'quantity_input_', 'qty_btn_', 'qty_btn_rem_', 'quantity_input2_div_', 'qty_btn2_', 'qty_btn_rem2_');
        //}else{
        //    Swal.fire({
        //        icon: 'error',
        //        title: 'Oops...',
        //        text: 'you should be logged in',
        //    });
        //}
    });

	$(document).on("click", ".qty_btn2", function(e) {
        //if(userObject){
            var id = $(this).data('id');
            var qty = $('#quantity_input2_' + id).val();
            AddToCart(id, qty, 'damastore_cart', 'quantity_input2_div_', 'qty_btn2_', 'qty_btn_rem2_', 'quantity_input_', 'qty_btn_', 'qty_btn_rem_');
        //}else{
        //    Swal.fire({
        //        icon: 'error',
        //        title: 'Oops...',
        //        text: 'you should be logged in',
        //    });
        //}
	});

    $(document).on("click", ".qty_btn_rem2", function(e) {
        //if(userObject){
            var id = $(this).data('id');
            removeFromCart(id, 'damastore_cart', 'quantity_input2_div_', 'qty_btn2_', 'qty_btn_rem2_', 'quantity_input_', 'qty_btn_', 'qty_btn_rem_');
        //}else{
        //    Swal.fire({
        //        icon: 'error',
        //        title: 'Oops...',
        //        text: 'you should be logged in',
        //    });
        //}
    });

    $('#list-btn').click(function(){
        //alert('list');
        $('#list-btn').addClass('active-btn');
        $('#grid-btn').removeClass('active-btn');
        $('#products-grid').hide();
        $('#products-list').show();
    });

    $('#grid-btn').click(function(){
        //alert('grid');
        $('#list-btn').removeClass('active-btn');
        $('#grid-btn').addClass('active-btn');
        $('#products-list').hide();
        $('#products-grid').show();
    });


    $.ajax({

        type: "POST",
        url: '{{route('category')}}',
        data: null,
        // mimeType: 'application/json',
        {{-- headers: {"Authorization": 'Bearer ' + userObject.token}, --}}
        processData: false,
        contentType: false,
        success: function (res) {
            if(res.status == 200) {
                //console.log('category', res);
                res.data.forEach(function(item, index, arr) {
                    $('#category-list').append('<option value="' + item.id + '">' + item.name + '</option>');
                    //if(index == 0){
                    //    $('#category-list').append('<option selected="selected" value="' + item.id + '">' + item.name + '</option>');
                    //}else {
                    //    $('#category-list').append('<option value="' + item.id + '">' + item.name + '</option>');
                    //}
                });
                {{-- get_type_1(res.data[0].id); --}}
            }
        },
        error: function (res, response) {
        },
    });



    $('#ProductFilterBtn').click(function (e) {

        var formData = new FormData();
        //formData.append('user_id', userObject.id);
        if($('#category-list').val() != -1){
            formData.append('category', $('#category-list').val());
        }
        if($('#type-list').val() != -1){
            formData.append('type', $('#type-list').val());
        }
        formData.append('sale', $('#checkbox-sidebar-3-1').prop("checked"));
        formData.append('min_num', $('#from-input').val());
        formData.append('max_num', $('#to-input').val());

        getData(0, formData);

    });

    getData(0);
	// ===============================================================

	{{--</script>--}}

@endsection
<script>
    function get_type_1(id) {
        let userObject = JSON.parse(localStorage.getItem("DamaStore_userObject"));
        let formData = new FormData();
        formData.append('category_id', id)
        $('#type-list').html('');
        $.ajax({
            type: "POST",
            url: '{{route('Type')}}',
            data: formData,
            // mimeType: 'application/json',
            // headers: {"Authorization": 'Bearer ' + userObject.token},
            processData: false,
            contentType: false,
            success: function (res) {
                if (res.status == 200) {

                    res.data.forEach(function (item, index, arr) {
                        if (index == 0) {
                            $('#type-list').append('<option selected="selected" value="' + item.id + '">' + item.name + '</option>');
                        } else {
                            $('#type-list').append('<option value="' + item.id + '">' + item.name + '</option>');
                        }
                    });
                }
            },
            error: function (res, response) {
                // $( ".loading" ).remove();
            },
        });

    }
</script>
