@extends('Welcome.views.home1')
@section('style')
<style>
    .responsive-map{
overflow: hidden;
padding-bottom:56.25%;
position:relative;
height:0;
}
.responsive-map iframe{
left:0;
top:0;
height:100%;
width:100%;
position:absolute;
}
    .contact-info{
    width: 100%;
    display: flex;
    justify-content: center;
    flex-wrap: wrap;
}

.card{
    position: relative;
    flex: 1;
    max-width: 300px;
    height: 140px;
    background-color: rgb(245, 245, 245);
    margin: 20px;
    border:1px solid goldenrod;
    cursor: pointer;
    border-radius:15px;
    display: flex;
    align-items: center;
    justify-content: center;
}

.icon{
    font-size: 50px;
    color: #ffb62f;
    transition: .3s linear;
}

.card:hover .icon{
    transform: scale(4);
    opacity: 0;
}

.card-content h3,
.card-content span{
    position: absolute;
    left: 50%;
    transform: translateX(-50%);
    font-size: 16px;
    opacity: 0;
}

.card-content h3{
    top: 20px;
    text-transform: uppercase;
    color: #353535;
}

.card-content span{
    bottom: 20px;
    color: goldenrod;
    font-weight: 500;
}

.card:hover h3{
    opacity: 1;
    /* top: 46px; */
    transition: .3s linear .3s;
}

.card:hover span{
    opacity: 1;
    /* bottom: 46px; */
    width: 250px;
    text-align: center;
    transition: .3s linear .3s;
}


@media screen and (max-width:900px){
    .card{
        flex: 100%;
        max-width: 500px;
    }
}
</style>
@endsection

{{-- @section('home_breadcrumb_title')
    @lang('home.Shop')
@endsection --}}

@section('home_content')

    <?php
    $locale = App::getLocale();
    ?>


    @if( $locale=="ar")
        <style>
            .c-layout-sidebar-menu {
                direction: rtl;
            }


        </style>
    @endif
    <div style="width: 100%;text-align:center;">
        <h1>Contact</h1>
    </div>

    <div class="contact-info">
        <div class="card">
          <i class="icon fas fa-envelope"></i>
          <div class="card-content">
            <h3>Email</h3>
            <span>Customer-service@li-app.com</span>
          </div>
        </div>

        <div class="card">
          <i class="icon fas fa-phone"></i>
          <div class="card-content">
            <h3>Phone Number</h3>
            <span> +1 (201) 7332228</span>
          </div>
        </div>

        <div class="card">
          <i class="icon fas fa-map-marker-alt"></i>
          <div class="card-content">
            <h3>Location</h3>
            <span>United States Of America</span>
          </div>
        </div>
      </div>

      <div class="responsive-map">
        <iframe src="https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d6136.79044545372!2d-75.5798916!3d39.7307722!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x89c6fd80eac5fe85%3A0x32121fb035784a0c!2s3%20Germay%20Dr%20Unit%204%20%231541%2C%20Wilmington%2C%20DE%2019804%2C%20USA!5e0!3m2!1sen!2snl!4v1615583224153!5m2!1sen!2snl" width="600" height="450" frameborder="0" style="border:0" allowfullscreen></iframe>
        </div>
@endsection



@section('script')

@endsection
