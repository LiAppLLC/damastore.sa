@extends('Welcome.views.home1')
@section('style')

@endsection
{{--
@section('home_breadcrumb_title')
    @lang('home.Shop')
@endsection --}}

@section('home_content')

    <?php
    $locale = App::getLocale();
    ?>


    @if( $locale=="ar")
        <style>
            .c-layout-sidebar-menu {
                direction: rtl;
            }


        </style>
    @endif
    <style>
        span{
                
                @if(app()->getLocale()==="ar")
             font-family: Tajawal !important;
            @else
            font-family: 'ahronbd' !important;
            @endif
            font-weight: 800 !important;
            }
        </style>

    @if( $locale=="ar")

<span id="docs-internal-guid-d19ad86b-7fff-7292-3abe-ce7cb27d03dd"><p dir="rtl" style="line-height:1.7999999999999998;text-align: right;margin-top:0pt;margin-bottom:8pt;"><span style="font-size:16pt;font-family:Calibri,sans-serif;color:#ff9933;background-color:transparent;font-weight:700;font-style:normal;font-variant:normal;text-decoration:none;vertical-align:baseline;white-space:pre;white-space:pre-wrap;">سياسة الإسترجاع والإستبدال</span></p><p dir="rtl" style="line-height:1.7999999999999998;text-align: right;margin-top:0pt;margin-bottom:8pt;"><span style="font-size:12pt;font-family:Calibri,sans-serif;color:#000000;background-color:transparent;font-weight:700;font-style:normal;font-variant:normal;text-decoration:none;vertical-align:baseline;white-space:pre;white-space:pre-wrap;">نشكر اختياركم متجرنا لشراء منتجاتكم المفضلة ويسعدنا أن نقوم بخدمتكم.&nbsp; في بعض الأحيان ربما يتغير رأيك في ما يتعلق بالمنتجات التي قمت بشرائها، لا عليك. فنحن في داما ستور سنبذل ما في وسعنا لتتم عملية الإسترجاع أو الإستبدال بمنتهى البساطة و السلاسة بقدر الإمكان.</span></p><ol style="margin-top:0;margin-bottom:0;padding-inline-start:48px;"><li dir="rtl" style="list-style-type:decimal;font-size:12pt;font-family:Calibri,sans-serif;color:#ff9933;background-color:transparent;font-weight:700;font-style:normal;font-variant:normal;text-decoration:underline;-webkit-text-decoration-skip:none;text-decoration-skip-ink:none;vertical-align:baseline;white-space:pre;margin-left: -10.95pt;" aria-level="1"><p dir="rtl" style="line-height:2.4;text-align: right;margin-top:0pt;margin-bottom:15pt;" role="presentation"><span style="font-size:12pt;font-family:Calibri,sans-serif;color:#ff9933;background-color:transparent;font-weight:700;font-style:normal;font-variant:normal;text-decoration:underline;-webkit-text-decoration-skip:none;text-decoration-skip-ink:none;vertical-align:baseline;white-space:pre;white-space:pre-wrap;">&nbsp;سياسة&nbsp;الفترة&nbsp;الزمنية&nbsp;لاسترجاع&nbsp;أو&nbsp;استبدال&nbsp;المشتريات:</span></p></li></ol><p dir="ltr" style="line-height:2.4;text-align: right;background-color:#ffffff;margin-top:0pt;margin-bottom:0pt;"><span style="font-size:12pt;font-family:Calibri,sans-serif;color:#ff9933;background-color:transparent;font-weight:700;font-style:normal;font-variant:normal;text-decoration:underline;-webkit-text-decoration-skip:none;text-decoration-skip-ink:none;vertical-align:baseline;white-space:pre;white-space:pre-wrap;">ماهي شروط الاستبدال والاسترجاع؟</span></p><ol style="margin-top:0;margin-bottom:0;padding-inline-start:48px;"><li dir="rtl" style="list-style-type:decimal;font-size:11pt;font-family:Calibri,sans-serif;color:#333333;background-color:transparent;font-weight:400;font-style:normal;font-variant:normal;text-decoration:none;vertical-align:baseline;white-space:pre;margin-left: -10.95pt;" aria-level="1"><p dir="rtl" style="line-height:2.4;text-align: right;margin-top:14pt;margin-bottom:0pt;" role="presentation"><span style="font-size:11pt;font-family:Calibri,sans-serif;color:#333333;background-color:transparent;font-weight:400;font-style:normal;font-variant:normal;text-decoration:none;vertical-align:baseline;white-space:pre;white-space:pre-wrap;">عند الرغبة في استبدال أو استرجاع المنتج عليك التواصل معنا أولاً عبر البريد الالكتروني الموضح بالموقع&nbsp; .</span></p></li><li dir="rtl" style="list-style-type:decimal;font-size:11pt;font-family:Calibri,sans-serif;color:#333333;background-color:transparent;font-weight:400;font-style:normal;font-variant:normal;text-decoration:none;vertical-align:baseline;white-space:pre;margin-left: -10.95pt;" aria-level="1"><p dir="rtl" style="line-height:2.4;text-align: right;margin-top:0pt;margin-bottom:0pt;" role="presentation"><span style="font-size:11pt;font-family:Calibri,sans-serif;color:#333333;background-color:transparent;font-weight:400;font-style:normal;font-variant:normal;text-decoration:none;vertical-align:baseline;white-space:pre;white-space:pre-wrap;">يحق للعميل استرجاع المنتج خلال 3 أيام من تاريخ الاستلام، بشرط أن يكون المنتج سليما لم يطرأ عليه أي تغيير،&nbsp;</span></p></li><li dir="rtl" style="list-style-type:decimal;font-size:11pt;font-family:Calibri,sans-serif;color:#333333;background-color:transparent;font-weight:400;font-style:normal;font-variant:normal;text-decoration:none;vertical-align:baseline;white-space:pre;margin-left: -10.95pt;" aria-level="1"><p dir="rtl" style="line-height:2.4;text-align: right;margin-top:0pt;margin-bottom:0pt;" role="presentation"><span style="font-size:11pt;font-family:Calibri,sans-serif;color:#333333;background-color:transparent;font-weight:400;font-style:normal;font-variant:normal;text-decoration:none;vertical-align:baseline;white-space:pre;white-space:pre-wrap;">يحق للعميل استبدال المنتج بمنتج آخر،&nbsp;وذلك قبل إرسال الشحنة.&nbsp;وبعد إرسال الشحنة يحق له الاستبدال خلال 14 أيام عمل، وعليه تحمل تكلفة الشحن كاملا وفرق تكلفة المنتج إن وجد.</span></p></li><li dir="rtl" style="list-style-type:decimal;font-size:11pt;font-family:Calibri,sans-serif;color:#333333;background-color:transparent;font-weight:400;font-style:normal;font-variant:normal;text-decoration:none;vertical-align:baseline;white-space:pre;margin-left: -10.95pt;" aria-level="1"><p dir="rtl" style="line-height:2.4;text-align: right;margin-top:0pt;margin-bottom:0pt;" role="presentation"><span style="font-size:11pt;font-family:Calibri,sans-serif;color:#333333;background-color:transparent;font-weight:400;font-style:normal;font-variant:normal;text-decoration:none;vertical-align:baseline;white-space:pre;white-space:pre-wrap;">أن يكون المنتج غير مستخدم وبحالته الأصلية وكافة البطاقات الملصقة غير منزوعة .</span></p></li><li dir="rtl" style="list-style-type:decimal;font-size:11pt;font-family:Calibri,sans-serif;color:#333333;background-color:transparent;font-weight:400;font-style:normal;font-variant:normal;text-decoration:none;vertical-align:baseline;white-space:pre;margin-left: -10.95pt;" aria-level="1"><p dir="rtl" style="line-height:2.4;text-align: right;margin-top:0pt;margin-bottom:0pt;" role="presentation"><span style="font-size:11pt;font-family:Calibri,sans-serif;color:#333333;background-color:transparent;font-weight:400;font-style:normal;font-variant:normal;text-decoration:none;vertical-align:baseline;white-space:pre;white-space:pre-wrap;">*أن يتم ارجاع المنتج مع صندوق الماركة غير مفتوح وان يكون سليم غير متضرر .</span></p></li><li dir="rtl" style="list-style-type:decimal;font-size:11pt;font-family:Calibri,sans-serif;color:#333333;background-color:transparent;font-weight:400;font-style:normal;font-variant:normal;text-decoration:none;vertical-align:baseline;white-space:pre;margin-left: -10.95pt;" aria-level="1"><p dir="rtl" style="line-height:2.4;text-align: right;margin-top:0pt;margin-bottom:14pt;" role="presentation"><span style="font-size:11pt;font-family:Calibri,sans-serif;color:#333333;background-color:transparent;font-weight:400;font-style:normal;font-variant:normal;text-decoration:none;vertical-align:baseline;white-space:pre;white-space:pre-wrap;">إذا كان المنتج معيبا، أو غير مطابق للمواصفات التي تم تحديدها وقت الشراء يحق للعميل استبدال المنتج أو استرجاعه خلال 14 يوما، وعلى العميل في هذه الحالة إرسال ما يثبت تكاليف الشحن موضح فيه: رقم الارسالية – الشحن – والتكلفة. فيحق للعميل عندئذ استرجاع المبلغ كاملا.</span></p></li></ol><p dir="rtl" style="line-height:2.4;margin-right: 7.05pt;text-align: right;background-color:#ffffff;margin-top:0pt;margin-bottom:0pt;"><span style="font-size:12pt;font-family:Calibri,sans-serif;color:#ff9933;background-color:transparent;font-weight:700;font-style:normal;font-variant:normal;text-decoration:underline;-webkit-text-decoration-skip:none;text-decoration-skip-ink:none;vertical-align:baseline;white-space:pre;white-space:pre-wrap;">ماهي المنتجات الغير قابلة للاستبدال والاسترجاع؟</span></p><p dir="rtl" style="line-height:2.4;text-align: right;background-color:#ffffff;margin-top:0pt;margin-bottom:0pt;"><span style="font-size:11pt;font-family:Calibri,sans-serif;color:#333333;background-color:transparent;font-weight:400;font-style:normal;font-variant:normal;text-decoration:none;vertical-align:baseline;white-space:pre;white-space:pre-wrap;">لا يحق للمستهلك استبدال واسترجاع المنتجات التالية:</span></p><ol style="margin-top:0;margin-bottom:0;padding-inline-start:48px;"><li dir="rtl" style="list-style-type:decimal;font-size:11pt;font-family:Calibri,sans-serif;color:#333333;background-color:transparent;font-weight:400;font-style:normal;font-variant:normal;text-decoration:none;vertical-align:baseline;white-space:pre;" aria-level="1"><p dir="rtl" style="line-height:2.4;text-align: right;background-color:#ffffff;margin-top:0pt;margin-bottom:0pt;" role="presentation"><span style="font-size:11pt;font-family:Calibri,sans-serif;color:#333333;background-color:transparent;font-weight:400;font-style:normal;font-variant:normal;text-decoration:none;vertical-align:baseline;white-space:pre;white-space:pre-wrap;">بعد مضي 3 أيام من تاريخ الاستلام، ولا يقبل الاستبدال بعد 14 يوم.</span></p></li><li dir="rtl" style="list-style-type:decimal;font-size:11pt;font-family:Calibri,sans-serif;color:#333333;background-color:transparent;font-weight:400;font-style:normal;font-variant:normal;text-decoration:none;vertical-align:baseline;white-space:pre;" aria-level="1"><p dir="rtl" style="line-height:2.4;text-align: right;background-color:#ffffff;margin-top:0pt;margin-bottom:0pt;" role="presentation"><span style="font-size:11pt;font-family:Calibri,sans-serif;color:#333333;background-color:transparent;font-weight:400;font-style:normal;font-variant:normal;text-decoration:none;vertical-align:baseline;white-space:pre;white-space:pre-wrap;">عند تغير المنتج عن حالته الأصلية لأي سبب من الاسباب.&nbsp;</span></p></li><li dir="rtl" style="list-style-type:decimal;font-size:11pt;font-family:Calibri,sans-serif;color:#333333;background-color:transparent;font-weight:400;font-style:normal;font-variant:normal;text-decoration:none;vertical-align:baseline;white-space:pre;" aria-level="1"><p dir="rtl" style="line-height:2.4;text-align: right;background-color:#ffffff;margin-top:0pt;margin-bottom:0pt;" role="presentation"><span style="font-size:11pt;font-family:Calibri,sans-serif;color:#333333;background-color:transparent;font-weight:400;font-style:normal;font-variant:normal;text-decoration:none;vertical-align:baseline;white-space:pre;white-space:pre-wrap;">المنتجات التي تم تصنيعها بطلب من العميل وفقا لمواصفات حددها.</span></p></li><li dir="rtl" style="list-style-type:decimal;font-size:11pt;font-family:Calibri,sans-serif;color:#333333;background-color:transparent;font-weight:400;font-style:normal;font-variant:normal;text-decoration:none;vertical-align:baseline;white-space:pre;" aria-level="1"><p dir="rtl" style="line-height:2.4;text-align: right;background-color:#ffffff;margin-top:0pt;margin-bottom:0pt;" role="presentation"><span style="font-size:11pt;font-family:Calibri,sans-serif;color:#000000;background-color:transparent;font-weight:400;font-style:normal;font-variant:normal;text-decoration:none;vertical-align:baseline;white-space:pre;white-space:pre-wrap;">&nbsp;الأعطال والاضرار الناتجة عن سوء الاستخدام.</span></p></li></ol><p dir="ltr" style="line-height:1.7999999999999998;text-align: justify;background-color:#ffffff;margin-top:0pt;margin-bottom:0pt;"><b style="font-weight:normal;" id="docs-internal-guid-e4948ca2-7fff-8366-5a11-c5aa4d82d148"><br></b></p><ol style="margin-top:0;margin-bottom:0;padding-inline-start:48px;" start="5"><li dir="rtl" style="list-style-type:decimal;font-size:11pt;font-family:Calibri,sans-serif;color:#000000;background-color:transparent;font-weight:400;font-style:normal;font-variant:normal;text-decoration:none;vertical-align:baseline;white-space:pre;" aria-level="1"><p dir="rtl" style="line-height:2.4;text-align: right;margin-top:0pt;margin-bottom:8pt;" role="presentation"><span style="font-size:11pt;font-family:Calibri,sans-serif;color:#000000;background-color:transparent;font-weight:400;font-style:normal;font-variant:normal;text-decoration:none;vertical-align:baseline;white-space:pre;white-space:pre-wrap;">عدم مطابقة الأرقام المتسلسلة للطلبية أو أي مظهر مختلف بالمقارنة مع المنتج عند استلامه خلال عملية الشراء و كما هو مدون في فاتورة الشراء.&nbsp;</span></p></li></ol><p dir="rtl" style="line-height:2.4;margin-right: 7.05pt;text-align: right;margin-top:0pt;margin-bottom:8pt;"><span style="font-size:12pt;font-family:Calibri,sans-serif;color:#ff9933;background-color:transparent;font-weight:700;font-style:normal;font-variant:normal;text-decoration:underline;-webkit-text-decoration-skip:none;text-decoration-skip-ink:none;vertical-align:baseline;white-space:pre;white-space:pre-wrap;">من يتحمل تكاليف الارجاع:</span></p><ol style="margin-top:0;margin-bottom:0;padding-inline-start:48px;"><li dir="rtl" style="list-style-type:decimal;font-size:11pt;font-family:Calibri,sans-serif;color:#1e1e1e;background-color:transparent;font-weight:400;font-style:normal;font-variant:normal;text-decoration:none;vertical-align:baseline;white-space:pre;margin-left: 10.350000000000001pt;" aria-level="2"><p dir="rtl" style="line-height:2.4;text-align: right;margin-top:0pt;margin-bottom:0pt;" role="presentation"><span style="font-size:11pt;font-family:Calibri,sans-serif;color:#1e1e1e;background-color:transparent;font-weight:400;font-style:normal;font-variant:normal;text-decoration:none;vertical-align:baseline;white-space:pre;white-space:pre-wrap;">اذا وصلك منتج غير المنتج الذي قمت بطلبه سنتكفل بتكاليف الشحن والإرجاع</span></p></li><li dir="rtl" style="list-style-type:decimal;font-size:11pt;font-family:Calibri,sans-serif;color:#1e1e1e;background-color:transparent;font-weight:400;font-style:normal;font-variant:normal;text-decoration:none;vertical-align:baseline;white-space:pre;margin-left: 10.350000000000001pt;" aria-level="2"><p dir="rtl" style="line-height:2.4;text-align: right;margin-top:0pt;margin-bottom:8pt;" role="presentation"><span style="font-size:11pt;font-family:Calibri,sans-serif;color:#1e1e1e;background-color:transparent;font-weight:400;font-style:normal;font-variant:normal;text-decoration:none;vertical-align:baseline;white-space:pre;white-space:pre-wrap;">اذا لم يكن هنالك خطأ او عيب في المنتج ولكن لم تعد ترغب به لسبب ما يسعدنا خدمتك باسترجاع المنتج&nbsp; ولكن لن يتحمل المتجر تكاليف الشحن والارجاع</span></p></li></ol><p dir="rtl" style="line-height:2.4;text-align: right;background-color:#ffffff;margin-top:0pt;margin-bottom:0pt;"><span style="font-size:12pt;font-family:Calibri,sans-serif;color:#ff9933;background-color:transparent;font-weight:700;font-style:normal;font-variant:normal;text-decoration:underline;-webkit-text-decoration-skip:none;text-decoration-skip-ink:none;vertical-align:baseline;white-space:pre;white-space:pre-wrap;">كيف يمكن استرداد المبلغ الذي دفعته في حالة الاسترجاع؟</span></p><ol style="margin-top:0;margin-bottom:0;padding-inline-start:48px;"><li dir="rtl" style="list-style-type:decimal;font-size:12pt;font-family:Calibri,sans-serif;color:#333333;background-color:transparent;font-weight:400;font-style:normal;font-variant:normal;text-decoration:none;vertical-align:baseline;white-space:pre;margin-left: -10.95pt;" aria-level="1"><p dir="rtl" style="line-height:2.4;text-align: right;background-color:#ffffff;margin-top:0pt;margin-bottom:0pt;" role="presentation"><span style="font-size:12pt;font-family:dinnextltarabic-regular;color:#333333;background-color:transparent;font-weight:400;font-style:normal;font-variant:normal;text-decoration:none;vertical-align:baseline;white-space:pre;white-space:pre-wrap;">&nbsp;</span><span style="font-size:12pt;font-family:Calibri,sans-serif;color:#333333;background-color:transparent;font-weight:400;font-style:normal;font-variant:normal;text-decoration:none;vertical-align:baseline;white-space:pre;white-space:pre-wrap;">يتم استرداد المبلغ بعد وصول المنتج إلينا والتأكد من سلامته، ومن ثم تحويل المبلغ لحسابكم خلال 24 ساعة أو حسب نظام البنك في تأكيد الحوالات.</span></p></li><li dir="rtl" style="list-style-type:decimal;font-size:12pt;font-family:Calibri,sans-serif;color:#333333;background-color:transparent;font-weight:400;font-style:normal;font-variant:normal;text-decoration:none;vertical-align:baseline;white-space:pre;margin-left: -10.95pt;" aria-level="1"><p dir="rtl" style="line-height:2.4;text-align: right;background-color:#ffffff;margin-top:0pt;margin-bottom:0pt;" role="presentation"><span style="font-size:12pt;font-family:Calibri,sans-serif;color:#333333;background-color:transparent;font-weight:400;font-style:normal;font-variant:normal;text-decoration:none;vertical-align:baseline;white-space:pre;white-space:pre-wrap;">بعد ارسال المنتج إلينا وتأكيد استلامه من فريقنا وفحصه يتم ارسال المنتج البديل الذي قمت باختياره،</span></p></li><li dir="rtl" style="list-style-type:decimal;font-size:12pt;font-family:Calibri,sans-serif;color:#333333;background-color:transparent;font-weight:400;font-style:normal;font-variant:normal;text-decoration:none;vertical-align:baseline;white-space:pre;margin-left: -10.95pt;" aria-level="1"><p dir="rtl" style="line-height:2.4;text-align: right;background-color:#ffffff;margin-top:0pt;margin-bottom:0pt;" role="presentation"><span style="font-size:12pt;font-family:Calibri,sans-serif;color:#333333;background-color:transparent;font-weight:400;font-style:normal;font-variant:normal;text-decoration:none;vertical-align:baseline;white-space:pre;white-space:pre-wrap;">إذا كان هناك فرق في السعر سيتم اصدار فاتورة لدفعها من طرفك.</span></p></li><li dir="rtl" style="list-style-type:decimal;font-size:12pt;font-family:Calibri,sans-serif;color:#333333;background-color:transparent;font-weight:400;font-style:normal;font-variant:normal;text-decoration:none;vertical-align:baseline;white-space:pre;margin-left: -10.95pt;" aria-level="1"><p dir="rtl" style="line-height:2.4;text-align: right;background-color:#ffffff;margin-top:0pt;margin-bottom:0pt;" role="presentation"><span style="font-size:11pt;font-family:Calibri,sans-serif;color:#1e1e1e;background-color:transparent;font-weight:400;font-style:normal;font-variant:normal;text-decoration:none;vertical-align:baseline;white-space:pre;white-space:pre-wrap;">في حال تم الدفع عبر</span><span style="font-size:11pt;font-family:Calibri,sans-serif;color:#1e1e1e;background-color:transparent;font-weight:700;font-style:normal;font-variant:normal;text-decoration:underline;-webkit-text-decoration-skip:none;text-decoration-skip-ink:none;vertical-align:baseline;white-space:pre;white-space:pre-wrap;">&nbsp;البطاقة الائتمانية</span><span style="font-size:11pt;font-family:Calibri,sans-serif;color:#1e1e1e;background-color:transparent;font-weight:400;font-style:normal;font-variant:normal;text-decoration:none;vertical-align:baseline;white-space:pre;white-space:pre-wrap;">&nbsp;سيتم إعادة قيمة ما دفعته إلى البطاقة التي تم استخدامها. الرجاء الملاحظة أنه قد يتطلب الأمر 10 ايام كحد أقصى عمل لتظهر القيمة في حسابك البنكي من تاريخ إعادتنا للقيمة.</span></p></li><li dir="rtl" style="list-style-type:decimal;font-size:12pt;font-family:Calibri,sans-serif;color:#333333;background-color:transparent;font-weight:400;font-style:normal;font-variant:normal;text-decoration:none;vertical-align:baseline;white-space:pre;margin-left: -10.95pt;" aria-level="1"><p dir="rtl" style="line-height:2.4;text-align: right;background-color:#ffffff;margin-top:0pt;margin-bottom:0pt;" role="presentation"><span style="font-size:11pt;font-family:Calibri,sans-serif;color:#1e1e1e;background-color:transparent;font-weight:400;font-style:normal;font-variant:normal;text-decoration:none;vertical-align:baseline;white-space:pre;white-space:pre-wrap;">في حال تم الدفع عن طريق&nbsp;</span><span style="font-size:11pt;font-family:Calibri,sans-serif;color:#1e1e1e;background-color:transparent;font-weight:400;font-style:normal;font-variant:normal;text-decoration:underline;-webkit-text-decoration-skip:none;text-decoration-skip-ink:none;vertical-align:baseline;white-space:pre;white-space:pre-wrap;">تحويل المبلغ</span><span style="font-size:11pt;font-family:Calibri,sans-serif;color:#1e1e1e;background-color:transparent;font-weight:400;font-style:normal;font-variant:normal;text-decoration:none;vertical-align:baseline;white-space:pre;white-space:pre-wrap;">&nbsp;لحساباتنا البنكية سنقوم بالتواصل معك لتزويدنا بالمعلومات البنكية لتحويل المبلغ.</span></p></li><li dir="rtl" style="list-style-type:decimal;font-size:12pt;font-family:Calibri,sans-serif;color:#333333;background-color:transparent;font-weight:400;font-style:normal;font-variant:normal;text-decoration:none;vertical-align:baseline;white-space:pre;margin-left: -10.95pt;" aria-level="1"><p dir="rtl" style="line-height:2.4;text-align: right;background-color:#ffffff;margin-top:0pt;margin-bottom:0pt;" role="presentation"><span style="font-size:11pt;font-family:Calibri,sans-serif;color:#1e1e1e;background-color:transparent;font-weight:400;font-style:normal;font-variant:normal;text-decoration:none;vertical-align:baseline;white-space:pre;white-space:pre-wrap;">في حال تم&nbsp;</span><span style="font-size:11pt;font-family:Calibri,sans-serif;color:#1e1e1e;background-color:transparent;font-weight:400;font-style:normal;font-variant:normal;text-decoration:underline;-webkit-text-decoration-skip:none;text-decoration-skip-ink:none;vertical-align:baseline;white-space:pre;white-space:pre-wrap;">الدفع عند الاستلام</span><span style="font-size:11pt;font-family:Calibri,sans-serif;color:#1e1e1e;background-color:transparent;font-weight:400;font-style:normal;font-variant:normal;text-decoration:none;vertical-align:baseline;white-space:pre;white-space:pre-wrap;">&nbsp;سنقوم بالتواصل معك لتزويدنا بالمعلومات البنكية لتحويل المبلغ&nbsp;.</span></p></li></ol><p dir="rtl" style="line-height:2.4;margin-right: 7.05pt;text-align: right;background-color:#ffffff;margin-top:0pt;margin-bottom:0pt;"><span style="font-size:12pt;font-family:Calibri,sans-serif;color:#ff9933;background-color:transparent;font-weight:700;font-style:normal;font-variant:normal;text-decoration:underline;-webkit-text-decoration-skip:none;text-decoration-skip-ink:none;vertical-align:baseline;white-space:pre;white-space:pre-wrap;">كيف يمكنني استبدال أو استرجاع المنتج التالف؟</span></p><ol style="margin-top:0;margin-bottom:0;padding-inline-start:48px;"><li dir="rtl" style="list-style-type:decimal;font-size:12pt;font-family:Calibri,sans-serif;color:#333333;background-color:transparent;font-weight:400;font-style:normal;font-variant:normal;text-decoration:none;vertical-align:baseline;white-space:pre;margin-left: -10.950000000000003pt;" aria-level="3"><p dir="rtl" style="line-height:2.4;text-align: right;background-color:#ffffff;margin-top:0pt;margin-bottom:0pt;" role="presentation"><span style="font-size:12pt;font-family:Calibri,sans-serif;color:#333333;background-color:transparent;font-weight:400;font-style:normal;font-variant:normal;text-decoration:none;vertical-align:baseline;white-space:pre;white-space:pre-wrap;">خالص الاعتذار عن أي منتج وصلك متضررًا أثناء النقل والشحن، يرجى التواصل معنا خلال</span><span style="font-size:12pt;font-family:Calibri,sans-serif;color:#333333;background-color:transparent;font-weight:700;font-style:normal;font-variant:normal;text-decoration:none;vertical-align:baseline;white-space:pre;white-space:pre-wrap;">&nbsp;24 ساعة</span><span style="font-size:12pt;font-family:Calibri,sans-serif;color:#333333;background-color:transparent;font-weight:400;font-style:normal;font-variant:normal;text-decoration:none;vertical-align:baseline;white-space:pre;white-space:pre-wrap;">&nbsp;من استلامك للطلب</span></p></li><li dir="rtl" style="list-style-type:decimal;font-size:12pt;font-family:Calibri,sans-serif;color:#333333;background-color:transparent;font-weight:400;font-style:normal;font-variant:normal;text-decoration:none;vertical-align:baseline;white-space:pre;margin-left: -10.950000000000003pt;" aria-level="3"><p dir="rtl" style="line-height:2.4;text-align: right;background-color:#ffffff;margin-top:0pt;margin-bottom:0pt;" role="presentation"><span style="font-size:12pt;font-family:Calibri,sans-serif;color:#333333;background-color:transparent;font-weight:400;font-style:normal;font-variant:normal;text-decoration:none;vertical-align:baseline;white-space:pre;white-space:pre-wrap;">وارسال الصور للمنتج المكسور (التالف).</span></p></li></ol><p dir="ltr" style="line-height:1.7999999999999998;text-align: justify;background-color:#ffffff;margin-top:0pt;margin-bottom:0pt;"></p><p dir="rtl" style="line-height:2.4;text-align: right;background-color:#ffffff;margin-top:0pt;margin-bottom:0pt;"><br></p><div><br></div></span>

    @else

   <h1>Return and exchange policy</h1>
   <p>
    We thank you for choosing our store to buy your favorite products, and we are happy to serve you. Sometimes your mind may change in relation to the products you have purchased, not yours. We at Dama Store will make every effort to make the return or exchange process as simple and smooth as possible.
   </p>

    <h4>
        - Time period policy for returning or replacing purchases:
    </h4>
    <h4>
        What are the conditions for exchange and return?
    </h4>
<p>
    <ul>
        <li>    If you want to replace or return a product, you must contact us first via the email shown on the site.
        </li>
        <li>
            The customer has the right to return the product within 3 days from the date of receipt, provided that the product is intact and has not undergone any change,

        </li>
        <li>
            The customer has the right to exchange the product with another product, before sending the shipment. After sending the shipment, he is entitled to exchange within 14 working days, and he must bear the full shipping cost and the product cost difference, if any.

        </li>
        <li>
            The product should be unused and in its original condition and all labels are not removed.

        </li>
        <li>
            That the product is returned with the brand box unopened and that it is intact and not damaged.

        </li>
        <li>
            If the product is defective, or not in conformity with the specifications that were determined at the time of purchase, the customer has the right to replace or return the product within 14 days, and the customer in this case must send proof of shipping costs, indicating: the consignment number - shipping - and cost. The customer is then entitled to a full refund.

        </li>
    </ul>

</p>

    <h1>What are non-exchangeable and non-refundable products?</h1>
    The consumer is not entitled to exchange and return the following products:
    <br>
    <ul>
        <li style="list-style-type: none;">
            1. After 3 days from the date of receipt, exchange is not accepted after 14 days.

        </li>
        <li style="list-style-type: none;">
            2. When the product has changed from its original condition for any reason.

        </li>
        <li style="list-style-type: none;">
            3. Products that were manufactured at the request of the customer according to the specifications specified.

        </li>
        <li style="list-style-type: none;">
            4. Malfunctions and damages resulting from misuse.

        </li>
        <li style="list-style-type: none;">
            5. Do not match the serial numbers of the order or any different appearance compared to the product when it was received during the purchase process and as it is indicated in the purchase invoice.

        </li>
    </ul>


  <h1>Who bears the costs of return:</h1>
  <ul>
      <li style="list-style-type: none;">
        1- If a product other than the one you ordered arrives, we will take care of the shipping and return costs

      </li>
      <li style="list-style-type: none;">
        2- If there is no error or defect in the product, but you no longer want it for some reason, we are happy to serve you to retrieve the product, but the store will not bear the shipping and return costs

    </li>
  </ul>




    <h1>How can I get a refund in case of a refund?</h1>
    <ul>
        <li style="list-style-type: none;">
            1. The amount is refunded after the product arrives to us and its safety is confirmed, and then the amount is transferred to your account within 24 hours or according to the bank’s system for confirming the transfers.

        </li>
        <li style="list-style-type: none;">
            2. After sending the product to us and confirming its receipt from our team and checking it, the alternative product you have chosen will be sent.

        </li>
        <li style="list-style-type: none;">
            3. If there is a difference in the price, an invoice will be issued for you to pay.

        </li>
        <li style="list-style-type: none;">
            4. If the payment was made by credit card, the value of what you paid will be returned to the card that was used. Please note that it may

        </li>
        <li style="list-style-type: none;">
            5. If the payment is made by transferring the amount to our bank accounts, we will contact you to provide us with bank information to transfer the amount.

        </li>
        <li style="list-style-type: none;">
            6. If the payment is made upon receipt, we will contact you to provide us with banking information to transfer the amount.

        </li>
    </ul>

    <h1>How can I replace or recover a damaged product?</h1>
    <ul>
        <li style="list-style-type: none;">
            1- Sincerely apologies for any product that you received damaged during transportation and shipment, please contact us within 24 hours of your receipt of the order

        </li>
        <li style="list-style-type: none;">
            2- And sending pictures of the broken (damaged) product.

        </li>
    </ul>
 @endif

@endsection



@section('script')

@endsection
