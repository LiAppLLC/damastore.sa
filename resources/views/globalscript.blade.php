{{--<script>--}}
    {{--$(document).ready(function() {--}}

    //App.init();


    // ==================================================================================
    // signed in first check

    var userObject = JSON.parse(localStorage.getItem("DamaStore_userObject"));


    //console.log('htht', userObject);
    var pageURL = $(location).attr("href");

    if(!userObject){
        if (pageURL.indexOf("order") >= 0 ){
            window.location = "/CustomerLoginRegister";
        }
        if (pageURL.indexOf("admin") >= 0 || pageURL.indexOf("CustomerDashboard") >= 0){
            window.location = "/";
        }else{
            if(pageURL.indexOf("CustomerLoginRegister") >= 0){
                $('#signout-btn').remove();
                $('#myprofile-btn').remove();
            }
            $('html').show();
            $( ".loading" ).remove();
        }
    }else{
        if(pageURL.indexOf("CustomerLoginRegister") >= 0){
            $('#signout-btn').remove();
            $('#myprofile-btn').remove();
        }
        $('html').show();
        $( ".loading" ).remove();
    }


    if(userObject){
        //console.log('signin-hide, signout-show');
        $('#signin-btn').remove();
        //$('#signout-btn').show();

        $('#username').text(userObject.username);
    }else{
        //console.log('signin-show, signout-hide');
        //$('#signin-btn').show();
        $('#signout-btn').remove();
        //$('#cart-btn').remove();
        $('#myprofile-btn').remove();

    }

    function emptyUserObject(){
        localStorage.removeItem("DamaStore_userObject");
    }


    // ==================================================================================


    // ==================================================================================
    // Language

    $('#Arabiclang').click(function () {
        //$('#ArabiclangForm').submit();
        //e.preventDefault();
        //e.stopPropagation();
        changeLanguage('ar');
    });

    $('#Englishlang').click(function () {
        //$('#ArabiclangForm').submit();
        //e.preventDefault();
        //e.stopPropagation();
        changeLanguage('en');
    });


    $('#Deutsche').click(function () {
        changeLanguage('de');
    });


    $('#española').click(function () {
        changeLanguage('es');
    });


    $('#français').click(function () {
        changeLanguage('fr');
    });


    $('#עִברִית').click(function () {
        changeLanguage('he');
    });


    $('#भारतीय').click(function () {
        changeLanguage('hi');
    });


    $('#日本人').click(function () {
        changeLanguage('ja');
    });


    $('#한국어').click(function () {
        changeLanguage('ko');
    });


    $('#Português').click(function () {
        changeLanguage('pt');
    });


    $('#русский').click(function () {
        changeLanguage('rus');
    });


    $('#اردو').click(function () {
        changeLanguage('ur');
    });


    $('#中文').click(function () {
        changeLanguage('zh');
    });

    function changeLanguage(lang){
        var formData = new FormData();
        formData.append('lang', lang);

        $('#How-It-Works').append('<div class="loading">Loading&#8230;</div>');
        $.ajax({


            type: "POST",
            url: '{{route('changeLang')}}',
            data: formData,
            // mimeType: 'application/json',
            processData: false,
            contentType: false,
            success: function (res) {

                window.location.reload();
                $( ".loading" ).remove();

            },
            error: function (res, response) {
                $( ".loading" ).remove();
            },
        });
    }

    // ==================================================================================


    // ==================================================================================
    // CSRF-TOKEN

    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });

    // ==================================================================================


    function get_hostname(url) {
        var m = url.match(/^http:\/\/[^/]+/);
        return m ? m[0] : null;
    }

    function readURL(input, image_id) {
        if (input.files && input.files[0]) {
            var reader = new FileReader();

            reader.onload = function (e) {
                $('#' + image_id).attr('src', e.target.result);
            }

            reader.readAsDataURL(input.files[0]);
        }
    }

    // ==================================================================================
    // pagination

    function changeLength(number) {
        return Math.ceil(number / pageSize);
    }

    function createB1tnList() {
        var num = 2;
        btnList = [];
        if (selectedPage - num > 0) {
            for (let i = selectedPage - num; i < selectedPage; i++) {
                btnList.push(i);
            }
        } else {
            for (let i = 1; i < selectedPage; i++) {
                btnList.push(i);
            }
        }

        for (let i = selectedPage; i <= selectedPage + num && i <= lenght; i++) {
            btnList.push(i);
        }
        {{-- console.log('btnList', btnList); --}}

        {{-- $('#pagination-div').empty(); --}}

        var before = '';
        if (selectedPage >= num) {
            {{-- before = '<li class="c-prev pagination-btn clickable-btn" data-id="-1"><i class="fa fa-angle-left"></i></li>'; --}}
        } else {
            {{-- before = '<li class="c-prev disabled clickable-btn" data-id="-1"><i class="fa fa-angle-left"></i></li>'; --}}
        }

        var after = '';
        if (selectedPage < lenght) {
            {{-- after = '<li class="c-next pagination-btn clickable-btn" data-id="-2"><i class="fa fa-angle-right"></i></li>'; --}}
        } else {
            {{-- after = '<li class="c-next disabled clickable-btn" data-id="-2"><i class="fa fa-angle-right"></i></li>'; --}}
        }


        $('#pagination-div').append(before);
        btnList.forEach(function (item, index, arr) {

            var html = '';
            if(selectedPage == item){
                {{-- html = '<li class="c-active pagination-btn clickable-btn" data-id="' + item + '">' + item + '</li>'; --}}
            }else{
                {{-- html = '<li class="pagination-btn clickable-btn" data-id=' + item + '>' + item + '</li>'; --}}
            }

            {{-- $('#pagination-div').append(html); --}}

        });
        {{-- $('#pagination-div').append(after); --}}


    }




    function changePage(num, selectedPage, localstorageVar) {
        let dont = false;
        if (num == -1) {
            selectedPage = selectedPage - 1;
            if (selectedPage <= 0) {
                dont = true;
                selectedPage = 1;
            }
        } else if (num == -2) {
            selectedPage = selectedPage + 1;
            if (selectedPage > lenght) {
                dont = true;
                selectedPage = lenght;
            }
        } else {
            selectedPage = num;
        }
        if (!dont) {
            var formDataInit = new FormData();
    formDataInit.append('sale', true);
    if($('#category-list').val() != -1){
        formDataInit.append('category', $('#category-list').val());
    }
    if($('#type-list').val() != -1){
        formDataInit.append('type', $('#type-list').val());
    }
            localStorage.setItem(localstorageVar, selectedPage.toString());
            getData(parseInt(localStorage.getItem(localstorageVar)) - 1,formDataInit);

            createB1tnList();
        }
        return selectedPage;
    }


    // =============================================================================================================


    // =============================================================================================================
    // cart

    function updateCartOrder(localStorageName){
        var cart_arr = localStorage.getItem(localStorageName);
        if(cart_arr){
            var arr = JSON.parse(cart_arr);
            $('#cart_num').text(arr.length);
            if($('#cart_num_mobile')){
                $('#cart_num_mobile').text(arr.length);
            }
        }
    }

    function AddCartEffect(id, input_id, btn_id, btn_rem_id, input_id2, btn_id2, btn_rem_id2){
        if(btn_id != ''){
            $('#' + btn_id + id).hide();
        }
        if(btn_rem_id != ''){
            $('#' + btn_rem_id + id).show();
        }
        if(input_id != ''){
            $('#' + input_id + id).hide();
        }
        if(btn_id2 != ''){
            $('#' + btn_id2 + id).hide();
        }
        if(btn_rem_id2 != ''){
            $('#' + btn_rem_id2 + id).show();
        }
        if(input_id2 != ''){
            $('#' + input_id2 + id).hide();
        }
    }

    function AddToCart(id, qty, localStorageName, input_id, btn_id, btn_rem_id, input_id2, btn_id2, btn_rem_id2){
        var cart_arr = localStorage.getItem(localStorageName);
        if(cart_arr){
            var arr = JSON.parse(cart_arr);
            var exist = false;
            arr.forEach(function (item, index, arr) {
                if(item[0] == id){
                    exist = true;
                }
            });
            if(!exist){
                arr.push([id, parseInt(qty)]);
                localStorage.setItem(localStorageName, JSON.stringify(arr));
            }
        }else{
            var arr = [];
            arr.push([id, parseInt(qty)]);
            localStorage.setItem(localStorageName, JSON.stringify(arr));
        }
        AddCartEffect(id, input_id, btn_id, btn_rem_id, input_id2, btn_id2, btn_rem_id2);
        updateCartOrder(localStorageName);
    }

    function RemoveFromCartEffet(id, input_id, btn_id, btn_rem_id, input_id2, btn_id2, btn_rem_id2){
        if(btn_id != ''){
            $('#' + btn_id + id).show();
        }
        if(btn_rem_id != ''){
            $('#' + btn_rem_id + id).hide();
        }
        if(input_id != ''){
            $('#' + input_id + id).show();
        }
        if(btn_id2 != ''){
            $('#' + btn_id2 + id).show();
        }
        if(btn_rem_id2 != ''){
            $('#' + btn_rem_id2 + id).hide();
        }
        if(input_id2 != ''){
            $('#' + input_id2 + id).show();
        }
    }

    function removeFromCartRec(id, localStorageName){
        var arr = JSON.parse(localStorage.getItem(localStorageName));

        if(arr){
            var new_arr = [];
            arr.forEach(function (item, index, arr) {
                if(item[0] != id){
                    new_arr.push(item);
                }
            });
            //console.log(new_arr);
            localStorage.setItem(localStorageName, JSON.stringify(new_arr));
        }
    }

    function increaseQty(id, localStorageName){
        var arr = JSON.parse(localStorage.getItem(localStorageName));

        if(arr){
            var new_arr = [];
            arr.forEach(function (item, index, arr) {
                if(item[0] == id){
                    new_arr.push([item[0], item[1] + 1]);
                }else{
                    new_arr.push(item);
                }
            });
            //console.log(new_arr);
            localStorage.setItem(localStorageName, JSON.stringify(new_arr));
        }
    }

    function decreaseQty(id, localStorageName){
        var arr = JSON.parse(localStorage.getItem(localStorageName));

        if(arr){
            var new_arr = [];
            arr.forEach(function (item, index, arr) {
                if(item[0] == id){
                    if(item[1] >= 2){
                        new_arr.push([item[0], item[1] - 1]);
                    }
                }else{
                    new_arr.push(item);
                }
            });
            //console.log(new_arr);
            localStorage.setItem(localStorageName, JSON.stringify(new_arr));
        }
    }

    function removeFromCart(id, localStorageName, input_id, btn_id, btn_rem_id, input_id2, btn_id2, btn_rem_id2){
        removeFromCartRec(id, localStorageName);
        RemoveFromCartEffet(id, input_id, btn_id, btn_rem_id, input_id2, btn_id2, btn_rem_id2);
        updateCartOrder(localStorageName);
    }

    function redOrBlueBtn(prod_id, localStorageName, input_id, btn_id, btn_rem_id, input_id2, btn_id2, btn_rem_id2){
        var cart_arr = localStorage.getItem(localStorageName);
        if(cart_arr){
            var arr = JSON.parse(cart_arr);
            var exist = false;

            arr.forEach(function (arritem, index, arr) {
                if(arritem[0] == prod_id){
                    exist = true;
                }
            });
            if(exist){
                // if added
                AddCartEffect(prod_id, input_id, btn_id, btn_rem_id, input_id2, btn_id2, btn_rem_id2);
            }else{
                // if not added
                RemoveFromCartEffet(prod_id, input_id, btn_id, btn_rem_id, input_id2, btn_id2, btn_rem_id2);
            }
        }
    }

    function emptyCart(){
        localStorage.removeItem("damastore_cart");
    }

    updateCartOrder('damastore_cart');

    // =============================================================================================================


    // ==================================================================================


    $('#signout-btn').click(function(){

        $('#How-It-Works').append('<div class="loading">Loading&#8230;</div>');
        emptyUserObject();
        emptyCart();
        // need to call service to log out and remove the session : 'userObject'
        window.location = "/CustomerLoginRegister/?google=false";
    });



    // ==================================================================================


    // =============================================================================================================


{{--});--}}

{{--</script>--}}
