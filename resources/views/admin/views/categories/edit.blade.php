


@extends('admin.adminHome')


@section('admin_title')

    {{--Add Products--}}

@endsection


@section('admin_breadcrumb')

    <li clclickass="breadcrumb-item"><a href="#">Home</a></li>
    <li class="breadcrumb-item">categories</li>
    <li class="breadcrumb-item active">Edit</li>

@endsection

@section('admin_style')

    <style>

        .img-card{
            width: 40%;
            margin-top: 45px;
            margin-bottom: 18px;
        }

    </style>

@endsection

@section('admin_content')

    <?php
    $locale = App::getLocale();
    ?>

    <!-- Main content -->
    <section class="content">
        <div class="container-fluid">
            <div class="row">
                <!-- left column -->
                <div class="col-md-12">
                    <!-- jquery validation -->
                    <div class="card card-primary">
                        <div class="card-header">
                            <h3 class="card-title">Categories Form <small>Edit category</small></h3>
                        </div>
                        <!-- /.card-header -->
                        <!-- form start -->
                        <form action="/admin/categories/edit/{{$category->id}}" enctype="multipart/form-data" method="post" id="CategorytEditForm">
                        @csrf
                            <div class="card-body row">
                                <div class="form-group col-12">
                                    <label for="name">Category Name</label>
                                    <input type="text" name="name" value="{{$category->name}}" class="form-control" id="name" placeholder="Category Name" >
                                </div>
                                <div class="form-group col-12">
                                    <label for="name">Category Name Arabic</label>
                                    <input type="text" name="ar_name" value="{{$category->ar_name}}" class="form-control" id="ar_name" placeholder="Category Name Arabic">
                                </div>
                                <div class="form-group col-12">
                                    <label for="description">Category Description</label>
                                    {{--<input type="text"  class="form-control"  placeholder="Product Description">--}}
                                    <textarea class="form-control"  name="description" id="description" rows="3" placeholder="Category Description">{{$category->description}}</textarea>
                                </div>
                                <div class="form-group col-12">
                                    <label for="description">Category Description Arabic</label>
                                    {{--<input type="text"  class="form-control"  placeholder="Product Description">--}}
                                    <textarea class="form-control" name="ar_description" id="ar_description" rows="3" placeholder="Category Description Arabic">{{$category->ar_description}}</textarea>
                                </div>
                                <div class="form-group col-12">
                                    <label for="Image1">Image</label>
                                    <img src="/{{$category->image}}" id="image" class="img-responsive img-card"/>
                                    <div class="custom-file">
                                        <input type="file" class="custom-file-input" id="cate_photo" name="cate_photo">
                                        <label class="custom-file-label" for="customFile">Choose file</label>
                                    </div>
                                </div>
                                <div class="form-group col-12">
                                    <label for="Image1">SVG</label>
                                    <img src="/{{$category->svg}}" id="svg" class="img-responsive img-card"/>
                                    <div class="custom-file">
                                        <input type="file" class="custom-file-input" id="cate_svg" name="cate_svg">
                                        <label class="custom-file-label" for="customFile">Choose file</label>
                                    </div>
                                </div>
                            </div>
                            <div class="card-footer">
                                <button type="submit" class="btn btn-primary">Submit</button>
                            </div>
                        </form>
                    </div>
                    <!-- /.card -->
                </div>
                <!--/.col (left) -->
                <!-- right column -->
                <div class="col-md-6">

                </div>
                <!--/.col (right) -->
            </div>
            <!-- /.row -->
        </div><!-- /.container-fluid -->
    </section>
    <!-- /.content -->
    </div>


@endsection





@section('admin_script')

{{--$('#CategorytEditForm').on('submit', function(){--}}

{{--});--}}

$("#cate_photo").change(function(){
readURL(this, 'image');
});
$("#cate_svg").change(function(){
readURL(this, 'svg');
});
@endsection



