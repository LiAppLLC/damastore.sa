


@extends('admin.adminHome')


@section('admin_title')

    Attributes

@endsection


@section('admin_breadcrumb')

	<li class="breadcrumb-item"><a class="navigation" href="#">Home</a></li>
	<li class="breadcrumb-item active">Attributes</li>

@endsection

@section('admin_style')

	<style>
		.image-div{
			width: 35px;
			height: 35px;
		}
	</style>

@endsection


@section('admin_content')


	<div class="card">
		<div class="card-header row">
            <div class="col-md-10">
                <h3 class="card-title">Attributes List</h3>
            </div>
            @if($permissions['types_add'])
            <div class="col-md-2">
                <button type="button" class="btn btn-block btn-info" onclick="on_clickadd()" data-toggle="modal" data-target="#exampleModal2">Add</button>
            </div>
            @endif
		</div>
		<!-- /.card-header -->
		<div class="card-body">
            <div class="row">
                <div class="col-md-6">
                    <div id="pagination"></div>
                </div>
                <div class="col-md-6">
                    <input type="text" class="form-control"
                           style="border:0;background-color:#efefef;border-radius:0;margin-bottom:2px;"
                           placeholder="Search all fields." id="search_input">
                </div>
            </div>
			<table id="content" class="table table-bordered table-striped">
				<thead>
				<tr>
					<th>Attributes Name</th>
					<th>Attributes Arabic Name</th>
					<th>Created At</th>
                    @if($permissions['types_edit'])
					<th>Edit</th>
                    @endif
                    @if($permissions['types_delete'])
					<th>Delete</th>
                    @endif
				</tr>
				</thead>
				<tbody id="users_table">
				</tbody>
			</table>
		</div>
	</div>

    @if($permissions['types_delete'])
	<!-- Modal -->
	<div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
		<div class="modal-dialog" role="document">
			<div class="modal-content">
				<div class="modal-header">
					<h5 class="modal-title" id="exampleModalLabel">Delete Alert</h5>
					<button type="button" class="close" data-dismiss="modal" aria-label="Close">
						<span aria-hidden="true">&times;</span>
					</button>
				</div>
				<div class="modal-body">
					are you sure you want to delete '<span id="type_name_modal" style="color: red;"></span>' ?
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
					<button id="delete-btn-modal" type="button" class="btn btn-danger">Delete</button>
				</div>
			</div>
		</div>
	</div>
    @endif


    @if($permissions['types_edit'])
	<!-- Modal -->
	<div class="modal fade" id="exampleModal1" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
		<div class="modal-dialog" role="document">
			<div class="modal-content">
				<div class="modal-header">
					<h5 class="modal-title" id="exampleModalLabel">Edit Type</h5>
					<button type="button" class="close" data-dismiss="modal" aria-label="Close">
						<span aria-hidden="true">&times;</span>
					</button>
				</div>
                <form id="editTypeForm">
                    <div class="modal-body">
                        <div class="form-group col-12">
                            <label for="name">Attributes Name</label>
                            <input type="text" name="name" class="form-control" id="name" placeholder="Type Name" >
                        </div>
                        <div class="form-group col-12">
                            <label for="name">Attributes Name Arabic</label>
                            <input type="text" name="ar_name" class="form-control" id="ar_name" placeholder="Type Name Arabic" >
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
                        <button id="edit-btn-modal" type="submit" class="btn btn-success">Submit</button>
                    </div>
                </form>
			</div>
		</div>
	</div>
    @endif


    @if($permissions['types_add'])
	<!-- Modal -->
	<div class="modal fade" id="exampleModal2" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
		<div class="modal-dialog" role="document">
			<div class="modal-content">
				<div class="modal-header">
					<h5 class="modal-title" id="exampleModalLabel">Add New Type</h5>
					<button type="button" class="close" data-dismiss="modal" aria-label="Close">
						<span aria-hidden="true">&times;</span>
					</button>
				</div>
                <form id="addTypeForm">
                    <div class="modal-body">
                        <div class="form-group col-12">
                            <label for="name">Attributes Name</label>
                            <input type="text" name="name" class="form-control" placeholder="Type Name" >
                        </div>
                        <div class="form-group col-12">
                            <label for="name">Attributes Name Arabic</label>
                            <input type="text" name="ar_name" class="form-control" id="ar_name" placeholder="Type Name Arabic" >
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
                        <button id="add-btn-modal" type="submit" class="btn btn-success">Submit</button>
                    </div>
                </form>
			</div>
		</div>
	</div>
    @endif


@endsection





@section('admin_script')


	{{--<script>--}}
        var userObject = JSON.parse(localStorage.getItem("DamaStore_userObject"));

        // blocked
        // bornDate
        // created_at
        // email
        // emailVerifiedAt
        // files
        // firstName
        // id
        // lastActivity
        // lastLoggedIn
        // lastName
        // phone
        // reputation
        // roles
        // token
        // updated_at
        // username
        // verify
        console.log('userObject', userObject);


        var formData = new FormData();
        formData.append('all', true);

        $.ajax({

            type: "POST",
            url: '{{route('AttributeGet')}}',
            data: null,
            // mimeType: 'application/json',
            processData: false,
            contentType: false,
            headers: {"Authorization": 'Bearer ' + userObject.token},
            success: function (res) {
            console.log(res)
                res.data.forEach(function(item, index, arr){

                    var date    = new Date(item.created_at),
                        yr      = date.getFullYear(),
                        month   = date.getMonth() < 10 ? '0' + date.getMonth() : date.getMonth(),
                        day     = date.getDate()  < 10 ? '0' + date.getDate()  : date.getDate(),
                        newDate = yr + '-' + month + '-' + day;

                    var deleteBtn = '';
                    if(item.name > 0){
                        deleteBtn = '<td style="text-align: center;color: red; font-size: 14px;"><i class="fa fa-times" aria-hidden="true"></i> <span> can\'t be deleted</span></td>';
                    }else{
                        deleteBtn = '<td ><button type="button" class="btn btn-block btn-danger" data-id="' + item.id + '" data-name="' + item.name + '"  data-toggle="modal" data-target="#exampleModal">Delete</button></td>';
                    }

                    $('#users_table').append('<tr>' +
                        '<td >'+ item.name +'</td>' +
                        '<td >'+ item.ar_name +'</td>' +
                        '<td >'+ newDate +'</td>' +
                        {{--'<td ><a href="/admin/products/edit/' + item.id + '"><button type="button" class="btn btn-block btn-success">Edit</button></a></td>' +--}}
                        @if($permissions['types_edit'])
                        '<td ><button type="button" class="btn btn-block btn-success" data-id="' + item.id + '" data-name="' + item.name + '" data-arname="' + item.ar_name +'" data-toggle="modal" data-target="#exampleModal1">Edit</button></td>' +
                        @endif
                        @if($permissions['types_delete'])
                        deleteBtn +
                        @endif
                        '</tr>');
                       pagenition_s();
                });
                pagenition_s();
            },
            error: function (res, response) {
            },
        });

        var type_id;
        $('#exampleModal').on('show.bs.modal', function(e) {

            var id = $(e.relatedTarget).data('id');
            type_id = id;
            var name = $(e.relatedTarget).data('name');
            $('#type_name_modal').text(name);

        });
        $('#delete-btn-modal').click(function (e) {

            e.preventDefault();
            e.stopPropagation();

            var formData = new FormData();
            formData.append('att_id', type_id);

            $.ajax({

                type: "POST",
                url: '{{route('AttributeDelete')}}',
                data: formData,
                // mimeType: 'application/json',
                processData: false,
                contentType: false,
                headers: {"Authorization": 'Bearer ' + userObject.token},
                success: function (res) {
                    $('#exampleModal').hide();
                    if(res.status == 200) {
                        Swal.fire({
                            icon: 'success',
                            title: 'success',
                            text: res.msg,
                        }).then(function() {

                            window.location = "/admin/attributes/list";
                        });
                    }else{
                        Swal.fire({
                            icon: 'error',
                            title: 'Oops...',
                            text: 'somthing went wrong',
                        });
                    }
                },
                error: function (res, response) {
                },
            });

        });



    $('#exampleModal1').on('show.bs.modal', function(e) {
        $.ajax({

            type: "POST",
            url: '{{route('category')}}',
            data: null,
            // mimeType: 'application/json',
            headers: {"Authorization": 'Bearer ' + userObject.token},
            processData: false,
            contentType: false,
            success: function (res) {
                if(res.status == 200) {
                    $('#category2').html('');
                    res.data.forEach(function(item, index, arr) {
                        if(index == 0){
                            $('#category2').append('<option selected="selected" value="' + item.id + '">' + item.name + '</option>');
                        }else {
                            $('#category2').append('<option value="' + item.id + '">' + item.name + '</option>');
                        }
                    });
                }
            },
            error: function (res, response) {
                $( ".loading" ).remove();
            },
        });
        var id = $(e.relatedTarget).data('id');
        type_id = id;
        var name = $(e.relatedTarget).data('name');
        var desc = $(e.relatedTarget).data('desc');
        var arname = $(e.relatedTarget).data('arname');
        var ardesc = $(e.relatedTarget).data('ardesc');
        console.log('hhh', id, name, desc);
        $('#name').val(name);
        $('#description').val(desc);
        $('#ar_name').val(arname);
        $('#ar_description').val(ardesc);

    });


        $('#editTypeForm').submit(function (e) {

            e.preventDefault();
            e.stopPropagation();

            var formData = new FormData($(this)[0]);
            formData.append('attr_id', type_id);

            $.ajax({

                type: "POST",
                url: '{{route('AttributeEdit')}}',
                data: formData,
                // mimeType: 'application/json',
                processData: false,
                contentType: false,
                headers: {"Authorization": 'Bearer ' + userObject.token},
                success: function (res) {
                    $('#exampleModal').hide();
                    if(res.status == 200) {
                        Swal.fire({
                            icon: 'success',
                            title: 'success',
                            text: res.msg,
                        }).then(function() {
                            window.location = "/admin/attributes/list";
                        });
                    }else{
                        Swal.fire({
                            icon: 'error',
                            title: 'Oops...',
                            text: 'somthing went wrong',
                        });
                    }
                },
                error: function (res, response) {
                },
            });

        });


        $('#addTypeForm').submit(function (e) {

            e.preventDefault();
            e.stopPropagation();

            var formData = new FormData($(this)[0]);
              console.log(formData)
            $.ajax({

                type: "POST",
                url: '{{route('AttributeAdd')}}',
                data: formData,
                // mimeType: 'application/json',
                processData: false,
                contentType: false,
                headers: {"Authorization": 'Bearer ' + userObject.token},
                success: function (res) {
                    $('#exampleModal').hide();
                    if(res.status == 200) {
                        Swal.fire({
                            icon: 'success',
                            title: 'success',
                            text: res.msg,
                        }).then(function() {
                            window.location = "/admin/attributes/list";
                        });
                    }else{
                        Swal.fire({
                            icon: 'error',
                            title: 'Oops...',
                            text: 'somthing went wrong',
                        });
                    }
                },
                error: function (res, response) {
                },
            });

        });




@endsection

