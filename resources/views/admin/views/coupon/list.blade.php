@extends('admin.adminHome')


@section('admin_title')

    Coupon Code

@endsection


@section('admin_breadcrumb')

    <li class="breadcrumb-item"><a class="navigation" href="#">Home</a></li>
    <li class="breadcrumb-item active"> Coupon Code</li>

@endsection

@section('admin_style')

    <style>
        .image-div {
            width: 35px;
            height: 35px;
        }

    </style>

@endsection


@section('admin_content')


    <div class="card">
        <div class="card-header row">
            <div class="col-md-10">
                <h3 class="card-title"> Coupon Code List</h3>
            </div>
            <div class="col-md-2">

            </div>

        </div>
        <div class="alert alert-danger" id="error" style="display: none" role="alert">
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
        </div>
        <!-- /.card-header -->
        <div class="card-body">
            <form id="add_coupon" method="post">
                <div class="row">

                    <div class="col-md-6">
                        <label>Coupon code :</label>
                        <input class="form-control" type="text" name="coupon" id="coupon">
                    </div>
                    <div class="col-md-6">
                        <label>Value (%):</label>
                        <input class="form-control" type="number" name="coupon_value" id="coupon_value">
                    </div>
                    <div class="col-md-6">
                        <label>Start :</label>
                        <input class="form-control" type="date" name="coupon_start" id="coupon_start">
                    </div>
                    <div class="col-md-6">
                        <label>End :</label>
                        <input class="form-control" type="date" name="coupon_end" id="coupon_end">
                    </div>
                    <div class="col-md-4 m-4">
                        <button type="submit" class="btn btn-primary form-control" id="coupon_submit">Submit</button>
                    </div>
                    <div class="alert alert-success m-3" id="done" style="display:none;" role="alert">
                        <h4 class="alert-heading">Well done!</h4>
                        <p>Add Coupon successfully</p>
                    </div>
                </div>
            </form>
        </div>
        <div class="card-body">
            <div class="row">
                <div class="col-md-12">
                    <table class="table">
                        <tr>
                            <th>ID</th>
                            <th>Coupon Code</th>
                            <th>Value</th>
                            <th>Start</th>
                            <th>End</th>
                            <th>Action</th>
                        </tr>
                        <tbody id="content">

                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>


@endsection
<script src="https://code.jquery.com/jquery-3.5.1.min.js"
    integrity="sha256-9/aliU8dGd2tb6OSsuzixeV4y/faTqgFtohetphbbj0=" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.form/4.3.0/jquery.form.min.js"
    integrity="sha384-qlmct0AOBiA2VPZkMY3+2WqkHtIQ9lSdAsAn5RUJD/3vA5MKDgSGcdmIv4ycVxyn" crossorigin="anonymous">
</script>


<script>
    var userObject = JSON.parse(localStorage.getItem("DamaStore_userObject"));
    $(document).ready(function() {
        $("#add_coupon").submit(function(e) {
            e.preventDefault(); // avoid to execute the actual submit of the form.
            var form = $(this);
            $.ajax({
                type: "POST",
                url: '{{ route('AddCoupon') }}',
                headers: {
                    "Authorization": 'Bearer ' + userObject.token
                },

                data: form.serialize(), // serializes the form's elements.
                success: function(data) {
                    if (data[0] === 'Validation Error.') {
                        $('#error').show('slow');
                        $('#error').html(JSON.stringify(data));
                    }
                    console.log(data)
                    if (data.successes == "done") {
                        $('#error').hide('slow');
                        $('#done').show('slow');
                        $.ajax({
                            type: "GET",
                            url: '{{ route('GetCoupon') }}',
                            headers: {
                                "Authorization": 'Bearer ' + userObject.token
                            },
                            success: function(data) {
                                let row = $('#content');
                                row.html('');
                                if (data[0] == 'coupon') {
                                    for (var i in data[1]) {
                                        row.append('<tr><td>' + data[1][i].id +
                                            '</td><td>' + data[1][i]
                                            .coupon +
                                            '</td><td>' + data[1][i].value +
                                            '</td><td>' + data[1][i].start +
                                            '</td><td>' + data[1][i].end +
                                            '</td><td><button class="btn-danger btn" onclick="deleted(' +
                                            data[1][i].id +
                                            ')">Delete</button></td></tr>')
                                    }
                                } else {
                                    $('#error').show('slow');
                                    $('#error').html(JSON.stringify(data));
                                }
                            }
                        });
                    }
                }
            });
        });


        $.ajax({
            type: "GET",
            url: '{{ route('GetCoupon') }}',
            headers: {
                "Authorization": 'Bearer ' + userObject.token
            },
            success: function(data) {
                let row = $('#content');
                if (data[0] == 'coupon') {
                    for (var i in data[1]) {
                        row.append('<tr><td>' + data[1][i].id + '</td><td>' + data[1][i].coupon +
                            '</td><td>' + data[1][i].value + '</td><td>' + data[1][i].start +
                            '</td><td>' + data[1][i].end +
                            '</td><td><button class="btn-danger btn" onclick="deleted(' +
                            data[1][i].id +
                            ')">Delete</button></td></tr>')
                    }
                } else {
                    $('#error').show('slow');
                    $('#error').html(JSON.stringify(data));
                }
            }
        });

    });

</script>
<script>
    function deleted(id) {
        $.ajax({

            type: "delete",
            url: '/api/DeleteCoupon/' + id,
            data: null,
            // mimeType: 'application/json',
            headers: {
                "Authorization": 'Bearer ' + userObject.token
            },
            processData: false,
            contentType: false,
            success: function(res) {

                $.ajax({
                    type: "GET",
                    url: '{{ route('GetCoupon') }}',
                    headers: {
                        "Authorization": 'Bearer ' + userObject.token
                    },
                    success: function(data) {
                        let row = $('#content');
                        row.html('');
                        if (data[0] == 'coupon') {
                            for (var i in data[1]) {
                                row.append('<tr><td>' + data[1][i].id + '</td><td>' + data[
                                        1][i].coupon +
                                    '</td><td>' + data[1][i].value + '</td><td>' + data[
                                        1][i].start +
                                    '</td><td>' + data[1][i].end +
                                    '</td><td><button class="btn-danger btn" onclick="deleted(' +
                                    data[1][i].id +
                                    ')">Delete</button></td></tr>')
                            }
                        } else {
                            $('#error').show('slow');
                            $('#error').html(JSON.stringify(data));
                        }
                    }
                });

            },
            error: function(res, response) {
                console.log(res, response);
            },
        });
    }

</script>
