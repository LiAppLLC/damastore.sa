


@extends('admin.adminHome')


@section('admin_title')

    User Roles

@endsection


@section('admin_breadcrumb')

	<li class="breadcrumb-item"><a class="navigation" href="#">Home</a></li>
	<li class="breadcrumb-item active">User Roles</li>

@endsection

@section('admin_style')

	<style>
        .switch {
            position: relative;
            display: inline-block;
            width: 60px;
            height: 34px;
        }

        .switch input {
            opacity: 0;
            width: 0;
            height: 0;
        }

        .slider {
            position: absolute;
            cursor: pointer;
            top: 0;
            left: 0;
            right: 0;
            bottom: 0;
            background-color: #ccc;
            -webkit-transition: .4s;
            transition: .4s;
        }

        .slider:before {
            position: absolute;
            content: "";
            height: 26px;
            width: 26px;
            left: 4px;
            bottom: 4px;
            background-color: white;
            -webkit-transition: .4s;
            transition: .4s;
        }

        input:checked + .slider {
            background-color: #2196F3;
        }

        input:focus + .slider {
            box-shadow: 0 0 1px #2196F3;
        }

        input:checked + .slider:before {
            -webkit-transform: translateX(26px);
            -ms-transform: translateX(26px);
            transform: translateX(26px);
        }

        /* Rounded sliders */
        .slider.round {
            border-radius: 34px;
        }

        .slider.round:before {
            border-radius: 50%;
        }
	</style>

@endsection


@section('admin_content')


    <form id="submitForm">
        <div class="card">
            <div class="card-header row">
                <div class="col-md-10">
                    <h3 class="card-title">User Roles List</h3>
                </div>
                <div class="col-md-2">
                    <button type="submit" id="submit-btn" class="btn btn-block btn-info">Submit</button>
                </div>
            </div>
            <!-- /.card-header -->
            <div class="card-body">
                <table id="example1" class="table table-bordered table-striped">
                    <thead>
                    <tr>
                        <th>Role Name</th>
                        <th>Switch</th><!-- Bootstrap Switch -->
                    </tr>
                    </thead>
                    <tbody id="users_table">
                    </tbody>
                </table>
            </div>
        </div>
    </form>

@endsection





@section('admin_script')


	{{--<script>--}}
        var userObject = JSON.parse(localStorage.getItem("DamaStore_userObject"));

        // blocked
        // bornDate
        // created_at
        // email
        // emailVerifiedAt
        // files
        // firstName
        // id
        // lastActivity
        // lastLoggedIn
        // lastName
        // phone
        // reputation
        // roles
        // token
        // updated_at
        // username
        // verify
        console.log('userObject', userObject);

        function remove(arr, id){
            var new_arr = [];
            arr.forEach(function(item, index, arr){
                if(item != id){
                    new_arr.push(item);
                }
            });
            return new_arr;
        }

        var url = window.location.pathname;
        var id = url.substring(url.lastIndexOf('/') + 1);

        var formData = new FormData();
        formData.append('user_id', id);

        var permissions_true_arr = [];
        var permissions_false_arr = [];

        $.ajax({

            type: "POST",
            url: '/api/getRolesByUser/0/1000',
            data: formData,
            // mimeType: 'application/json',
            processData: false,
            contentType: false,
            headers: {"Authorization": 'Bearer ' + userObject.token},
            success: function (res) {
                res.data.forEach(function(item, index, arr){

                    var date    = new Date(item.created_at),
                        yr      = date.getFullYear(),
                        month   = date.getMonth() < 10 ? '0' + date.getMonth() : date.getMonth(),
                        day     = date.getDate()  < 10 ? '0' + date.getDate()  : date.getDate(),
                        newDate = yr + '-' + month + '-' + day;

                    var checked = '';
                    if(item.added  == 1){
                        checked = 'checked';
                        permissions_true_arr.push(item.id);
                    }else{
                        permissions_false_arr.push(item.id);
                    }

                    $('#users_table').append('<tr>' +
                        '<td >'+ item.name +'</td>' +
                        '<td ><label class="switch"><input class="permission" id="permission_' + item.id + '" data-id="' + item.id + '" type="checkbox"' + checked + '><span class="slider round"></span></label></td>' +
                        '</tr>');



                });

    console.log('permissions_true_arr', permissions_true_arr);
    console.log('permissions_false_arr', permissions_false_arr);

                console.log('hey');

            },
            error: function (res, response) {
            },
        });




        $('#submitForm').submit(function (e) {

            e.preventDefault();
            e.stopPropagation();

            var formData = new FormData($(this)[0]);

            formData.append('user_id', id);
            formData.append('userRoles', permissions_true_arr);
    //console.log('permissions_true_arr', permissions_true_arr);
    //console.log('permissions_false_arr', permissions_false_arr);

    //console.log('formData', formData);

            $.ajax({

                type: "POST",
                url: '/api/addMultiUserRoles',
                data: formData,
                // mimeType: 'application/json',
                processData: false,
                contentType: false,
                headers: {"Authorization": 'Bearer ' + userObject.token},
                success: function (res) {

                    var formData2 = new FormData();

                    formData2.append('user_id', id);
                    formData2.append('userRoles', permissions_false_arr);

                    $.ajax({

                        type: "POST",
                        url: '/api/deleteMultiUserRoles',
                        data: formData2,
                        // mimeType: 'application/json',
                        processData: false,
                        contentType: false,
                        headers: {"Authorization": 'Bearer ' + userObject.token},
                        success: function (res) {
                            $('#exampleModal').hide();
                            if(res.status == 200) {
                                Swal.fire({
                                    icon: 'success',
                                    title: 'success',
                                    text: res.msg,
                                }).then(function() {
                                    window.location = "/admin/users";
                                });
                            }else{
                                Swal.fire({
                                    icon: 'error',
                                    title: 'Oops...',
                                    text: 'somthing went wrong',
                                });
                            }
                        },
                        error: function (res, response) {
                        },
                    });
                },
                error: function (res, response) {
                },
            });

        });

    $(document).on('change', '.permission', function(e){
        var id = $(this).data('id');
        var val = $("#permission_" + id).is(":checked");

        if(val){
            permissions_true_arr.push(id);
            permissions_false_arr = remove(permissions_false_arr, id);
        }else{
            permissions_false_arr.push(id);
            permissions_true_arr = remove(permissions_true_arr, id);
        }

        //console.log('permissions_true_arr', permissions_true_arr);
        //console.log('permissions_false_arr', permissions_false_arr);
    });


@endsection



