


@extends('admin.adminHome')


@section('admin_title')

	Users

@endsection


@section('admin_breadcrumb')

	<li class="breadcrumb-item"><a class="navigation" href="#">Home</a></li>
	<li class="breadcrumb-item active">Users</li>

@endsection




@section('admin_content')

    <?php
    $locale = App::getLocale();
    ?>


	<div class="card">
		<div class="card-header">
			<h3 class="card-title">Users List</h3>
		</div>
		<!-- /.card-header -->
		<div class="card-body">
            <div class="row">
                <div class="col-md-6">
                    <div id="pagination"></div>
                </div>
                <div class="col-md-6">
                    <input type="text" class="form-control"
                           style="border:0;background-color:#efefef;border-radius:0;margin-bottom:2px;"
                           placeholder="Search all fields." id="search_input">
                </div>
            </div>
            <div style="width: 100%;overflow:auto;">
                <table id="content" class="table table-bordered table-striped">
				<thead>
				<tr>
					<th>username</th>
					<th>firstName</th>
					<th>lastName</th>
					<th>bornDate</th>
					<th>email</th>
					<th>phone</th>
					<th>Roles</th>
				</tr>
				</thead>
				<tbody id="users_table">
				</tbody>
			</table>
		</div>
        </div>
	</div>


@endsection





@section('admin_script')


	{{--<script>--}}
        var userObject = JSON.parse(localStorage.getItem("DamaStore_userObject"));

        // blocked
        // bornDate
        // created_at
        // email
        // emailVerifiedAt
        // files
        // firstName
        // id
        // lastActivity
        // lastLoggedIn
        // lastName
        // phone
        // reputation
        // roles
        // token
        // updated_at
        // username
        // verify
        console.log('userObject', userObject);


        var formData = new FormData();

        $('#How-It-Works').append('<div class="loading">Loading&#8230;</div>');
        $.ajax({

            type: "POST",
            url: '/api/getAllUsersFilter/0/1000',
            data: formData,
            // mimeType: 'application/json',
            processData: false,
            contentType: false,
            headers: {"Authorization": 'Bearer ' + userObject.token},
            success: function (res) {

                res.data.forEach(function(item, index, arr){
                    console.log(item);
                    var permission_btn = '';
                    if(item.username != 'admin'){
                        permission_btn = '<td ><a class="navigation" href="/admin/user_roles_list/' + item.id + '"><button type="button" class="btn btn-block btn-success" data-id="' + item.id + '" data-name="' + item.name + '" >Roles</button></a></td>';
                    }else{
                        permission_btn = '<td style="text-align: center;color: red; font-size: 14px;"><i class="fa fa-times" aria-hidden="true"></i> <span> can\'t be edited</span></td>';
                    }
                    $('#users_table').append('<tr>' +
                        '<td >'+ item.username +'</td>' +
                        '<td >'+ item.firstName +'</td>' +
                        '<td >'+ item.lastName +'</td>' +
                        '<td >'+ item.bornDate +'</td>' +
                        '<td >'+ item.email +'</td>' +
                        '<td >'+ item.phone +'</td>' +
                        @if($permissions['assignRoleToUser'])
                            permission_btn +
                        @endif
                        '</tr>');
    pagenition_s();
                });

            },
            error: function (res, response) {
                $( ".loading" ).remove();
            },
        });



@endsection



