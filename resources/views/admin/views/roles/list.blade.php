


@extends('admin.adminHome')


@section('admin_title')

    Roles

@endsection


@section('admin_breadcrumb')

	<li class="breadcrumb-item"><a class="navigation" href="#">Home</a></li>
	<li class="breadcrumb-item active">Roles</li>

@endsection

@section('admin_style')

	<style>
		.image-div{
			width: 35px;
			height: 35px;
		}
	</style>

@endsection


@section('admin_content')


	<div class="card">
		<div class="card-header row">
            <div class="col-md-10">
                <h3 class="card-title">Roles List</h3>
            </div>
            @if($permissions['roles_add'])
            <div class="col-md-2">
                <button type="button" class="btn btn-block btn-info" data-toggle="modal" data-target="#exampleModal2">Add</button>
            </div>
            @endif
		</div>
		<!-- /.card-header -->
		<div class="card-body">
            <div class="row">
                <div class="col-md-6">
                    <div id="pagination"></div>
                </div>
                <div class="col-md-6">
                    <input type="text" class="form-control"
                           style="border:0;background-color:#efefef;border-radius:0;margin-bottom:2px;"
                           placeholder="Search all fields." id="search_input">
                </div>
            </div>
            <table id="content" class="table table-bordered table-striped">
				<thead>
				<tr>
					<th>Role Name</th>
					<th>Created At</th>
					<th>Permissions</th>
                    @if($permissions['roles_edit'])
					<th>Edit</th>
                    @endif
                    @if($permissions['roles_delete'])
					<th>Delete</th>
                    @endif
				</tr>
				</thead>
				<tbody id="users_table">
				</tbody>
			</table>
		</div>
	</div>


    @if($permissions['roles_delete'])
	<!-- Modal -->
	<div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
		<div class="modal-dialog" role="document">
			<div class="modal-content">
				<div class="modal-header">
					<h5 class="modal-title" id="exampleModalLabel">Delete Alert</h5>
					<button type="button" class="close" data-dismiss="modal" aria-label="Close">
						<span aria-hidden="true">&times;</span>
					</button>
				</div>
				<div class="modal-body">
					are you sure you want to delete '<span id="type_name_modal" style="color: red;"></span>' ?
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
					<button id="delete-btn-modal" type="button" class="btn btn-danger">Delete</button>
				</div>
			</div>
		</div>
	</div>
    @endif


    @if($permissions['roles_edit'])
	<!-- Modal -->
	<div class="modal fade" id="exampleModal1" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
		<div class="modal-dialog" role="document">
			<div class="modal-content">
				<div class="modal-header">
					<h5 class="modal-title" id="exampleModalLabel">Edit Role</h5>
					<button type="button" class="close" data-dismiss="modal" aria-label="Close">
						<span aria-hidden="true">&times;</span>
					</button>
				</div>
                <form id="editRoleForm">
                    <div class="modal-body">
                        <div class="form-group col-12">
                            <label for="name">Role Name</label>
                            <input type="text" name="name" class="form-control" id="name" placeholder="Role Name" >
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
                        <button id="edit-btn-modal" type="submit" class="btn btn-success">Submit</button>
                    </div>
                </form>
			</div>
		</div>
	</div>
    @endif


    @if($permissions['roles_add'])
	<!-- Modal -->
	<div class="modal fade" id="exampleModal2" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
		<div class="modal-dialog" role="document">
			<div class="modal-content">
				<div class="modal-header">
					<h5 class="modal-title" id="exampleModalLabel">Add New Role</h5>
					<button type="button" class="close" data-dismiss="modal" aria-label="Close">
						<span aria-hidden="true">&times;</span>
					</button>
				</div>
                <form id="addRoleForm">
                    <div class="modal-body">
                        <div class="form-group col-12">
                            <label for="name">Role Name</label>
                            <input type="text" name="name" class="form-control" placeholder="Role Name" >
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
                        <button id="add-btn-modal" type="submit" class="btn btn-success">Submit</button>
                    </div>
                </form>
			</div>
		</div>
	</div>
    @endif


@endsection





@section('admin_script')


	{{--<script>--}}
        var userObject = JSON.parse(localStorage.getItem("DamaStore_userObject"));

        // blocked
        // bornDate
        // created_at
        // email
        // emailVerifiedAt
        // files
        // firstName
        // id
        // lastActivity
        // lastLoggedIn
        // lastName
        // phone
        // reputation
        // roles
        // token
        // updated_at
        // username
        // verify
        console.log('userObject', userObject);


        var formData = new FormData();
        formData.append('all', true);

        $.ajax({

            type: "POST",
            url: '/api/getAllRoles/0/1000',
            data: formData,
            // mimeType: 'application/json',
            processData: false,
            contentType: false,
            headers: {"Authorization": 'Bearer ' + userObject.token},
            success: function (res) {
                res.data.forEach(function(item, index, arr){

                    var date    = new Date(item.created_at),
                        yr      = date.getFullYear(),
                        month   = date.getMonth() < 10 ? '0' + date.getMonth() : date.getMonth(),
                        day     = date.getDate()  < 10 ? '0' + date.getDate()  : date.getDate(),
                        newDate = yr + '-' + month + '-' + day;

                    var deleteBtn = '';
                    if(item.permission_count > 0){
                        deleteBtn = '<td style="text-align: center;color: red; font-size: 14px;"><i class="fa fa-times" aria-hidden="true"></i> <span> can\'t be deleted</span></td>';
                    }else{
                        deleteBtn = '<td ><button type="button" class="btn btn-block btn-danger" data-id="' + item.id + '" data-name="' + item.name + '" data-desc="' + item.description + '" data-toggle="modal" data-target="#exampleModal">Delete</button></td>';
                    }

                    $('#users_table').append('<tr>' +
                        '<td >'+ item.name +'</td>' +
                        '<td >'+ newDate +'</td>' +
                        {{--'<td ><a href="/admin/products/edit/' + item.id + '"><button type="button" class="btn btn-block btn-success">Edit</button></a></td>' +--}}
                        @if($permissions['assignPermissionsToRole'])
                        '<td ><a class="navigation" href="/admin/role_permissions_list/' + item.id + '"><button type="button" class="btn btn-block btn-success" data-id="' + item.id + '" data-name="' + item.name + '" >Permissions</button></a></td>' +
                        @endif
                        @if($permissions['roles_edit'])
                        '<td ><button type="button" class="btn btn-block btn-success" data-id="' + item.id + '" data-name="' + item.name + '" data-desc="' + item.description + '" data-toggle="modal" data-target="#exampleModal1">Edit</button></td>' +
                        @endif
                        @if($permissions['roles_delete'])
                        deleteBtn +
                        @endif
                        '</tr>');
    pagenition_s();
                });

                console.log('hey');

            },
            error: function (res, response) {
            },
        });

        var role_id;
        $('#exampleModal').on('show.bs.modal', function(e) {

            var id = $(e.relatedTarget).data('id');
    role_id = id;
            var name = $(e.relatedTarget).data('name');
            $('#type_name_modal').text(name);

        });
        $('#delete-btn-modal').click(function (e) {

            e.preventDefault();
            e.stopPropagation();

            var formData = new FormData();

            $.ajax({

                type: "POST",
                url: '/api/deleteRole/' + role_id,
                data: formData,
                // mimeType: 'application/json',
                processData: false,
                contentType: false,
                headers: {"Authorization": 'Bearer ' + userObject.token},
                success: function (res) {
                    $('#exampleModal').hide();
                    if(res.status == 200) {
                        Swal.fire({
                            icon: 'success',
                            title: 'success',
                            text: res.msg,
                        }).then(function() {

                            window.location = "/admin/roles/list";
                        });
                    }else{
                        Swal.fire({
                            icon: 'error',
                            title: 'Oops...',
                            text: 'somthing went wrong',
                        });
                    }
                },
                error: function (res, response) {
                },
            });

        });



    $('#exampleModal1').on('show.bs.modal', function(e) {

        var id = $(e.relatedTarget).data('id');
        role_id = id;
        var name = $(e.relatedTarget).data('name');
        $('#name').val(name);
    });


        $('#editRoleForm').submit(function (e) {

            e.preventDefault();
            e.stopPropagation();

            var formData = new FormData($(this)[0]);

            $.ajax({

                type: "POST",
                url: '/api/editRole/' + role_id,
                data: formData,
                // mimeType: 'application/json',
                processData: false,
                contentType: false,
                headers: {"Authorization": 'Bearer ' + userObject.token},
                success: function (res) {
                    $('#exampleModal').hide();
                    if(res.status == 201) {
                        Swal.fire({
                            icon: 'success',
                            title: 'success',
                            text: res.msg,
                        }).then(function() {
                            window.location = "/admin/roles/list";
                        });
                    }else{
                        Swal.fire({
                            icon: 'error',
                            title: 'Oops...',
                            text: 'somthing went wrong',
                        });
                    }
                },
                error: function (res, response) {
                },
            });

        });


        $('#addRoleForm').submit(function (e) {

            e.preventDefault();
            e.stopPropagation();

            var formData = new FormData($(this)[0]);

            $.ajax({

                type: "POST",
                url: '/api/addRole',
                data: formData,
                // mimeType: 'application/json',
                processData: false,
                contentType: false,
                headers: {"Authorization": 'Bearer ' + userObject.token},
                success: function (res) {
                    $('#exampleModal').hide();
                    if(res.status == 201) {
                        Swal.fire({
                            icon: 'success',
                            title: 'success',
                            text: res.msg,
                        }).then(function() {
                            window.location = "/admin/roles/list";
                        });
                    }else{
                        Swal.fire({
                            icon: 'error',
                            title: 'Oops...',
                            text: 'somthing went wrong',
                        });
                    }
                },
                error: function (res, response) {
                },
            });

        });




@endsection



