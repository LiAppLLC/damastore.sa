@extends('admin.adminHome')


@section('admin_title')

    {{-- Add Products --}}

@endsection


@section('admin_breadcrumb')

    <li class="breadcrumb-item"><a href="#">Home</a></li>
    <li class="breadcrumb-item">categories for Custom products</li>
    <li class="breadcrumb-item active">Add</li>

@endsection

@section('admin_style')

    <style>
        .img-card {
            width: 40%;
            margin-top: 45px;
            margin-bottom: 18px;
        }

    </style>

@endsection

@section('admin_content')

    <?php $locale = App::getLocale(); ?>

    <!-- Main content -->
    <section class="content">
        <div class="container-fluid">
            <div class="row">
                <!-- left column -->

                <div class="col-md-12">
                    <!-- jquery validation -->
                    <div class="card card-primary" style="background-color: rgba(128, 128, 128, 0.301)">
                        <div class="card-header row">
                            <div class="col-md-10">
                                <h3 class="card-title">Categories for Custom products Form <small> </small></h3>
                            </div>
                            <div class="col-md-2">
                                <a type="button" class="btn btn-block btn-warning"
                                    href="{{ route('c_admin_catecories_list') }}">Back</a>
                            </div>
                        </div>
                        <!-- /.card-header -->
                        <!-- form start -->
                        <form action="{{ route('c_admin_categories_add_post') }}" enctype="multipart/form-data"
                            method="post">
                            @csrf
                            <div class="card-body row">
                                <div class="form-group col-12">
                                    <label for="name">Category Name</label>
                                    <input type="text" name="name" class="form-control" id="name"
                                        placeholder="Category Name">
                                </div>
                                <div class="form-group col-12">
                                    <label for="name">Category Name Arabic</label>
                                    <input type="text" name="ar_name" class="form-control" id="ar_name"
                                        placeholder="Category Name Arabic">
                                </div>
                                <div class="form-group col-12">
                                    <label for="description">Category Description</label>
                                    {{-- <input type="text"  class="form-control"  placeholder="Product Description"> --}}
                                    <textarea class="form-control" name="description" id="description" rows="3"
                                        placeholder="Category Description"></textarea>
                                </div>
                                <div class="form-group col-12">
                                    <label for="description">Category Description Arabic</label>
                                    {{-- <input type="text"  class="form-control"  placeholder="Product Description"> --}}
                                    <textarea class="form-control" name="ar_description" id="ar_description" rows="3"
                                        placeholder="Category Description Arabic"></textarea>
                                </div>
                                <div class="form-group col-12">
                                    <label for="Image1">Image</label>
                                    <img src="/images/no_image.png" id="image" class="img-responsive img-card">
                                    <div class="custom-file">
                                        <input type="file" class="custom-file-input" id="cate_photo" name="cate_photo">
                                        <label class="custom-file-label" for="customFile">Choose file</label>
                                    </div>
                                </div>
                                <div class="form-group col-12">
                                    <label for="Image1">SVG</label>
                                    <img src="/images/no_image.png" id="svg" class="img-responsive img-card" />
                                    <div class="custom-file">
                                        <input type="file" class="custom-file-input" id="cate_svg" name="cate_svg">
                                        <label class="custom-file-label" for="customFile">Choose file</label>
                                    </div>
                                </div>
                            </div>
                            <!-- /.card-body -->
                            <div class="card-footer">
                                <button type="submit" class="btn btn-primary">Submit</button>
                            </div>
                        </form>
                    </div>
                    <!-- /.card -->
                </div>
                <!--/.col (left) -->
                <!-- right column -->
                <div class="col-md-6">

                </div>
                <!--/.col (right) -->
            </div>
            <!-- /.row -->
        </div><!-- /.container-fluid -->
    </section>
    <!-- /.content -->
    </div>


@endsection





@section('admin_script')

    $("#cate_photo").change(function(){
    readURL(this, 'image');
    });
    $("#cate_svg").change(function(){
    readURL(this, 'svg');
    });
@endsection
