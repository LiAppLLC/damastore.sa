@extends('admin.adminHome')


@section('admin_title')

    Categories for Custom products List

@endsection


@section('admin_breadcrumb')

    <li class="breadcrumb-item"><a href="#">Home</a></li>
    <li class="breadcrumb-item active">Categories for Custom products List</li>

@endsection

@section('admin_style')

    <style>
        .image-div {
            width: 35px;
            height: 35px;
        }

    </style>

@endsection

<?php
//dd($permissions['products_edit']);
?>

@section('admin_content')


    <div class="card">
        <div class="card-header row">
            <div class="col-md-10">
                <h3 class="card-title">Categories for Custom products List</h3>
            </div>
            <div class="col-md-2">
                <a type="button" class="btn btn-block btn-info" href="{{ route('c_admin_categories_add') }}">Add</a>
            </div>
        </div>

        <!-- /.card-header -->
        <div class="card-body">
            <div class="row">
                <div class="col-md-6">
                    <div id="pagination"></div>
                </div>
                <div class="col-md-6">
                    <input type="text" class="form-control"
                        style="border:0;background-color:#efefef;border-radius:0;margin-bottom:2px;"
                        placeholder="Search all fields." id="search_input">
                </div>
            </div>
            <table id="content" class="table table-bordered table-striped">
                <thead>
                    <tr>

                        <th>name</th>
                        <th>description</th>
                        <th>created_at</th>
                        <th>updated_at</th>
                        <th>Image</th>

                        {{-- @if ($permissions['products_edit']) --}}
                        <th>Edit</th>
                        {{-- @endif
                    @if ($permissions['products_delete']) --}}
                        <th>Delete</th>
                        {{-- @endif --}}
                    </tr>
                </thead>
                <tbody>
                    @foreach ($category as $cate)
                        <tr>
                            <td>{{ $cate->name }}</td>
                            <td>{{ $cate->description }}</td>
                            <td>{{ $cate->created_at }}</td>
                            <td>{{ $cate->updated_at }}</td>
                            <td><img src="/{{ $cate->image }}" width="75px" /></td>
                            @if ($permissions['products_edit'])
                                <td><a href="/admin/Ccategories/edit/{{ $cate->id }}"><button type="button"
                                            class="btn btn-block btn-success">Edit</button></a></td>
                            @endif
                            @if ($permissions['products_delete'])
                                <td><button type="button" class="btn btn-block btn-danger" data-id="{{ $cate->id }}"
                                        data-name="{{ $cate->name }}" data-toggle="modal"
                                        data-target="#exampleModal">Delete</button></td>
                            @endif
                        </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>

    {{-- @if ($permissions['products_delete']) --}}
    <!-- Modal -->
    <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
        aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Delete Alert</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    are you sure you want to delete '<span id="prod_name_modal" style="color: red;"></span>' ?
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
                    <button id="delete-btn-modal" type="button" class="btn btn-danger">Delete</button>
                </div>
            </div>
        </div>
    </div>
    {{-- @endif --}}

@endsection





@section('admin_script')


    {{-- <script> --}}
    var userObject = JSON.parse(localStorage.getItem("DamaStore_userObject"));

    // blocked
    // bornDate
    // created_at
    // email
    // emailVerifiedAt
    // files
    // firstName
    // id
    // lastActivity
    // lastLoggedIn
    // lastName
    // phone
    // reputation
    // roles
    // token
    // updated_at
    // username
    // verify
    //console.log('userObject', userObject);


    var formData = new FormData();
    formData.append('all', true);

    $.ajax({

    type: "get",
    url: '/api/category/0/30',
    data: formData,
    // mimeType: 'application/json',
    processData: false,
    contentType: false,
    headers: {"Authorization": 'Bearer ' + userObject.token},
    success: function (res) {
    res.data.forEach(function(item, index, arr){

    var date = new Date(item.created_at),
    yr = date.getFullYear(),
    month = date.getMonth() < 10 ? '0' + date.getMonth() : date.getMonth(), day=date.getDate() < 10 ? '0' + date.getDate() :
        date.getDate(), newDate=yr + '-' + month + '-' + day; $('#users_table').append('<tr>' +
        '<td>'+ item.name +'</td>' +
        '<td>'+ item.description +'</td>' +
        '<td>'+ item.created_at +'</td>' +
        '<td>'+ item.updated_at +'</td>' +


        {{-- @if ($permissions['products_edit']) --}}
        '<td><a href="/admin/categories/edit/' + item.id + '"><button type="button"
                    class="btn btn-block btn-success">Edit</button></a></td>' +
        {{-- @endif
        @if ($permissions['products_delete']) --}}
        '<td><button type="button" class="btn btn-block btn-danger" data-id="' + item.id + '" data-name="' + item.name + '"
                data-toggle="modal" data-target="#exampleModal">Delete</button></td>' +
        {{-- @endif --}}
        '</tr>');

        });

        //console.log('hey');

        },
        error: function (res, response) {
        },
        });
        // function get_hostname(url) {
        // var m = url.match(/^http:\/\/[^/]+/);
        // return m ? m[0] : null;
        // }

        var prod_id;
        $('#exampleModal').on('show.bs.modal', function(e) {

        var id = $(e.relatedTarget).data('id');
        cate_id = id;
        var name = $(e.relatedTarget).data('name');

        $('#prod_name_modal').text(name);

        });
        $('#delete-btn-modal').click(function (e) {

        e.preventDefault();
        e.stopPropagation();

        var formData = new FormData();
        formData.append('category_id', cate_id);

        $.ajax({

        type: "POST",
        url: '/api/deleteCategory/'+cate_id,

        data: formData,
        // mimeType: 'application/json',
        processData: false,
        contentType: false,
        headers: {"Authorization": 'Bearer ' + userObject.token},
        success: function (res) {
        //console.log('res'+res.Status);
        $('#exampleModal').hide();

        if(res.success === 'done') {
        Swal.fire({
        icon: 'success',
        title: 'success',
        text: res.msg,
        }).then(function() {

        window.location = "/admin/categories/list";
        });
        }else{
        Swal.fire({
        icon: 'error',
        title: 'Oops...',
        text: 'somthing went wrong',
        });
        }
        },
        error: function (res, response) {
        },
        });

        });



    @endsection
