@extends('admin.adminHome')


@section('admin_title')

    {{-- Add Products --}}

@endsection


@section('admin_breadcrumb')

    <li class="breadcrumb-item"><a class="navigation" href="#">Home</a></li>
    <li class="breadcrumb-item">products</li>
    <li class="breadcrumb-item active">Add</li>
@endsection

@section('admin_style')

    <style>
#snackbar {
  visibility: hidden;
  min-width: 250px;
  margin-left: 50%;
  background-color: #333;
  color: #fff;
  text-align: center;
  border-radius: 2px;
  padding: 16px;
  position: fixed;
  z-index: 1;
  bottom: 30px;
  font-size: 17px;
}

#snackbar.show {
  visibility: visible;
  -webkit-animation: fadein 0.8s, fadeout 0.5s 4.5s;
  animation: fadein 0.5s, fadeout 0.5s 4.5s;
}

@-webkit-keyframes fadein {
  from {bottom: 0; opacity: 0;}
  to {bottom: 30px; opacity: 1;}
}

@keyframes fadein {
  from {bottom: 0; opacity: 0;}
  to {bottom: 30px; opacity: 1;}
}

@-webkit-keyframes fadeout {
  from {bottom: 30px; opacity: 1;}
  to {bottom: 0; opacity: 0;}
}

@keyframes fadeout {
  from {bottom: 30px; opacity: 1;}
  to {bottom: 0; opacity: 0;}
}
        .upload_img{
            object-fit:cover;width:100px;height:100px;
            margin: 5px;
            border: 3px solid white;
            border-radius: 15px
        }
        .center_p{
            position: absolute;
            top:50%;
            left:50%;
            text-align: center;
            transform: translate(-50%,-50%)
        }
        .img-card {
            width: 40%;
            margin-top: 45px;
            margin-bottom: 18px;
        }

        .form-group {
            margin-left: 20px;
        }

        .form-control {
            width: 90%;
        }

        label {
            margin-top: 15px;
        }

        .input-img {
            width: 93%;
            background: #ddd;
            height: 150px;
            /* margin-bottom: 20px; */
        }

        .input-img2 {
            width: 93%;
            background: #ddd;
            min-height: 100px;
            position: relative;corsur:pointer;
        }

    </style>

@endsection

@section('admin_content')

    <?php $locale = App::getLocale(); ?>
    <h1>Add New Custom Product:</h1>
    <hr>
    <form id="form_product" enctype="multipart/form-data">
        <div class="row m-0">
            <div class="col-md-4 m-0 form-group">
                <label>Name (en)</label>
                <input name="name" class="form-control form-control-lg" type="text" required>
                <label>Name (ar)</label>
                <input name="ar_name" class="form-control form-control-lg" type="text" required>
                <label>description short (en)</label>
                <input name="short_description" class="form-control form-control-lg" type="text" required>
                <label>description short (ar)</label>
                <input name="ar_short_description" class="form-control form-control-lg" type="text" required>
                <label>description (en)</label>
                <input name="description" class="form-control form-control-lg" type="text" required>
                <label>description (ar)</label>
                <input name="ar_description" class="form-control form-control-lg" type="text" required>
            </div>
            <div class="col-md-5 m-0">
                <div class="row">
                    <div class="col-12">
                        <label>Price, if any</label>
                        <div class="form-check">
                            <div class="row">
                                <div class="col-4">
                                    <div class="row">
                                    <div class="col-6"><label>false</label>
                                        <input type="radio" checked value="0" name="is_price"></div>
                                    <div class="col-6">
                                        <label>true</label>
                                        <input type="radio" value="1" name="is_price">
                                    </div>
                                </div>
                                </div>
                                <div class="col-8">
                                    <input name="price" class="form-control form-control-lg" type="number">
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-12">
                        <div class="row">
                            <div class="col-4"><label>Available</label> :</div>
                            <div class="col-4"><label>false</label>
                                <input type="radio" checked value="0" name="available"></div>
                            <div class="col-4">
                                <label>true</label>
                                <input type="radio" value="1" name="available">
                            </div>
                        </div>
                    </div>
                </div>
                <label style="margin-top: 0px;">Images Upload</label>
                <div class="input-img" id="img" style="position: relative;corsur:pointer;">
                    <h5 class="center_p">Click to upload image</h5>
                    <img  style="object-fit:cover;width:100%;height:100%;">
                </div>
                <input type="file" id="input_img" name="image">
                <div class="input-img2" id="img2">
                    <h5 class="center_p">Click to upload images</h5>
                </div>
                <input type="file" multiple id="input_img2" name="image_gallery[]">
                <label>The Time required to process order and Build the Product</label>
                <input name="days" class="form-control form-control-lg" style="width: 93%;" type="text" placeholder="10 Days">

            </div>
            <div class="col-md-3 m-0">
                <label>Shipping Costs</label>
                <input name="shipping_costs" class="form-control form-control-lg" type="number">

                <label>Category</label>
                <select name="category_id" onchange="get_type(this.value)" class="form-control form-control-lg">
                    @foreach (App\Ccategory::all() as $item )
                        <option value="{{$item->id}}">{{$item->name}} -- {{$item->ar_name}}</option>
                    @endforeach
                </select>

                <label>Type</label>
                <select id="type" name="type_id" class="form-control form-control-lg"></select>

                <label>Product Code</label>
                <input name="product_code" class="form-control form-control-lg" type="text">

                <label>Supplier</label>
                <input name="supplier" class="form-control form-control-lg" type="text">
                <br>
                <div style="display: relative;">
                    <button class="btn btn-primary" type="submit" id="submit_btn" >submit</button>
                </div>
            </div>
        </div>
        <div id="snackbar"></div>
    </form>
@endsection
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
<script>
     function myFunction(response) {
     var x = document.getElementById("snackbar");
     x.className = "show";
     $('#snackbar').html('');
     $('#snackbar').append('<span>'+JSON.stringify(response)+'</span>');
    setTimeout(function(){ x.className = x.className.replace("show", ""); }, 6000);
     }
$(document).ready(function(){
    $('#form_product').submit(function (e) {
        e.preventDefault();
        var form = $('#form_product')[0];
        var formData = new FormData(form);
        $.ajax({
        type: "post",
        url: "{{route('CustomProduct.store')}}",
        data: formData,
        processData: false,
        contentType: false,
        success: function (response) {
           console.log(JSON.stringify(response));
           if(response.success){
            myFunction(response.msg);
           }else{
               var msg = "";
               for(var i in response[1]){
                msg +=response[1][i]+'<br>';
               }
            myFunction(msg);
           }
        }
       });
    });

    $('#img').on('click', function () {
       $('#input_img').click();
    });
    $('#input_img').change(function (e) {
        e.preventDefault();
        $('#img img').attr("src",URL.createObjectURL(e.target.files[0]));
    });
    $('#img2').on('click', function () {
       $('#input_img2').click();
    });
    $('#input_img2').change(function (e) {
        e.preventDefault();
        $('#img2').html('');
        for(var i in e.target.files)
        {
            $('#img2').append( "<img src='"+URL.createObjectURL(e.target.files[i])+"' class='upload_img'/>" );
        }
    });
});

function get_type(id) {
        let userObject = JSON.parse(localStorage.getItem("DamaStore_userObject"));
        let formData = new FormData();
        formData.append('category_id', id)
        $('#type').html('');
        $.ajax({
            type: "POST",
            url: '{{route('CType')}}',
            data: formData,
            // mimeType: 'application/json',
            headers: {"Authorization": 'Bearer ' + userObject.token},
            processData: false,
            contentType: false,
            success: function (res) {
                if (res.status == 200) {
                    console.log('Type', res);
                    res.data.forEach(function (item, index, arr) {
                        if (index == 0) {
                            $('#type').append('<option selected="selected" value="' + item.id + '">' + item.name + '</option>');
                        } else {
                            $('#type').append('<option value="' + item.id + '">' + item.name + '</option>');
                        }
                    });
                }
            },
            error: function (res, response) {
                // $( ".loading" ).remove();
            },
        });

    }

</script>



