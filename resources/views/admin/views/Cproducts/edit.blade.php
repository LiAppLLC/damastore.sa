


@extends('admin.adminHome')


@section('admin_title')

    {{--Add Products--}}

@endsection


@section('admin_breadcrumb')

	<li clclickass="breadcrumb-item"><a class="navigation" href="#">Home</a></li>
	<li class="breadcrumb-item">products</li>
	<li class="breadcrumb-item active">Edit</li>

@endsection

@section('admin_style')

    <style>

        .img-card{
            width: 40%;
            margin-top: 45px;
            margin-bottom: 18px;
        }

    </style>

@endsection

@section('admin_content')

    <?php
    $locale = App::getLocale();
    ?>

    <!-- Main content -->
    <section class="content">
        <div class="container-fluid">
            <div class="row">
                <!-- left column -->
                <div class="col-md-12">
                    <!-- jquery validation -->
                    <div class="card card-primary">
                        <div class="card-header">
                            <h3 class="card-title">Products Form <small>Edit product</small></h3>
                        </div>
                        <!-- /.card-header -->
                        <!-- form start -->
                        <?php
//                        dd($product);
                        ?>
                        <form role="form" id="ProductEditForm">
                            <div class="card-body row">
                                <div class="form-group col-12">
                                    <label for="name">Product Name</label>
                                    <input type="text" name="name" class="form-control" id="name" placeholder="Product Name">
                                </div>
                                <div class="form-group col-12">
                                    <label for="name">Product Name Arabic</label>
                                    <input type="text" name="ar_name" class="form-control" id="ar_name" placeholder="Product Name">
                                </div>
                                <div class="form-group col-12">
                                    <label for="supplier">Supplier</label>
                                    <input type="text" name="supplier" class="form-control" id="supplier" placeholder="supplier name">
                                </div>
                                <div class="form-group col-12">
                                    <label for="description">Product Short Description</label>
                                    {{--<input type="text"  class="form-control"  placeholder="Product Description">--}}
                                    <textarea class="form-control" name="short_description" id="short_description" rows="3" placeholder="Product Description"></textarea>
                                </div>
                                <div class="form-group col-12">
                                    <label for="description">Product Short Description Arabic</label>
                                    {{--<input type="text"  class="form-control"  placeholder="Product Description">--}}
                                    <textarea class="form-control" name="ar_short_description" id="ar_short_description" rows="3" placeholder="Product Description arabic short"></textarea>
                                </div>
                                <div class="form-group col-12">
                                    <label for="description">Product Description</label>
                                    {{--<input type="text"  class="form-control"  placeholder="Product Description">--}}
                                    <textarea class="form-control" name="description" id="description" rows="3" placeholder="Product Description"></textarea>
                                </div>

                                <div class="form-group col-12">
                                    <label for="description">Product Description Arabic</label>
                                    {{--<input type="text"  class="form-control"  placeholder="Product Description">--}}
                                    <textarea class="form-control" name="ar_description" id="ar_description" rows="3" placeholder="Product Description"></textarea>
                                </div>
                                {{-- <script>
                                CKEDITOR.replace( 'description' );
        CKEDITOR.replace( 'ar_description' );
                                </script> --}}
                                <div class="form-group col-12">
                                    <label for="description">Product Code</label>
                                    <input type="text" name="product_code" class="form-control" id="product_code" placeholder="Product Code">
                                </div>

                                <div class="form-group col-3">
                                    <label for="description">Price</label>
                                    <input type="number" name="price" class="form-control" id="price" placeholder="Price" >
                                </div>
                                <div class="form-group col-3">
                                    <label for="description">Sales Price</label>
                                    <input type="number" name="salesPrice" class="form-control" id="salesPrice" placeholder="SalesPrice" >
                                </div>
                                <div class="form-group col-6">
                                    <label for="description">Quantity</label>
                                    <input type="number" name="quantity" class="form-control" id="quantity" placeholder="Quantity" >
                                </div>
                                <div class="form-group col-6">
                                    <label for="description">Attribute :   </label>
                                    <a style="float:right;" onclick="add_attributes_to_table()" class="btn btn-primary">+</a>
                                    <div class="form-group col-6">
                                        <label for="description">Select attribute</label>
                                        <select name="attribute_name_add" class="form-control" id="attribute_name_add" placeholder="Size">
                                        <option disabled selected>Select attribute</option>
                                            @foreach($Attributes as $attribute)
                                            <option value="{{ $attribute->id }}">{{ $attribute->name }} -- {{ $attribute->ar_name }}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                    <div class="form-group col-6">
                                        <label for="description">value for attribute</label>
                                        <input type="text" name="attribute_value_add" class="form-control" id="attribute_value_add" placeholder="value for attribute">
                                        <input type="text" name="attribute_ar_value_add" class="form-control" id="attribute_ar_value_add" placeholder="Arabic value for attribute">

                                    </div>

                                </div>
                                <div class="form-group col-6">
                                    <table class="table" id="table_attributes">
                                    <tr>
                                        <th>Id</th>
                                   <th>Attribute</th>
                                   <th>Arabic Attribute</th>
                                   <th>Value</th>
                                   <th>Arabic Value</th>
                                   <th>Actions</th>
                                    </tr>
                                    <tbody id="table_attributes_tbody">
                                    </tbody>
                                    </table>
                                    </div>
                                    <script>
     var userObject = JSON.parse(localStorage.getItem("DamaStore_userObject"));
     var url = window.location.pathname;
     var product_id = url.substring(url.lastIndexOf('/') + 1);
     let table= $('#table_attributes_tbody');
                                    function add_attributes_to_table(){
                                        let attribute_id = $('#attribute_name_add').val();
                                        let value = $('#attribute_value_add').val();
                                        let ar_value = $('#attribute_ar_value_add').val();
                                        if(!value){
                                        return false;
                                        }else{
            formData=new FormData();
            formData.append('attribute_id',attribute_id);
            formData.append('product_id',product_id);
            formData.append('value',value);
            formData.append('ar_value',ar_value);
            console.log(formData);
            $.ajax({

                type: "POST",
                url: '{{route('AttributeAddForProduct')}}',
                data: formData,
                mimeType: 'application/json',
                headers: {"Authorization": 'Bearer ' + userObject.token},
                processData: false,
                contentType: false,
                success: function (res) {
                    if(res.status == 200) {
                        Swal.fire({
                            icon: 'success',
                            title: 'success',
                            text: res.msg,
                        }).then(function() {
                            location.reload();
                        });

                    }else{
                        Swal.fire({
                            icon: 'error',
                            title: 'Oops...',
                            text: 'somthing went wrong',
                        });
                    }

                },
                error: function (res, response) {
alert('some erorr');
                },
            });

                                        }
                                    }

                                        </script>
                                <div class="form-group col-6">
                                    <label for="description">Shipping costs</label>
                                    <input type="number" name="shipping_costs" class="form-control" id="shipping_costs" placeholder="shipping_costs" >
                                </div>
                                <div class="form-group col-6">
                                </div>

                                <div class="form-group col-6">
                                    <label>Category</label>
                                    <select name="category" id="category" class="form-control" style="width: 100%;">
                                        {{--<option selected="selected" value="1">Category1</option>--}}
                                        {{--<option value="2">Category2</option>--}}
                                        {{--<option value="3">Category3</option>--}}
                                        {{--<option value="4">Category4</option>--}}
                                        {{--<option value="5">Category5</option>--}}
                                        {{--<option value="6">Category6</option>--}}
                                        {{--<option value="7">Category7</option>--}}
                                    </select>
                                </div>

                                <div class="form-group col-6">
                                    <label>Type</label>
                                    <select name="type" id="type" class="form-control" style="width: 100%;">
                                        {{--<option selected="selected" value="1">Type1</option>--}}
                                        {{--<option value="2">Type2</option>--}}
                                        {{--<option value="3">Type3</option>--}}
                                        {{--<option value="4">Type4</option>--}}
                                        {{--<option value="5">Type5</option>--}}
                                        {{--<option value="6">Type6</option>--}}
                                        {{--<option value="7">Type7</option>--}}
                                    </select>
                                </div>

                                <label for="Image1">Image</label>
                                <img src="" id="image" class="img-responsive img-card">
                                <div class="custom-file">
                                    <input type="file" class="custom-file-input" id="product_photo" name="product_photo">
                                    <label class="custom-file-label" for="customFile">Choose file</label>
                                </div>


                            </div>
                            <!-- /.card-body -->
                            <div class="card-footer">
                                <button type="submit" id="Productbutton" class="btn btn-primary">Submit</button>
                            </div>

                        <div class="form-group col-12">
                            <div style="padding:15px;border:1px solid rgba(236, 235, 235, 1);margin:5px;background-color:rgba(236, 235, 235, 0.274);display:none;" id="productnotifcation"></div>
                        </div>
                        </form>
                    </div>
                    <!-- /.card -->
                </div>
                <!--/.col (left) -->
                <!-- right column -->
                <div class="col-md-6">

                </div>
                <!--/.col (right) -->
            </div>
            <!-- /.row -->
        </div><!-- /.container-fluid -->
    </section>
    <!-- /.content -->
    </div>


@endsection





@section('admin_script')


	{{--<script>--}}
        var userObject = JSON.parse(localStorage.getItem("DamaStore_userObject"));

        // blocked
        // bornDate
        // created_at
        // email
        // emailVerifiedAt
        // files
        // firstName
        // id
        // lastActivity
        // lastLoggedIn
        // lastName
        // phone
        // reputation
        // roles
        // token
        // updated_at
        // username
        // verify

        //console.log('userObject', userObject);


        // =============================================================================================================
        // get Data of product

        var url = window.location.pathname;
        var id = url.substring(url.lastIndexOf('/') + 1);
        var formData_getProd = new FormData();
        formData_getProd.append('id', id)

        $('#How-It-Works').append('<div class="loading">Loading&#8230;</div>');
        $.ajax({

            type: "POST",
            url: '{{route('get_products_by_id')}}',
            data: formData_getProd,
            // mimeType: 'application/json',
            headers: {"Authorization": 'Bearer ' + userObject.token},
            processData: false,
            contentType: false,
            success: function (res) {
                if(res.status == 200) {
                    console.log('get_products_by_id', res);
                    $('#unit').val(res.data.unit);
                    $('#shipping_costs').val(res.data.shipping_costs);
                    $('#quantity').val(res.data.quantity);
                    $('#price').val(res.data.price);
                    $('#salesPrice').val(res.data.price_sale);
                    $('#product_code').val(res.data.product_code);
                    $('#supplier').val(res.data.supplier);
                    CKEDITOR.replace( 'description' );
                    CKEDITOR.replace( 'ar_description' );
                    CKEDITOR.instances['description'].setData(res.data.description);
                    CKEDITOR.instances['ar_description'].setData(res.data.ar_description);
                    $('#short_description').text(res.data.short_description);
                    $('#ar_short_description').text(res.data.ar_short_description);
                    $('#name').val(res.data.name);
                    $('#ar_name').val(res.data.ar_name);
                    console.log(res.data.attributes);
                    for (var key in res.data.attributes) {
                        $('#table_attributes_tbody').append('<tr id="tr_'+res.data.attributes[key].id+'"><td>'+res.data.attributes[key].id+'</td><td>'+res.data.attributes[key].name+'</td><td>'+res.data.attributes[key].ar_name+'</td><td>'+res.data.attributes[key].value+'</td><td>'+res.data.attributes[key].ar_value+'</td><td><a class="btn btn-danger" onclick="delete_attr('+res.data.attributes[key].id+')">×</a></td></tr>')
                    }
                    var image = '';
                    if(res.data.image) {
                        image = get_hostname($(location).attr("href")) + '/images/' + res.data.image.path;
                    }
                    // else{
                    //     image = 'https://static.thenounproject.com/png/2999524-200.png';
                    // }
                    $('#image').attr('src', image);

                    var curr_category_id = res.data.category_id;
                    var curr_type_id = res.data.type_id;


                    $.ajax({

                        type: "POST",
                        url: '{{route('category')}}',
                        data: null,
                        // mimeType: 'application/json',
                        headers: {"Authorization": 'Bearer ' + userObject.token},
                        processData: false,
                        contentType: false,
                        success: function (res) {
                            if(res.status == 200) {
                                //console.log('category', res);
                                res.data.forEach(function(item, index, arr) {
                                    if(curr_category_id == item.id){
                                        $('#category').append('<option selected="selected" value="' + item.id + '">' + item.name + '</option>');
                                    }else {
                                        $('#category').append('<option value="' + item.id + '">' + item.name + '</option>');
                                    }
                                });
                            }
                        },
                        error: function (res, response) {
                        },
                    });


                    $.ajax({

                        type: "POST",
                        url: '{{route('Type')}}',
                        data: null,
                        // mimeType: 'application/json',
                        headers: {"Authorization": 'Bearer ' + userObject.token},
                        processData: false,
                        contentType: false,
                        success: function (res) {
                            if(res.status == 200) {
                                //console.log('Type', res);
                                res.data.forEach(function(item, index, arr) {
                                    if(curr_type_id == item.id){
                                        $('#type').append('<option selected="selected" value="' + item.id + '">' + item.name + '</option>');
                                    }else {
                                        $('#type').append('<option value="' + item.id + '">' + item.name + '</option>');
                                    }
                                });
                            }
                        },
                        error: function (res, response) {
                        },
                    });
                }
            },
            error: function (res, response) {
                $( ".loading" ).remove();
            },
        });

        // =============================================================================================================
        $("#product_photo").change(function(){
            readURL(this, 'image');
        });

        $('#ProductEditForm').submit(function (e) {
            $('#Productbutton').prop('disabled', true);
            $('#productnotifcation').css('background-color','rgba(236, 235, 235, 0.274)');
            $('#productnotifcation').css('color','black');
            $('#productnotifcation').text('Loading ...')
            CKEDITOR.instances['ar_description'].updateElement();
            CKEDITOR.instances['description'].updateElement();
            var formData = new FormData($(this)[0]);
            formData.append('user_id', userObject.id);
            formData.append('prod_id', id);
            e.preventDefault();
            e.stopPropagation();
            $('#How-It-Works').append('<div class="loading">Loading&#8230;</div>');
            $.ajax({

                type: "POST",
                url: '{{route('ProductEdit')}}',
                data: formData,
                // mimeType: 'application/json',
                headers: {"Authorization": 'Bearer ' + userObject.token},
                processData: false,
                contentType: false,
                success: function (res) {
                    if(res.status == 200) {
                        Swal.fire({
                            icon: 'success',
                            title: 'success',
                            text: res.msg,
                        }).then(function() {
                            window.location = "/admin/products/list";
                        });

                    }else{
                        {{-- Swal.fire({
                            icon: 'error',
                            title: 'Oops...',
                            text: 'somthing went wrong',
                        }); --}}
                        $('#Productbutton').prop('disabled', false);
                        $('#productnotifcation').show('slow');
                        $('#productnotifcation').css('background-color','red');
                        $('#productnotifcation').css('color','white');
                        $('#productnotifcation').html(res[0]+'<br>'+JSON.stringify(res[1]))
                    }

                },
                error: function (res, response) {
                    $('#Productbutton').prop('disabled', false);
                    $('#productnotifcation').text('please input all fields or login and return!')
                },
            });


        });
        //===========================subhe


@endsection

<script type="text/javascript">

  function delete_attr(id){
    var userObject = JSON.parse(localStorage.getItem("DamaStore_userObject"));
var formData = new FormData();
formData.append('att_id', id);
$.ajax({
    type: "POST",
    url: '{{route('AttributeDeleteForProduct')}}',
    data: formData,
    // mimeType: 'application/json',
    headers: {"Authorization": 'Bearer ' + userObject.token},
    processData: false,
    contentType: false,
    success: function (res) {
        if(res.status == 200) {
            Swal.fire({
                icon: 'success',
                title: 'success',
                text: res.msg,
            }).then(function() {
                $('#tr_'+id).remove();
            });

        }else{
            Swal.fire({
                icon: 'error',
                title: 'Oops...',
                text: 'somthing went wrong',
            });
        }

    },
    error: function (res, response) {

    }
    });
                        };

</script>

