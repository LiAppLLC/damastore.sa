


@extends('admin.adminHome')


@section('admin_title')

	Dashboard

@endsection


@section('admin_breadcrumb')

	<li class="breadcrumb-item"><a class="navigation" href="#">Home</a></li>
	<li class="breadcrumb-item active">Dashboard v1</li>

@endsection




@section('admin_content')

    <?php
    $locale = App::getLocale();
    ?>
<div style="margin: 0 auto;width:50%;">
    <img src="/assets/base/img/layout/logos/logo-5.png" alt="DamaStore" class="c-desktop-logo-inverse dlogo" style="width: 100%;">
</div>
<span data-href="/CsvGetter" id="export" class="btn btn-success btn-sm" onclick="exportTasks(event.target);">Export</span>

<script>
    function exportTasks(_this) {
       let _url = $(_this).data('href');
       window.location.href = _url;
    }
 </script>
@endsection

@section('admin_script')


	{{--<script>--}}
        var userObject = JSON.parse(localStorage.getItem("DamaStore_userObject"));
		if(userObject){
			$('#username').val(userObject.username);
			$('#firstName').val(userObject.firstName);
			$('#lastName').val(userObject.lastName);
			$('#bornDate').val(userObject.bornDate);
			$('#email').val(userObject.email);
			$('#phone').val(userObject.phone);
		}
        // blocked
        // bornDate
        // created_at
        // email
        // emailVerifiedAt
        // files
        // firstName
        // id
        // lastActivity
        // lastLoggedIn
        // lastName
        // phone
        // reputation
        // roles
        // token
        // updated_at
        // username
        // verify
        console.log('userObject', userObject);


        $('#editUserForm').submit(function (e) {

            var formData = new FormData($(this)[0]);
            e.preventDefault();
            e.stopPropagation();

            $('#How-It-Works').append('<div class="loading">Loading&#8230;</div>');
            $.ajax({

                type: "POST",
                url: '{{route('editUser')}}',
                data: formData,
                // mimeType: 'application/json',
                processData: false,
                contentType: false,
                headers: {"Authorization": 'Bearer ' + userObject.token},
                success: function (res) {
                    if(res.status == 201) {
                        Swal.fire({
                            icon: 'success',
                            title: 'success',
                            text: 'your profile has been edited successfuly, thanks for using our services',
                        }).then(function() {
                            localStorage.setItem("DamaStore_userObject", JSON.stringify(res.data));
                            window.location = "/CustomerDashboard";
                        });

                    }
                    if(res.status == 400) {
                        // console.log(res.data.validator);
                        Swal.fire({
                            icon: 'error',
                            title: 'Oops...',
                            text: res.data.validator,
                        });
                    }

                },
                error: function (res, response) {
                    $( ".loading" ).remove();
                },
            });


        });



@endsection



