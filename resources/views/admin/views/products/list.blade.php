@extends('admin.adminHome')


@section('admin_title')

    Products

@endsection


@section('admin_breadcrumb')

    <li class="breadcrumb-item"><a class="navigation" href="#">Home</a></li>
    <li class="breadcrumb-item active">Products</li>

<br>{{session('currency')}}
@endsection

@section('admin_style')

    <style>
        .image-div {
            width: 35px;
            height: 35px;
        }
    </style>

@endsection

<?php
//dd($permissions['products_edit']);
?>

@section('admin_content')


    <div class="card">
        <style>
            .gallery_class {
                display: none;
                width: 80%;
                padding: 50px;
                margin: 0 auto;
                background-color: white;
                border: 1px solid gray;
                box-shadow: 0 0 20px gray;
                position: absolute;
                top: 50%;
                left: 50%;
                z-index: 1035;
                transform: translate(-50%, -50%);
            }

            .close_gallery {
                position: absolute;
                top: 3px;
                right: 3px;
                background-color: transparent;
                font-size: 15px;
                border: 1px solid gray;
                transition: all .2s;
            }

            .close_gallery:hover {
                background-color: red;
                color: white;
            }

            .images_gallery {
                box-shadow: 0 0 10px gray;
                display: inline-block;
                overflow:hidden;
                width: 50px;
                height: 75px;
                margin: 2px;
                border: 1px solid gray;
                border-radius: 5px;
                text-align: center;
            }

            .images_gallery img {
                width: 50px;
                height: 50px;
                padding:0;
                margin: 0;
                object-fit: cover;
            }

            .images_gallery span{
                cursor: pointer;
                height:20px;
                width:100%;
                font-size: 20px;
                padding:0;
                margin: 0;
                text-decoration: none;
                background-color: white;
                color: red;
            }
        </style>
        <input type="hidden" id="check" value="0">
        <div class="gallery_class"><h3 style="width: 100%;border-bottom: 1px solid gray">Add Gallery</h3>
            <button type="button" class="close_gallery" onclick="close_function()">
                <span>&times;</span>
            </button>
            <br>
            <input type="hidden" name="_token" value="{{ csrf_token() }}">
            <input type="hidden" id="product_id_form" value="">
            <div class="form-group">
                <label>Alt Title:</label>
                <input type="text" name="title" id="title_images" class="form-control" placeholder="Add Title">
            </div>
            <div class="form-group">
                <label>Image:</label>
                <input type="file" name="image" id="file_image" class="form-control">
            </div>
            <div class="form-group">
                <button class="btn btn-success upload-image" onclick="add_image()">Upload Image</button>
                <div id="loading_div" style="display:none;padding:10px;background-color: #ccc"></div>
            </div>
            <div class="alert" id="images_class" style="display:block"></div>
        </div>
        <div class="card-header">
            <h3 class="card-title">Products List</h3>
        </div>
        <!-- /.card-header -->
        <div class="card-body">
            <div class="row">
                <div class="col-md-6">
                    <div id="pagination"></div>
                </div>
                <div class="col-md-6">
                    <input type="text" class="form-control"
                           style="border:0;background-color:#efefef;border-radius:0;margin-bottom:2px;"
                           placeholder="Search all fields." id="search_input">
                </div>
            </div>
            <div style="width: 100%;overflow:auto;">
            <table id="content" class="table table-bordered table-striped">
                <thead>
                <tr>
                    <th>Product Name</th>
                    <th>Description</th>
                    <th>Product Code</th>
                    <th>Image</th>
                    {{-- <th>Unit</th>
                    <th>Size</th> --}}
                    <th>Price</th>
                    <th>Price Sale</th>
                    <th>Quantity</th>
                    <th>Category</th>
                    {{--                    <th>Created At</th>--}}
                    <th>Gallery</th>
                    @if($permissions['products_edit'])
                        <th>Edit</th>
                    @endif
                    @if($permissions['products_delete'])
                        <th>Delete</th>
                    @endif
                </tr>
                </thead>
                <tbody>
                @foreach($products as $product)
                    <tr>
                        <td>{{$product->name}}</td>
                        <td>{{$product->short_description}}</td>
                        <td>{{$product->product_code}}</td>
                        <td><img width="75px" src="/images/{{$product->getImageUrl()}}"></td>
                        {{-- <td>{{$product->unit}}</td>
                        <td>{{$product->size}}</td> --}}
                        <td>{{$product->price}}</td>
                        <td>{{$product->price_sale}}</td>
                        <td>{{$product->quantity}}</td>
                        <td>{{$product->category->name}}</td>
                        {{--                        <td>{{$product->created_at}}</td>--}}
                        <td><a class="navigation" onclick="add_gallery('{{$product->id}}')">
                                <button type="button" class="btn btn-block btn-success"> <i class="fas fa-images"></i></button>
                            </a></td>
                        @if($permissions['products_edit'])
                            <td><a class="navigation" href="/admin/products/edit/{{$product->id}}">
                                    <button type="button" class="btn btn-block btn-success"> <i class="fas fa-edit"></i></button>
                                </a></td>
                        @endif
                        @if($permissions['products_delete'])
                            <td>
                                <button type="button" class="btn btn-block btn-danger"
                                        onclick="delete_product('{{$product->id}}')" data-id="{{$product->id}}"
                                        data-name="{{$product->name}}" data-toggle="modal" data-target="#exampleModal">
                                    <i class="fas fa-trash-alt"></i>
                                </button>
                            </td>
                        @endif
                    </tr>
                    @php($count = $loop->count)
                @endforeach
                </tbody>
            </table>
            </div>
            <br>
            <div class="row">
                <div class="col-md-12">

                    <h6>Results : {{$count ?? 'No Results'}}</h6>

                </div>
            </div>
        </div>
    </div>


@endsection

@section('admin_script')

@endsection
<script>
    let userObject = JSON.parse(localStorage.getItem("DamaStore_userObject"));

    function add_gallery(id) {
        $('.gallery_class').show('show');
        $('#product_id_form').val(id);
        get_all_gallery(id);
    }

    function get_all_gallery(id) {
        const formData = new FormData()
        formData.append('_token', "{{ csrf_token() }}")
        formData.append('product_id', id)
        let div_image = $('#images_class');
        div_image.html('');
        $.ajax({
            type: "POST",
            url: "{{ route('ajaxImageGet') }}",
            data: formData,
            processData: false,
            contentType: false,
            success: function (res) {
                //console.log(res);
                for (i in res.data) {
                    //console.log(res.data[i].path);

                    let image = '<div class="images_gallery"><img src="/' + res.data[i].path + '"/><span onclick="deleteImage_gallery(' + res.data[i].id + ','+id+')"><span>&times;</span></span></div>';
                    div_image.append(image);
                }

            },
            error: function (res, response) {

            },
        });
    }
    function deleteImage_gallery(id,id_pro){
        let loading_div=$('#loading_div');
        const formData = new FormData()
        formData.append('_token', "{{ csrf_token() }}")
        formData.append('image_id', id)
        let div_image = $('#images_class');
        div_image.html('');
        $.ajax({
            type: "POST",
            url: "{{ route('ajaxImageDelete') }}",
            data: formData,
            processData: false,
            contentType: false,
            success: function (res) {
                //console.log(res);
                loading_div.text('Delete Image successfully !');
                loading_div.css('background-color','green');
                        loading_div.show('slow');
                get_all_gallery(id_pro);
            },
            error: function (res, response) {
                loading_div.text('Error !');
                loading_div.css('background-color','red');
                        loading_div.hide('slow');
            },
        });
    }
    function close_function() {
        $('.gallery_class').hide('slow');
    }

    function add_image() {
        let loading_div=$('#loading_div');
        loading_div.show('slow');
        loading_div.text('Loading ...');
        let product_id_form = $('#product_id_form').val();
        let title_image = $('#title_images').val();
        let file_image = $('#file_image')[0].files[0];
        var formData = new FormData()
        formData.append('_token', "{{ csrf_token() }}")
        formData.append('product_id', product_id_form)
        formData.append('title', title_image)
        formData.append('image', file_image)
        //console.log(product_id_form, title_image, file_image);
        $.ajax({
            type: "POST",
            url: "{{ route('ajaxImageUpload') }}",
            data: formData,
            processData: false,
            contentType: false,
            success: function (res) {
                if (res.success === 'done') {
                    // Swal.fire({
                    //     icon: 'success',
                    //     title: 'success',
                    //     text: res.msg,
                    // }).then(function () {
                        $('#title_images').val('');
                        loading_div.text('Uploaded Images successfully !');
                        loading_div.css('background-color','green');
                        loading_div.show('slow');
                        get_all_gallery(product_id_form);
                    // });
                } else {
                    Swal.fire({
                        icon: 'error',
                        title: 'Oops...',
                        text: 'somthing went wrong',
                    });
                    loading_div.show('slow');
                    loading_div.css('background-color','red');
                    loading_div.text(JSON.stringify(res));
                }
            },
            error: function (res, response) {
                loading_div.show('slow');
                loading_div.css('background-color','red');
                loading_div.text(JSON.stringify(res));
            },
        });
    }

    @if($permissions['products_delete'])
    async function delete_product(id) {
        let formData = new FormData();
        formData.append('product_id', id);
        Swal.fire({
            title: 'Are you sure?',
            text: "You won't be able to revert this!",
            icon: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Yes, delete it!'
        }).then((result) => {
            if (result.isConfirmed) {
                $.ajax({
                    type: "POST",
                    url: '/api/ProductDelete',
                    data: formData,
                    // mimeType: 'application/json',
                    processData: false,
                    contentType: false,
                    headers: {"Authorization": 'Bearer ' + userObject.token},
                    success: function (res) {
                        $('#exampleModal').hide();
                        if (res.status == 200) {
                            Swal.fire({
                                icon: 'success',
                                title: 'success',
                                text: res.msg,
                            }).then(function () {

                                window.location = "/admin/products/list";
                            });
                        } else {
                            Swal.fire({
                                icon: 'error',
                                title: 'Oops...',
                                text: 'somthing went wrong',
                            });
                        }
                    },
                    error: function (res, response) {
                        $(".loading").remove();
                    },
                });
                Swal.fire(
                    'Deleted!',
                    'Your file has been deleted.',
                    'success'
                )
            }
        })
    }
    @endif
</script>
