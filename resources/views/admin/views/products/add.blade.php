@extends('admin.adminHome')


@section('admin_title')

    {{--Add Products--}}

@endsection


@section('admin_breadcrumb')

    <li class="breadcrumb-item"><a class="navigation" href="#">Home</a></li>
    <li class="breadcrumb-item">products</li>
    <li class="breadcrumb-item active">Add</li>
    <br>
    {{session('currency')}}

@endsection

@section('admin_style')

    <style>

        .img-card {
            width: 40%;
            margin-top: 45px;
            margin-bottom: 18px;
        }

    </style>

@endsection

@section('admin_content')

    <?php
    $locale = App::getLocale();
    ?>

    <!-- Main content -->
    <section class="content">
        <div class="container-fluid">
            <div class="row">
                <!-- left column -->
                <div class="col-md-12">
                    <!-- jquery validation -->
                    <div class="card card-primary">
                        <div class="card-header">
                            <h3 class="card-title">Products Form <small>Add new product</small></h3>
                        </div>
                        <!-- /.card-header -->
                        <!-- form start -->
                        <form role="form" id="ProductAddForm">
                            <div class="card-body row">
                                <div class="form-group col-12">
                                    <label for="name">Product Name</label>
                                    <input type="text" name="name" class="form-control" id="name"
                                           placeholder="Product Name">
                                </div>
                                <div class="form-group col-12">
                                    <label for="name">Product Name Arabic</label>
                                    <input type="text" name="ar_name" class="form-control" id="ar_name"
                                           placeholder="Product Name">
                                </div>
                                <div class="form-group col-12">
                                    <label for="supplier">Supplier</label>
                                    <input type="text" name="supplier" class="form-control" id="supplier"
                                           placeholder="supplier Name">
                                </div>
                                <div class="form-group col-12">
                                    <label for="short_description">Product Short Description</label>
                                    {{--<input type="text"  class="form-control"  placeholder="Product Description">--}}
                                    <textarea class="form-control" name="short_description" id="short_description"
                                              rows="3" placeholder="Product Description"></textarea>
                                </div>
                                <div class="form-group col-12">
                                    <label for="ar_short_description">Product Short Description Arabic</label>
                                    {{--<input type="text"  class="form-control"  placeholder="Product Description">--}}
                                    <textarea class="form-control" name="ar_short_description" id="ar_short_description"
                                              rows="3" placeholder="Product Description"></textarea>
                                </div>

                                <div class="form-group col-12">
                                    <label for="description">Product Description</label>
                                    {{--<input type="text"  class="form-control"  placeholder="Product Description">--}}
                                    <textarea class="form-control" name="description" id="description" rows="3"
                                              placeholder="Product Description"></textarea>
                                </div>
                                <script>
                                    CKEDITOR.replace('description');
                                </script>
                                <div class="form-group col-12">
                                    <label for="description">Product Description Arabic</label>
                                    {{--<input type="text"  class="form-control"  placeholder="Product Description">--}}
                                    <textarea class="form-control" name="ar_description" id="ar_description" rows="3"
                                              placeholder="Product Description"></textarea>
                                </div>
                                <script>
                                    CKEDITOR.replace('ar_description');
                                </script>
                                <div class="form-group col-12">
                                    <label for="description">Product Code</label>
                                    <input type="text" name="product_code" class="form-control" id="product_code"
                                           placeholder="Product Code">
                                </div>

                                <div class="form-group col-6">
                                    <label for="description">Price</label>
                                    <input type="number" name="price" class="form-control" id="price"
                                           placeholder="Price">
                                </div>
                                <div class="form-group col-6">
                                    <label for="description">Price Sale</label>
                                    <input type="number" name="price_sale" class="form-control" id="price_sale"
                                           placeholder="Price">
                                </div>
                                <div class="form-group col-6">
                                    <label for="description">Quantity</label>
                                    <input type="number" name="quantity" class="form-control" id="quantity"
                                           placeholder="Quantity">
                                </div>

                                {{-- Attribute start --}}
                            </div>
                            <div class="card-body row">
                                <div class="form-group col-6">
                                    <label for="description">Attribute : </label>
                                    <a style="float:right;" onclick="add_attributes_to_table()" class="btn btn-primary">+</a>
                                    <div class="form-group col-6">
                                        <label for="description">Select attribute</label>
                                        <select name="attribute_name_add" class="form-control" id="attribute_name_add"
                                                placeholder="Size">
                                            <option disabled selected>Select attribute</option>
                                            @foreach($Attributes as $attribute)
                                                <option value="{{ $attribute->id }}">{{ $attribute->name }}
                                                    -- {{ $attribute->ar_name }}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                    <div class="form-group col-6">
                                        <label for="description">value for attribute</label>
                                        <input type="text" name="attribute_value_add" class="form-control"
                                               id="attribute_value_add" placeholder="value for attribute">
                                        <input type="text" name="attribute_ar_value_add" class="form-control"
                                               id="attribute_ar_value_add" placeholder="Arabic value for attribute">

                                    </div>

                                </div>
                                <div class="form-group col-6">
                                    <table class="table" id="table_attributes">
                                        <tr>
                                            <th>Id</th>
                                            <th>Attribute</th>
                                            <th>Arabic Attribute</th>
                                            <th>Value</th>
                                            <th>Arabic Value</th>
                                            <th>Actions</th>
                                        </tr>
                                        <tbody id="table_attributes_tbody">
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                            <script>

                                function add_attributes_to_table() {
                                    let id = $('#attribute_name_add').val();
                                    let str = $('#attribute_name_add option:selected').text();
                                    let name = str.split("--")[0];
                                    let ar_name = str.split("--")[1];
                                    let value = $('#attribute_value_add').val();
                                    let ar_value = $('#attribute_ar_value_add').val();
                                    if (!value) {
                                        return false;
                                    }
                                    let table = $('#table_attributes_tbody');
                                    table.append('<tr id="tr_' + id + '"><td>' + id + '</td><td>' + name + '</td><td>' + ar_name + '</td><td>' + value + '</td><td>' + ar_value + '</td><td><a class="btn btn-danger" onclick="delete_tr(' + id + ')">×</a></td></tr>')
                                }

                            </script>
                            {{-- Attribute end --}}
                            <div class="card-body row">
                                <div class="form-group col-6">
                                    <label for="description">Shipping costs</label>
                                    <input type="number" name="shipping_costs" class="form-control" id="shipping_costs"
                                           placeholder="shipping_costs">
                                </div>
                                <div class="form-group col-6">
                                </div>

                                <div class="form-group col-6">
                                    <label>Category</label>
                                    <select name="category" onchange="get_type(this.value)" id="category"
                                            class="form-control" style="width: 100%;">
                                        {{--<option selected="selected" value="1">Category1</option>--}}
                                        {{--<option value="2">Category2</option>--}}
                                        {{--<option value="3">Category3</option>--}}
                                        {{--<option value="4">Category4</option>--}}
                                        {{--<option value="5">Category5</option>--}}
                                        {{--<option value="6">Category6</option>--}}
                                        {{--<option value="7">Category7</option>--}}
                                    </select>
                                </div>

                                <div class="form-group col-6">
                                    <label>Type</label>
                                    <select name="type" id="type" class="form-control " style="width: 100%;">
                                        {{--<option selected="selected" value="1">Type1</option>--}}
                                        {{--<option value="2">Type2</option>--}}
                                        {{--<option value="3">Type3</option>--}}
                                        {{--<option value="4">Type4</option>--}}
                                        {{--<option value="5">Type5</option>--}}
                                        {{--<option value="6">Type6</option>--}}
                                        {{--<option value="7">Type7</option>--}}
                                    </select>
                                </div>
                                <div class="form-group col-8">
                                    <label for="Image1">Image</label>
                                    <img src="/images/no_image.png" id="image" class="img-responsive img-card">
                                    <div class="custom-file">
                                        <input type="file" onchange="getnamephoto(this.value)" class="custom-file-input"
                                               id="product_photo" name="product_photo">
                                        <label class="custom-file-label" for="customFile">Choose file</label>
                                    </div>
                                    <!-- /.card-body -->
                                    <div class="card-footer">
                                        <button type="submit" id="Productbutton" class="btn btn-primary">Submit</button>
                                    </div>
                                </div>
                                <div class="form-group col-12">
                                    <div
                                        style="padding:15px;border:1px solid rgba(236, 235, 235, 1);margin:5px;background-color:rgba(236, 235, 235, 0.274);display:none;"
                                        id="productnotifcation"></div>
                                </div>
                            </div>

                        </form>
                    </div>
                    <!-- /.card -->
                </div>
                <!--/.col (left) -->
                <!-- right column -->
                <div class="col-md-6">

                </div>
                <!--/.col (right) -->
            </div>
            <!-- /.row -->
        </div><!-- /.container-fluid -->
    </section>
    <!-- /.content -->
    </div>


@endsection





@section('admin_script')


    {{--<script>--}}
    var userObject = JSON.parse(localStorage.getItem("DamaStore_userObject"));

    // blocked
    // bornDate
    // created_at
    // email
    // emailVerifiedAt
    // files
    // firstName
    // id
    // lastActivity
    // lastLoggedIn
    // lastName
    // phone
    // reputation
    // roles
    // token
    // updated_at
    // username
    // verify



    $('#How-It-Works').append('<div class="loading">Loading&#8230;</div>');
    $.ajax({

    type: "POST",
    url: '{{route('category')}}',
    data: null,
    // mimeType: 'application/json',
    headers: {"Authorization": 'Bearer ' + userObject.token},
    processData: false,
    contentType: false,
    success: function (res) {
    if(res.status == 200) {
    //console.log('category', res);
    res.data.forEach(function(item, index, arr) {
    if(index == 0){
    $('#category').append('<option selected="selected" value="' + item.id + '">' + item.name + '</option>');
    }else {
    $('#category').append('<option value="' + item.id + '">' + item.name + '</option>');
    }
    });
    get_type(res.data[0].id);
    }
    },
    error: function (res, response) {
    $( ".loading" ).remove();
    },
    });

    $('#How-It-Works').append('<div class="loading">Loading&#8230;</div>');

    $("#product_photo").change(function(){
    readURL(this, 'image');
    });


    $('#ProductAddForm').submit(function (e) {
    $('#Productbutton').prop('disabled', true);
    $('#productnotifcation').css('background-color','rgba(236, 235, 235, 0.274)');
    $('#productnotifcation').css('color','black');
    $('#productnotifcation').text('Loading ...')
    e.preventDefault();
    e.stopPropagation();
    var formData = new FormData($(this)[0]);
    formData.append('user_id', userObject.id)
    let data_table = [];
    let a,b,c,d,e_ar;
    $('#table_attributes tbody tr').each((index, tr)=> {
    //console.log(tr);

    $(tr).children('td').each ((index, td) => {

    if(index == 0)
    {
    a=$(td).text();
    }
    if(index == 1)
    {
    b=$(td).text();

    }
    if(index == 2)
    {
    c=$(td).text();
    }
    if(index == 3)
    {
    d=$(td).text();
    }
    if(index == 4)
    {
    e_ar=$(td).text();
    }

    });
    if(a){
    data_table.push({id:a,name:b,ar_name:c,value:d,ar_value:e_ar});
    }

    });

    console.log(JSON.stringify(data_table));


    formData.append('data',JSON.stringify(data_table))


    $('#How-It-Works').append('<div class="loading">Loading&#8230;</div>');
    $.ajax({

    type: "POST",
    url: '{{route('ProductAdd')}}',
    data: formData,
    mimeType: 'application/json',
    headers: {"Authorization": 'Bearer ' + userObject.token},
    processData: false,
    contentType: false,
    success: function (res) {
    if(res.status == 200) {
    Swal.fire({
    icon: 'success',
    title: 'success',
    text: res.msg,
    }).then(function() {
    window.location = "/admin/products/list";
    });

    }else{
    {{-- Swal.fire({
        icon: 'error',
        title: 'Oops...',
        text: 'somthing went wrong',
    }); --}}
    $('#Productbutton').prop('disabled', false);
    $('#productnotifcation').show('slow');
    $('#productnotifcation').css('background-color','red');
    $('#productnotifcation').css('color','white');
    $('#productnotifcation').html(res[0]+'<br>'+JSON.stringify(res[1]))
    }

    },
    error: function (res, response) {
    $('#Productbutton').prop('disabled', false);
    $('#productnotifcation').text('please input all fields or login and return!')
    },
    });
    });



@endsection

<script>
    function get_type(id) {
        let userObject = JSON.parse(localStorage.getItem("DamaStore_userObject"));
        let formData = new FormData();
        formData.append('category_id', id)
        $('#type').html('');
        $.ajax({
            type: "POST",
            url: '{{route('Type')}}',
            data: formData,
            // mimeType: 'application/json',
            headers: {"Authorization": 'Bearer ' + userObject.token},
            processData: false,
            contentType: false,
            success: function (res) {
                if (res.status == 200) {
                    console.log('Type', res);
                    res.data.forEach(function (item, index, arr) {
                        if (index == 0) {
                            $('#type').append('<option selected="selected" value="' + item.id + '">' + item.name + '</option>');
                        } else {
                            $('#type').append('<option value="' + item.id + '">' + item.name + '</option>');
                        }
                    });
                }
            },
            error: function (res, response) {
                // $( ".loading" ).remove();
            },
        });

    }

    function delete_tr(id) {
        $('#tr_' + id).remove();
    }

    function getnamephoto(value) {
        let userObject = JSON.parse(localStorage.getItem("DamaStore_userObject"));
        const filePath = value;
        var filename = filePath.replace(/^.*[\\\/]/, '')
        if (filename) {
            var str = filename;
            var n = str.indexOf(".");
            var res = filename.slice(0, n);
            var formData = new FormData();
            formData.append('name',res);
            $.ajax({
                type: "POST",
                url: '{{route('GetNameOfImage')}}',
                data:formData,
                // mimeType: 'application/json',
                headers: {"Authorization": 'Bearer ' + userObject.token},
                processData: false,
                contentType: false,
                success: function (res) {
                    if (res.success == false) {
                        $('#Productbutton').prop('disabled', true);
                        $('#productnotifcation').show('slow');
                        $('#productnotifcation').css('background-color','red');
                        $('#productnotifcation').css('color','white');
                        $('#productnotifcation').text(res.msg);
                    }
                    if (res.success == true) {
                        $('#Productbutton').prop('disabled', false);
                        $('#productnotifcation').show('slow');
                        $('#productnotifcation').css('background-color','green');
                        $('#productnotifcation').css('color','white');
                        $('#productnotifcation').text(res.msg);
                    }
                },
                error: function (res, response) {
                    console.log(res);
                },
            });
        }
    }
</script>

