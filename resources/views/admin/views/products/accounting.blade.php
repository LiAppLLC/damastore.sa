@extends('admin.adminHome')


@section('admin_title')

    Accounting

@endsection


@section('admin_breadcrumb')

    <li class="breadcrumb-item"><a class="navigation" href="#">Home</a></li>
    <li class="breadcrumb-item active"> Accounting</li>

@endsection

@section('admin_style')

    <style>
        .image-div {
            width: 35px;
            height: 35px;
        }

    </style>

@endsection


@section('admin_content')


    <div class="card">
        <div class="card-header row">
            <div class="col-md-10">
                <h3 class="card-title"> Accounting</h3>
            </div>
            <div class="col-md-2">

            </div>

        </div>
        <div class="alert alert-danger" id="error" style="display: none" role="alert">
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
        </div>
        <!-- /.card-header -->
        <div class="card-body">
            <div class="row justify-content-center">
                <div class="col-md-12">
                    <div class="row">
                        <div class="col-md-2">All price</div>
                        <div class="col-md-2">× </div>
                        <div class="col-md-4"><input id="number_for" class="form-control" type="number"
                                placeholder="number"></div>
                        <div class="col-md-4">
                            <button class="btn btn-danger" onclick="change()" id="submit_number_for">
                                Applying
                            </button>
                            <span id="span_msg"></span>
                        </div>
                    </div>
                    <hr>
                    <div class="row">
                        <div class="col-md-12">
                            <input type="hidden" name="arr_products[]" id="arr_all">
                            {{-- <input type="text" class="form-control"
                                style="border:0;background-color:#efefef;border-radius:0;margin-bottom:2px;"
                                placeholder="Search all fields." id="search_input"> --}}
                        </div>
                        <div class="col-md-12">

                            <table id="table" class="table table-bordered table-intel">
                                <thead>
                                    <tr>
                                        <th class="filter"><input type="checkbox" class="che" id="checkAll"></th>
                                        <th class="filter">ID</th>
                                        <th class="filter">Name of product</th>
                                        <th class="filter">Category</th>
                                        <th class="filter">Type</th>
                                        <th class="filter">Price</th>
                                        <th class="filter">Price Base</th>
                                    </tr>
                                </thead>
                                <form id="form_id">
                                    @foreach ($products as $item)
                                        <tr class="displays">
                                            <td><input class="input_ch che" onclick="add_pro();" value="{{ $item->id }}"
                                                    name="checkbox{{ $item->id }}" id="checkbox{{ $item->id }}"
                                                    type="checkbox" value="{{ $item->id }}"></td>
                                            <td>{{ $item->id }}</td>
                                            <td>{{ $item->name }}</td>
                                            <td>{{ $item->category->name }}</td>
                                            <td>{{ $item->type->name }}</td>
                                            <td>{{ $item->price }}</td>
                                            <td>{{ $item->unit }}</td>
                                        </tr>
                                    @endforeach
                                </form>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>


@endsection
<script src="https://code.jquery.com/jquery-3.5.1.min.js"
    integrity="sha256-9/aliU8dGd2tb6OSsuzixeV4y/faTqgFtohetphbbj0=" crossorigin="anonymous"></script>

<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.form/4.3.0/jquery.form.min.js"
    integrity="sha384-qlmct0AOBiA2VPZkMY3+2WqkHtIQ9lSdAsAn5RUJD/3vA5MKDgSGcdmIv4ycVxyn" crossorigin="anonymous">
</script>

<script>
    function add_pro() {
        var form = document.getElementById("form_id"),
            inputs = document.getElementsByClassName("input_ch"),
            arr = [];
        for (var i = 0, max = inputs.length; i < max; i += 1) {
            // Take only those inputs which are checkbox
            if (inputs[i].type === "checkbox" && inputs[i].checked) {
                arr.push(inputs[i].value);
            }
        }
        document.getElementById('arr_all').value = arr;
    }
    let userObject = JSON.parse(localStorage.getItem("DamaStore_userObject"));

    function change() {

        const formData = new FormData()
        let arr_products = $('#arr_all').val();
        let number_for = $('#number_for').val();
        let msg = $('#span_msg');
        if (arr_products && number_for) {
            $('#submit_number_for').prop('disabled', true);
            msg.html('Please white >>>');
        } else {
            msg.html('Please fill in the fields');
            return false;
        }
        formData.append('_token', "{{ csrf_token() }}")
        formData.append('number_for', number_for);
        formData.append('arr_products', arr_products);
        $.ajax({
            type: "POST",
            url: "{{ route('changeprices') }}",
            data: formData,
            headers: {
                "Authorization": 'Bearer ' + userObject.token
            },
            processData: false,
            contentType: false,
            success: function(res) {
                if (res.status == "all done") {
                    msg.html('all done');
                    window.location.reload();
                }
            },
            error: function(res, response) {
                console.log(res, response);
            },
        });
    }

</script>
<script>
    // $("#checkAll").click(function() {
    //     $('input:checkbox').not(this).prop('checked', this.checked);
    // });
    $(function() {
        // Apply the plugin
        $('#table').excelTableFilter();

    });
    $(document).ready(function() {
        $("#checkAll").click(function() {
            $(".che").prop('checked', $(this).prop('checked'));
            add_pro();
        });

    });

</script>
