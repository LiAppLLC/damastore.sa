


@extends('admin.adminHome')


@section('admin_title')

    Header List

@endsection


@section('admin_breadcrumb')

	<li class="breadcrumb-item"><a class="navigation" href="#">Home</a></li>
	<li class="breadcrumb-item">Config</li>
	<li class="breadcrumb-item">Header</li>
	<li class="breadcrumb-item active">list</li>

@endsection

@section('admin_style')

    <style>

    </style>

@endsection

@section('admin_content')

    <?php
    $locale = App::getLocale();
    ?>

    <div class="card">
        <div class="card-header row">
            <div class="col-10">
                <h3 class="card-title">Users List</h3>
            </div>
            <div class="col-2">
                <a class="navigation" href="{{route('config_header_add')}}"><button type="button" class="btn btn-block btn-primary">Add</button></a>
            </div>
        </div>
        <!-- /.card-header -->
        <div class="card-body">
            <table id="example1" class="table table-bordered table-striped">
                <thead>
                <tr>
                    <th>Images</th>
                    <th>Set</th>
                </tr>
                </thead>
                <tbody id="header_table">
                </tbody>
            </table>
        </div>
    </div>


@endsection





@section('admin_script')


    {{--<script>--}}
        var userObject = JSON.parse(localStorage.getItem("DamaStore_userObject"));

        // blocked
        // bornDate
        // created_at
        // email
        // emailVerifiedAt
        // files
        // firstName
        // id
        // lastActivity
        // lastLoggedIn
        // lastName
        // phone
        // reputation
        // roles
        // token
        // updated_at
        // username
        // verify
        console.log('userObject', userObject);


        var formData = new FormData();

        $.ajax({

            type: "POST",
            url: '/api/getAllHeader',
            data: formData,
            // mimeType: 'application/json',
            processData: false,
            contentType: false,
            headers: {"Authorization": 'Bearer ' + userObject.token},
            success: function (res) {

                // res.data.forEach(function(item, index, arr){
                //     console.log(item);
                //     $('#header_table').append('');
                // });

            },
            error: function (res, response) {
            },
        });



@endsection



