


@extends('admin.adminHome')


@section('admin_title')

    Add Header

@endsection


@section('admin_breadcrumb')

	<li class="breadcrumb-item"><a class="navigation" href="#">Home</a></li>
	<li class="breadcrumb-item">Config</li>
	<li class="breadcrumb-item">Header</li>
	<li class="breadcrumb-item active">Add</li>

@endsection

@section('admin_style')

    <style>

    </style>

@endsection

@section('admin_content')

    <?php
    $locale = App::getLocale();
    ?>

    <!-- Main content -->
    <section class="content">
        <div class="container-fluid">
            <div class="row">
                <!-- left column -->
                <div class="col-md-12">
                    <!-- jquery validation -->
                    <div class="card card-primary">
                        <div class="card-header">
                            <h3 class="card-title">Quick Example <small>jQuery Validation</small></h3>
                        </div>
                        <!-- /.card-header -->
                        <!-- form start -->
                        <form role="form" id="HeaderAddForm">
                            <div class="card-body">
                                <div class="form-group">
                                    <label for="name">Header Name</label>
                                    <input type="text" name="name" class="form-control" id="name" placeholder="Header Name">
                                </div>
                                <div class="form-group">
                                    <label for="description">Header Description</label>
                                    <input type="text" name="description" class="form-control" id="description" placeholder="Header Description">
                                </div>

                                <label for="Image1">First Image</label>
                                <div class="custom-file">
                                    <input type="file" class="custom-file-input" id="image1" name="image1">
                                    <label class="custom-file-label" for="customFile">Choose file</label>
                                </div>

                                <label for="Image2">second Image</label>
                                <div class="custom-file">
                                    <input type="file" class="custom-file-input" id="image2" name="image2">
                                    <label class="custom-file-label" for="customFile">Choose file</label>
                                </div>
                            </div>
                            <!-- /.card-body -->
                            <div class="card-footer">
                                <button type="submit" class="btn btn-primary">Submit</button>
                            </div>
                        </form>
                    </div>
                    <!-- /.card -->
                </div>
                <!--/.col (left) -->
                <!-- right column -->
                <div class="col-md-6">

                </div>
                <!--/.col (right) -->
            </div>
            <!-- /.row -->
        </div><!-- /.container-fluid -->
    </section>
    <!-- /.content -->
    </div>

	{{--<div class="card">--}}
		{{--<div class="card-header row">--}}
            {{--<div class="col-10">--}}
			    {{--<h3 class="card-title">Users List</h3>--}}
            {{--</div>--}}
            {{--<div class="col-2">--}}
                {{--<a href="{{asset('config_header_add')}}"><button type="button" class="btn btn-block btn-primary">Add</button></a>--}}
            {{--</div>--}}
		{{--</div>--}}
		{{--<!-- /.card-header -->--}}
		{{--<div class="card-body">--}}
			{{--<table id="example1" class="table table-bordered table-striped">--}}
				{{--<thead>--}}
				{{--<tr>--}}
					{{--<th>Images</th>--}}
					{{--<th>Set</th>--}}
				{{--</tr>--}}
				{{--</thead>--}}
				{{--<tbody id="header_table">--}}
				{{--</tbody>--}}
			{{--</table>--}}
		{{--</div>--}}
	{{--</div>--}}


@endsection





@section('admin_script')


	{{--<script>--}}
        var userObject = JSON.parse(localStorage.getItem("DamaStore_userObject"));

        // blocked
        // bornDate
        // created_at
        // email
        // emailVerifiedAt
        // files
        // firstName
        // id
        // lastActivity
        // lastLoggedIn
        // lastName
        // phone
        // reputation
        // roles
        // token
        // updated_at
        // username
        // verify

        console.log('userObject', userObject);


        // var formData = new FormData();
        //
        // $.ajax({
        //
        //     type: "POST",
        //     url: '/api/getAllUsersFilter/0/1000',
        //     data: formData,
        //     // mimeType: 'application/json',
        //     processData: false,
        //     contentType: false,
        //     headers: {"Authorization": 'Bearer ' + userObject.token},
        //     success: function (res) {
        //
        //         res.data.forEach(function(item, index, arr){
        //             console.log(item);
        //             $('#header_table').append('');
        //         });
        //
        //     },
        //     error: function (res, response) {
        //     },
        // });

        $('#HeaderAddForm').submit(function (e) {

            var formData = new FormData($(this)[0]);
            e.preventDefault();
            e.stopPropagation();

            $.ajax({

                type: "POST",
                url: '{{route('HeaderAdd')}}',
                data: formData,
                // mimeType: 'application/json',
                processData: false,
                contentType: false,
                success: function (res) {
                    if(res.status == 201) {
                        Swal.fire({
                            icon: 'success',
                            title: 'success',
                            text: res.msg,
                        }).then(function() {
                            window.location = "/admin/config/header/list";
                        });

                    }else{
                        Swal.fire({
                            icon: 'error',
                            title: 'Oops...',
                            text: 'somthing went wrong',
                        });
                    }
                    // if(res.status == 400) {
                    //     // console.log(res.data.validator);
                    //     Swal.fire({
                    //         icon: 'error',
                    //         title: 'Oops...',
                    //         text: res.data.validator,
                    //     });
                    // }

                },
                error: function (res, response) {
                },
            });


        });


@endsection



