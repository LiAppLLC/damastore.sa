


@extends('admin.adminHome')


@section('admin_title')

    Orders

@endsection


@section('admin_breadcrumb')

	<li class="breadcrumb-item"><a class="navigation" href="#">Home</a></li>
	<li class="breadcrumb-item active">Orders</li>

@endsection

@section('admin_style')

	<style>
		.image-div{
			width: 35px;
			height: 35px;
		}
	</style>

@endsection


@section('admin_content')


	<div class="card">
		<div class="card-header">
			<h3 class="card-title">Orders List</h3>
		</div>
		<!-- /.card-header -->
		<div class="card-body">
            <div class="row">
                <div class="col-md-6">
                    <div id="pagination"></div>
                </div>
                <div class="col-md-6">
                    <input type="text" class="form-control"
                           style="border:0;background-color:#efefef;border-radius:0;margin-bottom:2px;"
                           placeholder="Search all fields." id="search_input">
                </div>
            </div>
			<table id="content" class="table table-bordered table-striped">
				<thead>
				<tr>
					<th>Order Id</th>
					<th>User Name</th>
					<th>Total Price</th>
					<th>Quantity</th>
					<th>Date</th>
					<th>Order Status</th>
					<th>Details</th>
				</tr>
				</thead>
				<tbody id="users_table">
				</tbody>
			</table>
		</div>
	</div>

	<!-- Modal -->
	<div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
		<div class="modal-dialog" role="document">
			<div class="modal-content">
				<div class="modal-header">
					<h5 class="modal-title" id="exampleModalLabel">Delete Alert</h5>
					<button type="button" class="close" data-dismiss="modal" aria-label="Close">
						<span aria-hidden="true">&times;</span>
					</button>
				</div>
				<div class="modal-body">
					are you sure you want to delete '<span id="prod_name_modal" style="color: red;"></span>' ?
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
					<button id="delete-btn-modal" type="button" class="btn btn-danger">Delete</button>
				</div>
			</div>
		</div>
	</div>


@endsection





@section('admin_script')


	{{--<script>--}}
        var userObject = JSON.parse(localStorage.getItem("DamaStore_userObject"));

        // blocked
        // bornDate
        // created_at
        // email
        // emailVerifiedAt
        // files
        // firstName
        // id
        // lastActivity
        // lastLoggedIn
        // lastName
        // phone
        // reputation
        // roles
        // token
        // updated_at
        // username
        // verify
        console.log('userObject', userObject);


        var formData = new FormData();
        formData.append('all', true);
        formData.append("user" , userObject.token);

        $.ajax({

            type: "POST",
            url: '/api/MyAllOrderList/0/1000',
            data: formData,
            // mimeType: 'application/json',
            processData: false,
            contentType: false,
            headers: {"Authorization": 'Bearer ' + userObject.token},
            success: function (res) {

                res.data.forEach(function(item, index, arr){

                    var date    = new Date(item.created_at),
                        yr      = date.getFullYear(),
                        month   = date.getMonth()+1,
                        day     = date.getDate(),
                        newDate = yr + '-' + month + '-' + day;


                    $('#users_table').append('<tr>' +
                        '<td >'+ item.orderID +'</td>' +
                        '<td >'+ item.username +'</td>' +
                        '<td >'+ item.total +'</td>' +
                        '<td >'+ item.quantity +'</td>' +
                        '<td >'+ newDate +'</td>' +
                        '<td >'+ item.orderStatus_name +'</td>' +
                        '<td ><a class="navigation" href="/admin/order/' + item.id + '"><button type="button" class="btn btn-block btn-success">Details</button></a></td>' +
                        '</tr>');
    pagenition_s();
                });

                console.log('hey');

            },
            error: function (res, response) {
            },
        });
        // function get_hostname(url) {
        //     var m = url.match(/^http:\/\/[^/]+/);
        //     return m ? m[0] : null;
        // }

        var prod_id;
        $('#exampleModal').on('show.bs.modal', function(e) {

            var id = $(e.relatedTarget).data('id');
            prod_id = id;
            var name = $(e.relatedTarget).data('name');

            $('#prod_name_modal').text(name);

        });
        $('#delete-btn-modal').click(function (e) {

            e.preventDefault();
            e.stopPropagation();

            var formData = new FormData();
            formData.append('product_id', prod_id);

            $.ajax({

                type: "POST",
                url: '/api/ProductDelete',
                data: formData,
                // mimeType: 'application/json',
                processData: false,
                contentType: false,
                headers: {"Authorization": 'Bearer ' + userObject.token},
                success: function (res) {
                    $('#exampleModal').hide();
                    if(res.status == 200) {
                        Swal.fire({
                            icon: 'success',
                            title: 'success',
                            text: res.msg,
                        }).then(function() {

                            window.location = "/admin/products/list";
                        });
                    }else{
                        Swal.fire({
                            icon: 'error',
                            title: 'Oops...',
                            text: 'somthing went wrong',
                        });
                    }
                },
                error: function (res, response) {
                },
            });

        });



@endsection



