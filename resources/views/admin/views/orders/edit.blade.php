


@extends('admin.adminHome')


@section('admin_title')

    {{--Add Products--}}

@endsection


@section('admin_breadcrumb')

	<li clclickass="breadcrumb-item"><a class="navigation" href="#">Home</a></li>
	<li class="breadcrumb-item">Orders</li>
	<li class="breadcrumb-item active">Edit</li>

@endsection

@section('admin_style')

    <style>

        .img-card{
            width: 40%;
            margin-top: 45px;
            margin-bottom: 18px;
        }
        .value-div{
            font-weight: 400 !important;
        }
        .line1{
            width: 76%;
            margin-top: 0;
        }
        .line2{
            width: 94%;
            margin-top: 0;
        }


        /*Form Wizard*/
        .bs-wizard > .bs-wizard-step {padding: 0; position: relative;}
        .bs-wizard > .bs-wizard-step + .bs-wizard-step {}
        .bs-wizard > .bs-wizard-step .bs-wizard-stepnum {color: #595959; font-size: 16px; margin-bottom: 5px;}
        .bs-wizard > .bs-wizard-step .bs-wizard-info {color: #999; font-size: 14px;}
        .bs-wizard > .bs-wizard-step > .bs-wizard-dot {position: absolute; width: 30px; height: 30px; display: block; background: #fbe8aa; top: 45px; left: 50%; margin-top: -15px; margin-left: -15px; border-radius: 50%;}
        .bs-wizard > .bs-wizard-step > .bs-wizard-dot:after {content: ' '; width: 14px; height: 14px; background: #fbbd19; border-radius: 50px; position: absolute; top: 8px; left: 8px; }
        .bs-wizard > .bs-wizard-step > .progress {position: relative; border-radius: 0px; height: 8px; box-shadow: none; margin: 20px 0;cursor: pointer;}
        .bs-wizard > .bs-wizard-step > .progress > .progress-bar {width:0px; box-shadow: none; background: #fbe8aa;}
        .bs-wizard > .bs-wizard-step.complete > .progress > .progress-bar {width:100%;}
        .bs-wizard > .bs-wizard-step.active > .progress > .progress-bar {width:50%;}
        .bs-wizard > .bs-wizard-step:first-child.active > .progress > .progress-bar {width:0%;}
        .bs-wizard > .bs-wizard-step:last-child.active > .progress > .progress-bar {width: 100%;}
        .bs-wizard > .bs-wizard-step.disabled > .bs-wizard-dot {background-color: #f5f5f5;}
        .bs-wizard > .bs-wizard-step.disabled > .bs-wizard-dot:after {opacity: 0;}
        .bs-wizard > .bs-wizard-step:first-child  > .progress {left: 50%; width: 50%;}
        .bs-wizard > .bs-wizard-step:last-child  > .progress {width: 50%; }
        .bs-wizard > .bs-wizard-step.disabled a.bs-wizard-dot{ pointer-events: none; }
    </style>

@endsection

@section('admin_content')

    <?php
    $locale = App::getLocale();
    ?>





    <section class="content">
        <div class="container-fluid">
            <div class="row">
                <!-- left column -->
                <div class="col-md-12">
                    <!-- jquery validation -->
                    <div class="card card-primary">
                        <div class="card-header">
                            <div class="row">
                                <div class="col-md-10">
                                    <h3 class="card-title">Order <small>Order Details</small></h3>
                                </div>
                                <div class="col-md-2">
                                    <button type="button" class="btn btn-block btn-danger" id="refund-btn">Refund</button>
                                    <input type="hidden" id="order_pay_id">
                                </div>
                            </div>
                        </div>
                        <!-- /.card-header -->
                        <!-- form start -->
                        <?php
//                        dd($product);
                        ?>
                        <div>
                            <div class="card-body row">
                                <div class="form-group col-4">
                                    <h3>Order Details</h3>
                                </div>

                                <!-- BEGIN: ORDER FORM -->
                                <div class="col-md-12">
                                    <div class="c-content-bar-1 c-align-left c-bordered c-theme-border c-shadow">
                                        <h1 class="c-font-bold c-font-uppercase c-font-24">Order Status

                                        </h1>
                                        <select id="select_state" style="padding:15px;background-color: #2C3E50;color:white;">
                                            <option disabled selected>New Order Status</option>
                                        </select>
                                        <div class="row bs-wizard col-12" id="progressBar">

                                            <div id="Created" class="col-md-2 col-xs-2 bs-wizard-step complete steps_order">
                                                <div class="text-center bs-wizard-stepnum" id="Created">Created</div>
                                                <div class="progress"><div class="progress-bar"></div></div>
                                                <span class="bs-wizard-dot"></span>
                                            </div>
                                            <div id="Ordered" class="col-md-2 col-xs-2 bs-wizard-step complete steps_order">
                                                <div class="text-center bs-wizard-stepnum"  >Ordered</div>
                                                <div class="progress"><div class="progress-bar"></div></div>
                                                <span class="bs-wizard-dot"></span>
                                            </div>
                                            <div id="Paid" class="col-md-2 col-xs-2 bs-wizard-step disabled steps_order">
                                                <div class="text-center bs-wizard-stepnum"  >Paid</div>
                                                <div class="progress"><div class="progress-bar"></div></div>
                                                <span class="bs-wizard-dot"></span>
                                            </div>
                                            <div id="Confirm" class="col-md-2 col-xs-2 bs-wizard-step disabled steps_order">
                                                <div class="text-center bs-wizard-stepnum" >Confirm</div>
                                                <div class="progress"><div class="progress-bar"></div></div>
                                                <span class="bs-wizard-dot"></span>
                                            </div>
                                            <div id="Packed" class="col-md-2 col-xs-2 bs-wizard-step disabled steps_order">
                                                <div class="text-center bs-wizard-stepnum" >Packed</div>
                                                <div class="progress"><div class="progress-bar"></div></div>
                                                <span class="bs-wizard-dot"></span>
                                            </div>
                                            <div id="Shipped" class="col-md-2 col-xs-2 bs-wizard-step disabled disabled steps_order">
                                                <div class="text-center bs-wizard-stepnum" >Shipped</div>
                                                <div class="progress"><div class="progress-bar"></div></div>
                                                <span class="bs-wizard-dot"></span>
                                            </div>
                                            <div id="Deliver" class="col-md-2 col-xs-2 bs-wizard-step disabled steps_order">
                                                <div class="text-center bs-wizard-stepnum" >Deliver</div>
                                                <div class="progress"><div class="progress-bar"></div></div>
                                                <span class="bs-wizard-dot"></span>
                                            </div>
                                            <div id="Failed" class="col-md-2 col-xs-2 bs-wizard-step disabled  steps_order">
                                                <div class="text-center bs-wizard-stepnum" >Failed</div>
                                                <div class="progress"><div class="progress-bar"></div></div>
                                                <span class="bs-wizard-dot"></span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <!-- END: ORDER FORM -->

                                <hr class="line1">
                                <div class="form-group col-3"><label>Order Name</label></div>
                                <div class="form-group col-3"><label class="value-div" id="orderID"></label></div>

                                <div class="form-group col-3"><label>Order Status</label></div>
                                <div class="form-group col-3"><label class="value-div" id="orderStatus_id"></label>
                                </div>

                                <div class="form-group col-3"><label>Quantity</label></div>
                                <div class="form-group col-3"><label class="value-div" id="quantity"></label></div>

                                <div class="form-group col-3"><label>Total Price</label></div>
                                <div class="form-group col-3"><label class="value-div" id="total"></label></div>

                                <div class="form-group col-3"><label>Create Time</label></div>
                                <div class="form-group col-3"><label class="value-div" id="created_at"></label></div>

                            </div>

                            <hr class="line2">
                            <div class="form-group col-12">
                                <h3>Order Details</h3>
                            </div>
                            <hr class="line1">

                            <div class="card-body row">

                                <div class="form-group col-2"><label>Product</label></div>
                                {{--<div class="form-group col-3"><label class="value-div" id="orderID"></label></div>--}}
                                <div class="form-group col-2"><label>Total</label></div>
                                {{--<div class="form-group col-3"><label class="value-div" id="orderStatus_id"></label></div>--}}
                                <div class="form-group col-2"><label>Supplier</label></div>

                                <div class="form-group col-2"><label>first price</label></div>

                                <div class="form-group col-12" id="products_order"></div>

                                <div class="form-group col-6"><label>Total</label></div>
                                <div class="form-group col-6"><span class="c-shipping-total value-div" id="totalCost"></span></div>

                            </div>

                            <hr class="line2">

                            <div class="card-body row">
                                <div class="form-group col-12">
                                    <h3>Payment Details</h3>
                                </div>
                                <hr class="line1">

                                <div class="form-group col-3"><label>Payer Id</label></div>
                                <div class="form-group col-3"><label class="value-div" id="payer_id"></label></div>

                                <div class="form-group col-3"><label>status</label></div>
                                <div class="form-group col-3"><label class="value-div" id="status"></label></div>

                                <div class="form-group col-3"><label>Payee Merchant Id</label></div>
                                <div class="form-group col-3"><label class="value-div" id="payeeMerchant_id"></label></div>

                                <div class="form-group col-3"><label>Email Address</label></div>
                                <div class="form-group col-3"><label class="value-div" id="email_address"></label></div>

                                <div class="form-group col-3"><label>Amount Value</label></div>
                                <div class="form-group col-3"><label class="value-div"><span id="amountValue"></span> <span id="amountCurrency_code"></span></label></div>

                                <div class="form-group col-3"><label>Country Code</label></div>
                                <div class="form-group col-3"><label class="value-div" id="country_code"></label></div>

                                <div class="form-group col-3"><label>Create Time</label></div>
                                <div class="form-group col-3"><label class="value-div" id="created_at_payment"></label></div>

                                <div class="form-group col-6"></div>

                                <div class="form-group col-3"><label>Payee Email Address</label></div>
                                <div class="form-group col-6"><label class="value-div" id="payeeEmail_address"></label></div>
                                <div class="form-group col-12">
                                    <h3>Shipping information</h3>
                                </div>
                                <hr class="line1">
                                <div class="form-group col-3"><label>Address</label></div>
                                <div class="form-group col-3"><label class="value-div" id="address"></label></div>
                                <div class="form-group col-3"><label>Country</label></div>
                                <div class="form-group col-3"><label class="value-div" id="country"></label></div>
                                <div class="form-group col-3"><label>City</label></div>
                                <div class="form-group col-3"><label class="value-div" id="city"></label></div>
                                <div class="form-group col-3"><label>Zip Code</label></div>
                                <div class="form-group col-3"><label class="value-div" id="zipcode"></label></div>
                            </div>
                        </div>

                    </div>
                    <!-- /.card -->
                </div>
                <!--/.col (left) -->
                <!-- right column -->
                <div class="col-md-6">

                </div>
                <!--/.col (right) -->
            </div>
            <!-- /.row -->
        </div><!-- /.container-fluid -->
    </section>
    <!-- /.content -->
    </div>


@endsection





@section('admin_script')


	{{--<script>--}}
        var userObject = JSON.parse(localStorage.getItem("DamaStore_userObject"));

        // blocked
        // bornDate
        // created_at
        // email
        // emailVerifiedAt
        // files
        // firstName
        // id
        // lastActivity
        // lastLoggedIn
        // lastName
        // phone
        // reputation
        // roles
        // token
        // updated_at
        // username
        // verify

        console.log('userObject', userObject);


        // =============================================================================================================
        // get Data of product

        var url = window.location.pathname;
        var id = url.substring(url.lastIndexOf('/') + 1);
        var formData = new FormData();
        formData.append('id', id);
        formData.append("user" , userObject.token);

        $('#How-It-Works').append('<div class="loading">Loading&#8230;</div>');
        $.ajax({

            type: "POST",
            url: '{{route('GetOrderDetails')}}',
            data: formData,
            // mimeType: 'application/json',
            headers: {"Authorization": 'Bearer ' + userObject.token},
            processData: false,
            contentType: false,
            success: function (res) {
                if(res.status == 200) {
                    console.log('trtrt', res);

                    $('#products_order').empty();
                    // ==============================================================================
                    // order
                    var date = new Date(res.data.orders.created_at),
                    yr = date.getFullYear(),
                    month = date.getMonth() < 10 ? '0' + date.getMonth() : date.getMonth(),
                    day = date.getDate() < 10 ? '0' + date.getDate() : date.getDate(),
                    newDate = yr + '-' + month + '-' + day;
                    $('#created_at').text(res.data.orders.created_at);
                    $('#orderID').text(res.data.orders.orderID);
                    $('#quantity').text(res.data.orders.quantity);
                    $('#total').text( res.data.orders.total);
                    $('#totalCost').text(res.data.orders.total);
                    $('#orderStatus_id').text(res.data.orders.orderStatus_name);


                    // ==============================================================================



                    // ==============================================================================
                    // OrderStates
    if(res.data.orders.orderStatus_name=="Paid"){
    $('#'+res.data.orders.orderStatus_name).addClass('complete ');
    $('#'+res.data.orders.orderStatus_name).removeClass('disabled');
    }
    if(res.data.orders.orderStatus_name=="Confirm"){
    $('#'+res.data.orders.orderStatus_name).addClass('complete ');
    $('#'+res.data.orders.orderStatus_name).removeClass('disabled');
    $('#Paid').addClass('complete');
    $('#Paid').removeClass('disabled');
    }
    if(res.data.orders.orderStatus_name=="Packed"){
    $('#'+res.data.orders.orderStatus_name).addClass('complete ');
    $('#'+res.data.orders.orderStatus_name).removeClass('disabled');
    $('#Paid').addClass('complete');
    $('#Paid').removeClass('disabled');
    $('#Confirm').addClass('complete');
    $('#Confirm').removeClass('disabled');
    }
    if(res.data.orders.orderStatus_name=="Shipped"){
    $('#'+res.data.orders.orderStatus_name).addClass('complete ');
    $('#'+res.data.orders.orderStatus_name).removeClass('disabled');
    $('#Paid').addClass('complete');
    $('#Paid').removeClass('disabled');
    $('#Confirm').addClass('complete');
    $('#Confirm').removeClass('disabled');
    $('#Packed').addClass('complete');
    $('#Packed').removeClass('disabled');
    }
    if(res.data.orders.orderStatus_name=="Deliver"){
    $('#'+res.data.orders.orderStatus_name).addClass('complete ');
    $('#'+res.data.orders.orderStatus_name).removeClass('disabled');
    $('#Paid').addClass('complete');
    $('#Paid').removeClass('disabled');
    $('#Confirm').addClass('complete');
    $('#Confirm').removeClass('disabled');
    $('#Packed').addClass('complete');
    $('#Packed').removeClass('disabled');
    $('#Shipped').addClass('complete');
    $('#Shipped').removeClass('disabled');
    }
    if(res.data.orders.orderStatus_name=="Failed"){
    $('#Failed').addClass('complete active');
    $('#Failed').removeClass('disabled');
    }
                    var found = false;
                    var active = '';
                    var len = res.data.OrderStates.length;
                    var col = 0;
                    if(len > 6){
                        col = 1;
                    }
                    if(len == 5 || len == 6){
                        col = 2;
                    }
                    if(len == 4){
                        col = 3;
                    }
                    if(len == 3){
                        col = 4;
                    }
                    if(len == 2){
                        col = 6;
                    }
                    if(len == 1){
                        col = 12;
                    }
                    res.data.OrderStates.forEach(function (item, index, arr) {

                        if(!found){
                            if(item.active == 1){
                                active = 'active';
                                found = true;
                            }else{
                                active = 'complete';
                            }
                        }else{
                            active = 'disabled';
                        }

                        $('#select_state').append('<option data-id="' + index + '" value="' + (index+1) + '">' + item.name + '</option> ');

                    });

                    // ==============================================================================



                    // ==============================================================================
                    // payment
                    var date = new Date(res.data.payment.created_at),
                    yr = date.getFullYear(),
                    month = date.getMonth() < 10 ? '0' + date.getMonth() : date.getMonth(),
                    day = date.getDate() < 10 ? '0' + date.getDate() : date.getDate(),
                    newDate = yr + '-' + month + '-' + day;

                    $('#payer_id').text(res.data.payment.payer_id);
                    $('#status').text(res.data.payment.status);
                    $('#payeeMerchant_id').text(res.data.payment.payeeMerchant_id);
                    $('#payeeEmail_address').text(res.data.payment.payeeEmail_address);
                    $('#email_address').text(res.data.payment.email_address);
                    $('#amountCurrency_code').text(res.data.payment.amountCurrency_code);
                    $('#amountValue').text(res.data.payment.amountValue);
                    $('#country_code').text(res.data.payment.country_code);
                    $('#address').text(res.data.payment.address);
                    $('#city').text(res.data.payment.city);
                    $('#country').text(res.data.payment.country);
                    $('#order_pay_id').val(res.data.payment.id);
                    console.log(res.data.payment.captures_id);
                    $('#zipcode').text(res.data.payment.zipcode);
                    $('#created_at_payment').text(res.data.payment.created_at);
                    // ==============================================================================

                    res.data.ordered_product.forEach(function (item, index, arr) {


                    var date = new Date(item.created_at),
                    yr = date.getFullYear(),
                    month = date.getMonth() < 10 ? '0' + date.getMonth() : date.getMonth(),
                    day = date.getDate() < 10 ? '0' + date.getDate() : date.getDate(),
                    newDate = yr + '-' + month + '-' + day;

                    $('#products_order').append('<li class="row c-margin-b-15 c-margin-t-15"> ' +
                        '		<div class="col-md-2 c-font-20"> ' +
                            '			<a href="/productDetails/' + item.product_id + '" class="navigation c-theme-link">' + item.product_name + ' x ' + item.qty + '</a></div> ' +
                        '		<div class="col-md-2 c-font-20"> ' +
                            '			<p class="">' + item.price + '</p> ' +
                            '		</div> ' +
                        '		<div class="col-md-2 c-font-20"> ' +
                            '			<p class="">' + item.supplier + '</p> ' +
                            '		</div> ' +
                        '		<div class="col-md-2 c-font-20"> ' +
                            '			<p class="">' + item.unit + '</p> ' +
                            '		</div> ' +
                        '	</li>');
                    });
                }
            },
            error: function (res, response) {
                $( ".loading" ).remove();
            },
        });

    $(document).on("change", "#select_state", function(e) {
        var orderStatus_id = $("#select_state").val();
    console.log(orderStatus_id);
    e.preventDefault();
    e.stopPropagation();
        var url = window.location.pathname;
        var id = url.substring(url.lastIndexOf('/') + 1);
    console.log(id);
        var formData = new FormData();
        formData.append('id', id);
        formData.append("user" , userObject.token);
        formData.append("orderStatus_id" , orderStatus_id);

        $('#How-It-Works').append('<div class="loading">Loading&#8230;</div>');
        $.ajax({

            type: "POST",
            url: '{{route('ChangeOrderStatus')}}',
            data: formData,
            // mimeType: 'application/json',
            headers: {"Authorization": 'Bearer ' + userObject.token},
            processData: false,
            contentType: false,
            success: function (res) {
                if (res.status == 200) {
                    Swal.fire({
                        icon: 'success',
                        title: 'success',
                        text: res.msg,
                    }).then(function() {
                        location.reload(true);
                    });
                }
            },
            error: function ( res , response ) {
                $( ".loading" ).remove();
            },
        });
    });


        $(document).on("click", "#refund-btn", function(e) {

            var url = window.location.pathname;
            {{-- var id = url.substring(url.lastIndexOf('/') + 1); --}}

            var formData = new FormData();
            formData.append('order_id', $('#order_pay_id').val());
            formData.append("user" , userObject.token);

            $('#How-It-Works').append('<div class="loading">Loading&#8230;</div>');
            $.ajax({

                type: "POST",
                url: '{{route('refund')}}',
                data: formData,
                // mimeType: 'application/json',
                headers: {"Authorization": 'Bearer ' + userObject.token},
                processData: false,
                contentType: false,
                success: function (res) {
                    if (res.status == 200) {
                        Swal.fire({
                            icon: 'success',
                            title: 'refund successfuly',
                            text: res.msg,
                        }).then(function() {
                            //location.reload(true);
                        });
                    }
                },
                error: function ( res , response ) {
                    $( ".loading" ).remove();
                },
            });
        });



@endsection



