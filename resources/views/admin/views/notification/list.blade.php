@extends('admin.adminHome')


@section('admin_title')

    Notification

@endsection


@section('admin_breadcrumb')

    <li class="breadcrumb-item"><a class="navigation" href="#">Home</a></li>
    <li class="breadcrumb-item active"> Notification</li>

@endsection

@section('admin_style')

    <style>
        .image-div {
            width: 35px;
            height: 35px;
        }

    </style>

@endsection


@section('admin_content')


    <div class="card">
        <div class="card-header row">
            <div class="col-md-10">
                <h3 class="card-title"> Notification</h3>
            </div>
            <div class="col-md-2">

            </div>

        </div>
        <div class="alert alert-danger" id="error" style="display: none" role="alert">
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
        </div>
        <!-- /.card-header -->
        <div class="card-body">
            <div class="row justify-content-center">
                <div class="col-md-8">
                    <center>
                        <button id="btn-nft-enable" onclick="initFirebaseMessagingRegistration()" class="btn btn-danger btn-xs btn-flat">Allow for Notification</button>
                    </center>
                    <div class="card">
                        <div class="card-header">{{ __('Dashboard') }}</div>

                        <div class="card-body">
                            @if (session('status'))
                                <div class="alert alert-success" role="alert">
                                    {{ session('status') }}
                                </div>
                            @endif

                            <form action="{{ route('send.notification') }}" method="POST">
                                @csrf
                                <div class="form-group">
                                    <label>Title</label>
                                    <input type="text" class="form-control" name="title">
                                </div>
                                <div class="form-group">
                                    <label>Body</label>
                                    <textarea class="form-control" name="body"></textarea>
                                  </div>
                                <button type="submit" class="btn btn-primary">Send Notification</button>
                            </form>

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>


@endsection
<script src="https://code.jquery.com/jquery-3.5.1.min.js"
    integrity="sha256-9/aliU8dGd2tb6OSsuzixeV4y/faTqgFtohetphbbj0=" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.form/4.3.0/jquery.form.min.js"
    integrity="sha384-qlmct0AOBiA2VPZkMY3+2WqkHtIQ9lSdAsAn5RUJD/3vA5MKDgSGcdmIv4ycVxyn" crossorigin="anonymous">
</script>

<script src="https://www.gstatic.com/firebasejs/7.23.0/firebase.js"></script>

<script>
  var userObject = JSON.parse(localStorage.getItem("DamaStore_userObject"));
    var firebaseConfig = {
    apiKey: "AIzaSyCiUPH6xsFh8XzPHcxQTnyinXTpJQA_rQs",
    authDomain: "damastore-20a76.firebaseapp.com",
    databaseURL: "https://damastore-20a76.firebaseio.com",
    projectId: "damastore-20a76",
    storageBucket: "damastore-20a76.appspot.com",
    messagingSenderId: "614626349242",
    appId: "1:614626349242:web:30baab3b46cfc4e2f1b283",
    measurementId: "G-PR8PXQ1928"
    };

    firebase.initializeApp(firebaseConfig);
    const messaging = firebase.messaging();
    // firebase.analytics();
    function initFirebaseMessagingRegistration() {
            messaging
            .requestPermission()
            .then(function () {
                return messaging.getToken()
            })
            .then(function(token) {
                console.log(token);

                var formData = new FormData();

                $.ajax({
                    headers: {
                    "Authorization": 'Bearer ' + userObject.token
                },
                    url: '{{ route("save-token") }}',
                    type: 'POST',
                    data: {
                        "_token": "{{ csrf_token() }}",
                        token: token
                    },
                    dataType: 'JSON',
                    success: function (response) {
                        alert('Token saved successfully.');
                    },
                    error: function (err) {
                        console.log('User Chat Token Error'+ err);
                    },
                });

            }).catch(function (err) {
                console.log('User Chat Token Error'+ err);
            });
     }

    messaging.onMessage(function(payload) {
        const noteTitle = payload.notification.title;
        const noteOptions = {
            body: payload.notification.body,
            icon: payload.notification.icon,
        };
        new Notification(noteTitle, noteOptions);
    });

</script>
{{-- <script src="{{asset('assets/firebase-messaging-sw.js')}}" type="text/javascript"></script> --}}
