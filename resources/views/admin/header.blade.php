<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<title>Dama Store | Dashboard</title>
<!-- Tell the browser to be responsive to screen width -->
<meta name="viewport" content="width=device-width, initial-scale=1">
<!-- Font Awesome -->

<link href="{{asset('assets_new/excel-bootstrap-table-filter-style.css')}}" rel="stylesheet" type="text/css"/>
<link rel="stylesheet" href="{{asset('/adminlte/plugins/fontawesome-free/css/all.min.css')}}">
<!-- Ionicons -->
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.0.0-alpha.6/css/bootstrap.min.css" />

<link rel="stylesheet" href="{{asset('https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css')}}">
<!-- Tempusdominus Bbootstrap 4 -->
<link rel="stylesheet" href="{{'/adminlte/plugins/tempusdominus-bootstrap-4/css/tempusdominus-bootstrap-4.min.css'}}">
<!-- iCheck -->
<link rel="stylesheet" href="{{asset('/adminlte/plugins/icheck-bootstrap/icheck-bootstrap.min.css')}}">
<!-- JQVMap -->
<link rel="stylesheet" href="{{asset('/adminlte/plugins/jqvmap/jqvmap.min.css')}}">
<!-- Theme style -->
<link rel="stylesheet" href="{{asset('/adminlte/dist/css/adminlte.min.css')}}">
<!-- overlayScrollbars -->
<link rel="stylesheet" href="{{asset('/adminlte/plugins/overlayScrollbars/css/OverlayScrollbars.min.css')}}">
<!-- Daterange picker -->
<link rel="stylesheet" href="{{asset('/adminlte/plugins/daterangepicker/daterangepicker.css')}}">
<!-- summernote -->
<link rel="stylesheet" href="{{asset('/adminlte/plugins/summernote/summernote-bs4.css')}}">
<script src="https://cdn.ckeditor.com/4.15.1/standard/ckeditor.js"></script>
<!-- Google Font: Source Sans Pro -->
<link href="{{asset('https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700')}}" rel="stylesheet">
<style>
    ul.simple-pagination {
        list-style: none;
    }

    .simple-pagination {
        display: block;
        overflow: hidden;
        padding: 0 5px 5px 0;
        margin: 0;
    }

    .simple-pagination ul {
        list-style: none;
        padding: 0;
        margin: 0;
    }

    .simple-pagination li {
        list-style: none;
        padding: 0;
        margin: 0;
        float: left;
    }

    /*------------------------------------*\
        Compact Theme Styles
    \*------------------------------------*/

    .compact-theme a, .compact-theme span {
        float: left;
        color: #333;
        font-size:14px;
        line-height:24px;
        font-weight: normal;
        text-align: center;
        border: 1px solid #AAA;
        border-right: none;
        min-width: 14px;
        padding: 0 7px;
        box-shadow: 2px 2px 2px rgba(0,0,0,0.2);
        background: #efefef; /* Old browsers */
        background: -moz-linear-gradient(top, #ffffff 0%, #efefef 100%); /* FF3.6+ */
        background: -webkit-gradient(linear, left top, left bottom, color-stop(0%,#ffffff), color-stop(100%,#efefef)); /* Chrome,Safari4+ */
        background: -webkit-linear-gradient(top, #ffffff 0%,#efefef 100%); /* Chrome10+,Safari5.1+ */
        background: -o-linear-gradient(top, #ffffff 0%,#efefef 100%); /* Opera11.10+ */
        background: -ms-linear-gradient(top, #ffffff 0%,#efefef 100%); /* IE10+ */
        background: linear-gradient(top, #ffffff 0%,#efefef 100%); /* W3C */
    }

    .compact-theme a:hover {
        text-decoration: none;
        background: #efefef; /* Old browsers */
        background: -moz-linear-gradient(top, #efefef 0%, #bbbbbb 100%); /* FF3.6+ */
        background: -webkit-gradient(linear, left top, left bottom, color-stop(0%,#efefef), color-stop(100%,#bbbbbb)); /* Chrome,Safari4+ */
        background: -webkit-linear-gradient(top, #efefef 0%,#bbbbbb 100%); /* Chrome10+,Safari5.1+ */
        background: -o-linear-gradient(top, #efefef 0%,#bbbbbb 100%); /* Opera11.10+ */
        background: -ms-linear-gradient(top, #efefef 0%,#bbbbbb 100%); /* IE10+ */
        background: linear-gradient(top, #efefef 0%,#bbbbbb 100%); /* W3C */
    }

    .compact-theme .prev {
        border-radius: 3px 0 0 3px;
    }

    .compact-theme .next {
        border-right: 1px solid #AAA;
        border-radius: 0 3px 3px 0;
    }

    .compact-theme .current {
        background: #bbbbbb; /* Old browsers */
        background: -moz-linear-gradient(top, #bbbbbb 0%, #efefef 100%); /* FF3.6+ */
        background: -webkit-gradient(linear, left top, left bottom, color-stop(0%,#bbbbbb), color-stop(100%,#efefef)); /* Chrome,Safari4+ */
        background: -webkit-linear-gradient(top, #bbbbbb 0%,#efefef 100%); /* Chrome10+,Safari5.1+ */
        background: -o-linear-gradient(top, #bbbbbb 0%,#efefef 100%); /* Opera11.10+ */
        background: -ms-linear-gradient(top, #bbbbbb 0%,#efefef 100%); /* IE10+ */
        background: linear-gradient(top, #bbbbbb 0%,#efefef 100%); /* W3C */
        cursor: default;
    }

    .compact-theme .ellipse {
        background: #EAEAEA;
        padding: 0 10px;
        cursor: default;
    }

    /*------------------------------------*\
        Light Theme Styles
    \*------------------------------------*/

    .light-theme a, .light-theme span {
        float: left;
        color: #666;
        font-size:14px;
        line-height:24px;
        font-weight: normal;
        text-align: center;
        border: 1px solid #BBB;
        min-width: 14px;
        padding: 0 7px;
        margin: 0 5px 0 0;
        border-radius: 3px;
        box-shadow: 0 1px 2px rgba(0,0,0,0.2);
        background: #efefef; /* Old browsers */
        background: -moz-linear-gradient(top, #ffffff 0%, #efefef 100%); /* FF3.6+ */
        background: -webkit-gradient(linear, left top, left bottom, color-stop(0%,#ffffff), color-stop(100%,#efefef)); /* Chrome,Safari4+ */
        background: -webkit-linear-gradient(top, #ffffff 0%,#efefef 100%); /* Chrome10+,Safari5.1+ */
        background: -o-linear-gradient(top, #ffffff 0%,#efefef 100%); /* Opera11.10+ */
        background: -ms-linear-gradient(top, #ffffff 0%,#efefef 100%); /* IE10+ */
        background: linear-gradient(top, #ffffff 0%,#efefef 100%); /* W3C */
    }

    .light-theme a:hover {
        text-decoration: none;
        background: #FCFCFC;
    }

    .light-theme .current {
        background: #666;
        color: #FFF;
        border-color: #444;
        box-shadow: 0 1px 0 rgba(255,255,255,1), 0 0 2px rgba(0, 0, 0, 0.3) inset;
        cursor: default;
    }

    .light-theme .ellipse {
        background: none;
        border: none;
        border-radius: 0;
        box-shadow: none;
        font-weight: bold;
        cursor: default;
    }

    /*------------------------------------*\
        Dark Theme Styles
    \*------------------------------------*/

    .dark-theme a, .dark-theme span {
        float: left;
        color: #CCC;
        font-size:14px;
        line-height:24px;
        font-weight: normal;
        text-align: center;
        border: 1px solid #222;
        min-width: 14px;
        padding: 0 7px;
        margin: 0 5px 0 0;
        border-radius: 3px;
        box-shadow: 0 1px 2px rgba(0,0,0,0.2);
        background: #555; /* Old browsers */
        background: -moz-linear-gradient(top, #555 0%, #333 100%); /* FF3.6+ */
        background: -webkit-gradient(linear, left top, left bottom, color-stop(0%,#555), color-stop(100%,#333)); /* Chrome,Safari4+ */
        background: -webkit-linear-gradient(top, #555 0%,#333 100%); /* Chrome10+,Safari5.1+ */
        background: -o-linear-gradient(top, #555 0%,#333 100%); /* Opera11.10+ */
        background: -ms-linear-gradient(top, #555 0%,#333 100%); /* IE10+ */
        background: linear-gradient(top, #555 0%,#333 100%); /* W3C */
    }

    .dark-theme a:hover {
        text-decoration: none;
        background: #444;
    }

    .dark-theme .current {
        background: #222;
        color: #FFF;
        border-color: #000;
        box-shadow: 0 1px 0 rgba(255,255,255,0.2), 0 0 1px 1px rgba(0, 0, 0, 0.1) inset;
        cursor: default;
    }

    .dark-theme .ellipse {
        background: none;
        border: none;
        border-radius: 0;
        box-shadow: none;
        font-weight: bold;
        cursor: default;
    }
</style>
