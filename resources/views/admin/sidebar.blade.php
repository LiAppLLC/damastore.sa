<!-- Main Sidebar Container -->
<aside class="main-sidebar sidebar-dark-primary elevation-4" style="background-color:#110235c7;">
    <!-- Brand Logo -->
    <a href="/admin" class="navigation brand-link">
        <img src="/adminlte/dist/img/AdminLTELogo.png" alt="AdminLTE Logo" class="brand-image img-circle elevation-3"
            style="opacity: .8">
        <span class="brand-text font-weight-light">Dama Store</span>
    </a>
    <!-- Sidebar -->
    <div class="sidebar">
        <!-- Sidebar user panel (optional) -->
        <div class="user-panel mt-3 pb-3 mb-3 d-flex">
            <div class="image">
                <img src="/adminlte/dist/img/user2-160x160.jpg" class="img-circle elevation-2" alt="User Image">
            </div>
            <div class="infoDashboard">
                <a href="#" style="padding-left:10px;" class="navigation d-block" id="username"> username </a>
            </div>
        </div>

        <!-- Sidebar Menu -->
        <nav class="mt-2">
            <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">

                <?php
                // dd($permissions);
                ?>

                @if ($permissions['Dashbored'])
                    <li class="nav-item">
                        <a href="{{ route('admin') }}" class="navigation nav-link">
                            <i class="nav-icon fas fa-th"></i>
                            <p>Dashboard</p>
                        </a>
                    </li>
                @endif
                @if ($permissions['users_list'])
                    <li class="nav-item">
                        <a href="{{ route('admin_users') }}" class="navigation nav-link">
                            <i class="nav-icon fas fa-user"></i>
                            <p>Users</p>
                        </a>
                    </li>
                @endif
                @if ($permissions['products_list'])
                    <li class="nav-item has-treeview">
                        <a href="{{ route('admin_products_list') }}" class="navigation nav-link">
                            <i class="fab fa-product-hunt nav-icon"></i>
                            <p>
                                Products
                                <i class="fas fa-angle-left right"></i>
                            </p>
                        </a>
                        <ul class="nav nav-treeview">
                            <li class="nav-item">
                                <a href="{{ route('admin_products_list') }}" class="navigation nav-link">
                                    <i class="far fa-circle nav-icon"></i>
                                    <p>list</p>
                                </a>
                            </li>
                            @if ($permissions['types_add'])
                                <li class="nav-item">
                                    <a href="{{ route('admin_products_add') }}" class="navigation nav-link">
                                        <i class="far fa-circle nav-icon"></i>
                                        <p>Add</p>
                                    </a>
                                </li>
                                <li class="nav-item">
                                    <a href="{{ route('admin_products_accounting') }}" class="navigation nav-link">
                                        <i class="far fa-circle nav-icon"></i>
                                        <p>Accounting</p>
                                    </a>
                                </li>
                            @endif
                        </ul>
                    </li>
                    <li class="nav-item has-treeview">
                        <a href="{{ route('admin_products_list') }}" class="navigation nav-link">
                            <i class="fab fa-product-hunt nav-icon"></i>
                            <p>
                                Custom Products
                                <i class="fas fa-angle-left right"></i>
                            </p>
                        </a>
                        <ul class="nav nav-treeview">
                            <li class="nav-item">
                                <a href="{{ route('c_admin_products_list') }}" class="navigation nav-link">
                                    <i class="far fa-circle nav-icon"></i>
                                    <p>list</p>
                                </a>
                            </li>
                            @if ($permissions['types_add'])
                                <li class="nav-item">
                                    <a href="{{ route('c_admin_products_add') }}" class="navigation nav-link">
                                        <i class="far fa-circle nav-icon"></i>
                                        <p>Add</p>
                                    </a>
                                </li>
                                <li class="nav-item">
                                    <a href="{{ route('c_admin_catecories_list') }}" class="navigation nav-link">
                                        <i class="far fa-circle nav-icon"></i>
                                        <p>Category</p>
                                    </a>
                                </li>
                                <li class="nav-item">
                                    <a href="{{ route('c_admin_types_list') }}" class="navigation nav-link">
                                        <i class="far fa-circle nav-icon"></i>
                                        <p>type</p>
                                    </a>
                                </li>
                                {{-- <li class="nav-item">
                                    <a href="{{ route('c_admin_products_accounting') }}" class="navigation nav-link">
                                        <i class="far fa-circle nav-icon"></i>
                                        <p>Accounting</p>
                                    </a>
                                </li> --}}
                            @endif
                        </ul>
                    </li>
                @endif
                @if ($permissions['types_list'])
                    <li class="nav-item has-treeview">
                        <a href="{{ route('admin_types_list') }}" class="navigation nav-link">
                            <i class="nav-icon fas fa-table"></i>
                            <p>
                                Types
                                <i class="fas fa-angle-left right"></i>
                            </p>
                        </a>
                        <ul class="nav nav-treeview">
                            <li class="nav-item">
                                <a href="{{ route('admin_types_list') }}" class="navigation nav-link">
                                    <i class="far fa-circle nav-icon"></i>
                                    <p>list</p>
                                </a>
                            </li>
                        </ul>
                    </li>
                @endif


                @if ($permissions['types_list'])
                    <li class="nav-item has-treeview">
                        <a href="{{ route('admin_types_list') }}" class="navigation nav-link">
                            <i class="nav-icon fas fa-table"></i>
                            <p>
                                Attributes for products
                                <i class="fas fa-angle-left right"></i>
                            </p>
                        </a>
                        <ul class="nav nav-treeview">
                            <li class="nav-item">
                                <a href="{{ route('admin_attributes_list') }}" class="navigation nav-link">
                                    <i class="far fa-circle nav-icon"></i>
                                    <p>list</p>
                                </a>
                            </li>
                        </ul>
                    </li>
                    <li class="nav-item has-treeview">
                        <a class="navigation nav-link">
                            <i class="nav-icon fas fa-table"></i>
                            <p>
                                Coupon Code
                                <i class="fas fa-angle-left right"></i>
                            </p>
                        </a>
                        <ul class="nav nav-treeview">
                            <li class="nav-item">
                                <a href="{{ route('admin_coupon_list') }}" class="navigation nav-link">
                                    <i class="far fa-circle nav-icon"></i>
                                    <p>list</p>
                                </a>
                            </li>
                        </ul>
                    </li>
                    <li class="nav-item has-treeview">
                        <a class="navigation nav-link">
                            <i class="nav-icon fas fa-table"></i>
                            <p>
                                Notification
                                <i class="fas fa-angle-left right"></i>
                            </p>
                        </a>
                        <ul class="nav nav-treeview">
                            <li class="nav-item">
                                <a href="{{ route('notification') }}" class="navigation nav-link">
                                    <i class="far fa-circle nav-icon"></i>
                                    <p>list</p>
                                </a>
                            </li>
                        </ul>
                    </li>
                @endif
                @if ($permissions['orders_list'])
                    <li class="nav-item has-treeview">
                        <a href="{{ route('admin_orders_list') }}" class="navigation nav-link">
                            <i class="nav-icon fas fa-table"></i>
                            <p>
                                Orders
                                <i class="fas fa-angle-left right"></i>
                            </p>
                        </a>
                        <ul class="nav nav-treeview">
                            <li class="nav-item">
                                <a href="{{ route('admin_orders_list') }}" class="navigation nav-link">
                                    <i class="far fa-circle nav-icon"></i>
                                    <p>list</p>
                                </a>
                            </li>
                        </ul>
                    </li>
                @endif



                <li class="nav-item has-treeview">
                    <a href="{{ route('admin_catecories_list') }}" class="navigation nav-link">
                        <i class="nav-icon fas fa-table"></i>
                        <p>
                            Categories
                            <i class="fas fa-angle-left right"></i>
                        </p>
                    </a>
                    <ul class="nav nav-treeview">
                        <li class="nav-item">
                            <a href="{{ route('admin_catecories_list') }}" class="navigation nav-link">
                                <i class="far fa-circle nav-icon"></i>
                                <p>list</p>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a href="{{ route('admin_categories_add') }}" class="navigation nav-link">
                                <i class="far fa-circle nav-icon"></i>
                                <p>Add</p>
                            </a>
                        </li>

                    </ul>

                    @if ($permissions['roles_list'])
                <li class="nav-item has-treeview">
                    <a href="{{ route('admin_roles_list') }}" class="navigation nav-link">
                        <i class="nav-icon fas fa-table"></i>
                        <p>
                            Roles
                            <i class="fas fa-angle-left right"></i>
                        </p>
                    </a>
                    <ul class="nav nav-treeview">
                        <li class="nav-item">
                            <a href="{{ route('admin_roles_list') }}" class="navigation nav-link">
                                <i class="far fa-circle nav-icon"></i>
                                <p>list</p>
                            </a>
                        </li>
                    </ul>
                </li>
                @endif


                @if ($permissions['permissions_list'])
                    <li class="nav-item has-treeview">
                        <a href="{{ route('admin_permissions_list') }}" class="navigation nav-link">
                            <i class="nav-icon fas fa-table"></i>
                            <p>
                                Permissions
                                <i class="fas fa-angle-left right"></i>
                            </p>
                        </a>
                        <ul class="nav nav-treeview">
                            <li class="nav-item">
                                <a href="{{ route('admin_permissions_list') }}" class="navigation nav-link">
                                    <i class="far fa-circle nav-icon"></i>
                                    <p>list</p>
                                </a>
                            </li>
                        </ul>
                    </li>
                @endif

                <?php $configTab = false; ?>
                @if ($configTab)

                    <li class="nav-item has-treeview">
                        <a href="#" class="navigation nav-link">
                            <i class="nav-icon fas fa-table"></i>
                            <p>
                                Config
                                <i class="fas fa-angle-left right"></i>
                            </p>
                        </a>
                        <ul class="nav nav-treeview">
                            <li class="nav-item has-treeview">
                                <a href="#" class="navigation nav-link">
                                    <i class="nav-icon fas fa-table"></i>
                                    <p>
                                        Header
                                        <i class="fas fa-angle-left right"></i>
                                    </p>
                                </a>
                                <ul class="nav nav-treeview">
                                    <li class="nav-item">
                                        <a href="{{ route('config_header_list') }}" class="navigation nav-link">
                                            <i class="far fa-circle nav-icon"></i>
                                            <p>list</p>
                                        </a>
                                    </li>
                                    <li class="nav-item">
                                        <a href="{{ route('config_header_add') }}" class="navigation nav-link">
                                            <i class="far fa-circle nav-icon"></i>
                                            <p>Add</p>
                                        </a>
                                    </li>
                                </ul>
                            </li>
                            <li class="nav-item has-treeview">
                                <a href="#" class="navigation nav-link">
                                    <i class="nav-icon fas fa-table"></i>
                                    <p>
                                        Breadcrumb
                                        <i class="fas fa-angle-left right"></i>
                                    </p>
                                </a>
                                <ul class="nav nav-treeview">
                                    <li class="nav-item">
                                        <a href="../tables/simple.html" class="navigation nav-link">
                                            <i class="far fa-circle nav-icon"></i>
                                            <p>list</p>
                                        </a>
                                    </li>
                                    <li class="nav-item">
                                        <a href="../tables/data.html" class="navigation nav-link">
                                            <i class="far fa-circle nav-icon"></i>
                                            <p>Add</p>
                                        </a>
                                    </li>
                                </ul>
                            </li>
                        </ul>
                    </li>
                @endif

            </ul>
        </nav>
        <!-- /.sidebar-menu -->
    </div>
    <!-- /.sidebar -->
</aside>
