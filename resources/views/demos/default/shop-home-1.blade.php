@extends('Welcome.home')


@section('content')

    <?php
    $locale = App::getLocale();
    ?>

<body class="c-layout-header-fixed c-layout-header-mobile-fixed c-shop-demo-1">



<!-- END: LAYOUT/HEADERS/HEADER-SHOP-1 -->

	<!-- BEGIN: CONTENT/USER/FORGET-PASSWORD-FORM -->
<div class="modal fade c-content-login-form" id="forget-password-form" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content c-square">
            <div class="modal-header c-no-border">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            </div>
            <div class="modal-body">
                <h3 class="c-font-24 c-font-sbold">Password Recovery</h3>
                <p>To recover your password please fill in your email address</p>
                <form>
                    <div class="form-group">
                        <label for="forget-email" class="hide">Email</label>
                        <input type="email" class="form-control input-lg c-square" id="forget-email" placeholder="Email">
                    </div>
                    <div class="form-group">
                        <button type="submit" class="btn c-theme-btn btn-md c-btn-uppercase c-btn-bold c-btn-square c-btn-login">Submit</button>
                        <a href="javascript:;" class="c-btn-forgot" data-toggle="modal" data-target="#login-form" data-dismiss="modal">Back To Login</a>
                    </div>
                </form>
            </div>
            <div class="modal-footer c-no-border">
                <span class="c-text-account">Don't Have An Account Yet ?</span>
                <a href="javascript:;" data-toggle="modal" data-target="#signup-form" data-dismiss="modal" class="btn c-btn-dark-1 btn c-btn-uppercase c-btn-bold c-btn-slim c-btn-border-2x c-btn-square c-btn-signup">Signup!</a>
            </div>
        </div>
    </div>
</div><!-- END: CONTENT/USER/FORGET-PASSWORD-FORM -->
	<!-- BEGIN: CONTENT/USER/SIGNUP-FORM -->
<div class="modal fade c-content-login-form" id="signup-form" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content c-square">
            <div class="modal-header c-no-border">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            </div>
            <div class="modal-body">
                <h3 class="c-font-24 c-font-sbold">Create An Account</h3>
                <p>Please fill in below form to create an account with us</p>
                <form>
                    <div class="form-group">
                        <label for="signup-email" class="hide">Email</label>
                        <input type="email" class="form-control input-lg c-square" id="signup-email" placeholder="Email">
                    </div>
                    <div class="form-group">
                        <label for="signup-username" class="hide">Username</label>
                        <input type="email" class="form-control input-lg c-square" id="signup-username" placeholder="Username">
                    </div>
                    <div class="form-group">
                        <label for="signup-fullname" class="hide">Fullname</label>
                        <input type="email" class="form-control input-lg c-square" id="signup-fullname" placeholder="Fullname">
                    </div>
                    <div class="form-group">
                        <label for="signup-country" class="hide">Country</label>
                        <select class="form-control input-lg c-square" id="signup-country">
                            <option value="1">Country</option>
                        </select>
                    </div>
                    <div class="form-group">
                        <button type="submit" class="btn c-theme-btn btn-md c-btn-uppercase c-btn-bold c-btn-square c-btn-login">Signup</button>
                        <a href="javascript:;" class="c-btn-forgot" data-toggle="modal" data-target="#login-form" data-dismiss="modal">Back To Login</a>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div><!-- END: CONTENT/USER/SIGNUP-FORM -->
	<!-- BEGIN: CONTENT/USER/LOGIN-FORM -->
<div class="modal fade c-content-login-form" id="login-form" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content c-square">
            <div class="modal-header c-no-border">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            </div>
            <div class="modal-body">
                <h3 class="c-font-24 c-font-sbold">Good Afternoon!</h3>
                <p>Let's make today a great day!</p>
                <form>
                    <div class="form-group">
                        <label for="login-email" class="hide">Email</label>
                        <input type="email" class="form-control input-lg c-square" id="login-email" placeholder="Email">
                    </div>
                    <div class="form-group">
                        <label for="login-password" class="hide">Password</label>
                        <input type="password" class="form-control input-lg c-square" id="login-password" placeholder="Password">
                    </div>
                    <div class="form-group">
                        <div class="c-checkbox">
                            <input type="checkbox" id="login-rememberme" class="c-check">
                            <label for="login-rememberme" class="c-font-thin c-font-17">
                                <span></span>
                                <span class="check"></span>
                                <span class="box"></span>
                                Remember Me
                            </label>
                        </div>
                    </div>
                    <div class="form-group">
                        <button type="submit" class="btn c-theme-btn btn-md c-btn-uppercase c-btn-bold c-btn-square c-btn-login">Login</button>
                        <a href="javascript:;" data-toggle="modal" data-target="#forget-password-form" data-dismiss="modal" class="c-btn-forgot">Forgot Your Password ?</a>
                    </div>
                    <div class="clearfix">
                        <div class="c-content-divider c-divider-sm c-icon-bg c-bg-grey c-margin-b-20">
                            <span>or signup with</span>
                        </div>
                        <ul class="c-content-list-adjusted">
                            <li>
                                <a class="btn btn-block c-btn-square btn-social btn-twitter">
                                  <i class="fa fa-twitter"></i>
                                  Twitter
                                </a>
                            </li>
                            <li>
                                <a class="btn btn-block c-btn-square btn-social btn-facebook">
                                  <i class="fa fa-facebook"></i>
                                  Facebook
                                </a>
                            </li>
                            <li>
                                <a class="btn btn-block c-btn-square btn-social btn-google">
                                  <i class="fa fa-google"></i>
                                  Google
                                </a>
                            </li>
                        </ul>
                    </div>
                </form>
            </div>
            <div class="modal-footer c-no-border">
                <span class="c-text-account">Don't Have An Account Yet ?</span>
                <a href="javascript:;" data-toggle="modal" data-target="#signup-form" data-dismiss="modal" class="btn c-btn-dark-1 btn c-btn-uppercase c-btn-bold c-btn-slim c-btn-border-2x c-btn-square c-btn-signup">Signup!</a>
            </div>
        </div>
    </div>
</div><!-- END: CONTENT/USER/LOGIN-FORM -->

	<!-- BEGIN: LAYOUT/SIDEBARS/QUICK-SIDEBAR -->
<nav class="c-layout-quick-sidebar">
	<div class="c-header">
		<button type="button" class="c-link c-close">
		<i class="icon-login"></i>
		</button>
	</div>
	<div class="c-content">
		<div class="c-section">
			<h3>JANGO DEMOS</h3>
			<div class="c-settings c-demos c-bs-grid-reset-space">
				<div class="row">
					<div class="col-md-12">
						<a href="../default/index.html" class="c-demo-container c-demo-img-lg">
							<div class="c-demo-thumb active">
								<img src="../../assets/base/img/content/quick-sidebar/default.jpg" class="c-demo-thumb-img"/>
							</div>
						</a>
					</div>
				</div>
				<div class="row">
					<div class="col-md-6">
						<a href="../corporate_1/index.html" class="c-demo-container">
							<div class="c-demo-thumb  c-thumb-left">
								<img src="../../assets/base/img/content/quick-sidebar/corporate_1.jpg" class="c-demo-thumb-img"/>
							</div>
						</a>
					</div>
					<div class="col-md-6">
						<a href="../agency_1/index.html" class="c-demo-container">
							<div class="c-demo-thumb  c-thumb-right">
								<img src="../../assets/base/img/content/quick-sidebar/corporate_1-onepage.jpg" class="c-demo-thumb-img"/>
							</div>
						</a>
					</div>
				</div>
			</div>
		</div>
		<div class="c-section">
			<h3>Theme Colors</h3>
			<div class="c-settings">

				<span class="c-color c-default c-active" data-color="default"></span>

				<span class="c-color c-green1" data-color="green1"></span>
				<span class="c-color c-green2" data-color="green2"></span>
				<span class="c-color c-green3" data-color="green3"></span>

				<span class="c-color c-yellow1" data-color="yellow1"></span>
				<span class="c-color c-yellow2" data-color="yellow2"></span>
				<span class="c-color c-yellow3" data-color="yellow3"></span>

				<span class="c-color c-red1" data-color="red1"></span>
				<span class="c-color c-red2" data-color="red2"></span>
				<span class="c-color c-red3" data-color="red3"></span>

				<span class="c-color c-purple1" data-color="purple1"></span>
				<span class="c-color c-purple2" data-color="purple2"></span>
				<span class="c-color c-purple3" data-color="purple3"></span>

				<span class="c-color c-blue1" data-color="blue1"></span>
				<span class="c-color c-blue2" data-color="blue2"></span>
				<span class="c-color c-blue3" data-color="blue3"></span>

				<span class="c-color c-brown1" data-color="brown1"></span>
				<span class="c-color c-brown2" data-color="brown2"></span>
				<span class="c-color c-brown3" data-color="brown3"></span>

				<span class="c-color c-dark1" data-color="dark1"></span>
				<span class="c-color c-dark2" data-color="dark2"></span>
				<span class="c-color c-dark3" data-color="dark3"></span>
			</div>
		</div>
		<div class="c-section">
			<h3>Header Type</h3>
			<div class="c-settings">
				<input type="button" class="c-setting_header-type btn btn-sm c-btn-square c-btn-border-1x c-btn-white c-btn-sbold c-btn-uppercase active" data-value="boxed" value="boxed"/>
				<input type="button" class="c-setting_header-type btn btn-sm c-btn-square c-btn-border-1x c-btn-white c-btn-sbold c-btn-uppercase" data-value="fluid" value="fluid"/>
			</div>
		</div>
		<div class="c-section">
			<h3>Header Mode</h3>
			<div class="c-settings">
				<input type="button" class="c-setting_header-mode btn btn-sm c-btn-square c-btn-border-1x c-btn-white c-btn-sbold c-btn-uppercase active" data-value="fixed" value="fixed"/>
				<input type="button" class="c-setting_header-mode btn btn-sm c-btn-square c-btn-border-1x c-btn-white c-btn-sbold c-btn-uppercase" data-value="static" value="static"/>
			</div>
		</div>
		<div class="c-section">
			<h3>Mega Menu Style</h3>
			<div class="c-settings">
				<input type="button" class="c-setting_megamenu-style btn btn-sm c-btn-square c-btn-border-1x c-btn-white c-btn-sbold c-btn-uppercase active" data-value="dark" value="dark"/>
				<input type="button" class="c-setting_megamenu-style btn btn-sm c-btn-square c-btn-border-1x c-btn-white c-btn-sbold c-btn-uppercase" data-value="light" value="light"/>
			</div>
		</div>
		<div class="c-section">
			<h3>Font Style</h3>
			<div class="c-settings">
				<input type="button" class="c-setting_font-style btn btn-sm c-btn-square c-btn-border-1x c-btn-white c-btn-sbold c-btn-uppercase active" data-value="default" value="default"/>
				<input type="button" class="c-setting_font-style btn btn-sm c-btn-square c-btn-border-1x c-btn-white c-btn-sbold c-btn-uppercase" data-value="light" value="light"/>
			</div>
		</div>
		<div class="c-section">
			<h3>Reading Style</h3>
			<div class="c-settings">
				<a href="http://www.themehats.com/themes/jango/" class="c-setting_font-style btn btn-sm c-btn-square c-btn-border-1x c-btn-white c-btn-sbold c-btn-uppercase ">LTR</a>
				<a href="http://www.themehats.com/themes/jango/rtl/" class="c-setting_font-style btn btn-sm c-btn-square c-btn-border-1x c-btn-white c-btn-sbold c-btn-uppercase active">RTL</a>
			</div>
		</div>
	</div>
</nav><!-- END: LAYOUT/SIDEBARS/QUICK-SIDEBAR -->

	<!-- BEGIN: PAGE CONTAINER -->
	<div class="c-layout-page">
		<!-- BEGIN: PAGE CONTENT -->
		<!-- BEGIN: LAYOUT/SLIDERS/SHOP-REVO-SLIDER-1 -->
<!-- BEGIN: REVOLUTION SLIDER 2 -->
<section class="c-layout-revo-slider c-layout-revo-slider-13" dir="ltr">
	<div class="tp-banner-container tp-fullscreen">
		<div class="tp-banner rev_slider" data-version="5.0">
			<ul>
				<!--BEGIN: SLIDE #1 -->
				<li data-transition="fade" data-slotamount="1" data-masterspeed="1000">
					<img
						alt=""
						src="../../assets/base/img/content/shop4/57.jpg"
						data-bgposition="center center"
						data-bgfit="cover"
						data-bgrepeat="no-repeat"
					>

					<!--BEGIN: MAIN TITLE -->
					<div class="tp-caption customin customout tp-resizeme"
						data-x="center"
						data-y="center"
						data-hoffset="-385"
						data-voffset="-200"

						data-speed="500"
						data-start="1000"
						data-transform_in="x:0;y:0;z:0;rotationX:0.5;rotationY:0;rotationZ:0;scaleX:0.75;scaleY:0.75;skewX:0;skewY:0;opacity:0;s:500;e:Back.easeInOut;"
						data-transform_out="x:0;y:0;z:0;rotationX:0;rotationY:0;rotationZ:0;scaleX:0.75;scaleY:0.75;skewX:0;skewY:0;opacity:0;s:600;e:Back.easeInOut;"

						data-splitin="none"
						data-splitout="none"
						data-elementdelay="0.1"
						data-endelementdelay="0.1"
						data-endspeed="600"

						>
						<h3 class="c-main-title c-font-bold c-font-uppercase c-font-white">
							WINTER COLLECTION
						</h3>
					</div>
					<!--END -->

					<!--BEGIN: SUB TITLE -->
					<div class="tp-caption customin customout tp-resizeme c-center"
						data-x="center"
						data-y="center"
						data-hoffset="-390"
						data-voffset="-35"

						data-speed="500"
						data-start="1500"
						data-transform_in="x:0;y:0;z:0;rotationX:0.5;rotationY:0;rotationZ:0;scaleX:0.75;scaleY:0.75;skewX:0;skewY:0;opacity:0;s:500;e:Back.easeInOut;"
						data-transform_out="x:0;y:0;z:0;rotationX:0;rotationY:0;rotationZ:0;scaleX:0.75;scaleY:0.75;skewX:0;skewY:0;opacity:0;s:600;e:Back.easeInOut;"

						data-splitin="none"
						data-splitout="none"
						data-elementdelay="0.1"
						data-endelementdelay="0.1"
						data-endspeed="600"

						>
						<h1 class="c-highlight-title">ON SALE <span class="c-theme-font">NOW</span></h1>
						<div class="c-slider-line c-bg-white"></div>
						<p class="c-sub-title c-font-16 c-font-white c-font-uppercase">
						Lorem ipsum dolor sit amet, consectetuer<br>
						elit sed diam nonummy et nibh euismod
						</p>
					</div>
					<!--END -->

					<!--BEGIN: ACTION BUTTON -->
					<div class="tp-caption randomrotateout tp-resizeme"
						data-x="center"
						data-y="center"
						data-hoffset="-390"
						data-voffset="120"
						data-speed="500"
						data-start="2000"
						data-transform_in="x:0;y:0;z:0;rotationX:0.5;rotationY:0;rotationZ:0;scaleX:0.75;scaleY:0.75;skewX:0;skewY:0;opacity:0;s:500;e:Back.easeInOut;"
						data-transform_out="x:0;y:0;z:0;rotationX:0;rotationY:0;rotationZ:0;scaleX:0.75;scaleY:0.75;skewX:0;skewY:0;opacity:0;s:500;e:Back.easeInOut;" >
							<a href="#" class="c-action-btn btn btn-md c-btn-square c-btn-bold c-btn-border-2x c-btn-white c-btn-uppercase c-center">Shop Now</a>
					</div>
					<!--END -->
		 		</li>
		 		<!--END -->

				<!--BEGIN: SLIDE #2 -->
				<li data-transition="fade" data-slotamount="1" data-masterspeed="1000">
					<img
						alt=""
						src="../../assets/base/img/content/shop4/79.jpg"
						data-bgposition="center center"
						data-bgfit="cover"
						data-bgrepeat="no-repeat"
					>

					<!--BEGIN: MAIN TITLE -->
					<div class="tp-caption customin customout tp-resizeme"
						data-x="center"
						data-y="center"
						data-hoffset="385"
						data-voffset="-200"

						data-speed="500"
						data-start="1000"
						data-transform_in="x:0;y:0;z:0;rotationX:0.5;rotationY:0;rotationZ:0;scaleX:0.75;scaleY:0.75;skewX:0;skewY:0;opacity:0;s:500;e:Back.easeInOut;"
						data-transform_out="x:0;y:0;z:0;rotationX:0;rotationY:0;rotationZ:0;scaleX:0.75;scaleY:0.75;skewX:0;skewY:0;opacity:0;s:600;e:Back.easeInOut;"

						data-splitin="none"
						data-splitout="none"
						data-elementdelay="0.1"
						data-endelementdelay="0.1"
						data-endspeed="600"

						>
						<h3 class="c-main-title c-font-bold c-font-uppercase c-theme-font c-summer">
							SUMMER COLLECTION
						</h3>
					</div>
					<!--END -->

					<!--BEGIN: SUB TITLE -->
					<div class="tp-caption customin customout tp-resizeme c-center"
						data-x="center"
						data-y="center"
						data-hoffset="390"
						data-voffset="-35"

						data-speed="500"
						data-start="1500"
						data-transform_in="x:0;y:0;z:0;rotationX:0.5;rotationY:0;rotationZ:0;scaleX:0.75;scaleY:0.75;skewX:0;skewY:0;opacity:0;s:500;e:Back.easeInOut;"
						data-transform_out="x:0;y:0;z:0;rotationX:0;rotationY:0;rotationZ:0;scaleX:0.75;scaleY:0.75;skewX:0;skewY:0;opacity:0;s:600;e:Back.easeInOut;"

						data-splitin="none"
						data-splitout="none"
						data-elementdelay="0.1"
						data-endelementdelay="0.1"
						data-endspeed="600"

						>
						<h1 class="c-highlight-title">ON SALE <span class="c-theme-font">NOW</span></h1>
						<div class="c-slider-line c-theme-bg"></div>
						<p class="c-sub-title c-font-16 c-font-uppercase">
						Lorem ipsum dolor sit amet, consectetuer<br>
						elit sed diam nonummy et nibh euismod
						</p>
					</div>
					<!--END -->

					<!--BEGIN: ACTION BUTTON -->
					<div class="tp-caption randomrotateout tp-resizeme"
						data-x="center"
						data-y="center"
						data-hoffset="390"
						data-voffset="120"
						data-speed="500"
						data-start="2000"
						data-transform_in="x:0;y:0;z:0;rotationX:0.5;rotationY:0;rotationZ:0;scaleX:0.75;scaleY:0.75;skewX:0;skewY:0;opacity:0;s:500;e:Back.easeInOut;"
						data-transform_out="x:0;y:0;z:0;rotationX:0;rotationY:0;rotationZ:0;scaleX:0.75;scaleY:0.75;skewX:0;skewY:0;opacity:0;s:500;e:Back.easeInOut;" >
							<a href="#" class="c-action-btn btn btn-md c-btn-square c-btn-bold c-btn-border-2x c-theme-btn c-btn-uppercase c-center">Shop Now</a>
					</div>
					<!--END -->
		 		</li>
		 		<!--END -->


			</ul>
		</div>
	</div>
</section><!-- END: LAYOUT/SLIDERS/SHOP-REVO-SLIDER-1 -->

<!-- BEGIN: CONTENT/SHOPS/SHOP-1-1 -->
<div class="c-content-box c-size-md">
	<div class="container">
		<div class="c-content-tab-5 c-bs-grid-reset-space c-theme">
			<!-- Nav tabs -->
			<ul class="nav nav-pills c-nav-tab c-arrow" role="tablist">
				<li role="presentation" class="active">
					<a class="c-font-uppercase" href="#watches" aria-controls="watches" role="tab" data-toggle="tab">Watches</a>
				</li>
				<li role="presentation">
					<a class="c-font-uppercase" href="#phone" aria-controls="phone" role="tab" data-toggle="tab">Phone</a>
				</li>
				<li role="presentation">
					<a class="c-font-uppercase" href="#imac" aria-controls="imac" role="tab" data-toggle="tab">iMac</a>
				</li>
				<li role="presentation">
					<a class="c-font-uppercase" href="#accessories" aria-controls="accessories" role="tab" data-toggle="tab">Accessories</a>
				</li>
			</ul>
			<!-- Tab panes -->
			<div class="tab-content">
				<div role="tabpanel" class="tab-pane fade in active" id="watches">
					<div class="row">
						<div class="col-sm-4">
							<div class="c-content c-content-overlay">
								<div class="c-overlay-wrapper c-overlay-padding">
									<div class="c-overlay-content">
										<a href="shop-product-details-2.html" class="btn btn-md c-btn-grey-1 c-btn-uppercase c-btn-bold c-btn-border-1x c-btn-square">Explore</a>
									</div>
								</div>
								<div class="c-bg-img-center c-overlay-object" data-height="height" style="height: 270px; background-image: url(../../assets/base/img/content/shop2/69.jpg);"></div>
								<div class="c-overlay-border"></div>
							</div>
						</div>
						<div class="col-sm-4">
							<div class="c-content c-content-overlay">
								<div class="c-overlay-wrapper c-overlay-padding">
									<div class="c-overlay-content">
										<a href="shop-product-details-2.html" class="btn btn-md c-btn-grey-1 c-btn-uppercase c-btn-bold c-btn-border-1x c-btn-square">Explore</a>
									</div>
								</div>
								<div class="c-bg-img-center c-overlay-object" data-height="height" style="height: 270px; background-image: url(../../assets/base/img/content/shop2/70.jpg);"></div>
								<div class="c-overlay-border"></div>
							</div>
						</div>
						<div class="col-sm-4">
							<div class="c-content c-content-overlay">
								<div class="c-overlay-wrapper c-overlay-padding">
									<div class="c-overlay-content">
										<a href="shop-product-details-2.html" class="btn btn-md c-btn-grey-1 c-btn-uppercase c-btn-bold c-btn-border-1x c-btn-square">Explore</a>
									</div>
								</div>
								<div class="c-bg-img-center c-overlay-object" data-height="height" style="height: 270px; background-image: url(../../assets/base/img/content/shop2/79.jpg);"></div>
								<div class="c-overlay-border"></div>
							</div>
						</div>
					</div>
				</div>

				<div role="tabpanel" class="tab-pane fade" id="phone">
					<div class="row">
						<div class="col-sm-4">
							<div class="c-content c-content-overlay">
								<div class="c-overlay-wrapper c-overlay-padding">
									<div class="c-overlay-content">
										<a href="shop-product-details-2.html" class="btn btn-md c-btn-grey-1 c-btn-uppercase c-btn-bold c-btn-border-1x c-btn-square">Explore</a>
									</div>
								</div>
								<div class="c-bg-img-center c-overlay-object" data-height="height" style="height: 270px; background-image: url(../../assets/base/img/content/shop2/63.jpg);"></div>
								<div class="c-overlay-border"></div>
							</div>
						</div>
						<div class="col-sm-4">
							<div class="c-content c-content-overlay">
								<div class="c-overlay-wrapper c-overlay-padding">
									<div class="c-overlay-content">
										<a href="shop-product-details-2.html" class="btn btn-md c-btn-grey-1 c-btn-uppercase c-btn-bold c-btn-border-1x c-btn-square">Explore</a>
									</div>
								</div>
								<div class="c-bg-img-center c-overlay-object" data-height="height" style="height: 270px; background-image: url(../../assets/base/img/content/shop2/59.jpg);"></div>
								<div class="c-overlay-border"></div>
							</div>
						</div>
						<div class="col-sm-4">
							<div class="c-content c-content-overlay">
								<div class="c-overlay-wrapper c-overlay-padding">
									<div class="c-overlay-content">
										<a href="shop-product-details-2.html" class="btn btn-md c-btn-grey-1 c-btn-uppercase c-btn-bold c-btn-border-1x c-btn-square">Explore</a>
									</div>
								</div>
								<div class="c-bg-img-center c-overlay-object" data-height="height" style="height: 270px; background-image: url(../../assets/base/img/content/shop2/71.jpg);"></div>
								<div class="c-overlay-border"></div>
							</div>
						</div>
					</div>
				</div>

				<div role="tabpanel" class="tab-pane fade" id="imac">
					<div class="row">
						<div class="col-sm-4">
							<div class="c-content c-content-overlay">
								<div class="c-overlay-wrapper c-overlay-padding">
									<div class="c-overlay-content">
										<a href="shop-product-details-2.html" class="btn btn-md c-btn-grey-1 c-btn-uppercase c-btn-bold c-btn-border-1x c-btn-square">Explore</a>
									</div>
								</div>
								<div class="c-bg-img-center c-overlay-object" data-height="height" style="height: 270px; background-image: url(../../assets/base/img/content/shop3/73.jpg);"></div>
								<div class="c-overlay-border"></div>
							</div>
						</div>
						<div class="col-sm-4">
							<div class="c-content c-content-overlay">
								<div class="c-overlay-wrapper c-overlay-padding">
									<div class="c-overlay-content">
										<a href="shop-product-details-2.html" class="btn btn-md c-btn-grey-1 c-btn-uppercase c-btn-bold c-btn-border-1x c-btn-square">Explore</a>
									</div>
								</div>
								<div class="c-bg-img-center c-overlay-object" data-height="height" style="height: 270px; background-image: url(../../assets/base/img/content/shop3/74.jpg);"></div>
								<div class="c-overlay-border"></div>
							</div>
						</div>
						<div class="col-sm-4">
							<div class="c-content c-content-overlay">
								<div class="c-overlay-wrapper c-overlay-padding">
									<div class="c-overlay-content">
										<a href="shop-product-details-2.html" class="btn btn-md c-btn-grey-1 c-btn-uppercase c-btn-bold c-btn-border-1x c-btn-square">Explore</a>
									</div>
								</div>
								<div class="c-bg-img-center c-overlay-object" data-height="height" style="height: 270px; background-image: url(../../assets/base/img/content/shop3/91.jpg);"></div>
								<div class="c-overlay-border"></div>
							</div>
						</div>
					</div>
				</div>

				<div role="tabpanel" class="tab-pane fade" id="accessories">
					<div class="row">
						<div class="col-sm-4">
							<div class="c-content c-content-overlay">
								<div class="c-overlay-wrapper c-overlay-padding">
									<div class="c-overlay-content">
										<a href="shop-product-details-2.html" class="btn btn-md c-btn-grey-1 c-btn-uppercase c-btn-bold c-btn-border-1x c-btn-square">Explore</a>
									</div>
								</div>
								<div class="c-bg-img-center c-overlay-object" data-height="height" style="height: 270px; background-image: url(../../assets/base/img/content/shop3/63.jpg);"></div>
								<div class="c-overlay-border"></div>
							</div>
						</div>
						<div class="col-sm-4">
							<div class="c-content c-content-overlay">
								<div class="c-overlay-wrapper c-overlay-padding">
									<div class="c-overlay-content">
										<a href="shop-product-details-2.html" class="btn btn-md c-btn-grey-1 c-btn-uppercase c-btn-bold c-btn-border-1x c-btn-square">Explore</a>
									</div>
								</div>
								<div class="c-bg-img-center c-overlay-object" data-height="height" style="height: 270px; background-image: url(../../assets/base/img/content/shop3/67.jpg);"></div>
								<div class="c-overlay-border"></div>
							</div>
						</div>
						<div class="col-sm-4">
							<div class="c-content c-content-overlay">
								<div class="c-overlay-wrapper c-overlay-padding">
									<div class="c-overlay-content">
										<a href="shop-product-details-2.html" class="btn btn-md c-btn-grey-1 c-btn-uppercase c-btn-bold c-btn-border-1x c-btn-square">Explore</a>
									</div>
								</div>
								<div class="c-bg-img-center c-overlay-object" data-height="height" style="height: 270px; background-image: url(../../assets/base/img/content/shop3/77.jpg);"></div>
								<div class="c-overlay-border"></div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div><!-- END: CONTENT/SHOPS/SHOP-1-1 -->

<!-- BEGIN: CONTENT/SHOPS/SHOP-2-1 -->
<div class="c-content-box c-size-md c-overflow-hide c-bs-grid-small-space c-bg-grey-1">
	<div class="container">
		<div class="c-content-title-4">
			<h3 class="c-font-uppercase c-center c-font-bold c-line-strike"><span class="c-bg-grey-1">Most Popular</span></h3>
		</div>
		<div class="row">
			<div class="col-md-3 col-sm-6 c-margin-b-20">
				<div class="c-content-product-2 c-bg-white">
					<div class="c-content-overlay">
						<div class="c-label c-bg-red c-font-uppercase c-font-white c-font-13 c-font-bold">Sale</div>
						<div class="c-label c-label-right c-theme-bg c-font-uppercase c-font-white c-font-13 c-font-bold">New</div>
						<div class="c-overlay-wrapper">
							<div class="c-overlay-content">
								<a href="shop-product-details.html" class="btn btn-md c-btn-grey-1 c-btn-uppercase c-btn-bold c-btn-border-1x c-btn-square">Explore</a>
							</div>
						</div>
						<div class="c-bg-img-center c-overlay-object" data-height="height" style="height: 270px; background-image: url(../../assets/base/img/content/shop2/93.jpg);"></div>
					</div>
					<div class="c-info">
						<p class="c-title c-font-18 c-font-slim">Smartphone & Handset</p>
						<p class="c-price c-font-16 c-font-slim">$548 &nbsp;
							<span class="c-font-16 c-font-line-through c-font-red">$600</span>
						</p>
					</div>
					<div class="btn-group btn-group-justified" role="group">
						<div class="btn-group c-border-top" role="group">
							<a href="shop-product-wishlist.html" class="btn btn-lg c-btn-white c-btn-uppercase c-btn-square c-font-grey-3 c-font-white-hover c-bg-red-2-hover c-btn-product">Wishlist</a>
						</div>
						<div class="btn-group c-border-left c-border-top" role="group">
							<a href="shop-cart.html" class="btn btn-lg c-btn-white c-btn-uppercase c-btn-square c-font-grey-3 c-font-white-hover c-bg-red-2-hover c-btn-product">Cart</a>
						</div>
					</div>
				</div>
			</div>
			<div class="col-md-3 col-sm-6 c-margin-b-20">
				<div class="c-content-product-2 c-bg-white">
					<div class="c-content-overlay">
						<div class="c-overlay-wrapper">
							<div class="c-overlay-content">
								<a href="shop-product-details.html" class="btn btn-md c-btn-grey-1 c-btn-uppercase c-btn-bold c-btn-border-1x c-btn-square">Explore</a>
							</div>
						</div>
						<div class="c-bg-img-center c-overlay-object" data-height="height" style="height: 270px; background-image: url(../../assets/base/img/content/shop2/95.jpg);"></div>
					</div>
					<div class="c-info">
						<p class="c-title c-font-18 c-font-slim">Canvas Shoes</p>
						<p class="c-price c-font-16 c-font-slim">$548 &nbsp;
							<span class="c-font-16 c-font-line-through c-font-red">$600</span>
						</p>
					</div>
					<div class="btn-group btn-group-justified" role="group">
						<div class="btn-group c-border-top" role="group">
							<a href="shop-product-wishlist.html" class="btn btn-lg c-btn-white c-btn-uppercase c-btn-square c-font-grey-3 c-font-white-hover c-bg-red-2-hover c-btn-product">Wishlist</a>
						</div>
						<div class="btn-group c-border-left c-border-top" role="group">
							<a href="shop-cart.html" class="btn btn-lg c-btn-white c-btn-uppercase c-btn-square c-font-grey-3 c-font-white-hover c-bg-red-2-hover c-btn-product">Cart</a>
						</div>
					</div>
				</div>
			</div>
			<div class="col-md-3 col-sm-6 c-margin-b-20">
				<div class="c-content-product-2 c-bg-white">
					<div class="c-content-overlay">
						<div class="c-overlay-wrapper">
							<div class="c-overlay-content">
								<a href="shop-product-details.html" class="btn btn-md c-btn-grey-1 c-btn-uppercase c-btn-bold c-btn-border-1x c-btn-square">Explore</a>
							</div>
						</div>
						<div class="c-bg-img-center c-overlay-object" data-height="height" style="height: 270px; background-image: url(../../assets/base/img/content/shop2/91.jpg);"></div>
					</div>
					<div class="c-info">
						<p class="c-title c-font-18 c-font-slim">Shoes & Tie</p>
						<p class="c-price c-font-16 c-font-slim">$548 &nbsp;
							<span class="c-font-16 c-font-line-through c-font-red">$600</span>
						</p>
					</div>
					<div class="btn-group btn-group-justified" role="group">
						<div class="btn-group c-border-top" role="group">
							<a href="shop-product-wishlist.html" class="btn btn-lg c-btn-white c-btn-uppercase c-btn-square c-font-grey-3 c-font-white-hover c-bg-red-2-hover c-btn-product">Wishlist</a>
						</div>
						<div class="btn-group c-border-left c-border-top" role="group">
							<a href="shop-cart.html" class="btn btn-lg c-btn-white c-btn-uppercase c-btn-square c-font-grey-3 c-font-white-hover c-bg-red-2-hover c-btn-product">Cart</a>
						</div>
					</div>
				</div>
			</div>
			<div class="col-md-3 col-sm-6 c-margin-b-20">
				<div class="c-content-product-2 c-bg-white">
					<div class="c-content-overlay">
						<div class="c-label c-theme-bg c-font-uppercase c-font-white c-font-13 c-font-bold">New</div>
						<div class="c-overlay-wrapper">
							<div class="c-overlay-content">
								<a href="shop-product-details.html" class="btn btn-md c-btn-grey-1 c-btn-uppercase c-btn-bold c-btn-border-1x c-btn-square">Explore</a>
							</div>
						</div>
						<div class="c-bg-img-center c-overlay-object" data-height="height" style="height: 270px; background-image: url(../../assets/base/img/content/shop2/88.jpg);"></div>
					</div>
					<div class="c-info">
						<p class="c-title c-font-18 c-font-slim">Smartphone & Handset</p>
						<p class="c-price c-font-16 c-font-slim">$548 &nbsp;
							<span class="c-font-16 c-font-line-through c-font-red">$600</span>
						</p>
					</div>
					<div class="btn-group btn-group-justified" role="group">
						<div class="btn-group c-border-top" role="group">
							<a href="shop-product-wishlist.html" class="btn btn-lg c-btn-white c-btn-uppercase c-btn-square c-font-grey-3 c-font-white-hover c-bg-red-2-hover c-btn-product">Wishlist</a>
						</div>
						<div class="btn-group c-border-left c-border-top" role="group">
							<a href="shop-cart.html" class="btn btn-lg c-btn-white c-btn-uppercase c-btn-square c-font-grey-3 c-font-white-hover c-bg-red-2-hover c-btn-product">Cart</a>
						</div>
					</div>
				</div>
			</div>
		</div>

		<div class="row">
			<div class="col-md-3 col-sm-6 c-margin-b-20">
				<div class="c-content-product-2 c-bg-white">
					<div class="c-content-overlay">
						<div class="c-label c-bg-red c-font-uppercase c-font-white c-font-13 c-font-bold">Sale</div>
						<div class="c-overlay-wrapper">
							<div class="c-overlay-content">
								<a href="shop-product-details.html" class="btn btn-md c-btn-grey-1 c-btn-uppercase c-btn-bold c-btn-border-1x c-btn-square">Explore</a>
							</div>
						</div>
						<div class="c-bg-img-center c-overlay-object" data-height="height" style="height: 270px; background-image: url(../../assets/base/img/content/shop2/89.jpg);"></div>
					</div>
					<div class="c-info">
						<p class="c-title c-font-18 c-font-slim">Watches & Accessories</p>
						<p class="c-price c-font-16 c-font-slim">$548 &nbsp;
							<span class="c-font-16 c-font-line-through c-font-red">$600</span>
						</p>
					</div>
					<div class="btn-group btn-group-justified" role="group">
						<div class="btn-group c-border-top" role="group">
							<a href="shop-product-wishlist.html" class="btn btn-lg c-btn-white c-btn-uppercase c-btn-square c-font-grey-3 c-font-white-hover c-bg-red-2-hover c-btn-product">Wishlist</a>
						</div>
						<div class="btn-group c-border-left c-border-top" role="group">
							<a href="shop-cart.html" class="btn btn-lg c-btn-white c-btn-uppercase c-btn-square c-font-grey-3 c-font-white-hover c-bg-red-2-hover c-btn-product">Cart</a>
						</div>
					</div>
				</div>
			</div>
			<div class="col-md-3 col-sm-6 c-margin-b-20">
				<div class="c-content-product-2 c-bg-white">
					<div class="c-content-overlay">
						<div class="c-overlay-wrapper">
							<div class="c-overlay-content">
								<a href="shop-product-details.html" class="btn btn-md c-btn-grey-1 c-btn-uppercase c-btn-bold c-btn-border-1x c-btn-square">Explore</a>
							</div>
						</div>
						<div class="c-bg-img-center c-overlay-object" data-height="height" style="height: 270px; background-image: url(../../assets/base/img/content/shop2/96.jpg);"></div>
					</div>
					<div class="c-info">
						<p class="c-title c-font-18 c-font-slim">Exclusive Watches</p>
						<p class="c-price c-font-16 c-font-slim">$548 &nbsp;
							<span class="c-font-16 c-font-line-through c-font-red">$600</span>
						</p>
					</div>
					<div class="btn-group btn-group-justified" role="group">
						<div class="btn-group c-border-top" role="group">
							<a href="shop-product-wishlist.html" class="btn btn-lg c-btn-white c-btn-uppercase c-btn-square c-font-grey-3 c-font-white-hover c-bg-red-2-hover c-btn-product">Wishlist</a>
						</div>
						<div class="btn-group c-border-left c-border-top" role="group">
							<a href="shop-cart.html" class="btn btn-lg c-btn-white c-btn-uppercase c-btn-square c-font-grey-3 c-font-white-hover c-bg-red-2-hover c-btn-product">Cart</a>
						</div>
					</div>
				</div>
			</div>
			<div class="col-md-3 col-sm-6 c-margin-b-20">
				<div class="c-content-product-2 c-bg-white">
					<div class="c-content-overlay">
						<div class="c-overlay-wrapper">
							<div class="c-overlay-content">
								<a href="shop-product-details.html" class="btn btn-md c-btn-grey-1 c-btn-uppercase c-btn-bold c-btn-border-1x c-btn-square">Explore</a>
							</div>
						</div>
						<div class="c-bg-img-center c-overlay-object" data-height="height" style="height: 270px; background-image: url(../../assets/base/img/content/shop2/102.jpg);"></div>
					</div>
					<div class="c-info">
						<p class="c-title c-font-18 c-font-slim">Watches & Accessories</p>
						<p class="c-price c-font-16 c-font-slim">$548 &nbsp;
							<span class="c-font-16 c-font-line-through c-font-red">$600</span>
						</p>
					</div>
					<div class="btn-group btn-group-justified" role="group">
						<div class="btn-group c-border-top" role="group">
							<a href="shop-product-wishlist.html" class="btn btn-lg c-btn-white c-btn-uppercase c-btn-square c-font-grey-3 c-font-white-hover c-bg-red-2-hover c-btn-product">Wishlist</a>
						</div>
						<div class="btn-group c-border-left c-border-top" role="group">
							<a href="shop-cart.html" class="btn btn-lg c-btn-white c-btn-uppercase c-btn-square c-font-grey-3 c-font-white-hover c-bg-red-2-hover c-btn-product">Cart</a>
						</div>
					</div>
				</div>
			</div>
			<div class="col-md-3 col-sm-6 c-margin-b-20">
				<div class="c-content-product-2 c-bg-white">
					<div class="c-content-overlay">
						<div class="c-label c-theme-bg c-font-uppercase c-font-white c-font-13 c-font-bold">New</div>
						<div class="c-overlay-wrapper">
							<div class="c-overlay-content">
								<a href="shop-product-details.html" class="btn btn-md c-btn-grey-1 c-btn-uppercase c-btn-bold c-btn-border-1x c-btn-square">Explore</a>
							</div>
						</div>
						<div class="c-bg-img-center c-overlay-object" data-height="height" style="height: 270px; background-image: url(../../assets/base/img/content/shop2/100.jpg);"></div>
					</div>
					<div class="c-info">
						<p class="c-title c-font-18 c-font-slim">Titanium Watch</p>
						<p class="c-price c-font-16 c-font-slim">$548 &nbsp;
							<span class="c-font-16 c-font-line-through c-font-red">$600</span>
						</p>
					</div>
					<div class="btn-group btn-group-justified" role="group">
						<div class="btn-group c-border-top" role="group">
							<a href="shop-product-wishlist.html" class="btn btn-lg c-btn-white c-btn-uppercase c-btn-square c-font-grey-3 c-font-white-hover c-bg-red-2-hover c-btn-product">Wishlist</a>
						</div>
						<div class="btn-group c-border-left c-border-top" role="group">
							<a href="shop-cart.html" class="btn btn-lg c-btn-white c-btn-uppercase c-btn-square c-font-grey-3 c-font-white-hover c-bg-red-2-hover c-btn-product">Cart</a>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div><!-- END: CONTENT/SHOPS/SHOP-2-1 -->

<!-- BEGIN: CONTENT/SHOPS/SHOP-3-2 -->
<div class="c-content-box c-size-lg c-no-padding c-overflow-hide c-bg-white">
	<div class="c-content-product-3 c-bs-grid-reset-space">
		<div class="row">
			<div class="col-md-4 col-sm-6">
				<div class="c-wrapper c-theme-bg" style="height: 550px;">
					<div class="c-content c-border c-border-padding c-border-padding-left">
						<h3 class="c-title c-font-25 c-font-white c-font-uppercase c-font-bold">With Theme Color</h3>
						<p class="c-description c-font-17 c-font-white">Lorem ipsum dolor sit amet, consectetuer adipiscing elit sed diam nonummy et nibh euismod aliquam erat volutpat.</p>
						<p class="c-description c-font-17 c-font-white">Dolor sit amet, consectetuer adipiscing elit sed diam nonummy</p>
						<p class="c-price c-font-55 c-font-white c-font-thin">$249</p>
						<a href="shop-product-details.html" class="btn btn-lg c-btn-white c-font-uppercase c-btn-square c-btn-border-1x">BUY NOW</a>
					</div>
				</div>
			</div>
			<div class="col-md-8 col-sm-6">
				<div class="c-content-overlay">
					<div class="c-overlay-wrapper">
						<div class="c-overlay-content">
							<a href="shop-product-details.html" class="btn btn-md c-btn-grey-1 c-btn-uppercase c-btn-bold c-btn-border-1x c-btn-square">Explore</a>
						</div>
					</div>
					<div class="c-bg-img-center c-overlay-object" data-height="height" style="height: 550px; background-image: url(../../assets/base/img/content/shop7/04.png);"></div>
				</div>
			</div>
		</div>
	</div>
</div><!-- END: CONTENT/SHOPS/SHOP-3-2 -->

<!-- BEGIN: CONTENT/MISC/PROMO-1-4 -->
<div class="c-content-box c-size-lg c-bg-grey-1 c-no-bottom-padding">
	<div class="container">
		<div class="row">
			<div class="c-content-title-1 c-center c-margin-b-80">
				<h3 class="c-font-uppercase c-font-bold">Life Should be Great Rather Than Long</h3>
				<p class="c-font-uppercase c-font-grey-3">Lorem ipsum dolor sit amet, consectetuer adipiscing elit sed diam <br />nonummy et nibh euismod aliquam erat volutpat.</p>
				<a href="#" class="btn btn-lg c-theme-btn c-btn-square c-font-uppercase">Purchase</a>
			</div>
		</div>
	</div>
</div>
<!-- END: CONTENT/MISC/PROMO-1-4 -->

<!-- BEGIN: CONTENT/SHOPS/SHOP-2-3 -->
<div class="c-content-box c-size-md c-overflow-hide c-bs-grid-small-space" style="background-image: url(../../assets/base/img/content/shop-backgrounds/135.jpg)">
	<div class="container">
	<div class="c-content-title-1">
		<h3 class="c-font-uppercase c-center c-font-bold">Most Popular</h3>
		<div class="c-line-center c-theme-bg"></div>
	</div>
		<div class="row">
			<div class="col-md-3 col-sm-6 c-margin-b-20">
				<div class="c-content-product-2 c-bg-white">
					<div class="c-content-overlay">
						<div class="c-label c-bg-red c-font-uppercase c-font-white c-font-13 c-font-bold">Sale</div>
						<div class="c-label c-label-right c-theme-bg c-font-uppercase c-font-white c-font-13 c-font-bold">New</div>
						<div class="c-overlay-wrapper">
							<div class="c-overlay-content">
								<a href="shop-product-details.html" class="btn btn-md c-btn-grey-1 c-btn-uppercase c-btn-bold c-btn-border-1x c-btn-square">Explore</a>
							</div>
						</div>
						<div class="c-bg-img-center c-overlay-object" data-height="height" style="height: 330px; background-image: url(../../assets/base/img/content/shop/106.jpg);"></div>
					</div>
					<div class="c-info">
						<p class="c-title c-font-18 c-font-slim">Latest Fashion</p>
						<p class="c-price c-font-16 c-font-slim">$548 &nbsp;
							<span class="c-font-16 c-font-line-through c-font-red">$600</span>
						</p>
					</div>
					<div class="btn-group btn-group-justified" role="group">
						<div class="btn-group c-border-top" role="group">
							<a href="shop-product-wishlist.html" class="btn btn-lg c-btn-white c-btn-uppercase c-btn-square c-font-grey-3 c-font-white-hover c-bg-red-2-hover c-btn-product">Wishlist</a>
						</div>
						<div class="btn-group c-border-left c-border-top" role="group">
							<a href="shop-cart.html" class="btn btn-lg c-btn-white c-btn-uppercase c-btn-square c-font-grey-3 c-font-white-hover c-bg-red-2-hover c-btn-product">Cart</a>
						</div>
					</div>
				</div>
			</div>
			<div class="col-md-3 col-sm-6 c-margin-b-20">
				<div class="c-content-product-2 c-bg-white">
					<div class="c-content-overlay">
						<div class="c-overlay-wrapper">
							<div class="c-overlay-content">
								<a href="shop-product-details.html" class="btn btn-md c-btn-grey-1 c-btn-uppercase c-btn-bold c-btn-border-1x c-btn-square">Explore</a>
							</div>
						</div>
						<div class="c-bg-img-top-center c-overlay-object" data-height="height" style="height: 330px; background-image: url(../../assets/base/img/content/shop/107.jpg);"></div>
					</div>
					<div class="c-info">
						<p class="c-title c-font-18 c-font-slim">New Fashion</p>
						<p class="c-price c-font-16 c-font-slim">$548 &nbsp;
							<span class="c-font-16 c-font-line-through c-font-red">$600</span>
						</p>
					</div>
					<div class="btn-group btn-group-justified" role="group">
						<div class="btn-group c-border-top" role="group">
							<a href="shop-product-wishlist.html" class="btn btn-lg c-btn-white c-btn-uppercase c-btn-square c-font-grey-3 c-font-white-hover c-bg-red-2-hover c-btn-product">Wishlist</a>
						</div>
						<div class="btn-group c-border-left c-border-top" role="group">
							<a href="shop-cart.html" class="btn btn-lg c-btn-white c-btn-uppercase c-btn-square c-font-grey-3 c-font-white-hover c-bg-red-2-hover c-btn-product">Cart</a>
						</div>
					</div>
				</div>
			</div>
			<div class="col-md-3 col-sm-6 c-margin-b-20">
				<div class="c-content-product-2 c-bg-white">
					<div class="c-content-overlay">
						<div class="c-overlay-wrapper">
							<div class="c-overlay-content">
								<a href="shop-product-details.html" class="btn btn-md c-btn-grey-1 c-btn-uppercase c-btn-bold c-btn-border-1x c-btn-square">Explore</a>
							</div>
						</div>
						<div class="c-bg-img-center c-overlay-object" data-height="height" style="height: 330px; background-image: url(../../assets/base/img/content/shop/120.jpg);"></div>
					</div>
					<div class="c-info">
						<p class="c-title c-font-18 c-font-slim">Latest Trends</p>
						<p class="c-price c-font-16 c-font-slim">$548 &nbsp;
							<span class="c-font-16 c-font-line-through c-font-red">$600</span>
						</p>
					</div>
					<div class="btn-group btn-group-justified" role="group">
						<div class="btn-group c-border-top" role="group">
							<a href="shop-product-wishlist.html" class="btn btn-lg c-btn-white c-btn-uppercase c-btn-square c-font-grey-3 c-font-white-hover c-bg-red-2-hover c-btn-product">Wishlist</a>
						</div>
						<div class="btn-group c-border-left c-border-top" role="group">
							<a href="shop-cart.html" class="btn btn-lg c-btn-white c-btn-uppercase c-btn-square c-font-grey-3 c-font-white-hover c-bg-red-2-hover c-btn-product">Cart</a>
						</div>
					</div>
				</div>
			</div>
			<div class="col-md-3 col-sm-6 c-margin-b-20">
				<div class="c-content-product-2 c-bg-white">
					<div class="c-content-overlay">
						<div class="c-label c-theme-bg c-font-uppercase c-font-white c-font-13 c-font-bold">New</div>
						<div class="c-overlay-wrapper">
							<div class="c-overlay-content">
								<a href="shop-product-details.html" class="btn btn-md c-btn-grey-1 c-btn-uppercase c-btn-bold c-btn-border-1x c-btn-square">Explore</a>
							</div>
						</div>
						<div class="c-bg-img-center c-overlay-object" data-height="height" style="height: 330px; background-image: url(../../assets/base/img/content/shop/117.jpg);"></div>
					</div>
					<div class="c-info">
						<p class="c-title c-font-18 c-font-slim">Modern</p>
						<p class="c-price c-font-16 c-font-slim">$548 &nbsp;
							<span class="c-font-16 c-font-line-through c-font-red">$600</span>
						</p>
					</div>
					<div class="btn-group btn-group-justified" role="group">
						<div class="btn-group c-border-top" role="group">
							<a href="shop-product-wishlist.html" class="btn btn-lg c-btn-white c-btn-uppercase c-btn-square c-font-grey-3 c-font-white-hover c-bg-red-2-hover c-btn-product">Wishlist</a>
						</div>
						<div class="btn-group c-border-left c-border-top" role="group">
							<a href="shop-cart.html" class="btn btn-lg c-btn-white c-btn-uppercase c-btn-square c-font-grey-3 c-font-white-hover c-bg-red-2-hover c-btn-product">Cart</a>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div><!-- END: CONTENT/SHOPS/SHOP-2-3 -->
		<!-- END: PAGE CONTENT -->


	</div>
	<!-- END: PAGE CONTAINER -->

	<!-- BEGIN: LAYOUT/FOOTERS/FOOTER-6 -->
</body>


    @section('script')
        <!-- BEGIN: PAGE SCRIPTS -->
        <script>
            $(document).ready(function() {
                var slider = $('.c-layout-revo-slider .tp-banner');

                var cont = $('.c-layout-revo-slider .tp-banner-container');

                var api = slider.show().revolution({
                    sliderType:"standard",
                    sliderLayout:"fullscreen",
                    responsiveLevels:[2048,1024,778,320],
                    gridwidth: [1240, 1024, 778, 320],
                    gridheight: [868, 768, 960, 720],
                    delay: 15000,
                    startwidth:1170,
                    startheight: App.getViewPort().height,

                    navigationType: "hide",
                    navigationArrows: "solo",

                    touchenabled: "on",
                    navigation: {
                        keyboardNavigation:"off",
                        keyboard_direction: "horizontal",
                        mouseScrollNavigation:"off",
                        onHoverStop:"on",
                        arrows: {
                            style:"circle",
                            enable:true,
                            hide_onmobile:false,
                            hide_onleave:false,
                            tmp:'',
                            left: {
                                h_align:"left",
                                v_align:"center",
                                h_offset:30,
                                v_offset:0
                            },
                            right: {
                                h_align:"right",
                                v_align:"center",
                                h_offset:30,
                                v_offset:0
                            }
                        }
                    },

                    spinner: "spinner2",

                    fullScreenOffsetContainer: '.c-layout-header',

                    shadow: 0,

                    disableProgressBar:"on",

                    hideThumbsOnMobile: "on",
                    hideNavDelayOnMobile: 1500,
                    hideBulletsOnMobile: "on",
                    hideArrowsOnMobile: "on",
                    hideThumbsUnderResolution: 0
                });
            }); //ready
        </script>
        <!-- END: PAGE SCRIPTS -->

    @endsection
