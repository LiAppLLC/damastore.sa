


@extends('Welcome.home')


@section('content')

    <?php
    $locale = App::getLocale();
    ?>



<body class="c-layout-header-fixed c-layout-header-mobile-fixed c-layout-header-topbar c-layout-header-topbar-collapse">


	<!-- BEGIN: PAGE CONTAINER -->
	<div class="c-layout-page">
		<!-- BEGIN: LAYOUT/BREADCRUMBS/BREADCRUMBS-2 -->
<div class="c-layout-breadcrumbs-1 c-subtitle c-fonts-uppercase c-fonts-bold c-bordered c-bordered-both">
	<div class="container">
		<div class="c-page-title c-pull-left">
			<h3 class="c-font-uppercase c-font-sbold">Customer Edit Profile</h3>
			<h4 class="">Page Sub Title Goes Here</h4>
		</div>
		<ul class="c-page-breadcrumbs c-theme-nav c-pull-right c-fonts-regular">
															<li class="c-state_active">Jango Components</li>

		</ul>
	</div>
</div><!-- END: LAYOUT/BREADCRUMBS/BREADCRUMBS-2 -->
		<div class="container">
			<div class="c-layout-sidebar-menu c-theme ">
			<!-- BEGIN: LAYOUT/SIDEBARS/SHOP-SIDEBAR-DASHBOARD -->
<div class="c-sidebar-menu-toggler">
	<h3 class="c-title c-font-uppercase c-font-bold">My Profile</h3>
	<a href="javascript:;" class="c-content-toggler" data-toggle="collapse" data-target="#sidebar-menu-1">
		<span class="c-line"></span> <span class="c-line"></span> <span class="c-line"></span>
	</a>
</div>

<ul class="c-sidebar-menu collapse " id="sidebar-menu-1">
	<li class="c-dropdown c-open">
		<a href="javascript:;" class="c-toggler">My Profile<span class="c-arrow"></span></a>
        <ul class="c-dropdown-menu">
            <li class="">
                <a href="{{asset('CustomerDashboard')}}">My Dashbord</a>
            </li>
            <li class="c-active">
                <a href="{{asset('editprofile')}}">Edit Profile</a>
            </li>
            <li class="">
                <a href="{{asset('changePassword')}}">Change Password</a>
            </li>
            <li class="">
                <a href="shop-order-history.html">Order History</a>
            </li>
            <li class="">
                <a href="shop-customer-addresses.html">My Addresses</a>
            </li>
            <li class="">
                <a href="shop-product-wishlist.html">My Wishlist</a>
            </li>
        </ul>
	</li>
</ul><!-- END: LAYOUT/SIDEBARS/SHOP-SIDEBAR-DASHBOARD -->
			</div>
			<div class="c-layout-sidebar-content ">
			<!-- BEGIN: PAGE CONTENT -->
			<div class="c-content-title-1">
	<h3 class="c-font-uppercase c-font-bold">Edit Profile</h3>
	<div class="c-line-left"></div>
</div>
	<form class="c-shop-form-1" id="editUserForm">
	{{ csrf_field() }}
	<!-- BEGIN: ADDRESS FORM -->
	<div class="">



		<!-- BEGIN: BILLING ADDRESS -->
		{{--<div class="row">--}}
			{{--<div class="form-group col-md-12">--}}
				{{--<label class="control-label">Country</label>--}}
				{{--<select class="form-control c-square c-theme">--}}
					{{--<option value="1">Malaysia</option>--}}
					{{--<option value="2">Singapore</option>--}}
					{{--<option value="3">Indonesia</option>--}}
					{{--<option value="4">Thailand</option>--}}
					{{--<option value="5">China</option>--}}
				{{--</select>--}}
			{{--</div>--}}
		{{--</div>--}}
		{{--<div class="row">--}}
			{{--<div class="col-md-12">--}}
				{{--<div class="row">--}}
					{{--<div class="form-group col-md-6">--}}
						{{--<label class="control-label">First Name</label>--}}
						{{--<input type="text" class="form-control c-square c-theme" placeholder="First Name">--}}
					{{--</div>--}}
					{{--<div class="col-md-6">--}}
						{{--<label class="control-label">Last Name</label>--}}
						{{--<input type="text" class="form-control c-square c-theme" placeholder="Last Name">--}}
					{{--</div>--}}
				{{--</div>--}}
			{{--</div>--}}
		{{--</div>--}}
		{{--<div class="row">--}}
			{{--<div class="form-group col-md-12">--}}
				{{--<label class="control-label">Address</label>--}}
				{{--<input type="text" class="form-control c-square c-theme" placeholder="Street Address">--}}
			{{--</div>--}}
		{{--</div>--}}
		{{--<div class="row">--}}
			{{--<div class="form-group col-md-12">--}}
				{{--<input type="text" class="form-control c-square c-theme" placeholder="Apartment, suite, unit etc. (optional)">--}}
			{{--</div>--}}
		{{--</div>--}}
		{{--<div class="row">--}}
			{{--<div class="form-group col-md-12">--}}
				{{--<label class="control-label">Town / City</label>--}}
				{{--<input type="text" class="form-control c-square c-theme" placeholder="Town / City">--}}
			{{--</div>--}}
		{{--</div>--}}
		{{--<div class="row">--}}
			{{--<div class="col-md-12">--}}
				{{--<div class="row">--}}
					{{--<div class="form-group col-md-6">--}}
						{{--<label class="control-label">State / County</label> <select class="form-control c-square c-theme">--}}
							{{--<option value="0">Select an option...</option>--}}
							{{--<option value="1">Malaysia</option>--}}
							{{--<option value="2">Singapore</option>--}}
							{{--<option value="3">Indonesia</option>--}}
							{{--<option value="4">Thailand</option>--}}
							{{--<option value="5">China</option>--}}
						{{--</select>--}}
					{{--</div>--}}
					{{--<div class="col-md-6">--}}
						{{--<label class="control-label">Postcode / Zip</label>--}}
						{{--<input type="text" class="form-control c-square c-theme" placeholder="Postcode / Zip">--}}
					{{--</div>--}}
				{{--</div>--}}
			{{--</div>--}}
		{{--</div>--}}
		{{--<div class="row">--}}
			{{--<div class="col-md-12">--}}
				{{--<div class="row">--}}
					{{--<div class="form-group col-md-6">--}}
						{{--<label class="control-label">Email Address</label>--}}
						{{--<input type="email" class="form-control c-square c-theme" placeholder="Email Address">--}}
					{{--</div>--}}
					{{--<div class="col-md-6">--}}
						{{--<label class="control-label">Phone</label>--}}
						{{--<input type="tel" class="form-control c-square c-theme" placeholder="Phone">--}}
					{{--</div>--}}
				{{--</div>--}}
			{{--</div>--}}
		{{--</div>--}}
		<!-- END: BILLING ADDRESS -->



		<div class="form-group">
			<div class="row">
				<div class="col-md-12">
					<label class="control-label">User Name</label>
					<input type="text" id="username" name="username" class="form-control c-square c-theme" placeholder="User Name">
				</div>
				<div class="col-md-6">
					<label class="control-label">First Name</label>
					<input type="text" id="firstName" name="firstName" class="form-control c-square c-theme" placeholder="First Name">
				</div>
				<div class="col-md-6">
					<label class="control-label">Last Name</label>
					<input type="text" id="lastName" name="lastName" class="form-control c-square c-theme" placeholder="Last Name">
				</div>
			</div>
		</div>
		<div class="form-group">
			<label class="control-label">Born Date</label>
			<input type="date" id="bornDate" name="bornDate" class="form-control c-square c-theme" placeholder="yyyy-mm-dd">
		</div>
		<div class="row">
			<div class="form-group col-md-6">
				<label class="control-label">Email Address</label>
				<input type="email" id="email" name="email" class="form-control c-square c-theme" placeholder="Email Address">
			</div>
			<div class="col-md-6">
				<label class="control-label">Phone</label>
				<input type="tel" id="phone" name="phone" class="form-control c-square c-theme" placeholder="Phone">
			</div>
		</div>



		<!-- BEGIN: PASSWORD -->
		{{--<div class="row">--}}
			{{--<div class="form-group col-md-12">--}}
				{{--<label class="control-label">Change Password</label>--}}
				{{--<input type="password" class="form-control c-square c-theme" placeholder="Password">--}}
			{{--</div>--}}
		{{--</div>--}}
		{{--<div class="row">--}}
			{{--<div class="form-group col-md-12">--}}
				{{--<label class="control-label">Repeat Password</label>--}}
				{{--<input type="password" class="form-control c-square c-theme" placeholder="Password">--}}
				{{--<p class="help-block">Hint: The password should be at least six characters long. <br />--}}
					{{--To make it stronger, use upper and lower case letters, numbers, and symbols like ! " ? $ % ^ & ).</p>--}}
			{{--</div>--}}
		{{--</div>--}}
		<!-- END: PASSWORD -->




		<div class="row c-margin-t-30">
			<div class="form-group col-md-12" role="group">
				<button type="submit" class="btn btn-lg c-theme-btn c-btn-square c-btn-uppercase c-btn-bold">Submit</button>
				<button type="submit" class="btn btn-lg btn-default c-btn-square c-btn-uppercase c-btn-bold">Cancel</button>
			</div>
		</div>
	</div>
	<!-- END: ADDRESS FORM -->
</form>			<!-- END: PAGE CONTENT -->
			</div>
		</div>
	</div>
	<!-- END: PAGE CONTAINER -->

</body>


@endsection





@section('script')


	<script>
        var userObject = JSON.parse(localStorage.getItem("DamaStore_userObject"));
		$('#username').val(userObject.username);
		$('#firstName').val(userObject.firstName);
		$('#lastName').val(userObject.lastName);
		$('#bornDate').val(userObject.bornDate);
		$('#email').val(userObject.email);
		$('#phone').val(userObject.phone);
        // blocked
        // bornDate
        // created_at
        // email
        // emailVerifiedAt
        // files
        // firstName
        // id
        // lastActivity
        // lastLoggedIn
        // lastName
        // phone
        // reputation
        // roles
        // token
        // updated_at
        // username
        // verify
        console.log('userObject', userObject);


        $('#editUserForm').submit(function (e) {

            var formData = new FormData($(this)[0]);
            e.preventDefault();
            e.stopPropagation();

            $.ajax({

                type: "POST",
                url: '{{route('editUser')}}',
                data: formData,
                // mimeType: 'application/json',
                processData: false,
                contentType: false,
                headers: {"Authorization": 'Bearer ' + userObject.token},
                success: function (res) {
                    if(res.status == 201) {
                        Swal.fire({
                            icon: 'success',
                            title: 'success',
                            text: 'your profile has been edited successfuly, thanks for using our services',
                        }).then(function() {
                            localStorage.setItem("DamaStore_userObject", JSON.stringify(res.data));
                            window.location = "/CustomerDashboard";
                        });

                    }
                    if(res.status == 400) {
                        // console.log(res.data.validator);
                        Swal.fire({
                            icon: 'error',
                            title: 'Oops...',
                            text: res.data.validator,
                        });
                    }

                },
                error: function (res, response) {
                },
            });


        });


	</script>

@endsection



