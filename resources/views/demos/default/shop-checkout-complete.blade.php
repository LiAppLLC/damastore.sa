@extends('Welcome.home')


@section('content')

    <?php
    $locale = App::getLocale();
    ?>


    <body class="c-layout-header-fixed c-layout-header-mobile-fixed c-layout-header-topbar c-layout-header-topbar-collapse">


<div class="modal fade c-content-login-form" id="forget-password-form" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content c-square">
            <div class="modal-header c-no-border">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            </div>
            <div class="modal-body">
                <h3 class="c-font-24 c-font-sbold">Password Recovery</h3>
                <p>To recover your password please fill in your email address</p>
                <form>
                    <div class="form-group">
                        <label for="forget-email" class="hide">Email</label>
                        <input type="email" class="form-control input-lg c-square" id="forget-email" placeholder="Email">
                    </div>
                    <div class="form-group">
                        <button type="submit" class="btn c-theme-btn btn-md c-btn-uppercase c-btn-bold c-btn-square c-btn-login">Submit</button>
                        <a href="javascript:;" class="c-btn-forgot" data-toggle="modal" data-target="#login-form" data-dismiss="modal">Back To Login</a>
                    </div>
                </form>
            </div>
            <div class="modal-footer c-no-border">
                <span class="c-text-account">Don't Have An Account Yet ?</span>
                <a href="javascript:;" data-toggle="modal" data-target="#signup-form" data-dismiss="modal" class="btn c-btn-dark-1 btn c-btn-uppercase c-btn-bold c-btn-slim c-btn-border-2x c-btn-square c-btn-signup">Signup!</a>
            </div>
        </div>
    </div>
</div><!-- END: CONTENT/USER/FORGET-PASSWORD-FORM -->
	<!-- BEGIN: CONTENT/USER/SIGNUP-FORM -->
<div class="modal fade c-content-login-form" id="signup-form" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content c-square">
            <div class="modal-header c-no-border">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            </div>
            <div class="modal-body">
                <h3 class="c-font-24 c-font-sbold">Create An Account</h3>
                <p>Please fill in below form to create an account with us</p>
                <form>
                    <div class="form-group">
                        <label for="signup-email" class="hide">Email</label>
                        <input type="email" class="form-control input-lg c-square" id="signup-email" placeholder="Email">
                    </div>
                    <div class="form-group">
                        <label for="signup-username" class="hide">Username</label>
                        <input type="email" class="form-control input-lg c-square" id="signup-username" placeholder="Username">
                    </div>
                    <div class="form-group">
                        <label for="signup-fullname" class="hide">Fullname</label>
                        <input type="email" class="form-control input-lg c-square" id="signup-fullname" placeholder="Fullname">
                    </div>
                    <div class="form-group">
                        <label for="signup-country" class="hide">Country</label>
                        <select class="form-control input-lg c-square" id="signup-country">
                            <option value="1">Country</option>
                        </select>
                    </div>
                    <div class="form-group">
                        <button type="submit" class="btn c-theme-btn btn-md c-btn-uppercase c-btn-bold c-btn-square c-btn-login">Signup</button>
                        <a href="javascript:;" class="c-btn-forgot" data-toggle="modal" data-target="#login-form" data-dismiss="modal">Back To Login</a>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div><!-- END: CONTENT/USER/SIGNUP-FORM -->
	<!-- BEGIN: CONTENT/USER/LOGIN-FORM -->
<div class="modal fade c-content-login-form" id="login-form" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content c-square">
            <div class="modal-header c-no-border">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            </div>
            <div class="modal-body">
                <h3 class="c-font-24 c-font-sbold">Good Afternoon!</h3>
                <p>Let's make today a great day!</p>
                <form>
                    <div class="form-group">
                        <label for="login-email" class="hide">Email</label>
                        <input type="email" class="form-control input-lg c-square" id="login-email" placeholder="Email">
                    </div>
                    <div class="form-group">
                        <label for="login-password" class="hide">Password</label>
                        <input type="password" class="form-control input-lg c-square" id="login-password" placeholder="Password">
                    </div>
                    <div class="form-group">
                        <div class="c-checkbox">
                            <input type="checkbox" id="login-rememberme" class="c-check">
                            <label for="login-rememberme" class="c-font-thin c-font-17">
                                <span></span>
                                <span class="check"></span>
                                <span class="box"></span>
                                Remember Me
                            </label>
                        </div>
                    </div>
                    <div class="form-group">
                        <button type="submit" class="btn c-theme-btn btn-md c-btn-uppercase c-btn-bold c-btn-square c-btn-login">Login</button>
                        <a href="javascript:;" data-toggle="modal" data-target="#forget-password-form" data-dismiss="modal" class="c-btn-forgot">Forgot Your Password ?</a>
                    </div>
                    <div class="clearfix">
                        <div class="c-content-divider c-divider-sm c-icon-bg c-bg-grey c-margin-b-20">
                            <span>or signup with</span>
                        </div>
                        <ul class="c-content-list-adjusted">
                            <li>
                                <a class="btn btn-block c-btn-square btn-social btn-twitter">
                                  <i class="fa fa-twitter"></i>
                                  Twitter
                                </a>
                            </li>
                            <li>
                                <a class="btn btn-block c-btn-square btn-social btn-facebook">
                                  <i class="fa fa-facebook"></i>
                                  Facebook
                                </a>
                            </li>
                            <li>
                                <a class="btn btn-block c-btn-square btn-social btn-google">
                                  <i class="fa fa-google"></i>
                                  Google
                                </a>
                            </li>
                        </ul>
                    </div>
                </form>
            </div>
            <div class="modal-footer c-no-border">
                <span class="c-text-account">Don't Have An Account Yet ?</span>
                <a href="javascript:;" data-toggle="modal" data-target="#signup-form" data-dismiss="modal" class="btn c-btn-dark-1 btn c-btn-uppercase c-btn-bold c-btn-slim c-btn-border-2x c-btn-square c-btn-signup">Signup!</a>
            </div>
        </div>
    </div>
</div><!-- END: CONTENT/USER/LOGIN-FORM -->

	<!-- BEGIN: LAYOUT/SIDEBARS/QUICK-SIDEBAR -->
<nav class="c-layout-quick-sidebar">
	<div class="c-header">
		<button type="button" class="c-link c-close">
		<i class="icon-login"></i>
		</button>
	</div>
	<div class="c-content">
		<div class="c-section">
			<h3>JANGO DEMOS</h3>
			<div class="c-settings c-demos c-bs-grid-reset-space">
				<div class="row">
					<div class="col-md-12">
						<a href="../default/index.html" class="c-demo-container c-demo-img-lg">
							<div class="c-demo-thumb active">
								<img src="../../assets/base/img/content/quick-sidebar/default.jpg" class="c-demo-thumb-img"/>
							</div>
						</a>
					</div>
				</div>
				<div class="row">
					<div class="col-md-6">
						<a href="../corporate_1/index.html" class="c-demo-container">
							<div class="c-demo-thumb  c-thumb-left">
								<img src="../../assets/base/img/content/quick-sidebar/corporate_1.jpg" class="c-demo-thumb-img"/>
							</div>
						</a>
					</div>
					<div class="col-md-6">
						<a href="../agency_1/index.html" class="c-demo-container">
							<div class="c-demo-thumb  c-thumb-right">
								<img src="../../assets/base/img/content/quick-sidebar/corporate_1-onepage.jpg" class="c-demo-thumb-img"/>
							</div>
						</a>
					</div>
				</div>
			</div>
		</div>
		<div class="c-section">
			<h3>Theme Colors</h3>
			<div class="c-settings">

				<span class="c-color c-default c-active" data-color="default"></span>

				<span class="c-color c-green1" data-color="green1"></span>
				<span class="c-color c-green2" data-color="green2"></span>
				<span class="c-color c-green3" data-color="green3"></span>

				<span class="c-color c-yellow1" data-color="yellow1"></span>
				<span class="c-color c-yellow2" data-color="yellow2"></span>
				<span class="c-color c-yellow3" data-color="yellow3"></span>

				<span class="c-color c-red1" data-color="red1"></span>
				<span class="c-color c-red2" data-color="red2"></span>
				<span class="c-color c-red3" data-color="red3"></span>

				<span class="c-color c-purple1" data-color="purple1"></span>
				<span class="c-color c-purple2" data-color="purple2"></span>
				<span class="c-color c-purple3" data-color="purple3"></span>

				<span class="c-color c-blue1" data-color="blue1"></span>
				<span class="c-color c-blue2" data-color="blue2"></span>
				<span class="c-color c-blue3" data-color="blue3"></span>

				<span class="c-color c-brown1" data-color="brown1"></span>
				<span class="c-color c-brown2" data-color="brown2"></span>
				<span class="c-color c-brown3" data-color="brown3"></span>

				<span class="c-color c-dark1" data-color="dark1"></span>
				<span class="c-color c-dark2" data-color="dark2"></span>
				<span class="c-color c-dark3" data-color="dark3"></span>
			</div>
		</div>
		<div class="c-section">
			<h3>Header Type</h3>
			<div class="c-settings">
				<input type="button" class="c-setting_header-type btn btn-sm c-btn-square c-btn-border-1x c-btn-white c-btn-sbold c-btn-uppercase active" data-value="boxed" value="boxed"/>
				<input type="button" class="c-setting_header-type btn btn-sm c-btn-square c-btn-border-1x c-btn-white c-btn-sbold c-btn-uppercase" data-value="fluid" value="fluid"/>
			</div>
		</div>
		<div class="c-section">
			<h3>Header Mode</h3>
			<div class="c-settings">
				<input type="button" class="c-setting_header-mode btn btn-sm c-btn-square c-btn-border-1x c-btn-white c-btn-sbold c-btn-uppercase active" data-value="fixed" value="fixed"/>
				<input type="button" class="c-setting_header-mode btn btn-sm c-btn-square c-btn-border-1x c-btn-white c-btn-sbold c-btn-uppercase" data-value="static" value="static"/>
			</div>
		</div>
		<div class="c-section">
			<h3>Mega Menu Style</h3>
			<div class="c-settings">
				<input type="button" class="c-setting_megamenu-style btn btn-sm c-btn-square c-btn-border-1x c-btn-white c-btn-sbold c-btn-uppercase active" data-value="dark" value="dark"/>
				<input type="button" class="c-setting_megamenu-style btn btn-sm c-btn-square c-btn-border-1x c-btn-white c-btn-sbold c-btn-uppercase" data-value="light" value="light"/>
			</div>
		</div>
		<div class="c-section">
			<h3>Font Style</h3>
			<div class="c-settings">
				<input type="button" class="c-setting_font-style btn btn-sm c-btn-square c-btn-border-1x c-btn-white c-btn-sbold c-btn-uppercase active" data-value="default" value="default"/>
				<input type="button" class="c-setting_font-style btn btn-sm c-btn-square c-btn-border-1x c-btn-white c-btn-sbold c-btn-uppercase" data-value="light" value="light"/>
			</div>
		</div>
		<div class="c-section">
			<h3>Reading Style</h3>
			<div class="c-settings">
				<a href="http://www.themehats.com/themes/jango/" class="c-setting_font-style btn btn-sm c-btn-square c-btn-border-1x c-btn-white c-btn-sbold c-btn-uppercase ">LTR</a>
				<a href="http://www.themehats.com/themes/jango/rtl/" class="c-setting_font-style btn btn-sm c-btn-square c-btn-border-1x c-btn-white c-btn-sbold c-btn-uppercase active">RTL</a>
			</div>
		</div>
	</div>
</nav><!-- END: LAYOUT/SIDEBARS/QUICK-SIDEBAR -->

	<!-- BEGIN: PAGE CONTAINER -->
	<div class="c-layout-page">
		<!-- BEGIN: LAYOUT/BREADCRUMBS/BREADCRUMBS-2 -->
<div class="c-layout-breadcrumbs-1 c-subtitle c-fonts-uppercase c-fonts-bold c-bordered c-bordered-both">
	<div class="container">
		<div class="c-page-title c-pull-left">
			<h3 class="c-font-uppercase c-font-sbold">Checkout Complete</h3>
			<h4 class="">Page Sub Title Goes Here</h4>
		</div>
		<ul class="c-page-breadcrumbs c-theme-nav c-pull-right c-fonts-regular">
															<li><a href="shop-checkout-complete.html">Checkout Complete</a></li>
			<li>/</li>
															<li class="c-state_active">Jango Components</li>

		</ul>
	</div>
</div><!-- END: LAYOUT/BREADCRUMBS/BREADCRUMBS-2 -->
		<!-- BEGIN: PAGE CONTENT -->
		<div class="c-content-box c-size-lg c-overflow-hide c-bg-white">
	<div class="container">
		<div class="c-shop-order-complete-1 c-content-bar-1 c-align-left c-bordered c-theme-border c-shadow">
			<div class="c-content-title-1">
				<h3 class="c-center c-font-uppercase c-font-bold">Checkout Completed</h3>
				<div class="c-line-center c-theme-bg"></div>
			</div>
			<div class="c-theme-bg">
				<p class="c-message c-center c-font-white c-font-20 c-font-sbold">
					<i class="fa fa-check"></i> Thank you. Your order has been received.
				</p>
			</div>
			<!-- BEGIN: ORDER SUMMARY -->
			<div class="row c-order-summary c-center">
				<ul class="c-list-inline list-inline">
					<li>
						<h3>Order Number</h3>
						<p>#12345</p>
					</li>
					<li>
						<h3>Date Purchased</h3>
						<p>August 26, 2015</p>
					</li>
					<li>
						<h3>Total Payable</h3>
						<p>$95.00</p>
					</li>
					<li>
						<h3>Payment Method</h3>
						<p>Direct Bank Transfer</p>
					</li>
				</ul>
			</div>
			<!-- END: ORDER SUMMARY -->
			<!-- BEGIN: BANK DETAILS -->
			<div class="c-bank-details c-margin-t-30 c-margin-b-30">
				<p class="c-margin-b-20">Make your payment directly into our account. Please use your Order ID as the payment reference. Your order won't be shipped until the funds have cleared in our account.</p>

				<h3 class="c-margin-t-40 c-margin-b-20 c-font-uppercase c-font-22 c-font-bold">OUR BANK DETAILS</h3>
				<h3 class="c-border-bottom">Account Name : &nbsp;<span class="c-font-thin">Themehats</span></h3>
				<ul class="c-list-inline list-inline">
					<li>
						<h3>Account Number</h3>
						<p>12345678901234567</p>
					</li>
					<li>
						<h3>Sort Code</h3>
						<p>123</p>
					</li>
					<li>
						<h3>Bank</h3>
						<p>Bank Name</p>
					</li>
					<li>
						<h3>BIC</h3>
						<p>12345</p>
					</li>
				</ul>
			</div>
			<!-- END: BANK DETAILS -->
			<!-- BEGIN: ORDER DETAILS -->
			<div class="c-order-details">
				<div class="c-border-bottom hidden-sm hidden-xs">
					<div class="row">
						<div class="col-md-3">
							<h3 class="c-font-uppercase c-font-16 c-font-grey-2 c-font-bold">Product</h3>
						</div>
						<div class="col-md-5">
							<h3 class="c-font-uppercase c-font-16 c-font-grey-2 c-font-bold">Description</h3>
						</div>
						<div class="col-md-2">
							<h3 class="c-font-uppercase c-font-16 c-font-grey-2 c-font-bold">Unit Price</h3>
						</div>
						<div class="col-md-2">
							<h3 class="c-font-uppercase c-font-16 c-font-grey-2 c-font-bold">Total</h3>
						</div>
					</div>
				</div>
				<!-- BEGIN: PRODUCT ITEM ROW -->
				<div class="c-border-bottom c-row-item">
					<div class="row">
						<div class="col-md-3 col-sm-12 c-image">
							<div class="c-content-overlay">
								<div class="c-overlay-wrapper">
									<div class="c-overlay-content">
										<a href="shop-product-details-2.html" class="btn btn-md c-btn-grey-1 c-btn-uppercase c-btn-bold c-btn-border-1x c-btn-square">Explore</a>
									</div>
								</div>
								<div class="c-bg-img-top-center c-overlay-object" data-height="height">
									<img width="100%" class="img-responsive" src="../../assets/base/img/content/shop2/24.jpg">
								</div>
							</div>
						</div>
						<div class="col-md-5 col-sm-8">
							<ul class="c-list list-unstyled">
								<li class="c-margin-b-25"><a href="shop-product-details-2.html" class="c-font-bold c-font-22 c-theme-link">Winter Coat</a></li>
								<li class="c-margin-b-10">Color: Blue</li>
								<li>Size: S</li>
								<li>Quantity: x1</li>
							</ul>
						</div>
						<div class="col-md-2 col-sm-2">
							<p class="visible-xs-block c-theme-font c-font-uppercase c-font-bold">Unit Price</p>
							<p class="c-font-sbold c-font-uppercase c-font-18">$20.00</p>
						</div>
						<div class="col-md-2 col-sm-2">
							<p class="visible-xs-block c-theme-font c-font-uppercase c-font-bold">Total</p>
							<p class="c-font-sbold c-font-18">$20.00</p>
						</div>
					</div>
				</div>
				<!-- END: PRODUCT ITEM ROW -->
				<!-- BEGIN: PRODUCT ITEM ROW -->
				<div class="c-border-bottom c-row-item">
					<div class="row">
						<div class="col-md-3 col-sm-12 c-image">
							<div class="c-content-overlay">
								<div class="c-overlay-wrapper">
									<div class="c-overlay-content">
										<a href="shop-product-details.html" class="btn btn-md c-btn-grey-1 c-btn-uppercase c-btn-bold c-btn-border-1x c-btn-square">Explore</a>
									</div>
								</div>
								<div class="c-bg-img-top-center c-overlay-object" data-height="height">
									<img width="100%" class="img-responsive" src="../../assets/base/img/content/shop2/12.jpg">
								</div>
							</div>
						</div>
						<div class="col-md-5 col-sm-8">
							<ul class="c-list list-unstyled">
								<li class="c-margin-b-25"><a href="shop-product-details-2.html" class="c-font-bold c-font-22 c-theme-link">Sports Wear</a></li>
								<li class="c-margin-b-10">Color: Blue</li>
								<li>Size: S</li>
								<li>Quantity: x1</li>
							</ul>
						</div>
						<div class="col-md-2 col-sm-2">
							<p class="visible-xs-block c-theme-font c-font-uppercase c-font-bold">Unit Price</p>
							<p class="c-font-sbold c-font-uppercase c-font-18">$15.00</p>
						</div>
						<div class="col-md-2 col-sm-2">
							<p class="visible-xs-block c-theme-font c-font-uppercase c-font-bold">Total</p>
							<p class="c-font-sbold c-font-18">$30.00</p>
						</div>
					</div>
				</div>
				<!-- END: PRODUCT ITEM ROW -->
				<div class="c-row-item c-row-total c-right">
					<ul class="c-list list-unstyled">
						<li>
							<h3 class="c-font-regular c-font-22">Subtotal : &nbsp;
								<span class="c-font-dark c-font-bold c-font-22">$80.00</span>
							</h3>
						</li>
						<li>
							<h3 class="c-font-regular c-font-22">Shipping Fee : &nbsp;
								<span class="c-font-dark c-font-bold c-font-22">$15.00</span>
							</h3>
						</li>
						<li>
							<h3 class="c-font-regular c-font-22">Grand Total : &nbsp;
								<span class="c-font-dark c-font-bold c-font-22">$95.00</span>
							</h3>
						</li>
					</ul>
				</div>
			</div>
			<!-- END: ORDER DETAILS -->
			<!-- BEGIN: CUSTOMER DETAILS -->
			<div class="c-customer-details row" data-auto-height="true">
				<div class="col-md-6 col-sm-6 c-margin-t-20">
					<div data-height="height">
						<h3 class=" c-margin-b-20 c-font-uppercase c-font-22 c-font-bold">Customer Details</h3>
						<ul class="list-unstyled">
							<li>Name: John Doe</li>
							<li>Phone: 800 123 3456</li>
							<li>Fax: 800 123 3456</li>
							<li>Email: <a href="mailto:info@jango.com" class="c-theme-color">info@jango.com</a></li>
							<li>Skype: <span class="c-theme-color">jango</span></li>
						</ul>
					</div>
				</div>
				<div class="col-md-6 col-sm-6 c-margin-t-20">
					<div data-height="height">
						<h3 class=" c-margin-b-20 c-font-uppercase c-font-22 c-font-bold">Billing Address</h3>
						<ul class="list-unstyled">
							<li>John Doe</li>
							<li>
								25, Lorem Lis Street, Orange <br />
								California, US <br />
							</li>
						</ul>
					</div>
				</div>
			</div>
			<!-- END: CUSTOMER DETAILS -->
		</div>
	</div>
</div>
		<!-- END: PAGE CONTENT -->
	</div>
	<!-- END: PAGE CONTAINER -->

	<!-- BEGIN: LAYOUT/FOOTERS/FOOTER-6 -->

</body>
