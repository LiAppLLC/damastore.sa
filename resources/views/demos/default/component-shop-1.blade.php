@extends('Welcome.home')


@section('content')

    <?php
    $locale = App::getLocale();
    ?>



    <body class="c-layout-header-fixed c-layout-header-mobile-fixed">


<div class="modal fade c-content-login-form" id="forget-password-form" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content c-square">
            <div class="modal-header c-no-border">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            </div>
            <div class="modal-body">
                <h3 class="c-font-24 c-font-sbold">Password Recovery</h3>
                <p>To recover your password please fill in your email address</p>
                <form>
                    <div class="form-group">
                        <label for="forget-email" class="hide">Email</label>
                        <input type="email" class="form-control input-lg c-square" id="forget-email" placeholder="Email">
                    </div>
                    <div class="form-group">
                        <button type="submit" class="btn c-theme-btn btn-md c-btn-uppercase c-btn-bold c-btn-square c-btn-login">Submit</button>
                        <a href="javascript:;" class="c-btn-forgot" data-toggle="modal" data-target="#login-form" data-dismiss="modal">Back To Login</a>
                    </div>
                </form>
            </div>
            <div class="modal-footer c-no-border">
                <span class="c-text-account">Don't Have An Account Yet ?</span>
                <a href="javascript:;" data-toggle="modal" data-target="#signup-form" data-dismiss="modal" class="btn c-btn-dark-1 btn c-btn-uppercase c-btn-bold c-btn-slim c-btn-border-2x c-btn-square c-btn-signup">Signup!</a>
            </div>
        </div>
    </div>
</div><!-- END: CONTENT/USER/FORGET-PASSWORD-FORM -->
	<!-- BEGIN: CONTENT/USER/SIGNUP-FORM -->
<div class="modal fade c-content-login-form" id="signup-form" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content c-square">
            <div class="modal-header c-no-border">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            </div>
            <div class="modal-body">
                <h3 class="c-font-24 c-font-sbold">Create An Account</h3>
                <p>Please fill in below form to create an account with us</p>
                <form>
                    <div class="form-group">
                        <label for="signup-email" class="hide">Email</label>
                        <input type="email" class="form-control input-lg c-square" id="signup-email" placeholder="Email">
                    </div>
                    <div class="form-group">
                        <label for="signup-username" class="hide">Username</label>
                        <input type="email" class="form-control input-lg c-square" id="signup-username" placeholder="Username">
                    </div>
                    <div class="form-group">
                        <label for="signup-fullname" class="hide">Fullname</label>
                        <input type="email" class="form-control input-lg c-square" id="signup-fullname" placeholder="Fullname">
                    </div>
                    <div class="form-group">
                        <label for="signup-country" class="hide">Country</label>
                        <select class="form-control input-lg c-square" id="signup-country">
                            <option value="1">Country</option>
                        </select>
                    </div>
                    <div class="form-group">
                        <button type="submit" class="btn c-theme-btn btn-md c-btn-uppercase c-btn-bold c-btn-square c-btn-login">Signup</button>
                        <a href="javascript:;" class="c-btn-forgot" data-toggle="modal" data-target="#login-form" data-dismiss="modal">Back To Login</a>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div><!-- END: CONTENT/USER/SIGNUP-FORM -->
	<!-- BEGIN: CONTENT/USER/LOGIN-FORM -->
<div class="modal fade c-content-login-form" id="login-form" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content c-square">
            <div class="modal-header c-no-border">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            </div>
            <div class="modal-body">
                <h3 class="c-font-24 c-font-sbold">Good Afternoon!</h3>
                <p>Let's make today a great day!</p>
                <form>
                    <div class="form-group">
                        <label for="login-email" class="hide">Email</label>
                        <input type="email" class="form-control input-lg c-square" id="login-email" placeholder="Email">
                    </div>
                    <div class="form-group">
                        <label for="login-password" class="hide">Password</label>
                        <input type="password" class="form-control input-lg c-square" id="login-password" placeholder="Password">
                    </div>
                    <div class="form-group">
                        <div class="c-checkbox">
                            <input type="checkbox" id="login-rememberme" class="c-check">
                            <label for="login-rememberme" class="c-font-thin c-font-17">
                                <span></span>
                                <span class="check"></span>
                                <span class="box"></span>
                                Remember Me
                            </label>
                        </div>
                    </div>
                    <div class="form-group">
                        <button type="submit" class="btn c-theme-btn btn-md c-btn-uppercase c-btn-bold c-btn-square c-btn-login">Login</button>
                        <a href="javascript:;" data-toggle="modal" data-target="#forget-password-form" data-dismiss="modal" class="c-btn-forgot">Forgot Your Password ?</a>
                    </div>
                    <div class="clearfix">
                        <div class="c-content-divider c-divider-sm c-icon-bg c-bg-grey c-margin-b-20">
                            <span>or signup with</span>
                        </div>
                        <ul class="c-content-list-adjusted">
                            <li>
                                <a class="btn btn-block c-btn-square btn-social btn-twitter">
                                  <i class="fa fa-twitter"></i>
                                  Twitter
                                </a>
                            </li>
                            <li>
                                <a class="btn btn-block c-btn-square btn-social btn-facebook">
                                  <i class="fa fa-facebook"></i>
                                  Facebook
                                </a>
                            </li>
                            <li>
                                <a class="btn btn-block c-btn-square btn-social btn-google">
                                  <i class="fa fa-google"></i>
                                  Google
                                </a>
                            </li>
                        </ul>
                    </div>
                </form>
            </div>
            <div class="modal-footer c-no-border">
                <span class="c-text-account">Don't Have An Account Yet ?</span>
                <a href="javascript:;" data-toggle="modal" data-target="#signup-form" data-dismiss="modal" class="btn c-btn-dark-1 btn c-btn-uppercase c-btn-bold c-btn-slim c-btn-border-2x c-btn-square c-btn-signup">Signup!</a>
            </div>
        </div>
    </div>
</div><!-- END: CONTENT/USER/LOGIN-FORM -->

	<!-- BEGIN: LAYOUT/SIDEBARS/QUICK-SIDEBAR -->
<nav class="c-layout-quick-sidebar">
	<div class="c-header">
		<button type="button" class="c-link c-close">
		<i class="icon-login"></i>
		</button>
	</div>
	<div class="c-content">
		<div class="c-section">
			<h3>JANGO DEMOS</h3>
			<div class="c-settings c-demos c-bs-grid-reset-space">
				<div class="row">
					<div class="col-md-12">
						<a href="../default/index.html" class="c-demo-container c-demo-img-lg">
							<div class="c-demo-thumb active">
								<img src="../../assets/base/img/content/quick-sidebar/default.jpg" class="c-demo-thumb-img"/>
							</div>
						</a>
					</div>
				</div>
				<div class="row">
					<div class="col-md-6">
						<a href="../corporate_1/index.html" class="c-demo-container">
							<div class="c-demo-thumb  c-thumb-left">
								<img src="../../assets/base/img/content/quick-sidebar/corporate_1.jpg" class="c-demo-thumb-img"/>
							</div>
						</a>
					</div>
					<div class="col-md-6">
						<a href="../agency_1/index.html" class="c-demo-container">
							<div class="c-demo-thumb  c-thumb-right">
								<img src="../../assets/base/img/content/quick-sidebar/corporate_1-onepage.jpg" class="c-demo-thumb-img"/>
							</div>
						</a>
					</div>
				</div>
			</div>
		</div>
		<div class="c-section">
			<h3>Theme Colors</h3>
			<div class="c-settings">

				<span class="c-color c-default c-active" data-color="default"></span>

				<span class="c-color c-green1" data-color="green1"></span>
				<span class="c-color c-green2" data-color="green2"></span>
				<span class="c-color c-green3" data-color="green3"></span>

				<span class="c-color c-yellow1" data-color="yellow1"></span>
				<span class="c-color c-yellow2" data-color="yellow2"></span>
				<span class="c-color c-yellow3" data-color="yellow3"></span>

				<span class="c-color c-red1" data-color="red1"></span>
				<span class="c-color c-red2" data-color="red2"></span>
				<span class="c-color c-red3" data-color="red3"></span>

				<span class="c-color c-purple1" data-color="purple1"></span>
				<span class="c-color c-purple2" data-color="purple2"></span>
				<span class="c-color c-purple3" data-color="purple3"></span>

				<span class="c-color c-blue1" data-color="blue1"></span>
				<span class="c-color c-blue2" data-color="blue2"></span>
				<span class="c-color c-blue3" data-color="blue3"></span>

				<span class="c-color c-brown1" data-color="brown1"></span>
				<span class="c-color c-brown2" data-color="brown2"></span>
				<span class="c-color c-brown3" data-color="brown3"></span>

				<span class="c-color c-dark1" data-color="dark1"></span>
				<span class="c-color c-dark2" data-color="dark2"></span>
				<span class="c-color c-dark3" data-color="dark3"></span>
			</div>
		</div>
		<div class="c-section">
			<h3>Header Type</h3>
			<div class="c-settings">
				<input type="button" class="c-setting_header-type btn btn-sm c-btn-square c-btn-border-1x c-btn-white c-btn-sbold c-btn-uppercase active" data-value="boxed" value="boxed"/>
				<input type="button" class="c-setting_header-type btn btn-sm c-btn-square c-btn-border-1x c-btn-white c-btn-sbold c-btn-uppercase" data-value="fluid" value="fluid"/>
			</div>
		</div>
		<div class="c-section">
			<h3>Header Mode</h3>
			<div class="c-settings">
				<input type="button" class="c-setting_header-mode btn btn-sm c-btn-square c-btn-border-1x c-btn-white c-btn-sbold c-btn-uppercase active" data-value="fixed" value="fixed"/>
				<input type="button" class="c-setting_header-mode btn btn-sm c-btn-square c-btn-border-1x c-btn-white c-btn-sbold c-btn-uppercase" data-value="static" value="static"/>
			</div>
		</div>
		<div class="c-section">
			<h3>Mega Menu Style</h3>
			<div class="c-settings">
				<input type="button" class="c-setting_megamenu-style btn btn-sm c-btn-square c-btn-border-1x c-btn-white c-btn-sbold c-btn-uppercase active" data-value="dark" value="dark"/>
				<input type="button" class="c-setting_megamenu-style btn btn-sm c-btn-square c-btn-border-1x c-btn-white c-btn-sbold c-btn-uppercase" data-value="light" value="light"/>
			</div>
		</div>
		<div class="c-section">
			<h3>Font Style</h3>
			<div class="c-settings">
				<input type="button" class="c-setting_font-style btn btn-sm c-btn-square c-btn-border-1x c-btn-white c-btn-sbold c-btn-uppercase active" data-value="default" value="default"/>
				<input type="button" class="c-setting_font-style btn btn-sm c-btn-square c-btn-border-1x c-btn-white c-btn-sbold c-btn-uppercase" data-value="light" value="light"/>
			</div>
		</div>
		<div class="c-section">
			<h3>Reading Style</h3>
			<div class="c-settings">
				<a href="http://www.themehats.com/themes/jango/" class="c-setting_font-style btn btn-sm c-btn-square c-btn-border-1x c-btn-white c-btn-sbold c-btn-uppercase ">LTR</a>
				<a href="http://www.themehats.com/themes/jango/rtl/" class="c-setting_font-style btn btn-sm c-btn-square c-btn-border-1x c-btn-white c-btn-sbold c-btn-uppercase active">RTL</a>
			</div>
		</div>
	</div>
</nav><!-- END: LAYOUT/SIDEBARS/QUICK-SIDEBAR -->

	<!-- BEGIN: PAGE CONTAINER -->
	<div class="c-layout-page">
		<!-- BEGIN: LAYOUT/BREADCRUMBS/BREADCRUMBS-1 -->

<div class="c-layout-breadcrumbs-1 c-fonts-uppercase c-fonts-bold">
	<div class="container">
		<div class="c-page-title c-pull-left">
			<h3 class="c-font-uppercase c-font-sbold">Shop Component 1</h3>
		</div>
		<ul class="c-page-breadcrumbs c-theme-nav c-pull-right c-fonts-regular">
															<li><a href="component-shop-1.html">Shop Components 1</a></li>
			<li>/</li>
															<li class="c-state_active">Jango Components</li>

		</ul>
	</div>
</div><!-- END: LAYOUT/BREADCRUMBS/BREADCRUMBS-1 -->
		<!-- BEGIN: PAGE CONTENT -->
		<!-- BEGIN: CONTENT/SHOPS/SHOP-1-1 -->
<div class="c-content-box c-size-md">
	<div class="container">
		<div class="c-content-tab-5 c-bs-grid-reset-space c-theme">
			<!-- Nav tabs -->
			<ul class="nav nav-pills c-nav-tab c-arrow" role="tablist">
				<li role="presentation" class="active">
					<a class="c-font-uppercase" href="#watches" aria-controls="watches" role="tab" data-toggle="tab">Watches</a>
				</li>
				<li role="presentation">
					<a class="c-font-uppercase" href="#phone" aria-controls="phone" role="tab" data-toggle="tab">Phone</a>
				</li>
				<li role="presentation">
					<a class="c-font-uppercase" href="#imac" aria-controls="imac" role="tab" data-toggle="tab">iMac</a>
				</li>
				<li role="presentation">
					<a class="c-font-uppercase" href="#accessories" aria-controls="accessories" role="tab" data-toggle="tab">Accessories</a>
				</li>
			</ul>
			<!-- Tab panes -->
			<div class="tab-content">
				<div role="tabpanel" class="tab-pane fade in active" id="watches">
					<div class="row">
						<div class="col-sm-4">
							<div class="c-content c-content-overlay">
								<div class="c-overlay-wrapper c-overlay-padding">
									<div class="c-overlay-content">
										<a href="shop-product-details-2.html" class="btn btn-md c-btn-grey-1 c-btn-uppercase c-btn-bold c-btn-border-1x c-btn-square">Explore</a>
									</div>
								</div>
								<div class="c-bg-img-center c-overlay-object" data-height="height" style="height: 270px; background-image: url(../../assets/base/img/content/shop2/69.jpg);"></div>
								<div class="c-overlay-border"></div>
							</div>
						</div>
						<div class="col-sm-4">
							<div class="c-content c-content-overlay">
								<div class="c-overlay-wrapper c-overlay-padding">
									<div class="c-overlay-content">
										<a href="shop-product-details-2.html" class="btn btn-md c-btn-grey-1 c-btn-uppercase c-btn-bold c-btn-border-1x c-btn-square">Explore</a>
									</div>
								</div>
								<div class="c-bg-img-center c-overlay-object" data-height="height" style="height: 270px; background-image: url(../../assets/base/img/content/shop2/70.jpg);"></div>
								<div class="c-overlay-border"></div>
							</div>
						</div>
						<div class="col-sm-4">
							<div class="c-content c-content-overlay">
								<div class="c-overlay-wrapper c-overlay-padding">
									<div class="c-overlay-content">
										<a href="shop-product-details-2.html" class="btn btn-md c-btn-grey-1 c-btn-uppercase c-btn-bold c-btn-border-1x c-btn-square">Explore</a>
									</div>
								</div>
								<div class="c-bg-img-center c-overlay-object" data-height="height" style="height: 270px; background-image: url(../../assets/base/img/content/shop2/79.jpg);"></div>
								<div class="c-overlay-border"></div>
							</div>
						</div>
					</div>
				</div>

				<div role="tabpanel" class="tab-pane fade" id="phone">
					<div class="row">
						<div class="col-sm-4">
							<div class="c-content c-content-overlay">
								<div class="c-overlay-wrapper c-overlay-padding">
									<div class="c-overlay-content">
										<a href="shop-product-details-2.html" class="btn btn-md c-btn-grey-1 c-btn-uppercase c-btn-bold c-btn-border-1x c-btn-square">Explore</a>
									</div>
								</div>
								<div class="c-bg-img-center c-overlay-object" data-height="height" style="height: 270px; background-image: url(../../assets/base/img/content/shop2/63.jpg);"></div>
								<div class="c-overlay-border"></div>
							</div>
						</div>
						<div class="col-sm-4">
							<div class="c-content c-content-overlay">
								<div class="c-overlay-wrapper c-overlay-padding">
									<div class="c-overlay-content">
										<a href="shop-product-details-2.html" class="btn btn-md c-btn-grey-1 c-btn-uppercase c-btn-bold c-btn-border-1x c-btn-square">Explore</a>
									</div>
								</div>
								<div class="c-bg-img-center c-overlay-object" data-height="height" style="height: 270px; background-image: url(../../assets/base/img/content/shop2/59.jpg);"></div>
								<div class="c-overlay-border"></div>
							</div>
						</div>
						<div class="col-sm-4">
							<div class="c-content c-content-overlay">
								<div class="c-overlay-wrapper c-overlay-padding">
									<div class="c-overlay-content">
										<a href="shop-product-details-2.html" class="btn btn-md c-btn-grey-1 c-btn-uppercase c-btn-bold c-btn-border-1x c-btn-square">Explore</a>
									</div>
								</div>
								<div class="c-bg-img-center c-overlay-object" data-height="height" style="height: 270px; background-image: url(../../assets/base/img/content/shop2/71.jpg);"></div>
								<div class="c-overlay-border"></div>
							</div>
						</div>
					</div>
				</div>

				<div role="tabpanel" class="tab-pane fade" id="imac">
					<div class="row">
						<div class="col-sm-4">
							<div class="c-content c-content-overlay">
								<div class="c-overlay-wrapper c-overlay-padding">
									<div class="c-overlay-content">
										<a href="shop-product-details-2.html" class="btn btn-md c-btn-grey-1 c-btn-uppercase c-btn-bold c-btn-border-1x c-btn-square">Explore</a>
									</div>
								</div>
								<div class="c-bg-img-center c-overlay-object" data-height="height" style="height: 270px; background-image: url(../../assets/base/img/content/shop3/73.jpg);"></div>
								<div class="c-overlay-border"></div>
							</div>
						</div>
						<div class="col-sm-4">
							<div class="c-content c-content-overlay">
								<div class="c-overlay-wrapper c-overlay-padding">
									<div class="c-overlay-content">
										<a href="shop-product-details-2.html" class="btn btn-md c-btn-grey-1 c-btn-uppercase c-btn-bold c-btn-border-1x c-btn-square">Explore</a>
									</div>
								</div>
								<div class="c-bg-img-center c-overlay-object" data-height="height" style="height: 270px; background-image: url(../../assets/base/img/content/shop3/74.jpg);"></div>
								<div class="c-overlay-border"></div>
							</div>
						</div>
						<div class="col-sm-4">
							<div class="c-content c-content-overlay">
								<div class="c-overlay-wrapper c-overlay-padding">
									<div class="c-overlay-content">
										<a href="shop-product-details-2.html" class="btn btn-md c-btn-grey-1 c-btn-uppercase c-btn-bold c-btn-border-1x c-btn-square">Explore</a>
									</div>
								</div>
								<div class="c-bg-img-center c-overlay-object" data-height="height" style="height: 270px; background-image: url(../../assets/base/img/content/shop3/91.jpg);"></div>
								<div class="c-overlay-border"></div>
							</div>
						</div>
					</div>
				</div>

				<div role="tabpanel" class="tab-pane fade" id="accessories">
					<div class="row">
						<div class="col-sm-4">
							<div class="c-content c-content-overlay">
								<div class="c-overlay-wrapper c-overlay-padding">
									<div class="c-overlay-content">
										<a href="shop-product-details-2.html" class="btn btn-md c-btn-grey-1 c-btn-uppercase c-btn-bold c-btn-border-1x c-btn-square">Explore</a>
									</div>
								</div>
								<div class="c-bg-img-center c-overlay-object" data-height="height" style="height: 270px; background-image: url(../../assets/base/img/content/shop3/63.jpg);"></div>
								<div class="c-overlay-border"></div>
							</div>
						</div>
						<div class="col-sm-4">
							<div class="c-content c-content-overlay">
								<div class="c-overlay-wrapper c-overlay-padding">
									<div class="c-overlay-content">
										<a href="shop-product-details-2.html" class="btn btn-md c-btn-grey-1 c-btn-uppercase c-btn-bold c-btn-border-1x c-btn-square">Explore</a>
									</div>
								</div>
								<div class="c-bg-img-center c-overlay-object" data-height="height" style="height: 270px; background-image: url(../../assets/base/img/content/shop3/67.jpg);"></div>
								<div class="c-overlay-border"></div>
							</div>
						</div>
						<div class="col-sm-4">
							<div class="c-content c-content-overlay">
								<div class="c-overlay-wrapper c-overlay-padding">
									<div class="c-overlay-content">
										<a href="shop-product-details-2.html" class="btn btn-md c-btn-grey-1 c-btn-uppercase c-btn-bold c-btn-border-1x c-btn-square">Explore</a>
									</div>
								</div>
								<div class="c-bg-img-center c-overlay-object" data-height="height" style="height: 270px; background-image: url(../../assets/base/img/content/shop3/77.jpg);"></div>
								<div class="c-overlay-border"></div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div><!-- END: CONTENT/SHOPS/SHOP-1-1 -->

<!-- BEGIN: CONTENT/SHOPS/SHOP-1-4 -->
<div class="c-content-box c-size-md c-bg-parallax" style="background-image: url(../../assets/base/img/content/shop-backgrounds/127.jpg)">
	<div class="container">
		<div class="c-content-tab-5 c-bs-grid-reset-space">
			<!-- Nav tabs -->
			<ul class="nav nav-pills c-nav-tab c-arrow" role="tablist">
				<li role="presentation" class="active">
					<a class="c-font-uppercase" href="#watches4" aria-controls="watches" role="tab" data-toggle="tab">Watches</a>
				</li>
				<li role="presentation">
					<a class="c-font-uppercase" href="#phone4" aria-controls="phone" role="tab" data-toggle="tab">Phone</a>
				</li>
				<li role="presentation">
					<a class="c-font-uppercase" href="#imac4" aria-controls="imac" role="tab" data-toggle="tab">iMac</a>
				</li>
				<li role="presentation">
					<a class="c-font-uppercase" href="#accessories4" aria-controls="accessories" role="tab" data-toggle="tab">Accessories</a>
				</li>
			</ul>
			<!-- Tab panes -->
			<div class="tab-content">
				<div role="tabpanel" class="tab-pane fade in active" id="watches4">
					<div class="row">
						<div class="col-sm-4">
							<div class="c-content c-content-overlay">
								<div class="c-overlay-wrapper c-overlay-padding">
									<div class="c-overlay-content">
										<a href="shop-product-details.html" class="btn btn-md c-btn-grey-1 c-btn-uppercase c-btn-bold c-btn-border-1x c-btn-square">Explore</a>
									</div>
								</div>
								<div class="c-bg-img-center c-overlay-object" data-height="height" style="height: 270px; background-image: url(../../assets/base/img/content/shop2/69.jpg);"></div>
								<div class="c-overlay-border"></div>
							</div>
						</div>
						<div class="col-sm-4">
							<div class="c-content c-content-overlay">
								<div class="c-overlay-wrapper c-overlay-padding">
									<div class="c-overlay-content">
										<a href="shop-product-details.html" class="btn btn-md c-btn-grey-1 c-btn-uppercase c-btn-bold c-btn-border-1x c-btn-square">Explore</a>
									</div>
								</div>
								<div class="c-bg-img-center c-overlay-object" data-height="height" style="height: 270px; background-image: url(../../assets/base/img/content/shop2/70.jpg);"></div>
								<div class="c-overlay-border"></div>
							</div>
						</div>
						<div class="col-sm-4">
							<div class="c-content c-content-overlay">
								<div class="c-overlay-wrapper c-overlay-padding">
									<div class="c-overlay-content">
										<a href="shop-product-details.html" class="btn btn-md c-btn-grey-1 c-btn-uppercase c-btn-bold c-btn-border-1x c-btn-square">Explore</a>
									</div>
								</div>
								<div class="c-bg-img-center c-overlay-object" data-height="height" style="height: 270px; background-image: url(../../assets/base/img/content/shop2/79.jpg);"></div>
								<div class="c-overlay-border"></div>
							</div>
						</div>
					</div>
				</div>

				<div role="tabpanel" class="tab-pane fade" id="phone4">
					<div class="row">
						<div class="col-sm-4">
							<div class="c-content c-content-overlay">
								<div class="c-overlay-wrapper c-overlay-padding">
									<div class="c-overlay-content">
										<a href="shop-product-details.html" class="btn btn-md c-btn-grey-1 c-btn-uppercase c-btn-bold c-btn-border-1x c-btn-square">Explore</a>
									</div>
								</div>
								<div class="c-bg-img-center c-overlay-object" data-height="height" style="height: 270px; background-image: url(../../assets/base/img/content/shop2/63.jpg);"></div>
								<div class="c-overlay-border"></div>
							</div>
						</div>
						<div class="col-sm-4">
							<div class="c-content c-content-overlay">
								<div class="c-overlay-wrapper c-overlay-padding">
									<div class="c-overlay-content">
										<a href="shop-product-details.html" class="btn btn-md c-btn-grey-1 c-btn-uppercase c-btn-bold c-btn-border-1x c-btn-square">Explore</a>
									</div>
								</div>
								<div class="c-bg-img-center c-overlay-object" data-height="height" style="height: 270px; background-image: url(../../assets/base/img/content/shop2/59.jpg);"></div>
								<div class="c-overlay-border"></div>
							</div>
						</div>
						<div class="col-sm-4">
							<div class="c-content c-content-overlay">
								<div class="c-overlay-wrapper c-overlay-padding">
									<div class="c-overlay-content">
										<a href="shop-product-details.html" class="btn btn-md c-btn-grey-1 c-btn-uppercase c-btn-bold c-btn-border-1x c-btn-square">Explore</a>
									</div>
								</div>
								<div class="c-bg-img-center c-overlay-object" data-height="height" style="height: 270px; background-image: url(../../assets/base/img/content/shop2/71.jpg);"></div>
								<div class="c-overlay-border"></div>
							</div>
						</div>
					</div>
				</div>

				<div role="tabpanel" class="tab-pane fade" id="imac4">
					<div class="row">
						<div class="col-sm-4">
							<div class="c-content c-content-overlay">
								<div class="c-overlay-wrapper c-overlay-padding">
									<div class="c-overlay-content">
										<a href="shop-product-details.html" class="btn btn-md c-btn-grey-1 c-btn-uppercase c-btn-bold c-btn-border-1x c-btn-square">Explore</a>
									</div>
								</div>
								<div class="c-bg-img-center c-overlay-object" data-height="height" style="height: 270px; background-image: url(../../assets/base/img/content/shop3/73.jpg);"></div>
								<div class="c-overlay-border"></div>
							</div>
						</div>
						<div class="col-sm-4">
							<div class="c-content c-content-overlay">
								<div class="c-overlay-wrapper c-overlay-padding">
									<div class="c-overlay-content">
										<a href="shop-product-details.html" class="btn btn-md c-btn-grey-1 c-btn-uppercase c-btn-bold c-btn-border-1x c-btn-square">Explore</a>
									</div>
								</div>
								<div class="c-bg-img-center c-overlay-object" data-height="height" style="height: 270px; background-image: url(../../assets/base/img/content/shop3/74.jpg);"></div>
								<div class="c-overlay-border"></div>
							</div>
						</div>
						<div class="col-sm-4">
							<div class="c-content c-content-overlay">
								<div class="c-overlay-wrapper c-overlay-padding">
									<div class="c-overlay-content">
										<a href="shop-product-details.html" class="btn btn-md c-btn-grey-1 c-btn-uppercase c-btn-bold c-btn-border-1x c-btn-square">Explore</a>
									</div>
								</div>
								<div class="c-bg-img-center c-overlay-object" data-height="height" style="height: 270px; background-image: url(../../assets/base/img/content/shop3/91.jpg);"></div>
								<div class="c-overlay-border"></div>
							</div>
						</div>
					</div>
				</div>

				<div role="tabpanel" class="tab-pane fade" id="accessories4">
					<div class="row">
						<div class="col-sm-4">
							<div class="c-content c-content-overlay">
								<div class="c-overlay-wrapper c-overlay-padding">
									<div class="c-overlay-content">
										<a href="shop-product-details.html" class="btn btn-md c-btn-grey-1 c-btn-uppercase c-btn-bold c-btn-border-1x c-btn-square">Explore</a>
									</div>
								</div>
								<div class="c-bg-img-center c-overlay-object" data-height="height" style="height: 270px; background-image: url(../../assets/base/img/content/shop3/63.jpg);"></div>
								<div class="c-overlay-border"></div>
							</div>
						</div>
						<div class="col-sm-4">
							<div class="c-content c-content-overlay">
								<div class="c-overlay-wrapper c-overlay-padding">
									<div class="c-overlay-content">
										<a href="shop-product-details.html" class="btn btn-md c-btn-grey-1 c-btn-uppercase c-btn-bold c-btn-border-1x c-btn-square">Explore</a>
									</div>
								</div>
								<div class="c-bg-img-center c-overlay-object" data-height="height" style="height: 270px; background-image: url(../../assets/base/img/content/shop3/67.jpg);"></div>
								<div class="c-overlay-border"></div>
							</div>
						</div>
						<div class="col-sm-4">
							<div class="c-content c-content-overlay">
								<div class="c-overlay-wrapper c-overlay-padding">
									<div class="c-overlay-content">
										<a href="shop-product-details.html" class="btn btn-md c-btn-grey-1 c-btn-uppercase c-btn-bold c-btn-border-1x c-btn-square">Explore</a>
									</div>
								</div>
								<div class="c-bg-img-center c-overlay-object" data-height="height" style="height: 270px; background-image: url(../../assets/base/img/content/shop3/77.jpg);"></div>
								<div class="c-overlay-border"></div>
							</div>
						</div>
					</div>
				</div>

			</div>
		</div>
	</div>
</div><!-- END: CONTENT/SHOPS/SHOP-1-4 -->

<!-- BEGIN: CONTENT/SHOPS/SHOP-1-7 -->
<div class="c-content-box c-size-md" style="background-image: url(../../assets/base/img/content/shop-backgrounds/119.jpg)">
	<div class="container">
		<div class="c-content-tab-5 c-bs-grid-reset-space">
			<!-- Nav tabs -->
			<ul class="nav nav-pills c-nav-tab c-arrow" role="tablist">
				<li role="presentation" class="active">
					<a class="c-font-uppercase" href="#watches7" aria-controls="watches" role="tab" data-toggle="tab">Watches</a>
				</li>
				<li role="presentation">
					<a class="c-font-uppercase" href="#phone7" aria-controls="phone" role="tab" data-toggle="tab">Phone</a>
				</li>
				<li role="presentation">
					<a class="c-font-uppercase" href="#imac7" aria-controls="imac" role="tab" data-toggle="tab">iMac</a>
				</li>
				<li role="presentation">
					<a class="c-font-uppercase" href="#accessories7" aria-controls="accessories" role="tab" data-toggle="tab">Accessories</a>
				</li>
			</ul>
			<!-- Tab panes -->
			<div class="tab-content">
				<div role="tabpanel" class="tab-pane fade in active" id="watches7">
					<div class="row">
						<div class="col-sm-4">
							<div class="c-content c-content-overlay">
								<div class="c-overlay-wrapper c-overlay-padding">
									<div class="c-overlay-content">
										<a href="shop-product-details-2.html" class="btn btn-md c-btn-grey-1 c-btn-uppercase c-btn-bold c-btn-border-1x c-btn-square">Explore</a>
									</div>
								</div>
								<div class="c-bg-img-center c-overlay-object" data-height="height" style="height: 270px; background-image: url(../../assets/base/img/content/shop2/69.jpg);"></div>
								<div class="c-overlay-border"></div>
							</div>
						</div>
						<div class="col-sm-4">
							<div class="c-content c-content-overlay">
								<div class="c-overlay-wrapper c-overlay-padding">
									<div class="c-overlay-content">
										<a href="shop-product-details-2.html" class="btn btn-md c-btn-grey-1 c-btn-uppercase c-btn-bold c-btn-border-1x c-btn-square">Explore</a>
									</div>
								</div>
								<div class="c-bg-img-center c-overlay-object" data-height="height" style="height: 270px; background-image: url(../../assets/base/img/content/shop2/70.jpg);"></div>
								<div class="c-overlay-border"></div>
							</div>
						</div>
						<div class="col-sm-4">
							<div class="c-content c-content-overlay">
								<div class="c-overlay-wrapper c-overlay-padding">
									<div class="c-overlay-content">
										<a href="shop-product-details-2.html" class="btn btn-md c-btn-grey-1 c-btn-uppercase c-btn-bold c-btn-border-1x c-btn-square">Explore</a>
									</div>
								</div>
								<div class="c-bg-img-center c-overlay-object" data-height="height" style="height: 270px; background-image: url(../../assets/base/img/content/shop2/79.jpg);"></div>
								<div class="c-overlay-border"></div>
							</div>
						</div>
					</div>
				</div>

				<div role="tabpanel" class="tab-pane fade" id="phone7">
					<div class="row">
						<div class="col-sm-4">
							<div class="c-content c-content-overlay">
								<div class="c-overlay-wrapper c-overlay-padding">
									<div class="c-overlay-content">
										<a href="shop-product-details-2.html" class="btn btn-md c-btn-grey-1 c-btn-uppercase c-btn-bold c-btn-border-1x c-btn-square">Explore</a>
									</div>
								</div>
								<div class="c-bg-img-center c-overlay-object" data-height="height" style="height: 270px; background-image: url(../../assets/base/img/content/shop2/63.jpg);"></div>
								<div class="c-overlay-border"></div>
							</div>
						</div>
						<div class="col-sm-4">
							<div class="c-content c-content-overlay">
								<div class="c-overlay-wrapper c-overlay-padding">
									<div class="c-overlay-content">
										<a href="shop-product-details-2.html" class="btn btn-md c-btn-grey-1 c-btn-uppercase c-btn-bold c-btn-border-1x c-btn-square">Explore</a>
									</div>
								</div>
								<div class="c-bg-img-center c-overlay-object" data-height="height" style="height: 270px; background-image: url(../../assets/base/img/content/shop2/59.jpg);"></div>
								<div class="c-overlay-border"></div>
							</div>
						</div>
						<div class="col-sm-4">
							<div class="c-content c-content-overlay">
								<div class="c-overlay-wrapper c-overlay-padding">
									<div class="c-overlay-content">
										<a href="shop-product-details-2.html" class="btn btn-md c-btn-grey-1 c-btn-uppercase c-btn-bold c-btn-border-1x c-btn-square">Explore</a>
									</div>
								</div>
								<div class="c-bg-img-center c-overlay-object" data-height="height" style="height: 270px; background-image: url(../../assets/base/img/content/shop2/71.jpg);"></div>
								<div class="c-overlay-border"></div>
							</div>
						</div>
					</div>
				</div>

				<div role="tabpanel" class="tab-pane fade" id="imac7">
					<div class="row">
						<div class="col-sm-4">
							<div class="c-content c-content-overlay">
								<div class="c-overlay-wrapper c-overlay-padding">
									<div class="c-overlay-content">
										<a href="shop-product-details-2.html" class="btn btn-md c-btn-grey-1 c-btn-uppercase c-btn-bold c-btn-border-1x c-btn-square">Explore</a>
									</div>
								</div>
								<div class="c-bg-img-center c-overlay-object" data-height="height" style="height: 270px; background-image: url(../../assets/base/img/content/shop3/73.jpg);"></div>
								<div class="c-overlay-border"></div>
							</div>
						</div>
						<div class="col-sm-4">
							<div class="c-content c-content-overlay">
								<div class="c-overlay-wrapper c-overlay-padding">
									<div class="c-overlay-content">
										<a href="shop-product-details-2.html" class="btn btn-md c-btn-grey-1 c-btn-uppercase c-btn-bold c-btn-border-1x c-btn-square">Explore</a>
									</div>
								</div>
								<div class="c-bg-img-center c-overlay-object" data-height="height" style="height: 270px; background-image: url(../../assets/base/img/content/shop3/74.jpg);"></div>
								<div class="c-overlay-border"></div>
							</div>
						</div>
						<div class="col-sm-4">
							<div class="c-content c-content-overlay">
								<div class="c-overlay-wrapper c-overlay-padding">
									<div class="c-overlay-content">
										<a href="shop-product-details-2.html" class="btn btn-md c-btn-grey-1 c-btn-uppercase c-btn-bold c-btn-border-1x c-btn-square">Explore</a>
									</div>
								</div>
								<div class="c-bg-img-center c-overlay-object" data-height="height" style="height: 270px; background-image: url(../../assets/base/img/content/shop3/91.jpg);"></div>
								<div class="c-overlay-border"></div>
							</div>
						</div>
					</div>
				</div>

				<div role="tabpanel" class="tab-pane fade" id="accessories7">
					<div class="row">
						<div class="col-sm-4">
							<div class="c-content c-content-overlay">
								<div class="c-overlay-wrapper c-overlay-padding">
									<div class="c-overlay-content">
										<a href="shop-product-details-2.html" class="btn btn-md c-btn-grey-1 c-btn-uppercase c-btn-bold c-btn-border-1x c-btn-square">Explore</a>
									</div>
								</div>
								<div class="c-bg-img-center c-overlay-object" data-height="height" style="height: 270px; background-image: url(../../assets/base/img/content/shop3/63.jpg);"></div>
								<div class="c-overlay-border"></div>
							</div>
						</div>
						<div class="col-sm-4">
							<div class="c-content c-content-overlay">
								<div class="c-overlay-wrapper c-overlay-padding">
									<div class="c-overlay-content">
										<a href="shop-product-details-2.html" class="btn btn-md c-btn-grey-1 c-btn-uppercase c-btn-bold c-btn-border-1x c-btn-square">Explore</a>
									</div>
								</div>
								<div class="c-bg-img-center c-overlay-object" data-height="height" style="height: 270px; background-image: url(../../assets/base/img/content/shop3/67.jpg);"></div>
								<div class="c-overlay-border"></div>
							</div>
						</div>
						<div class="col-sm-4">
							<div class="c-content c-content-overlay">
								<div class="c-overlay-wrapper c-overlay-padding">
									<div class="c-overlay-content">
										<a href="shop-product-details-2.html" class="btn btn-md c-btn-grey-1 c-btn-uppercase c-btn-bold c-btn-border-1x c-btn-square">Explore</a>
									</div>
								</div>
								<div class="c-bg-img-center c-overlay-object" data-height="height" style="height: 270px; background-image: url(../../assets/base/img/content/shop3/77.jpg);"></div>
								<div class="c-overlay-border"></div>
							</div>
						</div>
					</div>
				</div>

			</div>
		</div>
	</div>
</div><!-- END: CONTENT/SHOPS/SHOP-1-7 -->

<!-- BEGIN: CONTENT/SHOPS/SHOP-1-2 -->
<div class="c-content-box c-size-md">
	<div class="container">
		<div class="c-content-tab-5 c-theme">
			<!-- Nav tabs -->
			<ul class="nav nav-pills c-nav-tab c-arrow" role="tablist">
				<li role="presentation" class="active">
					<a class="c-font-uppercase" href="#watches2" aria-controls="watches" role="tab" data-toggle="tab">Watches</a>
				</li>
				<li role="presentation">
					<a class="c-font-uppercase" href="#phone2" aria-controls="phone" role="tab" data-toggle="tab">Phone</a>
				</li>
				<li role="presentation">
					<a class="c-font-uppercase" href="#imac2" aria-controls="imac" role="tab" data-toggle="tab">iMac</a>
				</li>
				<li role="presentation">
					<a class="c-font-uppercase" href="#accessories2" aria-controls="accessories" role="tab" data-toggle="tab">Accessories</a>
				</li>
			</ul>
			<!-- Tab panes -->
			<div class="tab-content">
				<div role="tabpanel" class="tab-pane fade in active" id="watches2">
					<div class="row">
						<div class="col-sm-4">
							<div class="c-content c-content-overlay">
								<div class="c-overlay-wrapper c-overlay-padding">
									<div class="c-overlay-content">
										<a href="shop-product-details.html" class="btn btn-md c-btn-grey-1 c-btn-uppercase c-btn-bold c-btn-border-1x c-btn-square">Explore</a>
									</div>
								</div>
								<div class="c-bg-img-center c-overlay-object" data-height="height" style="height: 270px; background-image: url(../../assets/base/img/content/shop2/69.jpg);"></div>
								<div class="c-overlay-border"></div>
							</div>
						</div>
						<div class="col-sm-4">
							<div class="c-content c-content-overlay">
								<div class="c-overlay-wrapper c-overlay-padding">
									<div class="c-overlay-content">
										<a href="shop-product-details.html" class="btn btn-md c-btn-grey-1 c-btn-uppercase c-btn-bold c-btn-border-1x c-btn-square">Explore</a>
									</div>
								</div>
								<div class="c-bg-img-center c-overlay-object" data-height="height" style="height: 270px; background-image: url(../../assets/base/img/content/shop2/70.jpg);"></div>
								<div class="c-overlay-border"></div>
							</div>
						</div>
						<div class="col-sm-4">
							<div class="c-content c-content-overlay">
								<div class="c-overlay-wrapper c-overlay-padding">
									<div class="c-overlay-content">
										<a href="shop-product-details.html" class="btn btn-md c-btn-grey-1 c-btn-uppercase c-btn-bold c-btn-border-1x c-btn-square">Explore</a>
									</div>
								</div>
								<div class="c-bg-img-center c-overlay-object" data-height="height" style="height: 270px; background-image: url(../../assets/base/img/content/shop2/79.jpg);"></div>
								<div class="c-overlay-border"></div>
							</div>
						</div>
					</div>
				</div>

				<div role="tabpanel" class="tab-pane fade" id="phone2">
					<div class="row">
						<div class="col-sm-4">
							<div class="c-content c-content-overlay">
								<div class="c-overlay-wrapper c-overlay-padding">
									<div class="c-overlay-content">
										<a href="shop-product-details.html" class="btn btn-md c-btn-grey-1 c-btn-uppercase c-btn-bold c-btn-border-1x c-btn-square">Explore</a>
									</div>
								</div>
								<div class="c-bg-img-center c-overlay-object" data-height="height" style="height: 270px; background-image: url(../../assets/base/img/content/shop2/63.jpg);"></div>
								<div class="c-overlay-border"></div>
							</div>
						</div>
						<div class="col-sm-4">
							<div class="c-content c-content-overlay">
								<div class="c-overlay-wrapper c-overlay-padding">
									<div class="c-overlay-content">
										<a href="shop-product-details.html" class="btn btn-md c-btn-grey-1 c-btn-uppercase c-btn-bold c-btn-border-1x c-btn-square">Explore</a>
									</div>
								</div>
								<div class="c-bg-img-center c-overlay-object" data-height="height" style="height: 270px; background-image: url(../../assets/base/img/content/shop2/59.jpg);"></div>
								<div class="c-overlay-border"></div>
							</div>
						</div>
						<div class="col-sm-4">
							<div class="c-content c-content-overlay">
								<div class="c-overlay-wrapper c-overlay-padding">
									<div class="c-overlay-content">
										<a href="shop-product-details.html" class="btn btn-md c-btn-grey-1 c-btn-uppercase c-btn-bold c-btn-border-1x c-btn-square">Explore</a>
									</div>
								</div>
								<div class="c-bg-img-center c-overlay-object" data-height="height" style="height: 270px; background-image: url(../../assets/base/img/content/shop2/71.jpg);"></div>
								<div class="c-overlay-border"></div>
							</div>
						</div>
					</div>
				</div>

				<div role="tabpanel" class="tab-pane fade" id="imac2">
					<div class="row">
						<div class="col-sm-4">
							<div class="c-content c-content-overlay">
								<div class="c-overlay-wrapper c-overlay-padding">
									<div class="c-overlay-content">
										<a href="shop-product-details.html" class="btn btn-md c-btn-grey-1 c-btn-uppercase c-btn-bold c-btn-border-1x c-btn-square">Explore</a>
									</div>
								</div>
								<div class="c-bg-img-center c-overlay-object" data-height="height" style="height: 270px; background-image: url(../../assets/base/img/content/shop3/73.jpg);"></div>
								<div class="c-overlay-border"></div>
							</div>
						</div>
						<div class="col-sm-4">
							<div class="c-content c-content-overlay">
								<div class="c-overlay-wrapper c-overlay-padding">
									<div class="c-overlay-content">
										<a href="shop-product-details.html" class="btn btn-md c-btn-grey-1 c-btn-uppercase c-btn-bold c-btn-border-1x c-btn-square">Explore</a>
									</div>
								</div>
								<div class="c-bg-img-center c-overlay-object" data-height="height" style="height: 270px; background-image: url(../../assets/base/img/content/shop3/74.jpg);"></div>
								<div class="c-overlay-border"></div>
							</div>
						</div>
						<div class="col-sm-4">
							<div class="c-content c-content-overlay">
								<div class="c-overlay-wrapper c-overlay-padding">
									<div class="c-overlay-content">
										<a href="shop-product-details.html" class="btn btn-md c-btn-grey-1 c-btn-uppercase c-btn-bold c-btn-border-1x c-btn-square">Explore</a>
									</div>
								</div>
								<div class="c-bg-img-center c-overlay-object" data-height="height" style="height: 270px; background-image: url(../../assets/base/img/content/shop3/91.jpg);"></div>
								<div class="c-overlay-border"></div>
							</div>
						</div>
					</div>
				</div>

				<div role="tabpanel" class="tab-pane fade" id="accessories2">
					<div class="row">
						<div class="col-sm-4">
							<div class="c-content c-content-overlay">
								<div class="c-overlay-wrapper c-overlay-padding">
									<div class="c-overlay-content">
										<a href="shop-product-details.html" class="btn btn-md c-btn-grey-1 c-btn-uppercase c-btn-bold c-btn-border-1x c-btn-square">Explore</a>
									</div>
								</div>
								<div class="c-bg-img-center c-overlay-object" data-height="height" style="height: 270px; background-image: url(../../assets/base/img/content/shop3/63.jpg);"></div>
								<div class="c-overlay-border"></div>
							</div>
						</div>
						<div class="col-sm-4">
							<div class="c-content c-content-overlay">
								<div class="c-overlay-wrapper c-overlay-padding">
									<div class="c-overlay-content">
										<a href="shop-product-details.html" class="btn btn-md c-btn-grey-1 c-btn-uppercase c-btn-bold c-btn-border-1x c-btn-square">Explore</a>
									</div>
								</div>
								<div class="c-bg-img-center c-overlay-object" data-height="height" style="height: 270px; background-image: url(../../assets/base/img/content/shop3/67.jpg);"></div>
								<div class="c-overlay-border"></div>
							</div>
						</div>
						<div class="col-sm-4">
							<div class="c-content c-content-overlay">
								<div class="c-overlay-wrapper c-overlay-padding">
									<div class="c-overlay-content">
										<a href="shop-product-details.html" class="btn btn-md c-btn-grey-1 c-btn-uppercase c-btn-bold c-btn-border-1x c-btn-square">Explore</a>
									</div>
								</div>
								<div class="c-bg-img-center c-overlay-object" data-height="height" style="height: 270px; background-image: url(../../assets/base/img/content/shop3/77.jpg);"></div>
								<div class="c-overlay-border"></div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div><!-- END: CONTENT/SHOPS/SHOP-1-2 -->

<!-- BEGIN: CONTENT/SHOPS/SHOP-1-5 -->
<div class="c-content-box c-size-md">
	<div class="c-content-tab-5 c-bs-grid-reset-space c-theme">
		<!-- Nav tabs -->
		<ul class="nav nav-pills c-nav-tab c-arrow" role="tablist">
			<li role="presentation" class="active">
				<a class="c-font-uppercase" href="#watches5" aria-controls="watches" role="tab" data-toggle="tab">Watches</a>
			</li>
			<li role="presentation">
				<a class="c-font-uppercase" href="#phone5" aria-controls="phone" role="tab" data-toggle="tab">Phone</a>
			</li>
			<li role="presentation">
				<a class="c-font-uppercase" href="#imac5" aria-controls="imac" role="tab" data-toggle="tab">iMac</a>
			</li>
			<li role="presentation">
				<a class="c-font-uppercase" href="#accessories5" aria-controls="accessories" role="tab" data-toggle="tab">Accessories</a>
			</li>
		</ul>
		<!-- Tab panes -->
		<div class="tab-content">
			<div role="tabpanel" class="tab-pane fade in active" id="watches5">
				<div class="row">
					<div class="col-md-3 col-sm-6">
						<div class="c-content c-content-overlay">
							<div class="c-overlay-wrapper c-overlay-padding">
								<div class="c-overlay-content">
									<a href="shop-product-details-2.html" class="btn btn-md c-btn-grey-1 c-btn-uppercase c-btn-bold c-btn-border-1x c-btn-square">Explore</a>
								</div>
							</div>
							<div class="c-bg-img-center c-overlay-object" data-height="height" style="height: 270px; background-image: url(../../assets/base/img/content/shop2/69.jpg);"></div>
							<div class="c-overlay-border"></div>
						</div>
					</div>
					<div class="col-md-3 col-sm-6">
						<div class="c-content c-content-overlay">
							<div class="c-overlay-wrapper c-overlay-padding">
								<div class="c-overlay-content">
									<a href="shop-product-details-2.html" class="btn btn-md c-btn-grey-1 c-btn-uppercase c-btn-bold c-btn-border-1x c-btn-square">Explore</a>
								</div>
							</div>
							<div class="c-bg-img-center c-overlay-object" data-height="height" style="height: 270px; background-image: url(../../assets/base/img/content/shop2/70.jpg);"></div>
							<div class="c-overlay-border"></div>
						</div>
					</div>
					<div class="col-md-3 col-sm-6">
						<div class="c-content c-content-overlay">
							<div class="c-overlay-wrapper c-overlay-padding">
								<div class="c-overlay-content">
									<a href="shop-product-details-2.html" class="btn btn-md c-btn-grey-1 c-btn-uppercase c-btn-bold c-btn-border-1x c-btn-square">Explore</a>
								</div>
							</div>
							<div class="c-bg-img-center c-overlay-object" data-height="height" style="height: 270px; background-image: url(../../assets/base/img/content/shop2/79.jpg);"></div>
							<div class="c-overlay-border"></div>
						</div>
					</div>
					<div class="col-md-3 col-sm-6">
						<div class="c-content c-content-overlay">
							<div class="c-overlay-wrapper c-overlay-padding">
								<div class="c-overlay-content">
									<a href="shop-product-details-2.html" class="btn btn-md c-btn-grey-1 c-btn-uppercase c-btn-bold c-btn-border-1x c-btn-square">Explore</a>
								</div>
							</div>
							<div class="c-bg-img-center c-overlay-object" data-height="height" style="height: 270px; background-image: url(../../assets/base/img/content/shop2/60.jpg);"></div>
							<div class="c-overlay-border"></div>
						</div>
					</div>
				</div>
			</div>

			<div role="tabpanel" class="tab-pane fade" id="phone5">
				<div class="row">
					<div class="col-md-3 col-sm-6">
						<div class="c-content c-content-overlay">
							<div class="c-overlay-wrapper c-overlay-padding">
								<div class="c-overlay-content">
									<a href="shop-product-details-2.html" class="btn btn-md c-btn-grey-1 c-btn-uppercase c-btn-bold c-btn-border-1x c-btn-square">Explore</a>
								</div>
							</div>
							<div class="c-bg-img-center c-overlay-object" data-height="height" style="height: 270px; background-image: url(../../assets/base/img/content/shop2/63.jpg);"></div>
							<div class="c-overlay-border"></div>
						</div>
					</div>
					<div class="col-md-3 col-sm-6">
						<div class="c-content c-content-overlay">
							<div class="c-overlay-wrapper c-overlay-padding">
								<div class="c-overlay-content">
									<a href="shop-product-details-2.html" class="btn btn-md c-btn-grey-1 c-btn-uppercase c-btn-bold c-btn-border-1x c-btn-square">Explore</a>
								</div>
							</div>
							<div class="c-bg-img-center c-overlay-object" data-height="height" style="height: 270px; background-image: url(../../assets/base/img/content/shop2/59.jpg);"></div>
							<div class="c-overlay-border"></div>
						</div>
					</div>
					<div class="col-md-3 col-sm-6">
						<div class="c-content c-content-overlay">
							<div class="c-overlay-wrapper c-overlay-padding">
								<div class="c-overlay-content">
									<a href="shop-product-details-2.html" class="btn btn-md c-btn-grey-1 c-btn-uppercase c-btn-bold c-btn-border-1x c-btn-square">Explore</a>
								</div>
							</div>
							<div class="c-bg-img-center c-overlay-object" data-height="height" style="height: 270px; background-image: url(../../assets/base/img/content/shop2/71.jpg);"></div>
							<div class="c-overlay-border"></div>
						</div>
					</div>
					<div class="col-md-3 col-sm-6">
						<div class="c-content c-content-overlay">
							<div class="c-overlay-wrapper c-overlay-padding">
								<div class="c-overlay-content">
									<a href="shop-product-details-2.html" class="btn btn-md c-btn-grey-1 c-btn-uppercase c-btn-bold c-btn-border-1x c-btn-square">Explore</a>
								</div>
							</div>
							<div class="c-bg-img-center c-overlay-object" data-height="height" style="height: 270px; background-image: url(../../assets/base/img/content/shop3/91.jpg);"></div>
							<div class="c-overlay-border"></div>
						</div>
					</div>
				</div>
			</div>

			<div role="tabpanel" class="tab-pane fade" id="imac5">
				<div class="row">
					<div class="col-md-3 col-sm-6">
						<div class="c-content c-content-overlay">
							<div class="c-overlay-wrapper c-overlay-padding">
								<div class="c-overlay-content">
									<a href="shop-product-details-2.html" class="btn btn-md c-btn-grey-1 c-btn-uppercase c-btn-bold c-btn-border-1x c-btn-square">Explore</a>
								</div>
							</div>
							<div class="c-bg-img-center c-overlay-object" data-height="height" style="height: 270px; background-image: url(../../assets/base/img/content/shop3/73.jpg);"></div>
							<div class="c-overlay-border"></div>
						</div>
					</div>
					<div class="col-md-3 col-sm-6">
						<div class="c-content c-content-overlay">
							<div class="c-overlay-wrapper c-overlay-padding">
								<div class="c-overlay-content">
									<a href="shop-product-details-2.html" class="btn btn-md c-btn-grey-1 c-btn-uppercase c-btn-bold c-btn-border-1x c-btn-square">Explore</a>
								</div>
							</div>
							<div class="c-bg-img-center c-overlay-object" data-height="height" style="height: 270px; background-image: url(../../assets/base/img/content/shop3/74.jpg);"></div>
							<div class="c-overlay-border"></div>
						</div>
					</div>
					<div class="col-md-3 col-sm-6">
						<div class="c-content c-content-overlay">
							<div class="c-overlay-wrapper c-overlay-padding">
								<div class="c-overlay-content">
									<a href="shop-product-details-2.html" class="btn btn-md c-btn-grey-1 c-btn-uppercase c-btn-bold c-btn-border-1x c-btn-square">Explore</a>
								</div>
							</div>
							<div class="c-bg-img-center c-overlay-object" data-height="height" style="height: 270px; background-image: url(../../assets/base/img/content/shop3/91.jpg);"></div>
							<div class="c-overlay-border"></div>
						</div>
					</div>
					<div class="col-md-3 col-sm-6">
						<div class="c-content c-content-overlay">
							<div class="c-overlay-wrapper c-overlay-padding">
								<div class="c-overlay-content">
									<a href="shop-product-details-2.html" class="btn btn-md c-btn-grey-1 c-btn-uppercase c-btn-bold c-btn-border-1x c-btn-square">Explore</a>
								</div>
							</div>
							<div class="c-bg-img-center c-overlay-object" data-height="height" style="height: 270px; background-image: url(../../assets/base/img/content/shop2/71.jpg);"></div>
							<div class="c-overlay-border"></div>
						</div>
					</div>
				</div>
			</div>

			<div role="tabpanel" class="tab-pane fade" id="accessories5">
				<div class="row">
					<div class="col-md-3 col-sm-6">
						<div class="c-content c-content-overlay">
							<div class="c-overlay-wrapper c-overlay-padding">
								<div class="c-overlay-content">
									<a href="shop-product-details-2.html" class="btn btn-md c-btn-grey-1 c-btn-uppercase c-btn-bold c-btn-border-1x c-btn-square">Explore</a>
								</div>
							</div>
							<div class="c-bg-img-center c-overlay-object" data-height="height" style="height: 270px; background-image: url(../../assets/base/img/content/shop3/63.jpg);"></div>
							<div class="c-overlay-border"></div>
						</div>
					</div>
					<div class="col-md-3 col-sm-6">
						<div class="c-content c-content-overlay">
							<div class="c-overlay-wrapper c-overlay-padding">
								<div class="c-overlay-content">
									<a href="shop-product-details-2.html" class="btn btn-md c-btn-grey-1 c-btn-uppercase c-btn-bold c-btn-border-1x c-btn-square">Explore</a>
								</div>
							</div>
							<div class="c-bg-img-center c-overlay-object" data-height="height" style="height: 270px; background-image: url(../../assets/base/img/content/shop3/67.jpg);"></div>
							<div class="c-overlay-border"></div>
						</div>
					</div>
					<div class="col-md-3 col-sm-6">
						<div class="c-content c-content-overlay">
							<div class="c-overlay-wrapper c-overlay-padding">
								<div class="c-overlay-content">
									<a href="shop-product-details-2.html" class="btn btn-md c-btn-grey-1 c-btn-uppercase c-btn-bold c-btn-border-1x c-btn-square">Explore</a>
								</div>
							</div>
							<div class="c-bg-img-center c-overlay-object" data-height="height" style="height: 270px; background-image: url(../../assets/base/img/content/shop3/77.jpg);"></div>
							<div class="c-overlay-border"></div>
						</div>
					</div>
					<div class="col-md-3 col-sm-6">
						<div class="c-content c-content-overlay">
							<div class="c-overlay-wrapper c-overlay-padding">
								<div class="c-overlay-content">
									<a href="shop-product-details-2.html" class="btn btn-md c-btn-grey-1 c-btn-uppercase c-btn-bold c-btn-border-1x c-btn-square">Explore</a>
								</div>
							</div>
							<div class="c-bg-img-center c-overlay-object" data-height="height" style="height: 270px; background-image: url(../../assets/base/img/content/shop3/74.jpg);"></div>
							<div class="c-overlay-border"></div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div><!-- END: CONTENT/SHOPS/SHOP-1-5 -->

<!-- BEGIN: CONTENT/SHOPS/SHOP-1-6 -->
<div class="c-content-box c-size-md">
	<div class="container">
		<div class="c-content-tab-5 c-theme">
			<!-- Nav tabs -->
			<ul class="nav nav-pills c-nav-tab c-arrow" role="tablist">
				<li role="presentation" class="active">
					<a class="c-font-uppercase" href="#watches6" aria-controls="watches" role="tab" data-toggle="tab">Watches</a>
				</li>
				<li role="presentation">
					<a class="c-font-uppercase" href="#phone6" aria-controls="phone" role="tab" data-toggle="tab">Phone</a>
				</li>
				<li role="presentation">
					<a class="c-font-uppercase" href="#imac6" aria-controls="imac" role="tab" data-toggle="tab">iMac</a>
				</li>
				<li role="presentation">
					<a class="c-font-uppercase" href="#accessories6" aria-controls="accessories" role="tab" data-toggle="tab">Accessories</a>
				</li>
			</ul>
			<!-- Tab panes -->
			<div class="tab-content">
				<div role="tabpanel" class="tab-pane fade in active" id="watches6">
					<div class="row">
						<div class="col-sm-4">
							<div class="c-content c-content-overlay">
								<div class="c-overlay-wrapper c-overlay-padding">
									<div class="c-overlay-content">
										<a href="shop-product-details.html" class="btn btn-md c-btn-grey-1 c-btn-uppercase c-btn-bold c-btn-border-1x c-btn-square">Explore</a>
									</div>
								</div>
								<div class="c-bg-img-center c-overlay-object" data-height="height" style="height: 270px; background-image: url(../../assets/base/img/content/shop2/69.jpg);"></div>
								<div class="c-overlay-border"></div>
							</div>
						</div>
						<div class="col-sm-4">
							<div class="c-content c-content-overlay">
								<div class="c-overlay-wrapper c-overlay-padding">
									<div class="c-overlay-content">
										<a href="shop-product-details.html" class="btn btn-md c-btn-grey-1 c-btn-uppercase c-btn-bold c-btn-border-1x c-btn-square">Explore</a>
									</div>
								</div>
								<div class="c-bg-img-center c-overlay-object" data-height="height" style="height: 270px; background-image: url(../../assets/base/img/content/shop2/70.jpg);"></div>
								<div class="c-overlay-border"></div>
							</div>
						</div>
						<div class="col-sm-4">
							<div class="c-content c-content-overlay">
								<div class="c-overlay-wrapper c-overlay-padding">
									<div class="c-overlay-content">
										<a href="shop-product-details.html" class="btn btn-md c-btn-grey-1 c-btn-uppercase c-btn-bold c-btn-border-1x c-btn-square">Explore</a>
									</div>
								</div>
								<div class="c-bg-img-center c-overlay-object" data-height="height" style="height: 270px; background-image: url(../../assets/base/img/content/shop2/79.jpg);"></div>
								<div class="c-overlay-border"></div>
							</div>
						</div>
					</div>
					<div class="row c-margin-t-30">
						<div class="col-sm-4">
							<div class="c-content c-content-overlay">
								<div class="c-overlay-wrapper c-overlay-padding">
									<div class="c-overlay-content">
										<a href="shop-product-details.html" class="btn btn-md c-btn-grey-1 c-btn-uppercase c-btn-bold c-btn-border-1x c-btn-square">Explore</a>
									</div>
								</div>
								<div class="c-bg-img-center c-overlay-object" data-height="height" style="height: 270px; background-image: url(../../assets/base/img/content/shop2/63.jpg);"></div>
								<div class="c-overlay-border"></div>
							</div>
						</div>
						<div class="col-sm-4">
							<div class="c-content c-content-overlay">
								<div class="c-overlay-wrapper c-overlay-padding">
									<div class="c-overlay-content">
										<a href="shop-product-details.html" class="btn btn-md c-btn-grey-1 c-btn-uppercase c-btn-bold c-btn-border-1x c-btn-square">Explore</a>
									</div>
								</div>
								<div class="c-bg-img-center c-overlay-object" data-height="height" style="height: 270px; background-image: url(../../assets/base/img/content/shop2/59.jpg);"></div>
								<div class="c-overlay-border"></div>
							</div>
						</div>
						<div class="col-sm-4">
							<div class="c-content c-content-overlay">
								<div class="c-overlay-wrapper c-overlay-padding">
									<div class="c-overlay-content">
										<a href="shop-product-details.html" class="btn btn-md c-btn-grey-1 c-btn-uppercase c-btn-bold c-btn-border-1x c-btn-square">Explore</a>
									</div>
								</div>
								<div class="c-bg-img-center c-overlay-object" data-height="height" style="height: 270px; background-image: url(../../assets/base/img/content/shop2/71.jpg);"></div>
								<div class="c-overlay-border"></div>
							</div>
						</div>
					</div>
				</div>

				<div role="tabpanel" class="tab-pane fade" id="phone6">
					<div class="row">
						<div class="col-sm-4">
							<div class="c-content c-content-overlay">
								<div class="c-overlay-wrapper c-overlay-padding">
									<div class="c-overlay-content">
										<a href="shop-product-details.html" class="btn btn-md c-btn-grey-1 c-btn-uppercase c-btn-bold c-btn-border-1x c-btn-square">Explore</a>
									</div>
								</div>
								<div class="c-bg-img-center c-overlay-object" data-height="height" style="height: 270px; background-image: url(../../assets/base/img/content/shop2/63.jpg);"></div>
								<div class="c-overlay-border"></div>
							</div>
						</div>
						<div class="col-sm-4">
							<div class="c-content c-content-overlay">
								<div class="c-overlay-wrapper c-overlay-padding">
									<div class="c-overlay-content">
										<a href="shop-product-details.html" class="btn btn-md c-btn-grey-1 c-btn-uppercase c-btn-bold c-btn-border-1x c-btn-square">Explore</a>
									</div>
								</div>
								<div class="c-bg-img-center c-overlay-object" data-height="height" style="height: 270px; background-image: url(../../assets/base/img/content/shop2/59.jpg);"></div>
								<div class="c-overlay-border"></div>
							</div>
						</div>
						<div class="col-sm-4">
							<div class="c-content c-content-overlay">
								<div class="c-overlay-wrapper c-overlay-padding">
									<div class="c-overlay-content">
										<a href="shop-product-details.html" class="btn btn-md c-btn-grey-1 c-btn-uppercase c-btn-bold c-btn-border-1x c-btn-square">Explore</a>
									</div>
								</div>
								<div class="c-bg-img-center c-overlay-object" data-height="height" style="height: 270px; background-image: url(../../assets/base/img/content/shop2/71.jpg);"></div>
								<div class="c-overlay-border"></div>
							</div>
						</div>
					</div>
					<div class="row c-margin-t-30">
						<div class="col-sm-4">
							<div class="c-content c-content-overlay">
								<div class="c-overlay-wrapper c-overlay-padding">
									<div class="c-overlay-content">
										<a href="shop-product-details.html" class="btn btn-md c-btn-grey-1 c-btn-uppercase c-btn-bold c-btn-border-1x c-btn-square">Explore</a>
									</div>
								</div>
								<div class="c-bg-img-center c-overlay-object" data-height="height" style="height: 270px; background-image: url(../../assets/base/img/content/shop2/69.jpg);"></div>
								<div class="c-overlay-border"></div>
							</div>
						</div>
						<div class="col-sm-4">
							<div class="c-content c-content-overlay">
								<div class="c-overlay-wrapper c-overlay-padding">
									<div class="c-overlay-content">
										<a href="shop-product-details.html" class="btn btn-md c-btn-grey-1 c-btn-uppercase c-btn-bold c-btn-border-1x c-btn-square">Explore</a>
									</div>
								</div>
								<div class="c-bg-img-center c-overlay-object" data-height="height" style="height: 270px; background-image: url(../../assets/base/img/content/shop2/70.jpg);"></div>
								<div class="c-overlay-border"></div>
							</div>
						</div>
						<div class="col-sm-4">
							<div class="c-content c-content-overlay">
								<div class="c-overlay-wrapper c-overlay-padding">
									<div class="c-overlay-content">
										<a href="shop-product-details.html" class="btn btn-md c-btn-grey-1 c-btn-uppercase c-btn-bold c-btn-border-1x c-btn-square">Explore</a>
									</div>
								</div>
								<div class="c-bg-img-center c-overlay-object" data-height="height" style="height: 270px; background-image: url(../../assets/base/img/content/shop2/79.jpg);"></div>
								<div class="c-overlay-border"></div>
							</div>
						</div>
					</div>
				</div>

				<div role="tabpanel" class="tab-pane fade" id="imac6">
					<div class="row">
						<div class="col-sm-4">
							<div class="c-content c-content-overlay">
								<div class="c-overlay-wrapper c-overlay-padding">
									<div class="c-overlay-content">
										<a href="shop-product-details.html" class="btn btn-md c-btn-grey-1 c-btn-uppercase c-btn-bold c-btn-border-1x c-btn-square">Explore</a>
									</div>
								</div>
								<div class="c-bg-img-center c-overlay-object" data-height="height" style="height: 270px; background-image: url(../../assets/base/img/content/shop3/73.jpg);"></div>
								<div class="c-overlay-border"></div>
							</div>
						</div>
						<div class="col-sm-4">
							<div class="c-content c-content-overlay">
								<div class="c-overlay-wrapper c-overlay-padding">
									<div class="c-overlay-content">
										<a href="shop-product-details.html" class="btn btn-md c-btn-grey-1 c-btn-uppercase c-btn-bold c-btn-border-1x c-btn-square">Explore</a>
									</div>
								</div>
								<div class="c-bg-img-center c-overlay-object" data-height="height" style="height: 270px; background-image: url(../../assets/base/img/content/shop3/74.jpg);"></div>
								<div class="c-overlay-border"></div>
							</div>
						</div>
						<div class="col-sm-4">
							<div class="c-content c-content-overlay">
								<div class="c-overlay-wrapper c-overlay-padding">
									<div class="c-overlay-content">
										<a href="shop-product-details.html" class="btn btn-md c-btn-grey-1 c-btn-uppercase c-btn-bold c-btn-border-1x c-btn-square">Explore</a>
									</div>
								</div>
								<div class="c-bg-img-center c-overlay-object" data-height="height" style="height: 270px; background-image: url(../../assets/base/img/content/shop3/91.jpg);"></div>
								<div class="c-overlay-border"></div>
							</div>
						</div>
					</div>
					<div class="row c-margin-t-30">
						<div class="col-sm-4">
							<div class="c-content c-content-overlay">
								<div class="c-overlay-wrapper c-overlay-padding">
									<div class="c-overlay-content">
										<a href="shop-product-details.html" class="btn btn-md c-btn-grey-1 c-btn-uppercase c-btn-bold c-btn-border-1x c-btn-square">Explore</a>
									</div>
								</div>
								<div class="c-bg-img-center c-overlay-object" data-height="height" style="height: 270px; background-image: url(../../assets/base/img/content/shop3/63.jpg);"></div>
								<div class="c-overlay-border"></div>
							</div>
						</div>
						<div class="col-sm-4">
							<div class="c-content c-content-overlay">
								<div class="c-overlay-wrapper c-overlay-padding">
									<div class="c-overlay-content">
										<a href="shop-product-details.html" class="btn btn-md c-btn-grey-1 c-btn-uppercase c-btn-bold c-btn-border-1x c-btn-square">Explore</a>
									</div>
								</div>
								<div class="c-bg-img-center c-overlay-object" data-height="height" style="height: 270px; background-image: url(../../assets/base/img/content/shop3/67.jpg);"></div>
								<div class="c-overlay-border"></div>
							</div>
						</div>
						<div class="col-sm-4">
							<div class="c-content c-content-overlay">
								<div class="c-overlay-wrapper c-overlay-padding">
									<div class="c-overlay-content">
										<a href="shop-product-details.html" class="btn btn-md c-btn-grey-1 c-btn-uppercase c-btn-bold c-btn-border-1x c-btn-square">Explore</a>
									</div>
								</div>
								<div class="c-bg-img-center c-overlay-object" data-height="height" style="height: 270px; background-image: url(../../assets/base/img/content/shop3/77.jpg);"></div>
								<div class="c-overlay-border"></div>
							</div>
						</div>
					</div>
				</div>

				<div role="tabpanel" class="tab-pane fade" id="accessories6">
					<div class="row">
						<div class="col-sm-4">
							<div class="c-content c-content-overlay">
								<div class="c-overlay-wrapper c-overlay-padding">
									<div class="c-overlay-content">
										<a href="shop-product-details.html" class="btn btn-md c-btn-grey-1 c-btn-uppercase c-btn-bold c-btn-border-1x c-btn-square">Explore</a>
									</div>
								</div>
								<div class="c-bg-img-center c-overlay-object" data-height="height" style="height: 270px; background-image: url(../../assets/base/img/content/shop3/63.jpg);"></div>
								<div class="c-overlay-border"></div>
							</div>
						</div>
						<div class="col-sm-4">
							<div class="c-content c-content-overlay">
								<div class="c-overlay-wrapper c-overlay-padding">
									<div class="c-overlay-content">
										<a href="shop-product-details.html" class="btn btn-md c-btn-grey-1 c-btn-uppercase c-btn-bold c-btn-border-1x c-btn-square">Explore</a>
									</div>
								</div>
								<div class="c-bg-img-center c-overlay-object" data-height="height" style="height: 270px; background-image: url(../../assets/base/img/content/shop3/67.jpg);"></div>
								<div class="c-overlay-border"></div>
							</div>
						</div>
						<div class="col-sm-4">
							<div class="c-content c-content-overlay">
								<div class="c-overlay-wrapper c-overlay-padding">
									<div class="c-overlay-content">
										<a href="shop-product-details.html" class="btn btn-md c-btn-grey-1 c-btn-uppercase c-btn-bold c-btn-border-1x c-btn-square">Explore</a>
									</div>
								</div>
								<div class="c-bg-img-center c-overlay-object" data-height="height" style="height: 270px; background-image: url(../../assets/base/img/content/shop3/77.jpg);"></div>
								<div class="c-overlay-border"></div>
							</div>
						</div>
					</div>
					<div class="row c-margin-t-30">
						<div class="col-sm-4">
							<div class="c-content c-content-overlay">
								<div class="c-overlay-wrapper c-overlay-padding">
									<div class="c-overlay-content">
										<a href="shop-product-details.html" class="btn btn-md c-btn-grey-1 c-btn-uppercase c-btn-bold c-btn-border-1x c-btn-square">Explore</a>
									</div>
								</div>
								<div class="c-bg-img-center c-overlay-object" data-height="height" style="height: 270px; background-image: url(../../assets/base/img/content/shop3/73.jpg);"></div>
								<div class="c-overlay-border"></div>
							</div>
						</div>
						<div class="col-sm-4">
							<div class="c-content c-content-overlay">
								<div class="c-overlay-wrapper c-overlay-padding">
									<div class="c-overlay-content">
										<a href="shop-product-details.html" class="btn btn-md c-btn-grey-1 c-btn-uppercase c-btn-bold c-btn-border-1x c-btn-square">Explore</a>
									</div>
								</div>
								<div class="c-bg-img-center c-overlay-object" data-height="height" style="height: 270px; background-image: url(../../assets/base/img/content/shop3/74.jpg);"></div>
								<div class="c-overlay-border"></div>
							</div>
						</div>
						<div class="col-sm-4">
							<div class="c-content c-content-overlay">
								<div class="c-overlay-wrapper c-overlay-padding">
									<div class="c-overlay-content">
										<a href="shop-product-details.html" class="btn btn-md c-btn-grey-1 c-btn-uppercase c-btn-bold c-btn-border-1x c-btn-square">Explore</a>
									</div>
								</div>
								<div class="c-bg-img-center c-overlay-object" data-height="height" style="height: 270px; background-image: url(../../assets/base/img/content/shop3/91.jpg);"></div>
								<div class="c-overlay-border"></div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div><!-- END: CONTENT/SHOPS/SHOP-1-6 -->

<!-- BEGIN: CONTENT/SHOPS/SHOP-1-3 -->
<div class="c-content-box c-size-md c-overflow-hide c-bg-grey-1">
	<div class="c-content-tab-5 c-bs-grid-reset-space">
		<div class="c-content-title-4 c-theme">
			<h3 class="c-font-uppercase c-center c-font-bold c-line-strike"><span class="c-bg-grey-1">Most Popular</span></h3>
		</div>
		<div class="row">
			<div class="col-md-3 col-sm-6">
				<div class="c-content c-content-overlay">
					<div class="c-overlay-wrapper c-overlay-padding">
						<div class="c-overlay-content">
							<a href="shop-product-details-2.html" class="btn btn-md c-btn-grey-1 c-btn-uppercase c-btn-bold c-btn-border-1x c-btn-square">Explore</a>
						</div>
					</div>
					<div class="c-bg-img-center c-overlay-object" data-height="height" style="height: 270px; background-image: url(../../assets/base/img/content/shop3/85.jpg);"></div>
					<div class="c-overlay-border"></div>
				</div>
			</div>
			<div class="col-md-3 col-sm-6">
				<div class="c-content c-content-overlay">
					<div class="c-overlay-wrapper c-overlay-padding">
						<div class="c-overlay-content">
							<a href="shop-product-details-2.html" class="btn btn-md c-btn-grey-1 c-btn-uppercase c-btn-bold c-btn-border-1x c-btn-square">Explore</a>
						</div>
					</div>
					<div class="c-bg-img-center c-overlay-object" data-height="height" style="height: 270px; background-image: url(../../assets/base/img/content/shop3/86.jpg);"></div>
					<div class="c-overlay-border"></div>
				</div>
			</div>
			<div class="col-md-3 col-sm-6">
				<div class="c-content c-content-overlay">
					<div class="c-overlay-wrapper c-overlay-padding">
						<div class="c-overlay-content">
							<a href="shop-product-details-2.html" class="btn btn-md c-btn-grey-1 c-btn-uppercase c-btn-bold c-btn-border-1x c-btn-square">Explore</a>
						</div>
					</div>
					<div class="c-bg-img-center c-overlay-object" data-height="height" style="height: 270px; background-image: url(../../assets/base/img/content/shop3/87.jpg);"></div>
					<div class="c-overlay-border"></div>
				</div>
			</div>
			<div class="col-md-3 col-sm-6">
				<div class="c-content c-content-overlay">
					<div class="c-overlay-wrapper c-overlay-padding">
						<div class="c-overlay-content">
							<a href="shop-product-details-2.html" class="btn btn-md c-btn-grey-1 c-btn-uppercase c-btn-bold c-btn-border-1x c-btn-square">Explore</a>
						</div>
					</div>
					<div class="c-bg-img-center c-overlay-object" data-height="height" style="height: 270px; background-image: url(../../assets/base/img/content/shop3/88.jpg);"></div>
					<div class="c-overlay-border"></div>
				</div>
			</div>
		</div>
	</div>
</div><!-- END: CONTENT/SHOPS/SHOP-1-3 -->

<!-- BEGIN: CONTENT/SHOPS/SHOP-1-8 -->
<div class="c-content-box c-size-md c-overflow-hide c-bg-grey-1">
	<div class="c-content-title-4 c-theme">
		<h3 class="c-font-uppercase c-center c-font-bold c-line-strike"><span class="c-bg-grey-1">Most Popular</span>
		</h3>
	</div>
	<div class="row">
		<div data-slider="owl">
			<div class="owl-carousel c-theme owl-reset-space c-owl-nav-center" data-rtl="true" data-items="4">
				<div class="item">
					<div class="c-content c-content-overlay">
						<div class="c-overlay-wrapper c-overlay-padding">
							<div class="c-overlay-content">
								<a href="shop-product-details.html" class="btn btn-md c-btn-grey-1 c-btn-uppercase c-btn-bold c-btn-border-1x c-btn-square">Explore</a>
							</div>
						</div>
						<div class="c-bg-img-center c-overlay-object" data-height="height" style="height: 270px; background-image: url(../../assets/base/img/content/shop3/85.jpg);"></div>
						<div class="c-overlay-border"></div>
					</div>
				</div>
				<div class="item">
					<div class="c-content c-content-overlay">
						<div class="c-overlay-wrapper c-overlay-padding">
							<div class="c-overlay-content">
								<a href="shop-product-details.html" class="btn btn-md c-btn-grey-1 c-btn-uppercase c-btn-bold c-btn-border-1x c-btn-square">Explore</a>
							</div>
						</div>
						<div class="c-bg-img-center c-overlay-object" data-height="height" style="height: 270px; background-image: url(../../assets/base/img/content/shop3/86.jpg);"></div>
						<div class="c-overlay-border"></div>
					</div>
				</div>
				<div class="item">
					<div class="c-content c-content-overlay">
						<div class="c-overlay-wrapper c-overlay-padding">
							<div class="c-overlay-content">
								<a href="shop-product-details.html" class="btn btn-md c-btn-grey-1 c-btn-uppercase c-btn-bold c-btn-border-1x c-btn-square">Explore</a>
							</div>
						</div>
						<div class="c-bg-img-center c-overlay-object" data-height="height" style="height: 270px; background-image: url(../../assets/base/img/content/shop3/87.jpg);"></div>
						<div class="c-overlay-border"></div>
					</div>
				</div>
				<div class="item">
					<div class="c-content c-content-overlay">
						<div class="c-overlay-wrapper c-overlay-padding">
							<div class="c-overlay-content">
								<a href="shop-product-details.html" class="btn btn-md c-btn-grey-1 c-btn-uppercase c-btn-bold c-btn-border-1x c-btn-square">Explore</a>
							</div>
						</div>
						<div class="c-bg-img-center c-overlay-object" data-height="height" style="height: 270px; background-image: url(../../assets/base/img/content/shop3/88.jpg);"></div>
						<div class="c-overlay-border"></div>
					</div>
				</div>
				<div class="item">
					<div class="c-content c-content-overlay">
						<div class="c-overlay-wrapper c-overlay-padding">
							<div class="c-overlay-content">
								<a href="shop-product-details.html" class="btn btn-md c-btn-grey-1 c-btn-uppercase c-btn-bold c-btn-border-1x c-btn-square">Explore</a>
							</div>
						</div>
						<div class="c-bg-img-center c-overlay-object" data-height="height" style="height: 270px; background-image: url(../../assets/base/img/content/shop3/89.jpg);"></div>
						<div class="c-overlay-border"></div>
					</div>
				</div>
				<div class="item">
					<div class="c-content c-content-overlay">
						<div class="c-overlay-wrapper c-overlay-padding">
							<div class="c-overlay-content">
								<a href="shop-product-details.html" class="btn btn-md c-btn-grey-1 c-btn-uppercase c-btn-bold c-btn-border-1x c-btn-square">Explore</a>
							</div>
						</div>
						<div class="c-bg-img-center c-overlay-object" data-height="height" style="height: 270px; background-image: url(../../assets/base/img/content/shop3/90.jpg);"></div>
						<div class="c-overlay-border"></div>
					</div>
				</div>
				<div class="item">
					<div class="c-content c-content-overlay">
						<div class="c-overlay-wrapper c-overlay-padding">
							<div class="c-overlay-content">
								<a href="shop-product-details.html" class="btn btn-md c-btn-grey-1 c-btn-uppercase c-btn-bold c-btn-border-1x c-btn-square">Explore</a>
							</div>
						</div>
						<div class="c-bg-img-center c-overlay-object" data-height="height" style="height: 270px; background-image: url(../../assets/base/img/content/shop3/91.jpg);"></div>
						<div class="c-overlay-border"></div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div><!-- END: CONTENT/SHOPS/SHOP-1-8 -->

		<!-- END: PAGE CONTENT -->
	</div>
	<!-- END: PAGE CONTAINER -->

	<!-- BEGIN: LAYOUT/FOOTERS/FOOTER-5 -->

</body>
