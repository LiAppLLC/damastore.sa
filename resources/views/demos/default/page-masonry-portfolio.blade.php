@extends('Welcome.home')


@section('content')

    <?php
    $locale = App::getLocale();
    ?>

    <body class="c-layout-header-fixed c-layout-header-mobile-fixed">

	<!-- BEGIN: CONTENT/USER/FORGET-PASSWORD-FORM -->
<div class="modal fade c-content-login-form" id="forget-password-form" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content c-square">
            <div class="modal-header c-no-border">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            </div>
            <div class="modal-body">
                <h3 class="c-font-24 c-font-sbold">Password Recovery</h3>
                <p>To recover your password please fill in your email address</p>
                <form>
                    <div class="form-group">
                        <label for="forget-email" class="hide">Email</label>
                        <input type="email" class="form-control input-lg c-square" id="forget-email" placeholder="Email">
                    </div>
                    <div class="form-group">
                        <button type="submit" class="btn c-theme-btn btn-md c-btn-uppercase c-btn-bold c-btn-square c-btn-login">Submit</button>
                        <a href="javascript:;" class="c-btn-forgot" data-toggle="modal" data-target="#login-form" data-dismiss="modal">Back To Login</a>
                    </div>
                </form>
            </div>
            <div class="modal-footer c-no-border">
                <span class="c-text-account">Don't Have An Account Yet ?</span>
                <a href="javascript:;" data-toggle="modal" data-target="#signup-form" data-dismiss="modal" class="btn c-btn-dark-1 btn c-btn-uppercase c-btn-bold c-btn-slim c-btn-border-2x c-btn-square c-btn-signup">Signup!</a>
            </div>
        </div>
    </div>
</div><!-- END: CONTENT/USER/FORGET-PASSWORD-FORM -->
	<!-- BEGIN: CONTENT/USER/SIGNUP-FORM -->
<div class="modal fade c-content-login-form" id="signup-form" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content c-square">
            <div class="modal-header c-no-border">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            </div>
            <div class="modal-body">
                <h3 class="c-font-24 c-font-sbold">Create An Account</h3>
                <p>Please fill in below form to create an account with us</p>
                <form>
                    <div class="form-group">
                        <label for="signup-email" class="hide">Email</label>
                        <input type="email" class="form-control input-lg c-square" id="signup-email" placeholder="Email">
                    </div>
                    <div class="form-group">
                        <label for="signup-username" class="hide">Username</label>
                        <input type="email" class="form-control input-lg c-square" id="signup-username" placeholder="Username">
                    </div>
                    <div class="form-group">
                        <label for="signup-fullname" class="hide">Fullname</label>
                        <input type="email" class="form-control input-lg c-square" id="signup-fullname" placeholder="Fullname">
                    </div>
                    <div class="form-group">
                        <label for="signup-country" class="hide">Country</label>
                        <select class="form-control input-lg c-square" id="signup-country">
                            <option value="1">Country</option>
                        </select>
                    </div>
                    <div class="form-group">
                        <button type="submit" class="btn c-theme-btn btn-md c-btn-uppercase c-btn-bold c-btn-square c-btn-login">Signup</button>
                        <a href="javascript:;" class="c-btn-forgot" data-toggle="modal" data-target="#login-form" data-dismiss="modal">Back To Login</a>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div><!-- END: CONTENT/USER/SIGNUP-FORM -->
	<!-- BEGIN: CONTENT/USER/LOGIN-FORM -->
<div class="modal fade c-content-login-form" id="login-form" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content c-square">
            <div class="modal-header c-no-border">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            </div>
            <div class="modal-body">
                <h3 class="c-font-24 c-font-sbold">Good Afternoon!</h3>
                <p>Let's make today a great day!</p>
                <form>
                    <div class="form-group">
                        <label for="login-email" class="hide">Email</label>
                        <input type="email" class="form-control input-lg c-square" id="login-email" placeholder="Email">
                    </div>
                    <div class="form-group">
                        <label for="login-password" class="hide">Password</label>
                        <input type="password" class="form-control input-lg c-square" id="login-password" placeholder="Password">
                    </div>
                    <div class="form-group">
                        <div class="c-checkbox">
                            <input type="checkbox" id="login-rememberme" class="c-check">
                            <label for="login-rememberme" class="c-font-thin c-font-17">
                                <span></span>
                                <span class="check"></span>
                                <span class="box"></span>
                                Remember Me
                            </label>
                        </div>
                    </div>
                    <div class="form-group">
                        <button type="submit" class="btn c-theme-btn btn-md c-btn-uppercase c-btn-bold c-btn-square c-btn-login">Login</button>
                        <a href="javascript:;" data-toggle="modal" data-target="#forget-password-form" data-dismiss="modal" class="c-btn-forgot">Forgot Your Password ?</a>
                    </div>
                    <div class="clearfix">
                        <div class="c-content-divider c-divider-sm c-icon-bg c-bg-grey c-margin-b-20">
                            <span>or signup with</span>
                        </div>
                        <ul class="c-content-list-adjusted">
                            <li>
                                <a class="btn btn-block c-btn-square btn-social btn-twitter">
                                  <i class="fa fa-twitter"></i>
                                  Twitter
                                </a>
                            </li>
                            <li>
                                <a class="btn btn-block c-btn-square btn-social btn-facebook">
                                  <i class="fa fa-facebook"></i>
                                  Facebook
                                </a>
                            </li>
                            <li>
                                <a class="btn btn-block c-btn-square btn-social btn-google">
                                  <i class="fa fa-google"></i>
                                  Google
                                </a>
                            </li>
                        </ul>
                    </div>
                </form>
            </div>
            <div class="modal-footer c-no-border">
                <span class="c-text-account">Don't Have An Account Yet ?</span>
                <a href="javascript:;" data-toggle="modal" data-target="#signup-form" data-dismiss="modal" class="btn c-btn-dark-1 btn c-btn-uppercase c-btn-bold c-btn-slim c-btn-border-2x c-btn-square c-btn-signup">Signup!</a>
            </div>
        </div>
    </div>
</div><!-- END: CONTENT/USER/LOGIN-FORM -->

	<!-- BEGIN: LAYOUT/SIDEBARS/QUICK-SIDEBAR -->
<nav class="c-layout-quick-sidebar">
	<div class="c-header">
		<button type="button" class="c-link c-close">
		<i class="icon-login"></i>
		</button>
	</div>
	<div class="c-content">
		<div class="c-section">
			<h3>JANGO DEMOS</h3>
			<div class="c-settings c-demos c-bs-grid-reset-space">
				<div class="row">
					<div class="col-md-12">
						<a href="../default/index.html" class="c-demo-container c-demo-img-lg">
							<div class="c-demo-thumb active">
								<img src="../../assets/base/img/content/quick-sidebar/default.jpg" class="c-demo-thumb-img"/>
							</div>
						</a>
					</div>
				</div>
				<div class="row">
					<div class="col-md-6">
						<a href="../corporate_1/index.html" class="c-demo-container">
							<div class="c-demo-thumb  c-thumb-left">
								<img src="../../assets/base/img/content/quick-sidebar/corporate_1.jpg" class="c-demo-thumb-img"/>
							</div>
						</a>
					</div>
					<div class="col-md-6">
						<a href="../agency_1/index.html" class="c-demo-container">
							<div class="c-demo-thumb  c-thumb-right">
								<img src="../../assets/base/img/content/quick-sidebar/corporate_1-onepage.jpg" class="c-demo-thumb-img"/>
							</div>
						</a>
					</div>
				</div>
			</div>
		</div>
		<div class="c-section">
			<h3>Theme Colors</h3>
			<div class="c-settings">

				<span class="c-color c-default c-active" data-color="default"></span>

				<span class="c-color c-green1" data-color="green1"></span>
				<span class="c-color c-green2" data-color="green2"></span>
				<span class="c-color c-green3" data-color="green3"></span>

				<span class="c-color c-yellow1" data-color="yellow1"></span>
				<span class="c-color c-yellow2" data-color="yellow2"></span>
				<span class="c-color c-yellow3" data-color="yellow3"></span>

				<span class="c-color c-red1" data-color="red1"></span>
				<span class="c-color c-red2" data-color="red2"></span>
				<span class="c-color c-red3" data-color="red3"></span>

				<span class="c-color c-purple1" data-color="purple1"></span>
				<span class="c-color c-purple2" data-color="purple2"></span>
				<span class="c-color c-purple3" data-color="purple3"></span>

				<span class="c-color c-blue1" data-color="blue1"></span>
				<span class="c-color c-blue2" data-color="blue2"></span>
				<span class="c-color c-blue3" data-color="blue3"></span>

				<span class="c-color c-brown1" data-color="brown1"></span>
				<span class="c-color c-brown2" data-color="brown2"></span>
				<span class="c-color c-brown3" data-color="brown3"></span>

				<span class="c-color c-dark1" data-color="dark1"></span>
				<span class="c-color c-dark2" data-color="dark2"></span>
				<span class="c-color c-dark3" data-color="dark3"></span>
			</div>
		</div>
		<div class="c-section">
			<h3>Header Type</h3>
			<div class="c-settings">
				<input type="button" class="c-setting_header-type btn btn-sm c-btn-square c-btn-border-1x c-btn-white c-btn-sbold c-btn-uppercase active" data-value="boxed" value="boxed"/>
				<input type="button" class="c-setting_header-type btn btn-sm c-btn-square c-btn-border-1x c-btn-white c-btn-sbold c-btn-uppercase" data-value="fluid" value="fluid"/>
			</div>
		</div>
		<div class="c-section">
			<h3>Header Mode</h3>
			<div class="c-settings">
				<input type="button" class="c-setting_header-mode btn btn-sm c-btn-square c-btn-border-1x c-btn-white c-btn-sbold c-btn-uppercase active" data-value="fixed" value="fixed"/>
				<input type="button" class="c-setting_header-mode btn btn-sm c-btn-square c-btn-border-1x c-btn-white c-btn-sbold c-btn-uppercase" data-value="static" value="static"/>
			</div>
		</div>
		<div class="c-section">
			<h3>Mega Menu Style</h3>
			<div class="c-settings">
				<input type="button" class="c-setting_megamenu-style btn btn-sm c-btn-square c-btn-border-1x c-btn-white c-btn-sbold c-btn-uppercase active" data-value="dark" value="dark"/>
				<input type="button" class="c-setting_megamenu-style btn btn-sm c-btn-square c-btn-border-1x c-btn-white c-btn-sbold c-btn-uppercase" data-value="light" value="light"/>
			</div>
		</div>
		<div class="c-section">
			<h3>Font Style</h3>
			<div class="c-settings">
				<input type="button" class="c-setting_font-style btn btn-sm c-btn-square c-btn-border-1x c-btn-white c-btn-sbold c-btn-uppercase active" data-value="default" value="default"/>
				<input type="button" class="c-setting_font-style btn btn-sm c-btn-square c-btn-border-1x c-btn-white c-btn-sbold c-btn-uppercase" data-value="light" value="light"/>
			</div>
		</div>
		<div class="c-section">
			<h3>Reading Style</h3>
			<div class="c-settings">
				<a href="http://www.themehats.com/themes/jango/" class="c-setting_font-style btn btn-sm c-btn-square c-btn-border-1x c-btn-white c-btn-sbold c-btn-uppercase ">LTR</a>
				<a href="http://www.themehats.com/themes/jango/rtl/" class="c-setting_font-style btn btn-sm c-btn-square c-btn-border-1x c-btn-white c-btn-sbold c-btn-uppercase active">RTL</a>
			</div>
		</div>
	</div>
</nav><!-- END: LAYOUT/SIDEBARS/QUICK-SIDEBAR -->

	<!-- BEGIN: PAGE CONTAINER -->
	<div class="c-layout-page">
		<!-- BEGIN: LAYOUT/BREADCRUMBS/BREADCRUMBS-1 -->

<div class="c-layout-breadcrumbs-1 c-fonts-uppercase c-fonts-bold">
	<div class="container">
		<div class="c-page-title c-pull-left">
			<h3 class="c-font-uppercase c-font-sbold">Masonry Portfolio</h3>
		</div>
		<ul class="c-page-breadcrumbs c-theme-nav c-pull-right c-fonts-regular">
															<li><a href="#">Pages</a></li>
			<li>/</li>
															<li><a href="page-masonry-portfolio.html">Masonry Portfolio</a></li>
			<li>/</li>
															<li class="c-state_active">Jango Components</li>

		</ul>
	</div>
</div><!-- END: LAYOUT/BREADCRUMBS/BREADCRUMBS-1 -->
		<!-- BEGIN: PAGE CONTENT -->
		<div class="c-content-box c-size-md">
	<div class="container">
	    <div class="cbp-panel">
	        <div id="filters-container" class="cbp-l-filters-buttonCenter">
	            <div data-filter="*" class="cbp-filter-item-active cbp-filter-item">
	                All <div class="cbp-filter-counter"></div>
	            </div>
	            <div data-filter=".identity" class="cbp-filter-item">
	                Identity <div class="cbp-filter-counter"></div>
	            </div>
	            <div data-filter=".web-design" class="cbp-filter-item">
	                Web Design <div class="cbp-filter-counter"></div>
	            </div>
	            <div data-filter=".graphic" class="cbp-filter-item">
	                Graphic <div class="cbp-filter-counter"></div>
	            </div>
	            <div data-filter=".logos" class="cbp-filter-item">
	                Logo <div class="cbp-filter-counter"></div>
	            </div>
	            <div data-filter=".logos, .graphic" class="cbp-filter-item">
	                Logo & Graphic <div class="cbp-filter-counter"></div>
	            </div>
	        </div>

	        <div id="grid-container" class="cbp cbp-l-grid-masonry-projects">
	            <div class="cbp-item graphic">
	                <div class="cbp-caption">
	                    <div class="cbp-caption-defaultWrap">
	                        <img src="../../assets/base/img/content/stock/62.jpg" alt="">
	                    </div>
	                    <div class="cbp-caption-activeWrap">
	                    	<div class="c-masonry-border"></div>
	                        <div class="cbp-l-caption-alignCenter">
	                            <div class="cbp-l-caption-body">
	                                <a href="ajax/projects/project1.html" class="cbp-singlePage cbp-l-caption-buttonLeft btn c-btn-square c-btn-border-1x c-btn-white c-btn-bold c-btn-uppercase">Explore</a>
	                                <a href="../../assets/base/img/content/stock/62.jpg" class="cbp-lightbox cbp-l-caption-buttonRight btn c-btn-square c-btn-border-1x c-btn-white c-btn-bold c-btn-uppercase" data-title="Dashboard<br>by Paul Flavius Nechita">Zoom</a>
	                            </div>
	                        </div>
	                    </div>
	                </div>
	                <a href="ajax/projects/project1.html" class="cbp-singlePage cbp-l-grid-masonry-projects-title">BeenTo</a>
	                <div class="cbp-l-grid-masonry-projects-desc">web design / graphic</div>
	            </div>
	            <div class="cbp-item web-design logos">
	                <div class="cbp-caption">
	                    <div class="cbp-caption-defaultWrap">
	                        <img src="../../assets/base/img/content/stock/63.jpg" alt="">
	                    </div>
	                    <div class="cbp-caption-activeWrap">
	                    	<div class="c-masonry-border"></div>
	                        <div class="cbp-l-caption-alignCenter">
	                            <div class="cbp-l-caption-body">
	                                <a href="ajax/projects/project2.html" class="cbp-singlePage cbp-l-caption-buttonLeft btn c-btn-square c-btn-border-1x c-btn-white c-btn-bold c-btn-uppercase">Explore</a>
	                                <a href="../../assets/base/img/content/stock/63.jpg" class="cbp-lightbox cbp-l-caption-buttonRight btn c-btn-square c-btn-border-1x c-btn-white c-btn-bold c-btn-uppercase" data-title="World Clock Widget<br>by Paul Flavius Nechita">Zoom</a>
	                            </div>
	                        </div>
	                    </div>
	                </div>
	                <a href="ajax/projects/project2.html" class="cbp-singlePage cbp-l-grid-masonry-projects-title">World Clock Widget</a>
	                <div class="cbp-l-grid-masonry-projects-desc">logos / web design</div>
	            </div>
	            <div class="cbp-item graphic logos">
	                <div class="cbp-caption">
	                    <div class="cbp-caption-defaultWrap">
	                        <img src="../../assets/base/img/content/stock/64.jpg" alt="">
	                    </div>
	                    <div class="cbp-caption-activeWrap">
	                    	<div class="c-masonry-border"></div>
	                        <div class="cbp-l-caption-alignCenter">
	                            <div class="cbp-l-caption-body">
	                                <a href="ajax/projects/project3.html" class="cbp-singlePage cbp-l-caption-buttonLeft btn c-btn-square c-btn-border-1x c-btn-white c-btn-bold c-btn-uppercase">Explore</a>
	                                <a href="http://vimeo.com/14912890" class="cbp-lightbox cbp-l-caption-buttonRight btn c-btn-square c-btn-border-1x c-btn-white c-btn-bold c-btn-uppercase" data-title="To-Do Dashboard<br>by Tiberiu Neamu">Zoom</a>
	                            </div>
	                        </div>
	                    </div>
	                </div>
	                <a href="ajax/projects/project3.html" class="cbp-singlePage cbp-l-grid-masonry-projects-title">To-Do Dashboard</a>
	                <div class="cbp-l-grid-masonry-projects-desc">graphic / logos</div>
	            </div>
	            <div class="cbp-item identity web-design">
	                <div class="cbp-caption">
	                    <div class="cbp-caption-defaultWrap">
	                        <img src="../../assets/base/img/content/stock/65.jpg" alt="">
	                    </div>
	                    <div class="cbp-caption-activeWrap">
	                    	<div class="c-masonry-border"></div>
	                        <div class="cbp-l-caption-alignCenter">
	                            <div class="cbp-l-caption-body">
	                                <a href="ajax/projects/project4.html" class="cbp-singlePage cbp-l-caption-buttonLeft btn c-btn-square c-btn-border-1x c-btn-white c-btn-bold c-btn-uppercase">Explore</a>
	                                <a href="../../assets/base/img/content/stock/65.jpg" class="cbp-lightbox cbp-l-caption-buttonRight btn c-btn-square c-btn-border-1x c-btn-white c-btn-bold c-btn-uppercase" data-title="WhereTO App<br>by Tiberiu Neamu">Zoom</a>
	                            </div>
	                        </div>
	                    </div>
	                </div>
	                <a href="ajax/projects/project5.html" class="cbp-singlePage cbp-l-grid-masonry-projects-title">WhereTO App</a>
	                <div class="cbp-l-grid-masonry-projects-desc">web design / identity</div>
	            </div>
	            <div class="cbp-item web-design graphic">
	                <div class="cbp-caption">
	                    <div class="cbp-caption-defaultWrap">
	                        <img src="../../assets/base/img/content/stock/66.jpg" alt="">
	                    </div>
	                    <div class="cbp-caption-activeWrap">
	                    	<div class="c-masonry-border"></div>
	                        <div class="cbp-l-caption-alignCenter">
	                            <div class="cbp-l-caption-body">
	                                <a href="ajax/projects/project5.html" class="cbp-singlePage cbp-l-caption-buttonLeft btn c-btn-square c-btn-border-1x c-btn-white c-btn-bold c-btn-uppercase">Explore</a>
	                                <a href="../../assets/base/img/content/stock/66.jpg" class="cbp-lightbox cbp-l-caption-buttonRight btn c-btn-square c-btn-border-1x c-btn-white c-btn-bold c-btn-uppercase" data-title="Events and  More<br>by Tiberiu Neamu">Zoom</a>
	                            </div>
	                        </div>
	                    </div>
	                </div>
	                <a href="ajax/projects/project4.html" class="cbp-singlePage cbp-l-grid-masonry-projects-title">Events and More</a>
	                <div class="cbp-l-grid-masonry-projects-desc">web design / graphic</div>
	            </div>
	            <div class="cbp-item identity web-design">
	                <div class="cbp-caption">
	                    <div class="cbp-caption-defaultWrap">
	                        <img src="../../assets/base/img/content/stock/67.jpg" alt="">
	                    </div>
	                    <div class="cbp-caption-activeWrap">
	                    	<div class="c-masonry-border"></div>
	                        <div class="cbp-l-caption-alignCenter">
	                            <div class="cbp-l-caption-body">
	                                <a href="ajax/projects/project6.html" class="cbp-singlePage cbp-l-caption-buttonLeft btn c-btn-square c-btn-border-1x c-btn-white c-btn-bold c-btn-uppercase">Explore</a>
	                                <a href="../../assets/base/img/content/stock/67.jpg" class="cbp-lightbox cbp-l-caption-buttonRight btn c-btn-square c-btn-border-1x c-btn-white c-btn-bold c-btn-uppercase" data-title="Ski * Buddy<br>by Tiberiu Neamu">Zoom</a>
	                            </div>
	                        </div>
	                    </div>
	                </div>
	                <a href="ajax/projects/project6.html" class="cbp-singlePage cbp-l-grid-masonry-projects-title">Ski * Buddy</a>
	                <div class="cbp-l-grid-masonry-projects-desc">identity / web design</div>
	            </div>
	            <div class="cbp-item graphic logos">
	                <div class="cbp-caption">
	                    <div class="cbp-caption-defaultWrap">
	                        <img src="../../assets/base/img/content/stock/68.jpg" alt="">
	                    </div>
	                    <div class="cbp-caption-activeWrap">
	                    	<div class="c-masonry-border"></div>
	                        <div class="cbp-l-caption-alignCenter">
	                            <div class="cbp-l-caption-body">
	                                <a href="ajax/projects/project7.html" class="cbp-singlePage cbp-l-caption-buttonLeft btn c-btn-square c-btn-border-1x c-btn-white c-btn-bold c-btn-uppercase">Explore</a>
	                                <a href="../../assets/base/img/content/stock/68.jpg" class="cbp-lightbox cbp-l-caption-buttonRight btn c-btn-square c-btn-border-1x c-btn-white c-btn-bold c-btn-uppercase" data-title="Seemple* Music for iPad<br>by Tiberiu Neamu">Zoom</a>
	                            </div>
	                        </div>
	                    </div>
	                </div>
	                <a href="ajax/projects/project7.html" class="cbp-singlePage cbp-l-grid-masonry-projects-title">Seemple* Music for iPad</a>
	                <div class="cbp-l-grid-masonry-projects-desc">graphic / logos</div>
	            </div>
	            <div class="cbp-item identity graphic">
	                <div class="cbp-caption">
	                    <div class="cbp-caption-defaultWrap">
	                        <img src="../../assets/base/img/content/stock/69.jpg" alt="">
	                    </div>
	                    <div class="cbp-caption-activeWrap">
	                    	<div class="c-masonry-border"></div>
	                        <div class="cbp-l-caption-alignCenter">
	                            <div class="cbp-l-caption-body">
	                                <a href="ajax/projects/project8.html" class="cbp-singlePage cbp-l-caption-buttonLeft btn c-btn-square c-btn-border-1x c-btn-white c-btn-bold c-btn-uppercase">Explore</a>
	                                <a href="http://www.youtube.com/watch?v=Bu9OiDmxCrQ" class="cbp-lightbox cbp-l-caption-buttonRight btn c-btn-square c-btn-border-1x c-btn-white c-btn-bold c-btn-uppercase" data-title="Remind~Me More<br>by Tiberiu Neamu">Zoom</a>
	                            </div>
	                        </div>
	                    </div>
	                </div>
	                <a href="ajax/projects/project8.html" class="cbp-singlePage cbp-l-grid-masonry-projects-title">Remind~Me More</a>
	                <div class="cbp-l-grid-masonry-projects-desc">identity / graphic</div>
	            </div>
	            <div class="cbp-item web-design graphic">
	                <div class="cbp-caption">
	                    <div class="cbp-caption-defaultWrap">
	                        <img src="../../assets/base/img/content/stock/70.jpg" alt="">
	                    </div>
	                    <div class="cbp-caption-activeWrap">
	                    	<div class="c-masonry-border"></div>
	                        <div class="cbp-l-caption-alignCenter">
	                            <div class="cbp-l-caption-body">
	                                <a href="ajax/projects/project9.html" class="cbp-singlePage cbp-l-caption-buttonLeft btn c-btn-square c-btn-border-1x c-btn-white c-btn-bold c-btn-uppercase">Explore</a>
	                                <a href="https://www.ted.com/talks/andrew_bastawrous_get_your_next_eye_exam_on_a_smartphone" class="cbp-lightbox cbp-l-caption-buttonRight btn c-btn-square c-btn-border-1x c-btn-white c-btn-bold c-btn-uppercase" data-title="Workout Buddy<br>by Tiberiu Neamu">Zoom</a>
	                            </div>
	                        </div>
	                    </div>
	                </div>
	                <a href="ajax/projects/project9.html" class="cbp-singlePage cbp-l-grid-masonry-projects-title">Workout Buddy</a>
	                <div class="cbp-l-grid-masonry-projects-desc">web design / graphic</div>
	            </div>
	            <div class="cbp-item identity web-design">
	                <div class="cbp-caption">
	                    <div class="cbp-caption-defaultWrap">
	                        <img src="../../assets/base/img/content/stock/71.jpg" alt="">
	                    </div>
	                    <div class="cbp-caption-activeWrap">
	                    	<div class="c-masonry-border"></div>
	                        <div class="cbp-l-caption-alignCenter">
	                            <div class="cbp-l-caption-body">
	                                <a href="ajax/projects/project10.html" class="cbp-singlePage cbp-l-caption-buttonLeft btn c-btn-square c-btn-border-1x c-btn-white c-btn-bold c-btn-uppercase">Explore</a>
	                                <a href="https://www.youtube.com/watch?v=3wbvpOIIBQA" class="cbp-lightbox cbp-l-caption-buttonRight btn c-btn-square c-btn-border-1x c-btn-white c-btn-bold c-btn-uppercase" data-title="Bills Bills Bills<br>by Cosmin Capitanu">Zoom</a>
	                            </div>
	                        </div>
	                    </div>
	                </div>
	                <a href="ajax/projects/project10.html" class="cbp-singlePage cbp-l-grid-masonry-projects-title">Bills Bills Bills</a>
	                <div class="cbp-l-grid-masonry-projects-desc">identity / web design</div>
	            </div>
	            <div class="cbp-item identity logos">
	                <div class="cbp-caption">
	                    <div class="cbp-caption-defaultWrap">
	                        <img src="../../assets/base/img/content/stock/72.jpg" alt="">
	                    </div>
	                    <div class="cbp-caption-activeWrap">
	                    	<div class="c-masonry-border"></div>
	                        <div class="cbp-l-caption-alignCenter">
	                            <div class="cbp-l-caption-body">
	                                <a href="ajax/projects/project11.html" class="cbp-singlePage cbp-l-caption-buttonLeft btn c-btn-square c-btn-border-1x c-btn-white c-btn-bold c-btn-uppercase">Explore</a>
	                                <a href="../../assets/base/img/content/stock/72.jpg" class="cbp-lightbox cbp-l-caption-buttonRight btn c-btn-square c-btn-border-1x c-btn-white c-btn-bold c-btn-uppercase" data-title="Generic Apps<br>by Cosmin Capitanu">Zoom</a>
	                            </div>
	                        </div>
	                    </div>
	                </div>
	                <a href="ajax/projects/project11.html" class="cbp-singlePage cbp-l-grid-masonry-projects-title">Generic Apps</a>
	                <div class="cbp-l-grid-masonry-projects-desc">identity / logos</div>
	            </div>
	            <div class="cbp-item graphic web-design">
	                <div class="cbp-caption">
	                    <div class="cbp-caption-defaultWrap">
	                        <img src="../../assets/base/img/content/stock/73.jpg" alt="">
	                    </div>
	                    <div class="cbp-caption-activeWrap">
	                    	<div class="c-masonry-border"></div>
	                        <div class="cbp-l-caption-alignCenter">
	                            <div class="cbp-l-caption-body">
	                                <a href="ajax/projects/project12.html" class="cbp-singlePage cbp-l-caption-buttonLeft btn c-btn-square c-btn-border-1x c-btn-white c-btn-bold c-btn-uppercase">Explore</a>
	                                <a href="http://vimeo.com/877053" class="cbp-lightbox cbp-l-caption-buttonRight btn c-btn-square c-btn-border-1x c-btn-white c-btn-bold c-btn-uppercase" data-title="Speed Detector<br>by Cosmin Capitanu">Zoom</a>
	                            </div>
	                        </div>
	                    </div>
	                </div>
	                <a href="ajax/projects/project12.html" class="cbp-singlePage cbp-l-grid-masonry-projects-title">Speed Detector</a>
	                <div class="cbp-l-grid-masonry-projects-desc">graphic / web design</div>
	            </div>
	        </div>

	        <div id="loadMore-container" class="cbp-l-loadMore-button c-margin-t-60">
	            <a href="ajax/masonry-portfolio/load-more.html" class="cbp-l-loadMore-link btn c-btn-square c-btn-border-2x c-btn-dark c-btn-bold c-btn-uppercase">
	                <span class="cbp-l-loadMore-defaultText">LOAD MORE</span>
	                <span class="cbp-l-loadMore-loadingText">LOADING...</span>
	                <span class="cbp-l-loadMore-noMoreLoading">NO MORE WORKS</span>
	            </a>
	        </div>
	    </div>
	</div>
</div>
		<!-- END: PAGE CONTENT -->
	</div>
	<!-- END: PAGE CONTAINER -->

	<!-- BEGIN: LAYOUT/FOOTERS/FOOTER-5 -->



</body>
@section('script')
    <!-- BEGIN: PAGE SCRIPTS -->
    <script src="../../assets/demos/default/js/scripts/pages/masonry-portfolio.js" type="text/javascript"></script>
    <!-- END: PAGE SCRIPTS -->
    <!-- END: LAYOUT/BASE/BOTTOM -->

@endsection
