@extends('Welcome.home')


@section('content')

    <?php
    $locale = App::getLocale();
    ?>


    <body class="c-layout-header-fixed c-layout-header-mobile-fixed c-layout-header-topbar c-layout-header-topbar-collapse">



	<!-- BEGIN: PAGE CONTAINER -->
	<div class="c-layout-page">
		<!-- BEGIN: LAYOUT/BREADCRUMBS/BREADCRUMBS-2 -->
<div class="c-layout-breadcrumbs-1 c-subtitle c-fonts-uppercase c-fonts-bold c-bordered c-bordered-both">
	<div class="container">
		<div class="c-page-title c-pull-left">
			<h3 class="c-font-uppercase c-font-sbold">Customer Dashboard</h3>
			<h4 class="">Page Sub Title Goes Here</h4>
		</div>
		<ul class="c-page-breadcrumbs c-theme-nav c-pull-right c-fonts-regular">
															<li><a href="shop-customer-dashboard.html">Customer Dashboard</a></li>
			<li>/</li>
															<li class="c-state_active">Jango Components</li>

		</ul>
	</div>
</div>
		<!-- END: LAYOUT/BREADCRUMBS/BREADCRUMBS-2 -->



		<div class="container">
			<div class="c-layout-sidebar-menu c-theme ">
			<!-- BEGIN: LAYOUT/SIDEBARS/SHOP-SIDEBAR-DASHBOARD -->
<div class="c-sidebar-menu-toggler">
	<h3 class="c-title c-font-uppercase c-font-bold">My Profile</h3>
	<a href="javascript:;" class="c-content-toggler" data-toggle="collapse" data-target="#sidebar-menu-1">
		<span class="c-line"></span> <span class="c-line"></span> <span class="c-line"></span>
	</a>
</div>

<ul class="c-sidebar-menu collapse " id="sidebar-menu-1">
	<li class="c-dropdown c-open">
		<a href="javascript:;" class="c-toggler">My Profile<span class="c-arrow"></span></a>
		<ul class="c-dropdown-menu">
			<li class="c-active">
				<a href="{{asset('CustomerDashboard')}}">My Dashbord</a>
			</li>
			<li class="">
				<a href="{{asset('editprofile')}}">Edit Profile</a>
			</li>
			<li class="">
				<a href="{{asset('changePassword')}}">Change Password</a>
			</li>
			<li class="">
				<a href="shop-order-history.html">Order History</a>
			</li>
			<li class="">
				<a href="shop-customer-addresses.html">My Addresses</a>
			</li>
			<li class="">
				<a href="shop-product-wishlist.html">My Wishlist</a>
			</li>
		</ul>
	</li>
</ul><!-- END: LAYOUT/SIDEBARS/SHOP-SIDEBAR-DASHBOARD -->
			</div>
			<div class="c-layout-sidebar-content ">
			<!-- BEGIN: PAGE CONTENT -->
			<!-- BEGIN: CONTENT/SHOPS/SHOP-CUSTOMER-DASHBOARD-1 -->
<div class="c-content-title-1">
	<h3 class="c-font-uppercase c-font-bold">My Dashboard</h3>
	{{--<div class="c-line-left"></div>--}}
	{{--<p class="">--}}
		{{--Hello <a href="#" class="c-theme-link">Drake Hiro</a> (not <a href="#" class="c-theme-link">Drake Hiro</a>? <a href="#" class="c-theme-link">Sign out</a>). <br />--}}
	{{--</p>--}}
</div>
<div class="row">
	<div class="col-md-12 col-sm-12 col-xs-12 c-margin-b-20">

		<h3 class="c-font-uppercase c-font-bold" id="user_username"></h3>
		<ul class="list-unstyled">
			<li id="user_data"></li>
			<li id="user_phone"></li>
			{{--<li>Fax: 800 123 3456</li>--}}
			<li>Email: <a href="" class="c-theme-link" id="user_email"></a></li>
		</ul>
	</div>
</div><!-- END: CONTENT/SHOPS/SHOP-CUSTOMER-DASHBOARD-1 -->
			<!-- END: PAGE CONTENT -->
			</div>
		</div>
	</div>
	<!-- END: PAGE CONTAINER -->

	<!-- BEGIN: LAYOUT/FOOTERS/FOOTER-6 -->

</body>

@endsection

@section('script')


	{{--<script>--}}
	var userObject = JSON.parse(localStorage.getItem("DamaStore_userObject"));

	$('#user_email').text(userObject.email);
	$('#user_email').attr('href', 'mailto:' + userObject.email);
	$('#user_phone').text(userObject.phone);
	$('#user_username').text(userObject.username);
	$('#user_data').text(userObject.firstName + ' ' + userObject.lastName + ', ' + userObject.bornDate);
	// blocked
	// bornDate
	// created_at
	// email
	// emailVerifiedAt
	// files
	// firstName
	// id
	// lastActivity
	// lastLoggedIn
	// lastName
	// phone
	// reputation
	// roles
	// token
	// updated_at
	// username
	// verify
	console.log('userObject', userObject);




@endsection