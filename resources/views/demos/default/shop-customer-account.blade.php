@extends('Welcome.home')
@section('style')

    <meta name="google-signin-client_id" content="700057481815-ic788gvdi4a30usrqf688o7nsobk8dr7.apps.googleusercontent.com">


    <script src="https://apis.google.com/js/platform.js?onload=init" async defer></script>


    <meta name="csrf-token" content="{{ csrf_token() }}">

@endsection

@section('content')

    <?php
    $locale = App::getLocale();
    ?>


    <body class="c-layout-header-fixed c-layout-header-mobile-fixed c-layout-header-topbar c-layout-header-topbar-collapse">


	<!-- BEGIN: CONTENT/USER/FORGET-PASSWORD-FORM -->
<div class="modal fade c-content-login-form" id="forget-password-form" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content c-square">
            <div class="modal-header c-no-border">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            </div>
            <div class="modal-body">
                <h3 class="c-font-24 c-font-sbold">Password Recovery</h3>
                <p>To recover your password please fill in your email address</p>
                <form>
                    <div class="form-group">
                        <label for="forget-email" class="hide">Email</label>
                        <input type="email" class="form-control input-lg c-square" id="forget-email" placeholder="Email">
                    </div>
                    <div class="form-group">
                        <button type="submit" class="btn c-theme-btn btn-md c-btn-uppercase c-btn-bold c-btn-square c-btn-login">Submit</button>
                        <a href="javascript:;" class="c-btn-forgot" data-toggle="modal" data-target="#login-form" data-dismiss="modal">Back To Login</a>
                    </div>
                </form>
            </div>
            <div class="modal-footer c-no-border">
                <span class="c-text-account">Don't Have An Account Yet ?</span>
                <a href="javascript:;" data-toggle="modal" data-target="#signup-form" data-dismiss="modal" class="btn c-btn-dark-1 btn c-btn-uppercase c-btn-bold c-btn-slim c-btn-border-2x c-btn-square c-btn-signup">Signup!</a>
            </div>
        </div>
    </div>
</div>
	<!-- END: CONTENT/USER/FORGET-PASSWORD-FORM -->



	<!-- BEGIN: CONTENT/USER/SIGNUP-FORM -->
<div class="modal fade c-content-login-form" id="signup-form" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content c-square">
            <div class="modal-header c-no-border">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            </div>
            <div class="modal-body">
                <h3 class="c-font-24 c-font-sbold">Create An Account</h3>
                <p>Please fill in below form to create an account with us</p>
                <form>
                    <div class="form-group">
                        <label for="signup-email" class="hide">Email</label>
                        <input type="email" class="form-control input-lg c-square" id="signup-email" placeholder="Email">
                    </div>
                    <div class="form-group">
                        <label for="signup-username" class="hide">Username</label>
                        <input type="email" class="form-control input-lg c-square" id="signup-username" placeholder="Username">
                    </div>
                    <div class="form-group">
                        <label for="signup-fullname" class="hide">Fullname</label>
                        <input type="email" class="form-control input-lg c-square" id="signup-fullname" placeholder="Fullname">
                    </div>
                    <div class="form-group">
                        <label for="signup-country" class="hide">Country</label>
                        <select class="form-control input-lg c-square" id="signup-country">
                            <option value="1">Country</option>
                        </select>
                    </div>
                    <div class="form-group">
                        <button type="submit" class="btn c-theme-btn btn-md c-btn-uppercase c-btn-bold c-btn-square c-btn-login">Signup</button>
                        <a href="javascript:;" class="c-btn-forgot" data-toggle="modal" data-target="#login-form" data-dismiss="modal">Back To Login</a>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
	<!-- END: CONTENT/USER/SIGNUP-FORM -->



	<!-- BEGIN: CONTENT/USER/LOGIN-FORM -->
<div class="modal fade c-content-login-form" id="login-form" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content c-square">
            <div class="modal-header c-no-border">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            </div>
            <div class="modal-body">
                <h3 class="c-font-24 c-font-sbold">Good Afternoon!</h3>
                <p>Let's make today a great day!</p>
                <form>
                    <div class="form-group">
                        <label for="login-email" class="hide">Email</label>
                        <input type="email" class="form-control input-lg c-square" id="login-email" placeholder="Email">
                    </div>
                    <div class="form-group">
                        <label for="login-password" class="hide">Password</label>
                        <input type="password" class="form-control input-lg c-square" id="login-password" placeholder="Password">
                    </div>
                    <div class="form-group">
                        <div class="c-checkbox">
                            <input type="checkbox" id="login-rememberme" class="c-check">
                            <label for="login-rememberme" class="c-font-thin c-font-17">
                                <span></span>
                                <span class="check"></span>
                                <span class="box"></span>
                                Remember Me
                            </label>
                        </div>
                    </div>
                    <div class="form-group">
                        <button type="submit" class="btn c-theme-btn btn-md c-btn-uppercase c-btn-bold c-btn-square c-btn-login">Login</button>
                        <a href="javascript:;" data-toggle="modal" data-target="#forget-password-form" data-dismiss="modal" class="c-btn-forgot">Forgot Your Password ?</a>
                    </div>
                    <div class="clearfix">
                        <div class="c-content-divider c-divider-sm c-icon-bg c-bg-grey c-margin-b-20">
                            <span>or signup with</span>
                        </div>
                        <ul class="c-content-list-adjusted">
                            <li>
                                <a class="btn btn-block c-btn-square btn-social btn-twitter">
                                  <i class="fa fa-twitter"></i>
                                  Twitter
                                </a>
                            </li>
                            <li>
                                <a class="btn btn-block c-btn-square btn-social btn-facebook">
                                  <i class="fa fa-facebook"></i>
                                  Facebook
                                </a>
                            </li>
                            <li>
                                <a class="btn btn-block c-btn-square btn-social btn-google">
                                  <i class="fa fa-google"></i>
                                  Google
                                </a>
                            </li>
                        </ul>
                    </div>
                </form>
            </div>
            <div class="modal-footer c-no-border">
                <span class="c-text-account">Don't Have An Account Yet ?</span>
                <a href="javascript:;" data-toggle="modal" data-target="#signup-form" data-dismiss="modal" class="btn c-btn-dark-1 btn c-btn-uppercase c-btn-bold c-btn-slim c-btn-border-2x c-btn-square c-btn-signup">Signup!</a>
            </div>
        </div>
    </div>
</div>
	<!-- END: CONTENT/USER/LOGIN-FORM -->



	<!-- BEGIN: LAYOUT/SIDEBARS/QUICK-SIDEBAR -->
<nav class="c-layout-quick-sidebar">
	<div class="c-header">
		<button type="button" class="c-link c-close">
		<i class="icon-login"></i>
		</button>
	</div>
	<div class="c-content">
		<div class="c-section">
			<h3>JANGO DEMOS</h3>
			<div class="c-settings c-demos c-bs-grid-reset-space">
				<div class="row">
					<div class="col-md-12">
						<a href="../default/index.html" class="c-demo-container c-demo-img-lg">
							<div class="c-demo-thumb active">
								<img src="../../assets/base/img/content/quick-sidebar/default.jpg" class="c-demo-thumb-img"/>
							</div>
						</a>
					</div>
				</div>
				<div class="row">
					<div class="col-md-6">
						<a href="../corporate_1/index.html" class="c-demo-container">
							<div class="c-demo-thumb  c-thumb-left">
								<img src="../../assets/base/img/content/quick-sidebar/corporate_1.jpg" class="c-demo-thumb-img"/>
							</div>
						</a>
					</div>
					<div class="col-md-6">
						<a href="../agency_1/index.html" class="c-demo-container">
							<div class="c-demo-thumb  c-thumb-right">
								<img src="../../assets/base/img/content/quick-sidebar/corporate_1-onepage.jpg" class="c-demo-thumb-img"/>
							</div>
						</a>
					</div>
				</div>
			</div>
		</div>
		<div class="c-section">
			<h3>Theme Colors</h3>
			<div class="c-settings">

				<span class="c-color c-default c-active" data-color="default"></span>

				<span class="c-color c-green1" data-color="green1"></span>
				<span class="c-color c-green2" data-color="green2"></span>
				<span class="c-color c-green3" data-color="green3"></span>

				<span class="c-color c-yellow1" data-color="yellow1"></span>
				<span class="c-color c-yellow2" data-color="yellow2"></span>
				<span class="c-color c-yellow3" data-color="yellow3"></span>

				<span class="c-color c-red1" data-color="red1"></span>
				<span class="c-color c-red2" data-color="red2"></span>
				<span class="c-color c-red3" data-color="red3"></span>

				<span class="c-color c-purple1" data-color="purple1"></span>
				<span class="c-color c-purple2" data-color="purple2"></span>
				<span class="c-color c-purple3" data-color="purple3"></span>

				<span class="c-color c-blue1" data-color="blue1"></span>
				<span class="c-color c-blue2" data-color="blue2"></span>
				<span class="c-color c-blue3" data-color="blue3"></span>

				<span class="c-color c-brown1" data-color="brown1"></span>
				<span class="c-color c-brown2" data-color="brown2"></span>
				<span class="c-color c-brown3" data-color="brown3"></span>

				<span class="c-color c-dark1" data-color="dark1"></span>
				<span class="c-color c-dark2" data-color="dark2"></span>
				<span class="c-color c-dark3" data-color="dark3"></span>
			</div>
		</div>
		<div class="c-section">
			<h3>Header Type</h3>
			<div class="c-settings">
				<input type="button" class="c-setting_header-type btn btn-sm c-btn-square c-btn-border-1x c-btn-white c-btn-sbold c-btn-uppercase active" data-value="boxed" value="boxed"/>
				<input type="button" class="c-setting_header-type btn btn-sm c-btn-square c-btn-border-1x c-btn-white c-btn-sbold c-btn-uppercase" data-value="fluid" value="fluid"/>
			</div>
		</div>
		<div class="c-section">
			<h3>Header Mode</h3>
			<div class="c-settings">
				<input type="button" class="c-setting_header-mode btn btn-sm c-btn-square c-btn-border-1x c-btn-white c-btn-sbold c-btn-uppercase active" data-value="fixed" value="fixed"/>
				<input type="button" class="c-setting_header-mode btn btn-sm c-btn-square c-btn-border-1x c-btn-white c-btn-sbold c-btn-uppercase" data-value="static" value="static"/>
			</div>
		</div>
		<div class="c-section">
			<h3>Mega Menu Style</h3>
			<div class="c-settings">
				<input type="button" class="c-setting_megamenu-style btn btn-sm c-btn-square c-btn-border-1x c-btn-white c-btn-sbold c-btn-uppercase active" data-value="dark" value="dark"/>
				<input type="button" class="c-setting_megamenu-style btn btn-sm c-btn-square c-btn-border-1x c-btn-white c-btn-sbold c-btn-uppercase" data-value="light" value="light"/>
			</div>
		</div>
		<div class="c-section">
			<h3>Font Style</h3>
			<div class="c-settings">
				<input type="button" class="c-setting_font-style btn btn-sm c-btn-square c-btn-border-1x c-btn-white c-btn-sbold c-btn-uppercase active" data-value="default" value="default"/>
				<input type="button" class="c-setting_font-style btn btn-sm c-btn-square c-btn-border-1x c-btn-white c-btn-sbold c-btn-uppercase" data-value="light" value="light"/>
			</div>
		</div>
		<div class="c-section">
			<h3>Reading Style</h3>
			<div class="c-settings">
				<a href="http://www.themehats.com/themes/jango/" class="c-setting_font-style btn btn-sm c-btn-square c-btn-border-1x c-btn-white c-btn-sbold c-btn-uppercase ">LTR</a>
				<a href="http://www.themehats.com/themes/jango/rtl/" class="c-setting_font-style btn btn-sm c-btn-square c-btn-border-1x c-btn-white c-btn-sbold c-btn-uppercase active">RTL</a>
			</div>
		</div>
	</div>
</nav>
	<!-- END: LAYOUT/SIDEBARS/QUICK-SIDEBAR -->





	<!-- BEGIN: PAGE CONTAINER -->
	<div class="c-layout-page">
		<!-- BEGIN: LAYOUT/BREADCRUMBS/BREADCRUMBS-2 -->
<div class="c-layout-breadcrumbs-1 c-subtitle c-fonts-uppercase c-fonts-bold c-bordered c-bordered-both">
	<div class="container">
		<div class="c-page-title c-pull-left">
			<h3 class="c-font-uppercase c-font-sbold">Customer Login/Register</h3>
			<h4 class="">Page Sub Title Goes Here</h4>
		</div>
		<ul class="c-page-breadcrumbs c-theme-nav c-pull-right c-fonts-regular">
															<li><a href="shop-customer-account.html">Customer Login/Register</a></li>
			<li>/</li>
															<li class="c-state_active">Jango Components</li>

		</ul>
	</div>
</div>
<!-- END: LAYOUT/BREADCRUMBS/BREADCRUMBS-2 -->



<!-- BEGIN: PAGE CONTENT -->
<!-- BEGIN: CONTENT/SHOPS/SHOP-LOGIN-REGISTER-1 -->
<div class="c-content-box c-size-md c-bg-white">
	<div class="container">
		<div class="c-shop-login-register-1">
			<div class="row">
				<div class="col-md-6">
					<div class="panel panel-default c-panel">
						<div class="panel-body c-panel-body">
							<form class="c-form-login" id="signInForm">
								{{ csrf_field() }}
								<div class="form-group has-feedback">
									<input type="text" name="username" class="form-control c-square c-theme input-lg" placeholder="Username">
									<span class="glyphicon glyphicon-user form-control-feedback c-font-grey"></span>
								</div>
								<div class="form-group has-feedback">
									<input type="password" name="password" class="form-control c-square c-theme input-lg" placeholder="Password">
									<span class="glyphicon glyphicon-lock form-control-feedback c-font-grey"></span>
								</div>
								<div class="row c-margin-t-40">
									<div class="col-xs-8">
										{{--<div class="c-checkbox">--}}
											{{--<input type="checkbox" id="checkbox1-77" class="c-check">--}}
											{{--<label for="checkbox1-77"> <span class="inc"></span>--}}
												{{--<span class="check"></span> <span class="box"></span> Remember me--}}
											{{--</label>--}}
										{{--</div>--}}
									</div>
									<div class="col-xs-4">
										<button type="submit" class="pull-right btn btn-lg c-theme-btn c-btn-square c-btn-uppercase c-btn-bold">Login</button>
									</div>
								</div>
							</form>
							<div class="c-margin-fix" style="margin-top: 20px;">
								<div class="c-checkbox c-toggle-hide" data-object-selector="c-form-forgetMyPassword" data-animation-speed="600">
									<input type="checkbox" id="checkbox6-445" class="c-check">
									<label for="checkbox6-445"> <span class="inc"></span> <span class="check"></span>
										<span class="box"></span> Forget My Password! </label>
								</div>
							</div>
							<form class="c-form-forgetMyPassword c-margin-t-20" id="forgetPasswordForm">
								{{ csrf_field() }}
								<div class="form-group has-feedback">
									<input type="text" name="username" class="form-control c-square c-theme input-lg" placeholder="Username">
									<span class="glyphicon glyphicon-user form-control-feedback c-font-grey"></span>
								</div>
								<div class="form-group c-margin-t-40">
									<button type="submit" class="btn btn-lg c-theme-btn c-btn-square c-btn-uppercase c-btn-bold">Reset Password</button>
								</div>
							</form>
						</div>
					</div>
				</div>
				<div class="col-md-6">
					<div class="panel panel-default c-panel">
						<div class="panel-body c-panel-body">
							<div class="c-content-title-1">
								<h3 class="c-left"><i class="icon-user"></i> Don't have an account yet?</h3>
								<div class="c-line-left c-theme-bg"></div>
								<p>Join us and enjoy shopping online today.</p>
							</div>
							<div class="c-margin-fix">
								<div class="c-checkbox c-toggle-hide" data-object-selector="c-form-register" data-animation-speed="600">
									<input type="checkbox" id="checkbox6-444" class="c-check">
									<label for="checkbox6-444"> <span class="inc"></span> <span class="check"></span>
										<span class="box"></span> Register Now! </label>
								</div>
							</div>
							<form class="c-form-register c-margin-t-20" id="registerForm">
                                {{ csrf_field() }}
								{{--<div class="form-group">--}}
									{{--<label class="control-label">Country</label>--}}
									{{--<select class="form-control c-square c-theme">--}}
										{{--<option value="1">Malaysia</option>--}}
										{{--<option value="2">Singapore</option>--}}
										{{--<option value="3">Indonesia</option>--}}
										{{--<option value="4">Thailand</option>--}}
										{{--<option value="5">China</option>--}}
									{{--</select>--}}
								{{--</div>--}}
								<div class="form-group">
									<div class="row">
										<div class="col-md-12">
											<label class="control-label">User Name</label>
											<input type="text" name="username" class="form-control c-square c-theme" placeholder="User Name">
										</div>
										<div class="col-md-6">
											<label class="control-label">First Name</label>
											<input type="text" name="firstName" class="form-control c-square c-theme" placeholder="First Name">
										</div>
										<div class="col-md-6">
											<label class="control-label">Last Name</label>
											<input type="text" name="lastName" class="form-control c-square c-theme" placeholder="Last Name">
										</div>
									</div>
								</div>
								<div class="form-group">
									<label class="control-label">Born Date</label>
									<input type="date" name="bornDate" class="form-control c-square c-theme" placeholder="yyyy-mm-dd">
								</div>
								{{--<div class="form-group">--}}
									{{--<label class="control-label">Address</label>--}}
									{{--<input type="text" class="form-control c-square c-theme" placeholder="Street Address">--}}
								{{--</div>--}}
								{{--<div class="form-group">--}}
									{{--<input type="text" class="form-control c-square c-theme" placeholder="Apartment, suite, unit etc. (optional)">--}}
								{{--</div>--}}
								{{--<div class="form-group">--}}
									{{--<label class="control-label">Town / City</label>--}}
									{{--<input type="text" class="form-control c-square c-theme" placeholder="Town / City">--}}
								{{--</div>--}}
								{{--<div class="row">--}}
									{{--<div class="form-group col-md-6">--}}
										{{--<label class="control-label">State / County</label>--}}
										{{--<select class="form-control c-square c-theme">--}}
											{{--<option value="0">Select an option...</option>--}}
											{{--<option value="1">Malaysia</option>--}}
											{{--<option value="2">Singapore</option>--}}
											{{--<option value="3">Indonesia</option>--}}
											{{--<option value="4">Thailand</option>--}}
											{{--<option value="5">China</option>--}}
										{{--</select>--}}
									{{--</div>--}}
									{{--<div class="col-md-6">--}}
										{{--<label class="control-label">Postcode / Zip</label>--}}
										{{--<input type="text" class="form-control c-square c-theme" placeholder="Postcode / Zip">--}}
									{{--</div>--}}
								{{--</div>--}}
								<div class="row">
									<div class="form-group col-md-6">
										<label class="control-label">Email Address</label>
										<input type="email" name="email" class="form-control c-square c-theme" placeholder="Email Address">
									</div>
									<div class="col-md-6">
										<label class="control-label">Phone</label>
										<input type="tel" name="phone" class="form-control c-square c-theme" placeholder="Phone">
									</div>
								</div>
								<div class="form-group">
									<label class="control-label">Account Password</label>
									<input type="password" name="password" class="form-control c-square c-theme" placeholder="Password">
								</div>
								<div class="form-group c-margin-t-40">
									<button type="submit" class="btn btn-lg c-theme-btn c-btn-square c-btn-uppercase c-btn-bold">Register</button>
								</div>
								<div class="c-margin-fix" style="margin-top: 20px;">
									<div class="c-checkbox c-toggle-hide" data-object-selector="c-form-verifyAccount1" data-animation-speed="600">
										<input type="checkbox" id="checkbox6-446" class="c-check" hidden>
										<label for="checkbox6-446"> <span class="inc"></span> <span class="check"></span>
										<span class="box"></span> or you have the verification code! </label>
									</div>
								</div>
							</form>
							<div class="c-margin-fix" style="margin-top: 20px;">
								<div class="c-checkbox c-toggle-hide" data-object-selector="c-form-verifyAccount" data-animation-speed="600">
									<input type="checkbox" id="checkbox6-446" class="c-check" hidden>
									{{--<label for="checkbox6-446"> <span class="inc"></span> <span class="check"></span>--}}
										{{--<span class="box"></span> Forget My Password! </label>--}}
								</div>
							</div>

							<form class="c-form-verifyAccount1 c-margin-t-20" id="verifyAccountForm">
								{{ csrf_field() }}
								<label>Verify your accountyou! </label>
								<div class="form-group">
									<label class="control-label">Your username</label>
									<input type="text" id="verificationCode_username" name="username" class="form-control c-square c-theme" placeholder="Username">
									<span class="glyphicon glyphicon-user form-control-feedback c-font-grey"></span>
								</div>
								<div class="form-group">
									<label class="control-label">Verification Code</label>
									<input type="tel" name="verificationcode" class="form-control c-square c-theme" placeholder="Verification Code">
								</div>
								<div class="form-group c-margin-t-40">
									<button type="submit" class="btn btn-lg c-theme-btn c-btn-square c-btn-uppercase c-btn-bold">Verify</button>
								</div>
							</form>

							<form class="c-form-verifyAccount c-margin-t-20" id="verifyAccountForm">
								{{ csrf_field() }}
								<label>Verify your accountyou! </label>
								<div class="form-group">
									<label class="control-label">Your username</label>
									<input type="text" name="username" class="form-control c-square c-theme" placeholder="Username">
									<span class="glyphicon glyphicon-user form-control-feedback c-font-grey"></span>
								</div>
								<div class="form-group">
									<label class="control-label">Verification Code</label>
									<input type="tel" name="verificationcode" class="form-control c-square c-theme" placeholder="Verification Code">
								</div>
								<div class="form-group c-margin-t-40">
									<button type="submit" class="btn btn-lg c-theme-btn c-btn-square c-btn-uppercase c-btn-bold">Verify</button>
								</div>
							</form>
						</div>
					</div>
				</div>
			</div>
			<div class="row">
				<div class="col-md-12">
					<div class="list-unstyled c-bs-grid-small-space">
						<div class="row">
							<div class="col-md-4 col-sm-4 c-margin-t-10">
								<a class="btn btn-block btn-social c-btn-square c-btn-uppercase btn-md btn-twitter">
									<i class="fa fa-twitter"></i> Sign in with Twitter
								</a>
							</div>
							<div class="col-md-4 col-sm-4 c-margin-t-10">
								<a class="btn btn-block btn-social c-btn-square c-btn-uppercase btn-md btn-facebook">
									<i class="fa fa-facebook"></i> Sign in with Facebook
								</a>
							</div>
							{{--<div class="col-md-4 col-sm-4 c-margin-t-10">
								<a class="btn btn-block btn-social c-btn-square c-btn-uppercase btn-md btn-google">
									<i class="fa fa-google-plus"></i> Sign in with Google
								</a>
							</div>--}}
                            <div class="g-signin2" data-onsuccess="onSignIn"></div>
                            <form  id ="form_test">
                                {{csrf_field()}}



                                    <input id="em" type="text" name="email" value="" hidden>
                                    <input id="name" type="text" name="name" value="" hidden>
                                    <input id="Fname" type="text" name="Fname" value="" hidden>
                                    <input id="Lname" type="text" name="Lname" value="" hidden>

                            </form>


                            <a href="#" onclick="signOut();">Sign out</a>


						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<!-- END: CONTENT/SHOPS/SHOP-LOGIN-REGISTER-1 -->

		<!-- END: PAGE CONTENT -->
	</div>
	<!-- END: PAGE CONTAINER -->

	<!-- BEGIN: LAYOUT/FOOTERS/FOOTER-6 -->

	<!-- BEGIN: LAYOUT/BASE/BOTTOM -->
    <!-- BEGIN: CORE PLUGINS -->

</body>

@endsection



@section('script')


    <script>
      function onSignIn(googleUser) {
            var profile = googleUser.getBasicProfile();
           console.log('ID: ' + profile.getId()); // Do not send to your backend! Use an ID token instead.
            console.log('Name: ' + profile.getName());
            console.log('Image URL: ' + profile.getImageUrl());
            console.log('Email: ' + profile.getEmail()); // This is null if the 'email' scope is not present.




          let name = profile.getName();
          let email = profile.getEmail();

          $('#em').val(email);

          $('#form_test').submit();







        }
    </script>

    <script>
        function signOut() {
            var auth2 = gapi.auth2.getAuthInstance();
         /*   auth2.signOut().then(function () {
                console.log('User signed out.');
            });*/
        }




    </script>

    <script>
        $(document).ready(function (){
            $('#form_test').submit(function (e) {
                console.log('submiiiit');
                var formData = new FormData($(this)[0]);
                e.preventDefault();
                e.stopPropagation();
                /*
                            let _token = $('meta[name="csrf-token"]').attr('content');

                            console.log('csrf-token : ' + _token);

                 */
                $('#How-It-Works').append('<div class="loading">Loading&#8230;</div>');
                $.ajax({
                    type: "POST",
                    url: '{{route('signInWithGoogle')}}',
                    data: formData,

                    // mimeType: 'application/json',
                    processData: false,
                    contentType: false,
                    success: function ( res ) {
                        console.log('ajax done');
                        window.location.href = '/';

                        /*
                                                              res = JSON.parse(res);
                        */



                        /*    if(res.code==200){
                                Swal.fire({
                                    icon: 'success',
                                    title: 'تم تسجيل الدخول بنجاح',
                                    text: 'مرحبا',
                                    confirmButtonText: "نعم",
                                }).then(function() {
                                    window.location.href = '/admin';
                                });
                            }
                            else if(res.code==100){
                                Swal.fire({
                                    icon: 'success',
                                    title: 'تم تسجيل الدخول بنجاح',
                                    text: 'مرحبا',
                                    confirmButtonText: "نعم",
                                }).then(function() {
                                    window.location.href = '/user';
                                });
                            }
                            else if(res.code==-1){
                                Swal.fire({
                                    icon: 'error',
                                    title: 'عذرا',
                                    text: 'إنتظر تأكيد المدير لحسابك',
                                    confirmButtonText: "نعم",
                                }).then(function() {
                                    window.location.href = '/';
                                });
                            }
                            else {
                                Swal.fire({
                                    icon: 'error',
                                    title: 'عذرا',
                                    text: 'إسم مستخدم او كلمة سر خاطئة',
                                    confirmButtonText: "نعم",
                                }).then(function() {
                                    window.location.href = '/signin';
                                });
                            }*/


                    },
                    error: function ( res , response ) {
                        $( ".loading" ).remove();
                    },

                });



            });

            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
        });
    </script>





	<script>

        $('#signInForm').submit(function (e) {

            var formData = new FormData($(this)[0]);
            e.preventDefault();
            e.stopPropagation();

            $('#How-It-Works').append('<div class="loading">Loading&#8230;</div>');
            $.ajax({


                type: "POST",
                url: '{{route('login')}}',
                data: formData,
                // mimeType: 'application/json',
                processData: false,
                contentType: false,
                success: function (res) {

                    if(res.status == 201) {
                        Swal.fire({
                            icon: 'success',
                            title: 'success',
                            text: 'sign in successfuly, thanks for using our services',
                        }).then(function() {
                            window.location = "/";
                            // save user object to localstorage
                            localStorage.setItem("DamaStore_userObject", JSON.stringify(res.data));
                        });

                    }
                    if(res.status == 400) {
                        // console.log(res.data.validator);
                        Swal.fire({
                            icon: 'error',
                            title: 'Oops...',
                            text: res.data.validator,
                        });
                    }

                },
                error: function (res, response) {
                    $( ".loading" ).remove();
                },
            });
        });

        $('#verifyAccountForm').submit(function (e) {

            var formData = new FormData($(this)[0]);
            e.preventDefault();
            e.stopPropagation();

            $('#How-It-Works').append('<div class="loading">Loading&#8230;</div>');
            $.ajax({


                type: "POST",
                url: '{{route('verifyaccount')}}',
                data: formData,
                // mimeType: 'application/json',
                processData: false,
                contentType: false,
                success: function (res) {

                    if(res.status == 200) {
                        Swal.fire({
                            icon: 'success',
                            title: 'success',
                            text: 'you can sign in now',
                        }).then(function() {
                            $('#checkbox6-446').click();
                        });
                    }
                    if(res.status == 500) {
                        Swal.fire({
                            icon: 'error',
                            title: 'Oops...',
                            text: res.msg,
                        });
                    }

                },
                error: function (res, response) {
                    $( ".loading" ).remove();
                },
            });
        });

        $('#registerForm').submit(function (e) {

            var formData = new FormData($(this)[0]);
            e.preventDefault();
            e.stopPropagation();

            $('#How-It-Works').append('<div class="loading">Loading&#8230;</div>');
            $.ajax({

                type: "POST",
                url: '{{route('register')}}',
                data: formData,
                // mimeType: 'application/json',
                processData: false,
                contentType: false,
                success: function (res) {
                    if(res.status == 201) {
                        Swal.fire({
                            icon: 'success',
                            title: 'success',
                            text: 'check your mail to verify your account',
                        }).then(function() {
                            // window.location = "/";
                            $('#verificationCode_username').val(formData.get('username'));
                            $('#checkbox6-444').click();
                            $('#checkbox6-446').click();
                        });

                    }
                    if(res.status == 400) {
                        // console.log(res.data.validator);
                        Swal.fire({
                            icon: 'error',
                            title: 'Oops...',
                            text: res.data.validator,
                        });
                    }

                },
                error: function (res, response) {
                    $( ".loading" ).remove();
                },
            });


        });


	</script>

@endsection



