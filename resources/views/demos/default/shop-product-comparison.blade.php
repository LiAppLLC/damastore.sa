@extends('Welcome.home')


@section('content')

    <?php
    $locale = App::getLocale();
    ?>


<body class="c-layout-header-fixed c-layout-header-mobile-fixed c-layout-header-topbar c-layout-header-topbar-collapse">


	<!-- BEGIN: CONTENT/USER/FORGET-PASSWORD-FORM -->
<div class="modal fade c-content-login-form" id="forget-password-form" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content c-square">
            <div class="modal-header c-no-border">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            </div>
            <div class="modal-body">
                <h3 class="c-font-24 c-font-sbold">Password Recovery</h3>
                <p>To recover your password please fill in your email address</p>
                <form>
                    <div class="form-group">
                        <label for="forget-email" class="hide">Email</label>
                        <input type="email" class="form-control input-lg c-square" id="forget-email" placeholder="Email">
                    </div>
                    <div class="form-group">
                        <button type="submit" class="btn c-theme-btn btn-md c-btn-uppercase c-btn-bold c-btn-square c-btn-login">Submit</button>
                        <a href="javascript:;" class="c-btn-forgot" data-toggle="modal" data-target="#login-form" data-dismiss="modal">Back To Login</a>
                    </div>
                </form>
            </div>
            <div class="modal-footer c-no-border">
                <span class="c-text-account">Don't Have An Account Yet ?</span>
                <a href="javascript:;" data-toggle="modal" data-target="#signup-form" data-dismiss="modal" class="btn c-btn-dark-1 btn c-btn-uppercase c-btn-bold c-btn-slim c-btn-border-2x c-btn-square c-btn-signup">Signup!</a>
            </div>
        </div>
    </div>
</div><!-- END: CONTENT/USER/FORGET-PASSWORD-FORM -->
	<!-- BEGIN: CONTENT/USER/SIGNUP-FORM -->
<div class="modal fade c-content-login-form" id="signup-form" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content c-square">
            <div class="modal-header c-no-border">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            </div>
            <div class="modal-body">
                <h3 class="c-font-24 c-font-sbold">Create An Account</h3>
                <p>Please fill in below form to create an account with us</p>
                <form>
                    <div class="form-group">
                        <label for="signup-email" class="hide">Email</label>
                        <input type="email" class="form-control input-lg c-square" id="signup-email" placeholder="Email">
                    </div>
                    <div class="form-group">
                        <label for="signup-username" class="hide">Username</label>
                        <input type="email" class="form-control input-lg c-square" id="signup-username" placeholder="Username">
                    </div>
                    <div class="form-group">
                        <label for="signup-fullname" class="hide">Fullname</label>
                        <input type="email" class="form-control input-lg c-square" id="signup-fullname" placeholder="Fullname">
                    </div>
                    <div class="form-group">
                        <label for="signup-country" class="hide">Country</label>
                        <select class="form-control input-lg c-square" id="signup-country">
                            <option value="1">Country</option>
                        </select>
                    </div>
                    <div class="form-group">
                        <button type="submit" class="btn c-theme-btn btn-md c-btn-uppercase c-btn-bold c-btn-square c-btn-login">Signup</button>
                        <a href="javascript:;" class="c-btn-forgot" data-toggle="modal" data-target="#login-form" data-dismiss="modal">Back To Login</a>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div><!-- END: CONTENT/USER/SIGNUP-FORM -->
	<!-- BEGIN: CONTENT/USER/LOGIN-FORM -->
<div class="modal fade c-content-login-form" id="login-form" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content c-square">
            <div class="modal-header c-no-border">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            </div>
            <div class="modal-body">
                <h3 class="c-font-24 c-font-sbold">Good Afternoon!</h3>
                <p>Let's make today a great day!</p>
                <form>
                    <div class="form-group">
                        <label for="login-email" class="hide">Email</label>
                        <input type="email" class="form-control input-lg c-square" id="login-email" placeholder="Email">
                    </div>
                    <div class="form-group">
                        <label for="login-password" class="hide">Password</label>
                        <input type="password" class="form-control input-lg c-square" id="login-password" placeholder="Password">
                    </div>
                    <div class="form-group">
                        <div class="c-checkbox">
                            <input type="checkbox" id="login-rememberme" class="c-check">
                            <label for="login-rememberme" class="c-font-thin c-font-17">
                                <span></span>
                                <span class="check"></span>
                                <span class="box"></span>
                                Remember Me
                            </label>
                        </div>
                    </div>
                    <div class="form-group">
                        <button type="submit" class="btn c-theme-btn btn-md c-btn-uppercase c-btn-bold c-btn-square c-btn-login">Login</button>
                        <a href="javascript:;" data-toggle="modal" data-target="#forget-password-form" data-dismiss="modal" class="c-btn-forgot">Forgot Your Password ?</a>
                    </div>
                    <div class="clearfix">
                        <div class="c-content-divider c-divider-sm c-icon-bg c-bg-grey c-margin-b-20">
                            <span>or signup with</span>
                        </div>
                        <ul class="c-content-list-adjusted">
                            <li>
                                <a class="btn btn-block c-btn-square btn-social btn-twitter">
                                  <i class="fa fa-twitter"></i>
                                  Twitter
                                </a>
                            </li>
                            <li>
                                <a class="btn btn-block c-btn-square btn-social btn-facebook">
                                  <i class="fa fa-facebook"></i>
                                  Facebook
                                </a>
                            </li>
                            <li>
                                <a class="btn btn-block c-btn-square btn-social btn-google">
                                  <i class="fa fa-google"></i>
                                  Google
                                </a>
                            </li>
                        </ul>
                    </div>
                </form>
            </div>
            <div class="modal-footer c-no-border">
                <span class="c-text-account">Don't Have An Account Yet ?</span>
                <a href="javascript:;" data-toggle="modal" data-target="#signup-form" data-dismiss="modal" class="btn c-btn-dark-1 btn c-btn-uppercase c-btn-bold c-btn-slim c-btn-border-2x c-btn-square c-btn-signup">Signup!</a>
            </div>
        </div>
    </div>
</div><!-- END: CONTENT/USER/LOGIN-FORM -->

	<!-- BEGIN: LAYOUT/SIDEBARS/QUICK-SIDEBAR -->
<nav class="c-layout-quick-sidebar">
	<div class="c-header">
		<button type="button" class="c-link c-close">
		<i class="icon-login"></i>
		</button>
	</div>
	<div class="c-content">
		<div class="c-section">
			<h3>JANGO DEMOS</h3>
			<div class="c-settings c-demos c-bs-grid-reset-space">
				<div class="row">
					<div class="col-md-12">
						<a href="../default/index.html" class="c-demo-container c-demo-img-lg">
							<div class="c-demo-thumb active">
								<img src="../../assets/base/img/content/quick-sidebar/default.jpg" class="c-demo-thumb-img"/>
							</div>
						</a>
					</div>
				</div>
				<div class="row">
					<div class="col-md-6">
						<a href="../corporate_1/index.html" class="c-demo-container">
							<div class="c-demo-thumb  c-thumb-left">
								<img src="../../assets/base/img/content/quick-sidebar/corporate_1.jpg" class="c-demo-thumb-img"/>
							</div>
						</a>
					</div>
					<div class="col-md-6">
						<a href="../agency_1/index.html" class="c-demo-container">
							<div class="c-demo-thumb  c-thumb-right">
								<img src="../../assets/base/img/content/quick-sidebar/corporate_1-onepage.jpg" class="c-demo-thumb-img"/>
							</div>
						</a>
					</div>
				</div>
			</div>
		</div>
		<div class="c-section">
			<h3>Theme Colors</h3>
			<div class="c-settings">

				<span class="c-color c-default c-active" data-color="default"></span>

				<span class="c-color c-green1" data-color="green1"></span>
				<span class="c-color c-green2" data-color="green2"></span>
				<span class="c-color c-green3" data-color="green3"></span>

				<span class="c-color c-yellow1" data-color="yellow1"></span>
				<span class="c-color c-yellow2" data-color="yellow2"></span>
				<span class="c-color c-yellow3" data-color="yellow3"></span>

				<span class="c-color c-red1" data-color="red1"></span>
				<span class="c-color c-red2" data-color="red2"></span>
				<span class="c-color c-red3" data-color="red3"></span>

				<span class="c-color c-purple1" data-color="purple1"></span>
				<span class="c-color c-purple2" data-color="purple2"></span>
				<span class="c-color c-purple3" data-color="purple3"></span>

				<span class="c-color c-blue1" data-color="blue1"></span>
				<span class="c-color c-blue2" data-color="blue2"></span>
				<span class="c-color c-blue3" data-color="blue3"></span>

				<span class="c-color c-brown1" data-color="brown1"></span>
				<span class="c-color c-brown2" data-color="brown2"></span>
				<span class="c-color c-brown3" data-color="brown3"></span>

				<span class="c-color c-dark1" data-color="dark1"></span>
				<span class="c-color c-dark2" data-color="dark2"></span>
				<span class="c-color c-dark3" data-color="dark3"></span>
			</div>
		</div>
		<div class="c-section">
			<h3>Header Type</h3>
			<div class="c-settings">
				<input type="button" class="c-setting_header-type btn btn-sm c-btn-square c-btn-border-1x c-btn-white c-btn-sbold c-btn-uppercase active" data-value="boxed" value="boxed"/>
				<input type="button" class="c-setting_header-type btn btn-sm c-btn-square c-btn-border-1x c-btn-white c-btn-sbold c-btn-uppercase" data-value="fluid" value="fluid"/>
			</div>
		</div>
		<div class="c-section">
			<h3>Header Mode</h3>
			<div class="c-settings">
				<input type="button" class="c-setting_header-mode btn btn-sm c-btn-square c-btn-border-1x c-btn-white c-btn-sbold c-btn-uppercase active" data-value="fixed" value="fixed"/>
				<input type="button" class="c-setting_header-mode btn btn-sm c-btn-square c-btn-border-1x c-btn-white c-btn-sbold c-btn-uppercase" data-value="static" value="static"/>
			</div>
		</div>
		<div class="c-section">
			<h3>Mega Menu Style</h3>
			<div class="c-settings">
				<input type="button" class="c-setting_megamenu-style btn btn-sm c-btn-square c-btn-border-1x c-btn-white c-btn-sbold c-btn-uppercase active" data-value="dark" value="dark"/>
				<input type="button" class="c-setting_megamenu-style btn btn-sm c-btn-square c-btn-border-1x c-btn-white c-btn-sbold c-btn-uppercase" data-value="light" value="light"/>
			</div>
		</div>
		<div class="c-section">
			<h3>Font Style</h3>
			<div class="c-settings">
				<input type="button" class="c-setting_font-style btn btn-sm c-btn-square c-btn-border-1x c-btn-white c-btn-sbold c-btn-uppercase active" data-value="default" value="default"/>
				<input type="button" class="c-setting_font-style btn btn-sm c-btn-square c-btn-border-1x c-btn-white c-btn-sbold c-btn-uppercase" data-value="light" value="light"/>
			</div>
		</div>
		<div class="c-section">
			<h3>Reading Style</h3>
			<div class="c-settings">
				<a href="http://www.themehats.com/themes/jango/" class="c-setting_font-style btn btn-sm c-btn-square c-btn-border-1x c-btn-white c-btn-sbold c-btn-uppercase ">LTR</a>
				<a href="http://www.themehats.com/themes/jango/rtl/" class="c-setting_font-style btn btn-sm c-btn-square c-btn-border-1x c-btn-white c-btn-sbold c-btn-uppercase active">RTL</a>
			</div>
		</div>
	</div>
</nav><!-- END: LAYOUT/SIDEBARS/QUICK-SIDEBAR -->

	<!-- BEGIN: PAGE CONTAINER -->
	<div class="c-layout-page">
		<!-- BEGIN: LAYOUT/BREADCRUMBS/BREADCRUMBS-2 -->
<div class="c-layout-breadcrumbs-1 c-subtitle c-fonts-uppercase c-fonts-bold c-bordered c-bordered-both">
	<div class="container">
		<div class="c-page-title c-pull-left">
			<h3 class="c-font-uppercase c-font-sbold">Product Comparison</h3>
			<h4 class="">Page Sub Title Goes Here</h4>
		</div>
		<ul class="c-page-breadcrumbs c-theme-nav c-pull-right c-fonts-regular">
															<li><a href="shop-product-comparison.html">Product Comparison</a></li>
			<li>/</li>
															<li class="c-state_active">Jango Components</li>

		</ul>
	</div>
</div><!-- END: LAYOUT/BREADCRUMBS/BREADCRUMBS-2 -->
		<div class="container">
			<div class="c-layout-sidebar-menu c-theme ">
			<!-- BEGIN: LAYOUT/SIDEBARS/SHOP-SIDEBAR-MENU-2 -->
<div class="c-sidebar-menu-toggler">
	<h3 class="c-title c-font-uppercase c-font-bold">Navigation</h3>
	<a href="javascript:;" class="c-content-toggler" data-toggle="collapse" data-target="#sidebar-menu-1">
	<span class="c-line"></span>
	<span class="c-line"></span>
	<span class="c-line"></span>
	</a>
</div>

<!-- BEGIN: CONTENT/SHOPS/SHOP-FILTER-SEARCH-1 -->
<ul class="c-shop-filter-search-1 list-unstyled">
	<li>
		<label class="control-label c-font-uppercase c-font-bold">Category</label>
		<select class="form-control c-square c-theme">
			<option value="0">All Categories</option>
			<option value="1">Art</option>
			<option value="2">Baby</option>
			<option value="3">Books</option>
			<option value="4">Business &amp; Industrial</option>
			<option value="5">Cameras &amp; Photo</option>
			<option value="6">Cell Phones &amp; Accessories</option>
			<option value="7">Clothing, Shoes &amp; Accessories</option>
			<option value="8">Coins &amp; Paper Money</option>
			<option value="9">Collectibles</option>
			<option value="10">Computers/Tablets &amp; Networking</option>
		</select>
	</li>
	<li>
		<label class="control-label c-font-uppercase c-font-bold">Availability</label>
		<div class="c-checkbox">
			<input type="checkbox" id="checkbox-sidebar-3-1" class="c-check"> <label for="checkbox-sidebar-3-1">
				<span class="inc"></span> <span class="check"></span> <span class="box"></span>
				<p>Not Available (3)</p>
			</label>
		</div>
		<div class="c-checkbox c-checkbox-height">
			<input type="checkbox" id="checkbox-sidebar-3-2" class="c-check"> <label for="checkbox-sidebar-3-2">
				<span class="inc"></span> <span class="check"></span> <span class="box"></span>
				<p>In Stock (23)</p>
			</label>
		</div>
	</li>
	<li>
		<label class="control-label c-font-uppercase c-font-bold">Price Range</label>
		<div class="c-price-range-box input-group">
			<div class="c-price input-group col-md-6 pull-left">
				<span class="input-group-addon c-square c-theme">$</span>
				<input type="text" class="form-control c-square c-theme" placeholder="from">
			</div>
			<div class="c-price input-group col-md-6 pull-left">
				<span class="input-group-addon c-square c-theme">$</span>
				<input type="text" class="form-control c-square c-theme" placeholder="to">
			</div>
		</div>
	</li>
	<li>
		<label class="control-label c-font-uppercase c-font-bold">Price Range Slider Color</label>
		<p>Price Range: $1 - $ 500</p>
		<div class="c-price-range-slider c-theme-1 input-group">
			<input type="text" class="c-price-slider" value="" data-slider-min="1" data-slider-max="500" data-slider-step="1" data-slider-value="[100,250]">
		</div>
	</li>
	<li>
		<label class="control-label c-font-uppercase c-font-bold">Price Range Slider Color</label>
		<p>Price Range: $1 - $ 500</p>
		<div class="c-price-range-slider c-theme-2 input-group">
			<input type="text" class="c-price-slider" value="" data-slider-handle="square" data-slider-min="1" data-slider-max="500" data-slider-step="1" data-slider-value="[100,250]">
		</div>
	</li>
	<li>
		<label class="control-label c-font-uppercase c-font-bold">Price Group</label>
		<div class="input-group">
			<div class="c-checkbox">
				<input type="checkbox" id="checkbox-sidebar-1-1" class="c-check"> <label for="checkbox-sidebar-1-1">
					<span class="inc"></span> <span class="check"></span> <span class="box"></span> $0 - $10 (15)
				</label>
			</div>
			<div class="c-checkbox">
				<input type="checkbox" id="checkbox-sidebar-1-2" class="c-check"> <label for="checkbox-sidebar-1-2">
					<span class="inc"></span> <span class="check"></span> <span class="box"></span> $11 - $20 (17)
				</label>
			</div>
			<div class="c-checkbox">
				<input type="checkbox" id="checkbox-sidebar-1-3" class="c-check"> <label for="checkbox-sidebar-1-3">
					<span class="inc"></span> <span class="check"></span> <span class="box"></span> $21 - $30 (23)
				</label>
			</div>
			<div class="c-checkbox c-checkbox-height">
				<input type="checkbox" id="checkbox-sidebar-1-4" class="c-check"> <label for="checkbox-sidebar-1-4">
					<span class="inc"></span> <span class="check"></span> <span class="box"></span> $31 - $40 (19)
				</label>
			</div>
		</div>
	</li>
	<li class="c-margin-b-40">
		<label class="control-label c-font-uppercase c-font-bold">Review Rating</label>
		<div class="c-checkbox">
			<input type="checkbox" id="checkbox-sidebar-2-1" class="c-check"> <label for="checkbox-sidebar-2-1">
				<span class="inc"></span> <span class="check"></span> <span class="box"></span>
				<p class="c-review-star">
					<span class="fa fa-star c-theme-font"></span> <span class="fa fa-star c-theme-font"></span> <span class="fa fa-star c-theme-font"></span>
					<span class="fa fa-star c-theme-font"></span> <span class="fa fa-star c-theme-font"></span> (18)
				</p>
			</label>
		</div>
		<div class="c-checkbox">
			<input type="checkbox" id="checkbox-sidebar-2-2" class="c-check"> <label for="checkbox-sidebar-2-2">
				<span class="inc"></span> <span class="check"></span> <span class="box"></span>
				<p class="c-review-star">
					<span class="fa fa-star c-theme-font"></span> <span class="fa fa-star c-theme-font"></span> <span class="fa fa-star c-theme-font"></span>
					<span class="fa fa-star c-theme-font"></span> (20)
				</p>
			</label>
		</div>
		<div class="c-checkbox">
			<input type="checkbox" id="checkbox-sidebar-2-3" class="c-check"> <label for="checkbox-sidebar-2-3">
				<span class="inc"></span> <span class="check"></span> <span class="box"></span>
				<p class="c-review-star">
					<span class="fa fa-star c-theme-font"></span> <span class="fa fa-star c-theme-font"></span>
					<span class="fa fa-star c-theme-font"></span> (9)
				</p>
			</label>
		</div>
		<div class="c-checkbox">
			<input type="checkbox" id="checkbox-sidebar-2-4" class="c-check"> <label for="checkbox-sidebar-2-4">
				<span class="inc"></span> <span class="check"></span> <span class="box"></span>
				<p class="c-review-star">
					<span class="fa fa-star c-theme-font"></span> <span class="fa fa-star c-theme-font"></span> (4)
				</p>
			</label>
		</div>
		<div class="c-checkbox">
			<input type="checkbox" id="checkbox-sidebar-2-5" class="c-check"> <label for="checkbox-sidebar-2-5">
				<span class="inc"></span> <span class="check"></span> <span class="box"></span>
				<p class="c-review-star">
					<span class="fa fa-star c-theme-font"></span> (1)
				</p>
			</label>
		</div>
		<div class="c-checkbox">
			<input type="checkbox" id="checkbox-sidebar-2-6" class="c-check"> <label for="checkbox-sidebar-2-6">
				<span class="inc"></span> <span class="check"></span> <span class="box"></span>
				<p class="c-review-star">No yet rated (10)</p>
			</label>
		</div>
	</li>
</ul><!-- END: CONTENT/SHOPS/SHOP-FILTER-SEARCH-1 -->

<ul class="c-sidebar-menu collapse " id="sidebar-menu-1">
	<li class="c-dropdown c-active c-open">
		<a href="javascript:;" class="c-toggler">Active Section<span class="c-arrow"></span></a>
		<ul class="c-dropdown-menu">
			<li class="c-active">
				<a href="#">Active Link</a>
			</li>
			<li>
				<a href="#">Example Link</a>
			</li>
			<li>
				<a href="#">Example Link</a>
			</li>
			<li>
				<a href="#">Example Link</a>
			</li>
		</ul>
	</li>
	<li class="c-dropdown">
		<a href="javascript:;" class="c-toggler">Sub Menu Section<span class="c-arrow"></span></a>
		<ul class="c-dropdown-menu">
			<li>
				<a href="#">Example Link</a>
			</li>
			<li class="c-dropdown">
				<a href="javascript:;" class="c-toggler">Sub Menu
				<span class="c-arrow"></span></a>
				<ul class="c-dropdown-menu">
					<li>
						<a href="#">Example Link</a>
					</li>
					<li>
						<a href="#">Example Link</a>
					</li>
					<li>
						<a href="#">Example Link</a>
					</li>
					<li>
						<a href="#">Example Link</a>
					</li>
					<li>
						<a href="#">Example Link</a>
					</li>
				</ul>
			</li>
			<li>
				<a href="#">Example Link</a>
			</li>
			<li class="c-dropdown">
				<a href="javascript:;" class="c-toggler">Sub Menu<span class="c-arrow"></span></a>
				<ul class="c-dropdown-menu">
					<li>
						<a href="#">Example Link</a>
					</li>
					<li>
						<a href="#">Example Link</a>
					</li>
					<li class="c-dropdown">
						<a href="javascript:;" class="c-toggler">Sub Menu<span class="c-arrow"></span></a>
						<ul class="c-dropdown-menu">
							<li>
								<a href="#">Example Link</a>
							</li>
							<li>
								<a href="#">Example Link</a>
							</li>
							<li>
								<a href="#">Example Link</a>
							</li>
							<li>
								<a href="#">Example Link</a>
							</li>
							<li>
								<a href="#">Example Link</a>
							</li>
						</ul>
					</li>
					<li>
						<a href="#">Example Link</a>
					</li>
					<li>
						<a href="#">Example Link</a>
					</li>
				</ul>
			</li>
			<li>
				<a href="#">Example Link</a>
			</li>
		</ul>
	</li>
	<li class="c-dropdown">
		<a href="javascript:;" class="c-toggler">Section With Icons<span class="c-arrow"></span></a>
		<ul class="c-dropdown-menu">
			<li>
				<a href="#"><i class="icon-social-dribbble"></i> Example Link</a>
			</li>
			<li>
				<a href="#"><i class="icon-bell"></i> Example Link</a>
			</li>
			<li>
				<a href="#"><i class="icon-bubbles"></i> Example Link</a>
			</li>
			<li>
				<a href="#"><i class="icon-user"></i> Example Link</a>
			</li>
		</ul>
	</li>
	<li class="c-dropdown">
		<a href="javascript:;" class="c-toggler">Expanded Section<span class="c-arrow"></span></a>
		<ul class="c-dropdown-menu">
			<li>
				<a href="#">Example Link</a>
			</li>
			<li>
				<a href="#">Example Link</a>
			</li>
			<li>
				<a href="#">Example Link</a>
			</li>
			<li>
				<a href="#">Example Link</a>
			</li>
		</ul>
	</li>
	<li class="c-dropdown">
		<a href="javascript:;">Arrow Toggler <span class="c-arrow c-toggler"></span></a>
		<ul class="c-dropdown-menu">
			<li>
				<a href="#">Example Link</a>
			</li>
			<li>
				<a href="#">Example Link</a>
			</li>
			<li>
				<a href="#">Example Link</a>
			</li>
			<li>
				<a href="#">Example Link</a>
			</li>
			<li class="c-dropdown">
				<a href="javascript:;">Sub Menu
				<span class="c-arrow c-toggler"></span></a>
				<ul class="c-dropdown-menu">
					<li>
						<a href="#">Example Link</a>
					</li>
					<li>
						<a href="#">Example Link</a>
					</li>
					<li>
						<a href="#">Example Link</a>
					</li>
					<li>
						<a href="#">Example Link</a>
					</li>
					<li>
						<a href="#">Example Link</a>
					</li>
				</ul>
			</li>
		</ul>
	</li>
</ul><!-- END: LAYOUT/SIDEBARS/SHOP-SIDEBAR-MENU-2 -->
			</div>
			<div class="c-layout-sidebar-content ">
			<!-- BEGIN: PAGE CONTENT -->
			<div class="c-shop-product-compare">
	<div class="c-content-title-1">
	    <h3 class="c-font-uppercase c-font-bold">Product Comparison</h3>
	</div>
	<div class="c-product-compare-content">
		<div class="c-product-data c-compare-products clearfix">
	        <div class="table-wrapper-responsive">
	            <table class="c-product-compare-table" summary="Product Details">
	                <tr>
	                  <td class="c-compare-info c-bg-white">
	                    <p>There are 2 goods in the list.</p>
	                  </td>
	                  <td class="c-compare-item">
	                    <a href="javascript:;"><img src="../../assets/base/img/content/shop/43.jpg" alt="Black Winter Jacket"></a>
	                    <h3><a href="javascript:;">Black Winter Jacket</a></h3>
	                    <strong class="c-compare-price"><span>$</span>47.00</strong>
	                  </td>
	                  <td class="c-compare-item">
	                    <a href="#"><img src="../../assets/base/img/content/shop/44.jpg" alt="Denim Winter Jacket"></a>
	                    <h3><a href="javascript:;">Stylist Denim Winter Jacket</a></h3>
	                    <strong class="c-compare-price"><span>$</span>42.00</strong>
	                  </td>
	                </tr>

	                <tr>
	                  <th colspan="3">
	                    <h2>Product Details</h2>
	                  </th>
	                </tr>
	                <tr>
	                  <td class="c-compare-info">
	                    Material
	                  </td>
	                  <td class="c-compare-item">
	                    Fur & Wool
	                  </td>
	                  <td class="c-compare-item">
	                    Denim & Wool
	                  </td>
	                </tr>
	                <tr>
	                  <td class="c-compare-info">
	                    Dimension
	                  </td>
	                  <td class="c-compare-item">
	                    13.00cm x 0.00cm x 0.00cm
	                  </td>
	                  <td class="c-compare-item">
	                    13.00cm x 0.00cm x 0.00cm
	                  </td>
	                </tr>
	                <tr>
	                  <td class="c-compare-info">
	                    Weight
	                  </td>
	                  <td class="c-compare-item">
	                    110.00g
	                  </td>
	                  <td class="c-compare-item">
	                    110.00g
	                  </td>
	                </tr>

	                <tr>
	                  <th colspan="3">
	                    <h2>Features</h2>
	                  </th>
	                </tr>
	                <tr>
	                  <td class="c-compare-info">
	                    Sleeve Length
	                  </td>
	                  <td class="c-compare-item">
	                    13 cm
	                  </td>
	                  <td class="c-compare-item">
	                    15cm
	                  </td>
	                </tr>
	                <tr>
	                  <td class="c-compare-info">
	                    Stock Availability
	                  </td>
	                  <td class="c-compare-item">
	                    In Stock
	                  </td>
	                  <td class="c-compare-item">
	                    In Stock
	                  </td>
	                </tr>
	                <tr>
	                  <td class="c-compare-info">&nbsp;</td>
	                  <td class="c-compare-item">
	                    <a class="btn btn-primary c-btn c-btn-square c-theme-btn c-btn-md c-margin-b-10" href="javascript:;">Add to cart</a><br>
	                    <a class="btn btn-default c-btn c-btn-square c-theme-btn c-btn-border-1x c-btn-md" href="javascript:;">Delete</a>
	                  </td>
	                  <td class="c-compare-item">
	                    <a class="btn btn-primary c-btn c-btn-square c-theme-btn c-btn-md c-margin-b-10" href="javascript:;">Add to cart</a><br>
	                    <a class="btn btn-default c-btn c-btn-square c-theme-btn c-btn-border-1x c-btn-md" href="javascript:;">Delete</a>
	                  </td>
	                </tr>
	            </table>
	        <p class="c-margin-t-20"><strong>Notice:</strong> Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam sit nonummy nibh euismod tincidunt ut laoreet dolore magna aliquarm erat sit volutpat. Nostrud exerci tation ullamcorper suscipit lobortis nisl aliquip commodo consequat. </p>
	        </div>
	    </div>
	</div>
</div>			<!-- END: PAGE CONTENT -->
			</div>
		</div>
	</div>
	<!-- END: PAGE CONTAINER -->

	<!-- BEGIN: LAYOUT/FOOTERS/FOOTER-6 -->


</body>

