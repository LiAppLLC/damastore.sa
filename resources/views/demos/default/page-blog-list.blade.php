@extends('Welcome.home')


@section('content')

    <?php
    $locale = App::getLocale();
    ?>

<body class="c-layout-header-fixed c-layout-header-mobile-fixed">

	<!-- BEGIN: CONTENT/USER/FORGET-PASSWORD-FORM -->
<div class="modal fade c-content-login-form" id="forget-password-form" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content c-square">
            <div class="modal-header c-no-border">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            </div>
            <div class="modal-body">
                <h3 class="c-font-24 c-font-sbold">Password Recovery</h3>
                <p>To recover your password please fill in your email address</p>
                <form>
                    <div class="form-group">
                        <label for="forget-email" class="hide">Email</label>
                        <input type="email" class="form-control input-lg c-square" id="forget-email" placeholder="Email">
                    </div>
                    <div class="form-group">
                        <button type="submit" class="btn c-theme-btn btn-md c-btn-uppercase c-btn-bold c-btn-square c-btn-login">Submit</button>
                        <a href="javascript:;" class="c-btn-forgot" data-toggle="modal" data-target="#login-form" data-dismiss="modal">Back To Login</a>
                    </div>
                </form>
            </div>
            <div class="modal-footer c-no-border">
                <span class="c-text-account">Don't Have An Account Yet ?</span>
                <a href="javascript:;" data-toggle="modal" data-target="#signup-form" data-dismiss="modal" class="btn c-btn-dark-1 btn c-btn-uppercase c-btn-bold c-btn-slim c-btn-border-2x c-btn-square c-btn-signup">Signup!</a>
            </div>
        </div>
    </div>
</div><!-- END: CONTENT/USER/FORGET-PASSWORD-FORM -->
	<!-- BEGIN: CONTENT/USER/SIGNUP-FORM -->
<div class="modal fade c-content-login-form" id="signup-form" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content c-square">
            <div class="modal-header c-no-border">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            </div>
            <div class="modal-body">
                <h3 class="c-font-24 c-font-sbold">Create An Account</h3>
                <p>Please fill in below form to create an account with us</p>
                <form>
                    <div class="form-group">
                        <label for="signup-email" class="hide">Email</label>
                        <input type="email" class="form-control input-lg c-square" id="signup-email" placeholder="Email">
                    </div>
                    <div class="form-group">
                        <label for="signup-username" class="hide">Username</label>
                        <input type="email" class="form-control input-lg c-square" id="signup-username" placeholder="Username">
                    </div>
                    <div class="form-group">
                        <label for="signup-fullname" class="hide">Fullname</label>
                        <input type="email" class="form-control input-lg c-square" id="signup-fullname" placeholder="Fullname">
                    </div>
                    <div class="form-group">
                        <label for="signup-country" class="hide">Country</label>
                        <select class="form-control input-lg c-square" id="signup-country">
                            <option value="1">Country</option>
                        </select>
                    </div>
                    <div class="form-group">
                        <button type="submit" class="btn c-theme-btn btn-md c-btn-uppercase c-btn-bold c-btn-square c-btn-login">Signup</button>
                        <a href="javascript:;" class="c-btn-forgot" data-toggle="modal" data-target="#login-form" data-dismiss="modal">Back To Login</a>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div><!-- END: CONTENT/USER/SIGNUP-FORM -->
	<!-- BEGIN: CONTENT/USER/LOGIN-FORM -->
<div class="modal fade c-content-login-form" id="login-form" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content c-square">
            <div class="modal-header c-no-border">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            </div>
            <div class="modal-body">
                <h3 class="c-font-24 c-font-sbold">Good Afternoon!</h3>
                <p>Let's make today a great day!</p>
                <form>
                    <div class="form-group">
                        <label for="login-email" class="hide">Email</label>
                        <input type="email" class="form-control input-lg c-square" id="login-email" placeholder="Email">
                    </div>
                    <div class="form-group">
                        <label for="login-password" class="hide">Password</label>
                        <input type="password" class="form-control input-lg c-square" id="login-password" placeholder="Password">
                    </div>
                    <div class="form-group">
                        <div class="c-checkbox">
                            <input type="checkbox" id="login-rememberme" class="c-check">
                            <label for="login-rememberme" class="c-font-thin c-font-17">
                                <span></span>
                                <span class="check"></span>
                                <span class="box"></span>
                                Remember Me
                            </label>
                        </div>
                    </div>
                    <div class="form-group">
                        <button type="submit" class="btn c-theme-btn btn-md c-btn-uppercase c-btn-bold c-btn-square c-btn-login">Login</button>
                        <a href="javascript:;" data-toggle="modal" data-target="#forget-password-form" data-dismiss="modal" class="c-btn-forgot">Forgot Your Password ?</a>
                    </div>
                    <div class="clearfix">
                        <div class="c-content-divider c-divider-sm c-icon-bg c-bg-grey c-margin-b-20">
                            <span>or signup with</span>
                        </div>
                        <ul class="c-content-list-adjusted">
                            <li>
                                <a class="btn btn-block c-btn-square btn-social btn-twitter">
                                  <i class="fa fa-twitter"></i>
                                  Twitter
                                </a>
                            </li>
                            <li>
                                <a class="btn btn-block c-btn-square btn-social btn-facebook">
                                  <i class="fa fa-facebook"></i>
                                  Facebook
                                </a>
                            </li>
                            <li>
                                <a class="btn btn-block c-btn-square btn-social btn-google">
                                  <i class="fa fa-google"></i>
                                  Google
                                </a>
                            </li>
                        </ul>
                    </div>
                </form>
            </div>
            <div class="modal-footer c-no-border">
                <span class="c-text-account">Don't Have An Account Yet ?</span>
                <a href="javascript:;" data-toggle="modal" data-target="#signup-form" data-dismiss="modal" class="btn c-btn-dark-1 btn c-btn-uppercase c-btn-bold c-btn-slim c-btn-border-2x c-btn-square c-btn-signup">Signup!</a>
            </div>
        </div>
    </div>
</div><!-- END: CONTENT/USER/LOGIN-FORM -->

	<!-- BEGIN: LAYOUT/SIDEBARS/QUICK-SIDEBAR -->
<nav class="c-layout-quick-sidebar">
	<div class="c-header">
		<button type="button" class="c-link c-close">
		<i class="icon-login"></i>
		</button>
	</div>
	<div class="c-content">
		<div class="c-section">
			<h3>JANGO DEMOS</h3>
			<div class="c-settings c-demos c-bs-grid-reset-space">
				<div class="row">
					<div class="col-md-12">
						<a href="../default/index.html" class="c-demo-container c-demo-img-lg">
							<div class="c-demo-thumb active">
								<img src="../../assets/base/img/content/quick-sidebar/default.jpg" class="c-demo-thumb-img"/>
							</div>
						</a>
					</div>
				</div>
				<div class="row">
					<div class="col-md-6">
						<a href="../corporate_1/index.html" class="c-demo-container">
							<div class="c-demo-thumb  c-thumb-left">
								<img src="../../assets/base/img/content/quick-sidebar/corporate_1.jpg" class="c-demo-thumb-img"/>
							</div>
						</a>
					</div>
					<div class="col-md-6">
						<a href="../agency_1/index.html" class="c-demo-container">
							<div class="c-demo-thumb  c-thumb-right">
								<img src="../../assets/base/img/content/quick-sidebar/corporate_1-onepage.jpg" class="c-demo-thumb-img"/>
							</div>
						</a>
					</div>
				</div>
			</div>
		</div>
		<div class="c-section">
			<h3>Theme Colors</h3>
			<div class="c-settings">

				<span class="c-color c-default c-active" data-color="default"></span>

				<span class="c-color c-green1" data-color="green1"></span>
				<span class="c-color c-green2" data-color="green2"></span>
				<span class="c-color c-green3" data-color="green3"></span>

				<span class="c-color c-yellow1" data-color="yellow1"></span>
				<span class="c-color c-yellow2" data-color="yellow2"></span>
				<span class="c-color c-yellow3" data-color="yellow3"></span>

				<span class="c-color c-red1" data-color="red1"></span>
				<span class="c-color c-red2" data-color="red2"></span>
				<span class="c-color c-red3" data-color="red3"></span>

				<span class="c-color c-purple1" data-color="purple1"></span>
				<span class="c-color c-purple2" data-color="purple2"></span>
				<span class="c-color c-purple3" data-color="purple3"></span>

				<span class="c-color c-blue1" data-color="blue1"></span>
				<span class="c-color c-blue2" data-color="blue2"></span>
				<span class="c-color c-blue3" data-color="blue3"></span>

				<span class="c-color c-brown1" data-color="brown1"></span>
				<span class="c-color c-brown2" data-color="brown2"></span>
				<span class="c-color c-brown3" data-color="brown3"></span>

				<span class="c-color c-dark1" data-color="dark1"></span>
				<span class="c-color c-dark2" data-color="dark2"></span>
				<span class="c-color c-dark3" data-color="dark3"></span>
			</div>
		</div>
		<div class="c-section">
			<h3>Header Type</h3>
			<div class="c-settings">
				<input type="button" class="c-setting_header-type btn btn-sm c-btn-square c-btn-border-1x c-btn-white c-btn-sbold c-btn-uppercase active" data-value="boxed" value="boxed"/>
				<input type="button" class="c-setting_header-type btn btn-sm c-btn-square c-btn-border-1x c-btn-white c-btn-sbold c-btn-uppercase" data-value="fluid" value="fluid"/>
			</div>
		</div>
		<div class="c-section">
			<h3>Header Mode</h3>
			<div class="c-settings">
				<input type="button" class="c-setting_header-mode btn btn-sm c-btn-square c-btn-border-1x c-btn-white c-btn-sbold c-btn-uppercase active" data-value="fixed" value="fixed"/>
				<input type="button" class="c-setting_header-mode btn btn-sm c-btn-square c-btn-border-1x c-btn-white c-btn-sbold c-btn-uppercase" data-value="static" value="static"/>
			</div>
		</div>
		<div class="c-section">
			<h3>Mega Menu Style</h3>
			<div class="c-settings">
				<input type="button" class="c-setting_megamenu-style btn btn-sm c-btn-square c-btn-border-1x c-btn-white c-btn-sbold c-btn-uppercase active" data-value="dark" value="dark"/>
				<input type="button" class="c-setting_megamenu-style btn btn-sm c-btn-square c-btn-border-1x c-btn-white c-btn-sbold c-btn-uppercase" data-value="light" value="light"/>
			</div>
		</div>
		<div class="c-section">
			<h3>Font Style</h3>
			<div class="c-settings">
				<input type="button" class="c-setting_font-style btn btn-sm c-btn-square c-btn-border-1x c-btn-white c-btn-sbold c-btn-uppercase active" data-value="default" value="default"/>
				<input type="button" class="c-setting_font-style btn btn-sm c-btn-square c-btn-border-1x c-btn-white c-btn-sbold c-btn-uppercase" data-value="light" value="light"/>
			</div>
		</div>
		<div class="c-section">
			<h3>Reading Style</h3>
			<div class="c-settings">
				<a href="http://www.themehats.com/themes/jango/" class="c-setting_font-style btn btn-sm c-btn-square c-btn-border-1x c-btn-white c-btn-sbold c-btn-uppercase ">LTR</a>
				<a href="http://www.themehats.com/themes/jango/rtl/" class="c-setting_font-style btn btn-sm c-btn-square c-btn-border-1x c-btn-white c-btn-sbold c-btn-uppercase active">RTL</a>
			</div>
		</div>
	</div>
</nav><!-- END: LAYOUT/SIDEBARS/QUICK-SIDEBAR -->

	<!-- BEGIN: PAGE CONTAINER -->
	<div class="c-layout-page">
		<!-- BEGIN: LAYOUT/BREADCRUMBS/BREADCRUMBS-1 -->

<div class="c-layout-breadcrumbs-1 c-fonts-uppercase c-fonts-bold c-bordered c-bordered-both">
	<div class="container">
		<div class="c-page-title c-pull-left">
			<h3 class="c-font-uppercase c-font-sbold">Blog List View</h3>
		</div>
		<ul class="c-page-breadcrumbs c-theme-nav c-pull-right c-fonts-regular">
															<li><a href="#">Pages</a></li>
			<li>/</li>
															<li><a href="page-blog-list.html">Blog List View</a></li>
			<li>/</li>
															<li class="c-state_active">Jango Components</li>

		</ul>
	</div>
</div><!-- END: LAYOUT/BREADCRUMBS/BREADCRUMBS-1 -->
		<!-- BEGIN: PAGE CONTENT -->
		<!-- BEGIN: BLOG LISTING -->
<div class="c-content-box c-size-md">
	<div class="container">
		<div class="row">
			<div class="col-md-9">
				<div class="c-content-blog-post-1-list">
					<div class="c-content-blog-post-1">
						<div class="c-media">
							<div class="c-content-media-2-slider" data-slider="owl">
								<div class="owl-carousel owl-theme c-theme owl-single" data-single-item="true" data-auto-play="4000" data-rtl="true">
									<div class="item">
										<div class="c-content-media-2" style="background-image: url(../../assets/base/img/content/misc/latest-work-3.jpg); min-height: 460px;">
										</div>
									</div>
									<div class="item">
										<div class="c-content-media-2" style="background-image: url(../../assets/base/img/content/misc/latest-work-7.jpg); min-height: 460px;">
										</div>
									</div>
									<div class="item">
										<div class="c-content-media-2" style="background-image: url(../../assets/base/img/content/misc/9.jpg); min-height: 460px;">
										</div>
									</div>
								</div>
							</div>
						</div>

						<div class="c-title c-font-bold c-font-uppercase">
							<a href="#">UX Design Conference 2015</a>
						</div>

						<div class="c-desc">
							Lorem ipsum dolor sit amet, coectetuer diam ipsum dolor sit amet nonummy  coectetuer diam ipsum dolor sit
							coectetuer adipiscing elit adipiscing consectetuer ipsum dolor sit amipiscing elit sit amet, sit amet,
							coectetuer adipiscing elit adipiscing consectetuer ipsum dolor sit amet diam nonummy adipiscing elit sit amet, sit ame.
							<a href="#">read more...</a>
						</div>

						<div class="c-panel">
							<div class="c-author"><a href="#">By <span class="c-font-uppercase">Nick Strong</span></a></div>
							<div class="c-date">on <span class="c-font-uppercase">20 May 2015, 10:30AM</span></div>
							<ul class="c-tags c-theme-ul-bg">
								<li>ux</li>
								<li>marketing</li>
								<li>events</li>
							</ul>
							<div class="c-comments"><a href="#"><i class="icon-speech"></i> 30 comments</a></div>
						</div>
					</div>
					<div class="c-content-blog-post-1">
						<div class="c-media">
							<iframe src="https://player.vimeo.com/video/105329112" width="100%" height="381" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>
						</div>

						<div class="c-title c-font-bold c-font-uppercase">
							<a href="#">The Future...</a>
						</div>

						<div class="c-desc">
							Lorem ipsum dolor sit amet, coectetuer diam ipsum dolor sit amet nonummy  coectetuer diam ipsum dolor sit
							coectetuer adipiscing elit adipiscing consectetuer ipsum dolor sit amipiscing elit sit amet, sit amet,
							coectetuer adipiscing elit adipiscing consectetuer ipsum dolor sit amet diam nonummy adipiscing elit sit amet, sit ame.
							<a href="#">read more...</a>
						</div>

						<div class="c-panel">
							<div class="c-author"><a href="#">By <span class="c-font-uppercase">Nick Strong</span></a></div>
							<div class="c-date">on <span class="c-font-uppercase">20 May 2015, 10:30AM</span></div>
							<ul class="c-tags c-theme-ul-bg">
								<li>hi-tech</li>
								<li>enginering</li>
								<li>robots</li>
							</ul>
							<div class="c-comments"><a href="#"><i class="icon-speech"></i> 30 comments</a></div>
						</div>
					</div>
					<div class="c-content-blog-post-1">
						<div class="c-media">
							<div class="c-content-media-2-slider" data-slider="owl">
								<div class="owl-carousel owl-theme c-theme owl-single" data-single-item="true" data-auto-play="4000" data-rtl="true">
									<div class="item">
										<div class="c-content-media-2" style="background-image: url(../../assets/base/img/content/stock/23.jpg); min-height: 360px;">
										</div>
									</div>
									<div class="item">
										<div class="c-content-media-2" style="background-image: url(../../assets/base/img/content/stock/34.jpg); min-height: 360px;">
										</div>
									</div>
									<div class="item">
										<div class="c-content-media-2" style="background-image: url(../../assets/base/img/content/stock/37.jpg); min-height: 360px;">
										</div>
									</div>
								</div>
							</div>
						</div>

						<div class="c-title c-font-bold c-font-uppercase">
							<a href="#">TAKE THE WEB BY STORM WITH JANGO</a>
						</div>

						<div class="c-desc">
							Lorem ipsum dolor sit amet, coectetuer diam ipsum dolor sit amet nonummy  coectetuer diam ipsum dolor sit
							coectetuer adipiscing elit adipiscing consectetuer ipsum dolor sit amipiscing elit sit amet, sit amet,
							coectetuer adipiscing elit adipiscing consectetuer ipsum dolor sit amet diam nonummy adipiscing elit sit amet, sit ame.
							<a href="#">read more...</a>
						</div>

						<div class="c-panel">
							<div class="c-author"><a href="#">By <span class="c-font-uppercase">Nick Strong</span></a></div>
							<div class="c-date">on <span class="c-font-uppercase">20 May 2015, 10:30AM</span></div>
							<ul class="c-tags c-theme-ul-bg">
								<li>ux</li>
								<li>web</li>
								<li>html</li>
							</ul>
							<div class="c-comments"><a href="#"><i class="icon-speech"></i> 30 comments</a></div>
						</div>
					</div>

					<div class="c-pagination">
						<ul class="c-content-pagination c-theme">
							<li class="c-prev"><a href="#"><i class="fa fa-angle-left"></i></a></li>
							<li><a href="#">1</a></li>
							<li class="c-active"><a href="#">2</a></li>
							<li><a href="#">3</a></li>
							<li><a href="#">4</a></li>
							<li class="c-next"><a href="#"><i class="fa fa-angle-right"></i></a></li>
						</ul>
					</div>
				</div>
			</div>
			<div class="col-md-3">
			<!-- BEGIN: CONTENT/BLOG/BLOG-SIDEBAR-1 -->
<form action="#" method="post">
	<div class="input-group">
      <input type="text" class="form-control c-square c-theme-border" placeholder="Search blog...">
      <span class="input-group-btn">
        <button class="btn c-theme-btn c-theme-border c-btn-square" type="button">Go!</button>
      </span>
    </div>
</form>

<div class="c-content-ver-nav">
	<div class="c-content-title-1 c-theme c-title-md c-margin-t-40">
		<h3 class="c-font-bold c-font-uppercase">Categories</h3>
		<div class="c-line-left c-theme-bg"></div>
	</div>
	<ul class="c-menu c-arrow-dot1 c-theme">
		<li><a href="#">Web Development(2)</a></li>
		<li><a href="#">UX Design(12)</a></li>
		<li><a href="#">Mobile Development(5)</a></li>
		<li><a href="#">Internet Marketing(7)</a></li>
		<li><a href="#">Social Networks(11)</a></li>
		<li><a href="#">Web Design(18)</a></li>
	</ul>
</div>

<div class="c-content-tab-1 c-theme c-margin-t-30">
	<div class="nav-justified">
		<ul class="nav nav-tabs nav-justified">
		    <li class="active"><a href="#blog_recent_posts" data-toggle="tab">Recent Posts</a></li>
		    <li><a href="#blog_popular_posts" data-toggle="tab">Popular Posts</a></li>
		</ul>
		<div class="tab-content">
	    	<div class="tab-pane active" id="blog_recent_posts">
	    		<ul class="c-content-recent-posts-1">
	    			<li>
	    				<div class="c-image">
	    					<img src="../../assets/base/img/content/stock/09.jpg" alt="" class="img-responsive">
	    				</div>
	    				<div class="c-post">
	    					<a href="" class="c-title">
	    					UX Design Expo 2015...
	    					</a>
	    					<div class="c-date">27 Jan 2015</div>
	    				</div>
	    			</li>
	    			<li>
	    				<div class="c-image">
	    					<img src="../../assets/base/img/content/stock/08.jpg" alt="" class="img-responsive">
	    				</div>
	    				<div class="c-post">
	    					<a href="" class="c-title">
	    					UX Design Expo 2015...
	    					</a>
	    					<div class="c-date">27 Jan 2015</div>
	    				</div>
	    			</li>
	    			<li>
	    				<div class="c-image">
	    					<img src="../../assets/base/img/content/stock/07.jpg" alt="" class="img-responsive">
	    				</div>
	    				<div class="c-post">
	    					<a href="" class="c-title">
	    					UX Design Expo 2015...
	    					</a>
	    					<div class="c-date">27 Jan 2015</div>
	    				</div>
	    			</li>
	    			<li>
	    				<div class="c-image">
	    					<img src="../../assets/base/img/content/stock/32.jpg" alt="" class="img-responsive">
	    				</div>
	    				<div class="c-post">
	    					<a href="" class="c-title">
	    					UX Design Expo 2015...
	    					</a>
	    					<div class="c-date">27 Jan 2015</div>
	    				</div>
	    			</li>
	    		</ul>
	    	</div>
	    	<div class="tab-pane" id="blog_popular_posts">
	    		<ul class="c-content-recent-posts-1">
	    			<li>
	    				<div class="c-image">
	    					<img src="../../assets/base/img/content/stock/34.jpg" class="img-responsive" alt=""/>
	    				</div>
	    				<div class="c-post">
	    					<a href="" class="c-title">
	    					UX Design Expo 2015...
	    					</a>
	    					<div class="c-date">27 Jan 2015</div>
	    				</div>
	    			</li>
	    			<li>
	    				<div class="c-image">
	    					<img src="../../assets/base/img/content/stock/37.jpg" class="img-responsive" alt=""/>
	    				</div>
	    				<div class="c-post">
	    					<a href="" class="c-title">
	    					UX Design Expo 2015...
	    					</a>
	    					<div class="c-date">27 Jan 2015</div>
	    				</div>
	    			</li>
	    			<li>
	    				<div class="c-image">
	    					<img src="../../assets/base/img/content/stock/32.jpg" class="img-responsive" alt=""/>
	    				</div>
	    				<div class="c-post">
	    					<a href="" class="c-title">
	    					UX Design Expo 2015...
	    					</a>
	    					<div class="c-date">27 Jan 2015</div>
	    				</div>
	    			</li>
	    			<li>
	    				<div class="c-image">
	    					<img src="../../assets/base/img/content/stock/54.jpg" class="img-responsive" alt=""/>
	    				</div>
	    				<div class="c-post">
	    					<a href="" class="c-title">
	    					UX Design Expo 2015...
	    					</a>
	    					<div class="c-date">27 Jan 2015</div>
	    				</div>
	    			</li>
	    		</ul>
	    	</div>
	  	</div>
  	</div>
</div>

<div class="c-content-ver-nav">
	<div class="c-content-title-1 c-theme c-title-md c-margin-t-40">
		<h3 class="c-font-bold c-font-uppercase">Blogs</h3>
		<div class="c-line-left c-theme-bg"></div>
	</div>
	<ul class="c-menu c-arrow-dot c-theme">
		<li><a href="#">Fasion & Arts</a></li>
		<li><a href="#">UX & Web Design</a></li>
		<li><a href="#">Mobile Development</a></li>
		<li><a href="#">Internet Marketing</a></li>
		<li><a href="#">Frontend Development</a></li>
	</ul>
</div><!-- END: CONTENT/BLOG/BLOG-SIDEBAR-1 -->
			</div>
		</div>
	</div>
</div>
<!-- END: BLOG LISTING  -->

		<!-- END: PAGE CONTENT -->
	</div>
	<!-- END: PAGE CONTAINER -->

	<!-- BEGIN: LAYOUT/FOOTERS/FOOTER-5 -->

</body>
