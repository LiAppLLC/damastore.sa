


@extends('Welcome.home')


@section('content')

    <?php
    $locale = App::getLocale();
    ?>



<body class="c-layout-header-fixed c-layout-header-mobile-fixed c-layout-header-topbar c-layout-header-topbar-collapse">


	<!-- BEGIN: PAGE CONTAINER -->
	<div class="c-layout-page">
		<!-- BEGIN: LAYOUT/BREADCRUMBS/BREADCRUMBS-2 -->
<div class="c-layout-breadcrumbs-1 c-subtitle c-fonts-uppercase c-fonts-bold c-bordered c-bordered-both">
	<div class="container">
		<div class="c-page-title c-pull-left">
			<h3 class="c-font-uppercase c-font-sbold">Customer Edit Profile</h3>
			<h4 class="">Page Sub Title Goes Here</h4>
		</div>
		<ul class="c-page-breadcrumbs c-theme-nav c-pull-right c-fonts-regular">
															<li class="c-state_active">Jango Components</li>

		</ul>
	</div>
</div><!-- END: LAYOUT/BREADCRUMBS/BREADCRUMBS-2 -->
		<div class="container">
			<div class="c-layout-sidebar-menu c-theme ">
			<!-- BEGIN: LAYOUT/SIDEBARS/SHOP-SIDEBAR-DASHBOARD -->
<div class="c-sidebar-menu-toggler">
	<h3 class="c-title c-font-uppercase c-font-bold">My Profile</h3>
	<a href="javascript:;" class="c-content-toggler" data-toggle="collapse" data-target="#sidebar-menu-1">
		<span class="c-line"></span> <span class="c-line"></span> <span class="c-line"></span>
	</a>
</div>

<ul class="c-sidebar-menu collapse " id="sidebar-menu-1">
	<li class="c-dropdown c-open">
		<a href="javascript:;" class="c-toggler">My Profile<span class="c-arrow"></span></a>
		<ul class="c-dropdown-menu">
			<li class="">
				<a href="{{asset('CustomerDashboard')}}">My Dashbord</a>
			</li>
			<li class="">
				<a href="{{asset('editprofile')}}">Edit Profile</a>
			</li>
			<li class="c-active">
				<a href="{{asset('changePassword')}}">Change Password</a>
			</li>
			<li class="">
				<a href="shop-order-history.html">Order History</a>
			</li>
			<li class="">
				<a href="shop-customer-addresses.html">My Addresses</a>
			</li>
			<li class="">
				<a href="shop-product-wishlist.html">My Wishlist</a>
			</li>
		</ul>
	</li>
</ul><!-- END: LAYOUT/SIDEBARS/SHOP-SIDEBAR-DASHBOARD -->
			</div>
			<div class="c-layout-sidebar-content ">
			<!-- BEGIN: PAGE CONTENT -->
			<div class="c-content-title-1">
	<h3 class="c-font-uppercase c-font-bold">Change Password</h3>
	<div class="c-line-left"></div>
</div>
	<form class="c-shop-form-1" id="changePasswordForm">
	{{ csrf_field() }}
	<!-- BEGIN: ADDRESS FORM -->
	<div class="">



		<!-- BEGIN: PASSWORD -->
		<div class="row">
			<div class="form-group col-md-12">
				<label class="control-label">Old Password</label>
				<input type="password" id="password" name="password" class="form-control c-square c-theme" placeholder="Password">
			</div>
		</div>
		<div class="row">
			<div class="form-group col-md-12">
				<label class="control-label">New Password</label>
				<input type="password" id="newpassword" name="newpassword" class="form-control c-square c-theme" placeholder="Password">
			</div>
		</div>
		<div class="row">
			<div class="form-group col-md-12">
				<label class="control-label">Repeat Password</label>
				<input type="password" id="repeated_newpassword" class="form-control c-square c-theme" placeholder="Password">
			</div>
		</div>
		<!-- END: PASSWORD -->




		<div class="row c-margin-t-30">
			<div class="form-group col-md-12" role="group">
				<button type="submit" class="btn btn-lg c-theme-btn c-btn-square c-btn-uppercase c-btn-bold">Submit</button>
				<button type="submit" class="btn btn-lg btn-default c-btn-square c-btn-uppercase c-btn-bold">Cancel</button>
			</div>
		</div>
	</div>
	<!-- END: ADDRESS FORM -->
</form>			<!-- END: PAGE CONTENT -->
			</div>
		</div>
	</div>
	<!-- END: PAGE CONTAINER -->

</body>


@endsection





@section('script')


	<script>
        var userObject = JSON.parse(localStorage.getItem("DamaStore_userObject"));
        // blocked
        // bornDate
        // created_at
        // email
        // emailVerifiedAt
        // files
        // firstName
        // id
        // lastActivity
        // lastLoggedIn
        // lastName
        // phone
        // reputation
        // roles
        // token
        // updated_at
        // username
        // verify
        console.log('userObject', userObject);


        $('#changePasswordForm').submit(function (e) {

            var formData = new FormData($(this)[0]);
            e.preventDefault();
            e.stopPropagation();

            var repeated_newpassword = $('#repeated_newpassword').val();
            var newpassword = $('#newpassword').val();

            if(newpassword == repeated_newpassword) {
                $.ajax({

                    type: "POST",
                    url: '{{route('api_changePassword')}}',
                    data: formData,
                    // mimeType: 'application/json',
                    processData: false,
                    contentType: false,
                    headers: {"Authorization": 'Bearer ' + userObject.token},
                    success: function (res) {
                        if (res.status == 200) {
                            Swal.fire({
                                icon: 'success',
                                title: 'success',
                                text: 'your Password has been Changed successfuly, thanks for using our services',
                            }).then(function () {
                                window.location = "/CustomerDashboard";
                            });

                        }
                        if (res.status == 500) {
                            // console.log(res.data.validator);
                            Swal.fire({
                                icon: 'error',
                                title: 'Oops...',
                                text: res.msg,
                            });
                        }

                    },
                    error: function (res, response) {
                    },
                });
            }else{
                Swal.fire({
                    icon: 'error',
                    title: 'Oops...',
                    text: 'New password not matched',
                });
			}


        });


	</script>

@endsection



