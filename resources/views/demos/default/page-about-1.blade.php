@extends('Welcome.home')


@section('content')

    <?php
    $locale = App::getLocale();
    ?>

    <body class="c-layout-header-fixed c-layout-header-mobile-fixed c-layout-header-topbar c-layout-header-topbar-collapse">


    <div class="modal fade c-content-login-form" id="forget-password-form" role="dialog">
        <div class="modal-dialog">
            <div class="modal-content c-square">
                <div class="modal-header c-no-border">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                </div>
                <div class="modal-body">
                    <h3 class="c-font-24 c-font-sbold">Password Recovery</h3>
                    <p>To recover your password please fill in your email address</p>
                    <form>
                        <div class="form-group">
                            <label for="forget-email" class="hide">Email</label>
                            <input type="email" class="form-control input-lg c-square" id="forget-email" placeholder="Email">
                        </div>
                        <div class="form-group">
                            <button type="submit" class="btn c-theme-btn btn-md c-btn-uppercase c-btn-bold c-btn-square c-btn-login">Submit</button>
                            <a href="javascript:;" class="c-btn-forgot" data-toggle="modal" data-target="#login-form" data-dismiss="modal">Back To Login</a>
                        </div>
                    </form>
                </div>
                <div class="modal-footer c-no-border">
                    <span class="c-text-account">Don't Have An Account Yet ?</span>
                    <a href="javascript:;" data-toggle="modal" data-target="#signup-form" data-dismiss="modal" class="btn c-btn-dark-1 btn c-btn-uppercase c-btn-bold c-btn-slim c-btn-border-2x c-btn-square c-btn-signup">Signup!</a>
                </div>
            </div>
        </div>
    </div><!-- END: CONTENT/USER/FORGET-PASSWORD-FORM -->
    <!-- BEGIN: CONTENT/USER/SIGNUP-FORM -->
    <div class="modal fade c-content-login-form" id="signup-form" role="dialog">
        <div class="modal-dialog">
            <div class="modal-content c-square">
                <div class="modal-header c-no-border">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                </div>
                <div class="modal-body">
                    <h3 class="c-font-24 c-font-sbold">Create An Account</h3>
                    <p>Please fill in below form to create an account with us</p>
                    <form>
                        <div class="form-group">
                            <label for="signup-email" class="hide">Email</label>
                            <input type="email" class="form-control input-lg c-square" id="signup-email" placeholder="Email">
                        </div>
                        <div class="form-group">
                            <label for="signup-username" class="hide">Username</label>
                            <input type="email" class="form-control input-lg c-square" id="signup-username" placeholder="Username">
                        </div>
                        <div class="form-group">
                            <label for="signup-fullname" class="hide">Fullname</label>
                            <input type="email" class="form-control input-lg c-square" id="signup-fullname" placeholder="Fullname">
                        </div>
                        <div class="form-group">
                            <label for="signup-country" class="hide">Country</label>
                            <select class="form-control input-lg c-square" id="signup-country">
                                <option value="1">Country</option>
                            </select>
                        </div>
                        <div class="form-group">
                            <button type="submit" class="btn c-theme-btn btn-md c-btn-uppercase c-btn-bold c-btn-square c-btn-login">Signup</button>
                            <a href="javascript:;" class="c-btn-forgot" data-toggle="modal" data-target="#login-form" data-dismiss="modal">Back To Login</a>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div><!-- END: CONTENT/USER/SIGNUP-FORM -->
    <!-- BEGIN: CONTENT/USER/LOGIN-FORM -->
    <div class="modal fade c-content-login-form" id="login-form" role="dialog">
        <div class="modal-dialog">
            <div class="modal-content c-square">
                <div class="modal-header c-no-border">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                </div>
                <div class="modal-body">
                    <h3 class="c-font-24 c-font-sbold">Good Afternoon!</h3>
                    <p>Let's make today a great day!</p>
                    <form>
                        <div class="form-group">
                            <label for="login-email" class="hide">Email</label>
                            <input type="email" class="form-control input-lg c-square" id="login-email" placeholder="Email">
                        </div>
                        <div class="form-group">
                            <label for="login-password" class="hide">Password</label>
                            <input type="password" class="form-control input-lg c-square" id="login-password" placeholder="Password">
                        </div>
                        <div class="form-group">
                            <div class="c-checkbox">
                                <input type="checkbox" id="login-rememberme" class="c-check">
                                <label for="login-rememberme" class="c-font-thin c-font-17">
                                    <span></span>
                                    <span class="check"></span>
                                    <span class="box"></span>
                                    Remember Me
                                </label>
                            </div>
                        </div>
                        <div class="form-group">
                            <button type="submit" class="btn c-theme-btn btn-md c-btn-uppercase c-btn-bold c-btn-square c-btn-login">Login</button>
                            <a href="javascript:;" data-toggle="modal" data-target="#forget-password-form" data-dismiss="modal" class="c-btn-forgot">Forgot Your Password ?</a>
                        </div>
                        <div class="clearfix">
                            <div class="c-content-divider c-divider-sm c-icon-bg c-bg-grey c-margin-b-20">
                                <span>or signup with</span>
                            </div>
                            <ul class="c-content-list-adjusted">
                                <li>
                                    <a class="btn btn-block c-btn-square btn-social btn-twitter">
                                        <i class="fa fa-twitter"></i>
                                        Twitter
                                    </a>
                                </li>
                                <li>
                                    <a class="btn btn-block c-btn-square btn-social btn-facebook">
                                        <i class="fa fa-facebook"></i>
                                        Facebook
                                    </a>
                                </li>
                                <li>
                                    <a class="btn btn-block c-btn-square btn-social btn-google">
                                        <i class="fa fa-google"></i>
                                        Google
                                    </a>
                                </li>
                            </ul>
                        </div>
                    </form>
                </div>
                <div class="modal-footer c-no-border">
                    <span class="c-text-account">Don't Have An Account Yet ?</span>
                    <a href="javascript:;" data-toggle="modal" data-target="#signup-form" data-dismiss="modal" class="btn c-btn-dark-1 btn c-btn-uppercase c-btn-bold c-btn-slim c-btn-border-2x c-btn-square c-btn-signup">Signup!</a>
                </div>
            </div>
        </div>
    </div><!-- END: CONTENT/USER/LOGIN-FORM -->

    <!-- BEGIN: LAYOUT/SIDEBARS/QUICK-SIDEBAR -->
    <nav class="c-layout-quick-sidebar">
        <div class="c-header">
            <button type="button" class="c-link c-close">
                <i class="icon-login"></i>
            </button>
        </div>
        <div class="c-content">
            <div class="c-section">
                <h3>JANGO DEMOS</h3>
                <div class="c-settings c-demos c-bs-grid-reset-space">
                    <div class="row">
                        <div class="col-md-12">
                            <a href="../default/index.html" class="c-demo-container c-demo-img-lg">
                                <div class="c-demo-thumb active">
                                    <img src="../../assets/base/img/content/quick-sidebar/default.jpg" class="c-demo-thumb-img"/>
                                </div>
                            </a>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-6">
                            <a href="../corporate_1/index.html" class="c-demo-container">
                                <div class="c-demo-thumb  c-thumb-left">
                                    <img src="../../assets/base/img/content/quick-sidebar/corporate_1.jpg" class="c-demo-thumb-img"/>
                                </div>
                            </a>
                        </div>
                        <div class="col-md-6">
                            <a href="../agency_1/index.html" class="c-demo-container">
                                <div class="c-demo-thumb  c-thumb-right">
                                    <img src="../../assets/base/img/content/quick-sidebar/corporate_1-onepage.jpg" class="c-demo-thumb-img"/>
                                </div>
                            </a>
                        </div>
                    </div>
                </div>
            </div>
            <div class="c-section">
                <h3>Theme Colors</h3>
                <div class="c-settings">

                    <span class="c-color c-default c-active" data-color="default"></span>

                    <span class="c-color c-green1" data-color="green1"></span>
                    <span class="c-color c-green2" data-color="green2"></span>
                    <span class="c-color c-green3" data-color="green3"></span>

                    <span class="c-color c-yellow1" data-color="yellow1"></span>
                    <span class="c-color c-yellow2" data-color="yellow2"></span>
                    <span class="c-color c-yellow3" data-color="yellow3"></span>

                    <span class="c-color c-red1" data-color="red1"></span>
                    <span class="c-color c-red2" data-color="red2"></span>
                    <span class="c-color c-red3" data-color="red3"></span>

                    <span class="c-color c-purple1" data-color="purple1"></span>
                    <span class="c-color c-purple2" data-color="purple2"></span>
                    <span class="c-color c-purple3" data-color="purple3"></span>

                    <span class="c-color c-blue1" data-color="blue1"></span>
                    <span class="c-color c-blue2" data-color="blue2"></span>
                    <span class="c-color c-blue3" data-color="blue3"></span>

                    <span class="c-color c-brown1" data-color="brown1"></span>
                    <span class="c-color c-brown2" data-color="brown2"></span>
                    <span class="c-color c-brown3" data-color="brown3"></span>

                    <span class="c-color c-dark1" data-color="dark1"></span>
                    <span class="c-color c-dark2" data-color="dark2"></span>
                    <span class="c-color c-dark3" data-color="dark3"></span>
                </div>
            </div>
            <div class="c-section">
                <h3>Header Type</h3>
                <div class="c-settings">
                    <input type="button" class="c-setting_header-type btn btn-sm c-btn-square c-btn-border-1x c-btn-white c-btn-sbold c-btn-uppercase active" data-value="boxed" value="boxed"/>
                    <input type="button" class="c-setting_header-type btn btn-sm c-btn-square c-btn-border-1x c-btn-white c-btn-sbold c-btn-uppercase" data-value="fluid" value="fluid"/>
                </div>
            </div>
            <div class="c-section">
                <h3>Header Mode</h3>
                <div class="c-settings">
                    <input type="button" class="c-setting_header-mode btn btn-sm c-btn-square c-btn-border-1x c-btn-white c-btn-sbold c-btn-uppercase active" data-value="fixed" value="fixed"/>
                    <input type="button" class="c-setting_header-mode btn btn-sm c-btn-square c-btn-border-1x c-btn-white c-btn-sbold c-btn-uppercase" data-value="static" value="static"/>
                </div>
            </div>
            <div class="c-section">
                <h3>Mega Menu Style</h3>
                <div class="c-settings">
                    <input type="button" class="c-setting_megamenu-style btn btn-sm c-btn-square c-btn-border-1x c-btn-white c-btn-sbold c-btn-uppercase active" data-value="dark" value="dark"/>
                    <input type="button" class="c-setting_megamenu-style btn btn-sm c-btn-square c-btn-border-1x c-btn-white c-btn-sbold c-btn-uppercase" data-value="light" value="light"/>
                </div>
            </div>
            <div class="c-section">
                <h3>Font Style</h3>
                <div class="c-settings">
                    <input type="button" class="c-setting_font-style btn btn-sm c-btn-square c-btn-border-1x c-btn-white c-btn-sbold c-btn-uppercase active" data-value="default" value="default"/>
                    <input type="button" class="c-setting_font-style btn btn-sm c-btn-square c-btn-border-1x c-btn-white c-btn-sbold c-btn-uppercase" data-value="light" value="light"/>
                </div>
            </div>
            <div class="c-section">
                <h3>Reading Style</h3>
                <div class="c-settings">
                    <a href="http://www.themehats.com/themes/jango/" class="c-setting_font-style btn btn-sm c-btn-square c-btn-border-1x c-btn-white c-btn-sbold c-btn-uppercase ">LTR</a>
                    <a href="http://www.themehats.com/themes/jango/rtl/" class="c-setting_font-style btn btn-sm c-btn-square c-btn-border-1x c-btn-white c-btn-sbold c-btn-uppercase active">RTL</a>
                </div>
            </div>
        </div>
    </nav><!-- END: LAYOUT/SIDEBARS/QUICK-SIDEBAR -->

    <!-- BEGIN: PAGE CONTAINER -->
    <div class="c-layout-page">
        <!-- BEGIN: LAYOUT/BREADCRUMBS/BREADCRUMBS-3 -->
        <div class="c-layout-breadcrumbs-1 c-bgimage c-subtitle c-fonts-uppercase c-fonts-bold c-bg-img-center" style="background-image: url(../../assets/base/img/content/backgrounds/bg-28.jpg)">
            <div class="container">
                <div class="c-page-title c-pull-left">
                    <h3 class="c-font-uppercase c-font-bold c-font-white c-font-20 c-font-slim">About Us</h3>
                    <h4 class="c-font-white c-font-thin c-opacity-07">
                        Page Sub Title Goes Here			</h4>
                </div>
                <ul class="c-page-breadcrumbs c-theme-nav c-pull-right c-fonts-regular">
                    <li><a href="#" class="c-font-white">Pages</a></li>
                    <li class="c-font-white">/</li>
                    <li><a href="page-about-1.html" class="c-font-white">About Us 1</a></li>
                    <li class="c-font-white">/</li>
                    <li class="c-state_active c-font-white">Jango Components</li>

                </ul>
            </div>
        </div><!-- END: LAYOUT/BREADCRUMBS/BREADCRUMBS-3 -->
        <!-- BEGIN: PAGE CONTENT -->
        <!-- BEGIN: CONTENT/MISC/LATEST-ITEMS-3 -->
        <div class="c-content-box c-size-md c-bg-white">
            <div class="container">
                <div class="row">
                    <div class="col-md-4">
                        <div class="c-content-media-1 c-bordered wow animated fadeInLeft" style="min-height: 380px;">
                            <div class="c-content-label c-font-uppercase c-font-bold c-theme-bg">Our Mission</div>
                            <a href="#" class="c-title c-font-uppercase c-theme-on-hover c-font-bold">Take the web by storm with JANGO</a>
                            <p>Lorem ipsum dolor sit amet, coectetuer adipiscing elit sed diam nonummy et nibh euismod aliquam erat volutpat</p>
                            <div class="c-author">
                                <div class="c-portrait" style="background-image: url(../../assets/base/img/content/team/team16.jpg)"></div>
                                <div class="c-name c-font-uppercase">Jack Nilson</div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-8">
                        <div class="c-content-media-2-slider wow animated fadeInRight" data-slider="owl">
                            <div class="c-content-label c-font-uppercase c-font-bold">Latest Projects</div>
                            <div class="owl-carousel owl-theme c-theme owl-single" data-single-item="true" data-navigation-dots="true" data-auto-play="4000" data-rtl="true">
                                <div class="c-content-media-2 c-bg-img-center" style="background-image: url(../../assets/base/img/content/stock3/36.jpg); min-height: 380px;">
                                    <div class="c-panel">
                                        <div class="c-fav">
                                            <i class="icon-heart c-font-thin"></i>
                                            <p class="c-font-thin">16</p>
                                        </div>
                                    </div>
                                </div>
                                <div class="c-content-media-2 c-bg-img-center" style="background-image: url(../../assets/base/img/content/stock3/43.jpg); min-height: 380px;">
                                    <div class="c-panel">
                                        <div class="c-fav">
                                            <i class="icon-heart c-font-thin"></i>
                                            <p class="c-font-thin">24</p>
                                        </div>
                                    </div>
                                </div>
                                <div class="c-content-media-2 c-bg-img-center" style="background-image: url(../../assets/base/img/content/stock3/50.jpg); min-height: 380px;">
                                    <div class="c-panel">
                                        <div class="c-fav">
                                            <i class="icon-heart c-font-thin"></i>
                                            <p class="c-font-thin">19</p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div><!-- END: CONTENT/MISC/LATEST-ITEMS-3 -->

        <!-- BEGIN: CONTENT/MISC/SERVICES-2 -->
        <div class="c-content-box c-size-md c-bg-white">
            <div class="container">
                <div class="c-content-feature-2-grid">
                    <div class="c-content-title-1 wow animated fadeIn">
                        <h3 class="c-font-uppercase c-font-bold">Services We Do</h3>
                        <div class="c-line-left"></div>
                    </div>
                    <div class="row">
                        <div class="col-md-4 col-sm-6">
                            <div class="c-content-feature-2 c-option-2 c-theme-bg-parent-hover wow animated fadeInUp">
                                <div class="c-icon-wrapper c-theme-bg-on-parent-hover">
                                    <div class="c-content-line-icon c-theme c-icon-screen-chart"></div>
                                </div>
                                <h3 class="c-font-uppercase c-title">Web Design</h3>
                                <p>Lorem ipsum sit dolor eamet dolore adipiscing</p>
                            </div>
                        </div>
                        <div class="col-md-4 col-sm-6">
                            <div class="c-content-feature-2 c-option-2 c-theme-bg-parent-hover wow animated fadeInUp">
                                <div class="c-icon-wrapper c-theme-bg-on-parent-hover">
                                    <div class="c-content-line-icon c-theme c-icon-support"></div>
                                </div>
                                <h3 class="c-font-uppercase c-title">Mobile Apps</h3>
                                <p>Lorem ipsum sit dolor eamet dolore adipiscing</p>
                            </div>
                        </div>
                        <div class="col-md-4 col-sm-6">
                            <div class="c-content-feature-2 c-option-2 c-theme-bg-parent-hover wow animated fadeInUp">
                                <div class="c-icon-wrapper c-theme-bg-on-parent-hover">
                                    <div class="c-content-line-icon c-theme c-icon-comment"></div>
                                </div>
                                <h3 class="c-font-uppercase c-title">Consulting</h3>
                                <p>Lorem ipsum sit dolor eamet dolore adipiscing</p>
                            </div>
                        </div>
                        <div class="col-md-4 col-sm-6">
                            <div class="c-content-feature-2 c-option-2 c-theme-bg-parent-hover wow animated fadeInUp">
                                <div class="c-icon-wrapper c-theme-bg-on-parent-hover">
                                    <div class="c-content-line-icon c-theme c-icon-bulb"></div>
                                </div>
                                <h3 class="c-font-uppercase c-title">Campaigns</h3>
                                <p>Lorem ipsum sit dolor eamet dolore adipiscing</p>
                            </div>
                        </div>
                        <div class="col-md-4 col-sm-6">
                            <div class="c-content-feature-2 c-option-2 c-theme-bg-parent-hover wow animated fadeInUp">
                                <div class="c-icon-wrapper c-theme-bg-on-parent-hover">
                                    <div class="c-content-line-icon c-theme c-icon-sticker"></div>
                                </div>
                                <h3 class="c-font-uppercase c-title">UX Design</h3>
                                <p>Lorem ipsum sit dolor eamet dolore adipiscing</p>
                            </div>
                        </div>
                        <div class="col-md-4 col-sm-6">
                            <div class="c-content-feature-2 c-option-2 c-theme-bg-parent-hover wow animated fadeInUp">
                                <div class="c-icon-wrapper c-theme-bg-on-parent-hover">
                                    <div class="c-content-line-icon c-theme c-icon-globe"></div>
                                </div>
                                <h3 class="c-font-uppercase c-title">Hosting</h3>
                                <p>Lorem ipsum sit dolor eamet dolore adipiscing</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div><!-- END: CONTENT/MISC/SERVICES-2 -->

        <!-- BEGIN: CONTENT/SLIDERS/TEAM-2 -->
        <div class="c-content-box c-size-md c-bg-grey-1">
            <div class="container">
                <!-- Begin: Testimonals 1 component -->
                <div class="c-content-person-1-slider" data-slider="owl">
                    <!-- Begin: Title 1 component -->
                    <div class="c-content-title-1 wow animated fadeIn">
                        <h3 class="c-center c-font-uppercase c-font-bold">Meet The Team</h3>
                        <div class="c-line-center c-theme-bg"></div>
                    </div>
                    <!-- End-->

                    <!-- Begin: Owlcarousel -->
                    <div class="owl-carousel owl-theme c-theme c-owl-nav-center wow animated fadeInUp" data-items="3" data-slide-speed="8000" data-rtl="true">
                        <div class="c-content-person-1 c-option-2">
                            <div class="c-caption c-content-overlay">
                                <div class="c-overlay-wrapper">
                                    <div class="c-overlay-content">
                                        <a href="#"><i class="icon-link"></i></a>
                                        <a href="../../assets/base/img/content/team/team10.jpg" data-lightbox="fancybox" data-fancybox-group="gallery-2">
                                            <i class="icon-magnifier"></i>
                                        </a>
                                    </div>
                                </div>
                                <img src="../../assets/base/img/content/team/team10.jpg" class="img-responsive c-overlay-object" alt="">
                            </div>
                            <div class="c-body">
                                <div class="c-head">
                                    <div class="c-name c-font-uppercase c-font-bold">Randy JANGO</div>
                                    <ul class="c-socials c-theme-ul">
                                        <li><a href="#"><i class="icon-social-twitter"></i></a></li>
                                        <li><a href="#"><i class="icon-social-facebook"></i></a></li>
                                        <li><a href="#"><i class="icon-social-dribbble"></i></a></li>
                                    </ul>
                                </div>
                                <div class="c-position">
                                    CEO, JANGO Inc.
                                </div>
                                <p>
                                    Lorem ipsum dolor sit amet, dolor nemo amet elit.
                                    Nulla nemo consequuntur.
                                </p>
                            </div>
                        </div>
                        <div class="c-content-person-1 c-option-2">
                            <div class="c-caption c-content-overlay">
                                <div class="c-overlay-wrapper">
                                    <div class="c-overlay-content">
                                        <a href="#"><i class="icon-link"></i></a>
                                        <a href="../../assets/base/img/content/team/team9.jpg" data-lightbox="fancybox" data-fancybox-group="gallery-2">
                                            <i class="icon-magnifier"></i>
                                        </a>
                                    </div>
                                </div>
                                <img src="../../assets/base/img/content/team/team9.jpg" class="img-responsive c-overlay-object" alt="">
                            </div>
                            <div class="c-body">
                                <div class="c-head">
                                    <div class="c-name c-font-uppercase c-font-bold">Mary Jane</div>
                                    <ul class="c-socials c-theme-ul">
                                        <li><a href="#"><i class="icon-social-twitter"></i></a></li>
                                        <li><a href="#"><i class="icon-social-facebook"></i></a></li>
                                        <li><a href="#"><i class="icon-social-dribbble"></i></a></li>
                                    </ul>
                                </div>
                                <div class="c-position">
                                    CFO, JANGO Inc.
                                </div>
                                <p>
                                    Lorem ipsum dolor sit amet, dolor nemo amet elit.
                                    Nulla nemo consequuntur.
                                </p>
                            </div>
                        </div>
                        <div class="c-content-person-1 c-option-2">
                            <div class="c-caption c-content-overlay">
                                <div class="c-overlay-wrapper">
                                    <div class="c-overlay-content">
                                        <a href="#"><i class="icon-link"></i></a>
                                        <a href="../../assets/base/img/content/team/team7.jpg" data-lightbox="fancybox" data-fancybox-group="gallery-2">
                                            <i class="icon-magnifier"></i>
                                        </a>
                                    </div>
                                </div>
                                <img src="../../assets/base/img/content/team/team7.jpg" class="img-responsive c-overlay-object" alt="">
                            </div>
                            <div class="c-body">
                                <div class="c-head">
                                    <div class="c-name c-font-uppercase c-font-bold">Beard Mcbeardson</div>
                                    <ul class="c-socials c-theme-ul">
                                        <li><a href="#"><i class="icon-social-twitter"></i></a></li>
                                        <li><a href="#"><i class="icon-social-facebook"></i></a></li>
                                        <li><a href="#"><i class="icon-social-dribbble"></i></a></li>
                                    </ul>
                                </div>
                                <div class="c-position">
                                    CTO, JANGO Inc.
                                </div>
                                <p>
                                    Lorem ipsum dolor sit amet, dolor nemo amet elit.
                                    Nulla nemo consequuntur.
                                </p>
                            </div>
                        </div>
                        <div class="c-content-person-1 c-option-2">
                            <div class="c-caption c-content-overlay">
                                <div class="c-overlay-wrapper">
                                    <div class="c-overlay-content">
                                        <a href="#"><i class="icon-link"></i></a>
                                        <a href="../../assets/base/img/content/team/team11.jpg" data-lightbox="fancybox" data-fancybox-group="gallery-2">
                                            <i class="icon-magnifier"></i>
                                        </a>
                                    </div>
                                </div>
                                <img src="../../assets/base/img/content/team/team11.jpg" class="img-responsive c-overlay-object" alt="">
                            </div>
                            <div class="c-body">
                                <div class="c-head">
                                    <div class="c-name c-font-uppercase c-font-bold">Sara Conner</div>
                                    <ul class="c-socials c-theme-ul">
                                        <li><a href="#"><i class="icon-social-twitter"></i></a></li>
                                        <li><a href="#"><i class="icon-social-facebook"></i></a></li>
                                        <li><a href="#"><i class="icon-social-dribbble"></i></a></li>
                                    </ul>
                                </div>
                                <div class="c-position">
                                    Director, JANGO Inc.
                                </div>
                                <p>
                                    Lorem ipsum dolor sit amet, dolor nemo amet elit.
                                    Nulla nemo consequuntur.
                                </p>
                            </div>
                        </div>
                        <div class="c-content-person-1 c-option-2">
                            <div class="c-caption c-content-overlay">
                                <div class="c-overlay-wrapper">
                                    <div class="c-overlay-content">
                                        <a href="#"><i class="icon-link"></i></a>
                                        <a href="../../assets/base/img/content/team/team12.jpg" data-lightbox="fancybox" data-fancybox-group="gallery-2">
                                            <i class="icon-magnifier"></i>
                                        </a>
                                    </div>
                                </div>
                                <img src="../../assets/base/img/content/team/team12.jpg" class="img-responsive c-overlay-object" alt="">
                            </div>
                            <div class="c-body">
                                <div class="c-head">
                                    <div class="c-name c-font-uppercase c-font-bold">Jim Book</div>
                                    <ul class="c-socials c-theme-ul">
                                        <li><a href="#"><i class="icon-social-twitter"></i></a></li>
                                        <li><a href="#"><i class="icon-social-facebook"></i></a></li>
                                        <li><a href="#"><i class="icon-social-dribbble"></i></a></li>
                                    </ul>
                                </div>
                                <div class="c-position">
                                    Director, JANGO Inc.
                                </div>
                                <p>
                                    Lorem ipsum dolor sit amet, dolor nemo amet elit.
                                    Nulla nemo consequuntur.
                                </p>
                            </div>
                        </div>
                    </div>
                    <!-- End-->
                </div>
                <!-- End-->
            </div>
        </div><!-- END: CONTENT/SLIDERS/TEAM-2 -->

        <!-- BEGIN: CONTENT/MISC/ABOUT-1 -->
        <div class="c-content-box c-size-md c-bg-white">
            <div class="container">
                <div class="row">
                    <div class="col-sm-6 wow animated fadeInLeft">
                        <!-- Begin: Title 1 component -->
                        <div class="c-content-title-1">
                            <h3 class="c-font-uppercase c-font-bold">About us</h3>
                            <div class="c-line-left c-theme-bg"></div>
                        </div>
                        <!-- End-->
                        <p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed elit diam nonummy nibh euismod tincidunt ut laoreet dolore magna aluam erat volutpat. Ut wisi enim ad minim veniam quis nostrud exerci et tation diam nisl ut aliquip ex ea commodo consequat euismod tincidunt ut laoreet dolore magna aluam. </p>
                        <ul class="c-content-list-1 c-theme  c-font-uppercase">
                            <li>Perfect Design interface</li>
                            <li>Huge Community</li>
                            <li>Support for Everyone</li>
                        </ul>
                        <p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed elit diam nonummy nibh euismod tincidunt ut laoreet dolore.</p>
                    </div>
                    <div class="col-sm-6 wow animated fadeInRight">
                        <div class="c-content-client-logos-1">
                            <!-- Begin: Title 1 component -->
                            <div class="c-content-title-1">
                                <h3 class="c-font-uppercase c-font-bold">Our Clients</h3>
                                <div class="c-line-left c-theme-bg"></div>
                            </div>
                            <!-- End-->
                            <div class="c-logos">
                                <div class="row">
                                    <div class="col-md-4 col-xs-6 c-logo c-logo-1">
                                        <a href="#"><img class="c-img-pos" src="../../assets/base/img/content/client-logos/client1.jpg" alt=""/></a>
                                    </div>
                                    <div class="col-md-4 col-xs-6 c-logo c-logo-2">
                                        <a href="#"><img class="c-img-pos" src="../../assets/base/img/content/client-logos/client2.jpg" alt=""/></a>
                                    </div>
                                    <div class="col-md-4 col-xs-6 c-logo c-logo-3">
                                        <a href="#"><img class="c-img-pos" src="../../assets/base/img/content/client-logos/client3.jpg" alt=""/></a>
                                    </div>
                                    <div class="col-md-4 col-xs-6 c-logo c-logo-4">
                                        <a href="#"><img class="c-img-pos" src="../../assets/base/img/content/client-logos/client4.jpg" alt=""/></a>
                                    </div>
                                    <div class="col-md-4 col-xs-6 c-logo c-logo-5">
                                        <a href="#"><img class="c-img-pos" src="../../assets/base/img/content/client-logos/client5.jpg" alt=""/></a>
                                    </div>
                                    <div class="col-md-4 col-xs-6 c-logo c-logo-6">
                                        <a href="#"><img class="c-img-pos" src="../../assets/base/img/content/client-logos/client6.jpg" alt=""/></a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div><!-- END: CONTENT/MISC/ABOUT-1 -->

        <!-- BEGIN: CONTENT/TESTIMONIALS/TESTIMONIALS-2 -->
        <div class="c-content-box c-size-lg c-bg-white">
            <div class="container">
                <!-- Begin: testimonials 1 component -->
                <div class="c-content-testimonials-1 c-option-2 wow animated fadeIn" data-slider="owl">
                    <!-- Begin: Title 1 component -->
                    <div class="c-content-title-1">
                        <h3 class="c-center c-font-uppercase c-font-bold">Our Satisfied Customers</h3>
                        <div class="c-line-center c-theme-bg"></div>
                    </div>
                    <!-- End-->

                    <!-- Begin: Owlcarousel -->

                    <div class="owl-carousel owl-theme c-theme c-owl-nav-center wow animated fadeInUp" data-single-item="true" data-slide-speed="8000" data-rtl="true">
                        <div class="item">
                            <div class="c-testimonial">
                                <p>
                                    “JANGO is an international, privately held company that specializes in the start-up, promotion and operation of multiple online marketplaces”
                                </p>
                                <h3>
                                    <span class="c-name c-theme">John Snow</span>, CEO, Mockingbird
                                </h3>
                            </div>
                        </div>
                        <div class="item">
                            <div class="c-testimonial">
                                <p>
                                    “After co-founding the company in 2006 the group launched JANGO, the first digital marketplace which focused on rich multimedia web content”
                                </p>
                                <h3>
                                    <span class="c-name c-theme">Arya Stark</span>,  CFO, Valar Dohaeris
                                </h3>
                            </div>
                        </div>
                        <div class="item">
                            <div class="c-testimonial">
                                <p>
                                    “It was the smoothest implementation process I have ever been through with JANGO’s process and schedule.”
                                </p>
                                <h3>
                                    <span class="c-name c-theme">Arya Stark</span>,  CFO, Valar Dohaeris
                                </h3>
                            </div>
                        </div>
                        <div class="item">
                            <div class="c-testimonial">
                                <p>
                                    “A system change is always stressful and JANGO did a great job of staying positive, helpful, and patient with us.”
                                </p>
                                <h3>
                                    <span class="c-name c-theme">Arya Stark</span>,  CFO, Valar Dohaeris
                                </h3>
                            </div>
                        </div>
                    </div>
                    <!-- End-->
                </div>
                <!-- End-->
            </div>
        </div><!-- END: CONTENT/TESTIMONIALS/TESTIMONIALS-2 -->

        <!-- END: PAGE CONTENT -->
    </div>
    <!-- END: PAGE CONTAINER -->

    <!-- BEGIN: LAYOUT/FOOTERS/FOOTER-5 -->



@endsection


