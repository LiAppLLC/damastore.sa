php artisan migrate --path=/database/migrations/2020_12_29_111049_create_home_sliders_table.php
php artisan migrate --path=/database/migrations/2020_12_29_115528_payments.php
php artisan migrate --path=/database/migrations/2020_12_29_120658_orderstates.php
php artisan db:seed --class=orderstates
php artisan migrate --path=/database/migrations/2020_12_29_125541_products.php
php artisan migrate --path=/database/migrations/2020_12_29_175255_categories.php
php artisan migrate --path=/database/migrations/2020_12_29_183346_types.php

php artisan migrate:refresh --path=/database/migrations/2020_12_29_175255_categories.php

for update type with category
php artisan migrate:refresh --path=/database/migrations/2020_12_29_183346_types.php    //done in server

create attributes table with migrations file 2021_01_04_131023_create_attributes_table
php artisan migrate --path=/database/migrations/2021_01_04_131023_create_attributes_table.php //done in server

create attributes_value table with migrations files 2021_01_04_134150_create_attribute_values_table
php artisan migrate --path=/database/migrations/2021_01_04_134150_create_attribute_values_table.php //done in server

update arabic and english attributes
php artisan migrate:refresh --path=/database/migrations/2021_01_04_131023_create_attributes_table.php //done in server

update arabic and english attribute_values
php artisan migrate:refresh --path=/database/migrations/2021_01_04_134150_create_attribute_values_table.php //done in server

update shipping_costs
php artisan migrate:refresh --path=/database/migrations/2020_12_29_125541_products.php  //done in server

update category svg 2021_01_12_123804_orders
php artisan migrate:refresh --path=/database/migrations/2020_12_29_175255_categories.php  //done in server

create orders with migrations files 2021_01_12_123804_orders
php artisan migrate --path=/database/migrations/2021_01_12_123804_orders.php //done in server

create add_address_to_users with migrations files 2021_01_21_103319_add_address_to_users
php artisan make:migration add_address_to_users --table=users
php artisan migrate --path=/database/migrations/2021_01_21_103319_add_address_to_users.php//done in server


create add_supplier_to_products with migrations files 2021_01_21_132127_add_supplier_to_products
php artisan make:migration add_supplier_to_products --table=products
php artisan migrate --path=/database/migrations/2021_01_21_132127_add_supplier_to_products.php //done in server


create add_shipping_costs_to_orders with migrations files 2021_01_23_194704_add_shipping_costs_to_orders.php
php artisan make:migration add_shipping_costs_to_orders --table=orders
php artisan migrate --path=/database/migrations/2021_01_23_194704_add_shipping_costs_to_orders.php //done in server

2021_01_28_190116_add_shipping_info_to_payments

create 2021_01_28_190116_add_shipping_info_to_payments with migrations 2021_01_28_190116_add_shipping_info_to_payments.php
php artisan make:migration add_shipping_info_to_payments --table=payments
php artisan migrate --path=/database/migrations/2021_01_28_190116_add_shipping_info_to_payments.php //done in server

create refund table
php artisan migrate --path=/database/migrations/2021_02_05_143347_create_refunds_table.php //done in server

create coupon table
php artisan migrate --path=/database/migrations/2021_02_18_143541_create_coupons_table.php  //done in server

create user_coupon table
php artisan migrate --path=/database/migrations/2021_02_19_083430_create_user_coupons_table.php //done in server


add_column_device_token table
php artisan migrate --path=/database/migrations/2021_02_24_210105_add_column_device_token.php //done in server


Ccategory table
php artisan migrate --path=/database/migrations/2021_05_27_171349_create_ccategories_table.php //done in server


Ctype table
php artisan migrate --path=/database/migrations/2021_05_30_102603_create_ctypes_table.php //done in server


Cproduct table
php artisan migrate --path=/database/migrations/2021_05_30_102813_create_cproducts_table.php //done in server

cimages_galleries table
php artisan migrate --path=/database/migrations/2021_06_05_060703_create_cimages_galleries_table.php //done in server


