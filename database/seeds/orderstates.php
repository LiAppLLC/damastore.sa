<?php

use Illuminate\Database\Seeder;
use  Illuminate\Support\Facades\DB;

class orderstates extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('orderstates')->insert([
            'name' => 'Order'
        ]);
        DB::table('orderstates')->insert([
            'name' => 'Confirm'
        ]);
        DB::table('orderstates')->insert([
            'name' => 'Packed'
        ]);
        DB::table('orderstates')->insert([
            'name' => 'Shipped'
        ]);
        DB::table('orderstates')->insert([
            'name' => 'Deliver'
        ]);
        DB::table('orderstates')->insert([
            'name' => 'Created'
        ]);
        DB::table('orderstates')->insert([
            'name' => 'Canceled'
        ]);
        DB::table('orderstates')->insert([
            'name' => 'Paid'
        ]);
        DB::table('orderstates')->insert([
            'name' => 'Failed'
        ]);
    }
}
