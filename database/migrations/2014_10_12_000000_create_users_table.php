<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->id();
            $table->string('username')->unique();
            $table->string('email')->nullable();
            $table->string('firstName')->nullable();
            $table->string('lastName')->nullable();
            $table->date('bornDate')->nullable();
            $table->string('phone')->nullable();
            $table->timestamp('emailVerifiedAt')->nullable();
            $table->string('password');
            $table->boolean('verify')->default(false);
            $table->boolean('blocked')->default(false);
            $table->timestamp('lastActivity')->nullable();
            $table->timestamp('lastLoggedIn')->nullable();
            $table->rememberToken();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
