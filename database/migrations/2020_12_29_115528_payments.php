<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class Payments extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('payments', function (Blueprint $table) {
            $table->id();
            $table->string('orderID')->nullable();
            $table->string('payerID');
            $table->string('create_time')->nullable();
            $table->string('country_code');
            $table->string('email_address');
            $table->string('UserFullName');
            $table->string('payer_id');
            $table->string('amountValue');
            $table->string('amountCurrency_code');
            $table->string('payeeEmail_address');
            $table->string('payeeMerchant_id');
            $table->string('captures_id');
            $table->string('reference_id');
            $table->string('status');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('payments');
    }
}
