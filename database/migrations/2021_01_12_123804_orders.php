<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class Orders extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('orders', function (Blueprint $table) {
            $table->id();
            $table->string('orderID')->nullable();
            $table->integer('total');
            $table->integer('quantity');
            $table->integer('tax');
            $table->integer('shipping');
            $table->unsignedBigInteger('user_id');
            $table->unsignedBigInteger('orderStatus_id');
            $table->foreign('user_id')->references('id')->on('users');
            $table->foreign('orderStatus_id')->references('id')->on('orderstates');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('orders');
    }
}
