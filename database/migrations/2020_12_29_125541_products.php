<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class Products extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('products', function (Blueprint $table) {
            $table->id();
            $table->string('product_code');
            $table->bigInteger('price');
            $table->bigInteger('price_sale');
            $table->bigInteger('quantity');
            $table->bigInteger('type_id');
            $table->bigInteger('category_id');
            $table->bigInteger('user_id');
            $table->string('shipping_costs');
            $table->string('unit');
            $table->text('description');
            $table->text('ar_description');
            $table->string('name');
            $table->string('ar_name');
            $table->string('short_description');
            $table->string('ar_short_description');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('products');
    }
}
