<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCproductsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('cproducts', function (Blueprint $table) {
            $table->id();
            $table->string('product_code');
            $table->bigInteger('price')->nullable();
            $table->boolean('is_price')->default(0);
            $table->unsignedBigInteger('type_id');
            $table->unsignedBigInteger('category_id');
            $table->bigInteger('user_id')->nullable();
            $table->bigInteger('shipping_costs');
            $table->text('description');
            $table->text('ar_description');
            $table->string('name');
            $table->string('ar_name');
            $table->string('short_description');
            $table->string('ar_short_description');
            $table->string('supplier');
            $table->string('image',1000);
            $table->string('days');
            $table->boolean('available')->default(0);
            $table->foreign('type_id')->references('id')->on('ctypes')->onDelete('cascade');
            $table->foreign('category_id')->references('id')->on('ccategories')->onDelete('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('cproducts');
    }
}
